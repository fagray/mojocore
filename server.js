'use strict';
/**
 * Module dependencies.
 */
var init = require('./config/init')(),
	config = require('./config/config'),
	fs =  require('fs'),
	Q =  require('q');
	//mongoose = require('mongoose');


function checkDir(){
	var deferred = Q.defer();
	var dir = './log';
	fs.exists(dir, function(exists) {
		if (!exists) {
			console.error('logging directory not exist, but was created');
			fs.mkdirSync(dir);
		}
		deferred.resolve();
	});
	return deferred.promise;
}

function checkFile(){
	var deferred = Q.defer();
	var file = './log/logfile.log';
	fs.exists(file, function(exists) {
		if (!exists) {
			fs.writeFile(file, '', function(err, fd) {
				if (err) throw 'error writing file ' + err;
				console.error('logging file not exist, but was created');
			});
		}
		deferred.resolve();
	});
	return deferred.promise;
}

Q.fcall(checkDir)
.then(checkFile)
.then(function () {
	/**
	 * Main application entry file.
	 * Please note that the order of loading is important.
	 */

	// Bootstrap db connection

	/*var db = mongoose.connect(config.db, function(err) {
		if (err) {
			console.error('\x1b[31m', 'Could not connect to MongoDB!');
			console.log(err);
		}
	});*/

	var db = require('./config/sequelize');


	// Init the express application
	var app = require('./config/express')(db);

	// Bootstrap passport config
	require('./config/passport')();

	// Start the app by listening on <port>
	app.listen(config.port);

	// Expose app
	exports = module.exports = app;

	// Logging initialization
	console.log('MEAN.JS application started on port ' + config.port);    
})
.done();


