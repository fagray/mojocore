var fs = require('fs'),
	request = require('request'),
	log4js = require('log4js');


var logging = {
    	appenders: [
	        {
	            type: 'file',
	            filename: 'log/loadtest.log',
	            category: [ 'loadtest','console' ]
	        },
	        {
	            type: 'console'
	        }
	    ]
	};

log4js.configure(logging);
var Log = log4js.getLogger('loadtest');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

/* global vars */
//var root = "https://dev.mojogateway.mojopay.com/apitransactions?",
var root = "https://localhost:3000/apitransactions?",
	url,
	json,
	request,
	timelimit,
	amount,
	jsonNameFile,
	numberTransactions,
	text = '';

/* convert json file to json var*/
function fileToVar(csvString) {
    var json = [];
    var csvArray = csvString.split("\n");
    var csvColumns = JSON.parse("[" + csvArray.shift().replace(/'/g, '"') + "]");
    csvArray.forEach(function(csvRowString) {
        var csvRow = csvRowString.split(",");
        jsonRow = new Object();
        for ( var colNum = 0; colNum < csvRow.length; colNum++) {
            var colData = csvRow[colNum].replace(/^['"]|['"]$/g, "");
            jsonRow[csvColumns[colNum]] = colData;
        }
        json.push(jsonRow);
    });
    return JSON.stringify(json);
};

/* get arguments */
process.argv.forEach(function (val, index, array) {
  if (index===2) timelimit = val;
  if (index===3) numberTransactions = val;
  if (index===4) jsonNameFile = val;
});

if (typeof timelimit==='undefined'){
	timelimit = 300;
	console.log('Time between Transactions was not defined, we will set at 300 mili seconds');
}

if (typeof numberTransactions==='undefined'){
	numberTransactions = 1000000;
	console.log('Transactions items was not defined, we will do 1 Millon');
}

if (typeof jsonNameFile==='undefined'){
	console.log('A file test is required');
}else{
	fs.exists(jsonNameFile, function(exists) {
		if (!exists) {
			console.log('File do not exist');
		}else{
			var jsonFile = fs.readFileSync(jsonNameFile, 'utf8');
				json = fileToVar(jsonFile);
				json = JSON.parse(json);

			
			// closure function, resolve timeout between requests
			for (var i = 1; i <= numberTransactions; i++) {
			    setTimeout(function(x) {
			    	return function() {
			    		amount = (Math.random() * 1000).toFixed(2);
						url = root + 'type=' + json[0].type + '&processorId=' + json[0].processorId + '&referralProcessorId=' + json[0].referralProcessorId + '&merchantHash=' + json[0].merchantHash + '&referralHash=' + json[0].referralHash + '&customerHash=' + json[0].customerHash + '&amount=' + amount + '&cardSecurityCode=' + json[0].cardSecurityCode;
			    		console.log(x + ' ' + url);
			    		//
			    		request({url:url,async:true});
						/*request({url:url,async:true}, function (error, response, body) {
						    if (error || response.statusCode !== 200) {
						     	Log.debug(error+'\r\n');
						     }
						});*/
			    	};
			    }(i), parseInt(timelimit)*i);
			}
		}
	});
}

