'use strict';
var path = require('path'),
rootPath = path.normalize(__dirname + '/../..');
module.exports = {
    db1: {
        host: 'd55559b98368caac1c7f4d91c859af843770f995.rackspaceclouddb.com',
        port: 3306,
        name: 'mean_relational',
        password: 'pAtRa7puPuQu',
        username: 'mojogwdb'
    },
    root: rootPath,
    modelsDir : rootPath + '/app/models/sequelize',
	facebook: {
		clientID: process.env.FACEBOOK_ID || 'APP_ID',
		clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: 'http://localhost:3000/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || 'APP_ID',
		clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || 'APP_ID',
		clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/github/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'noreply@mojopay.com',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'SendGrid',    
			auth: {
                user: process.env.MAILER_EMAIL_ID || 'mojodisbursement',
                pass: process.env.MAILER_PASSWORD || 'cHEDa&6_SpuPhe5U'
			}
		}
	},
	logging: {
    	appenders: [
	        {
	            type: 'file',
	            filename: 'log/logfile.log',
	            category: [ 'user','console' ]
	        },    	
	        {
	            type: 'file',
	            filename: 'log/logfile.log',
	            category: [ 'transactions','console' ]
	        },
	        {
	            type: 'file',
	            filename: 'log/boardings.log',
	            category: [ 'boardings','console' ]
	        },
	        {
	            type: 'console'
	        }
	    ],
	    replaceConsole: false
	},
	merchant: {
		username: 'Demo321',
		password: 'demo1234'
	},
	api_url: {
		root_transact: 'https://secure.mojopaygateway.com/api/transact.php',
		root_query: 'https://secure.mojopaygateway.com/api/query.php',
		root_cart: 'https://secure.mojopaygateway.com/cart/cart.php'
	},
    resetPasswordExpires: 24, // (e.g.24 hours). Don't put 0, or string values, or special characters, etc. Just integer hour
	ssl: {
        secure: true
	},
	transactionsComeFromNMI: false // false means that come from mysql 
};
