'use strict';
var path = require('path'),
rootPath = path.normalize(__dirname + '/../..');
module.exports = {
	db1: {name: 'mean_relational',password: '',username: 'root'},
    root: rootPath,
    modelsDir : rootPath + '/app/models/sequelize',
	//db: 'mongodb://localhost/mojo-gateway-test',
	port: 3001,
	app: {
		title: 'Mojo Gateway - Test Environment'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
				pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
			}
		}
	}
};