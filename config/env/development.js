'use strict';

/**
 *
 * This file will be loaded in a development environment, a property defined here will overwrite the one defined in the all.js file.
 *
 * @type {exports}
 *
 * db1                      Database configuration
 * root                     Current root for this file
 * modelsDir                Root for Sequelize
 * app                      Contain a project title
 * facebook                 Set credentials to do sign up with Facebook API (not used yet)
 * twitter                  Set credentials to do sign up with Twitter API (not used yet)
 * google                   Set credentials to do sign up with Google API (not used yet)
 * linkedin                 Set credentials to do sign up with Linkedin API (not used yet)
 * github                   Set credentials to do sign up with Github API (not used yet)
 * mailer                   Set email provider and credentials to send automatic notifications, reference: http://www.nodemailer.com/
 * logging                  Log file configuration, reference: https://github.com/nomiddlename/log4js-node
 * api_url                  Represents API roots
 * resetPasswordExpires     Max time (hours) that a password could be reseted after requested
 * ssl                      Switch to use SSL
 * transactionsComeFromNMI  Switch used to indicate where come from transactions date,
 *                          false means that come from mysql and true means that come from NMI API
 */


var path, rootPath;
path = require('path');
rootPath = path.normalize(__dirname + '/../..');
module.exports = {
	//db config
    db1: {
        host: 'd55559b98368caac1c7f4d91c859af843770f995.rackspaceclouddb.com',
        port: 3306,
        name: 'mean_relational',
        password: 'pAtRa7puPuQu',
        username: 'mojogwdb'
    },
    root: rootPath,
    modelsDir : rootPath + '/app/models/sequelize',
	app: {
		title: 'Mojo Gateway - Development Environment'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || 'APP_ID',
		clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: 'http://localhost:3000/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || 'APP_ID',
		clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || 'APP_ID',
		clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/github/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'noreply@mojopay.com',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'SendGrid',    
			auth: {
                user: process.env.MAILER_EMAIL_ID || 'mojodisbursement',
                pass: process.env.MAILER_PASSWORD || 'cHEDa&6_SpuPhe5U'
			}
		}
	},
	logging: {
    	appenders: [
	        {
	            type: 'file',
	            filename: 'log/logfile.log',
	            category: [ 'user','console' ]
	        },    	
	        {
	            type: 'file',
	            filename: 'log/logfile.log',
	            category: [ 'transactions','console' ]
	        },
	        {
	            type: 'file',
	            filename: 'log/logfile.log',
	            category: [ 'disbursement','console' ]
	        },
	        {
	            type: 'file',
	            filename: 'log/boardings.log',
	            category: [ 'boardings','console' ]
	        },
	        {
	            type: 'console'
	        }
	    ],
	    replaceConsole: false
	},
	merchant: {
		username: 'Demo321',
		password: 'demo1234'
	},
	api_url: {
		root_transact: 'https://secure.mojopaygateway.com/api/transact.php',
		root_query: 'https://secure.mojopaygateway.com/api/query.php',
		root_cart: 'https://secure.mojopaygateway.com/cart/cart.php'
	},
    resetPasswordExpires: 24, // (e.g.24 hours). Don't put 0, or string values, or special characters, etc. Just integer hour
	ssl: {
        secure: true
	},
	transactionsComeFromNMI: false // false means that come from mysql
};
