'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
	User = require('../sequelize').User;

module.exports = function() {
	// Use local strategy
	passport.use(new LocalStrategy({
			usernameField: 'Username',
			passwordField: 'Password'
		},
		function(Username, Password, done) {
			User.find({ where:{Username: Username}}).success(function(user){                
				if (!user) {
						return done(null, false, {
							message: 'Unknown user or invalid password'
						});
					}
				if (!user.authenticate(Password)) {
					return done(null, false, {
						message: 'Unknown user or invalid password'
					});
				}
				if (user.Is_Active != 1){ // must remain != because boolean !== integer
					return done(null, false, {
						message: 'Inactive User'
					});
				}
				return done(null, user);
			 }).error(function(err,user){
	       		 if (err) {
						return done(err);
					}
   			});
		}
	));
};