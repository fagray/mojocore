'use strict';
var fs        = require('fs'),
    path      = require('path'),
    Sequelize = require('sequelize-mysql').sequelize,
     _         = require('lodash'),
    config    = require('./config'),
    db        = {};


// TODO: add Heroku configuration

console.log('Initializing Sequelize');

// create your instance of sequelize
var sequelize = new Sequelize(config.db1.name, config.db1.username, config.db1.password, {
  dialect: 'mysql',
  host: config.db1.host,
  port: config.db1.port,
  storage: config.db1.storage,
  logging: false
});
// loop through all files in models directory ignoring hidden files and this file
fs.readdirSync(config.modelsDir)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== 'index.js');
  })
  // import model files and save model names
  .forEach(function(file) {
    console.log('Loading model file ' + file);
    var model = sequelize.import(path.join(config.modelsDir, file));
    db[model.name] = model;
  });
// invoke associations on each of the models
Object.keys(db).forEach(function(modelName) {
  if (db[modelName].options.hasOwnProperty('associate')) {
    db[modelName].options.associate(db);
  }
});

// Synchronizing any model changes with database. 
// WARNING: this will DROP your database everytime you re-run your application
sequelize
  .sync({force: false})
  .complete(function(err){
    if(err) console.log('An error occured %j',err);
    else console.log('Database Up');
});
 
// assign the sequelize variables to the db object and returning the db. 
module.exports = _.extend({
  sequelize: sequelize,
  Sequelize: Sequelize
}, db);
