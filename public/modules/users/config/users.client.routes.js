'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('accounts', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('board', {
			url: '/signupnmi/:boardingId',
			templateUrl: 'modules/users/views/authentication/boardmerchantnmi.client.view.html'
		}).
		state('boardmojo', {
			url: '/signupmojo/:boardingId',
			templateUrl: 'modules/users/views/authentication/boardmerchantmojo.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invlaid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset-expired', {
			url: '/password/reset/expired',
			templateUrl: 'modules/users/views/password/reset-password-expired.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		}).
		state('nation-builder-integration', {
			url: '/settings/nation_builder_integration',
			templateUrl: 'modules/users/views/settings/nation-builder-integration.client.view.html'
		}).
		state('nation-builder-oauth-callback', {
			url: '/settings/nation_builder_oauth_callback',
			templateUrl: 'modules/users/views/settings/nation-builder-oauth-callback.client.view.html'
		});
	}
]);