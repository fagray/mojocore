/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('SettingsController', function() {
        // Initialize global variables
        var SettingsController,
            scope,
            $httpBackend,
			$stateParams,
			$location,
            $modal,
            Authentication,
            Users;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, _$modal_, _Authentication_, Users) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            
            $stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;
			$modal = _$modal_;
            Users = Users;
            
            // Point global variables to injected services
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25,
                    Username: 'Updater'
                }
            };

			// Initialize the controller
            SettingsController = $controller('SettingsController', {
                $scope: scope,
                Authentication: Authentication,
                Users: Users
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
            it('should have the variable $scope.user defined', function() {
                expect(scope.user).toBeDefined();
            });        
            it('should have function scope.modalOpen defined', function() {
                expect(scope.modalOpen).toBeDefined();
            });        
            it('should have function scope.setTitle defined', function() {
                expect(scope.setTitle).toBeDefined();
            });        
            it('should have function scope.hasConnectedAdditionalSocialAccounts defined', function() {
                expect(scope.hasConnectedAdditionalSocialAccounts).toBeDefined();
            });        
            it('should have function scope.isConnectedSocialAccount defined', function() {
                expect(scope.isConnectedSocialAccount).toBeDefined();
            });        
            it('should have function scope.removeUserSocialAccount  defined', function() {
                expect(scope.removeUserSocialAccount).toBeDefined();
            });        
            it('should have function scope.updateUserProfile defined', function() {
                expect(scope.updateUserProfile).toBeDefined();
            });        
            it('should have function scope.changeUserPassword defined', function() {
                expect(scope.changeUserPassword).toBeDefined();
            });        
        });

        describe('Interface', function() {
            it('should recognize modalOpen as a function', function() {
                expect(angular.isFunction(scope.modalOpen)).toBe(true);
            });
            it('should recognize setTitle as a function', function() {
                expect(angular.isFunction(scope.setTitle)).toBe(true);
            });
            it('should recognize hasConnectedAdditionalSocialAccounts as a function', function() {
                expect(angular.isFunction(scope.hasConnectedAdditionalSocialAccounts)).toBe(true);
            });
            it('should recognize isConnectedSocialAccount as a function', function() {
                expect(angular.isFunction(scope.isConnectedSocialAccount)).toBe(true);
            });
            it('should recognize removeUserSocialAccount as a function', function() {
                expect(angular.isFunction(scope.removeUserSocialAccount)).toBe(true);
            });
            it('should recognize updateUserProfile as a function', function() {
                expect(angular.isFunction(scope.updateUserProfile)).toBe(true);
            });
            it('should recognize changeUserPassword as a function', function() {
                expect(angular.isFunction(scope.changeUserPassword)).toBe(true);
            });
		});

        describe('Initialize scope.user', function() {
            it('should initialize scope.user to the Authentication.user object', function() {
                expect(scope.user).toEqual({ Roles: 'user', Boarding_ID: 25, Username: 'Updater' });
            });        
		});

        describe('modalOpen testing', function() {
            it('should update scope.selected on successful close', function() {
                var fakeModal = {
                    result: {
                        then: function(confirmCallback, cancelCallback) {
                            //Store the callbacks for later when the user clicks on the OK or Cancel button of the dialog
                            this.confirmCallBack = confirmCallback;
                            this.cancelCallback = cancelCallback;
                        }
                    },
                    close: function( item ) {
                        //The user clicked OK on the modal dialog, call the stored confirm callback with the selected item
                        this.result.confirmCallBack( item );
                    },
                    dismiss: function( item ) {
                        //The user clicked cancel on the modal dialog, call the stored cancel callback
                        this.result.cancelCallback( item );
                    }
                };

                scope.selected = '';
                scope.modalOpen('value');
                scope.modalInstance.dismiss('Cancel');
                expect(scope.selected).toBe('');

                scope.selected = '';
                spyOn($modal, 'open').and.returnValue(fakeModal);
                scope.modalOpen('value');
                scope.modalInstance.close('item1');
                expect(scope.selected).toBe('item1');
            });
        });
        
        describe('Not Logged in', function(){
            it('should not redirect if user is not logged in', function() {
                spyOn($location, 'path');
                expect(scope.user).toEqual({Roles: 'user', Boarding_ID: 25, Username: 'Updater' });
		    	expect(scope.user).not.toBe(false);
                expect($location.path).not.toHaveBeenCalledWith('/');
	    	});
        });

		describe('SetTitle function testing', function() {
            it('should call $broadcast on $scope.$parent.$parent',function() {
                var value = 'Foxtrot Oscar Oscar';
                var parent = scope.$parent.$parent;
                spyOn(parent, '$broadcast').and.callThrough();
                scope.setTitle(value);    		
                expect(parent.$broadcast).toHaveBeenCalledWith('setTitle', value);
                expect(parent.$broadcast.calls.mostRecent().args[0]).toEqual('setTitle');
                expect(parent.$broadcast.calls.mostRecent().args[1]).toEqual(value);
			});
        });

        describe('hasConnectedAdditionalSocialAccount function testing', function(){
            it('should return false if there are no additional accounts',function() {
                scope.user.additionalProvidersData = null;
                expect(scope.hasConnectedAdditionalSocialAccounts()).toBe(false);
            });
            it('should return true if there are additional accounts',function() {
                scope.user.additionalProvidersData = ['account'];
                expect(scope.hasConnectedAdditionalSocialAccounts()).toBe(true);
            });
        });
        
        describe('isConnectedSocialAccount function testing', function(){
            it('should return false if there are no additional accounts',function() {
                var provider;
                
                provider = 'junk';
                scope.user.provider = false; // this is false
                scope.user.additionalProvidersData = false; // this is false
                expect(scope.isConnectedSocialAccount(provider)).toBe(false);
                
                provider = 'junk';
                scope.user.provider = false; // this is false
                scope.user.additionalProvidersData = {junk:false}; // junk is false
                expect(scope.isConnectedSocialAccount(provider)).toBe(false);

                provider = 'junk';
                scope.user.provider = false;  // this is false
                scope.user.additionalProvidersData = {garbage:true}; // junk is undefined
                expect(scope.isConnectedSocialAccount(provider)).toBeUndefined();
                
                scope.user.provider = false; // this is false
                // scope.user.additionalProvidersData is undefined
                expect(scope.isConnectedSocialAccount(provider)).toBeUndefined();
            });
            it('should return true if there are additional accounts',function() {
                var provider = 'junk';

                scope.user.provider = provider; // this is true
                scope.user.additionalProvidersData = {junk:false}; // this is false
                expect(scope.isConnectedSocialAccount(provider)).toBe(true);
                
                scope.user.provider = false;  // this is false
                scope.user.additionalProvidersData = {junk:true}; // this is true
                expect(scope.isConnectedSocialAccount(provider)).toBe(true);
            });
            it('should return true if there are additional accounts II',function() {
                var provider = 'junk';
                scope.user.provider = provider; // this is true
                // scope.user.additionalProvidersData // this is undefined
                expect(scope.isConnectedSocialAccount(provider)).toBe(true);
            });
        });
        
        describe('removeUserSocialAccount function testing', function(){
            it('should define $scope.success variable',function() {
                expect(scope.success).toBeUndefined();
                scope.removeUserSocialAccount();
                expect(scope.success).toBeDefined();
                expect(scope.success).toBe(null);
            });
            it('should define $scope.error variable',function() {
                expect(scope.error).toBeUndefined();
                scope.removeUserSocialAccount();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe(null);
            });
            it('should call /users/accounts',function() {
    			$httpBackend.when('DELETE', '/users/accounts').respond(200, 'Response');
                scope.removeUserSocialAccount();
        		$httpBackend.flush();

                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should set scope.succes on successful call',function() {
    			$httpBackend.when('DELETE', '/users/accounts').respond(200, 'Response');
                scope.removeUserSocialAccount();
        		$httpBackend.flush();
                expect(scope.success).toBe(true);
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should set scope.user on successful call',function() {
    			$httpBackend.when('DELETE', '/users/accounts').respond(200, 'Response');
                scope.removeUserSocialAccount();
        		$httpBackend.flush();
                expect(scope.user).toBe('Response');
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should set Authentication.user on successful call',function() {
    			$httpBackend.when('DELETE', '/users/accounts').respond(200, 'Response');
                scope.removeUserSocialAccount();
        		$httpBackend.flush();
                // unsure how to test the Authentication.user update               
                expect(Authentication.user).not.toBe('bluebird');
                expect(Authentication.user).toBe('Response');
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should set scope.error on usuccessful call',function() {
    			$httpBackend.when('DELETE', '/users/accounts').respond(500, {message: 'Error!'});
                scope.removeUserSocialAccount();
        		$httpBackend.flush();
                expect(scope.error).toBe('Error!');
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
        });
        
        describe('updateUserProfile function', function(){
            it('should call scope.modalOpen function',function() {
                var value = false;
                spyOn(scope, 'modalOpen').and.returnValue(false);
                scope.updateUserProfile(value);
                expect(scope.modalOpen).toHaveBeenCalledWith();
    		});
            it('should set $scope.submitted if passed false value',function() {
                var value = false;
                spyOn(scope, 'modalOpen').and.returnValue(false);
                scope.updateUserProfile(value);
                expect(scope.submitted).toBe(true);
    		});
            it('should call $broadcast on $scope.$parent.$parent',function() {
                var value = true;
                var parent = scope.$parent.$parent;
                spyOn(parent, '$broadcast').and.callThrough();
                
                scope.updateUserProfile(value);    		
                expect(parent.$broadcast).toHaveBeenCalled();
                expect(parent.$broadcast.calls.mostRecent().args[0]).toEqual('updateSession');
                expect(JSON.stringify(parent.$broadcast.calls.mostRecent().args[1])).toEqual(JSON.stringify({Roles: 'user', Boarding_ID: 25, Username: 'Updater', Last_Changed_By: 'Updater' }));
            });
            it('should define $scope.success variable',function() {
                var value = true;
                expect(scope.success).toBeUndefined();
                scope.updateUserProfile(value);
                expect(scope.success).toBeDefined();
                expect(scope.success).toBe(null);
            });
            it('should define $scope.error variable',function() {
                var value = true;
                expect(scope.error).toBeUndefined();
                scope.updateUserProfile(value);
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe(null);
            });
            it('should call user.$update ',function() {
                var value = true;
                $httpBackend.when('GET', 'myModalContent.html').respond(200, 'Response');
                $httpBackend.when('PUT', 'users').respond(200, 'Response');
                scope.updateUserProfile(value);                
        		$httpBackend.flush();
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should set scope.success to true if call was successful',function() {
                var value = true;
                $httpBackend.when('GET', 'myModalContent.html').respond(200, 'Result');
                $httpBackend.when('PUT', 'users').respond(200, 'Response');
                scope.updateUserProfile(value);                
        		$httpBackend.flush();
                expect(scope.success).toBe(true);
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should set Authentication.user on successful call',function() {
                var value = true;
                $httpBackend.when('GET', 'myModalContent.html').respond(200, 'Result');
                $httpBackend.when('PUT', 'users').respond(200, 'Response');
                scope.updateUserProfile(value);                
        		$httpBackend.flush();
                
                // unsure how to test the Authentication.user update               
                expect(Authentication.user).not.toBe('bluebird');

                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should set scope.error to true if call failed',function() {
                var value = true;
                $httpBackend.when('GET', 'myModalContent.html').respond(200, 'Response');
                $httpBackend.when('PUT', 'users').respond(500, {message: 'Error!'});
                scope.updateUserProfile(value);                
        		$httpBackend.flush();
                expect(scope.error).toBe('Error!');
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
		});

        describe('changeUserPassword function', function() {
            it('should define $scope.success variable',function() {
                expect(scope.success).toBeUndefined();
                scope.passwordDetails = {password:'test'};
    			scope.changeUserPassword();
                expect(scope.success).toBeDefined();
                expect(scope.success).toBe(null);
            });
            it('should define $scope.error variable',function() {
                expect(scope.error).toBeUndefined();
                scope.passwordDetails = {password:'test'};
    			scope.changeUserPassword();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe(null);
            });
            it('should post to /users/password',function() {
                scope.passwordDetails = {password:'test'};
    			$httpBackend.when('POST', '/users/password').respond(200, {data: {message: 'Response'}});
                scope.changeUserPassword();
        		$httpBackend.flush();

                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should define $scope.success variable as true on success',function() {
                expect(scope.success).toBeUndefined();
                scope.passwordDetails = {password:'test'};
    			$httpBackend.when('POST', '/users/password').respond(200, {data: {messgae: 'Response'}});
                scope.changeUserPassword();
        		$httpBackend.flush();

                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.success).toBeDefined();
                expect(scope.success).toBe(true);
            });
            it('should set Authentication.user on successful call',function() {
                scope.passwordDetails = {password:'test'};
    			$httpBackend.when('POST', '/users/password').respond(200, {data: {message: 'Response'}});
                scope.changeUserPassword();
        		$httpBackend.flush();

                // unsure how to test the Authentication.user update               
                expect(Authentication.user).not.toBe('bluebird');

                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should return $scope.error variable if an error',function() {
                expect(scope.error).toBeUndefined();
                scope.passwordDetails = {password:'test'};
    			$httpBackend.when('POST', '/users/password').respond(500, {message: 'Error!'});
                scope.changeUserPassword();
        		$httpBackend.flush();
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.error).toBe('Error!');
            });
        });
    });
}());

(function() {
	describe('SettingsController II', function() {
		// Initialize global variables
		var SettingsController,
			scope,
			$httpBackend,
			$stateParams,
			$location,
            Authentication;

		// Load the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, Authentication) {
			// Set a new global scope
            scope = $rootScope.$new();
            $stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;
            
			// Point global variables to injected services
			spyOn($location, 'path');
            
    		// Initialize the controller
            SettingsController = $controller('SettingsController', {
                $scope: scope,
                Authentication: Authentication
            });

		}));

        describe('Logged in', function(){
           it('should not redirect if user is not logged in', function() {
		    	expect(scope.user).not.toEqual({Roles: 'user', Boarding_ID: 25});
		    	expect(scope.user).toBeUndefined();
                expect($location.path).toHaveBeenCalledWith('/');
	    	});
        });
    });
}());

(function() {
	describe('ModalProfileUpdateController', function() {
		// Initialize global variables
		var ModalProfileUpdateController,
			scope,
			$stateParams,
            modalInstance,
            items;
            
		// Load the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$stateParams_) {
            // Set a new global scope
            scope = $rootScope.$new();
            
            $stateParams = _$stateParams_;
			modalInstance = {                    // Create a mock object using spies
                close: jasmine.createSpy('modalInstance.close'),
                dismiss: jasmine.createSpy('modalInstance.dismiss'),
                result: {
                    then: jasmine.createSpy('modalInstance.result.then')
                }
            };


		    // Initialize the controller
            ModalProfileUpdateController = $controller('ModalProfileUpdateController', {
                $scope: scope,
                $modalInstance: modalInstance,
                items: function () { return ['a', 'b', 'c']; }
            });
        

		}));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have function scope.ok defined', function() {
                expect(scope.ok).toBeDefined();
            });        
            it('should have function scope.cancel defined', function() {
                expect(scope.cancel).toBeDefined();
            });        
        });

        describe('Interface', function() {
            it('should recognize ok as a function', function() {
                expect(angular.isFunction(scope.ok)).toBe(true);
            });
            it('should recognize cancel as a function', function() {
                expect(angular.isFunction(scope.cancel)).toBe(true);
            });
        });

        describe('ok function', function() {
            it('should call the $modalInterface.close() function', function() {
                scope.ok();
                expect(modalInstance.close).toHaveBeenCalledWith();
            });
        });

        describe('cancel function', function() {
            it('should call the $modalInterface.dismiss() function with "cancel"', function() {
                scope.cancel();
                expect(modalInstance.dismiss).toHaveBeenCalledWith('cancel');
            });
        });
    });
}());
