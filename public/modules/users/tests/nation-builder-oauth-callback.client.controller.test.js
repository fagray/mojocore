/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('NationBuilderOAuthController', function() {
        // Initialize global variables
        var $q,
            NationBuilderOAuthController,
            scope,
            $location,
            Authentication;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _Authentication_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();

            // Point global variables to injected services
            $location = _$location_;
            
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
             };            
            // Initialize the controller
            NationBuilderOAuthController = $controller('NationBuilderOAuthController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
            it('should have variable scope.user defined', function() {
                expect(scope.user).toBeDefined();
            });
            it('should have variable scope.sampleProductCategories defined', function() {
                expect(scope.sampleProductCategories).toBeDefined();
            });
            it('should not have variable scope.code defined', function() {
                expect(scope.code).toBeUndefined();
            });

            it('should have function scope.setTitle defined', function() {
                expect(scope.setTitle).toBeDefined();
            });        
            it('should have function scope.initialize defined', function() {
                expect(scope.initialize).toBeDefined();
            });        
        });

       describe('Interface', function() {
            it('should recognize setTitle as a function', function() {
                expect(angular.isFunction(scope.setTitle)).toBe(true);
            });
            it('should recognize initialize as a function', function() {
                expect(angular.isFunction(scope.initialize)).toBe(true);
            });
        });

        it('should expose the user service', function() {
            expect(scope.user).toEqual({Roles: 'user', Boarding_ID: 25});
        });

        describe('Signin Redirect', function() {
            beforeEach(function() {
                spyOn($location, 'path');
            });
            it('should not redirect if user is logged in', function() {
                expect($location.path).not.toHaveBeenCalled();
            });
        });

        describe('setTitle function testing', function() {
            it('should call $broadcast on $scope.$parent.$parent',function() {
                var value = 'Foxtrot Oscar Oscar';
                var parent = scope.$parent.$parent;
                spyOn(parent, '$broadcast').and.callThrough();
                scope.setTitle(value);    		
                expect(parent.$broadcast).toHaveBeenCalledWith('setTitle', value);
                expect(parent.$broadcast.calls.mostRecent().args[0]).toEqual('setTitle');
                expect(parent.$broadcast.calls.mostRecent().args[1]).toEqual(value);
			});
        });

        describe("dropdown-menu button click event", function() {
			var element;
            beforeEach(function(){
				this.element = $('<ul class="dropdown-menu"><li><input type="text" value="dropdown-menu"/></li></ul><ul class="nationBuilderButton"><li><input type="text" value="nationBuilderButton"/></li></ul>');
				this.element.appendTo('body');
			});
            
			// clean up
			afterEach(function(){
//				this.element.remove(); 
			});

			it ("should invoke dropdown-menu input click event", function() {
                var object = $('.dropdown-menu').find('input');
                spyOn(object, 'click').and.callThrough();
                object.trigger('click');
                object.click();
                expect(object.click).toHaveBeenCalledWith();
//            });
//			it ("should invoke nationBuilderButton input click event", function() {
                var object1 = $('.nationBuilderButton').find('input');
                spyOn(object1, 'click').and.callThrough();
                object1.trigger('click');
                object1.click();
                expect(object1.click).toHaveBeenCalledWith();
            });
        });
/*
        // unsure how to properly test $('.nationBuilderButton').find('input').click function handler
        xdescribe("nationBuilderButton button click event", function() {
 			var element1;
            beforeEach(function($compile){
				this.element1 = $('<ul class="nationBuilderButton"><li><input type="text" value="nationBuilderButton"/></li></ul>');
				this.element1.appendTo('body');
			});
            
			// clean up
			afterEach(function(){
//				this.element1.remove(); 
			});

        });
*/
        describe('initialize function testing', function() {
            it('should initialize the scope.code variable',function() {
                var returnedCode = '345ffe67899d798025aac8795462321bbbb2134658dea2121cbe213213eee255';
                spyOn($location, 'search').and.returnValue({code: returnedCode});
                expect(scope.code).toBeUndefined();
                scope.initialize();
                expect(scope.code).toBeDefined();
                expect(scope.code).toBe(returnedCode);
            });
        });

    });
}());


(function() {
        describe('NationBuilderOAuthController', function() {
        // Initialize global variables
        var $q,
            NationBuilderOAuthController,
            scope,
            $httpBackend,
            $stateParams,
            $location,
            Authentication;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, Authentication) {
            // Set a new global scope
            scope = $rootScope.$new();
            $stateParams = _$stateParams_;
            $httpBackend = _$httpBackend_;
            $location = _$location_;
            Authentication = {};
            
            // Point global variables to injected services
            spyOn($location, 'path');
            
        // Initialize the Authentication controller
            NationBuilderOAuthController = $controller('NationBuilderOAuthController', {
                $scope: scope,
                Authentication: Authentication
            });

        }));

        describe('Logged in', function(){
           it('should redirect if user is not logged in', function() {
                expect(scope.Authentication).toBeUndefined();
                expect($location.path).toHaveBeenCalledWith('/');
            });
        });
    });
}());
