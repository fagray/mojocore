'use strict';
describe('A test', function() {
	var base_url = "http://localhost:3000/";
	var Users;
	
	beforeEach(function() {
		module('creditcard');
		module('core');
		module('users');
  	});
	beforeEach(function() {
    	browser().navigateTo('/');
	});
	
	it('true should return true', function() {
		expect(true).toEqual(true);
	});
	
	it('should Exist', function() {
		expect(Users).toBeTruthy();
	});
});