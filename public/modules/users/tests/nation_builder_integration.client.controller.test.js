/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('NationBuilderIntegrationController', function() {
        // Initialize global variables
        var $q,
            NationBuilderIntegrationController,
            scope,
            $httpBackend,
			$stateParams,
			$location,
            $modal,
            Authentication,
            ProductService,
            PaymentService,
            Nmitransactions,
            NationBuilderIntegrationService,
            NationBuilderActivationService,
            NationBuilderInformationService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$q_, _$stateParams_, _$httpBackend_, _$modal_, _Authentication_,_ProductService_, _PaymentService_, _Nmitransactions_, _NationBuilderIntegrationService_, _NationBuilderActivationService_, _NationBuilderInformationService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();

            // Point global variables to injected services
            $stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;
            $q = _$q_;
            $modal = _$modal_;
            
            ProductService = _ProductService_; 
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            NationBuilderIntegrationService = _NationBuilderIntegrationService_;
            NationBuilderActivationService = _NationBuilderActivationService_;
            NationBuilderInformationService = _NationBuilderInformationService_;
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
             };            
            // Initialize the controller
            NationBuilderIntegrationController = $controller('NationBuilderIntegrationController', {
                $scope: scope,
                Authentication: Authentication,
                ProductService: ProductService,
                PaymentService: PaymentService,
                Nmitransactions: Nmitransactions,
                NationBuilderIntegrationService: NationBuilderIntegrationService,
                NationBuilderActivationService: NationBuilderActivationService,
                NationBuilderInformationService: NationBuilderInformationService
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
            it('should have variable scope.user defined', function() {
                expect(scope.user).toBeDefined();
            });
            it('should not have variable scope.queryproductid defined', function() {
                expect(scope.queryproductid).toBeUndefined();
            });
            it('should not have variable scope.productId defined', function() {
                expect(scope.productId).toBeUndefined();
            });
            it('should have variable scope.sampleProductCategories defined', function() {
                expect(scope.sampleProductCategories).toBeDefined();
            });
            it('should have function scope.setTitle defined', function() {
                expect(scope.setTitle).toBeDefined();
            });        
            it('should have function scope.setProductId defined', function() {
                expect(scope.setProductId).toBeDefined();
            });        
            it('should have function scope.initialize defined', function() {
                expect(scope.initialize).toBeDefined();
            });        
            it('should have function scope.getMerchantProductDropdown defined', function() {
                expect(scope.getMerchantProductDropdown).toBeDefined();
            });        
            it('should have function scope.setMerchant defined', function() {
                expect(scope.setMerchant).toBeDefined();
            });        
            it('should have function scope.refreshArray defined', function() {
                expect(scope.refreshArray).toBeDefined();
            });        
            it('should have function scope.inactivateNationBuilder defined', function() {
                expect(scope.inactivateNationBuilder).toBeDefined();
            });        
            it('should have function scope.getCode defined', function() {
                expect(scope.getCode).toBeDefined();
            });        
            it('should have function scope.integrate defined', function() {
                expect(scope.integrate).toBeDefined();
            });        
        });

        describe('Interface', function() {
            it('should recognize setTitle as a function', function() {
                expect(angular.isFunction(scope.setTitle)).toBe(true);
            });
            it('should recognize setProductId as a function', function() {
                expect(angular.isFunction(scope.setProductId)).toBe(true);
            });
            it('should recognize initialize as a function', function() {
                expect(angular.isFunction(scope.initialize)).toBe(true);
            });
            it('should recognize getMerchantProductDropdown as a function', function() {
                expect(angular.isFunction(scope.getMerchantProductDropdown)).toBe(true);
            });
            it('should recognize setMerchant as a function', function() {
                expect(angular.isFunction(scope.setMerchant)).toBe(true);
            });
            it('should recognize refreshArray as a function', function() {
                expect(angular.isFunction(scope.refreshArray)).toBe(true);
            });
            it('should recognize inactivateNationBuilder as a function', function() {
                expect(angular.isFunction(scope.inactivateNationBuilder)).toBe(true);
            });
            it('should recognize getCode as a function', function() {
                expect(angular.isFunction(scope.getCode)).toBe(true);
            });
            it('should recognize integrate as a function', function() {
                expect(angular.isFunction(scope.integrate)).toBe(true);
            });
        });

        it('should expose the user service', function() {
            expect(scope.user).toEqual({Roles: 'user', Boarding_ID: 25});
        });

        describe('Signin Redirect', function() {
            beforeEach(function() {
                spyOn($location, 'path');
            });
            it('should not redirect if user is logged in', function() {
                expect($location.path).not.toHaveBeenCalled();
            });
  
        });

        describe('Initialize function', function(){
            beforeEach(function() {
                spyOn(scope, 'getMerchantProductDropdown');
            });
            it('should call the getMerchantProductDropdown function', function() {
                scope.initialize();
                expect(scope.getMerchantProductDropdown).toHaveBeenCalled();
            });
        });
		
        describe('SetTitle function testing', function() {
            it('should call $broadcast on $scope.$parent.$parent',function() {
                var value = 'Foxtrot Oscar Oscar';
                var parent = scope.$parent.$parent;
                spyOn(parent, '$broadcast').and.callThrough();
                scope.setTitle(value);    		
                expect(parent.$broadcast).toHaveBeenCalledWith('setTitle', value);
                expect(parent.$broadcast.calls.mostRecent().args[0]).toEqual('setTitle');
                expect(parent.$broadcast.calls.mostRecent().args[1]).toEqual(value);
			});
        });

        describe("dropdown-menu button click event", function() {
			var element;
            beforeEach(function(){
				this.element = $('<ul class="dropdown-menu"><li><input type="text" value="dropdown-menu"/></li></ul><ul class="nationBuilderButton"><li><input type="text" value="nationBuilderButton"/></li></ul>');
				this.element.appendTo('body');
			});
            
            // clean up
			afterEach(function() {
                this.element.remove();
            });
            
			it ("should invoke dropdown-menu input click event", function() {
                var object = $('.dropdown-menu').find('input');
                spyOn(object, 'click').and.callThrough();
                object.trigger('click');
                object.click();
                expect(object.click).toHaveBeenCalledWith();
//            });
//			it ("should invoke nationBuilderButton input click event", function() {
                var object1 = $('.nationBuilderButton').find('input');
                spyOn(object1, 'click').and.callThrough();
                object1.trigger('click');
                object1.click();
                expect(object1.click).toHaveBeenCalledWith();
            });
        });
/*
        // unsure how to properly test $('.nationBuilderButton').find('input').click function handler
        describe("nationBuilderButton button click event", function() {
			var element1;
            beforeEach(function(){
				this.element1 = $('<ul class="dropdown-menu"><li><input type="text" value="dropdown-menu"/></li></ul><ul class="nationBuilderButton"><li><input type="text" value="nationBuilderButton"/></li></ul>');
				this.element1.appendTo('body');
			});

			afterEach(function(){
				this.element1.remove(); 
			});

			it ("should invoke nationBuilderButton input click event", function() {
                var object1 = $('.nationBuilderButton').find('input');
console.log('nn----------------',$('.nationBuilderButton'))
console.log('nn================',$('.nationBuilderButton').find('input'))
                spyOn(object1, 'click').and.callThrough();
                object1.click("green");
                expect(object1.click).toHaveBeenCalledWith(true);
            });
        });
*/
        describe('setProductId function', function(){
            beforeEach(function(){
				this.element = $('<div id="parent"><div id="dropdownMenu2"></div></div>');
				this.element.appendTo('body');
			});
			
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});
            
            it('should return the class of "parent" element as blank', function() {
                expect(document.getElementById('parent').className).toBe("");
            });
            it('should toggle the class of element "parent" to open', function() {
                var productId = 'blue';
                scope.setProductId(productId);
                expect(document.getElementById('parent').className).toBe("open");
                scope.setProductId(productId);
                expect(document.getElementById('parent').className).toBe("");
            });
            it('should define scope.queryproductid', function() {
                var passedProductId = 'blue';
                scope.setProductId(passedProductId);
                expect(scope.queryproductid).toBeDefined();
            });
            it('should set scope.queryproductid to blank', function() {
                var passedProductId = 'blue';
                scope.queryproductid = 'red';
                expect(scope.queryproductid).toBe('red');
                scope.setProductId(passedProductId);
                expect(scope.queryproductid).toBe('');
            });
            it('should set scope.productId to passed parameter', function() {
                var passedProductId = 'blue';
                scope.productId = 'red';
                expect(scope.productId).toBe('red');
                scope.setProductId(passedProductId);
                expect(scope.productId).toBe('blue');
            });
        });
        
        describe('getMerchantProductDropdown function I', function(){
            beforeEach(function() {

                var productDeferred,
                    paymentDeferred,
                    nmiTransactionsDeferred,
                    NationBuilderIntegrationDeferred,
                    $promise;
                    
                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                 
                 paymentDeferred = $q.defer();
                 paymentDeferred.resolve({
                        "0":{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":null,"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},
                        "1":{"id":44,"Boarding_ID":25,"Product_ID":2,"Discount_Rate" :0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-05-04T16:40:13.000Z","updatedAt":"2015-08-05T22:02:52.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name0","Processor_ID":"processor0","Password":"LamePassword1","Ach_Reject_Fee":0,"Marketplace_Name":null},
                        "2":{"id":78,"Boarding_ID":25,"Product_ID":4,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-09T23:23:51.000Z","updatedAt":"2015-08-05T21:47:02.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name1","Processor_ID":"processor1","Password":"LamePassword2","Ach_Reject_Fee":0,"Marketplace_Name":null},
                        "3":{"id":85,"Boarding_ID":25,"Product_ID":5,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-19T17:15:41.000Z","updatedAt":"2015-08-05T19:10:52.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name2","Processor_ID":"processor2","Password":"LamePassword3","Ach_Reject_Fee":0,"Marketplace_Name":"Marketplace"}
                 });
                 
                 nmiTransactionsDeferred = $q.defer();
                 nmiTransactionsDeferred.resolve({
                     "id":25,
                     "Date":"2015-04-21",
                     "Merchant_Name":"Demo Account",
                     "Address":"123 Demo Street",
                     "City":"Demoville",
                     "Phone":"555-555-5555",
                     "Merchant_ID":"1",
                     "Bank_ID":"1",
                     "Terminal_ID":"1",
                     "MCC":"8396",
                     "Classification":"0",
                     "Visa":true,
                     "Mastercard":true,
                     "Discover":true,
                     "American_Express":false,
                     "Diners_Club":null,
                     "JCB":null,
                     "Maestro":null,
                     "Max_Ticket_Amount":1500,
                     "Max_Monthly_Amount":100,
                     "Precheck":"nopreauth",
                     "Duplicate_Checking":null,
                     "Allow_Merchant":null,
                     "Duplicate_Threshold":1200,
                     "Account_Description":null,
                     "createdAt":"2015-04-21T20:58:50.000Z",
                     "updatedAt":"2015-08-05T23:32:54.000Z",
                     "Location_Number":"1",
                     "Aquire_Bin":"1",
                     "Store_Number":"1",
                     "Vital_Number":"1",
                     "Agent_Chain":"1",
                     "Required_Name":true,
                     "Required_Company":null,
                     "Required_Address":null,
                     "Required_City":true,
                     "Required_State":true,
                     "Required_Zip":false,
                     "Required_Country":null,
                     "Required_Phone":null,
                     "Required_Email":null,
                     "Reserve_Rate":0,
                     "Reserve_Condition":false,
                     "Cap_Amount":null,
                     "Disbursement":null,
                     "Status":"active",
                     "Welcome_Email":true,
                     "State":"CA",
                     "Zip":"92656"
                 });
                 
                 NationBuilderIntegrationDeferred = $q.defer();
                 NationBuilderIntegrationDeferred.resolve({
                     "response":{
                        "id":10,
                        "Processor_ID":"processor1",
                        "Nation_Builder_Key":"345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25",
                        "URL":"nationtest",
                        "Active":false,
                        "createdAt":"2015-01-01T01:01:01.000Z",
                        "updatedAt":"2015-01-01T01:01:01.000Z"
                    }
                });
                
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions,'get').and.returnValue({$promise: nmiTransactionsDeferred.promise}); //returns a fake promise;
                spyOn(NationBuilderIntegrationService,'get').and.returnValue({$promise: NationBuilderIntegrationDeferred.promise}); //returns a fake promise;
            });
            
            it ('should call ProductService.get service', function() {
                scope.getMerchantProductDropdown();
                scope.$digest();
                expect(ProductService.get).toHaveBeenCalled();
                expect(ProductService.get).toHaveBeenCalledWith({productId: 'all'});
            });
            it ('should call PaymentService.get service', function() {
                scope.getMerchantProductDropdown();
                scope.$digest();
                expect(PaymentService.get).toHaveBeenCalled();
                expect(PaymentService.get).toHaveBeenCalledWith({paymentId:25,productId:'all'});
            });
            it ('should call Nmitransactions.get service', function() {
                scope.getMerchantProductDropdown();
                scope.$digest();
                expect(Nmitransactions.get).toHaveBeenCalled();
                expect(Nmitransactions.get).toHaveBeenCalledWith({type:'getMerchantInfo',boardingId:25});
            });
            it ('should call scope.setMerchant service', function() {
                spyOn(scope, 'setMerchant').and.callThrough();
                scope.getMerchantProductDropdown();
                scope.$digest();
                expect(scope.setMerchant).toHaveBeenCalled();
                expect(scope.setMerchant).toHaveBeenCalledWith([ { id : 25, Name : 'Demo Account', value : 25, products : [ { Name : '#Twt2Pay', Boarding_ID : 25, Product_ID : 2, Processor_ID : 'processor0' }, { Name : 'MojoPay', Boarding_ID : 25, Product_ID : 4, Processor_ID : 'processor1', nationBuilderId : 10, nationURL : 'nationtest', nationActive : false }, { Name : 'Marketplace', Boarding_ID : 25, Product_ID : 5, Processor_ID: 'processor2' } ] } ]);
            });

        });
        
        describe('getMerchantProductDropdown function II', function(){
            beforeEach(function() {

                var productDeferred,
                    paymentDeferred,
                    nmiTransactionsDeferred,
                    NationBuilderIntegrationDeferred,
                    $promise;
                    
                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                 
                 paymentDeferred = $q.defer();
                 paymentDeferred.resolve({
                        "0":{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":null,"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},
                        "1":{"id":44,"Boarding_ID":25,"Product_ID":2,"Discount_Rate" :0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-05-04T16:40:13.000Z","updatedAt":"2015-08-05T22:02:52.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name0","Processor_ID":"processor0","Password":"LamePassword1","Ach_Reject_Fee":0,"Marketplace_Name":null},
                        "2":{"id":78,"Boarding_ID":25,"Product_ID":4,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-09T23:23:51.000Z","updatedAt":"2015-08-05T21:47:02.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name1","Processor_ID":"processor1","Password":"LamePassword2","Ach_Reject_Fee":0,"Marketplace_Name":null},
                        "3":{"id":85,"Boarding_ID":25,"Product_ID":5,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-19T17:15:41.000Z","updatedAt":"2015-08-05T19:10:52.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name2","Processor_ID":"processor2","Password":"LamePassword3","Ach_Reject_Fee":0,"Marketplace_Name":"Marketplace"}
                 });
                 
                 nmiTransactionsDeferred = $q.defer();
                 nmiTransactionsDeferred.reject("This is an error response");
                 
                 NationBuilderIntegrationDeferred = $q.defer();
                 NationBuilderIntegrationDeferred.resolve({
                     "response":{
                        "id":10,
                        "Processor_ID":"processor1",
                        "Nation_Builder_Key":"345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25",
                        "URL":"nationtest",
                        "Active":false,
                        "createdAt":"2015-01-01T01:01:01.000Z",
                        "updatedAt":"2015-01-01T01:01:01.000Z"
                    }
                });
                
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions,'get').and.returnValue({$promise: nmiTransactionsDeferred.promise}); //returns a fake promise;
                spyOn(NationBuilderIntegrationService,'get').and.returnValue({$promise: NationBuilderIntegrationDeferred.promise}); //returns a fake promise;
            });
            
            it ('should display an error when NationBuilderIntegrationService fails', function() {
                spyOn(console,'log').and.callThrough();
                scope.getMerchantProductDropdown();
                scope.$digest();
                expect(console.log).toHaveBeenCalledWith('This is an error response');
             });
        });
        
        describe('setMerchant function', function(){
            var merchantData = new Array(
                {   id : 25, 
                    Name : 'Test Account', 
                    value : 25, 
                    products : [ 
                        {
                            Name : 'test', 
                            Boarding_ID : 25, 
                            Product_ID : 99, 
                            Processor_ID : 'processor0', 
                            nationBuilderId : 10, 
                            nationURL : 'nationtest', 
                            nationActive : true }
                    ]
                 });
                 
            beforeEach(function() {

            });
            
            it('should initialize scope.noIntegrations variable', function(){
                expect(scope.noIntegrations).toBeUndefined();
                scope.setMerchant(merchantData);
                expect(scope.noIntegrations).toBeDefined();
                expect(scope.noIntegrations).toBe(true);
            });

            it('should initialize scope.sampleProductCategories variable', function(){
                expect(scope.sampleProductCategories).toBeDefined();
                expect(scope.sampleProductCategories).toEqual([]);
                scope.setMerchant(merchantData);
                expect(scope.sampleProductCategories).toBeDefined();
                expect(scope.sampleProductCategories).toBe(merchantData);
            });
            it('should initialize scope.Product_ID variable', function(){
                expect(scope.Product_ID).toBeUndefined();
                scope.setMerchant(merchantData);
                expect(scope.Product_ID).toBeDefined();
                expect(scope.Product_ID).toEqual({ Name : 'test', Boarding_ID : 25, Product_ID : 99, Processor_ID : 'processor0', nationBuilderId : 10, nationURL : 'nationtest', nationActive : true });
            });
            
            describe('setMerchant function noIntegration options I', function(){
               var merchantData2 = new Array(
                    {   id : 26, 
                        Name : 'Test Account', 
                        value : 26, 
                        products : [ 
                            {
                                Name : 'test', 
                                Boarding_ID : 26, 
                                Product_ID : 99, 
                                Processor_ID : 'processor1', 
                                nationBuilderId : 10, 
                                nationURL : 'nationtest', 
                                nationActive : true
                            }
                        ]
                     });
                     
                beforeEach(function() {
                    var NationBuilderIntegrationDeferred,
                        $promise;
                    
                    NationBuilderIntegrationDeferred = $q.defer();
                    NationBuilderIntegrationDeferred.resolve({
                        "response":{
                           "id":10,
                           "Processor_ID":"processor1",
                           "Nation_Builder_Key":"345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25",
                           "URL":"nationtest",
                           "Active":true,
                           "createdAt":"2015-01-01T01:01:01.000Z",
                           "updatedAt":"2015-01-01T01:01:01.000Z"
                        }
                    });
                
                    spyOn(NationBuilderIntegrationService,'get').and.returnValue({$promise: NationBuilderIntegrationDeferred.promise}); //returns a fake promise;
                });
                
                it('should call NationBuilderIntegrationService.get()', function(){
                    scope.setMerchant(merchantData2);
                    scope.$digest();
                    expect(NationBuilderIntegrationService.get).toHaveBeenCalledWith({ProcessorID:'processor1'});
                });


                it('should set scope.noIntegrations variable to false if there are integrations', function(){
                    scope.setMerchant(merchantData2);
                    scope.$digest();
                    expect(scope.noIntegrations).toBe(false);
                });
            });
            
            describe('setMerchant function noIntegration options II', function(){
                var merchantData2 = new Array(
                    {   id : 27, 
                        Name : 'Test Account', 
                        value : 27, 
                        products : [ 
                            {
                                Name : 'test', 
                                Boarding_ID : 27, 
                                Product_ID : 99, 
                                Processor_ID : 'processor2', 
                                nationBuilderId : 10, 
                                nationURL : 'nationtest', 
                                nationActive : true }
                        ]
                     });
                     
                 beforeEach(function() {
                    var NationBuilderIntegrationDeferred,
                        $promise;
                    
                    NationBuilderIntegrationDeferred = $q.defer();
                    NationBuilderIntegrationDeferred.resolve({
                        "response":{
                           "id":10,
                           "Processor_ID":"processor2",
                           "Nation_Builder_Key":"345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25",
                           "URL":"nationtest",
                           "Active":false,
                           "createdAt":"2015-01-01T01:01:01.000Z",
                           "updatedAt":"2015-01-01T01:01:01.000Z"
                        }
                    });
                
                    spyOn(NationBuilderIntegrationService,'get').and.returnValue({$promise: NationBuilderIntegrationDeferred.promise}); //returns a fake promise;
                 });
            
            
                it('should leave scope.noIntegrations variable as true if integration is not active', function(){
                    scope.setMerchant(merchantData2);
                    scope.$digest();
                    expect(scope.noIntegrations).toBe(true);
                });
            });
            
            describe('setMerchant function noIntegration options III', function(){
                var merchantData2 = new Array(
                    {   id : 28, 
                        Name : 'Test Account', 
                        value : 28, 
                        products : [ 
                            {
                                Name : 'test', 
                                Boarding_ID : 28, 
                                Product_ID : 99, 
                                Processor_ID : 'processor3'}
                        ]
                     });
                     
                beforeEach(function() {
                    var NationBuilderIntegrationDeferred,
                        $promise;
                    
                    NationBuilderIntegrationDeferred = $q.defer();
                    NationBuilderIntegrationDeferred.resolve({
                        "response":{}
                    });
                
                    spyOn(NationBuilderIntegrationService,'get').and.returnValue({$promise: NationBuilderIntegrationDeferred.promise}); //returns a fake promise;
                });

                it('should leave scope.noIntegrations variable as true if there are no integrations', function(){
                        scope.setMerchant(merchantData2);
                        scope.$digest();
                        expect(scope.noIntegrations).toBe(true);
                });
            });
        
            describe('setMerchant function noIntegration options IV', function(){
                var merchantData2 = new Array(
                    {   id : 28, 
                        Name : 'Test Account', 
                        value : 28, 
                        products : [ 
                            {
                                Name : 'test', 
                                Boarding_ID : 28, 
                                Product_ID : 99, 
                                Processor_ID : ''}
                        ]
                     });
                     
                beforeEach(function() {
                    var NationBuilderIntegrationDeferred,
                        $promise;
                    
                    NationBuilderIntegrationDeferred = $q.defer();
                    NationBuilderIntegrationDeferred.resolve({
                        "response":{
                           "id":10,
                           "Processor_ID":"processor2",
                           "Nation_Builder_Key":"345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25",
                           "URL":"nationtest",
                           "Active":false,
                           "createdAt":"2015-01-01T01:01:01.000Z",
                           "updatedAt":"2015-01-01T01:01:01.000Z"
                        }
                    });
                
                    spyOn(NationBuilderIntegrationService,'get').and.returnValue({$promise: NationBuilderIntegrationDeferred.promise}); //returns a fake promise;
                });

                it('should leave scope.noIntegrations variable as true if there is no Processor_ID', function(){
                        scope.setMerchant(merchantData2);
                        scope.$digest();
                        expect(scope.noIntegrations).toBe(true);
                });
            });
        
        });
        
        describe('refreshArray function I', function(){
            beforeEach(function() {
                var productDeferred,
                    paymentDeferred,
                    nmiTransactionsDeferred,
                    NationBuilderIntegrationDeferred,
                    $promise;
                    
                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                 
                 paymentDeferred = $q.defer();
                 paymentDeferred.resolve({
                        "0":{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":null,"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},
                        "1":{"id":44,"Boarding_ID":25,"Product_ID":2,"Discount_Rate" :0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-05-04T16:40:13.000Z","updatedAt":"2015-08-05T22:02:52.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name0","Processor_ID":"processor0","Password":"LamePassword1","Ach_Reject_Fee":0,"Marketplace_Name":null},
                        "2":{"id":78,"Boarding_ID":25,"Product_ID":4,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-09T23:23:51.000Z","updatedAt":"2015-08-05T21:47:02.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name1","Processor_ID":"processor1","Password":"LamePassword2","Ach_Reject_Fee":0,"Marketplace_Name":null},
                        "3":{"id":85,"Boarding_ID":25,"Product_ID":5,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-19T17:15:41.000Z","updatedAt":"2015-08-05T19:10:52.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name2","Processor_ID":"processor2","Password":"LamePassword3","Ach_Reject_Fee":0,"Marketplace_Name":"Marketplace"}
                 });
                 
                 nmiTransactionsDeferred = $q.defer();
                 nmiTransactionsDeferred.resolve({
                     "id":25,
                     "Date":"2015-04-21",
                     "Merchant_Name":"Demo Account",
                     "Address":"123 Demo Street",
                     "City":"Demoville",
                     "Phone":"555-555-5555",
                     "Merchant_ID":"1",
                     "Bank_ID":"1",
                     "Terminal_ID":"1",
                     "MCC":"8396",
                     "Classification":"0",
                     "Visa":true,
                     "Mastercard":true,
                     "Discover":true,
                     "American_Express":false,
                     "Diners_Club":null,
                     "JCB":null,
                     "Maestro":null,
                     "Max_Ticket_Amount":1500,
                     "Max_Monthly_Amount":100,
                     "Precheck":"nopreauth",
                     "Duplicate_Checking":null,
                     "Allow_Merchant":null,
                     "Duplicate_Threshold":1200,
                     "Account_Description":null,
                     "createdAt":"2015-04-21T20:58:50.000Z",
                     "updatedAt":"2015-08-05T23:32:54.000Z",
                     "Location_Number":"1",
                     "Aquire_Bin":"1",
                     "Store_Number":"1",
                     "Vital_Number":"1",
                     "Agent_Chain":"1",
                     "Required_Name":true,
                     "Required_Company":null,
                     "Required_Address":null,
                     "Required_City":true,
                     "Required_State":true,
                     "Required_Zip":false,
                     "Required_Country":null,
                     "Required_Phone":null,
                     "Required_Email":null,
                     "Reserve_Rate":0,
                     "Reserve_Condition":false,
                     "Cap_Amount":null,
                     "Disbursement":null,
                     "Status":"active",
                     "Welcome_Email":true,
                     "State":"CA",
                     "Zip":"92656"
                 });
                 
                 NationBuilderIntegrationDeferred = $q.defer();
                 NationBuilderIntegrationDeferred.resolve({
                     "response":{
                        "id":10,
                        "Processor_ID":"processor1",
                        "Nation_Builder_Key":"345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25",
                        "URL":"nationtest",
                        "Active":true,
                        "createdAt":"2015-01-01T01:01:01.000Z",
                        "updatedAt":"2015-01-01T01:01:01.000Z"
                    }
                });
                
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions,'get').and.returnValue({$promise: nmiTransactionsDeferred.promise}); //returns a fake promise;
                spyOn(NationBuilderIntegrationService,'get').and.returnValue({$promise: NationBuilderIntegrationDeferred.promise}); //returns a fake promise;
            });

            it('should change scope.noIntegrations variable from false to true', function(){
                scope.sampleProductCategories = new Array(
                    {   id : 30, 
                        Name : 'Test Account', 
                        value : 30, 
                        products : [ 
                            {
                                Name : 'test', 
                                Boarding_ID : 30, 
                                Product_ID : 99, 
                                Processor_ID : 'processor1', 
                                nationBuilderId : 10, 
                                nationURL : 'nationtest', 
                                nationActive : true }
                        ]
                     });
                var NBarray = {
                    "response":{
                       "id":10,
                       "Processor_ID":"processor1",
                       "Nation_Builder_Key":"345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25",
                       "URL":"nationtest",
                       "Active":false,
                       "createdAt":"2015-01-01T01:01:01.000Z",
                       "updatedAt":"2015-01-01T01:01:01.000Z"
                    }
                };
                scope.getMerchantProductDropdown();
                scope.$digest();
                expect(scope.noIntegrations).toBe(false);
                scope.refreshArray(NBarray);
                expect(scope.noIntegrations).toBe(true);
            });
        });

        describe('refreshArray function II', function(){
            beforeEach(function() {
                var productDeferred,
                    paymentDeferred,
                    nmiTransactionsDeferred,
                    NationBuilderIntegrationDeferred,
                    $promise;
                    
                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                 
                 paymentDeferred = $q.defer();
                 paymentDeferred.resolve({
                        "0":{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":null,"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},
                        "1":{"id":44,"Boarding_ID":25,"Product_ID":2,"Discount_Rate" :0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-05-04T16:40:13.000Z","updatedAt":"2015-08-05T22:02:52.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name0","Processor_ID":"processor0","Password":"LamePassword1","Ach_Reject_Fee":0,"Marketplace_Name":null},
                        "2":{"id":78,"Boarding_ID":25,"Product_ID":4,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-09T23:23:51.000Z","updatedAt":"2015-08-05T21:47:02.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name1","Processor_ID":"processor1","Password":"LamePassword2","Ach_Reject_Fee":0,"Marketplace_Name":null},
                        "3":{"id":85,"Boarding_ID":25,"Product_ID":5,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-19T17:15:41.000Z","updatedAt":"2015-08-05T19:10:52.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name2","Processor_ID":"processor2","Password":"LamePassword3","Ach_Reject_Fee":0,"Marketplace_Name":"Marketplace"}
                 });
                 
                 nmiTransactionsDeferred = $q.defer();
                 nmiTransactionsDeferred.resolve({
                     "id":25,
                     "Date":"2015-04-21",
                     "Merchant_Name":"Demo Account",
                     "Address":"123 Demo Street",
                     "City":"Demoville",
                     "Phone":"555-555-5555",
                     "Merchant_ID":"1",
                     "Bank_ID":"1",
                     "Terminal_ID":"1",
                     "MCC":"8396",
                     "Classification":"0",
                     "Visa":true,
                     "Mastercard":true,
                     "Discover":true,
                     "American_Express":false,
                     "Diners_Club":null,
                     "JCB":null,
                     "Maestro":null,
                     "Max_Ticket_Amount":1500,
                     "Max_Monthly_Amount":100,
                     "Precheck":"nopreauth",
                     "Duplicate_Checking":null,
                     "Allow_Merchant":null,
                     "Duplicate_Threshold":1200,
                     "Account_Description":null,
                     "createdAt":"2015-04-21T20:58:50.000Z",
                     "updatedAt":"2015-08-05T23:32:54.000Z",
                     "Location_Number":"1",
                     "Aquire_Bin":"1",
                     "Store_Number":"1",
                     "Vital_Number":"1",
                     "Agent_Chain":"1",
                     "Required_Name":true,
                     "Required_Company":null,
                     "Required_Address":null,
                     "Required_City":true,
                     "Required_State":true,
                     "Required_Zip":false,
                     "Required_Country":null,
                     "Required_Phone":null,
                     "Required_Email":null,
                     "Reserve_Rate":0,
                     "Reserve_Condition":false,
                     "Cap_Amount":null,
                     "Disbursement":null,
                     "Status":"active",
                     "Welcome_Email":true,
                     "State":"CA",
                     "Zip":"92656"
                 });
                 
                 NationBuilderIntegrationDeferred = $q.defer();
                 NationBuilderIntegrationDeferred.resolve({
                     "response":{
                        "id":10,
                        "Processor_ID":"processor1",
                        "Nation_Builder_Key":"345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25",
                        "URL":"nationtest",
                        "Active":false,
                        "createdAt":"2015-01-01T01:01:01.000Z",
                        "updatedAt":"2015-01-01T01:01:01.000Z"
                    }
                });
                
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions,'get').and.returnValue({$promise: nmiTransactionsDeferred.promise}); //returns a fake promise;
                spyOn(NationBuilderIntegrationService,'get').and.returnValue({$promise: NationBuilderIntegrationDeferred.promise}); //returns a fake promise;
            });

            it('should change scope.noIntegrations variable from true to false', function(){

                var NBarray = {
                       "id":10,
                       "Processor_ID":"processor1",
                       "Nation_Builder_Key":"345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25",
                       "URL":"nationtest",
                       "Active":true,
                       "createdAt":"2015-01-01T01:01:01.000Z",
                       "updatedAt":"2015-01-01T01:01:01.000Z"
                };
                scope.getMerchantProductDropdown();
                scope.$digest();
                expect(scope.noIntegrations).toBe(true);
                scope.refreshArray(NBarray);
                expect(scope.noIntegrations).toBe(false);
            });
        });
        
        describe('refreshArray function III', function(){
            beforeEach(function() {
                var productDeferred,
                    paymentDeferred,
                    nmiTransactionsDeferred,
                    NationBuilderIntegrationDeferred,
                    $promise;
                    
                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                 
                 paymentDeferred = $q.defer();
                 paymentDeferred.resolve({
                        "0":{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":null,"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},
                        "1":{"id":44,"Boarding_ID":25,"Product_ID":2,"Discount_Rate" :0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-05-04T16:40:13.000Z","updatedAt":"2015-08-05T22:02:52.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name0","Processor_ID":"processor0","Password":"LamePassword1","Ach_Reject_Fee":0,"Marketplace_Name":null},
                        "2":{"id":78,"Boarding_ID":25,"Product_ID":4,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-09T23:23:51.000Z","updatedAt":"2015-08-05T21:47:02.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name1","Processor_ID":"processor1","Password":"LamePassword2","Ach_Reject_Fee":0,"Marketplace_Name":null},
                        "3":{"id":85,"Boarding_ID":25,"Product_ID":5,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-19T17:15:41.000Z","updatedAt":"2015-08-05T19:10:52.000Z","Is_Active":true,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"Name2","Processor_ID":"processor2","Password":"LamePassword3","Ach_Reject_Fee":0,"Marketplace_Name":"Marketplace"}
                 });
                 
                 nmiTransactionsDeferred = $q.defer();
                 nmiTransactionsDeferred.resolve({
                     "id":25,
                     "Date":"2015-04-21",
                     "Merchant_Name":"Demo Account",
                     "Address":"123 Demo Street",
                     "City":"Demoville",
                     "Phone":"555-555-5555",
                     "Merchant_ID":"1",
                     "Bank_ID":"1",
                     "Terminal_ID":"1",
                     "MCC":"8396",
                     "Classification":"0",
                     "Visa":true,
                     "Mastercard":true,
                     "Discover":true,
                     "American_Express":false,
                     "Diners_Club":null,
                     "JCB":null,
                     "Maestro":null,
                     "Max_Ticket_Amount":1500,
                     "Max_Monthly_Amount":100,
                     "Precheck":"nopreauth",
                     "Duplicate_Checking":null,
                     "Allow_Merchant":null,
                     "Duplicate_Threshold":1200,
                     "Account_Description":null,
                     "createdAt":"2015-04-21T20:58:50.000Z",
                     "updatedAt":"2015-08-05T23:32:54.000Z",
                     "Location_Number":"1",
                     "Aquire_Bin":"1",
                     "Store_Number":"1",
                     "Vital_Number":"1",
                     "Agent_Chain":"1",
                     "Required_Name":true,
                     "Required_Company":null,
                     "Required_Address":null,
                     "Required_City":true,
                     "Required_State":true,
                     "Required_Zip":false,
                     "Required_Country":null,
                     "Required_Phone":null,
                     "Required_Email":null,
                     "Reserve_Rate":0,
                     "Reserve_Condition":false,
                     "Cap_Amount":null,
                     "Disbursement":null,
                     "Status":"active",
                     "Welcome_Email":true,
                     "State":"CA",
                     "Zip":"92656"
                 });
                 
                 NationBuilderIntegrationDeferred = $q.defer();
                 NationBuilderIntegrationDeferred.resolve({
                     "response":{
                        "id":10,
                        "Processor_ID":"processor1",
                        "Nation_Builder_Key":"345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25",
                        "URL":"nationtest",
                        "Active":false,
                        "createdAt":"2015-01-01T01:01:01.000Z",
                        "updatedAt":"2015-01-01T01:01:01.000Z"
                    }
                });
                
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions,'get').and.returnValue({$promise: nmiTransactionsDeferred.promise}); //returns a fake promise;
                spyOn(NationBuilderIntegrationService,'get').and.returnValue({$promise: NationBuilderIntegrationDeferred.promise}); //returns a fake promise;
            });

            it('should set scope.noIntegrations if the key is not active', function(){

                var NBarray = {
                       "id":10,
                       "Processor_ID":"processor1",
                       "Nation_Builder_Key":"345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25",
                       "URL":"nationtest",
                       "Active":false,
                       "createdAt":"2015-01-01T01:01:01.000Z",
                       "updatedAt":"2015-01-01T01:01:01.000Z"
                };
                scope.getMerchantProductDropdown();
                scope.$digest();
                expect(scope.noIntegrations).toBe(true);
                scope.refreshArray(NBarray);
                expect(scope.noIntegrations).toBe(true);
            });
        });
        
        describe('inactivateNationBuilder', function(){
            var nationParameters = {
                name: 'NationName',
                nationBuilderId: '9999',
                Processor_ID: "processor0"
            };
            
            beforeEach(function(){
				this.element = $('<input id="nationBuilderURL" value="theURL" />');
				this.element.appendTo('body');
                
                var NationBuilderActivationDeferred,
                    $promise;
                
                NationBuilderActivationDeferred = $q.defer();
                NationBuilderActivationDeferred.resolve({
                  "result":true
                 });
                 
                spyOn(NationBuilderActivationService,'update').and.returnValue({$promise: NationBuilderActivationDeferred.promise});
                spyOn(scope,'refreshArray').and.returnValue(true);
                
			});
			
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});
            
            it('should define scope.message', function(){
                expect(scope.message).toBeUndefined();
                scope.inactivateNationBuilder(nationParameters);
                expect(scope.message).toEqual({ title : 'Confirm', message : 'Are you sure you want to remove NationName integration?', accept : 'Yes', deny: 'No' });
            });
               
            it('should define scope.modalInstance function', function(){
                expect(scope.modalInstance).toBeUndefined();
                scope.inactivateNationBuilder(nationParameters);
                expect(scope.modalInstance).toBeDefined();
            });
               
            it('should call the function refreshArray to update the list of integrations if "YES" is pressed', function(){
                var fakeModal = {
                    result: {
                        then: function(confirmCallback, cancelCallback) {
                            //Store the callbacks for later when the user clicks on the Yes or No button of the dialog
                            this.confirmCallBack = confirmCallback;
                            this.cancelCallback = cancelCallback;
                        }
                    },
                    close: function( item ) {
                        //The user clicked Yes on the modal dialog, call the stored confirm callback with the selected item
                        this.result.confirmCallBack( item );
                    },
                    dismiss: function( item ) {
                        //The user clicked cancel on the modal dialog, call the stored cancel callback
                        this.result.cancelCallback( {accept:'No', deny:'Yes'} );
                    }
                };
                
                beforeEach(function(){
                });

                spyOn($modal, 'open').and.returnValue(fakeModal);
                scope.inactivateNationBuilder(nationParameters);
                scope.modalInstance.close( {accept:'Yes', deny:'No'} );
                scope.modalInstance = undefined;
                scope.$digest();
                expect(NationBuilderActivationService.update).toHaveBeenCalledWith( { nationBuilderKeyId : '9999', ProcessorID : 'processor0', URL : 'theURL', Type : 'inactivate' });
                expect(scope.refreshArray).toHaveBeenCalledWith({"result":true});
            });
            
            it('should not update the list of integrations if "No" is pressed', function(){
                var fakeModal = {
                    result: {
                        then: function(confirmCallback, cancelCallback) {
                            //Store the callbacks for later when the user clicks on the Yes or No button of the dialog
                            this.confirmCallBack = confirmCallback;
                            this.cancelCallback = cancelCallback;
                        }
                    },
                    close: function( item ) {
                        //The user clicked Yes on the modal dialog, call the stored confirm callback with the selected item
                        this.result.confirmCallBack( item );
                    },
                    dismiss: function( item ) {
                        //The user clicked cancel on the modal dialog, call the stored cancel callback
                        this.result.cancelCallback(item);
                    }
                };
                
                spyOn($modal, 'open').and.returnValue(fakeModal);
                scope.inactivateNationBuilder(nationParameters);
                scope.modalInstance.close( {accept:'No', deny:'Yes'} );
                scope.modalInstance = undefined;
                scope.$digest();
                expect(NationBuilderActivationService.update).not.toHaveBeenCalledWith( { nationBuilderKeyId : '9999', ProcessorID : 'processor0', URL : 'theURL', Type : 'inactivate' });
                expect(scope.refreshArray).not.toHaveBeenCalledWith({"result":true});
            });
         });
        
        describe('getCode', function(){
            beforeEach(function(){
				this.element = $('<input id="nationBuilderSlug" value="theSLUG" />');
				this.element.appendTo('body');
                
                var NationBuilderInformationDeferred,
                    $promise;
                
                NationBuilderInformationDeferred = $q.defer();
                NationBuilderInformationDeferred.resolve({
                    "response": {
                        Client_ID: "88888",
                        Redirect_URL: "/junkProgram.html"
                        }
                 });
                 
                spyOn(NationBuilderInformationService,'get').and.returnValue({$promise: NationBuilderInformationDeferred.promise});
                spyOn( window, 'open' ).and.callFake( function() {return true;});
			});
			
			// clean up

			afterEach(function(){
				this.element.remove(); 
			});
            
            it('should call NationBuilderInformationService.get', function(){
                scope.getCode();
                scope.$digest();
                expect(NationBuilderInformationService.get).toHaveBeenCalledWith( { slug : 'totalapps' });
            });
            it('should open a new window to nationbuilder.com.', inject( function( $window ) {
                scope.getCode();
                scope.$digest();
                expect($window.open).toHaveBeenCalled();
                expect($window.open).toHaveBeenCalledWith ('https://theSLUG.nationbuilder.com/oauth/authorize?response_type=code&client_id=88888&redirect_uri=http://server:80/junkProgram.html');
            }));
        });

        describe('integrate I', function(){
            beforeEach(function(){
				this.element = $('<input id="nationBuilderSlug" value="theSLUG" /><input id="AuthorizationCode" value="345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25" />');
				this.element.appendTo('body');
                
                var NationBuilderActivationDeferred,
                    NationBuilderInformationDeferred,
                    $promise;
                var postResponse = {"access_token": "205998f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25" };
                
                NationBuilderInformationDeferred = $q.defer();
                NationBuilderInformationDeferred.resolve({
                    "response": {
                        Client_ID: "88888",
                        Redirect_URL: "/junkProgram.html"
                        }
                 });
                 
                NationBuilderActivationDeferred = $q.defer();
                NationBuilderActivationDeferred.resolve({
                  "result":true
                 });
                 
                spyOn(NationBuilderActivationService,'update').and.returnValue({$promise: NationBuilderActivationDeferred.promise});
                spyOn(scope,'refreshArray').and.returnValue(true);
                
                spyOn(NationBuilderInformationService,'get').and.returnValue({$promise: NationBuilderInformationDeferred.promise});
                $httpBackend.when('POST', '/nation_builder_get_oauth').respond(200, postResponse);
 			});

            afterEach(function(){
				this.element.remove(); 
			});

            it('should call NationBuilderInformationService.get', function(){
                scope.integrate();
                //scope.$digest();
                expect(NationBuilderInformationService.get).toHaveBeenCalledWith( { slug : 'totalapps' });
            });
            it('should POST to /nation_builder_get_oath URL', function(){
                scope.productId = {"Processor_ID": 'processor0'};
                scope.integrate();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should call NationBuilderActivationService.update on successful POST to /nation_builder_get_oath URL', function(){
                scope.productId = {"Processor_ID": 'processor0'};
                scope.integrate();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(NationBuilderActivationService.update).toHaveBeenCalledWith({ ProcessorID : 'processor0', Nation_Builder_Key : '205998f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25', URL : 'theSLUG', Type : 'create'});
            });
            it('should call scope.refreshArray on successful NationBuilderActivationService.get call', function(){
                scope.productId = {"Processor_ID": 'processor0'};
                scope.integrate();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.refreshArray).toHaveBeenCalledWith({"result":true});
            });
        });

        describe('integrate II', function(){
            beforeEach(function(){
				this.element = $('<input id="nationBuilderSlug" value="theSLUG" /><input id="AuthorizationCode" value="345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25" />');
				this.element.appendTo('body');
                
                var NationBuilderActivationDeferred,
                    NationBuilderInformationDeferred,
                    $promise;
                var postResponse = {error_description: 'Error!'};
                
                NationBuilderInformationDeferred = $q.defer();
                NationBuilderInformationDeferred.resolve({
                    "response": {
                        Client_ID: "88888",
                        Redirect_URL: "/junkProgram.html"
                        }
                 });
                 
                NationBuilderActivationDeferred = $q.defer();
                NationBuilderActivationDeferred.resolve({
                  "result":true
                 });
                 
                spyOn(NationBuilderActivationService,'update').and.returnValue({$promise: NationBuilderActivationDeferred.promise});
                spyOn(scope,'refreshArray').and.returnValue(true);
                
                spyOn(NationBuilderInformationService,'get').and.returnValue({$promise: NationBuilderInformationDeferred.promise});
                $httpBackend.when('POST', '/nation_builder_get_oauth').respond(500, {message: 'ERROR!'});
 			});
            it('should display an error on unsuccessful POST to /nation_builder_get_oath URL', function(){
                expect(scope.error).toBeUndefined();
                scope.productId = {"Processor_ID": 'processor0'};
                scope.integrate();
                expect(scope.error).toBeDefined();
                scope.$digest();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.error).toBe('ERROR!');
            });
        });

        describe('integrate III', function(){
            beforeEach(function(){
				this.element = $('<input id="nationBuilderSlug" value="theSLUG" /><input id="AuthorizationCode" value="345778f124378ca423879bddd452f98aa98dba24395fff908bfadce23859ee25" />');
				this.element.appendTo('body');
                
                var NationBuilderActivationDeferred,
                    NationBuilderInformationDeferred,
                    $promise;
                var postResponse = {error_description: 'Error!'};
                
                NationBuilderInformationDeferred = $q.defer();
                NationBuilderInformationDeferred.resolve({
                    "response": {
                        Client_ID: "88888",
                        Redirect_URL: "/junkProgram.html"
                        }
                 });
                 
                NationBuilderActivationDeferred = $q.defer();
                NationBuilderActivationDeferred.resolve({
                  "result":true
                 });
                 
                spyOn(NationBuilderActivationService,'update').and.returnValue({$promise: NationBuilderActivationDeferred.promise});
                spyOn(scope,'refreshArray').and.returnValue(true);
                
                spyOn(NationBuilderInformationService,'get').and.returnValue({$promise: NationBuilderInformationDeferred.promise});
                $httpBackend.when('POST', '/nation_builder_get_oauth').respond(200, postResponse);
 			});
            it('should display an error on unsuccessful POST to /nation_builder_get_oath URL', function(){
                expect(scope.error).toBeUndefined();
                scope.productId = {"Processor_ID": 'processor0'};
                scope.integrate();
                expect(scope.error).toBeDefined();
                scope.$digest();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.error).toBe('Error!');
            });
       });
    });

}());

(function() {
    describe('NationBuilderIntegrationController II', function() {
        // Initialize global variables
        var NationBuilderIntegrationController,
            scope,
            $httpBackend,
            $stateParams,
            $location,
            Authentication;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));

        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, Authentication) {
            // Set a new global scope
            scope = $rootScope.$new();
            $stateParams = _$stateParams_;
            $httpBackend = _$httpBackend_;
            $location = _$location_;
            Authentication = {};
            
            // Point global variables to injected services
            spyOn($location, 'path');
            
        // Initialize the Authentication controller
            NationBuilderIntegrationController = $controller('NationBuilderIntegrationController', {
                $scope: scope,
                Authentication: Authentication
            });

        }));

        describe('Logged in', function(){
           it('should redirect if user is not logged in', function() {
                expect(scope.authentication).toBeUndefined();
                expect($location.path).toHaveBeenCalledWith('/');
            });
        });
    });
}());

(function() {
    describe('NationBuilderIntegrationController III', function() {
        // Initialize global variables
        var NationBuilderIntegrationController,
            scope,
            $httpBackend,
            $stateParams,
            $location,
            Authentication,
            ProductService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));

        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, Authentication, _ProductService_) {
            // Set a new global scope
            scope = $rootScope.$new();
            $stateParams = _$stateParams_;
            $httpBackend = _$httpBackend_;
            $location = _$location_;
            Authentication = {
                user: {
                    Roles: "imposter"
                }
            };
            ProductService = _ProductService_;
                        
        // Initialize the Authentication controller
        //this.init = function() {
            NationBuilderIntegrationController = $controller('NationBuilderIntegrationController', {
                $scope: scope,
                Authentication: Authentication,
                ProductService: ProductService
            });
        //}

        }));

        describe('Logged in', function(){
            it ('should not exexute if Authentication.user.Roles !== "user"', function() {
                spyOn(ProductService, 'get').and.returnValue('FakeReturn'); //returns a fake promise
                scope.getMerchantProductDropdown();
                expect(ProductService.get).not.toHaveBeenCalledWith();
            });
        });
    });
}());