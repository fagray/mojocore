/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('PasswordController', function() {
        // Initialize global variables
        var PasswordController,
            scope,
            $httpBackend,
			$stateParams,
			$location,
            Authentication;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, Authentication) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            $stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

            // Point global variables to injected services
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

			// Initialize the controller
            PasswordController = $controller('PasswordController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
            it('should have function scope.setTitle defined', function() {
                expect(scope.setTitle).toBeDefined();
            });        
            it('should have function scope.verify defined', function() {
                expect(scope.verify).toBeDefined();
            });        
            it('should have function scope.askForPasswordReset defined', function() {
                expect(scope.askForPasswordReset).toBeDefined();
            });        
            it('should have function scope.resetUserPassword defined', function() {
                expect(scope.resetUserPassword).toBeDefined();
            });        
        });

        describe('Interface', function() {
            it('should recognize setTitle as a function', function() {
                expect(angular.isFunction(scope.setTitle)).toBe(true);
            });
            it('should recognize verify as a function', function() {
                expect(angular.isFunction(scope.verify)).toBe(true);
            });
            it('should recognize askForPasswordReset as a function', function() {
                expect(angular.isFunction(scope.askForPasswordReset)).toBe(true);
            });
            it('should recognize resetUserPassword as a function', function() {
                expect(angular.isFunction(scope.resetUserPassword)).toBe(true);
            });
        });
        
        describe('Not Logged in', function(){
            it('should not redirect if user is not logged in', function() {
                spyOn($location, 'path');
                expect(scope.authentication.user).toEqual({Roles: 'user', Boarding_ID: 25});
		    	expect(scope.authentication.user).not.toBe(false);
                expect($location.path).not.toHaveBeenCalledWith('/');
	    	});
        });
        
   		describe('SetTitle function testing', function() {
            it('should call $broadcast on $scope.$parent.$parent',function() {
                var value = 'Foxtrot Oscar Oscar';
                var parent = scope.$parent.$parent;
                spyOn(parent, '$broadcast').and.callThrough();
                scope.setTitle(value);    		
                expect(parent.$broadcast).toHaveBeenCalledWith('setTitle', value);
                expect(parent.$broadcast.calls.mostRecent().args[0]).toEqual('setTitle');
                expect(parent.$broadcast.calls.mostRecent().args[1]).toEqual(value);
			});
		});

        describe('Initialize scope.authentication', function() {
            it('should have initialize scope.authentication to the Authentication object', function() {
                expect(scope.authentication).toEqual({user : { Roles: 'user', Boarding_ID: 25 }});
            });        
            
		});

        describe('$scope.verify should validate token for password reset ', function() {
            it('shoud define $scope.success variable',function() {
                expect(scope.success).toBeUndefined();
                scope.verify();
                expect(scope.success).toBeDefined();
                expect(scope.success).toBe(null);
            });
            it('shoud define $scope.error variable',function() {
                expect(scope.error).toBeUndefined();
                scope.verify();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe(null);
            });
            it('shoud define $scope.passwordDetails variable',function() {
                expect(scope.passwordDetails).toBeUndefined();
                scope.verify();
                expect(scope.passwordDetails).toBeDefined();
                expect(scope.passwordDetails).toBe(null);
            });
            it('shoud post to /auth/validateResetToken',function() {
                $stateParams.token = '1234abcd5678efgh90'; 
                scope.passwordDetails = 'test';
    			$httpBackend.when('POST', '/auth/validateResetToken/1234abcd5678efgh90').respond(200, 'Response');
                scope.verify();
        		$httpBackend.flush();

                $httpBackend.verifyNoOutstandingRequest();
            });
            it('shoud redirect to /password/reset/expired if status returns 203',function() {
                spyOn($location, 'path');
                $stateParams.token = '1234abcd5678efgh90'; 
                scope.passwordDetails = 'test';
    			$httpBackend.when('POST', '/auth/validateResetToken/1234abcd5678efgh90').respond(203, 'Response');
                scope.verify();
        		$httpBackend.flush();
                
                expect($location.path).toHaveBeenCalledWith('/password/reset/expired');
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('shoud define $scope.error variable as blank on success',function() {
                expect(scope.error).toBeUndefined();
                $stateParams.token = '1234abcd5678efgh90'; 
                scope.passwordDetails = 'test';
    			$httpBackend.when('POST', '/auth/validateResetToken/1234abcd5678efgh90').respond(203, 'Response');
                scope.verify();
        		$httpBackend.flush();

                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe('');
            });
            it('shoud return $scope.error variable if an error',function() {
                expect(scope.error).toBeUndefined();
                $stateParams.token = '1234abcd5678efgh90'; 
                scope.passwordDetails = 'test';
    			$httpBackend.when('POST', '/auth/validateResetToken/1234abcd5678efgh90').respond(500, {message: 'Error!'});
                scope.verify();
        		$httpBackend.flush();

                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.error).toBe('Error!');
            });
		});

        describe('$scope.askForPasswordReset should return return data if successful', function() {
           it('shoud define $scope.success variable',function() {
                expect(scope.success).toBeUndefined();
                scope.askForPasswordReset();
                expect(scope.success).toBeDefined();
                expect(scope.success).toBe(null);
            });
            it('shoud define $scope.error variable',function() {
                expect(scope.error).toBeUndefined();
                scope.askForPasswordReset();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe(null);
            });
            it('shoud post to /auth/forgot',function() {
                $stateParams.token = '1234abcd5678efgh90'; 
                scope.passwordDetails = 'test';
    			$httpBackend.when('POST', '/auth/forgot').respond(200, 'Response');
                scope.askForPasswordReset();
        		$httpBackend.flush();

                $httpBackend.verifyNoOutstandingRequest();
            });
            it('shoud initialize variables $scope.credentials and $scope.success on successful http call',function() {
                expect(scope.credentials).toBeUndefined();
                expect(scope.success).toBeUndefined();
                $stateParams.token = '1234abcd5678efgh90'; 
                scope.passwordDetails = 'test';
    			$httpBackend.when('POST', '/auth/forgot').respond(200, {message: 'Response'});
                scope.askForPasswordReset();
        		$httpBackend.flush();

                expect(scope.credentials).toBe(null);
                expect(scope.success).toBe('Response');
                
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('shoud initialize variables $scope.credentials and $scope.success on unsuccessful http call',function() {
                expect(scope.credentials).toBeUndefined();
                $stateParams.token = '1234abcd5678efgh90'; 
                scope.passwordDetails = 'test';
    			$httpBackend.when('POST', '/auth/forgot').respond(500, {message: 'Error!'});
                scope.askForPasswordReset();
        		$httpBackend.flush();

                expect(scope.credentials).toBe(null);
                expect(scope.error).toBe('Error!');
                
                $httpBackend.verifyNoOutstandingRequest();
            });
 		});

        describe('$scope.resetUserPassword should return return data if successful', function() {
           it('shoud define $scope.success variable',function() {
                scope.passwordDetails = {newPassword: 'foobar', verifyPassword: 'foobar'};
                expect(scope.success).toBeUndefined();
                scope.resetUserPassword ();
                expect(scope.success).toBeDefined();
                expect(scope.success).toBe(null);
            });
            it('shoud define $scope.error variable',function() {
                scope.passwordDetails = {newPassword: 'foobar', verifyPassword: 'foobar'};
                expect(scope.error).toBeUndefined();
                scope.resetUserPassword ();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe(null);
            });
            it('shoud generate and error if newPassword !== verifyPassword',function() {
                scope.passwordDetails = {newPassword: 'foo', verifyPassword: 'bar'};
                expect(scope.error).toBeUndefined();
                scope.resetUserPassword ();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe('Passwords do not match');
            });
            it('shoud post to /auth/reset/',function() {
                $stateParams.token = '1234abcd5678efgh90'; 
                scope.passwordDetails = {newPassword: 'foobar', verifyPassword: 'foobar'};
                $httpBackend.when('POST', '/auth/reset/1234abcd5678efgh90').respond(200, 'Response');
                scope.resetUserPassword();
        		$httpBackend.flush();

                $httpBackend.verifyNoOutstandingRequest();
            });
            it('shoud reset $scope.passwordDetails on successful HTTP call',function() {
                $stateParams.token = '1234abcd5678efgh90'; 
                scope.passwordDetails = {newPassword: 'foobar', verifyPassword: 'foobar'};
                $httpBackend.when('POST', '/auth/reset/1234abcd5678efgh90').respond(200, 'Response');
                scope.resetUserPassword();
        		$httpBackend.flush();
                expect(scope.passwordDetails).toBe(null);
                
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('shoud reset Authentication.user on successful HTTP call',function() {
                $stateParams.token = '1234abcd5678efgh90'; 
                scope.passwordDetails = {newPassword: 'foobar', verifyPassword: 'foobar'};
                $httpBackend.when('POST', '/auth/reset/1234abcd5678efgh90').respond(200, 'Response');
                scope.resetUserPassword();
        		$httpBackend.flush();               
                expect(scope.authentication.user).toBe('Response');
                
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('shoud generate and error on unsuccessful HTTP call',function() {
                $stateParams.token = '1234abcd5678efgh90'; 
                scope.passwordDetails = {newPassword: 'foobar', verifyPassword: 'foobar'};
                $httpBackend.when('POST', '/auth/reset/1234abcd5678efgh90').respond(500, {message: 'Error!'});
                scope.resetUserPassword();
        		$httpBackend.flush();               
                expect(scope.error).toBe('Error!');
                
                $httpBackend.verifyNoOutstandingRequest();
            });
        });
        
    });
}());

(function() {
	describe('PasswordController II', function() {
		// Initialize global variables
		var PasswordController,
			scope,
			$httpBackend,
			$stateParams,
			$location,
            Authentication;

		// Load the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, Authentication) {
			// Set a new global scope
            scope = $rootScope.$new();
            $stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;
            
			// Point global variables to injected services
			spyOn($location, 'path');
            
		// Initialize the Authentication controller
        //this.init = function() {
            PasswordController = $controller('PasswordController', {
                $scope: scope,
                Authentication: Authentication
            });
        //}

		}));

        describe('Logged in', function(){
           it('should not redirect if user is not logged in', function() {
		    	expect(scope.authentication.user).not.toEqual({Roles: 'user', Boarding_ID: 25});
		    	expect(scope.authentication.user).toBeUndefined();
                expect($location.path).not.toHaveBeenCalledWith('/');
	    	});
        });
    });
}());