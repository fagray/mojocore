/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
	// Authentication controller Spec
	describe('AuthenticationController', function() {
		// Initialize global variables
		var AuthenticationController,
			scope,
			$httpBackend,
			$stateParams,
			$location;

		// Load the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new().$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Authentication controller
			AuthenticationController = $controller('AuthenticationController', {
				$scope: scope
			});
		}));

		it('should verify true is true', function() {
			expect(true).toBe(true);
		});

		describe('defined parameters', function (){
			it('should have scope defined', function() {
				expect(scope).toBeDefined();
			});		

			it('should have variable scope.authentication defined', function() {
				expect(scope.authentication).toBeDefined();
			});
			
			it('should have function scope.signup defined', function() {
				expect(scope.signup).toBeDefined();
			});		
			it('should have function scope.signin defined', function() {
				expect(scope.signin).toBeDefined();
			});		
			it('should have function scope.setTitle defined', function() {
				expect(scope.setTitle).toBeDefined();
			});		
		});

		describe('Interface', function() {
			it('should recognize setTitle as a function', function() {
				expect(angular.isFunction(scope.setTitle)).toBe(true);
			});
			it('should recognize signup as a function', function() {
				expect(angular.isFunction(scope.signup)).toBe(true);
			});
			it('should recognize signin as a function', function() {
				expect(angular.isFunction(scope.signin)).toBe(true);
			});
		});


		it('should expose the authentication service', function() {
			expect(scope.authentication).toBeTruthy();
		});

        describe('Not Logged in', function(){
            beforeEach(function() {
                spyOn($location, 'path');
            });
            it('should not redirect if user is not logged in', function() {
                expect($location.path).not.toHaveBeenCalled();
	    	});
        });

		it('$scope.signin() should login with a correct user and password', function() {
			// Test expected GET request
			$httpBackend.when('POST', '/auth/signin').respond(200, 'Fred');

			scope.signin();
			$httpBackend.flush();

			// Test scope value
			expect(scope.authentication.user).toEqual('Fred');
			expect($location.url()).toEqual('/');
		});

		it('$scope.signin() should fail to log in with nothing', function() {
			// Test expected POST request
			$httpBackend.expectPOST('/auth/signin').respond(400, {
				'message': 'Missing credentials'
			});

			scope.signin();
			$httpBackend.flush();

			// Test scope value
			expect(scope.error).toEqual('Missing credentials');
		});

		it('$scope.signin() should fail to log in with wrong credentials', function() {
			// Foo/Bar combo assumed to not exist
			scope.authentication.user = 'Foo';
			scope.credentials = 'Bar';

			// Test expected POST request
			$httpBackend.expectPOST('/auth/signin').respond(400, {
				'message': 'Unknown user'
			});

			scope.signin();
			$httpBackend.flush();

			// Test scope value
			expect(scope.error).toEqual('Unknown user');
		});

		it('$scope.signup() should register with correct data', function() {
			// Test expected GET request
			scope.authentication.user = 'Fred';
			$httpBackend.when('POST', '/auth/signup').respond(200, 'Fred');

			scope.signup();
			$httpBackend.flush();

			// test scope value
			expect(scope.authentication.user).toBe('Fred');
			expect(scope.error).toEqual(undefined);
			expect($location.url()).toBe('/');
		});

		it('$scope.signup() should fail to register with duplicate Username', function() {
			// Test expected POST request
			$httpBackend.when('POST', '/auth/signup').respond(400, {
				'message': 'Username already exists'
			});

			scope.signup();
			$httpBackend.flush();

			// Test scope value
			expect(scope.error).toBe('Username already exists');
		});

   		// not sure how to test broadcast on $scope.$parent.$parent
		describe('SetTitle function testing', function() {
            it('should call $broadcast on $scope.$parent.$parent',function() {
                var value = 'Foxtrot Oscar Oscar';
                var parent = scope.$parent.$parent;
                spyOn(parent, '$broadcast').and.callThrough();
                scope.setTitle(value);    		
                expect(parent.$broadcast).toHaveBeenCalledWith('setTitle', value);
                expect(parent.$broadcast.calls.mostRecent().args[0]).toEqual('setTitle');
                expect(parent.$broadcast.calls.mostRecent().args[1]).toEqual(value);
			});
		});

	});
}());

(function() {
	describe('AuthenticationController II', function() {
		// Initialize global variables
		var AuthenticationController,
			scope,
			$httpBackend,
			$stateParams,
			$location,
            Authentication;

		// Load the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, Authentication) {
			// Set a new global scope
            scope = $rootScope.$new();
            $stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;
            Authentication = {user: 'Fred'};
            
			// Point global variables to injected services
			spyOn($location, 'path');
            
		// Initialize the Authentication controller
        //this.init = function() {
            AuthenticationController = $controller('AuthenticationController', {
                $scope: scope,
                Authentication: Authentication
            });
        //}

		}));

        describe('Logged in', function(){
           it('should redirect if user is logged in', function() {
		    	expect(scope.authentication.user).toEqual('Fred');
		    	expect(scope.authentication.user).not.toBe(false);
                expect($location.path).toHaveBeenCalledWith('/');
	    	});
        });
        

    });
}());