'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('UsersUpdate', ['$resource',
	function($resource) {
		return $resource('usersupdate/:boardingId', {
	        boardingId: '@Boarding_ID'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);