'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('AdminUpdate', ['$resource',
	function($resource) {
		return $resource('adminsupdate/:id', {
	        id: '@id'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);