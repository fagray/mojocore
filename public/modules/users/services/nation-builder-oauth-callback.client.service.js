'use strict';

angular.module('users').factory('NationBuilderOAuthService', ['$resource',
	function($resource) {
		return $resource('nation_builder_get_oauth', {},  
			{
			update: {
				method: 'PUT'
			}
		});
	}
]);
