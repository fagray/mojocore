'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('NMIService', ['$resource',
	function($resource) {
		return $resource('boardmerchant/:nmiboardingId', {
	        nmiboardingId: '@boarding_Id'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
