'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('NationBuilderIntegrationService', ['$resource',
    function($resource) {
        return $resource('nationbuilderintegration/:ProcessorID', {
            ProcessorId: '@ProcessorId'
        },  {
            update: {
                method: 'PUT'
            }
        });
    }
]);
angular.module('users').factory('NationBuilderActivationService', ['$resource',
    function($resource) {
        return $resource('nationbuilderactivation', {
            NationBuilderKeyId: '@NationBuilderKeyId',
            ProcessorId: '@ProcessorID',
            URL: '@URL',
            Type: '@Type'
        },  {
            update: {
                method: 'PUT'
            }
        });
    }
]);

angular.module('users').factory('NationBuilderInformationService', ['$resource',
    function($resource) {
        return $resource('nationbuilderinfo/:slug', {
            slug: '@slug',
        },  {
            update: {
                method: 'PUT'
            }
        });
    }
]);

