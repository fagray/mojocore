'use strict';

angular.module('users').controller('NationBuilderIntegrationController', ['$scope', '$http', '$window', '$location', 'Users', 'Authentication','NationBuilderIntegrationService','NationBuilderActivationService','PaymentService','Nmitransactions','ProductService','NationBuilderInformationService','NationBuilderOAuthService','$modal',
    function($scope, $http, $window, $location, Users, Authentication,NationBuilderIntegrationService,NationBuilderActivationService,PaymentService,Nmitransactions,ProductService,NationBuilderInformationService,NationBuilderOAuthService,$modal) {
        
        $scope.user = Authentication.user;

        // If user is not signed in then redirect back home
        if (!$scope.user) $location.path('/');
        
        $scope.setTitle=function(viewTitle){
            $scope.$parent.$parent.$broadcast('setTitle',viewTitle);
        };
        
         $(document).ready(function() {
            $('.dropdown-menu').find('input').click(function (e) {
                e.stopPropagation();
            });
    
            $('.nationBuilderButton').find('input').click(function (e) {
                e.stopPropagation();
            });
         });
/*
        Array.prototype.remove = function(from, to) {
            var rest = this.slice((to || from) + 1 || this.length);
            this.length = from < 0 ? this.length + from : from;
            return this.push.apply(this, rest);
        };

        Object.size = function(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };
*/
        var arrayMerchantsProducts = [],
            counterProcessors = 0,
            processorsReserves = [];
        $scope.sampleProductCategories = [];

        $scope.initialize = function() {          
            $scope.getMerchantProductDropdown();
        };

        $scope.setProductId = function(productid){
            $('#dropdownMenu2').parent().toggleClass('open');
            $scope.queryproductid = '';
            $scope.productId = productid;
        };

        $scope.getMerchantProductDropdown = function(){
            if (Authentication.user.Roles === 'user'){
                var boarding = Authentication.user.Boarding_ID;
                ProductService.get({
                    productId:'all'
                }).$promise.then(function(allProducts){
                        PaymentService.get({paymentId:boarding,productId:'all'}).$promise.then(function(payment){
                            delete payment.$promise;
                            delete payment.$resolved;
                            Nmitransactions.get({type: 'getMerchantInfo',boardingId: boarding}).$promise.then(
                                function(boardingPlan){
                                    var tempProduct_ID = 0,
                                        products = [],
                                        merchant = {};
                                    for(var j = 0; j < Object.keys(payment).length; j++) {
                                        var productIndex = parseInt(payment[j].Product_ID) - 1;
                                        if (Object.keys(products).length === 0) {
                                            // inserting All products
                                        //    products.push({
                                        //        'name': 'All Products',
                                        //        'processor': '',
                                        //        'boarding_id': boarding,
                                        //        'Product_ID': 'all'
                                        //    });
                                        }
                                        if((payment[j].Product_ID > 1)){
                                            
                                            products.push({
                                                'Name': allProducts[productIndex].Description,
                                                'Boarding_ID': boarding,
                                                'Product_ID': payment[j].Product_ID,
                                                'Processor_ID': payment[j].Processor_ID
                                            });
                                        }
                                    }
                                    arrayMerchantsProducts.push({'id':boarding, 'Name':boardingPlan.Merchant_Name , 'value': boarding, 'products':products});
                                    $scope.setMerchant(arrayMerchantsProducts);
                                },
                                function(err){
                                    console.log(err);
                                }
                            );
                        });
                });
            }
        };

        $scope.setMerchant = function(arrayMerchantsProducts) {
            $scope.noIntegrations = true;
            angular.element(document).ready(function () {
                $scope.sampleProductCategories = arrayMerchantsProducts;
                var processorsArray = [];

                $scope.Product_ID = arrayMerchantsProducts[0].products[0]; //arrayMerchantsProducts[0].products[0];
                /**
                 * Looking for reserves in case of get account balance report, in mike's table
                 */
                for (var j = 0; j < Object.keys(arrayMerchantsProducts[0].products).length; j++) {
                    if (arrayMerchantsProducts[0].products[j].Processor_ID !== '' && arrayMerchantsProducts[0].products[j].Processor_ID !== undefined ) {
                        processorsArray.push(arrayMerchantsProducts[0].products[j].Processor_ID);
                        // check if nation is already built and create a button.

                        NationBuilderIntegrationService.get({ProcessorID: arrayMerchantsProducts[0].products[j].Processor_ID}).$promise.then(function(nationBuilder){
                            if (nationBuilder.response !== undefined && nationBuilder.response.Processor_ID !== undefined) {
                                var displayName="unknown";
                                for (var key in arrayMerchantsProducts[0].products) {
                                    if (arrayMerchantsProducts[0].products[key].Processor_ID === nationBuilder.response.Processor_ID){
                                        displayName = arrayMerchantsProducts[0].products[key].Name;
                                        arrayMerchantsProducts[0].products[key].nationBuilderId = nationBuilder.response.id;
                                        arrayMerchantsProducts[0].products[key].nationURL = nationBuilder.response.URL;
                                        arrayMerchantsProducts[0].products[key].nationActive = nationBuilder.response.Active;
                                        if (arrayMerchantsProducts[0].products[key].nationActive) $scope.noIntegrations = false;
                                    }
                                }
    
                            }
                        });
                    }
                }

            });
        };

        $scope.refreshArray = function(integratedObject) {
//console.log(integratedObject);
            $scope.noIntegrations = true;
            for (var key in $scope.sampleProductCategories[0].products) {
                if (arrayMerchantsProducts[0].products[key].Processor_ID === integratedObject.Processor_ID){
                    arrayMerchantsProducts[0].products[key].nationBuilderId = integratedObject.id;
                    arrayMerchantsProducts[0].products[key].nationURL = integratedObject.URL;
                    arrayMerchantsProducts[0].products[key].nationActive = integratedObject.Active;
                    if (arrayMerchantsProducts[0].products[key].nationActive) $scope.noIntegrations = false;
                }
            }
};

/*
        $scope.inactivatePressed = function(nation) {
        };
*/

        $scope.inactivateNationBuilder = function(nation) {
//console.log("inactivateNationBuilder:", nation);
            $scope.message = {
                title: "Confirm",
                message: "Are you sure you want to remove " + nation.name + " integration?",
                accept: "Yes",
                deny: "No"
            };
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/creditcard/views/modalconf_nation_builder.client.view.html',
                controller: 'ModalConfNBCtrl',
                size: 'md',
                resolve: {
                    message: function () {
                        return $scope.message;
                    }
                }
            });
            $scope.modalInstance.result.then(function (selectedItem) {
                if (selectedItem.accept === 'Yes' && selectedItem.deny === "No"){
                    NationBuilderActivationService.update({
                        nationBuilderKeyId: nation.nationBuilderId,
                        ProcessorID: nation.Processor_ID,
                        URL: $('#nationBuilderURL').val(),
                        Type: 'inactivate'
                    }).$promise.then(function(changedNation) {
                        $scope.refreshArray(changedNation);
                    });
                }
            });
        };



        $scope.getCode = function(){
            var ourslug="totalapps";
//console.log("getting code: ", $scope.productId.Processor_ID, $scope.productId.nationBuilderId, $('#nationBuilderSlug').val());
//            NationBuilderActivationService.update({
//                NationBuilderKeyId: $scope.productId.nationBuilderId,
//                ProcessorID: $scope.productId.Processor_ID,
//                URL: $('#nationBuilderSlug').val(),
//                Type: 'create'
//            }).$promise.then(function(changedNation) {
//                $scope.refreshArray(changedNation);
//            });
            var protocol=$location.protocol();
            var host=$location.host();
            var port=$location.port();
            var returnURL=protocol +"://" + host + ":" + port;
            var theirSlug = $('#nationBuilderSlug').val();

            NationBuilderInformationService.get({
                slug: ourslug
            }).$promise.then(function(nb_info) {
                var nationBuilderURL = "https://" + theirSlug + ".nationbuilder.com/oauth/authorize?response_type=code&client_id=" + nb_info.response.Client_ID + "&redirect_uri=" + returnURL + nb_info.response.Redirect_URL;
//console.log(nationBuilderURL);
                $window.open(nationBuilderURL);
            });
        };


        $scope.integrate = function() {
//console.log('integrate:');
            $scope.error = '';
            var ourslug="totalapps";
            var protocol=$location.protocol();
            var host=$location.host();
            var port=$location.port();
            var returnURL=protocol +"://" + host + ":" + port;
            var theirSlug = $('#nationBuilderSlug').val();
            var theirCode = $('#AuthorizationCode').val();

            NationBuilderInformationService.get({
                slug: ourslug
            }).$promise.then(function(nb_info) {
                var nationBuilderURL = theirSlug + ".nationbuilder.com";
                var NationOauthPath = "/oauth/token";
//console.log(nationBuilderURL);
                var redirect_uri = returnURL + nb_info.response.Redirect_URL;
                //redirect_uri="https://devcore.mojopay.com:3000/nation_builder_code_callback";
//console.log(redirect_uri);
                var postdata = {
                    url: nationBuilderURL, 
                    path: NationOauthPath,
                    'client_id': nb_info.response.Client_ID, 
                    'redirect_uri': redirect_uri,
                    'grant_type': 'authorization_code', 
                    'client_secret': nb_info.response.Client_Secret, 
                    'code': theirCode
                };
//console.log(postdata);
                $http.post('/nation_builder_get_oauth', postdata).success(function(response, status) {
                    if(response.access_token !== undefined) {
                       NationBuilderActivationService.update({
                            ProcessorID: $scope.productId.Processor_ID,
                            Nation_Builder_Key: response.access_token,
                            URL: $('#nationBuilderSlug').val(),
                            Type: 'create'
                        }).$promise.then(function(changedNation) {
                            $scope.refreshArray(changedNation);
                            alert("authorization successful");
                        });
                    }else{
                        $scope.error = response.error_description;
                        alert(response.error_description);
                    }

                }).error(function(response) {
                    $scope.error = response.message;
                });
            });
        };
}]);
