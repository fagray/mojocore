'use strict';

angular.module('users').controller('NationBuilderOAuthController', ['$scope', '$http', '$location', 'Users', 'Authentication','NationBuilderOAuthService',
    function($scope, $http, $location, Users, Authentication,NationBuilderOAuthService) {
        $scope.user = Authentication.user;

        // If user is not signed in then redirect back home
        if (!$scope.user) $location.path('/');
        
        $scope.setTitle=function(viewTitle){
            $scope.$parent.$parent.$broadcast('setTitle',viewTitle);
        };

         $(document).ready(function() {
            $('.dropdown-menu').find('input').click(function (e) {
                e.stopPropagation();
            });
    
            $('.nationBuilderButton').find('input').click(function (e) {
                e.stopPropagation();
            });
        });
/*
       $('.dropdown-menu').find('input').click(function (e) {
            e.stopPropagation();
        });

        $('.nationBuilderButton').find('input').click(function (e) {
            e.stopPropagation();
        });
*/
/*
        Array.prototype.remove = function(from, to) {
            var rest = this.slice((to || from) + 1 || this.length);
            this.length = from < 0 ? this.length + from : from;
            return this.push.apply(this, rest);
        };

        Object.size = function(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };
*/
        var arrayMerchantsProducts = [],
            counterProcessors = 0,
            processorsReserves = [];
        $scope.sampleProductCategories = [];

        $scope.initialize = function() {
            $scope.code = $location.search().code;
        };

}]);
