'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', '$modal', 'Users', 'Authentication', 
    function($scope, $http, $location, $modal, Users, Authentication) {
        $scope.user = Authentication.user;
        
        $scope.modalOpen = function (keyID) {
            $scope.modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: 'ModalProfileUpdateController',
                resolve: {
                    items: function () {
                        return keyID;
                    }
                }
            });

            $scope.modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };

        // If user is not signed in then redirect back home
        if (!$scope.user) $location.path('/');
        
        $scope.setTitle=function(viewTitle){
            $scope.$parent.$parent.$broadcast('setTitle',viewTitle);
        };
        
        // Check if there are additional accounts 
        $scope.hasConnectedAdditionalSocialAccounts = function(provider) {
            for (var i in $scope.user.additionalProvidersData) {
                return true;
            }

            return false;
        };

        // Check if provider is already in use with current user
        $scope.isConnectedSocialAccount = function(provider) {
            return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
        };

        // Remove a user social account
        $scope.removeUserSocialAccount = function(provider) {
            $scope.success = $scope.error = null;

            $http.delete('/users/accounts', {
                params: {
                    provider: provider
                }
            }).success(function(response) {
                // If successful show success message and clear form
                $scope.success = true;
                $scope.user = Authentication.user = response;
            }).error(function(response) {
                $scope.error = response.message;
            });
        };

        // Update a user profile
        $scope.updateUserProfile = function(isValid) {    
                        
            $scope.modalOpen();
                    
            if (isValid){                
                $scope.success = $scope.error = null;
                var user = new Users($scope.user);        
                user.Last_Changed_By = Authentication.user.Username;
                $scope.$parent.$parent.$broadcast('updateSession',user);    
    
                user.$update(function(response) {    
                    $scope.success = true;
                    Authentication.user = response;                    
                }, function(response) {
                    $scope.error = response.data.message;
                });
            } else {
                $scope.submitted = true;
            }
        };

        // Change user password
        $scope.changeUserPassword = function() {
            $scope.success = $scope.error = null;
            $scope.passwordDetails.Last_Changed_By = Authentication.user.Username;

            $http.post('/users/password', $scope.passwordDetails).success(function(response) {
                // If successful show success message and clear form
                $scope.success = true;
                $scope.passwordDetails = null;
                $location.path('/password/reset/success');
            }).error(function(response) {
                $scope.error = response.message;
            });
        };
    }
]);

angular.module('users')
   .controller('ModalProfileUpdateController', function ($scope, $modalInstance, items, $modal) {
     
        $scope.ok = function () {              
            $modalInstance.close();
        };

        $scope.cancel = function () {            
            $modalInstance.dismiss('cancel');
        };
        
});