'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication',
	function($scope, $stateParams, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');
        
		$scope.setTitle=function(viewTitle){
			$scope.$parent.$parent.$broadcast('setTitle',viewTitle);
		};
        
		$scope.verify = function() {
			$scope.success = $scope.error = null;
//			console.log($stateParams.token);
			$scope.passwordDetails = null;
			$http.post('/auth/validateResetToken/' + $stateParams.token, $scope.passwordDetails).success(function(response, status) {
				$scope.error = '';
				if (status===203) $location.path('/password/reset/expired');
			}).error(function(response) {
				$scope.error = response.message;
			});
			
		};

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				$scope.success = response.message;

			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				$scope.error = response.message;
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

//			console.log($stateParams.token);
//			console.log($scope.passwordDetails);

			if ($scope.passwordDetails.newPassword!==$scope.passwordDetails.verifyPassword){
				$scope.error = 'Passwords do not match';
				return false;
			}

			$http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
				Authentication.user = response;

				// And redirect to the index page
				$location.path('/password/reset/success');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);