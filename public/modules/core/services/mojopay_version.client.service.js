'use strict';
//Articles service used for articles REST endpoint
angular.module('core').factory('VersionService', ['$resource',
 function($resource) {
	    return $resource('getversion', {
	    }, {
	        update: {
	            method: 'PUT'
	        }
	    });
}]);