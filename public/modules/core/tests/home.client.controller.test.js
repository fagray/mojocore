'use strict';

(function() {
	describe('HomeController', function() {
		//Initialize global variables
		var scope,
			HomeController;

		// Load the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		beforeEach(inject(function($controller, $rootScope) {
			scope = $rootScope.$new().$new();

			HomeController = $controller('HomeController', {
				$scope: scope
			});
		}));

		it('should verify true is true', function() {
			expect(true).toBe(true);
		});

		describe('defined parameters', function (){
			it('should have scope defined', function() {
				expect(scope).toBeDefined();
			});		

			it('should have variable scope.authentication defined', function() {
				expect(scope.authentication).toBeDefined();
			});
			
			it('should have function scope.setTitle defined', function() {
				expect(scope.setTitle).toBeDefined();
			});		
		});
		
		describe('Interface', function() {
			it('should recognize setTitle as a function', function() {
				expect(angular.isFunction(scope.setTitle)).toBe(true);
			});
		});

		it('should expose the authentication service', function() {
			expect(scope.authentication).toBeTruthy();
		});

		describe('SetTitle function testing', function() {
            it('should call $broadcast on $scope.$parent.$parent',function() {
                var value = 'Foxtrot Oscar Oscar';
                var parent = scope.$parent.$parent;
                spyOn(parent, '$broadcast').and.callThrough();
                scope.setTitle(value);    		
                expect(parent.$broadcast).toHaveBeenCalledWith('setTitle', value);
                expect(parent.$broadcast.calls.mostRecent().args[0]).toEqual('setTitle');
                expect(parent.$broadcast.calls.mostRecent().args[1]).toEqual(value);
			});
		});
	});
})();