'use strict';

(function() {
	describe('HeaderController', function() {
		//Initialize global variables
		var $q,
            HeaderController,
            scope,
			$location,
            Authentication,
            VersionService;

		// Load the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		beforeEach(inject(function($controller, $rootScope, _$location_, _$q_, Authentication, _VersionService_) {
			scope = $rootScope.$new();
			$location = _$location_;
            
            // Point global variables to injected services
            $q = _$q_;
            VersionService = _VersionService_;
			Authentication = {
                    user: {Display_Name: ''}
                };
            
            HeaderController = $controller('HeaderController', {
				$scope: scope,
                Authentication: Authentication
			});
		}));
		
		it('should verify true is true', function() {
			expect(true).toBe(true);
		});

		describe('defined parameters', function (){
			it('should have scope defined', function() {
				expect(scope).toBeDefined();
			});		

			it('should have variable scope.authentication defined', function() {
				expect(scope.authentication).toBeDefined();
			});
			
			it('should have variable scope.isCollapsed defined', function() {
				expect(scope.isCollapsed).toBeDefined();
			});		
			
			it('should have variable scope.menu defined', function() {
				expect(scope.menu).toBeDefined();
			});		
			
			it('should have function scope.verifyMenu defined', function() {
				expect(scope.verifyMenu).toBeDefined();
			});		
			
			it('should have function scope.toggleCollapsibleMenu defined', function() {
				expect(scope.toggleCollapsibleMenu).toBeDefined();
			});		
			
			it('should have function scope.outSsl defined', function() {
				expect(scope.outSsl).toBeDefined();
			});		
			it('should have function scope.getVersion defined', function() {
				expect(scope.getVersion).toBeDefined();
			});		
		});
		
		it('should expose the authentication service', function() {
			expect(scope.authentication).toBeTruthy();
		});
		
		describe('verifyMenu function',function(){
			beforeEach(function(){
				this.element = $('<div id="theMenu"></div><div id="theTitle"></div><div class="navbar-toggle"></div><div class="container"></div><div class="navbar-collapse"></div>');
				this.element.appendTo('body');
			});
			
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});
			
			it('should add classes/css to screen elements when contractins', function() {
				scope.isCollapsed = false;
				scope.verifyMenu();				
				expect(document.getElementById('theMenu').className).toBe("navbar navbar-fixed-top");
				expect(document.getElementById('theTitle').style.marginLeft).toBe('50px');
				expect(document.getElementsByClassName('container')[0].style.marginLeft).toBe('20px');
				expect(document.getElementsByClassName('navbar-toggle')[0].className).toBe('navbar-toggle goDark');
				expect(document.getElementsByClassName('navbar-collapse')[0].style.display).toBe('none');
			});
			
			it('should add classes/css to screen elements when expanding', function() {
				scope.isCollapsed = true;
				scope.verifyMenu();				
				expect(document.getElementById('theMenu').className).toBe("navbar navbar-fixed-top navbar-inverse");
				expect(document.getElementById('theTitle').style.marginLeft).toBe('240px');
				expect(document.getElementsByClassName('container')[0].style.marginLeft).toBe('200px');
				expect(document.getElementsByClassName('navbar-toggle')[0].className).toBe('navbar-toggle');
				expect(document.getElementsByClassName('navbar-collapse')[0].style.display).toBe('block');
			});
            
            it('should retain isCollapsed when location changed', function() {
                scope.$emit('$locationChangeStart');
                expect(scope.isCollapsed).toBe(false);
                var modClass = document.querySelector('.navbar-toggle');
                modClass.classList.add("goDark");
                scope.$emit('$locationChangeStart');
                expect(scope.isCollapsed).toBe(true);
            });
            
            it('should change name when updateSession is triggered', function() {
                scope.$broadcast('updateSession',({First_Name: "Fred", Last_Name: "Flintstone"}));
                expect(scope.authentication.user.Display_Name).toBe('Fred Flintstone');
            });
		});
		
		describe('toggleCollapsibleMenu function', function() {			
			
			it('should toggle the menu on', function() {
				spyOn(scope, 'verifyMenu');
				scope.isCollapsed = false;
				scope.toggleCollapsibleMenu();
				expect(scope.verifyMenu).toHaveBeenCalled();
				expect(scope.isCollapsed).toBe(true);
			});
			
			it('should toggle the menu off', function() {
				spyOn(scope, 'verifyMenu');
				scope.isCollapsed = true;
				scope.toggleCollapsibleMenu();
				expect(scope.verifyMenu).toHaveBeenCalled();
				expect(scope.isCollapsed).toBe(false);
			});
		});
		
		// unsure how to test this function (e2e?).
		describe('outSsl function',function() {
			it('should change https to http in URL',function() {
                expect('successful test of header.client.controller.js function outSsl()').toBe(true);
//                spyOn($location, 'href').and.returnValue(false);
//                $location.$$protocol = 'https';
//               $location.$$absUrl = 'https://server';
//				scope.outSsl();
//                scope.$digest();
//				expect($location.$$absUrl).toBe('http://server');
			});
		});

		describe('getVersion function',function() {
            beforeEach(function() {
                var versionDeferred,
                    $promise;

                versionDeferred = $q.defer();
                versionDeferred.resolve({
                       version: '1.2.3.4'
                 });
                 
                spyOn(VersionService, 'get').and.returnValue({$promise: versionDeferred.promise}); //returns a fake promise;
            });
			it('should call VersionService.get',function() {
                expect(scope.version).toBeUndefined();
                scope.getVersion();
                scope.$digest();
				expect(VersionService.get).toHaveBeenCalled();
                expect(scope.version).toBe('1.2.3.4');
			});
		});
	});
})();