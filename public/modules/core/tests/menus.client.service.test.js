/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
	// Sale Controller Spec
	describe('Service: Menus', function() {
		// Initialize global variables
		var MenusService,
			httpBackend,
			resource;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));
		beforeEach(module('core'));
		
		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($injector, _Menus_, _$httpBackend_) {
			// Set a new global scope
//			scope = $rootScope.$new();

			// Point global variables to injected services
			httpBackend = _$httpBackend_;


			// Initialize the Menus service.
			MenusService = _Menus_;

		}));
		afterEach(function() {
    		httpBackend.verifyNoOutstandingExpectation();
		    httpBackend.verifyNoOutstandingRequest();
  		});


		it('should verify true is true', function() {
			expect(true).toBe(true);
		});

		describe('defined parameters', function (){
			it('should have a variable defaultRoles defined', function() {
				expect(MenusService.defaultRoles).toBeDefined();
			});

			it('should have a variable menus defined', function() {
				expect(MenusService.menus).toBeDefined();
			});

			it('should have a function validateMenuExistance defined', function() {
				expect(MenusService.validateMenuExistance).toBeDefined();
			});

			it('should have a function getMenu defined', function() {
				expect(MenusService.getMenu).toBeDefined();
			});

			it('should have a function addMenu defined', function() {
				expect(MenusService.addMenu).toBeDefined();
			});

			it('should have a function removeMenu defined', function() {
				expect(MenusService.removeMenu).toBeDefined();
			});

			it('should have a function addMenuItem defined', function() {
				expect(MenusService.addMenuItem).toBeDefined();
			});

			it('should have a function addSubMenuItem defined', function() {
				expect(MenusService.addSubMenuItem).toBeDefined();
			});

			it('should have a function removeMenuItem defined', function() {
				expect(MenusService.removeMenuItem).toBeDefined();
			});

			it('should have a function removeSubMenuItem defined', function() {
				expect(MenusService.removeSubMenuItem).toBeDefined();
			});

			it('should not recognize private function shouldRender', function() {
				expect(MenusService.shouldRender).not.toBeDefined();
			});

			it('should recognize function shouldRender as part of menu item', function() {
                var topbar = MenusService.getMenu('topbar');
				expect(topbar.shouldRender).toBeDefined();
			});
		});
		
		describe('Data types/values', function() {
			it('should recognize defaultRoles as an object, value [ * ]', function() {
				expect(typeof MenusService.defaultRoles).toBe('object');
				expect(MenusService.defaultRoles).toEqual([ '*' ]);
			});

			it('should recognize menus as an object', function() {
				expect(typeof MenusService.menus).toBe('object');
			});
		});

		describe('Interface', function() {
			it('should recognize validateMenuExistance as a function', function() {
				expect(angular.isFunction(MenusService.validateMenuExistance)).toBe(true);
			});

			it('should recognize getMenu as a function', function() {
				expect(angular.isFunction(MenusService.getMenu)).toBe(true);
			});

			it('should recognize addMenu as a function', function() {
				expect(angular.isFunction(MenusService.addMenu)).toBe(true);
			});

			it('should recognize removeMenu as a function', function() {
				expect(angular.isFunction(MenusService.removeMenu)).toBe(true);
			});

			it('should recognize addMenuItem as a function', function() {
				expect(angular.isFunction(MenusService.addMenuItem)).toBe(true);
			});

			it('should recognize addSubMenuItem as a function', function() {
				expect(angular.isFunction(MenusService.addSubMenuItem)).toBe(true);
			});

			it('should recognize removeMenuItem as a function', function() {
				expect(angular.isFunction(MenusService.removeMenuItem)).toBe(true);
			});

			it('should recognize removeSubMenuItem as a function', function() {
				expect(angular.isFunction(MenusService.removeSubMenuItem)).toBe(true);
			});
		});

		describe('test validateMenuExistence function', function() {
			it('should find no menu item named "bologna"', function() {
			/* MENU
			 *  / bologna <- tests that +this does not exist
			 */
				expect(function(){ MenusService.validateMenuExistance('bologna');}).toThrow(new Error("Menu does not exists"));
			});

			it('should throw error id no menuID is passed', function() {
				expect(function(){ MenusService.validateMenuExistance();}).toThrow(new Error("MenuId was not provided"));
			});

			it('should return true if menu is in sturcture', function() {
			/* MENU
			 *  / topbar <- tests this exists
			 */
				expect(MenusService.validateMenuExistance('topbar')).toBe(true);
			});
		});

		describe('Verify menu sctruction of topbar', function () {			
				/* Menu is defined in /public/modules/creditcard/config/creditcard.client.config.js
                 * MENU (assumes previous tests passed.)
				 *  / topbar
				 *      ├─ Home
				 *      ├─ Super Admin
				 *      │     ├─ Create Admin User
				 *      │     └─ Manage Admin Users
				 *      ├─ Merchant Accounts
				 *      │     ├─ Board a Merchant
				 *      │     └─ List Merchants
				 *      ├─ Virtual Terminal
                 *      │     ├─ Sale
                 *      │     ├─ Authorize
                 *      │     ├─ Capture
                 *      │     ├─ Void
                 *      │     ├─ Refund
                 *      │     └─ Credit
				 *      ├─ Customer Vault
				 *      │     ├─ Add Customer
				 *      │     └─ List Customer
				 *      ├─ Recurring Payments
				 *      │     ├─ Add Plan
				 *      │     └─ List Plan
				 *      ├─ Easy Buttons
				 *      │    ├─ Add Button
				 *      │    └─ List Buttons
                 *      ├─ Transaction History
				 *      ├─ Transaction Snapshot
				 *      ├─ Advanced Reports
                 *      └─ Integration
				 */
			it('should have a root with 8 submenues', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items.length).toBe(11);
			});
			it('should have a Home menu with 0 submenues', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items[0].title).toBe('Home');
                expect(topbar.items[0].items.length).toBe(0);
			});
			it('should have a Super Admin menu with 2 submenus (Create Admin User and Manage Admin Users)', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items[1].title).toBe('Super Admin');
                expect(topbar.items[1].items.length).toBe(2);
                expect(topbar.items[1].items[0].title).toBe('Create Admin User');
                expect(topbar.items[1].items[1].title).toBe('Manage Admin Users');
                expect(topbar.items[1].items[2]).toBeUndefined();
			});
			it('should have a Merchant Accounts menu with 2 submenus (Board a Merchant and List Merchants)', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items[2].title).toBe('Merchant Accounts');
                expect(topbar.items[2].items.length).toBe(2);
                expect(topbar.items[2].items[0].title).toBe('Board a Merchant');
                expect(topbar.items[2].items[1].title).toBe('List Merchants');
                expect(topbar.items[2].items[2]).toBeUndefined();
			});
			it('should have a Virtual Terminal menu with 6 submenus', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items[3].title).toBe('Virtual Terminal');
                expect(topbar.items[3].items.length).toBe(6);
                expect(topbar.items[3].items[0].title).toBe('Sale');
                expect(topbar.items[3].items[1].title).toBe('Authorize');
                expect(topbar.items[3].items[2].title).toBe('Capture');
                expect(topbar.items[3].items[3].title).toBe('Void');
                expect(topbar.items[3].items[4].title).toBe('Refund');
                expect(topbar.items[3].items[5].title).toBe('Credit');
                expect(topbar.items[3].items[6]).toBeUndefined();
			});
			it('should have a Customer Vault menu with 3 submenus (Add Customer and List Customer)', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items[4].title).toBe('Customer Vault');
                expect(topbar.items[4].items.length).toBe(3);
                expect(topbar.items[4].items[0].title).toBe('Add Customer');
                expect(topbar.items[4].items[1].title).toBe('List Customer');
                expect(topbar.items[4].items[2].title).toBe('Customer Expiry Report');
                expect(topbar.items[4].items[3]).toBeUndefined();
			});
			it('should have a Recurring Payments menu with 4 submenus (Add Plan and List Plan)', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items[5].title).toBe('Recurring Payments');
                expect(topbar.items[5].items.length).toBe(4);
                expect(topbar.items[5].items[0].title).toBe('Add Plan');
                expect(topbar.items[5].items[1].title).toBe('List Plan');
                expect(topbar.items[5].items[2].title).toBe('Add Subscription');
                expect(topbar.items[5].items[3].title).toBe('List Subscription');
                expect(topbar.items[5].items[4]).toBeUndefined();
			});
			it('should have an Easy Button menu with 2 submenus (Add Button and List Buttonss)', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items[6].title).toBe('Easy Buttons');
                expect(topbar.items[6].items.length).toBe(2);
                expect(topbar.items[6].items[0].title).toBe('Add Button');
                expect(topbar.items[6].items[1].title).toBe('List Buttons');
                expect(topbar.items[6].items[2]).toBeUndefined();
			});
			it('should have a Transaction History menu with 0 submenues', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items[7].title).toBe('Transaction History');
                expect(topbar.items[7].items.length).toBe(0);
			});
			it('should have a Transaction Snapshot menu with 0 submenues', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items[8].title).toBe('Transaction Snapshot');
                expect(topbar.items[8].items.length).toBe(0);
			});
			it('should have a Advanced Reports menu with 0 submenues', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items[9].title).toBe('Advanced Reports');
                expect(topbar.items[9].items.length).toBe(0);
			});
			it('should have a Integration menu with 0 submenues', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(topbar.items[10].title).toBe('Integration');
                expect(topbar.items[10].items.length).toBe(0);
			});
		});

		describe('test addMenu function', function () {			
			it('should return that menu with 1 parameter was added', function() {
				/* MENU
				 *  / bologna <- test adding this
				 */
				expect(function(){ MenusService.validateMenuExistance('bologna');}).toThrow(new Error("Menu does not exists"));
				MenusService.addMenu('bologna');
				expect(MenusService.validateMenuExistance('bologna')).toBe(true);
			});

			it('should return that menu with 2 parameters was added', function() {
				/* MENU
				 *  / bologna <- test adding this
				 */
				expect(function(){ MenusService.validateMenuExistance('bologna');}).toThrow(new Error("Menu does not exists"));
				MenusService.addMenu('bologna', false);
				expect(MenusService.validateMenuExistance('bologna')).toBe(true);
			});

			it('should return that menu with 3 parameters was added', function() {
				/* MENU
				 *  / bologna <- test adding this
				 */
				expect(function(){ MenusService.validateMenuExistance('bologna');}).toThrow(new Error("Menu does not exists"));
				MenusService.addMenu('bologna', false, [ '*' ]);
				expect(MenusService.validateMenuExistance('bologna')).toBe(true);
			});
		});

		describe('test removeMenu function', function () {
			it('should delete a menu', function() {
				/* MENU (assumes previous tests passed.
				 *  / bologna <- test removing this
				 */
				expect(function(){ MenusService.validateMenuExistance('bologna');}).toThrow(new Error("Menu does not exists"));
				MenusService.addMenu('bologna');
				expect(MenusService.validateMenuExistance('bologna')).toBe(true);
				MenusService.removeMenu('bologna');
				expect(function(){ MenusService.validateMenuExistance('bologna');}).toThrow(new Error("Menu does not exists"));
			});
		});
		
		describe('test getMenu function', function () {
			/* MENU (assumes previous tests passed.
			 *  / bologna <- test retrieving data about this
			 */
			it('should return a menu object', function() {
				MenusService.addMenu('bologna');
				var bologna = MenusService.getMenu('bologna');

				expect(typeof bologna).toBe('object');
				expect(Object.keys(bologna).length).toBe(4);				
				expect(bologna.isPublic).toBe(false);
				expect(bologna.roles).toEqual([ '*' ]);
				expect(Object.keys(bologna.items).length).toBe(0);
				expect(bologna.items).toEqual([]);
				expect(angular.isFunction(bologna.shouldRender)).toBe(true);
			});
		});

		describe('test addMenuItem function', function () {
			it('should add a submenu with 3 parameters', function() {
				/* MENU (assumes previous tests passed.
				 *  / bologna
				 *      └─ salami <- test adding this
				 */
				MenusService.addMenu('bologna');
				MenusService.addMenuItem('bologna', 'salami', 'link');
				var bologna = MenusService.getMenu('bologna');
				
				expect(Object.keys(bologna.items).length).toBe(1);
				expect(Object.keys(bologna.items[0]).length).toBe(13);
				expect(bologna.items[0].title).toBe('salami');
				expect(bologna.items[0].link).toBe('link');
				expect(bologna.items[0].menuItemType).toBe('item');
				expect(bologna.items[0].menuItemClass).toBeUndefined();
				expect(bologna.items[0].uiRoute).toBe('/link');
				expect(bologna.items[0].isPublic).toBe(false);
				expect(bologna.items[0].roles).toEqual([ '*' ]);
				expect(bologna.items[0].dynamicItem).toBeUndefined();
				expect(bologna.items[0].icon).toBeUndefined();
				expect(bologna.items[0].extra).toBeUndefined();
				expect(bologna.items[0].position).toBe(0);
				expect(Object.keys(bologna.items[0].items).length).toBe(0);
				expect(bologna.items[0].items).toEqual([]);
				expect(angular.isFunction(bologna.shouldRender)).toBe(true);
			});

			it('should add a submenu with all parameters', function() {
				/* MENU (assumes previous tests passed.
				 *  / bologna
				 *      └─ salami <- test adding this
				 */
				MenusService.addMenu('bologna');
				MenusService.addMenuItem('bologna', 'salami', 'link', 'type', '/uiRoute', true, 'admin', 1, 'dynamicItem', 3, true);
				var bologna = MenusService.getMenu('bologna');
				
				expect(Object.keys(bologna.items).length).toBe(1);
				expect(Object.keys(bologna.items[0]).length).toBe(13);
				expect(bologna.items[0].title).toBe('salami');
				expect(bologna.items[0].link).toBe('link');
				expect(bologna.items[0].menuItemType).toBe('type');
				expect(bologna.items[0].menuItemClass).toBe('type');
				expect(bologna.items[0].uiRoute).toBe('/uiRoute');
				expect(bologna.items[0].isPublic).toBe(true);
				expect(bologna.items[0].roles).toBe('admin');
				expect(bologna.items[0].dynamicItem).toBe('dynamicItem');
				expect(bologna.items[0].icon).toBe(3);
				expect(bologna.items[0].extra).toBe(true);
				expect(bologna.items[0].position).toBe(1);
				expect(Object.keys(bologna.items[0].items).length).toBe(0);
				expect(bologna.items[0].items).toEqual([]);
				expect(angular.isFunction(bologna.items[0].shouldRender)).toBe(true);
			});
		
			it('should add multiple submenus', function() {
				/* MENU (assumes previous tests passed.
				 *  / bologna
				 *      ├─ salami
				 *      └─ pastrami <- test adding this
				 */
				MenusService.addMenu('bologna');
				MenusService.addMenuItem('bologna', 'salami', 'link', 'type', '/uiRoute', true, 'admin', 1, 'dynamicItem', 3, true);
				MenusService.addMenuItem('bologna', 'pastrami', 'link2', 'type2', '/uiRoute2', true, 'admin2', 2, 'dynamicItem2', 4, false);
				var bologna = MenusService.getMenu('bologna');
				
				expect(Object.keys(bologna.items).length).toBe(2);
				expect(Object.keys(bologna.items[0]).length).toBe(13);
				expect(Object.keys(bologna.items[1]).length).toBe(13);
				expect(bologna.items[1].title).toBe('pastrami');
				expect(bologna.items[1].link).toBe('link2');
				expect(bologna.items[1].menuItemType).toBe('type2');
				expect(bologna.items[1].menuItemClass).toBe('type2');
				expect(bologna.items[1].uiRoute).toBe('/uiRoute2');
				expect(bologna.items[1].isPublic).toBe(true);
				expect(bologna.items[1].roles).toBe('admin2');
				expect(bologna.items[1].dynamicItem).toBe('dynamicItem2');
				expect(bologna.items[1].icon).toBe(4);
				expect(bologna.items[1].extra).toBe(false);
				expect(bologna.items[1].position).toBe(2);
				expect(Object.keys(bologna.items[1].items).length).toBe(0);
				expect(bologna.items[1].items).toEqual([]);
				expect(angular.isFunction(bologna.items[1].shouldRender)).toBe(true);
			});

		});

		describe('test addSubMenu function', function () {
			it('should add submenus with minimal input', function() {
				/* MENU (assumes previous tests passed.
				 *  / bologna
				 *      └─ salami
				 *            └─ pastrami <- test adding this
				 */
				MenusService.addMenu('bologna');
				MenusService.addMenuItem('bologna', 'salami', 'link', 'type', '/uiRoute', true, 'admin', 1, 'dynamicItem', 3, true);
				MenusService.addSubMenuItem('bologna', 'link', 'pastrami', 'link2');
				var bologna = MenusService.getMenu('bologna');
				
				expect(Object.keys(bologna.items).length).toBe(1);
				expect(Object.keys(bologna.items[0]).length).toBe(13);
				expect(Object.keys(bologna.items[0].items[0]).length).toBe(9);
				expect(bologna.items[0].items[0].title).toBe('pastrami');
				expect(bologna.items[0].items[0].link).toBe('link2');
				expect(bologna.items[0].items[0].uiRoute).toBe('/link2');
				expect(bologna.items[0].items[0].isPublic).toBe(true);
				expect(bologna.items[0].items[0].roles).toBe('admin');
				expect(bologna.items[0].items[0].dynamicItem).toBeUndefined();
				expect(bologna.items[0].items[0].icon).toBeUndefined();
				expect(bologna.items[0].items[0].position).toBe(0);
				expect(angular.isFunction(bologna.items[0].items[0].shouldRender)).toBe(true);
			});

			it('should add submenus with full input', function() {
				/* MENU (assumes previous tests passed.
				 *  / bologna
				 *      └─ salami
				 *            └─ pastrami <- test adding this
				 */
				MenusService.addMenu('bologna');
				MenusService.addMenuItem('bologna', 'salami', 'link', 'type', '/uiRoute', true, 'admin', 1, 'dynamicItem', 3, true);
				MenusService.addSubMenuItem('bologna', 'link', 'pastrami', 'link2', '/uiRoute2', true, 'admin2', 2, 'dynamicItem2', 4);
				var bologna = MenusService.getMenu('bologna');
				
				expect(Object.keys(bologna.items).length).toBe(1);
				expect(Object.keys(bologna.items[0]).length).toBe(13);
				expect(Object.keys(bologna.items[0].items[0]).length).toBe(9);
				expect(bologna.items[0].items[0].title).toBe('pastrami');
				expect(bologna.items[0].items[0].link).toBe('link2');
				expect(bologna.items[0].items[0].uiRoute).toBe('/uiRoute2');
				expect(bologna.items[0].items[0].isPublic).toBe(true);
				expect(bologna.items[0].items[0].roles).toBe('admin2');
				expect(bologna.items[0].items[0].dynamicItem).toBe('dynamicItem2');
				expect(bologna.items[0].items[0].icon).toBe(4);
				expect(bologna.items[0].items[0].position).toBe(2);
				expect(angular.isFunction(bologna.items[0].items[0].shouldRender)).toBe(true);
			});

			it('should add multiple submenus', function() {
				/* MENU (assumes previous tests passed.
				 *  / bologna
				 *      └─ salami
				 *            ├─ pastrami <- test adding this
				 *            └─ liverwurst <- test adding this
				 */
				MenusService.addMenu('bologna');
				MenusService.addMenuItem('bologna', 'salami', 'link', 'type', '/uiRoute', true, 'admin', 1, 'dynamicItem', 3, true);
				MenusService.addSubMenuItem('bologna', 'link', 'pastrami', 'link2', '/uiRoute2', true, 'admin2', 2, 'dynamicItem2', 4);
				MenusService.addSubMenuItem('bologna', 'link', 'liverwurst', 'link3', '/uiRoute3', true, 'admin3', 1, 'dynamicItem3', 5);
				var bologna = MenusService.getMenu('bologna');
				
				expect(Object.keys(bologna.items).length).toBe(1);
				expect(Object.keys(bologna.items[0]).length).toBe(13);
				expect(Object.keys(bologna.items[0].items[0]).length).toBe(9);
				expect(Object.keys(bologna.items[0].items[1]).length).toBe(9);
				expect(bologna.items[0].items[1].title).toBe('liverwurst');
				expect(bologna.items[0].items[1].link).toBe('link3');
				expect(bologna.items[0].items[1].uiRoute).toBe('/uiRoute3');
				expect(bologna.items[0].items[1].isPublic).toBe(true);
				expect(bologna.items[0].items[1].roles).toBe('admin3');
				expect(bologna.items[0].items[1].dynamicItem).toBe('dynamicItem3');
				expect(bologna.items[0].items[1].icon).toBe(5);
				expect(bologna.items[0].items[1].position).toBe(1);
				expect(angular.isFunction(bologna.items[0].items[1].shouldRender)).toBe(true);
			});
		});

		describe('test removeMenuItem function', function () {
			it('should remove a menu item', function() {
				/* MENU (assumes previous tests passed.
				 *  / bologna
				 *      └─ salami <- test removing this
				 */
				MenusService.addMenu('bologna');
				MenusService.addMenuItem('bologna', 'salami', 'link2');
				var bologna = MenusService.getMenu('bologna');
				expect(Object.keys(bologna.items).length).toBe(1);
				expect(Object.keys(bologna.items[0]).length).toBe(13);

				MenusService.removeMenuItem('bologna', 'link2');
				bologna = MenusService.getMenu('bologna');
				expect(Object.keys(bologna.items).length).toBe(0);			
			});
		});

		describe('test removeSubMenuItem function', function () {
				/* MENU (assumes previous tests passed.
				 *  / bologna
				 *      └─ salami
				 *            └─ pastrami <- test removing this
				 */
			it('should remove a submenu item', function() {
				MenusService.addMenu('bologna');
				MenusService.addMenuItem('bologna', 'salami', 'link');
				MenusService.addSubMenuItem('bologna', 'link', 'pastrami', 'link2');
				var bologna = MenusService.getMenu('bologna');
				expect(Object.keys(bologna.items).length).toBe(1);
				expect(Object.keys(bologna.items[0]).length).toBe(13);
				expect(Object.keys(bologna.items[0].items[0]).length).toBe(9);

				MenusService.removeSubMenuItem('bologna', 'link2');
				bologna = MenusService.getMenu('bologna');
				expect(Object.keys(bologna.items[0]).length).toBe(13);
				expect(Object.keys(bologna.items[0].items).length).toEqual(0);			
			});
		});
        
		describe('Can\'t verify private functionshouldRender directly, but can verify it is in menu object', function () {
				/* Menu is defined in /public/modules/creditcard/config/creditcard.client.config.js
                 * MENU (assumes previous tests passed.)
				 *  / topbar
				 *      ├─ Home  <-- should have a shouldRender function
				 *      ├─ Super Admin
				 *      │     ├─ Create Admin User
				 *      │     └─ Manage Admin Users
				 *      ├─ Merchant Accounts
				 *      │     ├─ Board a Merchant
				 *      │     └─ List Merchants
				 *      ├─ Virtual Terminal
                 *      │     ├─ Sale
                 *      │     ├─ Authorize
                 *      │     ├─ Capture
                 *      │     ├─ Void
                 *      │     ├─ Refund
                 *      │     └─ Credit
				 *      ├─ Customer Vault
				 *      │     ├─ Add Customer
				 *      │     ├─ List Customer
				 *      │     └─ Customer Expiry Report
				 *      ├─ Recurring Payments
				 *      │     ├─ Add Plan
				 *      │     ├─ List Plan
				 *      │     ├─ Add Subscription
				 *      │     └─ List Subecription
				 *      ├─ Easy Buttons
				 *      │    ├─ Add Button
				 *      │    └─ List Buttons
                 *      ├─ Transaction History
				 *      ├─ Transaction Snapshot
				 *      ├─ Advanced Reports
                 *      └─ Integration
				 */
			it('should have a function called shouldRender', function() {
                var topbar = MenusService.getMenu('topbar');
                expect(angular.isFunction(topbar.items[0].shouldRender)).toBeDefined();
			});
		});
        
		describe('Test shouldRender via menu object', function () {
				/* Menu is defined in /public/modules/creditcard/config/creditcard.client.config.js
                 * MENU (assumes previous tests passed.)
				 *  / topbar
				 *      ├─ Home
				 *      ├─ Super Admin
				 *      │     ├─ Create Admin User
				 *      │     └─ Manage Admin Users
				 *      ├─ Merchant Accounts
				 *      │     ├─ Board a Merchant
				 *      │     └─ List Merchants
				 *      ├─ Virtual Terminal
                 *      │     ├─ Sale
                 *      │     ├─ Authorize
                 *      │     ├─ Capture
                 *      │     ├─ Void
                 *      │     ├─ Refund
                 *      │     └─ Credit
				 *      ├─ Customer Vault
				 *      │     ├─ Add Customer
				 *      │     ├─ List Customer
				 *      │     └─ Customer Expiry Report
				 *      ├─ Recurring Payments
				 *      │     ├─ Add Plan
				 *      │     ├─ List Plan
				 *      │     ├─ Add Subscription
				 *      │     └─ List Subecription
				 *      ├─ Easy Buttons
				 *      │    ├─ Add Button
				 *      │    └─ List Buttons
                 *      ├─ Transaction History
				 *      ├─ Transaction Snapshot
				 *      ├─ Advanced Reports
                 *      └─ Integration
				 */
            it('should return proper values if not passed a role based on isPublic parameter', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               expect(topbar.items[0].shouldRender(user)).toBe('');
               topbar.items[0].isPublic = false;
               expect(topbar.items[0].shouldRender(user)).toBe(false);
               topbar.items[0].isPublic = true;
               expect(topbar.items[0].shouldRender(user)).toBe(true);
            });
            it('should return proper values if passed a role', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {Roles:'*'};
               expect(topbar.items[0].shouldRender(user)).toBe(true);  // roles = '*'
               expect(topbar.items[1].shouldRender(user)).toBe(false); // roles = 'super'
               expect(topbar.items[2].shouldRender(user)).toBe(false); // roles = 'admin'
               expect(topbar.items[3].shouldRender(user)).toBe(false); // roles = ''
               expect(topbar.items[8].shouldRender(user)).toBe(false); // roles = 'admin'
               user = {Roles:'user'};
               expect(topbar.items[0].shouldRender(user)).toBe(true);  // roles = '*'
               expect(topbar.items[1].shouldRender(user)).toBe(false); // roles = 'super'
               expect(topbar.items[2].shouldRender(user)).toBe(false); // roles = 'admin'
               expect(topbar.items[3].shouldRender(user)).toBe(false); // roles = ''
               expect(topbar.items[7].shouldRender(user)).toBe(false); // roles = 'admin'
               user = {Roles:'marketplace'};
               expect(topbar.items[0].shouldRender(user)).toBe(true);  // roles = '*'
               expect(topbar.items[1].shouldRender(user)).toBe(false); // roles = 'super'
               expect(topbar.items[2].shouldRender(user)).toBe(false); // roles = 'admin'
               expect(topbar.items[3].shouldRender(user)).toBe(false); // roles = ''
               expect(topbar.items[8].shouldRender(user)).toBe(false); // roles = 'admin'
               user = {Roles:'admin'};
               expect(topbar.items[0].shouldRender(user)).toBe(true);  // roles = '*'
               expect(topbar.items[1].shouldRender(user)).toBe(false); // roles = 'super'
               expect(topbar.items[2].shouldRender(user)).toBe(true);  // roles = 'admin'
               expect(topbar.items[3].shouldRender(user)).toBe(false); // roles = ''
               expect(topbar.items[8].shouldRender(user)).toBe(true);  // roles = 'admin'
               user = {Roles:'super'};
               expect(topbar.items[0].shouldRender(user)).toBe(true);  // roles = '*'
               expect(topbar.items[1].shouldRender(user)).toBe(true);  // roles = 'super'
               expect(topbar.items[2].shouldRender(user)).toBe(true);  // roles = 'admin'
               expect(topbar.items[3].shouldRender(user)).toBe(false); // roles = ''
               expect(topbar.items[8].shouldRender(user)).toBe(true);  // roles = 'admin'
            });
		});
        
		describe('Test shouldRender Virtual Terminal permissions', function () {
				/* Menu is defined in /public/modules/creditcard/config/creditcard.client.config.js
                 * MENU (assumes previous tests passed.)
				 *  / topbar
				 *      ├─ Home
				 *      ├─ Super Admin
				 *      │     ├─ Create Admin User
				 *      │     └─ Manage Admin Users
				 *      ├─ Merchant Accounts
				 *      │     ├─ Board a Merchant
				 *      │     └─ List Merchants
				 *      ├─ Virtual Terminal  <-- testing this menu and sub menus
                 *      │     ├─ Sale
                 *      │     ├─ Authorize
                 *      │     ├─ Capture
                 *      │     ├─ Void
                 *      │     ├─ Refund
                 *      │     └─ Credit
				 *      ├─ Customer Vault
				 *      │     ├─ Add Customer
				 *      │     ├─ List Customer
				 *      │     └─ Customer Expiry Report
				 *      ├─ Recurring Payments
				 *      │     ├─ Add Plan
				 *      │     ├─ List Plan
				 *      │     ├─ Add Subscription
				 *      │     └─ List Subecription
				 *      ├─ Easy Buttons
				 *      │    ├─ Add Button
				 *      │    └─ List Buttons
                 *      ├─ Transaction History
				 *      ├─ Transaction Snapshot
				 *      ├─ Advanced Reports
                 *      └─ Integration
				 */
            it('should return proper values if Virtual_Terminal permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Virtual_Terminal: false};
               expect(topbar.items[3].shouldRender(user)).toBe(false);  // virtual Terminal
               user = {roles:'user', Virtual_Terminal: true};
               expect(topbar.items[3].shouldRender(user)).toBe(true);   // virtual Terminal
    		});
            it('should return proper values if Terminal_Sale permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Terminal_Sale: false};
               expect(topbar.items[3].items[0].shouldRender(user)).toBe(false);  // terminal Sale
               user = {roles:'user', Terminal_Sale: true};
               expect(topbar.items[3].items[0].shouldRender(user)).toBe(true);   // terminal Sale
    		});
            it('should return proper values if Terminal_Authorize permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Terminal_Authorize: false};
               expect(topbar.items[3].items[1].shouldRender(user)).toBe(false);  // terminal Authorize
               user = {roles:'user', Terminal_Authorize: true};
               expect(topbar.items[3].items[1].shouldRender(user)).toBe(true);   // terminal Authorize
    		});
            it('should return proper values if Terminal_Capture permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Terminal_Capture: false};
               expect(topbar.items[3].items[2].shouldRender(user)).toBe(false);  // terminal Capture
               user = {roles:'user', Terminal_Capture: true};
               expect(topbar.items[3].items[2].shouldRender(user)).toBe(true);   // terminal Capture
    		});
            it('should return proper values if Terminal_Void permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Terminal_Void: false};
               expect(topbar.items[3].items[3].shouldRender(user)).toBe(false);  // terminal Void
               user = {roles:'user', Terminal_Void: true};
               expect(topbar.items[3].items[3].shouldRender(user)).toBe(true);   // terminal Void
    		});
            it('should return proper values if Terminal_Refund permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Terminal_Refund: false};
               expect(topbar.items[3].items[4].shouldRender(user)).toBe(false);  // terminal Refund
               user = {roles:'user', Terminal_Refund: true};
               expect(topbar.items[3].items[4].shouldRender(user)).toBe(true);   // terminal Refund
    		});
            it('should return proper values if Terminal_Credit permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Terminal_Credit: false};
               expect(topbar.items[3].items[5].shouldRender(user)).toBe(false);  // terminal Credit
               user = {roles:'user', Terminal_Credit: true};
               expect(topbar.items[3].items[5].shouldRender(user)).toBe(true);   // terminal Credit
    		});
		});
        
		describe('Test shouldRender Customer Vault permissions', function () {
				/* Menu is defined in /public/modules/creditcard/config/creditcard.client.config.js
                 * MENU (assumes previous tests passed.)
				 *  / topbar
				 *      ├─ Home
				 *      ├─ Super Admin
				 *      │     ├─ Create Admin User
				 *      │     └─ Manage Admin Users
				 *      ├─ Merchant Accounts
				 *      │     ├─ Board a Merchant
				 *      │     └─ List Merchants
				 *      ├─ Virtual Terminal
                 *      │     ├─ Sale
                 *      │     ├─ Authorize
                 *      │     ├─ Capture
                 *      │     ├─ Void
                 *      │     ├─ Refund
                 *      │     └─ Credit
				 *      ├─ Customer Vault  <-- testing this menu and sub menus
				 *      │     ├─ Add Customer
				 *      │     ├─ List Customer
				 *      │     └─ Customer Expiry Report
				 *      ├─ Recurring Payments
				 *      │     ├─ Add Plan
				 *      │     ├─ List Plan
				 *      │     ├─ Add Subscription
				 *      │     └─ List Subecription
				 *      ├─ Easy Buttons
				 *      │    ├─ Add Button
				 *      │    └─ List Buttons
                 *      ├─ Transaction History
				 *      ├─ Transaction Snapshot
				 *      ├─ Advanced Reports
                 *      └─ Integration
				 */
           it('should return proper values if Customer_Vault permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Customer_Vault: false};
               expect(topbar.items[4].shouldRender(user)).toBe(false);  // customerVault
               user = {roles:'user', Customer_Vault: true};
               expect(topbar.items[4].shouldRender(user)).toBe(true);   // customerVault
    		});
            it('should return proper values if Customer_Add permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Customer_Add: false};
               expect(topbar.items[4].items[0].shouldRender(user)).toBe(false);  // customerAdd
               user = {roles:'user', Customer_Add: true};
               expect(topbar.items[4].items[0].shouldRender(user)).toBe(true);   // customerAdd
    		});
            it('should return proper values if Customer_List permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Customer_List: false};
               expect(topbar.items[4].items[1].shouldRender(user)).toBe(false);  // customerList
               user = {roles:'user', Customer_List: true};
               expect(topbar.items[4].items[1].shouldRender(user)).toBe(true);   // customerList
    		});
            it('should return proper values if Customer_Expiry permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Customer_Expiry: false};
               expect(topbar.items[4].items[2].shouldRender(user)).toBe(false);  // customerexpiry
               user = {roles:'user', Customer_Expiry: true};
               expect(topbar.items[4].items[2].shouldRender(user)).toBe(true);   // customerexpiry
    		});
		});
        
		describe('Test shouldRender Recurring Payments permissions', function () {
				/* Menu is defined in /public/modules/creditcard/config/creditcard.client.config.js
                 * MENU (assumes previous tests passed.)
				 *  / topbar
				 *      ├─ Home
				 *      ├─ Super Admin
				 *      │     ├─ Create Admin User
				 *      │     └─ Manage Admin Users
				 *      ├─ Merchant Accounts
				 *      │     ├─ Board a Merchant
				 *      │     └─ List Merchants
				 *      ├─ Virtual Terminal
                 *      │     ├─ Sale
                 *      │     ├─ Authorize
                 *      │     ├─ Capture
                 *      │     ├─ Void
                 *      │     ├─ Refund
                 *      │     └─ Credit
				 *      ├─ Customer Vault
				 *      │     ├─ Add Customer
				 *      │     ├─ List Customer
				 *      │     └─ Customer Expiry Report
				 *      ├─ Recurring Payments  <-- testing this menu and sub menus
				 *      │     ├─ Add Plan
				 *      │     ├─ List Plan
				 *      │     ├─ Add Subscription
				 *      │     └─ List Subecription
				 *      ├─ Easy Buttons
				 *      │    ├─ Add Button
				 *      │    └─ List Buttons
                 *      ├─ Transaction History
				 *      ├─ Transaction Snapshot
				 *      ├─ Advanced Reports
                 *      └─ Integration
				 */
            it('should return proper values if Recurring_Payments permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Recurring_Payments: false};
               expect(topbar.items[5].shouldRender(user)).toBe(false);  // Recurring Payments  
               user = {roles:'user', Recurring_Payments: true};
               expect(topbar.items[5].shouldRender(user)).toBe(true);   // Recurring Payments  
    		});
            it('should return proper values if Plan_Add permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Plan_Add: false};
               expect(topbar.items[5].items[0].shouldRender(user)).toBe(false);  // recurringplancreate
               user = {roles:'user', Plan_Add: true};
               expect(topbar.items[5].items[0].shouldRender(user)).toBe(true);   // recurringplancreate
    		});
            it('should return proper values if Plan_List permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Plan_List: false};
               expect(topbar.items[5].items[1].shouldRender(user)).toBe(false);  // recurringplanlist
               user = {roles:'user', Plan_List: true};
               expect(topbar.items[5].items[1].shouldRender(user)).toBe(true);   // recurringplanlist
    		});
            it('should return proper values if Subscription_Add permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Subscription_Add: false};
               expect(topbar.items[5].items[2].shouldRender(user)).toBe(false);  // subscriptioncreate
               user = {roles:'user', Subscription_Add: true};
               expect(topbar.items[5].items[2].shouldRender(user)).toBe(true);   // subscriptioncreate
    		});
            it('should return proper values if Subscription_List permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Subscription_List: false};
               expect(topbar.items[5].items[3].shouldRender(user)).toBe(false);  // subscriptionlist
               user = {roles:'user', Subscription_List: true};
               expect(topbar.items[5].items[3].shouldRender(user)).toBe(true);   // subscriptionlist
    		});
		});
        
		describe('Test shouldRender Integration permissions', function () {
				/* Menu is defined in /public/modules/creditcard/config/creditcard.client.config.js
                 * MENU (assumes previous tests passed.)
				 *  / topbar
				 *      ├─ Home
				 *      ├─ Super Admin
				 *      │     ├─ Create Admin User
				 *      │     └─ Manage Admin Users
				 *      ├─ Merchant Accounts
				 *      │     ├─ Board a Merchant
				 *      │     └─ List Merchants
				 *      ├─ Virtual Terminal
                 *      │     ├─ Sale
                 *      │     ├─ Authorize
                 *      │     ├─ Capture
                 *      │     ├─ Void
                 *      │     ├─ Refund
                 *      │     └─ Credit
				 *      ├─ Customer Vault
				 *      │     ├─ Add Customer
				 *      │     ├─ List Customer
				 *      │     └─ Customer Expiry Report
				 *      ├─ Recurring Payments
				 *      │     ├─ Add Plan
				 *      │     ├─ List Plan
				 *      │     ├─ Add Subscription
				 *      │     └─ List Subecription
				 *      ├─ Easy Buttons  <-- testing this menu
				 *      │     ├─ Add Button
				 *      │     └─ List Buttons─ Transaction History
				 *      ├─ Transaction Snapshot
				 *      ├─ Advanced Reports
				 *      └─ Integration
				 */
            it('should return proper values if Easy_Buttons permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Easy_Buttons: false};
               expect(topbar.items[6].shouldRender(user)).toBe(false);  // Easy_Buttons
               user = {roles:'user', Easy_Buttons: true};
               expect(topbar.items[6].shouldRender(user)).toBe(true);   // Easy_Buttons
    		});
            it('should return proper values if Button_Add permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Button_Add: false};
               expect(topbar.items[6].items[0].shouldRender(user)).toBe(false);  // Button_Add
               user = {roles:'user', Button_Add: true};
               expect(topbar.items[6].items[0].shouldRender(user)).toBe(true);   // Button_Add
    		});
            it('should return proper values if Button_List permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Button_List: false};
               expect(topbar.items[6].items[1].shouldRender(user)).toBe(false);  // Button_List
               user = {roles:'user', Button_List: true};
               expect(topbar.items[6].items[1].shouldRender(user)).toBe(true);   // Button_List
    		});
		});

		describe('Test shouldRender Transaction History permissions', function () {
				/* Menu is defined in /public/modules/creditcard/config/creditcard.client.config.js
                 * MENU (assumes previous tests passed.)
				 *  / topbar
				 *      ├─ Home
				 *      ├─ Super Admin
				 *      │     ├─ Create Admin User
				 *      │     └─ Manage Admin Users
				 *      ├─ Merchant Accounts
				 *      │     ├─ Board a Merchant
				 *      │     └─ List Merchants
				 *      ├─ Virtual Terminal
                 *      │     ├─ Sale
                 *      │     ├─ Authorize
                 *      │     ├─ Capture
                 *      │     ├─ Void
                 *      │     ├─ Refund
                 *      │     └─ Credit
				 *      ├─ Customer Vault
				 *      │     ├─ Add Customer
				 *      │     ├─ List Customer
				 *      │     └─ Customer Expiry Report
				 *      ├─ Recurring Payments
				 *      │     ├─ Add Plan
				 *      │     ├─ List Plan
				 *      │     ├─ Add Subscription
				 *      │     └─ List Subecription
				 *      ├─ Easy Buttons
				 *      │    ├─ Add Button
				 *      │    └─ List Buttons
				 *      ├─ Transaction History  <-- testing this menu and sub menus
				 *      ├─ Transaction Snapshot
				 *      ├─ Advanced Reports
                 *      └─ Integration
				 */
            it('should return proper values if Transaction_Report permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Transaction_Report: false};
               expect(topbar.items[7].shouldRender(user)).toBe(false);  // Transaction History
               user = {roles:'user', Transaction_Report: true};
               expect(topbar.items[7].shouldRender(user)).toBe(true);   // Transaction History
    		});
		});
        
		describe('Test shouldRender Advanced Reports permissions', function () {
				/* Menu is defined in /public/modules/creditcard/config/creditcard.client.config.js
                 * MENU (assumes previous tests passed.)
				 *  / topbar
				 *      ├─ Home
				 *      ├─ Super Admin
				 *      │     ├─ Create Admin User
				 *      │     └─ Manage Admin Users
				 *      ├─ Merchant Accounts
				 *      │     ├─ Board a Merchant
				 *      │     └─ List Merchants
				 *      ├─ Virtual Terminal
                 *      │     ├─ Sale
                 *      │     ├─ Authorize
                 *      │     ├─ Capture
                 *      │     ├─ Void
                 *      │     ├─ Refund
                 *      │     └─ Credit
				 *      ├─ Customer Vault
				 *      │     ├─ Add Customer
				 *      │     ├─ List Customer
				 *      │     └─ Customer Expiry Report
				 *      ├─ Recurring Payments
				 *      │     ├─ Add Plan
				 *      │     ├─ List Plan
				 *      │     ├─ Add Subscription
				 *      │     └─ List Subecription
				 *      ├─ Easy Buttons
				 *      │    ├─ Add Button
				 *      │    └─ List Buttons
				 *      ├─ Transaction History
				 *      ├─ Transaction Snapshot
				 *      ├─ Advanced Reports  <-- testing this menu
                 *      └─ Integration
				 */
            it('should return proper values if Advanced_Report permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Advanced_Report: false};
               expect(topbar.items[9].shouldRender(user)).toBe(false);  // Advanced Reports
               user = {roles:'user', Advanced_Report: true};
               expect(topbar.items[9].shouldRender(user)).toBe(true);   // Advanced Reports
    		});
		});
        
		describe('Test shouldRender Integration permissions', function () {
				/* Menu is defined in /public/modules/creditcard/config/creditcard.client.config.js
                 * MENU (assumes previous tests passed.)
				 *  / topbar
				 *      ├─ Home
				 *      ├─ Super Admin
				 *      │     ├─ Create Admin User
				 *      │     └─ Manage Admin Users
				 *      ├─ Merchant Accounts
				 *      │     ├─ Board a Merchant
				 *      │     └─ List Merchants
				 *      ├─ Virtual Terminal
                 *      │     ├─ Sale
                 *      │     ├─ Authorize
                 *      │     ├─ Capture
                 *      │     ├─ Void
                 *      │     ├─ Refund
                 *      │     └─ Credit
				 *      ├─ Customer Vault
				 *      │     ├─ Add Customer
				 *      │     ├─ List Customer
				 *      │     └─ Customer Expiry Report
				 *      ├─ Recurring Payments
				 *      │     ├─ Add Plan
				 *      │     ├─ List Plan
				 *      │     ├─ Add Subscription
				 *      │     └─ List Subecription
				 *      ├─ Easy Buttons
				 *      │    ├─ Add Button
				 *      │    └─ List Buttons
				 *      ├─ Transaction History
				 *      ├─ Transaction Snapshot
				 *      ├─ Advanced Reports
				 *      └─ Integration  <-- testing this menu
				 */
            it('should return proper values if Integration permission set', function() {
               var topbar = MenusService.getMenu('topbar');
               var user;
               user = {roles:'user', Integration: false};
               expect(topbar.items[10].shouldRender(user)).toBe(false);  // Integration
               user = {roles:'user', Integration: true};
               expect(topbar.items[10].shouldRender(user)).toBe(true);   // Integration
    		});
		});
	});
}());