'use strict';

angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus','$location','VersionService',
	function($scope, Authentication, Menus, $location, VersionService) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');

		// Austin - sets the display name in the header upper right to the currently logged in username		

		//$scope.displayUserName = displayName(Authentication.user.displayName).name;


		// ensure main menu remains hidden (if it is already hidden) when changing locations

		$scope.$on('$locationChangeStart', function(event) {
			if (((document.querySelector('.navbar-toggle').getAttribute('class')).indexOf('goDark')) !== -1){
				$scope.isCollapsed = true;
 			}
		});

		$scope.$on('updateSession', function (event, data) {
		  //alert('session updatad');

//		  console.log(data);
		  $scope.authentication.user.Display_Name = data.First_Name + ' ' + data.Last_Name;

		});


		// helper function to show/collapse the menu 
		$scope.verifyMenu = function(){
			var togglePushBottom = document.querySelector( '.navbar-toggle' ),
				bodyContent = document.querySelector( '.container' ),
				menuBody = document.querySelector('.navbar-collapse');

			if ($scope.isCollapsed === false){  // collapse
				document.getElementById('theMenu').className = 'navbar navbar-fixed-top';
	    		bodyContent.style.marginLeft = '20px';
	    		document.getElementById('theTitle').style.marginLeft = '50px';
	    		togglePushBottom.className = 'navbar-toggle goDark';
	    		menuBody.style.display = 'none';
			}else{ // show
				document.getElementById('theMenu').className = 'navbar navbar-fixed-top navbar-inverse';
				bodyContent.style.marginLeft = '200px';
				document.getElementById('theTitle').style.marginLeft = '240px';
				togglePushBottom.className = 'navbar-toggle';
	    		menuBody.style.display = 'block';
			}
		};

		// called function that show/collapse the menu 
		$scope.toggleCollapsibleMenu = function() {
			$scope.verifyMenu();
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// ensure we are on the HTTP site upon logout
		$scope.outSsl=function(){
		    if ($location.protocol() !== 'http')
		        window.location.href = $location.absUrl().replace('https', 'http');
		};
        
        $scope.getVersion = function() {
            VersionService.get().$promise.then(function(version) {
                $scope.version = version.version;
            });
        };

	}
]);

