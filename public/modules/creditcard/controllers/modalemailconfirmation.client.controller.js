'use strict';

angular.module('creditcard').controller('ModalEmailConfCtrl',['$scope','$modalInstance','message',
  function ($scope, $modalInstance, message) {

    $scope.emailmessage = message;
// console.log($scope.emailmessage);


    /**
     * Ok button
     */
    $scope.ok = function () {
        var emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
/*  not sure what this is here for
        if (false) {
            $modalInstance.close();
        }
*/

        var addressInput = $scope.emailAddressModal.trim();
        /* invalid inputs */
        if ($scope.emailAddressModal.indexOf(',') > -1) {
            $scope.error = 'There is an illegal character (a comma) in the e-mail list';
        } else {
            addressInput = addressInput.replace(/(\r\n|\n|\r)/gm,';');
            addressInput = addressInput.replace(/\s/g,'') ;
            var addresses = addressInput.split(";");
            if (addresses.length > 10) {
                $scope.error = 'You may have a maximum of 10 e-mail addresses, you have ' + addresses.length;
            } else {
                for(var i = 0; i < addresses.length; i++) {
                    if (!emailRegex.test(addresses[i])) {
                        $scope.error = 'You may have an invalid e-mail address: ' + addresses[i];
                    } else {
                        message.response = addresses;
                        $modalInstance.close();
                    }
                }
            }
        }
    };

    
    /**
     * Cancel Button 
     */
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

 
    /**
    * Builds the view
    */
    $scope.getInformation = function(){
        $scope.title = message.title;
        $scope.emailAddressModal = message.emailAddress;
        $scope.error = '';
    };   
}]);
