'use strict';

/**
 * Builds an Data object that can then is rendered in the Views, specifically Reports.
 *
 *
 * @param  $q                   A service that helps you run functions asynchronously.
 * @param  $scope               An object that refers to the application model.
 * @param  Authentication       A service used for retrieve users and roles
 * @param  PaymentService       A service used for retrieve payment plans
 * @param  Nmitransactions      A service used for retrieve data from NMI API
 * @param  $location            This service parses the URL in the browser.
 * @param  $anchorScroll        A service components in Angular to control scrollbars.
 * @param  $filter              A service used for formatting data displayed.
 * @param  $parse               A service used for converts Angular expression into a function.
 * @param  $modal               A direcGive used for build modal windows.
 * @param  MasService           A service used for retrieve data from Backend
 *
 */

angular.module('creditcard').controller('SaleController', ['$q','$scope','Authentication','PaymentService','Nmitransactions','ReserveService','$location','$anchorScroll','$filter','$parse','$modal','MasService','TransactionsService','ProcessorService','ProductService','MerchantReportService','RunningBalanceService','SettleService','DiscountService','TransactionFeeService','Account_BalanceService','disbursementsService','$http','AccountBalanceReportService','Users','ACHReportService','PaymentCompleteService','LineItemsService','RefundService','$locale','RecurringPaymentPlanService','RecurringPlanService','AuditRecurrPlanService','AuditRecurrPlanHeaderService','AuditRecurrPlanDetailService','CheckRecurringPlanService','VaultService','SubscriptionService','SubscriptionFindOne','Line_ItemsService','MdfService',
    function($q,$scope,Authentication,PaymentService,Nmitransactions,ReserveService,$location, $anchorScroll,$filter,$parse,$modal,MasService,TransactionsService,ProcessorService,ProductService,MerchantReportService,RunningBalanceService,SettleService,DiscountService,TransactionFeeService,Account_BalanceService,disbursementsService,$http,AccountBalanceReportService,Users,ACHReportService,PaymentCompleteService,LineItemsService,RefundService,$locale,RecurringPaymentPlanService,RecurringPlanService,AuditRecurrPlanService,AuditRecurrPlanHeaderService,AuditRecurrPlanDetailService,CheckRecurringPlanService,VaultService,SubscriptionService,SubscriptionFindOne,Line_ItemsService,MdfService) {

        if (!Authentication.user){
            $location.path('/signin');
            return false;
        }
        if (Authentication.user.Roles!=='user'){
            $scope.theAdmin = true;
            $scope.isAdmin = true;
        }else{
            $scope.theAdmin = false;
            $scope.isAdmin = false;
        }
        
        // Prototyping array for delete a item
        Array.prototype.remove = function(from, to) {
            var rest = this.slice((to || from) + 1 || this.length);
            this.length = from < 0 ? this.length + from : from;
            return this.push.apply(this, rest);
        };
        
        // Austin - passthrough functions created bc passing object directly did not work (undefined), can refactor later? 
          
        $scope.Product_NameChangeMAR = function() {                                   
            $scope.setProducts($scope.Product_NameMAR);           
        };
                
        $scope.Product_NameChangeUserMAR = function() {                                   
            $scope.setProductId($scope.Product_NameUserMAR);           
        };
                
        $scope.Product_NameChangeABR = function() {                      
            $scope.setProducts($scope.Product_NameABR);           
        };  

        $scope.Product_NameChangeACH = function() {                      
            $scope.setProducts($scope.Product_NameACH);           
        };    
        
        $scope.Product_NameChangeABR_Merchant = function() {                 
            $scope.setProductId($scope.Product_NameABR_Merchant);       
        }; 
        
        $scope.Product_NameChangeDASH = function() {                       
            $scope.setProductId8($scope.Product_NameDASH);       
        };          
              
        $scope.Product_NameChangeSearchTransactions = function() {
            $scope.setProductId($scope.Product_NameSearchTransactions);           
        };                  
                    
                 
        // ----------------------------------------------------------------------------------------------------        

        $('.dropdown-menu').find('input').click(function (e) {
            e.stopPropagation();
        });

        // NationBuilder Button Stuff
        if(Authentication.user.Enable_Nation){
            $scope.hideNationBuilderAlert = false;
        }else{
            $scope.hideNationBuilderAlert = true;
        }

        if ($scope.isAdmin || Authentication.user.Roles!=='user') {
            $scope.hideNationBuilderAlert = true;
        }else{
            if(!Authentication.user.Enable_Nation) {
                $scope.hideNationBuilderAlert = true;
            }
        }
        
        $scope.hideNationBuilderButton = function(){
            $scope.hideNationBuilderAlert = true;

            //set database value
            $scope.success = $scope.error = null;
            var user = new Users(Authentication.user);
            user.Enable_Nation = false;
            user.$update(function(response) {
                $scope.success = true;
                Authentication.user = response;
            }, function(response) {
                $scope.error = response.data.message;
            });
        };

        $scope.NationBuilderButtonPressed = function() {
            window.location.href = "/#!/settings/nation_builder_integration";
        };

        /**
         * Global vars
         * @type {Array}
         */
        $scope.treportFormChanged = false;
        $scope.joinedTransactions=[];
        var counterProcessors = 0,
            processorsReserves = [],
            allTransactions = [],
            resultsOrderByTime = [],
            transactionObject = [],
            settle = 0,
            refund = 0,
            param,
            i,
            flagGeneral = false,
            jsonMerchantsProducts = jsonMerchantsProducts || {},
            arrayMerchantsProducts = [],
            chargeback,
            retrieval_fee,
            chargeback_reversal,
            ach_reject,
            misc,
            line_itemsArray = [],
            disbursementDatesArray = [],
            productIdSelected,
            productsList,
            masArray = [];

        Object.size = function(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };
        /**
         * Order by Time
         */
        $scope.criteria = 'orderTime';
        $scope.direction = true;
        $scope.productId = 2;

        $scope.hyphenize_date = function(date){
            if (date === undefined) {
                return '';
            }
            var tempdate = date;
            var yy = '';
            var mm = '';
            var dd = '';
            tempdate = tempdate.replace(/\//g,'-');
            tempdate = tempdate.replace(/\./g,'-');
            if (tempdate.charAt(4) === '-' && tempdate.charAt(7) === '-'){
                yy = tempdate.substr(0, 4);
                mm = tempdate.substr(5, 2);
                dd = tempdate.substr(8, 2);
            } else if(tempdate.charAt(2) === '-' && tempdate.charAt(5) === '-'){
                yy = tempdate.substr(6, 4);
                mm = tempdate.substr(0, 2);
                dd = tempdate.substr(3, 2);
            }

            var formatted_date = yy + '-' + mm + '-' + dd;
            return formatted_date;
        };


        $scope.date_error = false;
        $scope.DateChecker = function(date) {
            var tempdate = $scope.hyphenize_date(document.getElementById('oneDate').value);
            var date1 = new Date(tempdate + 'T00:00:00');
            var limit_date = new Date();
            limit_date.setDate(limit_date.getDate() - 1);
            limit_date.setHours(0);
            limit_date.setMinutes(0);
            limit_date.setSeconds(0);
            limit_date.setMilliseconds(0);
            if (date1 <= limit_date) {
                $scope.date_error = false;
            }else{
                $scope.date_error = 'Account balance is not available for the selected date';
                document.getElementById('oneDate').focus();
            }
        };


        $scope.setCriteria = function(criteria) {
            if ($scope.criteria === criteria) {
                $scope.direction = !$scope.direction;
            } else {
                $scope.criteria = criteria;
                $scope.direction  = false;
            }

            // re-order the items
            var dir = ($scope.direction) ? 'desc': 'asc';
            $scope.items = sortByKey($scope.items, $scope.criteria, dir);
        };
        // follow function correspond to sort stuff
        var getStatusString = function(code){
            var status = '';
            switch(code) {
                case '100': status = 'Transaction was approved'; break;
                case '200': status = 'Transaction was declined by processor'; break;
                case '201': status = 'Do not honor'; break;
                case '202': status = 'Insufficient funds'; break;
                case '203': status = 'Over limit'; break;
                case '204': status = 'Transaction not allowed'; break;
                case '220': status = 'Incorrect payment information'; break;
                case '221': status = 'No such card issuer'; break;
                case '222': status = 'No card number on file with issuer'; break;
                case '223': status = 'Expired card'; break;
                case '224': status = 'Invalid expiration date'; break;
                case '225': status = 'Invalid card security code'; break;
                case '240': status = 'Call issuer for further information'; break;
                case '250': status = 'Pick up card'; break;
                case '251': status = 'Lost card'; break;
                case '252': status = 'Stolen card'; break;
                case '253': status = 'Fraudulent card'; break;
                case '260': status = 'Declined with further instructions available. (See response text)'; break;
                case '261': status = 'Declined-Stop all recurring payments'; break;
                case '262': status = 'Declined-Stop this recurring program'; break;
                case '263': status = 'Declined-Update cardholder data available'; break;
                case '264': status = 'Declined-Retry in a few days'; break;
                case '300': status = 'Transaction was rejected by gateway'; break;
                case '400': status = 'Transaction error returned by processor'; break;
                case '410': status = 'Invalid merchant configuration'; break;
                case '411': status = 'Merchant account is inactive'; break;
                case '420': status = 'Communication error'; break;
                case '421': status = 'Communication error with issuer'; break;
                case '430': status = 'Duplicate transaction at processor'; break;
                case '440': status = 'Processor format error'; break;
                case '441': status = 'Invalid transaction information'; break;
                case '460': status = 'Processor feature not available'; break;
                case '461': status = 'Unsupported card type'; break;
                case '600': status = 'Success'; break;
            }
            return status;
        };
        /**
         * Pagination scopes
         */
        $scope.initPaginator = function(){
            $scope.itemsPerPage = 25;
            $scope.currentPage = 0;
        };

        $scope.initPaginator();
        $scope.items = [];
        $scope.itemsBalance = [];
        $scope.itemsSettlement = [];
        $scope.itemsTransactionDetailByDate = [];

        $scope.range = function() {
            var rangeSize = 5;
            var ret = [];
            var start;
            start = parseInt($scope.currentPage);

            if ($scope.pageCount() < rangeSize){
                rangeSize = $scope.pageCount()+1;
            }

            if ( start > $scope.pageCount()-rangeSize ) {
                start = $scope.pageCount()-rangeSize+1;
            }

            var finish = parseInt(start)+parseInt(rangeSize);
            for (i=start; i<finish; i++) {
                ret.push(i);
            }
            return ret;
        };

        $scope.rangePages = function() {
            var rp_rangeSize = $scope.pageCount();
            var rp_ret = [];
            var rp_start;

            rp_start = $scope.currentPage;
            if ( rp_start > $scope.pageCount()-rp_rangeSize ) {
                rp_start = $scope.pageCount()-rp_rangeSize;
            }

            for (i=rp_start; i<rp_start+rp_rangeSize+1; i++) {
                rp_ret.push(i);
            }
            return rp_ret;
        };

        /**
         * Start and Prev
         */
        $scope.startPage = function() {
            if ($scope.currentPage > 0) {
                $scope.currentPage=0;
            }
            setItems();
        };

        $scope.startPageDisabled = function() {
            setItems();
            return $scope.currentPage === 0 ? 'disabled' : '';
        };

        $scope.prevPage = function() {
            if ($scope.currentPage > 0) {
                $scope.currentPage--;
            }
            setItems();
        };

        $scope.prevPageDisabled = function() {
            return $scope.currentPage === 0 ? 'disabled' : '';
        };

        /**
         * End and Next
         */
        $scope.nextPage = function() {
            if ($scope.currentPage < $scope.pageCount()) {
                $scope.currentPage++;
            }
        };

        $scope.setItems = function(){                      

            switch($scope.currentView) {                
                case 'transaction_detail_by_date':
                    $scope.items = $scope.itemsTransactionDetailByDate;
                    break;
                case 'settlement_details':                       
                    $scope.items = $scope.itemsSettlement;
                    break;
                case 'account_summary':                
                    $scope.items = $scope.itemsAccountSummary;
                    break;
                case 'merchant_activity':
                    $scope.items = $scope.itemsMerchantActivity;
                    break;
                case 'account_balance':
                    $scope.items = $scope.itemsBalance;
                    break;
                case 'ACH_activity':
                    $scope.items = $scope.itemsACHActivity;
                    break;
             }
        };

        $scope.endPage = function() {
            $scope.setItems();
            if ($scope.currentPage < $scope.pageCount()) {
                $scope.currentPage=Math.ceil($scope.items.length/$scope.itemsPerPage)-1;
            }
        };

        $scope.nextPageDisabled = function() {
            return $scope.currentPage === $scope.pageCount() ? 'disabled' : '';
        };

        $scope.endPageDisabled = function() {
            return $scope.currentPage === $scope.pageCount() ? 'disabled' : '';
        };

        /**
         * Page count
         */
        $scope.pageCount = function() {
            $scope.setItems();
            return Math.ceil($scope.items.length/$scope.itemsPerPage)-1;
        };

        $scope.setPage = function(n) {
            $scope.currentPage = n;
            setItems();
        };

        var setItems = function(){
            $scope.setItems();
            $scope.firstItem = 1 + ($scope.currentPage * $scope.itemsPerPage);
            $scope.lastPage = Math.ceil($scope.items.length/$scope.itemsPerPage)-1;
            if ($scope.currentPage === $scope.lastPage){
                $scope.lastItem = $scope.items.length;
            }else{
                $scope.lastItem = $scope.itemsPerPage + ($scope.currentPage * $scope.itemsPerPage);
            }
        };

        $scope.goToPage = function(){
            $scope.currentPage = parseInt($scope.pageSelected);
        };

        var sortByKey = function (array, key, sortBy) {
            if (sortBy==='asc'){ // before
                return array.sort(function(a, b) {
                    var x = a[key];
                    var y = b[key];
                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                });
            }else{
                return array.sort(function(a, b) {
                    var x = a[key];
                    var y = b[key];
                    return ((x > y) ? -1 : ((x < y) ? 1 : 0));
                });
            }
        };


        /**
         * Search transactions
         */
        $scope.reportTransaction = function() {
            
            $scope.treportFormChanged = false;
            $scope.reportProcesed = false;
            $scope.reportProcessorCounter = 1;
            $scope.reportProcessorTotal = 1;
            $scope.reportProcessorName = '';
            $scope.reportProcessorMessage = '';
			$scope.showSeachTransactionsReport = false;
            
            //$scope.showReport = true;
            

            var checkDate = $scope.checkDateRange();
            if (!checkDate) return;

            $scope.initPaginator();
            if ( ($scope.endDate.length===0) || ($scope.startDate.length===0) ){
                $scope.config();
            }

            $scope.items = [];
            var startDate = $scope.startDate;
            startDate = startDate.split('-');
            startDate = startDate[0]+startDate[1]+startDate[2]+'000000';
            var endDate = $scope.endDate;
            endDate = endDate.split('-');
            endDate = endDate[0]+endDate[1]+endDate[2]+'235959';
                
            var boardingId = false;
            var productId = false;
            if ($scope.isAdmin) {
                boardingId = $scope.merchantId.id;
//                productId = $scope.Product_NameSearchTransactions.productName;
                productId = $scope.products.productName;
            } else {
                boardingId = Authentication.user.Boarding_ID;
                productId = $scope.productId.productName;
            }
            $scope.boardingId = boardingId;
            $scope.productName = productId;

            PaymentCompleteService.get({
//                boardingId: Authentication.user.Boarding_ID,
//                productId: $scope.productId.productName
                boardingId: boardingId,
                productId: productId
            }).$promise.then(
                function(result){
                    var results = result.Response[0];

                    var transactionReport = new Nmitransactions({
                        boardingId:       boardingId,//Authentication.user.Boarding_ID,
                        originalId:       true,
                        transactionId:    $scope.transactionId,
                        actionType:       $scope.actionType,
                        orderId:          $scope.orderId,
                        condition:        $scope.condition,
                        firstName:        $scope.firstName,
                        lastName:         $scope.lastName,
                        customerVaultId:  $scope.customerVaultId,
                        merchantIdent:    $scope.merchantIdent,
                        amountFrom:       $scope.amountFrom,
                        amountTo:         $scope.amountTo,
                        last4ofCC:        $scope.last4ofCC,
                        startDate:        startDate,
                        endDate:          endDate
                    });

                    if ($scope.isAdmin) {
                        if (productId === 'all' && boardingId === 'all' && (typeof($scope.merchantIdent) === 'undefined' || $scope.merchantIdent === '')) {
                            transactionReport.processorId = 'all';
                            transactionReport.type = 'treport_all';
                            $scope.reportProcessorName = 'all';
                        } else {
                            transactionReport.productId = productId;
                            transactionReport.type = 'treport';
                            $scope.reportProcessorName = results.Description;
                        }
                    } else {
                        if (productId === 'all') {
                            transactionReport.processorId = 'all';
                            transactionReport.type = 'treport_all';
                            $scope.reportProcessorName = 'all';
                        } else {
                            transactionReport.processorId = results.Processor_ID;
                            transactionReport.type = 'treport';
                            $scope.reportProcessorName = results.Description;
                        }
                    }

                    Nmitransactions.get(transactionReport).$promise.then(
                        function(results){

                            if (Object.keys(results).length > 2){ // just have $promise and $resolved, but not nm_response
                                if(results.nm_response.transaction !== 'undefined'){
                                    if (results.nm_response.transaction.hasOwnProperty('transaction_id')){
                                        var parseResult = [];
                                        results.nm_response.transaction.orderStatus = getStatusString(results.nm_response.transaction.action.response_code.$t);
                                        parseResult.push(results.nm_response.transaction);
                                        $scope.items = parseResult;
                                    }else{
                                        /**
                                         * Set consistency in Time field
                                         */
                                        $scope.reportProcessorMessage = 'Processing ' + Object.keys(results.nm_response.transaction).length + ' transactions, please wait...';
                                        results.nm_response.transaction.forEach(function (list) {
                                            if (Authentication.user.Roles === 'user') {
                                                list.Boarding_ID = Authentication.user.Boarding_ID;
                                            } else {
                                                list.Boarding_ID = list.boarding_id.$t;
                                            }
                                            if (typeof list.action.date === 'undefined'){
                                                list.orderTime = list.action[0].date.$t;
                                                list.orderAmount = list.action[0].amount.$t;
                                                list.orderType = list.action[0].action_type.$t;
                                                list.orderStatus = getStatusString(list.action[0].response_code.$t);
//                                                list.tax = list.tax.$t;
//                                                list.shipping = list.shipping.$t;
//                                                list.orderMerchantName = list.merchant_name.$t;
//                                                list.orderMerchantId = list.merchant_id.$t;
//                                                list.orderTransactionId = list.transaction_id.$t
                                            }else{
                                                list.orderTime = list.action.date.$t;
                                                list.orderAmount = list.action.amount.$t;
                                                list.orderType = list.action.action_type.$t;
                                                list.orderStatus = getStatusString(list.action.response_code.$t);
//                                                list.tax = list.tax.$t;
//                                                list.shipping = list.shipping.$t;
//                                                list.orderMerchantName = list.merchant_name.$t;
//                                                list.orderMerchantId = list.merchant_id.$t;
//                                                list.orderTransactionId = list.transaction_id.$t
                                            }
                                            list.tax = list.tax.$t;
                                            list.shipping = list.shipping.$t;
                                            list.orderMerchantName = list.merchant_name.$t;
                                            list.orderMerchantId = list.merchant_id.$t;
                                            list.orderTransactionId = list.transaction_id.$t;
                                            list.orderCustomer = list.first_name.$t;
                                            list.orderCCNumber = list.cc_number.$t;

                                            for (var i_proc = 0; i_proc < Object.keys(result.Response).length; i_proc++) {
                                                if (result.Response[i_proc].Processor_ID === list.processor_id.$t) {
                                                    list.productDescription = result.Response[i_proc].Description;
                                                    list.Product_ID = result.Response[i_proc].Product_ID;
                                                }
                                            }
                                        });

                                        /**
                                         * Order By Time field
                                         *        - DESC: if no 'transaction id' is entered (latest is shown first)
                                         *        - ASC: if 'transaction id' is entered - this allows for the sale to be first in the list and then any subsequent transactions will follow
                                         */
                                        if (!$scope.transactionId || $scope.transactionId === '') {
                                            resultsOrderByTime = sortByKey(results.nm_response.transaction, 'orderTime', 'desc');
                                        } else {
                                            resultsOrderByTime = sortByKey(results.nm_response.transaction, 'orderTime', 'asc');
                                        }

                                        $scope.items = resultsOrderByTime;
                                        if ($scope.items.length > 0) {$scope.showSeachTransactionsReport = true;}
                                        
                                        $scope.reportProcessorMessage = '';
                                        $scope.reportProcesed = true;
                                    }
                                    $scope.noResults = false;
                                    
                                    // get MDF values 
                                    if (productId !== 'all' || $scope.sampleProductCategories[0].products.length === 2) { 
                                        $scope.items.forEach(function(item) {
                                            MdfService.get({historyId: item.id}).$promise.then(function(mdfs) {
                                                mdfs.response.forEach(function(mdf) {
                                                    switch (mdf.Number) {
                                                        case "1":
                                                            item.MDF1 = mdf.Value;
                                                            break;
                                                        case "2":
                                                            item.MDF2 = mdf.Value;
                                                            break;
                                                        case "3":
                                                            item.MDF3 = mdf.Value;
                                                            break;
                                                        case "4":
                                                            item.MDF4 = mdf.Value;
                                                            break;
                                                        case "5":
                                                            item.MDF5 = mdf.Value;
                                                            break;
                                                        case "6":
                                                            item.MDF6 = mdf.Value;
                                                            break;
                                                        case "7":
                                                            item.MDF7 = mdf.Value;
                                                            break;
                                                        case "8":
                                                            item.MDF8 = mdf.Value;
                                                            break;
                                                        case "9":
                                                            item.MDF9 = mdf.Value;
                                                            break;
                                                        case "10":
                                                            item.MDF10 = mdf.Value;
                                                            break;
                                                        case "11":
                                                            item.MDF11 = mdf.Value;
                                                            break;
                                                        case "12":
                                                            item.MDF12 = mdf.Value;
                                                            break;
                                                        case "13":
                                                            item.MDF13 = mdf.Value;
                                                            break;
                                                        case "14":
                                                            item.MDF14 = mdf.Value;
                                                            break;
                                                        case "15":
                                                            item.MDF15 = mdf.Value;
                                                            break;
                                                        case "16":
                                                            item.MDF16 = mdf.Value;
                                                            break;
                                                        case "17":
                                                            item.MDF17 = mdf.Value;
                                                            break;
                                                        case "18":
                                                            item.MDF18 = mdf.Value;
                                                            break;
                                                        case "19":
                                                            item.MDF19 = mdf.Value;
                                                            break;
                                                        case "20":
                                                            item.MDF20 = mdf.Value;
                                                            break;
                                                    }
                                                });
                                                
                                            });
                                            switch (item.productDescription) {
                                                case 'Video Checkout':
                                                    $scope.MDF1_label = 'Occupation';
                                                    $scope.MDF2_label = 'Corporation';
                                                    $scope.MDF3_label = 'Employer';
                                                    break;
                                                case '#Twt2Pay':
                                                    $scope.MDF1_label = 'Product Hashtag';
                                                    $scope.MDF2_label = 'First Product Hashtag';
                                                    $scope.MDF3_label = 'processorId of First Product';
                                                    break;
                                            }
                                        });
                                    }
                                }else{
                                    $scope.reportProcesed = true;
                                    $scope.noResults = true;
                                }
                            }else{
                                $scope.reportProcesed = true;
                                $scope.noResults = true;
                            }
                        },
                        function(err){
                            console.log(err);
                        }
                    );
                },
                function(err){

                }
            );
        };
        /**
         * Copy all the values from Billing to Shipping
         */
        $scope.sameAsBilling = function(){
            if ($scope.sameBilling !== true){
                $scope.firstNameShipping = $scope.firstNameCard;
                $scope.lastNameShipping = $scope.lastNameCard;
                $scope.companyShipping = $scope.companyCard;
                $scope.countryShipping = $scope.countryCard;
                $scope.addressShipping = $scope.addressCard;
                $scope.addressContShipping = $scope.addressContCard;
                $scope.cityShipping = $scope.cityCard;
                $scope.stateProvinceShipping = $scope.stateProvinceCard;
                $scope.stateProvinceShippingText = $scope.stateProvinceCardText;
                $scope.zipPostalCodeShipping = $scope.zipPostalCodeCard;
                $scope.emailAddressShipping = $scope.emailAddressCard;
                
                $scope.verifyCountryShipping();
            }
        };

        $scope.setMerchant = function(arrayMerchantsProducts) {
            //angular.element(document).ready(function () {
            $scope.sampleProductCategories = arrayMerchantsProducts;    
            
            // set the dropdown to default to the first item in the array so the first item in dropdown is not blank            
            $scope.Product_NameABR_Merchant = arrayMerchantsProducts[0].products[0];
            $scope.Product_NameSearchTransactions = arrayMerchantsProducts[0].products[0];
            $scope.Product_NameUserMAR = arrayMerchantsProducts[0].products[0];
            
            var processorsArray = [];
            if (Authentication.user.Roles === 'user') {
                $scope.productId = arrayMerchantsProducts[0].products[0];
                // added setMerchantDropdown to allow Merchant to view pending line items  MC-645
                $scope.setMerchantDropdown(arrayMerchantsProducts);
                //arrayMerchantsProducts[0].products[0];
                /**
                 * Looking for reserves in case of get account balance report, in mike's table
                 */
                for (var j = 0; j < Object.keys(arrayMerchantsProducts[0].products).length; j++) {
                    if (arrayMerchantsProducts[0].products[j].Processor_ID !== '') processorsArray.push(arrayMerchantsProducts[0].products[j].Processor_ID);
                }                  
                                      
            } else {            
                $scope.setMerchantDropdown(arrayMerchantsProducts);
                /**
                 * Looking for reserves in case of get account balance report, in mike's table
                 */

                for (var j1 = 0; j1 < Object.keys(arrayMerchantsProducts).length; j1++) {
                    for (var k = 0; k < Object.keys(arrayMerchantsProducts[j1].products).length; k++) {

                        if (arrayMerchantsProducts[j1].products[k] !== undefined) {
                            if (arrayMerchantsProducts[j1].products[k].Processor_ID !== '') processorsArray.push(arrayMerchantsProducts[j1].products[k].Processor_ID);
                        }
                    }
                }
                
                // Austin - set Product Name dropdown to 'All Products'
                $scope.Product_NameMAR = $scope.merchantId.products[0]; 
                $scope.Product_NameUserMAR = $scope.merchantId.products[0]; 
                $scope.Product_NameABR = $scope.merchantId.products[0];
                $scope.Product_NameACH = $scope.merchantId.products[0];  
            }
            counterProcessors = 0;
            processorsReserves = [];
            //$scope.processorsWrap(processorsArray);
            //});                                   
            
        };

        $scope.processorsWrap = function(processorsArray){
            var processorsLength = Object.keys(processorsArray).length;
            if (processorsLength>counterProcessors){
                $scope.processorsGetReserve(processorsArray[counterProcessors], processorsArray);
            }
        };

        $scope.processorsGetReserve = function(processorId, processorsArray){
            ReserveService.get({
                processorId: processorId
            }, function(reserveResult) {
                processorsReserves.push({'processorId': processorId, 'reserve': reserveResult.Response[0].amount});
                counterProcessors+=1;
                $scope.processorsWrap(processorsArray);
            });
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
        
        $scope.validateDate = function(date) {
            var hyphenated_date = $scope.hyphenize_date(date);
            var yy = hyphenated_date.substr(0, 4);
            var mm = hyphenated_date.substr(5, 2) - 1;
            var dd = hyphenated_date.substr(8, 2);
//console.log(hyphenated_date, yy,mm,dd)
            var calculated_date = new Date(yy, mm, dd, 0, 0, 0, 0);
            var calc_yy = calculated_date.getFullYear();
            var calc_mm = calculated_date.getMonth();
            var calc_dd = calculated_date.getDate();
//console.log(calculated_date, calc_yy,calc_mm,calc_dd)
            if ((parseInt(yy) !== parseInt(calc_yy)) || (parseInt(mm) !== parseInt(calc_mm)) || (parseInt(dd) !== parseInt(calc_dd)))  {
                return false;
            }
            return true;
        };

        $scope.itemFeeRecord = function() {

            $scope.alerts = [];
            var transactionExist = validateIfTransactionExist($scope.transaction_id, $scope.products.processor);
            var description,
                date,
                transactionFeeObject = {};
            var rules = true;
            transactionExist.then(function(line_itemsResults) {
                if (($scope.itemType !== 'misc' && $scope.itemType !== 'other')&& !$scope.validateDate($scope.date)) {
                    $scope.alerts.push({type: 'danger', msg: 'The date entered is not the correct format'});
                    rules = false;
                }
                if ((Object.keys($scope.merchantIdModal).length === 0) || ($scope.merchantIdModal.value === 'all')) {
                    $scope.alerts.push({type: 'danger', msg: 'You must select a merchant'});
                    rules = false;
                }
                if ($scope.products.processor === '' || $scope.products.processor === undefined) {
                    $scope.alerts.push({type: 'danger', msg: 'You must select a product'});
                    rules = false;
                }
                if (typeof $scope.itemType === 'undefined') {
                    $scope.alerts.push({type: 'danger', msg: 'You must select an item adjustment'});
                    rules = false;
                }
                if ((typeof $scope.feeAmount === 'undefined') || ($scope.feeAmount === '')) {
                    if (/* $scope.itemType !== 'ach_reject' && */ $scope.itemType !== 'retrieval_fee') {
                        $scope.alerts.push({type: 'danger', msg: 'You must enter a valid amount'});
                        rules = false;
                    }
                }
                if ($scope.itemType === 'misc' || $scope.itemType === 'other' || $scope.itemType === 'ach_reject') {
                    if (typeof $scope.payment === 'undefined') {
                        $scope.alerts.push({type: 'danger', msg: 'You must select either a credit or debit'});
                        rules = false;
                    }
                }
                if (typeof $scope.description === 'undefined') {
                    if ($scope.itemType !== 'ach_reject' && $scope.itemType !== 'reserve_adjustment') {
                        $scope.alerts.push({type: 'danger', msg: 'You must enter a description'});
                        rules = false;
                    }
                }
                if (($scope.itemType !== 'misc') && ($scope.itemType !== 'other') && ($scope.itemType !== 'ach_reject') && ($scope.itemType !== 'reserve_adjustment') && ($scope.itemType !== 'reserve_increase')) {
                    if (typeof $scope.transaction_id === 'undefined') {
                        $scope.alerts.push({type: 'danger', msg: 'You must enter a valid transaction ID'});
                        rules = false;
                    } else {
                        transactionFeeObject = line_itemsResults;
                        if (typeof line_itemsResults === 'undefined' || Object.keys(line_itemsResults).length === 0) {
                            $scope.alerts.push({type: 'danger', msg: 'Transaction ID not found'});
                            rules = false;
                        } else {
                            if (parseFloat($scope.feeAmount) > parseFloat(transactionExist.amount)) {
                                transactionFeeObject = line_itemsResults;
                                //var msn = 'Amount cannot be greater than ' + transactionExist.amount;
                                //$scope.alerts.push({ type: 'danger', msg: msn });
                                //rules = false;
                            }
                        }
                    }
                } else {
                    if ((typeof $scope.feeAmount === 'undefined') && ($scope.itemType !== 'misc')&& ($scope.itemType !== 'other')) {
                        $scope.alerts.push({type: 'danger', msg: ' You must select an amount'});
                        rules = false;
                    }
                }
                if (typeof $scope.date === 'undefined') {
                    if (($scope.itemType !== 'misc') && ($scope.itemType !== 'other')) {
                        $scope.alerts.push({type: 'danger', msg: 'You must select an occurrence date'});
                        rules = false;
                    }
                }
                /*
                 This is only for chargebacks
                 */
                if (rules) {
                    line_itemsArray = [];
                    // looking for all Time Fees necessary for know if before a Transaction had changeback
                    var promiseline_items = findLineItems();
                    promiseline_items.then(function (line_itemsResults) {
                        line_itemsArray = line_itemsResults;
                        delete line_itemsArray.$promise;
                        delete line_itemsArray.$resolved;

                        //flagGeneral = true;
                        flagGeneral = false;
                        var counterChargebacks = 0; // count how many chargeback exist
                        var sumFeeAmount = 0;       // sum amounts
                        var sumReversalAmount = 0;  // sum amounts
                        var chargebackExist = validateIfChargebackExist(line_itemsArray, $scope.transaction_id);

                        if (Object.keys(chargebackExist).length === 0) {
                            flagGeneral = false;
                        } else {
                            chargebackExist.forEach(function (item) {
                                if (item.Fee_Name === 'chargeback') {
                                    //flagGeneral = false;
                                    counterChargebacks += 1;
                                    sumFeeAmount = parseFloat(sumFeeAmount) + parseFloat(item.Fee_Amount);
                                    flagGeneral = true;
                                } else {
                                    if (item.Fee_Name === 'chargeback_reversal') {
                                        sumReversalAmount = parseFloat(sumReversalAmount) + parseFloat(item.Fee_Amount);
                                    }
                                }
                            });
                        }

                        var recordingNewFee = false;
                        var dateString;

                        if (typeof chargebackExist === 'undefined') {
                            recordingNewFee = true; // transaction_id no exist = can record it
                        } else {
                            if ($scope.itemType === 'chargeback_reversal') {
                                if (flagGeneral) {
                                    recordingNewFee = true; // fee = chargeback, then only can accept chargeback reverse
                                } else {
                                    $scope.alerts.push({
                                        type: 'danger',
                                        msg: 'The Chargeback Reversal cannot be applied unless there is an existing Chargeback'
                                    });
                                    recordingNewFee = false;
                                }
                                var totalChargebacksReversalsRemaining = parseFloat(sumFeeAmount) - parseFloat(sumReversalAmount);
                                if (totalChargebacksReversalsRemaining - $scope.feeAmount < 0) {
                                    if (totalChargebacksReversalsRemaining > 0) {
                                        var msn = 'Amount cannot be greater than ' + totalChargebacksReversalsRemaining.toFixed(2);
                                        $scope.alerts.push({type: 'danger', msg: msn});
                                    } else {
                                        $scope.alerts.push({
                                            type: 'danger',
                                            msg: 'You have already reached the maximum amount of chargebacks reversal allowed for this transaction'
                                        });
                                    }
                                    recordingNewFee = false;
                                } else {

                                }
                            } else {
                                if ($scope.itemType === 'chargeback') {
                                    $scope.date = $scope.hyphenize_date($scope.date);
                                    if (parseInt(transactionFeeObject.time.substr(0, 8)) > parseInt(dateString)) {
                                        $scope.alerts.push({
                                            type: 'danger',
                                            msg: 'The occurrence date cannot be dated before the transaction date'
                                        });
                                        recordingNewFee = false;
                                    } else {
                                        if (counterChargebacks >= 0 && counterChargebacks < 4) {
                                            var totalChargebacksAmount = parseFloat(sumFeeAmount) + parseFloat($scope.feeAmount);
                                            var totalRemaining = parseFloat(transactionFeeObject.amount) - parseFloat(sumFeeAmount);

                                            if (totalChargebacksAmount > transactionFeeObject.amount) {
                                                var msn1 = 'Amount cannot be greater than ' + totalRemaining.toFixed(2);
                                                $scope.alerts.push({type: 'danger', msg: msn1});
                                            } else {
                                                recordingNewFee = true;
                                            }
                                        } else {
                                            $scope.alerts.push({
                                                type: 'danger',
                                                msg: 'You have already reached the amount of chargebacks allowed for this transaction'
                                            });
                                        }
                                    }
                                } else {
                                    recordingNewFee = true;
                                }

                            }
                        }

                        var promiseReserve_balance = findReserveAmount();
                        promiseReserve_balance.then(function (reserve_balanceResults) {
                            if ($scope.itemType === 'reserve_adjustment') {
                                if (reserve_balanceResults.response[0] !== undefined) {
                                    if (($scope.feeAmount > reserve_balanceResults.response[0].Reserve_Amount) && (reserve_balanceResults.response[0].Reserve_Amount !== 0)) {
                                        var msn = 'Amount cannot be greater than ' + reserve_balanceResults.response[0].Reserve_Amount;
                                        $scope.alerts.push({type: 'danger', msg: msn});
                                        recordingNewFee = false;
                                    } if (reserve_balanceResults.response[0].Reserve_Amount === 0) {
                                        var msn1 = 'There are no funds in merchants reserve account';
                                        $scope.alerts.push({type: 'danger', msg: msn1});
                                        recordingNewFee = false;
                                    } else {
                                        recordingNewFee = true;
                                    }
                                }
                            }
                            dateString = '';
                            var yy;
                            var mm;
                            var dd;
                            var mockDate;
                            if ($scope.date !== undefined) {
                                dateString = $scope.hyphenize_date($scope.date);
                                yy = dateString.substr(0, 4);
                                mm = dateString.substr(5, 2);
                                dd = dateString.substr(8, 2);
                                dateString = yy + '' + mm + '' + dd;
                            } else {
                                mockDate = new Date();
                                dateString = mockDate.getFullYear() + '' + ("0" + (mockDate.getMonth() + 1)).slice(-2) + '' + ("0" + mockDate.getDate()).slice(-2);
                            }
                            if (recordingNewFee) {
                                if (($scope.itemType === 'ach_reject')) {
//                                    $scope.payment = 'debit';
                                } else if ($scope.itemType === 'chargeback_reversal') {
                                    $scope.payment = 'credit';
                                } else if ($scope.itemType === 'chargeback') {
                                    $scope.payment = 'debit';
                                } else if ($scope.itemType === 'retrieval_fee') {
                                    $scope.payment = 'debit';
                                } else if ($scope.itemType === 'reserve_adjustment') {
                                    $scope.payment = 'credit';
                                } else if ($scope.itemType === 'reserve_increase') {
                                    $scope.payment = 'debit';
                                } else if ($scope.itemType === 'misc') {
                                } else if ($scope.itemType === 'other') {
                                }
                                var promiseItemFees = setItemFees($scope.merchantIdModal.id, $scope.products.processor, $scope.itemType, $scope.feeAmount, $scope.payment, $scope.description, $scope.transaction_id, dateString);
                                promiseItemFees.then(function (itemFeesResults) {
                                    if (itemFeesResults.Response) {
                                     var infoarray = {
                                        'title': 'Add a Line Item',
                                        'message': 'Line Item Added',
                                        'accept': '',
                                        'deny': 'Ok'
                                    };
                                    $scope.confirmationModal('', infoarray);
                                        $scope.closeModal();

                                        line_itemsArray = [];
                                        var promiseLine_items = findLineItems();
                                        promiseLine_items.then(function (line_itemsResults) {

                                            line_itemsArray = line_itemsResults;
                                            delete line_itemsArray.$promise;
                                            delete line_itemsArray.$resolved;
                                        });

                                    } else {
                                        $scope.alerts.push({type: 'danger', msg: 'An error has occurred'});
                                    }
                                });
                            }
                        });
                    });
                }
            });
        };

        $scope.openHelpModal = function () {

            var modalInstance = $modal.open({
                templateUrl: 'modules/creditcard/views/modalhelp.client.view.html',
                controller: 'ModalInstanceCtrl',
                size: 'lg',
                resolve: {
                    items: function () {
                        //return $scope.items;
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {});
        };

        $scope.openFeeModal = function () {
            //$scope.items = $scope.sampleProductCategories;
            $scope.items = {'merchants_products': $scope.sampleProductCategories, 'all_transactions':allTransactions, 'transaction_id': '', 'description': '', 'occurrenceDateModal': '', 'merchantId': $scope.merchantId, 'productId': $scope.Product_ID};
            var modalInstance = $modal.open({
                templateUrl: 'modules/creditcard/views/modalfee.client.view.html',
                controller: 'ModalInstanceCtrl',
                size: 'lg',
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {});
        };

        $scope.setView = function(view){
            $scope.reportProcessorCounter = 0;
            $scope.reportProcessorTotal = 0;
            $scope.reportProcessorName = '';
            $scope.reportProcessorMessage = '';
            $scope.reportProcesed = true;

            /* clean report scopes */
            $scope.itemsTransactionDetailByDate = [];
            $scope.itemsSettlement = [];
            $scope.itemsAccountSummary = [];
            $scope.itemsMerchantActivity = [];
            $scope.itemsACHActivity = [];
            $scope.itemsBalance = [];
            /* define current view */
            $scope.currentView = view;
            $scope.getMerchantProductDropdown();
            if (($scope.currentView==='Transaction Report') || ($scope.currentView==='transaction_detail_by_date') || ($scope.currentView==='merchant_activity') || ($scope.currentView==='account_balance') || ($scope.currentView==='settlement_details') || ($scope.currentView==='account_summary') || ($scope.currentView==='ACH_activity') || ($scope.currentView==='recurring_plan') || ($scope.currentView==='audit_recurr_plan_activity') || ($scope.currentView==='Transaction Snapshot')){
                disbursementDatesArray = [];
                var promiseDisbursementDates = findDisbursementDates();
                promiseDisbursementDates.then(function(disbursementDatesResults){
                    disbursementDatesArray = disbursementDatesResults;
                    delete disbursementDatesArray.$promise;
                    delete disbursementDatesArray.$resolved;
                    line_itemsArray = [];
                    // looking for all Time Fees necessary for know if before a Transaction had changeback
                    var promiseLine_items = findLineItems();
                    promiseLine_items.then(function(line_itemsResults) {
                        line_itemsArray = line_itemsResults;
                        delete line_itemsArray.$promise;
                        delete line_itemsArray.$resolved;
                        var promiseProcessor = findMerchantID();
                        promiseProcessor.then(function(merchantResults) {

                            $scope.merchantResults = merchantResults;
                            var promiseFees = findPaymentplans();
                            promiseFees.then(function(paymentResults) { // Payment_plans table
                                var promiseProducts = findProducts();
                                promiseProducts.then(function(productsResults) { // Products name
                                    if (Authentication.user.Roles !== 'user') {                                        
                                        productsList = productsResults;

                                        delete merchantResults.$promise;
                                        delete merchantResults.$resolved;

                                        delete paymentResults.$promise;
                                        delete paymentResults.$resolved;

                                        delete productsResults.$promise;
                                        delete productsResults.$resolved;

                                        $scope.products = productsResults;

                                        i = 0;

                                        Object.keys(merchantResults).forEach(function (keyMerchant) {
                                            jsonMerchantsProducts = {};
                                            var m = merchantResults[keyMerchant];
                                            var products = [];
                                            //var merchants = [];
                                            Object.keys(paymentResults).forEach(function (keyPayment) {
                                                //  if (Authentication.user.Roles != "user" || Authentication.user.Boarding_ID == m.id) {
                                                var p = paymentResults[keyPayment];                                                
                                                if ((p.Boarding_ID === m.id) && (p.Processor_ID !== null) && (p.Processor_ID !== '') && (m.Merchant_Name !== '')) {
                                                    jsonMerchantsProducts.id = m.id;
                                                    jsonMerchantsProducts.value = m.id;
                                                    jsonMerchantsProducts.name = m.Merchant_Name;
                                                    Object.keys(productsResults).forEach(function (keyProduct) {
                                                        var n = productsResults[keyProduct];
                                                        if (n.id === p.Product_ID) {// productid
                                                            if (p.Product_ID === 4){
                                                                $scope.reportProcessorName = 'MojoPay';
                                                            }else{
                                                                if (p.Product_ID === 2){
                                                                    $scope.reportProcessorName = 'Twt2pay';
                                                                }else{
                                                                    if (p.Product_ID === 3){
                                                                        $scope.reportProcessorName = 'VideoCheckout';
                                                                    }else{
                                                                        if (p.Product_ID === 5){
                                                                            $scope.reportProcessorName = 'Marketplace';
                                                                        }else{
                                                                            $scope.reportProcessorName = 'Unknown product';//feesResults[counterMerchantActivity].Processor_ID;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if (Object.keys(products).length === 0) {
                                                                // inserting All products
                                                                products.push({
                                                                    'name': 'All Products',
                                                                    'processor': '',
                                                                    'boarding_id': p.Boarding_ID,
                                                                    'productName': 'all'
                                                                });
                                                            }
                                                            products.push({
                                                                'name': $scope.reportProcessorName,// + ' ' + p.Processor_ID,
                                                                'processor': p.Processor_ID,
                                                                'boarding_id': p.Boarding_ID,
                                                                'productName': p.Product_ID
                                                            });
                                                        }
                                                    });
                                                }
                                                //  }
                                            });

                                            if (products.length > 0) {
                                                jsonMerchantsProducts.products = products;                                                
                                            }
                                            
                                            if (Object.keys(arrayMerchantsProducts).length === 0) {
                                                var tmpProduct = [];
                                                var count = 0;
                                                for (var k = 0; k < Object.keys(productsResults).length; k++) {
                                                    if (Object.keys(tmpProduct).length === 0) {
                                                        tmpProduct.push({
                                                            'boarding_id': 'all',
                                                            'name': 'All Products',
                                                            'processor': '',
                                                            'productName': 'all'
                                                        });
                                                    }
                                                    count += 1;
                                                    if (productsResults[k].id > 1) {
                                                        tmpProduct.push({
                                                            'boarding_id': 'all',
                                                            'name': productsResults[k].Description,
                                                            'processor': '',
                                                            'productName': productsResults[k].id
                                                        });
                                                    }
                                                    if (count === Object.keys(productsResults).length) {
                                                        arrayMerchantsProducts.push({
                                                            'id': 'all',
                                                            'name': 'All Merchants',
                                                            'value': 'all',
                                                            'products': tmpProduct
                                                        });
                                                    }
                                                }
                                            }
                                            if (products.length > 0) {
                                                arrayMerchantsProducts.push(jsonMerchantsProducts);
                                            }
                                            i += 1;
                                            if (i === Object.keys(merchantResults).length) {
                                                $scope.setMerchant(arrayMerchantsProducts);
                                            }
                                        });
                                    }
                                });
                            });
                        });
                    });
                });
            }
            
            
            
        };
        
        $scope.getMerchantProductDropdown = function(){
            if (Authentication.user.Roles === 'user'){
                var boarding= Authentication.user.Boarding_ID;
                PaymentCompleteService.get({boardingId:boarding,productId:'all'}).$promise.then(function(payment){
                    payment = payment.Response;
                    Nmitransactions.get({type: 'getMerchantInfo',boardingId: boarding}).$promise.then(
                        function(boardingPlan){
                            var products = [],
                                merchant = {};

                            for(var j = 0; j < Object.keys(payment).length; j++) {
                                if (Object.keys(products).length === 0) {
                                    // inserting All products
                                    products.push({
                                        'name': 'All Products',
                                        'processor': '',
                                        'boarding_id': boarding,
                                        'productName': 'all'
                                    });
                                }
                                if(payment[j].Product_ID !== 1){
                                    products.push({
                                        'name': payment[j].Description,
                                        // why is this a space?
                                        //'processor': ' ',
                                        'processor': payment[j].Processor_ID,
                                        'boarding_id': boarding,
                                        'productName': payment[j].Product_ID
                                    });
                                }
                            }
                            arrayMerchantsProducts.push({'id':boarding, 'name':boardingPlan.Merchant_Name , 'value': boarding, 'products':products});
                            $scope.setMerchant(arrayMerchantsProducts);
                        },
                        function(err){
                            console.log(err);
                        }
                    );
                });
            }
        };
        $scope.setMerchantDropdown = function(array){
            if (array!==undefined){
                
                //alert('set merchantID');
                                
                //  <div ng-show="isAdmin && merchantId.products.length == 2" class="col-md-5">
                //      <div class="form-group">  
                //         <label class="control-label">Product Name</label>      
                //         <div ng-model="Product_NameMAR"
                //              ng-click="Product_NameChangeMAR()"
                //              class="form-control"> 
                //              {{ merchantId.products[1].name }}                        
                //          </div>
                //      </div>    
                //  </div>                     
                $scope.merchantId = array[0];
                $scope.products = array[0].products[0];   
            }
        };
        $scope.updateMerchantDropdown = function(array){
            if (array.products[0]!==undefined){
                $scope.products = array.products[0];
            }
        };
        /**
         * Deal with dates
         */
        $scope.config = function(){
            $scope.items = [];
            
            var CurrentDate = new Date(),
                endDate = new Date(),
                endDateYesterday = new Date(),
                oneDate = new Date(),
                maxDate = new Date();
                
            endDate.setDate(endDate.getDate());
            endDateYesterday.setDate(endDateYesterday.getDate() -1); // #gb
            oneDate.setDate(oneDate.getDate() -1); // #gb
            maxDate.setDate(maxDate.getDate() -1); // #gb
            
            CurrentDate.setMonth(CurrentDate.getMonth() - 1);
            $scope.startDate = $filter('date')(CurrentDate, 'yyyy-MM-dd');            
            $scope.endDate = $filter('date')(endDate, 'yyyy-MM-dd');
            $scope.endDateYesterday =  $filter('date')(endDateYesterday, 'yyyy-MM-dd');
            $scope.oneDate = $filter('date')(oneDate, 'yyyy-MM-dd');
            $scope.maxDate = $filter('date')(maxDate, 'yyyy-MM-dd');
            /**
             * Looking for merchants in case of Admin
             * or
             * my boarding_id in case of Merchant Account
             */
            var promiseProcessor = findMerchantID();
            promiseProcessor.then(function(merchantResults) {
                $scope.merchants = merchantResults;
            });

        };
        $scope.today = function(){
            $scope.items = [];
            var CurrentDate = new Date();
            $scope.startDate = $filter('date')(CurrentDate, 'yyyy-MM-dd');
            $scope.endDate = $filter('date')(CurrentDate, 'yyyy-MM-dd');
            $scope.endDateYesterday = $filter('date')(CurrentDate, 'yyyy-MM-dd');
        };
        $scope.thisWeek = function(){
            $scope.items = [];
            var CurrentDate = new Date(),
                endDate = new Date();
            endDate.setDate(endDate.getDate());
            CurrentDate.setDate(CurrentDate.getDate() - 7);
            $scope.startDate = $filter('date')(CurrentDate, 'yyyy-MM-dd');
            $scope.endDate = $filter('date')(endDate, 'yyyy-MM-dd');
        };
        $scope.lastMonth = function(){
            $scope.items = [];
            var lastMonth = new Date(),
                beforeMonth = new Date();
            lastMonth.setMonth(lastMonth.getMonth() - 1);
            beforeMonth.setMonth(beforeMonth.getMonth() - 2);
            $scope.startDate = $filter('date')(beforeMonth, 'yyyy-MM-dd');
            $scope.endDate = $filter('date')(lastMonth, 'yyyy-MM-dd');
        };
        $scope.lastYear = function(){
            $scope.items = [];
            var lastYear = new Date();
            lastYear.setMonth(lastYear.getMonth() - 12);
            $scope.startDate = $filter('date')(lastYear, 'yyyy-MM-dd');
            $scope.endDate = $filter('date')(Date.now(), 'yyyy-MM-dd');
        };
        $scope.fillRecursiveDates = function(merchantId, merchantName){
            var deferred = $q.defer();
            var counter = 0;
            if (Object.keys(masArray).length===0){
                deferred.resolve(false);
            }else{
                for (var i = 0; i < Object.keys(masArray).length; i++) {
                    var yy = masArray[i].date.substr(0,4);
                    var mm = masArray[i].date.substr(5,2);
                    var dd = masArray[i].date.substr(8,2);
                    var date = yy+''+mm+''+dd;
                    transactionObject.push({
                        merchantId: merchantId,
                        merchantName: merchantName,
                        time: date,
                        settle: 0,
                        refund: '',
                        discountFee: '',
                        transactionFee: '',
                        chargebackFee: '',
                        chargebackreversalFee: '',
                        retrievalFee: '',
                        achrejection: '',
                        isLine_items: {},
                        productName: 'flag'
                    });
                    counter+=1;
                    if (counter===Object.keys(masArray).length){
                        deferred.resolve(true);
                    }
                }
            }
            return deferred.promise;
        };

        function formatDate(date) {

            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('');
            //return year+''+month+''+day;
        }

        $scope.holdDisbursementDate = function(transactionsHeld, disbursementDateCalculation){

            var counter = 0;
            var flagTmp = '';
            if (transactionsHeld.length>0){ // ===1
                $scope.flagTmp = 0;
                return 0;
            }else{
                transactionsHeld.forEach(function(items, key){
                    if (parseInt(items.time) === parseInt(disbursementDateCalculation)){
                        $scope.flagTmp = items.balance;
                        return flagTmp;
                    }
                    counter+=1;
                    if (parseInt(transactionsHeld.length) === parseInt(counter)){
                        var yy = String(disbursementDateCalculation).substr(0, 4);
                        var mm = String(disbursementDateCalculation).substr(4, 2);
                        var dd = String(disbursementDateCalculation).substr(6, 2);
                        var dateString = mm+'/'+dd+'/'+yy;
                        var dateBefore = new Date(dateString);
                        dateBefore.setDate(dateBefore.getDate()-1);

                        $scope.holdDisbursementDate(transactionsHeld, formatDate(dateBefore));
                    }

                });
            }
        };
        $scope.isdisbursementdate = function(processor,paymentdate){
            var deferred = $q.defer();
            if (processor!==''){
                MasService.get({
                        masprocessorid: processor,
                        paymentdate:paymentdate
                    }, function(results){
                        deferred.resolve(results);
                    },
                    function(err){
                        deferred.resolve(err);
                    });
            }else{
                deferred.resolve('');
            }
            return deferred.promise;
        };

        /**
         *
         * @param argProcessor      An array with processors id
         *
         * This function retrieve transactions according with processors id passed,
         *      ... these transactions are stored in a temporal array called "chainedTransactionsArray"
         */

        $scope.recursiveDate = function(dateStart,dayCounter,counter,processor){
            var deferred = $q.defer();
            if (counter<=dayCounter){
                var tempDate = $filter('date')(dateStart, 'yyyy-MM-dd');
                $scope.isdisbursementdate(processor,tempDate).then(function(results){
                    dateStart.setDate(dateStart.getDate() + 1);
                    counter++;
                    if (results.response){
                        if (results.response!==''){
                            masArray.push({'processor': processor, 'date': tempDate});
                        }
                    }
                    $scope.recursiveDate(dateStart,dayCounter,counter,processor).then(function(results){
                        deferred.resolve(true);
                    });
                });
            }else{
                deferred.resolve(counter);
            }
            return deferred.promise;
        };

        function findMerchantID(){
            if (typeof $scope.Product_ID==='object'){
                productIdSelected = $scope.productId.productName;
            }else{
                productIdSelected = $scope.productId;
            }

            if (productIdSelected==='all') productIdSelected = '2';
            var deferred = $q.defer();

            PaymentService.get({
                paymentId: Authentication.user.Boarding_ID,
                productId: productIdSelected
            }).$promise.then(
                function(results){
                    var find_merchantId = new Nmitransactions({
                        type: 'find_merchantId',
                        boardingId: Authentication.user.Boarding_ID,
                        processorId: results.Processor_ID
                    });
                    Nmitransactions.get(find_merchantId).$promise.then(
                        function(results){
                            deferred.resolve(results);
                        },
                        function(err){
                            deferred.resolve(err);
                        }
                    );
                },
                function(err){

                }
            );
            return deferred.promise;
        }

        function validateIfTransactionExist(transaction_id, processor_id) {
            var deferred = $q.defer(),
                foundData;
            var transactionDetail = new Nmitransactions({
                transactionId:transaction_id,
                actionType:'',
                orderId:'',
                condition:'',
                lastName:'',
                customerVaultId:'',
                startDate:'',
                endDate:'',
                type: 'treport',
                processorId:processor_id,
                boardingId: Authentication.user.Boarding_ID
            }); // var transactionDetail = new Nmitransactions
            Nmitransactions.get(transactionDetail).$promise.then(
                function(transResults){
                    if (transResults.nm_response !== undefined){
                        if(transResults.nm_response.transaction !== undefined){
                            foundData = {
                                transaction_id: transResults.nm_response.transaction[0].transaction_id.$t,
                                amount: transResults.nm_response.transaction[0].action.amount.$t,
                                type: transResults.nm_response.transaction[0].action.action_type.$t,
                                time: transResults.nm_response.transaction[0].action.date.$t
                            };
                            deferred.resolve(foundData);
                        }else{
                            deferred.resolve(foundData);
                        }
                    } else {
                        deferred.resolve(foundData);
                    }
                }); // Nmitransactions.get(transactionDetail).$promise.then
            return deferred.promise;
        }

        function validateIfChargebackExist(arr, transaction_id) {
            var count = Object.keys(arr).length;
            if (count===0){
                return [];
            }else{
                var line_itemsArrayTemporal = [];
                for (var d = 0, len = count; d < len; d += 1) {
                    if (typeof arr[d] !== 'undefined'){
                        if (arr[d].Transaction_ID === transaction_id) {
                            line_itemsArrayTemporal.push({
                                Transaction_ID: arr[d].Transaction_ID,
                                Fee_Amount: arr[d].Fee_Amount,
                                Fee_Name: arr[d].Fee_Name,
                                Occurrence_Date: arr[d].Occurrence_Date,
                                Debit_Credit: arr[d].Debit_Credit
                            });
                        }
                    }
                    var counter = parseInt(d) + 1;
                    if (counter === count){
                        return line_itemsArrayTemporal;
                    }
                }
            }
        }



        /**
         * netSales = ALL Settle - ALL refund
         * amount = settle value
         * rate, e.g. 5%
         * cap = capAmount
         * iscap: is capped or is not capped, this valur is provided by mysql.boardings.reserveCondition
         */
            //function calcReserve(netSales, trasactionAmount, rate, cap) {
        $scope.calcReserve = function(lastPeriodsNetSales, lastPeriodsCap, lifetimeNetSales, trasactionAmount, rate, lifetimeCap, isLifetimeCap){
            var reserveNetSales = 0;
            reserveNetSales = Math.round((trasactionAmount * (rate/100))*100)/100;
            return reserveNetSales;

        };

        function setItemFees(boarding_id, processor, itemType, feeAmount, payment, description, transaction_id, date){
            var deferred = $q.defer();
            var set_itemfees = new Nmitransactions({
                boarding_id: boarding_id,
                processor: processor,
                itemType: itemType,
                feeAmount: feeAmount,
                payment: payment,
                description: description,
                transaction_id: transaction_id,
                date: date,
                type: 'set_itemfees'
            });
            Nmitransactions.get(set_itemfees).$promise.then(
                function(results){
                    deferred.resolve(results);
                },
                function(err){
                    deferred.resolve(err);
                }
            );
            return deferred.promise;
        }

        function findProducts(){
            var deferred = $q.defer();

            var find_products = new Nmitransactions({
                type: 'find_products'
            });
            Nmitransactions.get(find_products).$promise.then(
                function(results){
                    deferred.resolve(results);
                },
                function(err){
                    deferred.resolve(err);
                }
            );

            return deferred.promise;
        }

        function findLineItems(){
            var deferred = $q.defer();

            var find_line_items = new Nmitransactions({
                type: 'find_line_items'
            });
            Nmitransactions.get(find_line_items).$promise.then(
                function(results){
                    deferred.resolve(results);
                },
                function(err){
                    deferred.resolve(err);
                }
            );

            return deferred.promise;
        }

        function findReserveAmount() {
            var deferred = $q.defer();
            var formattedDate = (new Date().getFullYear()) + (('0' + (new Date().getMonth() + 1)).slice(-2)) +
                (('0' + (new Date().getDate() - 1)).slice(-2)) + "000000";
            ProductService.get({
                productId:$scope.products.productName
            }).$promise.then(
                function(productResults){
                    PaymentService.get({
                        paymentId: $scope.merchantIdModal.id,
                        productId: productResults.id
                    }).$promise.then(
                        function(results){
                            if (results.Product_ID === productResults.id && results.Processor_ID !== null) {
                                Account_BalanceService.get({
                                    processorId: results.Processor_ID,
                                    balanceDate: formattedDate
                                }).$promise.then(function(reserve_balanceResults){
                                    deferred.resolve(reserve_balanceResults);
                                });
                            }
                        });
                });
            return deferred.promise;
        }

        function findDisbursementDates(){
            var deferred = $q.defer();

            var find_disbursement = new Nmitransactions({
                type: 'find_disbursement'
            });
            Nmitransactions.get(find_disbursement).$promise.then(
                function(results){
                    deferred.resolve(results);
                },
                function(err){
                    deferred.resolve(err);
                }
            );

            return deferred.promise;
        }

        function findPaymentplans(){
            var deferred = $q.defer();

            var find_products = new Nmitransactions({
                type: 'find_paymentplans'
            });
            Nmitransactions.get(find_products).$promise.then(
                function(results){
                    deferred.resolve(results);
                },
                function(err){
                    deferred.resolve(err);
                }
            );

            return deferred.promise;
        }

        $scope.checkDateRange = function(){
            $scope.alerts = [];
            
            var startDate = $scope.startDate;
            startDate = startDate.split('-');
            startDate = startDate[0]+''+startDate[1]+''+startDate[2];

            var endDate = $scope.endDateYesterday;
            endDate = endDate.split('-');
            endDate = endDate[0]+''+endDate[1]+''+endDate[2];            
                        
            var endDateYesterday = $scope.endDateYesterday;
            endDateYesterday = endDateYesterday.split('-');
            endDateYesterday = endDateYesterday[0]+''+endDateYesterday[1]+''+endDateYesterday[2];           

            if (parseInt(startDate) > parseInt(endDate)){
                $scope.alerts.push({ type: 'danger', msg: 'Please select the correct date range' });
                return false;
            }
            // Austin Harris added endDateYesterday - this else is now run below
            // else{
            //     $scope.alerts = [];
            //     return true;
            // }
            
            var todaysDate = new Date();
            todaysDate.setDate(todaysDate.getDate());
            todaysDate = $filter('date')(todaysDate, 'yyyyMMdd');    
            
            if (endDateYesterday >= todaysDate && $scope.currentView !== 'Transaction Report'){
                $scope.alerts.push({ type: 'danger', msg: "Merchant activity only reflects data up to end of day yesterday" });
                return false;
            }else{
                $scope.alerts = [];
                return true;
            }        
                        
        };

        $scope.confirmationEmailModal = function (message) {
            var modalInstance = $modal.open({
                templateUrl: 'modules/creditcard/views/modalemailconfirmation.client.view.html',
                controller: 'ModalEmailConfCtrl',
                size: 'md',
                resolve: {
                    message: function () {
                        return message;
                    }
                }
            });
            
            modalInstance.result.then(function (result) {
                $scope.emailmessage = null;
                $http.get('/users/emailreceipt', {
                    params: {
                        // boardingID added to find the Merchant Name for the email MC-691.
                        boardingID: Authentication.user.Boarding_ID,
                        merchantName: Authentication.user.Display_Name,
                        transactionId: $scope.transaction[0].transaction_id.$t,
                        processorId: $scope.transaction[0].processor_id.$t,
                        emailList: $scope.message.response
                    }
                }).success(function(response) {
                    // Show user success message
                   var infoarray = {
                        'title': 'Email Receipt',
                        'message': response.message,
                        'accept': '',
                        'deny': 'OK'
                    };
                    $scope.confirmationModal('', infoarray); 
                }).error(function(response) {
                    // show user failure message
                   var infoarray = {
                        'title': 'Error sending Email',
                        'message': response.message,
                        'accept': '',
                        'deny': 'OK'
                    };
                    $scope.confirmationModal('', infoarray); 
                });

            }, function () {
            }); 
        };


        $scope.emailReceipt = function() {
            $scope.message = {
                title: 'Email Reciept'
            };
            if ($scope.transaction[0].email.$t !== '-') {
                $scope.message.emailAddress = $scope.transaction[0].email.$t;
            }
            $scope.confirmationEmailModal($scope.message);
        }; // $scope.emailReceipt = function()


        $scope.printReceipt = function() {
            var originalPage = document.body.innerHTML;
            $http.get('/users/printreceipt', {
                params: {
                    // boardingID added to find the Merchant Name for printing - MC-691.
                    boardingID: Authentication.user.Boarding_ID,
                    merchantName: Authentication.user.Display_Name,
                    transactionId: $scope.transaction[0].transaction_id.$t,
                    processorId: $scope.transaction[0].processor_id.$t
                }
            }).success(function(response) {
                response = response.replace(/&gt;/g, ">").replace(/&lt;/g, "<");
                //document.body.innerHTML = JSON.parse(response);
                //window.print();
                //document.body.innerHTML = originalPage;
                var printWindow=window.open('','','width=810,height=700');
                printWindow.document.write(JSON.parse(response));
                printWindow.document.close();
                printWindow.focus();
                printWindow.print();
                printWindow.close();
            }).error(function(response) {
            });
        };


    $scope.confirmationModal = function (size,message) { // used internally
        var modalInstance = $modal.open({
            templateUrl: 'modules/creditcard/views/modalconfirmation.client.view.html',
            controller: 'ModalConfCtrl',
            size: size,
            resolve: {
                items: function () {
                    return emessage;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
        }, function () {
        });
    };

        angular.module('creditcard').expandSaleController($scope,$location,$modal,$anchorScroll,PaymentService,Nmitransactions,$parse,$filter,Authentication,TransactionsService,ProcessorService,$http,VaultService);
        angular.module('creditcard').reportController($q,$scope,Authentication,PaymentService,Nmitransactions,ReserveService,$location, $anchorScroll,$filter,$parse,$modal,MasService,TransactionsService,ProcessorService,ProductService,MerchantReportService,RunningBalanceService,SettleService,DiscountService,TransactionFeeService,Account_BalanceService,disbursementsService,$http,AccountBalanceReportService,Users,ACHReportService,PaymentCompleteService,LineItemsService,RefundService);
        angular.module('creditcard').dashboardController($q,$scope,Authentication,PaymentService,Nmitransactions,ReserveService,$location, $anchorScroll,$filter,$parse,$modal,MasService,TransactionsService,ProcessorService,ProductService,MerchantReportService,RunningBalanceService,SettleService,DiscountService,TransactionFeeService,Account_BalanceService,disbursementsService,$http,AccountBalanceReportService,Users,ACHReportService,PaymentCompleteService,LineItemsService);
        angular.module('creditcard').dropdownController($q,$scope,Authentication,PaymentService,Nmitransactions,ReserveService,$location, $anchorScroll,$filter,$parse,$modal,MasService,TransactionsService,ProcessorService,ProductService,MerchantReportService,RunningBalanceService,SettleService,DiscountService,TransactionFeeService,Account_BalanceService,disbursementsService,$http,AccountBalanceReportService,Users,ACHReportService,PaymentCompleteService,LineItemsService);
        angular.module('creditcard').miscFuncController($q,$scope,Authentication,PaymentService,Nmitransactions,ReserveService,$location, $anchorScroll,$filter,$parse,$modal,MasService,TransactionsService,ProcessorService,ProductService,MerchantReportService,RunningBalanceService,SettleService,DiscountService,TransactionFeeService,Account_BalanceService,disbursementsService,$http,AccountBalanceReportService,Users,ACHReportService,PaymentCompleteService,LineItemsService);
        angular.module('creditcard').tsnapshotController($q,$scope,Authentication,PaymentService,Nmitransactions,ReserveService,$location, $anchorScroll,$filter,$parse,$modal,MasService,TransactionsService,ProcessorService,ProductService,MerchantReportService,RunningBalanceService,SettleService,DiscountService,TransactionFeeService,Account_BalanceService,disbursementsService,$http,AccountBalanceReportService,Users,ACHReportService,PaymentCompleteService,LineItemsService,$locale);
        angular.module('creditcard').RecurringPaymentController($q,$scope,$modal,$http,$location,Authentication,PaymentService,Nmitransactions,TransactionsService,ProductService,RecurringPaymentPlanService,RecurringPlanService,CheckRecurringPlanService,SubscriptionService);
        angular.module('creditcard').AuditRecurrPlanController($scope,Authentication,$location,AuditRecurrPlanService,AuditRecurrPlanHeaderService,AuditRecurrPlanDetailService);
        angular.module('creditcard').CustomerVaultController($scope,$location,Authentication,VaultService);
        angular.module('creditcard').SubscriptionController($q,$scope,$location,$locale,$http,$modal,Authentication,VaultService,RecurringPaymentPlanService,PaymentService,SubscriptionService,SubscriptionFindOne,ProductService);
    }



/**
 * Directives and filters:
 * offset                                       Returns the selected elements in an array, as a new array object.
 * dateForHuman, dateForHumanWithoutHour        Converts dates to a more readable format.
 * displaynullcurrency                          If a value contain 0, return $0.00
 * orderObjectBy                                Take an array and is order using reduce map
 * forcePrecision
 */
]).filter('offset', function() {
    return function(input, start) {
        start = parseInt(start, 10);
        if(!input){
            return false;
        }
        return input.slice(start);
    };
}).filter('dateForHuman',function(){
    return function(input){
        if(angular.isDefined(input)){
            // YYYYMMDDhhmmss
            if(input.length >= 8){
                input = input.slice(0,14);
                //input = input.slice(4,6) + '/' + input.slice(6,8) + '/' + input.slice(0,4) + ' ' + input.slice(8,10) + ':' + input.slice(10,12) + ':' + input.slice(12,14);
                //input = input.slice(4,6) + '/' + input.slice(6,8) + '/' + input.slice(0,4) + ' ' + input.slice(8,10);
                input = input.slice(4,6) + '/' + input.slice(6,8) + '/' + input.slice(0,4);
            }
        }
        return input;
    };
}).filter('dateForHumanWithoutHour',function(){
    return function(input){
        if(angular.isDefined(input)){
            // YYYYMMDDhhmmss
            if(input.length >= 8){
                input = input.slice(0,14);
                input = input.slice(4,6) + '/' + input.slice(6,8) + '/' + input.slice(0,4);
            }
        }
        return input;
    };
}).filter('displaynullcurrency', function($filter){
    return function(input){
        if(typeof input === 'undefined') return;
        //if(input<0){ return $filter('currency')(input); }
        if(input===0){ return '$0.00'; }
        if(!input){ return ' '; }
        if (input === 0){
            return '$0.00';
        }else{
            if(parseFloat(input) >= 0){
                var numericValue = input;
                //return (numericValue);
                return $filter('currency')(numericValue);
            }else{
                var num = Math.abs(input);
                //return (num);
                return $filter('currency')(num).replace("(", "-").replace(")", "");
            }
        }
        return '';
    };
}).filter('displaynullbalance', function($filter){
    return function(input){
        if(typeof input === 'undefined') return;
        //if(input<0){ return $filter('currency')(input); }
        if(input===0){ return '$0.00'; }
        if(!input){ return ' '; }
        if (input === 0){
            return '$0.00';
        }else{
            if(parseFloat(input) >= 0){
                var numericValue = input;
                //return numericValue;
                return $filter('currency')(numericValue);
            }else{
                var num = input;
                //return num;
                return $filter('currency')(num).replace("(", "-").replace(")", "");
            }
        }
        return '';
    };
}).filter('firstUpper',function($filter){
    return function(input){
        var num = input.charAt(0).toUpperCase() + input.slice(1);
        return num;
    };
}).filter('orderObjectBy', function() {
    return function (items, field, reverse, json, currentPage) {
        var filtered = [];
        angular.forEach(json, function(item) {
            filtered.push(item);
        });
        function index(obj, i) {
            return obj[i];
        }
        filtered.sort(function (a, b) {
            var comparator;
            var reducedA = field.split('.').reduce(index, a); // http://underscorejs.org/#reduce
            var reducedB = field.split('.').reduce(index, b);
            if (reducedA === reducedB) {
                comparator = 0;
            } else {
                if (field==='orderAmount'){
                    comparator = (parseInt(reducedA) > parseInt(reducedB) ? 1 : -1);
                }else{
                    comparator = (reducedA > reducedB ? 1 : -1);
                }
            }
            return comparator;
        });
        if (reverse) {
            filtered.reverse();
        }
        var sliceFrom = currentPage * 25;
        var sliceTo = sliceFrom + 25;
        return filtered.slice(sliceFrom, sliceTo);
    };
}).directive('forcePrecision', function () {
    return {
        restrict: 'A',
        scope: {
            step: '@'
        },
        link: function (scope, element, attrs) {
            if (!scope.step || scope.step === 'any') {
                return;
            }

            var prec = 1;
            for (var i = scope.step; i !== 1; i *= 10) {
                prec *= 10;
            }

            element.on('keypress', function (e) {
                var val = Number(element.val() + (e.charCode !== 0  ? String.fromCharCode(e.charCode) : ''));

                if (val) {
                    var newVal = Math.floor(val * prec) / prec;

                    if (val !== newVal) {
                        e.preventDefault();
                    }
                }
            });
        }
    };
}).filter('ccardToHumanDate', function($filter){
    return function(input){
        var year = input.substring(0, 4);
        var month = input.substring(4, 6) - 1;
        var day = input.substring(6, 8);
        var hour = input.substring(8, 10);
        var minute = input.substring(10, 12);
        var second = input.substring(12, 14);
        var formattedDate = new Date(year, month, day, hour, minute, second);
        return formattedDate.toLocaleDateString( 'en-US',{ 'month': '2-digit', 'day': '2-digit', 'year': 'numeric', 'hour': 'numeric', 'minute': 'numeric', 'second': 'numeric', 'timeZoneName' : 'short'});
    };
}).filter('ccardType', function($filter){
    return function(input, lengthDigits){
        var cardType='UNKNOWN';
        var oneDigit = input.substring(0,1);
        var twoDigits = input.substring(0,2);
        var threeDigits = input.substring(0,3);
        var fourDigits = input.substring(0,4);
        var fiveDigits = input.substring(0,5);
        var sixDigits = input;

        if ((twoDigits === '34' || twoDigits === '37') && lengthDigits === 15) {
            cardType='American Express';
        } else {
            if (oneDigit === '4' && (lengthDigits === 13 || lengthDigits === 16)) {
                if ((fourDigits === '4026' || sixDigits === '417500' || fourDigits === '4405' || fourDigits === '4508' || fourDigits === '4844' || fourDigits === '4913' || fourDigits === '4917') && lengthDigits === 16 ) {
                    cardType='Visa Electron';
                } else {
                    cardType='Visa';
                }
            } else {
                if ((twoDigits === '51' || twoDigits === '52' || twoDigits === '53' || twoDigits === '54' || twoDigits === '55') && lengthDigits === 16) {
                    cardType='MasterCard';
                } else {
                    if ((fourDigits === '6011' || (parseInt(sixDigits) >= 622126 && parseInt(sixDigits) <= 622925) || (parseInt(threeDigits) >= 644 && parseInt(threeDigits) <= 649) || twoDigits === '65') && lengthDigits === 16) {
                        cardType = 'Discover';
                    } else {
                        if ((parseInt(fourDigits) >= 3528 && parseInt(fourDigits) <= 3589) && lengthDigits === 16) {
                            cardType = 'JCB';
                        } else {


                        }
                    }
                }
            }
        }
        return cardType;
    };
}).filter('capitalize', function() {
    return function(input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    };
}).filter('vaultDisplay', function() {
    return function(input) {
        if (input === '' || input === undefined) return 'N/A'; else return input;
    };
}).filter('productDisplay',['ProductService',function(ProductService) {
    return function(input) {
        var productIdString = 'UNKNOWN';
//        console.log(input);
    };
}]).filter ('ccardExpanded', function($filter){
    return function(input, ccardPrefix){
        var ccardNumber = ccardPrefix.substring(0,4) + input.substring(4);
        return ccardNumber;
    };
}).filter('status_capitalize', function() {
    return function(input, all) {
        if (input==="pendingsettlement") {
            return "Pending Settlement";
        }else{
            return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
        }
    };
}).filter('avs_status', function() {
    return function(input) {
        var lookup_avsresponse = {'X':'Exact match, 9-character numeric ZIP',
            'Y':'Exact match, 5-character numeric ZIP',
            'D':'Exact match, 5-character numeric ZIP',
            'M':'Exact match, 5-character numeric ZIP',
            'A':'Address match Only',
            'B':'Address match only',
            'W':'9-character numeric ZIP match Only',
            'Z':'5-character numeric ZIP match Only',
            'P':'5-character ZIP match only',
            'L':'5-character ZIP match only',
            'N':'No address or Zip match',
            'C':'No address or ZIP match only',
            'U':'Address unavailable',
            'G':'Non-U.S. Issuer does not participate',
            'I':'Non-U.S. Issuer does not participate',
            'R':'Issuer system unavailable',
            'E':'Not a mail/phone order',
            'S':'Service not supported',
            '0':'AVS Not Available',
            'O':'AVS Not Available'};
        if (input === "-") {
            return input;
        }else{
            return lookup_avsresponse[input];
        }
    };
}).filter('cvv_status', function() {
    return function(input) {
        var lookup_cvvresponse ={'M':'CVV2/CVC2 Match',
            'N':'CVV2/CVC2 No Match',
            'P':'Not Processed',
            'S':'Merchant has indicated that CVV2/CVC2 is not present on card',
            'U':'Issuer is not certified and/or has not provided Visa encryption keys'};
        if (input === "-") {
            return input;
        }else {
            return lookup_cvvresponse[input];
        }
    };
}).filter('displayFeeName', function($filter){
    return function(input) {
        var lookup_FeeName ={
            'other':'Other',
            'retrieval_fee':'Retrieval fee',
            'misc':'Misc.',
            'ach_reject':'ACH Reject',
            'chargeback':'Chargeback',
            'chargeback_reversal':'Chargeback reversal',
            'reserve_adjustment':'Reserve Release',
            'reserve_increase':'Reserve Increase',
            
        };
        if (input === '-' || input === '' || input === null || input === undefined) {
            return input;
        }else {
            return lookup_FeeName[input];
        }
    };
});

