'use strict';

angular.module('creditcard').controller('BoardingController', ['$scope','Authentication','$location','$modal','PaymentService','Nmitransactions','VaultService','SubscriptionService',
    function($scope,Authentication,$location,$modal,PaymentService,Nmitransactions,VaultService,SubscriptionService) {
        if (!Authentication.user){
            $location.path('/signin');
            return false;
        }
        
        var toggleLoader = function(val){
            if (val) { // show them
                document.getElementById('loadingImg').style.display='block';
            } else { // hide them
                document.getElementById('loadingImg').style.display='none';
            } // if (val)
        };
    

        var sortByKey = function (array, key, sortBy) {
            if (sortBy==='asc'){ // before
                return array.sort(function(a, b) {
                    var x = a[key];
                    var y = b[key];
                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                });
            }else{
                return array.sort(function(a, b) {
                    var x = a[key];
                    var y = b[key];
                    return ((x > y) ? -1 : ((x < y) ? 1 : 0));
                });
            }
        };
        
        $scope.setTitle=function(viewTitle){
            $scope.$parent.$parent.$broadcast('setTitle',viewTitle);
        };
        

        $scope.customerVaultWrap = function(){
            toggleLoader(true);
            $scope.showSearchVault = false;
            $scope.noResults = true;
            $scope.items = [];
            $scope.error = ''; 
            var vault;
            
            $scope.root = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');

            VaultService.get({
                boarding: Authentication.user.Boarding_ID,
                firstName:  $scope.firstName,
                lastName:   $scope.lastName,
                email:      $scope.email,
                last4cc:    $scope.last4ofCC
            }).$promise.then(
                function(vault){
                    vault.response.forEach(function(vaultRecord){
                        $scope.items.push(vaultRecord);
                    });
                },
                function(err){
                    console.log(err);
                    $scope.error = err;
                }
            ).then(function() {
                toggleLoader(false);
                if ($scope.items.length > 0) {
                    $scope.showSearchVault = true;
                    $scope.noResults = false;
                } else {
                    $scope.showSearchVault = false;
                    $scope.noResults = true;
                }
            });
        };


        /*
         * Display modal, warning if there are any plans or verifying if they want to really delete this plan
         * 
         */
        $scope.verifyDelete = function(custId, vaultId) {
            var infoarray = {};
            // verify there are no plans associated with this customer
            SubscriptionService.get({cust:vaultId}).$promise.then(function(subscriptions) {

                if (subscriptions.response.length !== 0 ) {
                    infoarray = {
                        'title': 'Delete Customer Vault',
                        'message': 'You cannot delete this Customer.  The customer is associated with a subscription.',
                        'accept': '',
                        'deny': 'Cancel'
                    };
                    $scope.confirmationModal('', infoarray);
                } else {
                    // no customers - verify merchant wants to remove this record
                    infoarray = {
                        'title': 'Delete Customer Vault',
                        'message': 'Do you wish to delete this customer?',
                        'accept': 'Yes',
                        'deny': 'No',
                        'planId': vaultId
                    };
                    $scope.confirmationModal('', infoarray);
                } // if

            }); // Subscription.get().$promise.then(function()
        };


            
    /**
     * Modal calls with separate controllers
     */
    $scope.confirmationModal = function (size,message) { // used internally
        var modalInstance = $modal.open({
            templateUrl: 'modules/creditcard/views/modalrecurringdeleteconf.client.view.html',
            controller: 'ModalRecurrDeleteConfCtrl',
            size: size,
            resolve: {
                message: function () {
                    return message;
                }
            }
        });

        /*
         * Perform the delete if the modal returns with a plan ID
         *
         * planId = the Recurring_Plan record id that will be deleted
         */
        modalInstance.result.then(function (vaultId) {
            var cust = vaultId.planId;
            if (cust !== '' && vaultId !== cust) {
                // get the vault record
                VaultService.get({id: cust}).$promise.then(function(vaultResponse) {
                    if (vaultResponse) {
                        var vault = vaultResponse.response[0];
                        vault.Is_Active = 0;
                        VaultService.update(vault).$promise.then(function(changedVault) {
                            $scope.customerVaultWrap();
                        }); // Vault.update
                    }  else {
                        console.log('Error retrieving Vault record');
                    }
                }); // VaultService.get({id: vaultId}).$promise.then(function(vault)
            } else {
                console.log('Error retrieving Vault record'); 
            } // if (vaultId !== '' && vaultId !== undefined)
        }, function () {
        });
    };

        $scope.customerExpiryWrap = function(){
            toggleLoader(true);
            $scope.showSearchExpiry = false;
            $scope.noResults = true;
            $scope.items = [];
            $scope.error = '';
            var checkDate = new Date();
            checkDate.setMonth(checkDate.getMonth() + 2);
            checkDate.setDate(0);
            var expiryYear =  checkDate.getFullYear().toString().slice(-2);
            var expiryMonth =  ("0" + (checkDate.getMonth() + 1)).slice(-2);
//console.log('checkdate',expiryYear, expiryMonth)
            VaultService.get({boarding: Authentication.user.Boarding_ID}).$promise.then(
                function(expiry) {
                    for (var i = 0; i < expiry.response.length; i++) {
                        var cardExpYear = expiry.response[i].cc_exp.substr(2,2);
                        var cardExpMonth = expiry.response[i].cc_exp.substr(0,2);
//console.log('exp date',cardExpYear, cardExpMonth)
                        if ( (cardExpYear === expiryYear && cardExpMonth <= expiryMonth) || (cardExpYear < expiryYear) ) {
                            $scope.items.push(expiry.response[i]);
                            $scope.items[$scope.items.length -1].year = cardExpYear;
                        } //
                    } // for (var i = 0; i < expiry.response.length; i++) 
                    sortByKey($scope.items, 'year', 'asc');
                    toggleLoader(false);
                    if ($scope.items.length > 0) {
                        $scope.showSearchExpiry = true;
                        $scope.noResults = false;
                    } else {
                        $scope.showSearchExpiry = false;
                        $scope.noResults = true;
                    } // if ($scope.items.length > 0)
                } // function(expiry)
            ); // ExpiryService.get().$promise.then
        };
        

/*
        $scope.sendEmail = function(vaultId) {
//console.log('email sent to ', vaultId);
            VaultService.get({id: vaultId}).$promise.then(function(vaultResponse) {
                if (vaultResponse.response[0].email !== '' && vaultResponse.response[0].email !== null && vaultResponse.response[0].email !== undefined) {
                    var vault = vaultResponse.response[0];
                    vault.Num_Emails_Sent += 1;
                    vault.Last_Email_Sent = new Date();
                    VaultService.update(vault).$promise.then(function(changedVault) {
                        $scope.customerExpiryWrap();
                    }); // VaultService.update
                } // if (vaultResponse.response[0].email !== '' && vaultResponse.response[0].email !== null && vaultResponse.response[0].email !== undefined) 
            }); // VaultService.get({id: vaultId}).$promise.then(function(vault)
        };
        
        
        $scope.makeCall = function(vaultId) {
//console.log('call made to ', vaultId);
            VaultService.get({id: vaultId}).$promise.then(function(vaultResponse) {
                var vault = vaultResponse.response[0];
                vault.Last_Call_Made = new Date();
                VaultService.update(vault).$promise.then(function(changedVault) {
                    $scope.customerExpiryWrap();
                }); // VaultService.update
            }); // VaultService.get({id: vaultId}).$promise.then(function(vault)

        };
*/
    }
]);
