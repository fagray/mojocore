'use strict';

angular.module('creditcard').controller('ModalConfNBCtrl',['$scope','$modalInstance','message',
  function ($scope, $modalInstance, message) {

    /**
     * Updates acording to option
     */
    $scope.ok = function () {
        $modalInstance.close(message);
    };

    /**
     * Builds the view
     */
    $scope.getInformation = function(){
        $scope.title = message.title;
        $scope.message = message.message;
        $scope.accept = message.accept;
        $scope.deny = message.deny;
  };   

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
        $modalInstance.close();
    };

}]);
