'use strict';

angular.module('creditcard').controller('TransactionsController', ['$scope','$location','Authentication',
    function($scope,$location,Authentication) {
        if (!Authentication.user){
            $location.path('/signin');
            return false;
        }
        
        $scope.$on('succesfullResult', function (event) {
           $scope.Initialize();
        });
        
        
        /**
         * Makes the response view readable
         * @constructor
         */
        $scope.Initialize = function(){
            angular.element(document).ready(function () {
                var saleResponse = $scope.saletest;
                if (saleResponse !== undefined){
                    if ((saleResponse.authcode === undefined)||(saleResponse.authcode === '')){
                        document.getElementById('authorizationCodeContainer').style.display = 'none';
                    }else{
                        document.getElementById('authorizationCodeContainer').style.display = 'block';
                    }
                    if ((saleResponse.transactionid === undefined)||(saleResponse.transactionid === '')){
                        document.getElementById('transactionIdContainer').style.display = 'none';
                    }else{
                        document.getElementById('transactionIdContainer').style.display = 'block';
                    }
                    if ((saleResponse.responsetext === undefined)||(saleResponse.responsetext === '')){
                        document.getElementById('responseContainer').style.display = 'none';
                    }else{
                        document.getElementById('responseContainer').style.display = 'block';
                    }
                    if ((saleResponse.response_code === undefined)||(saleResponse.response_code === '')){
                        document.getElementById('responseCodeContainer').style.display = 'none';
                    }else{
                        document.getElementById('responseCodeContainer').style.display = 'block';
                    }
                    if ((saleResponse.avsresponse === undefined)||(saleResponse.avsresponse === '')){
                        document.getElementById('avsResponseContainer').style.display = 'none';
                    }else{
                        document.getElementById('avsResponseContainer').style.display = 'block';
                    }
                    if ((saleResponse.cvvresponse === undefined)||(saleResponse.cvvresponse === '')){
                        document.getElementById('cvvResponseContainer').style.display = 'none';
                    }else{
                        document.getElementById('cvvResponseContainer').style.display = 'block';
                    }
                    var lookup_cvvresponse ={'M':'CVV2/CVC2 Match',
                            'N':'CVV2/CVC2 No Match',
                            'P':'Not Processed',
                            'S':'Merchant has indicated that CVV2/CVC2 is not present on card',
                            'U':'Issuer is not certified and/or has not provided Visa encryption keys'},
                         lookup_avsresponse = {'X':'Exact match, 9-character numeric ZIP',
                            'Y':'Exact match, 5-character numeric ZIP',
                            'D':'Exact match, 5-character numeric ZIP',
                            'M':'Exact match, 5-character numeric ZIP',
                            'A':'Address match Only',
                            'B':'Address match only',
                            'W':'9-character numeric ZIP match Only',
                            'Z':'5-character numeric ZIP match Only',
                            'P':'5-character ZIP match only',
                            'L':'5-character ZIP match only',
                            'N':'No address or Zip match',
                            'C':'No address or ZIP match only',
                            'U':'Address unavailable',
                            'G':'Non-U.S. Issuer does not participate',
                            'I':'Non-U.S. Issuer does not participate',
                            'R':'Issuer system unavailable',
                            'E':'Not a mail/phone order',
                            'S':'Service not supported',
                            '0':'AVS Not Available',
                            'O':'AVS Not Available'},
                         lookup_response_code = {'100':'Transaction was approved',
                            '200':'Transaction was declined by processor',
                            '201':'Do not honor',
                            '202':'Insufficient funds',
                            '203':'Over limit',
                            '204':'Transaction not allowed',
                            '220':'Incorrect payment information',
                            '221':'No such card issuer',
                            '222':'No card number on file with issuer',
                            '223':'Expired card',
                            '224':'Invalid expiration date',
                            '225':'Invalid card security code',
                            '240':'Call issuer for further information',
                            '250':'Pick up card',
                            '251':'Lost card',
                            '252':'Stolen card',
                            '253':'Fraudulent card'    ,
                            '260':'Declined with further instructions available. (See response text)',
                            '261':'Declined-Stop all recurring payments',
                            '262':'Declined-Stop this recurring program',
                            '263':'Declined-Update cardholder data available',
                            '264':'Declined-Retry in a few days',
                            '300':'Transaction was rejected by gateway',
                            '400':'Transaction error returned by processor',
                            '410':'Invalid merchant configuration',
                            '411':'Merchant account is inactive',
                            '420':'Communication error',
                            '421':'Communication error with issuer',
                            '430':'Duplicate transaction at processor',
                            '440':'Processor format error',
                            '441':'Invalid transaction information',
                            '460':'Processor feature not available',
                            '461':'Unsupported card type',
                            '600':'Success'};
                    document.getElementById('authorizationCode').value = saleResponse.authcode;
                    document.getElementById('transactionId').value = saleResponse.transactionid;
                    document.getElementById('response').value = saleResponse.responsetext;
                    document.getElementById('responseCode').value = lookup_response_code[saleResponse.response_code];
                    document.getElementById('avsResponse').value = lookup_avsresponse[saleResponse.avsresponse];
                    document.getElementById('cvvResponse').value = lookup_cvvresponse[saleResponse.cvvresponse];
                }
            });
        };


        $scope.closeMe = function(){
            document.getElementById('fieldsForm').style.display='block';
            document.getElementById('transactionCompleted').style.display='none';
        };
    }
]);
