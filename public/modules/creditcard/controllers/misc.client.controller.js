'use strict';

angular.module('creditcard').controller('MiscController', ['$scope','Authentication','$location',
    function($scope,Authentication,$location) {
        /**
         * Since the title is part of the layout and not of the content
         * this function just waits for the calls from the content
         * to set the title
         */
        $scope.$on('setTitle', function (event,data) {
             $scope.title=data;
        });
    }
]);
