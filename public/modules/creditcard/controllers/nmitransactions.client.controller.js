'use strict';

angular.module('creditcard').expandSaleController = function($scope,$location,$modal,$anchorScroll,PaymentService,Nmitransactions,$parse,$filter,Authentication,TransactionsService,ProcessorService,$http,VaultService)
{
    $scope.x = 0;
    $scope.Product_ID=2;
    var param = $location.path(),
        validateData = true;
    $scope.param = param.split('/');
    var createUserObject = function(){
            var fieldsUser = {
                 customerVaultId: $scope.customerVaultId,
                 billingId: $scope.billingId,
                 creditCardNumber: $scope.creditCardNumber,
                 expirationDate: $scope.expirationDate,
                  currency: $scope.currency,
                 firstNameCard: $scope.firstNameCard,
                 lastNameCard: $scope.lastNameCard,
                 companyCard: $scope.companyCard,
                 countryCard: $scope.countryCard,
                 addressCard: $scope.addressCard,
                 addressContCard: $scope.addressContCard,
                 cityCard : $scope.cityCard,
                 zipPostalCodeCard: $scope.zipPostalCodeCard,
                 phoneNumberCard: $scope.phoneNumberCard,
                 faxNumberCard: $scope.faxNumberCard,
                 emailAddressCard: $scope.emailAddressCard,
                 shippingId: $scope.shippingId,
                 firstNameShipping: $scope.firstNameShipping,
                 lastNameShipping: $scope.lastNameShipping,
                 companyShipping: $scope.companyShipping,
                 countryShipping: $scope.countryShipping,
                 addressShipping: $scope.addressShipping,
                 addressContShipping: $scope.addressContShipping,
                 cityShipping : $scope.cityShipping,
                 zipPostalCodeShipping: $scope.zipPostalCodeShipping,
                 emailAddressShipping: $scope.emailAddressShipping
            };
            if ($scope.countryCard === 'US'){
                if($scope.stateProvinceCard !== ''){
                    fieldsUser.stateProvinceCard = $scope.stateProvinceCard;
                }
            }else{
                fieldsUser.stateProvinceCard = $scope.stateProvinceCardText;
            }
            if ($scope.countryShipping === 'US'){
                if($scope.stateProvinceShipping !== ''){
                    fieldsUser.stateProvinceShipping = $scope.stateProvinceShipping;
                }
            }else{
                fieldsUser.stateProvinceShipping = $scope.stateProvinceShippingText;
            }
            return fieldsUser;
        },
        toggleLoader = function(val){
            if (val){
                document.getElementById('loadingImg').style.display='block';
                document.getElementById('submitButton').style.display='none';
                //document.getElementById('resetButton').style.display='none';
            }else{
                document.getElementById('loadingImg').style.display='none';
                document.getElementById('submitButton').style.display='inline-block';
                //document.getElementById('resetButton').style.display='inline-block';
            }
        },
        longFieldObject = function(){
            var allFields = {
                 expirationDate: $scope.expirationDate,
                 amount: $scope.amount,
                 currency: $scope.currency,
                 cardSecurityCode: $scope.cardSecurityCode,
                 orderId: $scope.orderId,
                 orderDescription: $scope.orderDescription,
                 poNumber: $scope.poNumber,
                 shipping: $scope.shipping,
                 tax: $scope.tax,
                 firstNameCard: $scope.firstNameCard,
                 lastNameCard: $scope.lastNameCard,
                 companyCard: $scope.companyCard,
                 countryCard: $scope.countryCard,
                 addressCard: $scope.addressCard,
                 addressContCard: $scope.addressContCard,
                 cityCard : $scope.cityCard,
                 zipPostalCodeCard: $scope.zipPostalCodeCard,
                 phoneNumberCard: $scope.phoneNumberCard,
                 faxNumberCard: $scope.faxNumberCard,
                 emailAddressCard: $scope.emailAddressCard,
                 websiteAddressCard: $scope.websiteAddressCard,
                 firstNameShipping: $scope.firstNameShipping,
                 lastNameShipping: $scope.lastNameShipping,
                 companyShipping: $scope.companyShipping,
                 countryShipping: $scope.countryShipping,
                 addressShipping: $scope.addressShipping,
                 addressContShipping: $scope.addressContShipping,
                 cityShipping : $scope.cityShipping,
                 zipPostalCodeShipping: $scope.zipPostalCodeShipping,
                 emailAddressShipping: $scope.emailAddressShipping
            };
            
            if (($scope.param[2])&&(($scope.creditCardNumber).toLowerCase().indexOf('xxx') >= 0)){
                 allFields.customerVaultId = $scope.customerVaultId;
            }else if($scope.param[2]){
                 allFields.customerVaultId = $scope.customerVaultId;
                 allFields.creditCardNumber = $scope.creditCardNumber;
            }else{
                 allFields.creditCardNumber = $scope.creditCardNumber;
            }
            if ($scope.countryCard === 'US'){
                if($scope.stateProvinceCard !== ''){
                    allFields.stateProvinceCard = $scope.stateProvinceCard;
                }
            }else{
                allFields.stateProvinceCard = $scope.stateProvinceCardText;
            }
            if ($scope.countryShipping === 'US'){
                if($scope.stateProvinceShipping !== ''){
                    allFields.stateProvinceShipping = $scope.stateProvinceShipping;
                }
            }else{
                allFields.stateProvinceShipping = $scope.stateProvinceShippingText;
            }
            if ($scope.taxExempt) allFields.tax = -1;
            if ($scope.sendReceiptEmail) allFields.customerReceipt = true;
            if ($scope.addCustomerVault) allFields.customerVault = 'add_customer';

            for (var i = 0; i < Object.keys($scope.itemList).length; i++)
            {
                var item = $scope.itemList[i],
                    itemnumber= i + 1;    
                for(var attribute in item){
                    allFields[attribute+'_'+itemnumber] = item[attribute];
                }
            }

            return allFields;
    },
    validateMask = function(){
        validateData = true;
        var elementArray = ['lastNameCard','firstNameCard','amount','shipping','tax','zipPostalCodeCard','phoneNumberCard','faxNumberCard','zipPostalCodeShipping'];
        elementArray.forEach(function(entry){
            if ($scope.inputForm[entry].$error.pattern){
                $anchorScroll.yOffset = 50;
                $location.hash(entry);
                $anchorScroll();
                validateData = false;
            }
        });
    },
    clearVaultForm = function() {
        $scope.amount                   = '';
        $scope.customerVaultId          = '';
        $scope.billingId                = '';
        $scope.creditCardNumber         = '';
        $scope.expirationDate           = '';
        $scope.cardSecurityCode         = '';
        $scope.currency                 = 'USD';
        $scope.addCustomerVault         = false;
        $scope.orderId                  = '';
        $scope.orderDescription         = '';
        $scope.poNumber                 = '';
        $scope.shipping                 = '';
        $scope.tax                      = '';
        $scope.taxExempt                = false;
        $scope.firstNameCard             = '';
        $scope.lastNameCard             = '';
        $scope.companyCard                 = '';
        $scope.countryCard                 = 'US';
        $scope.addressCard                 = '';
        $scope.addressContCard             = '';
        $scope.cityCard                 = '';
        $scope.stateProvinceCard         = '';
        $scope.zipPostalCodeCard         = '';
        $scope.phoneNumberCard             = '';
        $scope.faxNumberCard             = '';
        $scope.emailAddressCard         = '';
        $scope.websiteAddressCard         = '';
        $scope.sameBilling                = false;
        $scope.shippingId                 = '';
        $scope.firstNameShipping         = '';
        $scope.lastNameShipping         = '';
        $scope.companyShipping             = '';
        $scope.countryShipping             = 'US';
        $scope.addressShipping             = '';
        $scope.addressContShipping         = '';
        $scope.cityShipping                = '';
        $scope.stateProvinceShipping     = '';
        $scope.zipPostalCodeShipping    = '';
        $scope.emailAddressShipping        = '';
        $scope.sendReceiptEmail            = false;
        
        $scope.transactionsId              = '';
        
        $scope.inputForm.$setPristine();
    };
    $scope.itemsView = Authentication.user.Items;
    $scope.cleanUser = function(){
        $scope.customerVaultId             = '';
        $scope.billingId                = '';
        $scope.creditCardNumber         = '';
        $scope.expirationDate            = '';
        $scope.currency                    = 'USD';
        $scope.firstNameCard             = '';
        $scope.lastNameCard             = '';
        $scope.companyCard                 = '';
        $scope.countryCard                 = 'US';
        $scope.addressCard                 = '';
        $scope.addressContCard             = '';
        $scope.cityCard                 = '';
        $scope.stateProvinceCard         = '';
        $scope.zipPostalCodeCard         = '';
        $scope.phoneNumberCard             = '';
        $scope.faxNumberCard             = '';
        $scope.emailAddressCard         = '';
        $scope.websiteAddressCard         = '';
        $scope.shippingId                 = '';
        $scope.firstNameShipping         = '';
        $scope.lastNameShipping         = '';
        $scope.companyShipping             = '';
        $scope.countryShipping             = 'US';
        $scope.addressShipping             = '';
        $scope.addressContShipping         = '';
        $scope.cityShipping                = '';
        $scope.stateProvinceShipping     = '';
        $scope.zipPostalCodeShipping    = '';
        $scope.emailAddressShipping        = '';
        $scope.verifyCountryCard();
        $scope.verifyCountryShipping();    
    };
    $scope.cleanSmallForm = function(){
        $scope.transactionsId = '';
        $scope.amount          = '';
        $scope.TransError           = '';
    };
    $scope.cleanForm = function(){
        $scope.creditCardNumber         = '';
        $scope.expirationDate            = '';
        $scope.amount                    = '';
        $scope.currency                    = 'USD';
        $scope.cardSecurityCode            = '';
        $scope.orderId                     = '';
        $scope.orderDescription            = '';
        $scope.poNumber                 = '';
        $scope.shipping                 = '';
        $scope.tax                         = '';
        $scope.taxExempt                 = '';
        $scope.firstNameCard             = '';
        $scope.lastNameCard             = '';
        $scope.companyCard                 = '';
        $scope.countryCard                 = 'US';
        $scope.addressCard                 = '';
        $scope.addressContCard             = '';
        $scope.cityCard                 = '';
        $scope.stateProvinceCard         = '';
        $scope.zipPostalCodeCard         = '';
        $scope.phoneNumberCard             = '';
        $scope.faxNumberCard             = '';
        $scope.emailAddressCard         = '';
        $scope.websiteAddressCard         = '';
        $scope.firstNameShipping         = '';
        $scope.lastNameShipping         = '';
        $scope.companyShipping             = '';
        $scope.countryShipping             = 'US';
        $scope.addressShipping             = '';
        $scope.addressContShipping         = '';
        $scope.cityShipping                = '';
        $scope.stateProvinceShipping     = '';
        $scope.zipPostalCodeShipping    = '';
        $scope.emailAddressShipping        = '';
        $scope.verifyCountryCard();
        $scope.verifyCountryShipping();    
    };
    $scope.doCreate = function(){
        var infoarray = {};
        //validateMask1();
        //if (validateData){
            if ($scope.creditCardNumber.indexOf('x') === -1 && ($scope.ccv === '' || $scope.ccv === null || $scope.ccv === undefined)) {
                $scope.ccvError = "Card Security Code is required";
            } else {
                $scope.ccvError = "";
                toggleLoader(true);
                if ($scope.checkVaultCreate()) {
                    // Add the record
                    var fieldArray = createUserObject();
                        fieldArray.type = 'create';
                        fieldArray.customerVault = 'add_customer';
                    PaymentService.get({
                        paymentId: Authentication.user.Boarding_ID,
                        productId:4
                    }).$promise.then(
                    function(paymentResults){
                        fieldArray.processorId = paymentResults.Processor_ID;
                        fieldArray.boardingId = Authentication.user.Boarding_ID;
                        var validate = new Nmitransactions(fieldArray);
                        Nmitransactions.get(validate).$promise.then(
                            function(results) {
                                // Looking and recording customer_card_hash for future reports
                                if (results.Response.response_code === '100') { // Appendix 3 - Direct Post API
                                    var validation = new Nmitransactions({
                                        processorId: paymentResults.Processor_ID,
                                        boardingId: Authentication.user.Boarding_ID,
                                        type: 'salevault',
                                        amount: '0.00',
                                        cardSecurityCode: $scope.ccv,
                                        orderId: '',
                                        orderDescription: '',
                                        poNumber: '',
                                        shipping: 0,
                                        tax: 0,
                                        customerHash:results.Response.customer_vault_id
                                    }); // var transactionDetail = new Nmitransactions
                                    Nmitransactions.get(validation).$promise.then(
                                        function(verifyResults){
                                            // Looking and recording customer_card_hash for future reports
                                            if (verifyResults.Response.response_code === '100') { // Appendix 3 - Direct Post API
                                                recordHash('', fieldArray.creditCardNumber,results.Response.customer_vault_id); // transactionid, cc_number
                                                createCustomerVault(results.Response.customer_vault_id,'create');
                    
                                                //document.getElementById('fieldsForm').style.display='none';
                                                //document.getElementById('transactionCompleted').style.display='block';
                                                // results.Response.Title = 'Created User';
                                                //    $scope.saletest = results.Response;
                                                //    $scope.$broadcast('succesfullResult');
                                                
                                                infoarray = {
                                                    'title': 'Create Customer',
                                                    'message': 'Customer successfully created.',
                                                    'accept': '',
                                                    'deny': 'OK'
                                                };
                                                $scope.confirmationSubModal('', infoarray);
                                                clearVaultForm();
                                                toggleLoader(false);
                                            } else {
                                              infoarray = {
                                                    'title': 'Create Customer',
                                                    'message': 'Customer Credit Card Failed Validation.',
                                                    'accept': '',
                                                    'deny': 'OK'
                                                };
                                                $scope.confirmationSubModal('', infoarray);
                                                toggleLoader(false);
                                            }
                                        },
                                        function(err){
                                            toggleLoader(false);
                                            console.log(err);
                                        }
                                    );

                                } else {
                                    infoarray = {
                                          'title': 'Create Customer',
                                          'message': 'Error adding the customer to the vault - ' + results.Response.responsetext,
                                          'accept': '',
                                          'deny': 'OK'
                                      };
                                      $scope.confirmationSubModal('', infoarray);
                                      toggleLoader(false);
                                }
                            },
                            function(err){
                                toggleLoader(false);
                                console.log(err);
                            }
                        );
                    },
                    function(err){
                        toggleLoader(false);
                        console.log(err);
                    }
                    );
    
                } else {
                    // Update the record
                    var fieldArray1 = createUserObject();
                        fieldArray1.type = 'custvaultupdate';
                        fieldArray1.customerVault = 'update_customer';
                    PaymentService.get({
                        paymentId: Authentication.user.Boarding_ID,
                        productId:4
                    }).$promise.then(
                    function(results){
                        fieldArray1.processorId = results.Processor_ID;
                        fieldArray1.boardingId = Authentication.user.Boarding_ID;
                        var validate = new Nmitransactions(fieldArray1);
                        Nmitransactions.get(validate).$promise.then(
                            function(results){
                                // Looking and recording customer_card_hash for future reports
                                if (results.Response.response_code === '100') // Appendix 3 - Direct Post API
                                    recordHash('', fieldArray1.creditCardNumber,results.Response.customer_vault_id); // transactionid, cc_number
                                    createCustomerVault(results.Response.customer_vault_id, 'update');
        
                                document.getElementById('fieldsForm').style.display='none';
                                document.getElementById('transactionCompleted').style.display='block';
                                 results.Response.Title = 'Created User';
                                    $scope.saletest = results.Response;
                                    $scope.$broadcast('succesfullResult');
                                    toggleLoader(false);
                            },
                            function(err){
                               toggleLoader(false);
                                console.log(err);
                            }
                        );
                    },
                    function(err){
                        toggleLoader(false);
                        console.log(err);
                    }
                    );
    
                }
            }
        //}
    };
    
    function recordHash(transactionid, cc_number,customerVaultId){
        var transactionDetail;
        PaymentService.get({
                paymentId: Authentication.user.Boarding_ID,
                productId:4
            }).$promise.then(
            function(paymentResults){
                if (transactionid === ''){
                    transactionDetail = new Nmitransactions({
                        customerVaultId: customerVaultId,
                        processorId: paymentResults.Processor_ID,
                        boardingId: paymentResults.Boarding_ID,
                        type: 'creport'
                    });
                }else{
                    transactionDetail = new Nmitransactions({
                        transactionId: transactionid,
                        type: 'treport'
                    });
                }
                
                Nmitransactions.get(transactionDetail).$promise.then(
                    function(results){
//console.log(results.nm_response.customer_vault.customer.cc_hash.$t)
                        /* Recording in customer_card_hash table */
                        var customer_card_hash;
                        if (transactionid === ''){
                            customer_card_hash = {
                                Transaction_ID: transactionid, 
                                CC_Number: cc_number, 
                                Hash: results.nm_response.customer_vault.customer.cc_hash.$t,
                                processorId: paymentResults.Processor_ID,
                                boardingId: paymentResults.Boarding_ID,
                                type: 'create_customer_card_hash'
                            };
                        }else{
                            customer_card_hash = {
                                Transaction_id: transactionid, 
                                CC_Number: cc_number, 
                                Hash: results.nm_response.transaction.cc_hash.$t, 
                                processorId: paymentResults.Processor_ID,
                                boardingId: paymentResults.Boarding_ID,
                                type: 'create_customer_card_hash'
                            };
                        }
                        var create_customer_card_hash = new Nmitransactions(customer_card_hash);
                        Nmitransactions.get(create_customer_card_hash).$promise.then(
                            function(results){
//                                console.log(results);
                            },
                            function(err){
                                console.log(err);
                            }
                        );
                    },
                    function(err){
                        console.log(err);
                    }
                    );
            },
            function(err){
                console.log(err);
            }
        );
    } 
    function createCustomerVault(customerVaultId, operation) {
//console.log('Entering createCustomerVault(' + customerVaultId + ')')
        var customerVaultDetail;
        if (operation === 'update') {
//console.log('updating a customer vault record')
           PaymentService.get({
                paymentId: Authentication.user.Boarding_ID,
                productId:4
            }).$promise.then(
                function(paymentResults){
                    customerVaultDetail = new Nmitransactions({
                        customerVaultId: customerVaultId,
                        processorId: paymentResults.Processor_ID,
                        boardingId: paymentResults.Boarding_ID,
                        type: 'creport'
                    });
                    Nmitransactions.get(customerVaultDetail).$promise.then(
                        function(vaultResults){
//console.log(vaultResults);
                            if (vaultResults.nm_response.customer_vault.customer.hasOwnProperty('customer_vault_id')) { 
                                var vaultObject = {
                                    customer_vault_id: vaultResults.nm_response.customer_vault.customer.customer_vault_id.$t,
                                    
                                    first_name: vaultResults.nm_response.customer_vault.customer.first_name.$t,
                                    last_name: vaultResults.nm_response.customer_vault.customer.last_name.$t,
                                    address_1: vaultResults.nm_response.customer_vault.customer.address_1.$t,
                                    address_2: vaultResults.nm_response.customer_vault.customer.address_2.$t,
                                    company: vaultResults.nm_response.customer_vault.customer.company.$t,
                                    city: vaultResults.nm_response.customer_vault.customer.city.$t,
                                    state: vaultResults.nm_response.customer_vault.customer.state.$t,
                                    postal_code: vaultResults.nm_response.customer_vault.customer.postal_code.$t,
                                    country: vaultResults.nm_response.customer_vault.customer.country.$t,
                                    email: vaultResults.nm_response.customer_vault.customer.email.$t,
                                    phone: vaultResults.nm_response.customer_vault.customer.phone.$t,
                                    fax: vaultResults.nm_response.customer_vault.customer.fax.$t,
                                    cell_phone: vaultResults.nm_response.customer_vault.customer.cell_phone.$t,
                                    //customer_tax_id: vaultResults.nm_response.customer_vault.customer.customer_tax_id.$t,
                                    website: vaultResults.nm_response.customer_vault.customer.website.$t,
                                    shipping_first_name: vaultResults.nm_response.customer_vault.customer.shipping_first_name.$t,
                                    shipping_last_name: vaultResults.nm_response.customer_vault.customer.shipping_last_name.$t,
                                    shipping_address_1: vaultResults.nm_response.customer_vault.customer.shipping_address_1.$t,
                                    shipping_address_2: vaultResults.nm_response.customer_vault.customer.shipping_address_2.$t,
                                    shipping_company: vaultResults.nm_response.customer_vault.customer.shipping_company.$t,
                                    shipping_city: vaultResults.nm_response.customer_vault.customer.shipping_city.$t,
                                    shipping_state: vaultResults.nm_response.customer_vault.customer.shipping_state.$t,
                                    shipping_postal_code: vaultResults.nm_response.customer_vault.customer.shipping_postal_code.$t,
                                    shipping_country: vaultResults.nm_response.customer_vault.customer.shipping_country.$t,
                                    shipping_email: vaultResults.nm_response.customer_vault.customer.shipping_email.$t,
                                    shipping_carrier: vaultResults.nm_response.customer_vault.customer.shipping_carrier.$t,
                                    tracking_number: vaultResults.nm_response.customer_vault.customer.tracking_number.$t,
                                    shipping_date: vaultResults.nm_response.customer_vault.customer.shipping_date.$t,
                                    shipping: vaultResults.nm_response.customer_vault.customer.shipping.$t,
                                    cc_number: vaultResults.nm_response.customer_vault.customer.cc_number.$t,
                                    cc_hash: vaultResults.nm_response.customer_vault.customer.cc_hash.$t,
                                    cc_exp: vaultResults.nm_response.customer_vault.customer.cc_exp.$t,
                                    cc_start_date: vaultResults.nm_response.customer_vault.customer.cc_start_date.$t,
                                    cc_issue_number: vaultResults.nm_response.customer_vault.customer.cc_issue_number.$t,
                                    check_account: vaultResults.nm_response.customer_vault.customer.check_account.$t,
                                    check_hash: vaultResults.nm_response.customer_vault.customer.check_hash.$t,
                                    check_aba: vaultResults.nm_response.customer_vault.customer.check_aba.$t,
                                    check_name: vaultResults.nm_response.customer_vault.customer.check_name.$t,
                                    //account_holder_type: vaultResults.nm_response.customer_vault.account_holder_type.$t,
                                    account_type: vaultResults.nm_response.customer_vault.customer.account_type.$t,
                                    sec_code: vaultResults.nm_response.customer_vault.customer.sec_code.$t,
                                    processor_id: vaultResults.nm_response.customer_vault.customer.processor_id.$t,
                                    cc_bin: vaultResults.nm_response.customer_vault.customer.cc_bin.$t,
                                    //merchant_defined_field_1: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_1.$t,
                                    //merchant_defined_field_2: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_2.$t,
                                    //merchant_defined_field_3: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_3.$t,
                                    //merchant_defined_field_4: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_4.$t,
                                    //merchant_defined_field_5: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_5.$t,
                                    //merchant_defined_field_6: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_6.$t,
                                    //merchant_defined_field_7: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_7.$t,
                                    //merchant_defined_field_8: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_8.$t,
                                    //merchant_defined_field_9: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_9.$t,
                                    //merchant_defined_field_10: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_10.$t,
                                    //merchant_defined_field_11: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_11.$t,
                                    //merchant_defined_field_12: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_12.$t,
                                    //merchant_defined_field_13: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_13.$t,
                                    //merchant_defined_field_14: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_14.$t,
                                    //merchant_defined_field_15: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_15.$t,
                                    //merchant_defined_field_16: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_16.$t,
                                    //merchant_defined_field_17: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_17.$t,
                                    //merchant_defined_field_18: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_18.$t,
                                    //merchant_defined_field_19: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_19.$t,
                                    //merchant_defined_field_20: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_20.$t,
                                    //amount: vaultResults.nm_response.customer_vault.customer.amount.$t,
                                    //currency: vaultResults.nm_response.customer_vault.customer.currency.$t,
                                    //descriptor: vaultResults.nm_response.customer_vault.customer.descriptor.$t,
                                    //descriptor_phone: vaultResults.nm_response.customer_vault.customer.descriptor_phone.$t,
                                    //order_description: vaultResults.nm_response.customer_vault.customer.order_description.$t,
                                    //order_id: vaultResults.nm_response.customer_vault.customer.order_id.$t,
                                    //po_number: vaultResults.nm_response.customer_vault.customer.po_number.$t,
                                    //tax: vaultResults.nm_response.customer_vault.customer.tax.$t,
                                    //tax_exempt: vaultResults.nm_response.customer_vault.customer.tax_exempt.$t,
                                    Is_Active: 1
                                };
                                
                                // update the record
                                VaultService.update(vaultObject).$promise.then(
                                    function(changedVault) {
                                    }
                                ); // PaymentService.update
                            } // if
                        },
                        function(err){
                            console.log(err);
                        }
                    );
                },
                function(err){
                    console.log(err);
                }
            );
        } else {
            PaymentService.get({
                paymentId: Authentication.user.Boarding_ID,
                productId:4
            }).$promise.then(
                function(paymentResults){
                    customerVaultDetail = new Nmitransactions({
                        customerVaultId: customerVaultId,
                        processorId: paymentResults.Processor_ID,
                        boardingId: paymentResults.Boarding_ID,
                        type: 'creport'
                    });
                    
                    Nmitransactions.get(customerVaultDetail).$promise.then(
                        function(vaultResults){
//console.log(vaultResults)
                            var vaultObject = {
                                nmi: vaultResults.nm_response.customer_vault.customer.customer_vault_id.$t,
                                Merchant_ID: paymentResults.Boarding_ID,
                                customer_vault_id: vaultResults.nm_response.customer_vault.customer.customer_vault_id.$t,
                                first_name: vaultResults.nm_response.customer_vault.customer.first_name.$t,
                                last_name: vaultResults.nm_response.customer_vault.customer.last_name.$t,
                                address_1: vaultResults.nm_response.customer_vault.customer.address_1.$t,
                                address_2: vaultResults.nm_response.customer_vault.customer.address_2.$t,
                                company: vaultResults.nm_response.customer_vault.customer.company.$t,
                                city: vaultResults.nm_response.customer_vault.customer.city.$t,
                                state: vaultResults.nm_response.customer_vault.customer.state.$t,
                                postal_code: vaultResults.nm_response.customer_vault.customer.postal_code.$t,
                                country: vaultResults.nm_response.customer_vault.customer.country.$t,
                                email: vaultResults.nm_response.customer_vault.customer.email.$t,
                                phone: vaultResults.nm_response.customer_vault.customer.phone.$t,
                                fax: vaultResults.nm_response.customer_vault.customer.fax.$t,
                                cell_phone: vaultResults.nm_response.customer_vault.customer.cell_phone.$t,
                                //customer_tax_id: vaultResults.nm_response.customer_vault.customer.customer_tax_id.$t,
                                website: vaultResults.nm_response.customer_vault.customer.website.$t,
                                shipping_first_name: vaultResults.nm_response.customer_vault.customer.shipping_first_name.$t,
                                shipping_last_name: vaultResults.nm_response.customer_vault.customer.shipping_last_name.$t,
                                shipping_address_1: vaultResults.nm_response.customer_vault.customer.shipping_address_1.$t,
                                shipping_address_2: vaultResults.nm_response.customer_vault.customer.shipping_address_2.$t,
                                shipping_company: vaultResults.nm_response.customer_vault.customer.shipping_company.$t,
                                shipping_city: vaultResults.nm_response.customer_vault.customer.shipping_city.$t,
                                shipping_state: vaultResults.nm_response.customer_vault.customer.shipping_state.$t,
                                shipping_postal_code: vaultResults.nm_response.customer_vault.customer.shipping_postal_code.$t,
                                shipping_country: vaultResults.nm_response.customer_vault.customer.shipping_country.$t,
                                shipping_email: vaultResults.nm_response.customer_vault.customer.shipping_email.$t,
                                shipping_carrier: vaultResults.nm_response.customer_vault.customer.shipping_carrier.$t,
                                tracking_number: vaultResults.nm_response.customer_vault.customer.tracking_number.$t,
                                shipping_date: vaultResults.nm_response.customer_vault.customer.shipping_date.$t,
                                shipping: vaultResults.nm_response.customer_vault.customer.shipping.$t,
                                cc_number: vaultResults.nm_response.customer_vault.customer.cc_number.$t,
                                cc_hash: vaultResults.nm_response.customer_vault.customer.cc_hash.$t,
                                cc_exp: vaultResults.nm_response.customer_vault.customer.cc_exp.$t,
                                cc_start_date: vaultResults.nm_response.customer_vault.customer.cc_start_date.$t,
                                cc_issue_number: vaultResults.nm_response.customer_vault.customer.cc_issue_number.$t,
                                check_account: vaultResults.nm_response.customer_vault.customer.check_account.$t,
                                check_hash: vaultResults.nm_response.customer_vault.customer.check_hash.$t,
                                check_aba: vaultResults.nm_response.customer_vault.customer.check_aba.$t,
                                check_name: vaultResults.nm_response.customer_vault.customer.check_name.$t,
                                //account_holder_type: vaultResults.nm_response.customer_vault.account_holder_type.$t,
                                account_type: vaultResults.nm_response.customer_vault.customer.account_type.$t,
                                sec_code: vaultResults.nm_response.customer_vault.customer.sec_code.$t,
                                processor_id: vaultResults.nm_response.customer_vault.customer.processor_id.$t,
                                cc_bin: vaultResults.nm_response.customer_vault.customer.cc_bin.$t,
                                //merchant_defined_field_1: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_1.$t,
                                //merchant_defined_field_2: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_2.$t,
                                //merchant_defined_field_3: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_3.$t,
                                //merchant_defined_field_4: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_4.$t,
                                //merchant_defined_field_5: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_5.$t,
                                //merchant_defined_field_6: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_6.$t,
                                //merchant_defined_field_7: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_7.$t,
                                //merchant_defined_field_8: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_8.$t,
                                //merchant_defined_field_9: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_9.$t,
                                //merchant_defined_field_10: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_10.$t,
                                //merchant_defined_field_11: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_11.$t,
                                //merchant_defined_field_12: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_12.$t,
                                //merchant_defined_field_13: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_13.$t,
                                //merchant_defined_field_14: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_14.$t,
                                //merchant_defined_field_15: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_15.$t,
                                //merchant_defined_field_16: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_16.$t,
                                //merchant_defined_field_17: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_17.$t,
                                //merchant_defined_field_18: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_18.$t,
                                //merchant_defined_field_19: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_19.$t,
                                //merchant_defined_field_20: vaultResults.nm_response.customer_vault.customer.merchant_defined_field_20.$t,
                                //amount: vaultResults.nm_response.customer_vault.customer.amount.$t,
                                //currency: vaultResults.nm_response.customer_vault.customer.currency.$t,
                                //descriptor: vaultResults.nm_response.customer_vault.customer.descriptor.$t,
                                //descriptor_phone: vaultResults.nm_response.customer_vault.customer.descriptor_phone.$t,
                                //order_description: vaultResults.nm_response.customer_vault.customer.order_description.$t,
                                //order_id: vaultResults.nm_response.customer_vault.customer.order_id.$t,
                                //po_number: vaultResults.nm_response.customer_vault.customer.po_number.$t,
                                //tax: vaultResults.nm_response.customer_vault.customer.tax.$t,
                                //tax_exempt: vaultResults.nm_response.customer_vault.customer.tax_exempt.$t,
                                Is_Active: 1
                                
                            };
                            $http.post('/customervault', vaultObject)
                            .success(function (response) {
                                
                            }).error(function (response) {
                                console.log(response);
                            });
                        },
                        function(err){
                            console.log(err);
                        }
                    );
                },
                function(err){
                    console.log(err);
                }
            );
        } // if (operation === 'update') 
    }
    $scope.doSale = function(){
        validateMask();
        if (validateData){
           toggleLoader(true);
            var fieldArray = longFieldObject();
                fieldArray.type = 'sale';
            PaymentService.get({
                paymentId: Authentication.user.Boarding_ID,
                productId:4
            }).$promise.then(
            function(results){
                fieldArray.processorId = results.Processor_ID;
                fieldArray.boardingId = results.Boarding_ID;
                var sale = new Nmitransactions(fieldArray);
                Nmitransactions.get(sale).$promise.then(
                    function(results){
                        
                        // Looking and recording customer_card_hash for future reports
                        if (results.Response.response_code === '100') { // Appendix 3 - Direct Post API
                            recordHash(results.Response.transactionid, fieldArray.creditCardNumber); // transactionid, cc_number
                            if (results.Response.customer_vault_id !== '' & results.Response.customer_vault_id !== null && results.Response.customer_vault_id !== undefined) {
                                createCustomerVault(results.Response.customer_vault_id);
                            } // if (results.Response.customer_vault_id !== '' & results.Response.customer_vault_id !== null && results.Response.customer_vault_id !== undefined)
                        }
                        document.getElementById('fieldsForm').style.display='none';
                        document.getElementById('transactionCompleted').style.display='block';
                        results.Response.Title='Sale';
                        $scope.saletest = results.Response;
                        $scope.$broadcast('succesfullResult');
                        clearVaultForm();
                        toggleLoader(false);
                    },
                    function(err){
                        toggleLoader(false);
                        console.log(err);
                    }
                );
            },
            function(err){
                toggleLoader(false);
                console.log(err);
            }
            );
        }
    };
    $scope.doAuthorize = function(){
        validateMask();
        if (validateData){
           toggleLoader(true);
            var fieldArray = longFieldObject();
                fieldArray.type = 'auth';
            PaymentService.get({
                paymentId: Authentication.user.Boarding_ID,
                productId:4
            }).$promise.then(
            function(results){
                fieldArray.processorId = results.Processor_ID;
                fieldArray.boardingId = Authentication.user.Boarding_ID;
                var authorize = new Nmitransactions(fieldArray);
                Nmitransactions.get(authorize).$promise.then(
                    function(results){

                        // Looking and recording customer_card_hash for future reports
                        if (results.Response.response_code === '100') { // Appendix 3 - Direct Post API
                            recordHash(results.Response.transactionid, fieldArray.creditCardNumber,''); // transactionid, cc_number
                            if (results.Response.customer_vault_id !== '' & results.Response.customer_vault_id !== null && results.Response.customer_vault_id !== undefined) {
                                createCustomerVault(results.Response.customer_vault_id);
                            } // if (results.Response.customer_vault_id !== '' & results.Response.customer_vault_id !== null && results.Response.customer_vault_id !== undefined)
                        }
                        document.getElementById('fieldsForm').style.display='none';
                        document.getElementById('transactionCompleted').style.display='block';
                         results.Response.Title='Authorize';
                            $scope.saletest = results.Response;
                            $scope.$broadcast('succesfullResult');
                            clearVaultForm();
                            toggleLoader(false);
                    },
                    function(err){
                       toggleLoader(false);
                        console.log(err);
                    }
                );    
            },
            function(err){
                 console.log(err);
            }
            );
        }
    };
    $scope.doCredit = function(){
        validateMask();
        if (validateData){
           toggleLoader(true);
            var fieldArray = longFieldObject();
                fieldArray.type = 'credit';
            PaymentService.get({
                paymentId: Authentication.user.Boarding_ID,
                productId:4
            }).$promise.then(
            function(results){
                fieldArray.processorId = results.Processor_ID;
                fieldArray.boardingId = Authentication.user.Boarding_ID;
                var    credit = new Nmitransactions(fieldArray);
                Nmitransactions.get(credit).$promise.then(
                    function(results){

                        // Looking and recording customer_card_hash for future reports
                        if (results.Response.response_code === '100') { // Appendix 3 - Direct Post API
                            recordHash(results.Response.transactionid, fieldArray.creditCardNumber,''); // transactionid, cc_number
                            if (results.Response.customer_vault_id !== '' & results.Response.customer_vault_id !== null && results.Response.customer_vault_id !== undefined) {
                                createCustomerVault(results.Response.customer_vault_id);
                            } // if (results.Response.customer_vault_id !== '' & results.Response.customer_vault_id !== null && results.Response.customer_vault_id !== undefined)
                        }
                        document.getElementById('fieldsForm').style.display='none';
                        document.getElementById('transactionCompleted').style.display='block';
                         results.Response.Title='Credit';
                            $scope.saletest = results.Response;
                            $scope.$broadcast('succesfullResult');
                            clearVaultForm();
                            toggleLoader(false);
                    },
                    function(err){
                       toggleLoader(false);
                        console.log(err);
                    }
                );    
            },
            function(err){

            }
            );
        }
    };
    $scope.doCapture = function(){
       toggleLoader(true);
        PaymentService.get({
                paymentId: Authentication.user.Boarding_ID,
                productId:4
            }).$promise.then(
            function(results){
                var capture = new Nmitransactions({
                    transactionId: $scope.transactionsId,
                    amount: $scope.amount,
                    type: 'capture',
                    processorId:results.Processor_ID,
                    boardingId: Authentication.user.Boarding_ID
                });
                Nmitransactions.get(capture).$promise.then(
                    function(results){
                        document.getElementById('fieldsForm').style.display='none';
                        document.getElementById('transactionCompleted').style.display='block';
                         results.Response.Title='Capture';
                            $scope.saletest = results.Response;
                            $scope.$broadcast('succesfullResult');
                            clearVaultForm();
                            toggleLoader(false);
                    },
                    function(err){
                       toggleLoader(false);
                        console.log(err);
                    }
                );
            },
            function(err){

            });
    };
    $scope.doRefund = function(){
       toggleLoader(true);
        TransactionsService.get({
            transactionid:$scope.transactionsId
        },function(transaction){
            if (!transaction.hasOwnProperty('transaction')) {
                // Transaction does not belong to this client
               toggleLoader(false);
                $scope.TransError="Transaction does not belong to this client";
                console.log('Transaction does not belong to this client');
            } else {
                var processorId = transaction.transaction.processor_id.$t;
                var theProductId = transaction.transaction.processor_id.$t; // not reliable

                var validProcessorId = '';
                var foundProcessor = false;

                console.log('Transactions ID: ' + $scope.transactionsId);
                console.log('Boarding ID: ' + Authentication.user.Boarding_ID);
                console.log('Amount: ' + $scope.amount);
                console.log('Processor ID: ' + processorId);

                var objectPayment={paymentId:Authentication.user.Boarding_ID, productId: 'all'};
                PaymentService.get(objectPayment).$promise.then(
                function(payment_plan) {
                    for (var plan in payment_plan) {
                        if (!isNaN(plan)) {
                            if (processorId === payment_plan[plan].Processor_ID) {
                                foundProcessor = true;
                                validProcessorId = payment_plan[plan].Processor_ID;
                            }
                        }
                    }
                    console.log('validProcessorId: ' + validProcessorId);
                    
                    if (!foundProcessor) {
                        // Transaction does not belong to this client
                       toggleLoader(false);
                        $scope.TransError="Transaction does not belong to this client";
                        console.log('Transaction does not belong to this client');
                    } else {
                        $scope.TransError="";
                        console.log('Transaction does belong to this client');                                
                        var refund = new Nmitransactions({
                            transactionId:$scope.transactionsId,
                            amount:$scope.amount,
                            type:'refund',
                            processorId:validProcessorId,
                            boardingId: Authentication.user.Boarding_ID
                        });
                        Nmitransactions.get(refund).$promise.then(
                            function(results){
                                document.getElementById('fieldsForm').style.display='none';
                                document.getElementById('transactionCompleted').style.display='block';
                                results.Response.Title='Refund';
                                $scope.saletest = results.Response;
                                $scope.$broadcast('succesfullResult');
                                clearVaultForm();
                                toggleLoader(false);
                            },
                            function(err){
                               toggleLoader(false);
                                console.log(err);
                            }
                        );    
                    }
                });
            }
        });
    };
    $scope.doVoid = function(){
       toggleLoader(true);
        TransactionsService.get({
            transactionid:$scope.transactionsId
        },function(transaction){
            if (!transaction.hasOwnProperty('transaction')) {
                // Transaction does not belong to this client
               toggleLoader(false);
                $scope.TransError="Transaction does not belong to this client";
                console.log('Transaction does not belong to this client');
            } else {
                var processorId = transaction.transaction.processor_id.$t;
                var theProductId = transaction.transaction.processor_id.$t; // not reliable

                var validProcessorId = '';
                var foundProcessor = false;

                console.log('Transactions ID: ' + $scope.transactionsId);
                console.log('Boarding ID: ' + Authentication.user.Boarding_ID);
                console.log('Amount: ' + $scope.amount);
                console.log('Processor ID: ' + processorId);

                var objectPayment={paymentId:Authentication.user.Boarding_ID, productId: 'all'};
                PaymentService.get(objectPayment).$promise.then(
                function(payment_plan) {
                    for (var plan in payment_plan) {
                        if (!isNaN(plan)) {
                            if (processorId === payment_plan[plan].Processor_ID) {
                                foundProcessor = true;
                                validProcessorId = payment_plan[plan].Processor_ID;
                            }
                        }
                    }
                    console.log('validProcessorId: ' + validProcessorId);
                    
                    if (!foundProcessor) {
                        // Transaction does not belong to this client
                       toggleLoader(false);
                        $scope.TransError="Transaction does not belong to this client";
                        console.log('Transaction does not belong to this client');
                    } else {
                        $scope.TransError="";
                        //console.log('Transaction does belong to this client');                                
                        var voidv = new Nmitransactions({
                            transactionId:$scope.transactionsId,
                            type: 'void',
                            processorId:validProcessorId,
                            boardingId: Authentication.user.Boarding_ID
                        });
                        Nmitransactions.get(voidv).$promise.then(
                            function(voidv){
                                document.getElementById('fieldsForm').style.display='none';
                                document.getElementById('transactionCompleted').style.display='block';
                                 voidv.Response.Title='Void';
                                    $scope.saletest = voidv.Response;
                                    $scope.$broadcast('succesfullResult');
                                    clearVaultForm();
                                    toggleLoader(false);
                            },
                            function(err){
                               toggleLoader(false);
                                console.log(err);
                            }
                        );    
                    }
                });
            }
        });
	};
	$scope.updateItemTotal = function(attribute,title){
		if ((title === 'item_unit_cost')||(title === 'item_discount_amount')||(title === 'item_tax_amount')||(title === 'item_quantity')){
			var cost = parseFloat(attribute.item_unit_cost) || 0,
				tax = parseFloat(attribute.item_tax_amount) || 0,
				disc = parseFloat(attribute.item_discount_amount) || 0,
				qty = parseInt(attribute.item_quantity) || 0,
				tax_rate = cost/tax,
				sum = 0,
				taxsum=0;
				attribute.item_quantity = qty;
				attribute.item_tax_total = $filter('currency')(tax * qty,'',2);
				attribute.item_tax_rate = $filter('currency')(tax_rate,'',2);
				attribute.item_total_amount = $filter('currency')(((cost + tax)*qty) - disc,'$',2);
			for (var i = 0; i < Object.keys($scope.itemList).length; i++)
				{
					var item = $scope.itemList[i],
					totalAmount = item.item_total_amount.replace('$',''),
					totalTax = item.item_tax_total.replace('$','');
					sum+=parseFloat(totalAmount);
					taxsum+=parseFloat(totalTax);
				}
			$scope.totalTaxTransaction=taxsum;
			$scope.totalCostTransaction=sum;
		}
		if  (((title === 'item_product_code')||(title === 'item_description'))&&(attribute.item_quantity === 0)){
				attribute.item_quantity = 1;
		}
	};
	$scope.roundNumbers = function(attribute,title){
		if ((title === 'item_unit_cost')||(title === 'item_discount_amount')||(title === 'item_tax_amount')||(title === 'item_quantity')){
			var cost = parseFloat(attribute.item_unit_cost) || 0,
				tax = parseFloat(attribute.item_tax_amount) || 0,
				disc = parseFloat(attribute.item_discount_amount) || 0,
				qty = parseInt(attribute.item_quantity) || 0,
				tax_rate = cost/tax,
				sum = 0,
				taxsum=0;
				attribute.item_quantity = qty;
				attribute.item_unit_cost = $filter('currency')(cost,'',4);
				attribute.item_tax_amount= $filter('currency')(tax,'',2);
				attribute.item_tax_discount_amount= $filter('currency')(disc,'',2);
				attribute.item_tax_total = $filter('currency')(tax * qty,'',2);
				attribute.item_tax_rate = $filter('currency')(tax_rate,'',2);
				attribute.item_total_amount = $filter('currency')(((cost + tax)*qty) - disc,'$',2);
			for (var i = 0; i < Object.keys($scope.itemList).length; i++)
				{
					var item = $scope.itemList[i],
					totalAmount = item.item_total_amount.replace('$',''),
					totalTax = item.item_tax_total.replace('$','');
					sum+=parseFloat(totalAmount);
					taxsum+=parseFloat(totalTax);
				}
			$scope.totalTaxTransaction=taxsum;
			$scope.totalCostTransaction=sum;
		}
		if  (((title === 'item_product_code')||(title === 'item_description'))&&(attribute.item_quantity === 0)){
				attribute.item_quantity = 1;
		}
	};
	$scope.setAmount = function(){
		$scope.amount = $scope.totalCostTransaction;
		$scope.tax = $scope.totalTaxTransaction;
	};
	$scope.addRow = function(){
		var newItem = {	'item_product_code':'',
						'item_description':'',
						'item_commodity_code':'',
						'item_quantity':'0',
						'item_unit_of_measure':'',
						'item_unit_cost':'0.00',
						'item_tax_amount':'0.00',
						'item_discount_amount':'0.00',
						'item_total_amount':'0.00',
						'item_tax_rate':''};
		$scope.itemList.push(newItem);
	};
	$scope.removeRow = function(){
		if (Object.keys($scope.itemList).length > 1){
			$scope.itemList.pop();
		}
	};
	$scope.setTitle=function(viewTitle){
		$scope.$parent.$parent.$broadcast('setTitle',viewTitle);
	};
	$scope.initialize=function(){
		 angular.element(document).ready(function () {
             $scope.ccvRequired = true;
	    	if ($scope.param[2]){
				if (($scope.param[1] === 'refund')||($scope.param[1] === 'void')||($scope.param[1] === 'capture')){
	    			$scope.transactionsId = $scope.param[2];
	    			$scope.$apply();
	    		}else if (($scope.param[1] === 'sale')||($scope.param[1] === 'credit')||($scope.param[1] === 'authorize')){
	    					PaymentService.get({
								paymentId: Authentication.user.Boarding_ID,
								productId:4
							}).$promise.then(
							function(results){
									var populate = new Nmitransactions({
										type: 'creport',
									    customerVaultId: $scope.param[2],
										processorId: results.Processor_ID,
                                        boardingId: Authentication.user.Boarding_ID
									});
									Nmitransactions.get(populate).$promise.then(
										function(results){
											document.getElementById('checked').disabled = true;
											document.getElementById('addCustomerVault').disabled = true;
											document.getElementById('creditCardNumber').disabled = true;
											document.getElementById('expirationDate').disabled = true;
											document.getElementById('addCustomerVault').style.display = 'none';
											document.getElementById('checked').style.display = 'block';
                                            $scope.ccvRequired = false;
											document.getElementById("cardSecurityCode").placeholder = '';
											document.getElementById('cardSecurityCode').style.display = 'none';
											document.getElementById('cardSecurityCodeLabel').style.display = 'none';
											$scope.customerVaultId 			= results.nm_response.customer_vault.customer.customer_vault_id.$t;
											$scope.creditCardNumber 		= results.nm_response.customer_vault.customer.cc_number.$t;
											$scope.expirationDate			= results.nm_response.customer_vault.customer.cc_exp.$t;
											$scope.amount					= '';
											$scope.currency					= 'USD';
											$scope.cardSecurityCode			= '';
											$scope.orderId 					= '';
											$scope.orderDescription			= '';
											$scope.poNumber 				= '';
											$scope.shipping 				= results.nm_response.customer_vault.customer.shipping.$t;
											$scope.tax 						= '';
											$scope.taxExempt 				= '';
											$scope.firstNameCard 			= results.nm_response.customer_vault.customer.first_name.$t;
											$scope.lastNameCard 			= results.nm_response.customer_vault.customer.last_name.$t;
											$scope.companyCard 				= results.nm_response.customer_vault.customer.company.$t;
											$scope.countryCard 				= results.nm_response.customer_vault.customer.country.$t;
											$scope.addressCard 				= results.nm_response.customer_vault.customer.address_1.$t;
											$scope.addressContCard 			= results.nm_response.customer_vault.customer.address_2.$t;
											$scope.cityCard 				= results.nm_response.customer_vault.customer.city.$t;
											$scope.stateProvinceCard 		= results.nm_response.customer_vault.customer.state.$t;
											$scope.zipPostalCodeCard 		= results.nm_response.customer_vault.customer.postal_code.$t;
											$scope.phoneNumberCard 			= results.nm_response.customer_vault.customer.phone.$t;
											$scope.faxNumberCard 			= results.nm_response.customer_vault.customer.fax.$t;
											$scope.emailAddressCard 		= results.nm_response.customer_vault.customer.email.$t;
											$scope.websiteAddressCard 		= results.nm_response.customer_vault.customer.website.$t;
											$scope.firstNameShipping 		= results.nm_response.customer_vault.customer.shipping_first_name.$t;
											$scope.lastNameShipping 		= results.nm_response.customer_vault.customer.shipping_last_name.$t;
											$scope.companyShipping 			= results.nm_response.customer_vault.customer.shipping_company.$t;
											$scope.countryShipping 			= results.nm_response.customer_vault.customer.shipping_country.$t;
											$scope.addressShipping 			= results.nm_response.customer_vault.customer.shipping_address_1.$t;
											$scope.addressContShipping 		= results.nm_response.customer_vault.customer.shipping_address_2.$t;
											$scope.cityShipping				= results.nm_response.customer_vault.customer.shipping_city.$t;
											$scope.stateProvinceShipping 	= results.nm_response.customer_vault.customer.shipping_state.$t;
											$scope.zipPostalCodeShipping	= results.nm_response.customer_vault.customer.shipping_postal_code.$t;
											$scope.emailAddressShipping		= results.nm_response.customer_vault.customer.shipping_email.$t;
											},
										function(err){
											console.log(err);
										}
									);
							},
							function(err){
                            }
                        );
                }
            }
        });
    };
    
    $scope.confirmationSubModal = function (size,message) { // used internally
        var modalInstance = $modal.open({
            templateUrl: 'modules/creditcard/views/modalrecurringdeleteconf.client.view.html',
            controller: 'ModalRecurrDeleteConfCtrl',
            size: size,
            resolve: {
                message: function () {
                    return message;
                }
            }
        });

        /*
         * Perform the delete if the modal returns with a plan ID
         *
         * planId = the Recurring_Plan record id that will be deleted
         */
        modalInstance.result.then(function (subId) {               
        }, function () {
        });
    };


};
