'use strict';


angular.module('creditcard').controller('IntegrationController', ['$scope','$location','Authentication','ApikeyService','PaymentService', '$http',
    function($scope,$location,Authentication,ApikeyService,PaymentService,$http) {
        if (!Authentication.user){
            $location.path('/signin');
            return false;
        }

        $scope.pagenumber = 1;
        $scope.documentpdf = 'MojoCore_API_Merchant_Api_Transactions.pdf';
        $scope.integrationdocument = 'modules/creditcard/views/integrationdoc'+$scope.pagenumber+'.client.view.html';

        $scope.page = 1;
        $scope.integration_page_1 = 'modules/creditcard/views/integration.client.view.html';
        $scope.integration_page_2 = 'modules/users/views/settings/nation-builder-integration.client.view.html';
        $scope.templateurl = '';
        
        $scope.showNationBuilderTab = true;

        $scope.integration_init = function() {
//            console.log('integration_init()');
            $scope.showNationBuilderTab = Authentication.user.Enable_Nation;
        };

        $scope.integration_next = function(page) {
            switch(page) {
                case 1:
                       $scope.page = 1;
                       $scope.templateurl = $scope.integration_page_1;
                       break;
                   case 2:
                       $scope.page = 2;
                       $scope.templateurl = $scope.integration_page_2;
                       break;
                   default:
                       $scope.page = 1;
                       $scope.templateurl = $scope.integration_page_1;
                       break;
            }
        };

        $scope.changeDocument = function(){
            $scope.integrationdocument = 'modules/creditcard/views/integrationdoc'+$scope.pagenumber+'.client.view.html';        
            if ($scope.pagenumber === '1') {
                $scope.documentpdf = 'MojoCore_API_Merchant_Api_Transactions.pdf';
            } else if ($scope.pagenumber === '2') {
                $scope.documentpdf = 'MojoCore_API_Merchant_Api_Customer.pdf';
            } else if ($scope.pagenumber === '3') {
                $scope.documentpdf = 'MojoCore_API_Merchant_Api_Plans.pdf';
            } else if ($scope.pagenumber === '4') {
                $scope.documentpdf = 'MojoCore_API_Merchant_Api_Subscriptions.pdf';
            } else if ($scope.pagenumber === '5') {
                $scope.documentpdf = 'MojoCore_API_Merchant_Api_Appendicies.pdf';
            }
        };
        
        $scope.setTitle=function(viewTitle){
            $scope.$parent.$parent.$broadcast('setTitle',viewTitle);
        };
        
        
        /**
         * Makes the calls to refresh Api
         */
        $scope.refreshApi = function() {
            var apikey = {
                boarding_id:Authentication.user.Boarding_ID,
                username:Authentication.user.Username
            };
              $http.post('/apikey', apikey).success(function(response) {
                 $scope.apikeys = response;
          }).error(function(response) {
            $scope.error = response.message;
          });
        };
        
        
        $scope.copyApi = function(){
            document.getElementById('Token').select();
 
            var elementExists;
            elementExists = document.getElementById('themessageA');
            if (elementExists !== null) {
                document.getElementById('themessageA').style.display='block';
            }
            setTimeout(function() {
                elementExists = document.getElementById('themessageA');
                if (elementExists !== null){
                    document.getElementById('themessageA').style.display='none';
                }
             }, 2500);

        };
        
        
        $scope.copyProcessor = function(){
            document.getElementById('Processor_ID').select();
            var elementExists;
            elementExists = document.getElementById('themessageP');
            if (elementExists !== null) {
                document.getElementById('themessageP').style.display='block';
            }
            setTimeout(function() {
                var elementExists = document.getElementById('themessageP');
                if (elementExists !== null) {
                    document.getElementById('themessageP').style.display = 'none';
                }
             }, 2500);

        };
        
        
        $scope.initialize = function(){
            ApikeyService.get({
                boardingapi: Authentication.user.Boarding_ID
            }, function(apikey) {
                $scope.apikeys = apikey;
                    
                PaymentService.get({
                        paymentId: Authentication.user.Boarding_ID,
                        productId:4
                    }).$promise.then(
                        function(results){
                            $scope.Processor_ID = results.Processor_ID;
                        },
                        function(err){
        
                        }
                );
            });
        };
    }
]);
