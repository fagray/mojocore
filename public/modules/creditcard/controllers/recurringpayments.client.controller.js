'use strict';

angular.module('creditcard').RecurringPaymentController = function($q,$scope,$modal,$http,$location,Authentication,PaymentService,Nmitransactions,TransactionsService,ProductService,RecurringPaymentPlanService,RecurringPlanService,CheckRecurringPlanService,SubscriptionService)
{
    
    var arrayMerchantsProducts = [];
    var arrayMerchantsPlans = [];
    
    var params = $location.path();
    $scope.param = params.split('/');

    $scope.sampleProductCategories = [];
    $scope.originalPaymentPlan = {};
    $scope.recurrdays = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
    $scope.frequencies = [
        {value: 1, desc: '1'},
        {value: 2, desc: '2'},
        {value: 3, desc: '3 (Quarterly)'},
        {value: 4, desc: '4'},
        {value: 5, desc: '5'},
        {value: 6, desc: '6 (Bi-Annually)'},
        {value: 7, desc: '7'},
        {value: 8, desc: '8'},
        {value: 9, desc: '9'},
        {value: 10, desc: '10'},
        {value: 11, desc: '11'},
        {value: 12, desc: '12 (Annually)'},
        {value: 13, desc: '13'},
        {value: 14, desc: '14'},
        {value: 15, desc: '15'},
        {value: 16, desc: '16'},
        {value: 17, desc: '17'},
        {value: 18, desc: '18'},
        {value: 19, desc: '19'},
        {value: 20, desc: '20'},
        {value: 21, desc: '21'},
        {value: 22, desc: '22'},
        {value: 23, desc: '23'},
        {value: 24, desc: '24'}
    ];
    $scope.frequency = 'everySoManyDays';
    

    var clearRecurringPlanForm = function() {
        $scope.dropdownMenu2 = '';
        $scope.plan_name = '';
        $scope.plan_id = '';
        $scope.plan_amount = '';
        $scope.plan_payments_1 = '';
        $scope.plan_payments_0 = false;
        $scope.frequency = 'everySoManyDays';
        $scope.day_frequency = '';
        $scope.day_of_month = '';
        $scope.month_frequency = '';
        $scope.toggleNumberOfTimes($scope.plan_payments_0);
        $scope.toggleFrequency($scope.frequency);
        $scope.fieldsForm.$setPristine();
    };


    /*
     * Show/Hide the spinning icon along with the submit button
     *
     */
    var toggleLoader = function(val){
        if (val) { // show them
            document.getElementById('loadingImg').style.display='block';
            document.getElementById('submitButton').style.display='none';
        } else { // hide them
            document.getElementById('loadingImg').style.display='none';
            document.getElementById('submitButton').style.display='inline-block';
        } // if (val)
    };
        

    /*
     * Build the object to be inserted/updated in the database
     *
     */
    var createRecurringPlanObject = function() {
        // the object will be returned in fieldsPlan 
        var fieldsPlan = {
            plan_id: $scope.plan_id,
            plan_name: $scope.plan_name,
            plan_amount: $scope.plan_amount            
        };
        
        // how many times should we charge this?
        if (document.getElementById('plan_payments_0').checked) { // Charge until cancelled
            fieldsPlan.plan_payments = 0;
        } else {
            fieldsPlan.plan_payments = $scope.plan_payments_1; // Charge a set number of times
        } // if (document.getElementById('plan_payments_0').checked
        
        // when do we charge this?
        if (document.querySelector('input[name="frequency"]:checked').value === 'everySoManyDays') { // Charge every X days
            fieldsPlan.day_frequency = $scope.day_frequency;
            fieldsPlan.day_of_month = '';
            fieldsPlan.month_frequency = '';
        } else { // Charge on the Xth day of every Y months
            fieldsPlan.day_frequency = '';
            fieldsPlan.day_of_month = $scope.day_of_month;
            fieldsPlan.month_frequency = $scope.month_frequency.value;
        } // if (document.querySelector('input[name="frequency"]:checked').value === 'everySoManyDays')
        
        if (!$scope.checkCreate()) { // this is an update.  Need the Recurring_Plan record id
            fieldsPlan.id = $scope.originalPaymentPlan.id;
        } // if (!checkCreate())
        return fieldsPlan;
    };


    /*
     * Send the record to the database Recurring_Plan Service
     *
     * newObject = this information from the form
     * oldObject = "create" or the record as it existed before any changes
     */
    function recordRecurringPlan(oldObject, newObject) {
        if ($scope.checkCreate()) {
            // create the database entry for the plan if it is new
            $http.post('/recurringplan', newObject)
            .success(function(response) {
                $scope.auditRecurringPlanChange(oldObject, response); // create the initial auditing Record.
                $scope.displayConfirmation('okay');
                clearRecurringPlanForm();
            })
            .error(function(err) {
                console.log(err);
            });
        } else {
            // update the database entry for the plan if it is being changed
            RecurringPlanService.update({
                id: oldObject.id,
                Boarding_ID: newObject.Boarding_ID,
                ProcessorID: newObject.Processor_ID,
                Plan_Name: newObject.plan_name,
                Plan_ID: newObject.plan_id,
                Plan_Amount: newObject.plan_amount,
                Plan_Payments: newObject.plan_payments,
                Day_Frequency: newObject.day_frequency,
                Month_Frequency: newObject.month_frequency,
                Day_Of_Month: newObject.day_of_month,
                Is_Active: newObject.Is_Active
            }).$promise.then(function(changedRecurringPlan) {
                // audit the changed plan
                $scope.auditRecurringPlanChange(oldObject, changedRecurringPlan);
            }); // PaymentService.update
        } // if (checkCreate())
    }


    $scope.displayConfirmation = function(returnResponse) {
        var results = {};
        var infoarray;
        //document.getElementById('fieldsForm').style.display='none'; // hide the form
        //document.getElementById('transactionCompleted').style.display='block'; // show the modal popup
        
        // mock up a response to the display confirmation modal
        if ( returnResponse === "error" ) {
            infoarray = {
                'title': 'Error Creating Recurring Plan',
                'message': 'Plan Was Not Created',
                'accept': '',
                'deny': 'Cancel'
            };
        } else {
            infoarray = {
                'title': 'Created Recurring Plan',
                'message': 'Plan Created.',
                'accept': '',
                'deny': 'OK'
            };
        }
        $scope.confirmationModal('', infoarray); 
        
    };


    /*
     * Check if this is a create record session or if we are modifying an existing record
     *
     */
    $scope.checkCreate = function () {
        if ($scope.param[1] === 'recurringplancreate' && $scope.param[2] === undefined) {
            // we are creating Recurring Plan from scratch
            return true;
        } else { // if ($scope.param[1] === 'recurringplanmodify' && $scope.param[2] !== undefined)
            // we are modifying and existing Recurring Plan
            return false;
        }
    };


    /*
     * Send the record to the database Recurring_Plan Auditing Service
     *
     * newObject = the recently inserted/updated Recurring_Plan record
     * oldObject = "create" or the record as it existed before any changes (if updating)
     */
    $scope.auditRecurringPlanChange = function(firstObject, secondObject) {
        var oldObject = {};
        var postObject = {};
        
        // Add the auditing header record
        postObject = {
            Boarding_ID:  secondObject.Boarding_ID,
            Processor_ID: secondObject.Processor_ID,
            Recurring_Plan_ID: secondObject.id
        };
        $http.post('/auditrecurringplanheader', postObject)
        .success(function(response) { // post the detail lines
            
            if (typeof firstObject !== 'object' && firstObject === 'create') {
                // record was created - set old object to blanks
                Object.keys(secondObject).forEach(function(key) {
                    oldObject.key = '';
                }); // Object.keys(secondObject).forEach
            } else {
                // record was modified 
                oldObject = firstObject;
            } // if (typeof firstObject !== 'object' && firstObject ===  'create')
            Object.keys(secondObject).forEach(function(key) {
                // check if this is a table field we audit
                if (key !== 'id' && key !== 'createdAt' && key !== 'updatedAt' && key !== '$resolved') {
                    // audit this table field by looping through the Recurring Plan objects (old and new)
                    if (oldObject[key] !== secondObject[key]) {
                        // new value is different from old value - audit it
                        postObject = {
                            Audit_Header_ID: response.id,
                            Field_Name: key,
                            Old_Value: oldObject[key],
                            New_Value: secondObject[key]
                        };
                        $http.post('/auditrecurringplandetail', postObject)
                        .success(function(response) {
                        })
                        .error(function(err) {
                            console.log(err);
                        }); // $http.post('/auditrecurringplandetail', postObject)
                    }  // if (oldObject[key] !== secondObject[key]) 
                } // if (key !== 'id' && key !== 'createdAt' && key !== 'updatedAt') 
            }); // Object.keys(secondObject).forEach
            
            $scope.originalPaymentPlan = secondObject; // reset the "original" plan to this newly modified one.
        }) // $http.post('/auditrecurringplanheader', postObject) success
        .error(function(err) {
            console.log(err);
        }); // $http.post('/auditrecurringplanheader', postObject) error
        
    };


    /*
     * Display modal, warning if there are any customers or verifying if they want to really delete this plan
     * 
     */
    $scope.verifyDelete = function(planId, id) {
        var infoarray = {};
        // verify there are no customers in this plan
        SubscriptionService.get({planId:planId}).$promise.then(function(subscriptions) {
            if (subscriptions.response.length !== 0) {
                infoarray = {
                    'title': 'Delete Recurring Plan',
                    'message': 'You cannot delete this plan.  There are customers associated with this plan.',
                    'accept': '',
                    'deny': 'Cancel',
                    'delete': true
                };
                $scope.confirmationModal('', infoarray);
            } else {
    
                // no customers - verify merchant wants to remove this record
                infoarray = {
                    'title': 'Delete Recurring Plan',
                    'message': 'Do you wish to delete this plan?',
                    'accept': 'Yes',
                    'deny': 'No',
                    'planId': planId,
                    'delete': true,
                    'id': id
                };
                $scope.confirmationModal('', infoarray);
            } // if
        });
     };


    /*
     * Display modal, warning if there are any customers or verifying if they want to really modify this plan
     * 
     */
    $scope.verifyEdit = function(planId) {
        var infoarray = {};
        // verify there are no customers in this plan
        SubscriptionService.get({planId:planId}).$promise.then(function(subscriptions) {
            if (subscriptions.response.length !== 0) {
                infoarray = {
                    'title': 'Modify Recurring Plan',
                    'message': 'There are customers associated with this plan. Any changes to this plan will affect those customers.',
                    'accept': 'OK',
                    'deny': 'Cancel',
                    'planId': planId,
                    'delete': false
                };
                $scope.confirmationModal('', infoarray);
            } else {
                $location.path('recurringplanmodify/' + planId);
            } // if
        });
     };


    /*
     * listener for "Charge until canceled" checkbox being checked/unckecked
     *
     */
    $scope.changeNumberOfTimes = function() {
        var selectChecked = document.getElementById('plan_payments_0').checked;
        $scope.toggleNumberOfTimes(selectChecked); // toggle fields based on checkbox
    };


    /*
     * function to deactivate/activate fields/labels based on passed value
     *
     * value - true = charge until cancelled, false = charge a set number of times
     */
    $scope.toggleNumberOfTimes = function(value) {
        var i;
        var untilCancelled = document.getElementsByClassName('chargeUntilCancelled');
        var numberOfTimes = document.getElementsByClassName('chargeANumberOfTimes');

        if(value === true) { // charge until cancelled
            // disable / fade the form elements for "Charge This Many Times" 
            for (i = 0; i < numberOfTimes.length; i++) {
                numberOfTimes[i].disabled = true;
                numberOfTimes[i].style.opacity = 0.5;
                $scope.plan_payments_1 = ''; // empty out the field for "Charge This Many Times"              
            } //for (i = 0; i < numberOfTimes.length; i++) 
            // enable / un-fade the form elements for "Charge Until Cancelled" 
            for (i = 0; i < untilCancelled.length; i++) {
                untilCancelled[i].disabled = false;
                untilCancelled[i].style.opacity = 1;                   
            } // for (i = 0; i < untilCancelled.length; i++)
        } else { // charge a set number of times
           // enable / un-fade the form elements for "Charge This Many Times" 
           for (i = 0; i < numberOfTimes.length; i++) {
                numberOfTimes[i].disabled = false;
                numberOfTimes[i].style.opacity = 1;                   
            } // for (i = 0; i < numberOfTimes.length; i++)
            // disable / fade the form elements for "Charge Until Cancelled" 
            for (i = 0; i < untilCancelled.length; i++) {
                untilCancelled[i].disabled = true;
                untilCancelled[i].style.opacity = 0.5;
            } // for (i = 0; i < untilCancelled.length; i++)
        } // if(value === true) 
    };


    /*
     * listener for "frequency" radio being checked/unckecked
     *
     */
    $scope.changeFrequency = function() {
        var radioChecked = document.querySelector('input[name="frequency"]:checked').value;
        $scope.toggleFrequency(radioChecked); // toggle fields based on radio button checked
    }; 
    

    /*
     * function to deactivate/activate fields/labels based on passed value
     *
     * value - everySoManyDays = charge the customer every so many days, onDayOfMonth = charge the customer on a set day every so many months
     */
    $scope.toggleFrequency = function(value) {
        var i;
        var onDayOfMonthElements = document.getElementsByClassName('onDayOfMonth'); // get all the elements associated with "charge the customer on a set day every so many months"
        var everySoManyDays = document.getElementsByClassName('everySoManyDays'); // get all the elements associated with "charge the customer every so many days"
        
        switch(value) {
            case 'everySoManyDays': 
                // disable/fade the form elements for "charge the customer every so many days"
                for (i = 0; i < onDayOfMonthElements.length; i++) {
                    onDayOfMonthElements[i].disabled = true;                    
                    onDayOfMonthElements[i].style.opacity = 0.5;
                    $scope.day_of_month = undefined; // empty out the field for "Charge The Customer on Day" 
                    $scope.month_frequency = undefined;// empty out the field for "of every ___ Months" 
                } // for (i = 0; i < onDayOfMonthElements.length; i++) 
                // enable/un-fade the form elements for "charge the customer on a set day every so many months"
                for (i = 0; i < everySoManyDays.length; i++) {
                    everySoManyDays[i].disabled = false;                    
                    everySoManyDays[i].style.opacity = 1;
                } // for (i = 0; i < everySoManyDays.length; i++) 
                break;
            case 'onDayOfMonth':
                // enable/un-fade the form elements for "charge the customer on a set day every so many months"
                for (i = 0; i < onDayOfMonthElements.length; i++) {
                    onDayOfMonthElements[i].disabled = false;
                    onDayOfMonthElements[i].style.opacity = 1;                   
                }
                // disable/fade the form elements for "charge the customer every so many days"
                for (i = 0; i < everySoManyDays.length; i++) {
                    everySoManyDays[i].disabled = true;
                    everySoManyDays[i].style.opacity = 0.5;                    
                    $scope.day_frequency = ''; // empty out the field for "Charge the Customer Every ___ Days" 
                }
                break;
        } // switch(value) 
    };


    /**
     * Modal calls with separate controllers
     */
    $scope.confirmationModal = function (size,message) { // used internally
        var modalInstance = $modal.open({
            templateUrl: 'modules/creditcard/views/modalrecurringdeleteconf.client.view.html',
            controller: 'ModalRecurrDeleteConfCtrl',
            size: size,
            resolve: {
                message: function () {
                    return message;
                }
            }
        });

        /*
         * Perform the delete if the modal returns with a plan ID
         *
         * planId = the Recurring_Plan record id that will be deleted
         */
        modalInstance.result.then(function (planId) {
//console.log(planId);
           var plan_id = planId.id;
           if (plan_id !== '' && plan_id !== undefined && planId.delete === true) {
                // remove this record
                //first, get this recurring plan
                RecurringPlanService.get({id: plan_id}).$promise.then(function(recurr) {
                    $scope.originalPaymentPlan = recurr.response[0]; // set the historical information on this plan for audit purposes
                    $scope.originalPaymentPlan.id = $scope.originalPaymentPlan.Recurr_ID;
                    var recurringPlan = recurr.response[0];
                    // Create the updated record
                    var fieldArray = {
                        id: recurringPlan.Recurr_ID,
                        Boarding_ID: Authentication.user.Boarding_ID,
                        Processor_ID: recurringPlan.Processor_ID,
                        plan_id: recurringPlan.Plan_ID,
                        plan_name: recurringPlan.Plan_Name,
                        plan_amount: recurringPlan.Plan_Amount,
                        plan_payments: recurringPlan.Plan_Payments,
                        day_frequency: recurringPlan.Day_Frequency,
                        day_of_month: recurringPlan.Day_Of_Month,
                        month_frequency: recurringPlan.Month_Frequency,
                        Is_Active: 0 // this is the only field that should be changed
                    };
                    recordRecurringPlan($scope.originalPaymentPlan, fieldArray); // Change the record
                    
                    $scope.recurringPlansWrap(); // recreate the display
                }); // RecurringPlanService.get({Recurring_Plan_ID: planId}).promise.then
           } else if (planId.delete === false){
                $location.path('recurringplanmodify/' + planId.planId);
            } // if (planId !== '' && planId !== undefined)
        }, function () {
        });
    };


    /*
     * Populate the Product dropdown
     *
     */
    $scope.getRecurrPlanMerchantProductDropdown = function(){
        var boarding = Authentication.user.Boarding_ID;
        ProductService.get({
            productId:'all'
        }).$promise.then(
            function(allProducts){
                PaymentService.get({paymentId:boarding,productId:'all'}).$promise.then(
                    function(payment) {
                        delete payment.$promise;
                        delete payment.$resolved;
                        Nmitransactions.get({type: 'getMerchantInfo',boardingId: boarding}).$promise.then(
                            function(boardingPlan) {
                                var tempProduct_ID = 0,
                                    products = [],
                                    merchant = {};
                                for(var j = 0; j < Object.keys(payment).length; j++) {
                                    var productIndex = parseInt(payment[j].Product_ID) - 1;
        
                                    if (payment[j].Product_ID > 1) {
                                        products.push({
                                            'Name': allProducts[productIndex].Description,
                                            'Boarding_ID': boarding,
                                            'Product_ID': payment[j].Product_ID,
                                            'Processor_ID': payment[j].Processor_ID
                                        });
                                    } // if (payment[j].Product_ID > 1) 
                                } // for(var j = 0; j < Object.keys(payment).length; j++) 
                                arrayMerchantsProducts.push({'id':boarding, 'Name':boardingPlan.Merchant_Name , 'value': boarding, 'products':products});
                                $scope.setRecurrPlanMerchant(arrayMerchantsProducts);
                            }, // function(boardingPlan)
                            function(err){
                                console.log(err);
                            } // function(err)
                        ); // Nmitransactions.get.$promise.then
                    }, // function(payment)
                    function(err){
                        console.log(err);
                    } // function(err)
                ); // PaymentService.get().$promise.then
            }, // function(allProducts)
            function(err){
                console.log(err);
            } // function(err)
        ); // ProductService.get.$promise.then
    };


    /*
     * Once the DOM is loaded, populates the Product Name dropdown list and sets the selected item to the first one
     *
     * arrayMerchantsProducts = array of products for this merchant
     */
    $scope.setRecurrPlanMerchant = function(arrayMerchantsProducts) {
        angular.element(document).ready(function () {
            $scope.sampleProductCategories = arrayMerchantsProducts; // populate the Product Name dropdown
            $scope.Product_ID = arrayMerchantsProducts[0].products[0]; // first item in the array
        }); // angular.element(document).ready
    };    

    
    /*
     * Initialize the input form, taking into account whether or not this is a create or update situation
     *
     */
    $scope.recurringInitialize = function() {
        $scope.getRecurrPlanMerchantProductDropdown(); // build the Product Name dropdown list
        if (!$scope.checkCreate()) { // updating an existing record
            // grab the record and populate the fields.
            RecurringPlanService.get({Recurring_Plan_ID: $scope.param[2]})
                .$promise.then(function(recurr) {
                    var recurringPlan = recurr.Response[0];
                    $scope.id = recurringPlan.id;
                    $scope.originalPaymentPlan = recurringPlan;
                    $scope.plan_id = recurringPlan.Plan_ID;
                    $scope.plan_name = recurringPlan.Plan_Name;
                    $scope.plan_amount = (recurringPlan.Plan_Amount).toFixed(2);

                    if (recurringPlan.Plan_Payments !== 0) { // This is not a "Charge Until Cancelled" situation, set field value and call the toggle function to disable/enable and fade/un-fade elements
                        $scope.plan_payments_1 = recurringPlan.Plan_Payments;
                        $scope.toggleNumberOfTimes(false);
                    } else {// "Charge Until Cancelled",  set checkbox value and call the toggle function to disable/enable and fade/un-fade elements
                        document.getElementById('plan_payments_0').checked = true;
                        $scope.plan_payments_0 = true; 
                        $scope.toggleNumberOfTimes(true);
                    } // if (recurringPlan.Plan_Payments !== 0) 
                    if (recurringPlan.Day_Of_Month === 0 && recurringPlan.Month_Frequency === 0) { // This is not a "charge the customer on a set day every so many months" set value and call the toggle function to disable/enable and fade/un-fade elements
                        $scope.frequency = 'everySoManyDays';
                        $scope.day_of_month = recurringPlan.Day_Of_Month;
                        $scope.month_frequency = recurringPlan.Month_Frequency;
                        $scope.day_frequency = recurringPlan.Day_Frequency;
                        $scope.toggleFrequency('everySoManyDays');
                    } else { // "charge the customer on a set day every so many months", set radio button, set values, and call the toggle function to disable/enable and fade/un-fade elements
                        $scope.frequency = 'onDayOfMonth';
                        document.getElementsByName('frequency')[0].checked = false;
                        document.getElementsByName('frequency')[1].checked = true;
                        $scope.day_of_month = recurringPlan.Day_Of_Month;
                        $scope.month_frequency = $scope.frequencies[recurringPlan.Month_Frequency - 1];
                        $scope.day_frequency = recurringPlan.Day_Frequency;
                        $scope.toggleFrequency('onDayOfMonth');
                    } // if (recurringPlan.Day_Of_Month === 0 && recurringPlan.Month_Frequency === 0) 
                    
                    document.getElementById('productControl').innerHTML = recurringPlan.Description; // set the display portion of the dropdown
                }); // RecurringPlanService.get.promise        
        } // if (!$scope.checkCreate())
    };


    /*
     * Validate the items in the form
     *
     */
    $scope.validForm = function() {
        var validForm = true;
        var regexDollars  = /^[1-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
        $scope.error = '';
        
        // Product ID
        if($scope.checkCreate()) {
            if ($scope.productId.Product_ID === '' || $scope.productId.Product_ID === null || $scope.productId.Product_ID === undefined) {
                $scope.error = 'Please Choose a Product for this plan';
                return(false);
            }
        }
        
        // Plan Name
        if ($scope.plan_name === '' || $scope.plan_name === null || $scope.plan_name === undefined) {
            $scope.error = 'Please enter the Plan Name';
            return(false);
        }
        
        // Plan Id
        if($scope.checkCreate()) {
            if ($scope.plan_id === '' || $scope.plan_id === null || $scope.plan_id === undefined) {
                $scope.error = 'Please enter the Plan ID';
                return(false);
            } else if (parseInt($scope.plan_id, 10) !== $scope.plan_id || parseInt($scope.plan_id, 10) < 0) {
                $scope.error = 'Plan ID must be a positive number';
                return(false);
            }
        }
        
        // Plan Amount
        if ($scope.plan_amount === '' || $scope.plan_amount === null || $scope.plan_amount === undefined) {
            $scope.error = 'Please enter a proper Plan Amount';
            return(false);
        } else if (parseFloat($scope.plan_amount) <= 0) {
            $scope.error = 'Plan Amount must be greater than zero';
            return(false);
        } else if (!regexDollars.test($scope.plan_amount)) {
            $scope.error = 'Plan Amount must be a dollar amount';
            return(false);            
        }
        
        // Charge This Many Times / Charge Until Canceled
        if(!$scope.plan_payments_0) {
            if($scope.plan_payments_1 === '' || $scope.plan_payments_1 === null || $scope.plan_payments_1 === undefined) {
                $scope.error = 'Please enter a valid value for Charge This Many Times';
                return(false);
            } else if (parseInt($scope.plan_payments_1, 10) !== $scope.plan_payments_1 || parseInt($scope.plan_payments_1, 10) < 0) {
                $scope.error = 'Charge This Many Times must be a positive number';
                return(false);
            }
        }
        
        // Charge Every So Many Days / Charge On Day Of Month(s)
        if ($scope.frequency === 'everySoManyDays') {
            if($scope.day_frequency === '' || $scope.day_frequency === null || $scope.day_frequency === undefined) {
                $scope.error = 'Please enter a valid value for Charge the Customer Every X Days';
                return(false);
            } else if (parseInt($scope.day_frequency, 10) !== $scope.day_frequency || parseInt($scope.day_frequency, 10) < 0) {
                $scope.error = 'Charge the Customer Every X Days must be a positive number';
                return(false);
            }
        } else if ($scope.frequency === 'onDayOfMonth') {
            if($scope.day_of_month === '' || $scope.day_of_month === null || $scope.day_of_month === undefined) {
                $scope.error = 'Please choose a proper day for Charge the Customer On Day';
                return(false);
            }
            if($scope.month_frequency === '' || $scope.month_frequency === null || $scope.month_frequency === undefined) {
                $scope.error = 'Please choose a proper month interval for Charge the Customer On Day x of every Months';
                return(false);
            }
        }
        
        // validate that this record is not a duplicate
        if($scope.checkCreate()) {
            return($scope.checkPlanExists());
        } else {
  //          if ($scope.checkCreate()) { // create the record
  //              $scope.doCreateRecurringPlan();
  //          } else { // update the record
                $scope.doUpdateRecurringPlan();
 //           } // if ($scope.checkCreate());
        }
    };


    $scope.checkPlanExists = function() {
        CheckRecurringPlanService.get({ // See if this plan exists for this merchant ID/Poduct ID/ Plan_ID
                boardingId: Authentication.user.Boarding_ID,
                productId: $scope.productId.Product_ID,
                planId: $scope.plan_id
            }).$promise.then(function(recurring_plan) {
                if (recurring_plan.clean !== undefined && recurring_plan.clean === 'clean') {
                    if ($scope.checkCreate()) { // create the record
                        $scope.doCreateRecurringPlan();
                    } else { // update the record
                        $scope.doUpdateRecurringPlan();
                    } // if ($scope.checkCreate());
                } else {
                    $scope.error = 'There is already an active or previously deleted plan with this Plan ID';
                    return false;
                }
           }, // function(recurring_plan)
            function(err){
                console.log(err);
            } // function(err)
        ); // CheckRecurringPlanService.get().$promise.then
    };


    /*
     * Submit button - call a different function depending on if we are creating vs updating a record
     *
     */
    $scope.submitRecurringPlan = function() {
        $scope.validForm();
    };


    /*
     * create a Recurring Plan Record based on form input
     *
     */
    $scope.doCreateRecurringPlan = function() {
        toggleLoader(true); // turn on the spinner icon
        var fieldArray = createRecurringPlanObject(); // generate the object with form values 
        fieldArray.type = 'add_recurring_plan'; // we are adding a plan
        PaymentService.get({ // get the payment plan for this merchant ID/product ID
            paymentId: Authentication.user.Boarding_ID,
            productId: $scope.productId.Product_ID
        }).$promise.then(
            function(results) {
                fieldArray.processorId = results.Processor_ID; // set the boarding ID for this plan
                fieldArray.boardingId = Authentication.user.Boarding_ID; // set the processor ID for this plan
                recordRecurringPlan('create', fieldArray); // record and audit the Payment_Plan creation
                toggleLoader(false); // turn off the spinner icon
             }, // function(results)
            function(err) {
                toggleLoader(false); // turn off the spinner icon
                console.log(err); // display the error
            } // function(err)
        ); // PaymentService.get.$promise.then
    };


    /*
     * update a Recurring Plan Record based on form input
     *
     */
    $scope.doUpdateRecurringPlan = function() {
        toggleLoader(true);
        var fieldArray = createRecurringPlanObject(); // generate the object with form values  
        PaymentService.get({ // get the payment plan for this merchant ID/product ID
            paymentId: Authentication.user.Boarding_ID,
            productId: $scope.productId.Product_ID
        }).$promise.then(
            function(results) {
                fieldArray.Boarding_ID = Authentication.user.Boarding_ID; // set the boarding ID for this plan
                fieldArray.Processor_ID = $scope.originalPaymentPlan.Processor_ID; // set the processor ID for this plan
                recordRecurringPlan($scope.originalPaymentPlan, fieldArray); // record and audit the Payment_Plan creation
                //document.getElementById('fieldsForm').style.display='none'; // hide the form
                //document.getElementById('transactionCompleted').style.display='block'; // show the modal popup
                
                // mock up a response to the display confirmation modal
                var infoarray = {
                    'title': 'Updated Recurring Plan',
                    'message': 'Plan Updated.',
                    'accept': '',
                    'deny': 'OK'
                };
                $scope.confirmationModal('', infoarray); 
                //$scope.saletest = results;
                //$scope.$broadcast('succesfullResult'); // broadcast to display the popup modal
                
                toggleLoader(false); // turn off the spinner icon
            }, // function(results)
            function(err) {
                toggleLoader(false); // turn off the spinner icon
                console.log(err); // display the error
            } // function(err)
        ); // PaymentService.get.$promise.then
    };
 

    /*
     * Builds a list of Recurring Plans to display on the list screen
     *
     */
    $scope.recurringPlansWrap = function() {
        toggleLoader(true);
        $scope.noResults = false;
        $scope.showRecurringPlans = false;
        $scope.items = []; // the list that will be displayed
        var j = 0;
        //$scope.paymentPlansArray = [];
        var boardingId = Authentication.user.Boarding_ID;
        var billingCycle = '';
        var frequency = '';
        var ordinal = ["th","st","nd","rd"];
        
        // get all payment plans for this merchant id
        PaymentService.get({
            paymentId: boardingId,
            productId: 'all'
        }).$promise.then(function(payment_results) {
                delete payment_results.$promise; // don't need this
                delete payment_results.$resolved; // don't need this
                // get all the recurring payments plans for this merchant id
                RecurringPaymentPlanService.get({
                    boarding: boardingId, 
                    product: $scope.productId.productName, 
                    plan: $scope.plan,
                }).$promise.then(function(recurringPlan) {
                    recurringPlan.response.forEach(function(item) {
                        for(j = 0; j < Object.keys(payment_results).length; j++) {
                            if (payment_results[j].Processor_ID === item.Processor_ID) { // match! add to the list
                                ProductService.get({
                                    productId: payment_results[j].Product_ID
                                }).$promise.then(function(product_result) {
                                    // build display for billing cycle field
                                    if ( item.Month_Frequency === 0 && item.Day_Of_Month === 0 ) {
                                        billingCycle = 'Runs every ' + item.Day_Frequency + ' day(s)';
                                    } else {
                                        if (item.Month_Frequency > 1) {
                                            billingCycle = 'Runs every ' + item.Month_Frequency + ' month(s)';
                                        } else {
                                            // calculate ordinal (1st, 2nd, etc.)    
                                            billingCycle = 'Runs every ' + item.Day_Of_Month + (ordinal[((item.Day_Of_Month%100)-20)%10]||ordinal[(item.Day_Of_Month%100)]||ordinal[0]) + ' day of the month';
                                        } // if (item.Month_Frequency > 1) 
                                    } // if ( item.Month_Frequency === 0 && item.Day_Of_Month === 0 )
                                    
                                    if(item.Plan_Payments === 0) {
                                        frequency = 'Charge until cancelled';
                                    } else {
                                        frequency = 'Charge a total of ' + item.Plan_Payments + ' times';
                                    }

                                    // add this item to the list 
                                    $scope.items.push({
                                        id: item.id,
                                        Product_Description: product_result.Description,
                                        Plan_Name: item.Plan_Name,
                                        Plan_ID: item.Plan_ID,
                                        description: billingCycle,
                                        frequency: frequency,
                                        Plan_Amount: item.Plan_Amount,
                                        Recurr_ID: item.Recurr_ID
                                    });
                                    
                                    $scope.noResults = false;
                                    $scope.showRecurringPlans = true;
                               }); // ProductService.get().$promise.then
                            } // if (payment_results[j].Processor_ID === item.Processor_ID) 
                        } // for(var j = 0; j < Object.keys(payment_results).length; j++)
                    }); // recurringPlan.response.forEach(function(item)
                    if (j === 0 || $scope.items.length < 1) {
                        $scope.noResults = true;
                    }
                }); // RecurringPaymentPlanService.get().$promise.then
        }); // PaymentService.get.$promise.then
        toggleLoader(false);
    };


    $scope.getRecurringPlanList = function() {
        var deferred = $q.defer();
        var arrayMerchantPlans = [];
        RecurringPaymentPlanService.get({boardingId: Authentication.user.Boarding_ID, productId: 'all'})
        .$promise.then(function(planResults) {
            for(var i = 0; i < Object.keys(planResults.response).length; i++) {
                arrayMerchantsPlans.push({id:planResults.response[i].id, value: planResults.response[i].Plan_Name});
                deferred.resolve(arrayMerchantPlans);
            }
        });
        return deferred.promise;
    };
    
    
    $scope.recurringPlansQueryInitialize = function() {
        arrayMerchantsPlans = [{id:'all', value:'All Plans'}];
        var junk = $scope.getRecurringPlanList();
        junk.then(function() {
            $scope.samplePlans = arrayMerchantsPlans;
            $scope.Plan_NameSearchTransactions = arrayMerchantsPlans[0];
        });
    };

};
