/// <reference path="../../../../typings/jquery/jquery.d.ts"/>
/// <reference path="../../../../typings/angularjs/angular.d.ts"/>

'use strict';

angular.module('creditcard').dropdownController = function($q,$scope,Authentication,PaymentService,Nmitransactions,ReserveService,$location, $anchorScroll,$filter,$parse,$modal,MasService,TransactionsService,ProcessorService,ProductService,MerchantReportService,RunningBalanceService,SettleService,DiscountService,TransactionFeeService,Account_BalanceService,disbursementsService,$http,AccountBalanceReportService,Users,ACHReportService,PaymentCompleteService,LineItemsService)
{
    $scope.setMerchantId = function(merchantid){
        $('#dropdownMenu1').parent().toggleClass('open');
        $scope.query = '';
        $scope.merchantId = merchantid;
        $scope.products = merchantid.products[0]; 
        
        // sets up the Product Name dropdown as a div with only a single item
        if (merchantid.products.length === 2) {            
            $scope.Product_NameMAR = merchantid.products[1]; // this gets reset below - BUG?
            $scope.Product_NameChangeMAR(); 
            $scope.Product_NameChangeUserMAR(); 
            $scope.Product_NameChangeABR();
            $scope.Product_NameChangeACH();           
        }
                
        // Austin        
        // could test to see what report were viewing before setting values?    
             
        $scope.showReportMAR = false;         
        $scope.showReportABR = false; 
        $scope.showReportACH = false; 
        $scope.showSeachTransactionsReport = false;
           
        // Austin - this is used to set the Prodcut Name to the first entry 'All Products' in the reports  
        $scope.Product_NameMAR = $scope.merchantId.products[0]; 
        $scope.Product_NameUserMAR = $scope.merchantId.products[0]; 
        $scope.Product_NameABR = $scope.merchantId.products[0]; 
        $scope.Product_NameACH = $scope.merchantId.products[0]; 
    };         


    $scope.setProducts = function(productid,open){
        $('#dropdownMenu3').parent().toggleClass('open');
        $scope.queryproduct = '';
        $scope.products = productid;              
                     
         // Austin
         $scope.showReportMAR = false;
         $scope.showReportABR = false;   
         $scope.showReportACH = false;  
         $scope.showReportABR_Merchant = false; 
    };

    
    $scope.setProductId = function(productid){
        $('#dropdownMenu2').parent().toggleClass('open');
        $scope.queryproductid = '';
        $scope.productId = productid;
        $scope.reportProcessed = false;

        // Austin
        $scope.showReportMAR = false;
        $scope.showReportABR = false;
        $scope.showReportACH = false;   
        $scope.showSeachTransactionsReport = false;   
        //$scope.showReportABR_Merchant = false;
        
        // Austin - I changed this to use a boolean value with an ng-show
        //var hideDiv = document.getElementById('report-body');        
        // if (hideDiv !== null){
        //      hideDiv.className = hideDiv.className + " ng-hide";
        // }
        // hideDiv = document.getElementById('export-buttons');
        // if (hideDiv !== null){
        //     hideDiv.className = hideDiv.className + " ng-hide";
        // }
    };
    
    
    $scope.setProductId8 = function(productid){
        $('#dropdownMenu8').parent().toggleClass('open');
        $scope.queryproductid = '';
        $scope.productId = productid; 
        for(var i = 0; i < $scope.dashboardData.length; i++){
            if ($scope.dashboardData[i].productId === productid) {
                $scope.productIndex = i;
            }
        }
        if (document.getElementById('dayButton') !== undefined && document.getElementById('dayButton') !== null) {
            if (document.getElementById('dayButton').getAttribute("class").indexOf('active') > -1) {
                $scope.DashboardActivityWrap('day');
            }else if (document.getElementById('weekButton').getAttribute("class").indexOf('active') > -1) {
                $scope.DashboardActivityWrap('week');
            }else if (document.getElementById('monthButton').getAttribute("class").indexOf('active') > -1) {
                $scope.DashboardActivityWrap('month');
            }else if (document.getElementById('yearButton').getAttribute("class").indexOf('active') > -1) {
                $scope.DashboardActivityWrap('year');
            }
        }
    };
};
