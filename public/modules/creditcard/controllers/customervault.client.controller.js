'use strict';

angular.module('creditcard').CustomerVaultController = function($scope,$location,Authentication,VaultService)
{
    var param = $location.path();
    $scope.param = param.split('/');
    
    /*
     * Check if this is a create record session or if we are modifying an existing record
     *
     */
    $scope.checkVaultCreate = function () {
        if ($scope.param[1] === 'create' && $scope.param[2] === undefined) {
            // we are creating Customer Vault from scratch
            return true;
        } else { // if ($scope.param[1] === 'create' && $scope.param[2] === undefined) {
            // we are modifying and existing Customer Vault
            return false;
        }
    };


    $scope.custVaultInitialize = function() {
        if (!$scope.checkVaultCreate()) {
            // modifying a record - load this record's data into the form
            VaultService.get({id: $scope.param[2]}).$promise.then(
                function(vault){
                    vault.response.forEach(function(vaultRecord){
                        $scope.customerVaultId = vaultRecord.customer_vault_id;
                        $scope.creditCardNumber = vaultRecord.cc_number;
                        $scope.expirationDate = vaultRecord.cc_exp;
                        $scope.firstNameCard = vaultRecord.first_name;
                        $scope.lastNameCard = vaultRecord.last_name;
                        $scope.companyCard = vaultRecord.company;
                        $scope.countryCard = vaultRecord.country;
                        $scope.addressCard = vaultRecord.address_1;
                        $scope.addressContCard = vaultRecord.address_2;
                        $scope.cityCard = vaultRecord.city;
                        $scope.stateProvinceCard = vaultRecord.state;
                        $scope.zipPostalCodeCard = vaultRecord.postal_code;
                        $scope.phoneNumberCard = vaultRecord.phone;
                        $scope.faxNumberCard = vaultRecord.fax;
                        $scope.emailAddressCard = vaultRecord.email;
                        if (
                            vaultRecord.first_name === vaultRecord.shipping_first_name &&
                            vaultRecord.last_name === vaultRecord.shipping_last_name &&
                            vaultRecord.company === vaultRecord.shipping_company &&
                            vaultRecord.country === vaultRecord.shipping_country &&
                            vaultRecord.address_1 === vaultRecord.shipping_address_1 &&
                            vaultRecord.address_2 === vaultRecord.shipping_address_2 &&
                            vaultRecord.city === vaultRecord.shipping_city &&
                            vaultRecord.state === vaultRecord.shipping_state &&
                            vaultRecord.postal_code === vaultRecord.shipping_postal_code &&
                            vaultRecord.email === vaultRecord.shipping_email

                        ) {
                              $scope.sameBilling = true;                          
                        }
                        $scope.firstNameShipping = vaultRecord.shipping_first_name;
                        $scope.lastNameShipping = vaultRecord.shipping_last_name;
                        $scope.companyShipping = vaultRecord.shipping_company;
                        $scope.countryShipping = vaultRecord.shipping_country;
                        $scope.addressShipping = vaultRecord.shipping_address_1;
                        $scope.addressContShipping = vaultRecord.shipping_address_2;
                        $scope.cityShipping= vaultRecord.shipping_city;
                        $scope.stateProvinceShipping = vaultRecord.shipping_state;
                        $scope.zipPostalCodeShipping = vaultRecord.shipping_postal_code;
                        $scope.emailAddressShipping = vaultRecord.shipping_email;
                        
                    });
                },
                function(err){
                    console.log(err);
                    $scope.error = err;
                }
            );
        } else {
            // adding a record
        }
    };

};
