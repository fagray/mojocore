'use strict';

angular.module('creditcard').AuditRecurrPlanController = function($scope,Authentication,$location,AuditRecurrPlanService,AuditRecurrPlanHeaderService,AuditRecurrPlanDetailService) {

    var toggleLoader = function(val){
        if (val) { // show them
            document.getElementById('loadingImg').style.display='block';
            document.getElementById('submitButton').style.display='none';
        } else { // hide them
            document.getElementById('loadingImg').style.display='none';
            document.getElementById('submitButton').style.display='inline-block';
        } // if (val)
    };

    $scope.convertDate = function(start_end, textDate) {
        //convert seperators to dashes
        textDate = textDate.replace(/\//g,'-');
        textDate = textDate.replace(/\./g,'-');

        var dateElements = textDate.split('-'); // divide date into component parts

        // create a properly formatted date based on where the 4-digit year is
        var properlyFormattedDate;
        if (dateElements[0].length === 4) { // assume yyyy mm dd format
            properlyFormattedDate = dateElements[0] + '-' + ("0" + (dateElements[1])).slice(-2) + '-' + ("0" + (dateElements[2])).slice(-2); 
        } else if (dateElements[2].length === 4) { // assume mm-dd-yyyy format
            properlyFormattedDate = dateElements[2] + '-' + ("0" + (dateElements[0])).slice(-2) + '-' + ("0" + (dateElements[1])).slice(-2); 
        }

        // set midnight as time for start dates, 23:59:59 for end dates
        var fullDate;
        if (start_end === 'start') {
            fullDate = properlyFormattedDate + 'T00:00:00';
        } else {
            fullDate = properlyFormattedDate + 'T23:59:59';
        }

        // convert the text date to a date object
        var realDate = new Date(fullDate);
        return realDate;
    };

    $scope.Recurring_Changes_ActivityWrap = function() {
        toggleLoader(true); // turn on the spinner icon
        $scope.auditItems = [];
        $scope.alerts = [];
        $scope.showReport = false;
        $scope.noResults = false;
        $scope.error = '';
        
        // convert text dates into real date objects
        var start_date_obj = $scope.convertDate("start", $scope.startDate), 
            end_date_obj = $scope.convertDate("end", $scope.endDate);
            
        // error if dates are invalid
        if (start_date_obj.toString() === "Invalid Date") {
            $scope.alerts.push({type: 'danger', msg: 'The Start Date is invalid'});
        }
        if (end_date_obj.toString() === "Invalid Date") {
            $scope.alerts.push({type: 'danger', msg: 'The End Date is invalid'});
        }
//console.log($scope.alerts.length)        
        if ($scope.alerts.length === 0) {
            //set the parameters for the report based on user role
            var role = Authentication.user.Roles,
                productId = '',
                boardingId = '';
            if (role === 'user') {
                boardingId = Authentication.user.Boarding_ID;
                productId = $scope.productId.productName;
            } else if (role === 'admin' || role === 'super') {
                boardingId = $scope.merchantId.id;
                productId = $scope.products.productName;
            } // if (role === 'user')
            
            // get the report data
            AuditRecurrPlanService.get({
                start_date: start_date_obj,
                end_date: end_date_obj,
                productId: productId,
                boardingId : boardingId
            }).$promise.then(
                function(results) {
                    if (results.Response.length === 0) {
                        toggleLoader(false); // turn the spinner icon off
                        $scope.noResults = true; // show the No Results display
                        $scope.showReport = false; // hide the results panel
                    } else {
                        results.Response.forEach(function(list) {
                            $scope.auditItems.push(list); // add this record to the list
                            $scope.auditItems[$scope.auditItems.length - 1].createdAt = list.createdAt.replace(/\-/g,'').replace(/\:/g,'').replace('T',''); // format the date for the report
                        }); // results.Response.forEach(function(list)
                        toggleLoader(false); // turn the spinner icon off
                        $scope.noResults = false; // hide the No Results display
                        $scope.showReport = true; // show the results panel
                    } // if (results.Response.length === 0) 
                }, // function(results)
                function(err) {
                    console.log(err);
                    $scope.error = err;
                } // function(err)
            ); // AuditRecurrPlanService.get.$promise.then
        } else {
            toggleLoader(false);
        }// if ($scope.alerts.length === 0) 
    };


    $scope.Recurring_Changes_Detail_ActivityWrap = function() {
        $scope.auditItems = [];
        $scope.detailItems = [];
        $scope.error = '';
        var params = $location.path(),
            param = params.split('/');
        var description = '',
            text_oldValue = '',
            text_newValue = '';
        $scope.header_id = param[3]; // the plan

        // get the audit header
        AuditRecurrPlanHeaderService.get({ Header_ID: $scope.header_id  }).$promise.then(
            function(headerResults) {
                if (headerResults.Response.length !== 0) {
                    $scope.showReport = true;
                    $scope.auditItems.push(headerResults.Response[0]); // add this record to the list
                    $scope.auditItems[0].createdAt = headerResults.Response[0].createdAt.replace(/\-/g,'').replace(/\:/g,'').replace('T',''); // format the date for the report                  
                    // get the audit detail items
                    AuditRecurrPlanDetailService.get({ Audit_Header_ID: headerResults.Response[0].id  }).$promise.then(
                        function(detailResults) {
                            detailResults.Response.forEach(function(list) {
                                $scope.detailItems.push(list); // add to the list
                                
                                // describe some of the more unusual events
                                description = '';
                                text_oldValue = '';
                                text_newValue = '';
                                if (list.Field_Name === 'Plan_Payments' && list.Old_Value !== '0' && list.New_Value === '0' ) {
                                    description = 'Charge customer until cancelled';
                                    text_oldValue = 'Charge customer a total of ' +  list.Old_Value + ' times';
                                    text_newValue  = 'Charge customer until plan cancelled';
                                }
                                if (list.Field_Name === 'Plan_Payments' && list.New_Value !== '0' ) {
                                    description = 'Charge customer ' + list.New_Value + ' times';
                                    if (list.Old_Value === '0') {
                                        text_oldValue = 'Stop charging customer until plan cancelled';
                                    } else {
                                        text_oldValue = 'Charge customer ' + list.Old_Value  + ' times';
                                    }
                                    text_newValue  = 'Charge customer a total of ' + list.New_Value + ' times';
                                }
                                
                                
                                
                                
                                if (list.Field_Name === 'Day_Frequency' && list.Old_Value !== '0' && list.New_Value === '0' ) {
                                    description = 'Don\'t charge customer every ' + list.Old_Value + ' days';
                                    text_oldValue = 'Charge customer every ' + list.Old_Value + ' days';
                                    if (list.Old_Value === null && list.New_Value === '0') {
                                        text_newValue = 'Don\'t charge every "x" days';
                                    } else {
                                        text_newValue = ' ';
                                    }
                                }
                                if (list.Field_Name === 'Day_Frequency' && list.New_Value !== '0' ) {
                                    description = 'Charge customer every ' + list.New_Value + ' days';
                                    text_oldValue = '';
                                    text_newValue  = 'Charge customer every ' + list.New_Value + ' days';
                                }
                                
                                
                                
                                
                                if (list.Field_Name === 'Day_Of_Month' && list.Old_Value !== '0' && list.New_Value === '0' ) {
                                    description = 'Don\'t charge customer on day ' + list.Old_Value + ' of every "x" Months';
                                    text_oldValue = '';
                                    if (list.Old_Value !== '0' && list.Old_Value !== null) {
                                        text_newValue = 'Don\'t charge customer on day ' + list.Old_Value + ' of every "x" Months';
                                    } else {
                                        text_newValue = 'Don\'t charge customer on day "x" of every "y" Months';
                                    }
                                }
                                if (list.Field_Name === 'Day_Of_Month' && list.New_Value !== '0' ) {
                                    description = 'Charge customer on day ' + list.New_Value + ' of every "x" Months';
                                    if (list.Old_Value !== '0' && list.Old_Value !== null) {
                                        text_oldValue = 'Don\'t charge customer on day ' + list.New_Value + ' of every "x" Months';
                                    }
                                    text_newValue = 'Charge customer on day ' + list.New_Value + ' of every "x" Months';
                                                                             
                                }
                                
                                
                                
                                
                                if (list.Field_Name === 'Month_Frequency' && list.Old_Value !== '0' && list.New_Value === '0' ) {
                                    description = 'Don\'t charge customer on day "x" of every ' + list.Old_Value + ' Months';
                                    text_oldValue = '';
                                    if (list.Old_Value !== '0' && list.Old_Value !== null) {
                                        text_newValue = 'Don\'t charge customer on day "x" of every ' + list.Old_Value + ' Months';
                                    } else {
                                        text_newValue = 'Don\'t charge customer on day "x" of every "y" Months';
                                    }
                                }
                                
                                if (list.Field_Name === 'Month_Frequency' &&  list.New_Value !== '0' ) {
                                    description = 'Charge customer on day "x" of every ' + list.New_Value + ' Months';
                                    if (list.Old_Value !== '0') {
                                        text_oldValue = 'Don\'t charge customer on day "x" of every ' + list.Old_Value + ' Months';
                                    }
                                    text_newValue = 'Charge customer on day "x" of every ' + list.New_Value + ' Months';
                                }
                                
                                
                                
                                
                                
                                if (list.Old_Value === null) {
                                    description = 'Plan created';
                                    text_oldValue = '';
                                }
                                if (list.Field_Name === 'Is_Active' && list.Old_Value === '1' &&  list.New_Value === '0' ) {
                                    description = 'Plan deleted';
                                }
                                if (text_newValue !== '') {
                                    list.Old_Value = text_oldValue;
                                }
                                if (text_newValue !== '') {
                                    list.New_Value = text_newValue;
                                }
                                if (list.Field_Name === 'Plan_Amount') {
                                    if (list.Old_Value !== null) {
                                        list.Old_Value = parseFloat(list.Old_Value).toFixed(2);
                                    }
                                    if (list.New_Value !== null) {
                                        list.New_Value = parseFloat(list.New_Value).toFixed(2);
                                    }
                                }
                                $scope.detailItems[$scope.detailItems.length - 1].Description = description;
                            }); // detailResults.forEach(function(list)
                        }, // function(detailResults)
                        function(err) {
                            console.log(err);
                            $scope.error = err;
                        } // function(err)
                    ); // AuditRecurrPlanDetailService.get.$promise.then
                } // if (headerResults.Response.length === 0)
            }, // function(headerResults)
            function(err) {
                console.log(err);
                $scope.error = err;
            } // function(err)
        ); // AuditRecurrPlanHeaderService.get.$promise.then
    };
};
