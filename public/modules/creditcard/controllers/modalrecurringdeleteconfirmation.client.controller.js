'use strict';

angular.module('creditcard').controller('ModalRecurrDeleteConfCtrl',['$scope','$modalInstance', 'message',
    function ($scope, $modalInstance, message) {


        /**
         * ok - should return the passed plan
         */
        $scope.ok = function () {
            $modalInstance.close({planId: message.planId, delete: message.delete, id:message.id});
        };


        /**
         * Builds the modal view
         */
        $scope.getInformation = function(){
            $scope.title = message.title;
            $scope.message = message.message;
            $scope.accept = message.accept;
            $scope.deny = message.deny;
        };   

        /**
         * cancel - should return nothing
         */
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
            $modalInstance.close();
        };
}]);
