/// <reference path="../../../../typings/angularjs/angular.d.ts"/>

'use strict';

angular.module('creditcard').reportController = function($q,$scope,Authentication,PaymentService,Nmitransactions,ReserveService,$location, $anchorScroll,$filter,$parse,$modal,MasService,TransactionsService,ProcessorService,ProductService,MerchantReportService,RunningBalanceService,SettleService,DiscountService,TransactionFeeService,Account_BalanceService,disbursementsService,$http,AccountBalanceReportService,Users,ACHReportService,PaymentCompleteService,LineItemsService,RefundService)
{
    var toggleLoader = function(val) {
        if (val) {
            document.getElementById('loadingImg').style.display='block';
            document.getElementById('report-body').style.display='none';
        } else {
            document.getElementById('loadingImg').style.display='none';
            document.getElementById('report-body').style.display='block';
        }
    };
    
    $scope.MerchantActivityWrap = function() {
		// check to see if alerts are present, if they are do not allow submit
		if (typeof($scope.alerts) !== 'undefined')
		{
			if ($scope.alerts.length > 0)
			{	// alerts are present - do not allow submit
//				$scope.alerts.push({ type: 'danger', msg: 'Please correct errors before submitting' });
				return false;
			}
		}

        $scope.initPaginator();
        $scope.itemsMerchantActivity = [];    
        // Austin testing
        //console.log($scope.merchantNameMAR);
        //$scope.setMerchantId($scope.merchantNameMAR);        
        $scope.showReportMAR = true;
        $scope.noResults = false;

        var balanceAfter = 0;

        var startDate = $scope.startDate,
            endDate = $scope.endDateYesterday,
            priorDate = $scope.startDate,
            itemsMerchantActivityArray = [];
        startDate = startDate.split('-');
        startDate = startDate[0]+''+startDate[1]+''+startDate[2]+'000000';
        endDate = endDate.split('-');
        endDate = endDate[0]+''+endDate[1]+''+endDate[2]+'235959';
        priorDate = priorDate.split('-');
        priorDate = new Date(priorDate[0], priorDate[1] - 1, priorDate[2]);
        priorDate.setDate(priorDate.getDate() - 1);
        priorDate = ('0' + priorDate.getFullYear()).slice(-4) + 
                    ('0' + (priorDate.getMonth() + 1)).slice(-2) + 
                    ('0' + priorDate.getDate()).slice(-2) + 
                    '000000';

        var role = Authentication.user.Roles,
            varProductId = '',
            varBoardingId = '';
        if (role==='user'){
            varProductId = $scope.productId.productName;
            varBoardingId = Authentication.user.Boarding_ID;
        } else if (role==='admin' || role==='super') {
            varBoardingId = $scope.merchantId.id;
            varProductId = $scope.products.productName;
        }
        RunningBalanceService.get( {productId:varProductId,boardingId:varBoardingId,endDate:priorDate,startDate:priorDate}).$promise.then(function(priorACHBalances) {
            var priorDateBalances = priorACHBalances.Response;
            var priorBalance = 0;
            itemsMerchantActivityArray[0] = {
                                                productName: '',
                                                time: priorDate.slice(0,8),
                                                merchantId: '',
                                                merchantName: '',
                                                short_description: 'Ending Balance',
                                                whole_description: '',
                                                debit: '',
                                                credit: '',
                                                balance: priorBalance,
                                                link: '',
                                                occurrenceDateModal: '',
                                                filterTime: priorDate.slice(0,8)
                                            };
            priorDateBalances.forEach(function(balance){
                itemsMerchantActivityArray[0].balance += balance.Net_Amount;         
            });
            priorBalance = itemsMerchantActivityArray[0].balance;
            RunningBalanceService.get( {productId:varProductId,boardingId:varBoardingId,endDate:endDate,startDate:startDate}).$promise.then(
                function(Balance){
                var currentBalance = Balance.Response; 
    //console.log('Performing MerchantReportService with: ',varProductId, varBoardingId, $scope)
            MerchantReportService.get({productId:varProductId,boardingId:varBoardingId,endDate:endDate,startDate:startDate}).$promise.then(
                function(results){
                    
                    var calculations = results.Response, date = '', settle = 0, refund = 0, reserve = 0, goachreject = 0, achreject = 0,goachrejectfee = 0,achrejectfee = 0,
                        discount = 0, transactionFee = 0, splitPayment = 0, monthlyFee = 0, chargebacks = 0,chargebacksreversal= 0, goretrievalfee = 0,retrieval = 0,
                        sumBalance = 0, additionalCharges = 0,gosettle = 0, gorefund = 0, goreserve = 0,gochargebacksfee = 0,gochargebacksreversalfee = 0,
                        godiscount = 0, gotransactionFee = 0, gosplitPayment = 0, gomonthlyFee = 0, gochargebacks = 0,print = 0,chargebacksfee = 0,chargebacksreversalfee = 0,
                        gosumBalance = 0, goadditionalCharges = 0,gochargebacksreversal = 0, tempDate = '',counter= 0,length = Object.keys(calculations).length, flagBoardingId = '', flagMerchantId = -1, flagMerchantName = '', flagProductId = '', achPaymentsProcessed = [], goreserveadj = 0, reserveadj = 0, otherCharges = 0, gootherCharges = 0, goreserveincrease = 0, reserveincrease = 0, goachrejectamount = 0, achrejectamount = 0;
                    if (length !== 0){
                        date = calculations[0].Original_Date;
                        date = date.slice(0,8);
                        flagBoardingId = calculations[0].Boarding_ID;
                        flagMerchantId = calculations[0].Merchant_ID;
                        flagMerchantName = calculations[0].Merchant_Name;
                        flagProductId = calculations[0].Product_ID;
                    }

                    var getACHPayments = function(boarding_id) {
                        balanceAfter = priorBalance;
                        var achStartDate = date + '000000';
                        var achEndDate   = date + '235959';
                        var achLength = currentBalance.length;
                        var disbursementTotal;
                        var tempDate;
                        var achProductName;
                        if (boarding_id === 'all') { // display for all merchants
                            for (var i=0; i<achLength; i++) { 
                                if ((achPaymentsProcessed.indexOf(currentBalance[i].Boarding_ID) === -1 &&
                                    (currentBalance[i].Date >= achStartDate && currentBalance[i].Date <= achEndDate)) && 
                                    currentBalance[i].Disbursement !== 0) {
                                    disbursementTotal = currentBalance[i].Disbursed_Transactions_Amount - currentBalance[i].Disbursed_Fees_Amount;
                                    tempDate = currentBalance[i].Date;
                                    tempDate = tempDate.slice(0,8);
                                    if (currentBalance[i].Description === null || currentBalance[i].Description === '') {
                                        achProductName = 'MojoCore';
                                    } else {
                                        achProductName = currentBalance[i].Description;
                                    }

                                    if (disbursementTotal < 0) {
                                        balanceAfter = balanceAfter - disbursementTotal;
                                        itemsMerchantActivityArray.push({
                                            productName: achProductName,
                                            time: tempDate,
                                            merchantId: currentBalance[i].Merchant_ID,
                                            merchantName: currentBalance[i].Merchant_Name,
                                            short_description: 'ACH Credit - Account Funding',
                                            whole_description: '',
                                            debit: '',
                                            credit: disbursementTotal,
                                            balance: balanceAfter,
                                            link: '',
                                            occurrenceDateModal: '',
                                            filterTime: tempDate
                                        });
                                        priorBalance = balanceAfter;
                                    }else{
                                        balanceAfter = balanceAfter - disbursementTotal;
                                        itemsMerchantActivityArray.push({
                                            productName: achProductName,
                                            time: tempDate,
                                            merchantId: currentBalance[i].Merchant_ID,
                                            merchantName: currentBalance[i].Merchant_Name,
                                            short_description: 'ACH Disbursement',
                                            whole_description: '',
                                            debit: disbursementTotal,
                                            credit: '',
                                            balance: balanceAfter,
                                            link: '',
                                            occurrenceDateModal: '',
                                            filterTime: tempDate
                                        });
                                        priorBalance = balanceAfter;
                                    }
                                }
                            } // for
                            achPaymentsProcessed = []; // clear the list of already displayed merchants
                        } else { // display for a specific merchant
                            for (var j=0; j<achLength; j++) { 
                                if (currentBalance[j].Boarding_ID === boarding_id && 
                                    (currentBalance[j].Date >= achStartDate && currentBalance[j].Date <= achEndDate) && 
                                    currentBalance[j].Disbursement !== 0) {
                                    disbursementTotal = currentBalance[j].Disbursed_Transactions_Amount - currentBalance[j].Disbursed_Fees_Amount;
                                    tempDate = currentBalance[j].Date;
                                    tempDate = tempDate.slice(0,8);
                                    if (currentBalance[j].Description === null || currentBalance[j].Description === '') {
                                        achProductName = 'MojoCore';
                                    } else {
                                        achProductName = currentBalance[j].Description;
                                    }

                                    if (disbursementTotal < 0) {
                                        balanceAfter = balanceAfter - disbursementTotal;
                                        itemsMerchantActivityArray.push({
                                            productName: achProductName,
                                            time: tempDate,
                                            merchantId: currentBalance[j].Merchant_ID,
                                            merchantName: currentBalance[j].Merchant_Name,
                                            short_description: 'ACH Credit - Account Funding',
                                            whole_description: '',
                                            debit: '',
                                            credit: disbursementTotal,
                                            balance: balanceAfter,
                                            link: '',
                                            occurrenceDateModal: '',
                                            filterTime: tempDate
                                        });
                                        priorBalance = balanceAfter;

                                    }else{
                                        balanceAfter = balanceAfter - disbursementTotal;
                                        itemsMerchantActivityArray.push({
                                            productName: achProductName,
                                            time: tempDate,
                                            merchantId: currentBalance[j].Merchant_ID,
                                            merchantName: currentBalance[j].Merchant_Name,
                                            short_description: 'ACH Disbursement',
                                            whole_description: '',
                                            debit: disbursementTotal,
                                            credit: '',
                                            balance: balanceAfter,
                                            link: '',
                                            occurrenceDateModal: '',
                                            filterTime: tempDate
                                        });
                                        priorBalance = balanceAfter;
                                    }
                                }
                            } // for
                            achPaymentsProcessed.push(boarding_id); // add merchant to the list of already displayed ones
                        }
                        sumBalance = priorBalance;
                    };
               
                    calculations.forEach(function(calculation){ 
                        sumBalance = priorBalance;
                        counter++;
                        var firstDate = calculation.Original_Date,
                            firstBoardingId = calculation.Boarding_ID,
                            initialDate,
                            merchant_id = calculation.Merchant_ID,
                            merchant_name = calculation.Merchant_Name,
                            boarding_id = calculation.Boarding_ID,
                            product_id = varProductId;
                            merchant_id = flagMerchantId;
                            merchant_name = flagMerchantName;
                        firstDate = firstDate.slice(0,8);
                        var createObjects = function(){
                            tempDate = date;
                            initialDate = tempDate + '000000';
                            // Sale - Refund
                            if (gosettle !== 0) {
                                sumBalance += settle;
                                var arr ={
                                    productName: '',
                                    type:'settle',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    time: tempDate,
                                    short_description: 'Settle ',
                                    whole_description: '',
                                    balance: sumBalance,
                                    occurrenceDateModal: '',
                                    filterTime: tempDate};
                                if (role==='user'){
                                    arr.link ='#!/reports/settlement_transaction_details/' + Authentication.user.Boarding_ID + '/' + initialDate + '/' + $scope.productId.productName;
                                } else if (role==='admin' || role==='super') {
                                    arr.link ='#!/reports/settlement_transaction_details/' + flagBoardingId + '/' + initialDate + '/' + product_id;
                                }
                                if (settle>0){
                                    arr.debit ='';
                                    arr.credit = settle;
                                }else{
                                    arr.debit = settle;
                                    arr.credit ='';
                                }
                                itemsMerchantActivityArray.push(arr);
                                print = 1;
                            }
                            //Reserve
                            if (goreserve !== 0) {
                                sumBalance -= reserve;
                                var arr0 = {
                                    productName: '',
                                    type:'reserve',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    time: '',
                                    short_description: 'Reserve',
                                    whole_description: '',
                                    debit: reserve,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (print === 0){
                                    arr0.time = tempDate;
                                }else{
                                    arr0.time = '';
                                    arr0.merchantId = '';
                                    arr0.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr0);
                                print = 1;
                            }
                            //Discount
                            if (godiscount !== 0) {
                                sumBalance -= discount;
                                var arr1 = {
                                    productName: '',
                                    type:'discount',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    time: '',
                                    short_description: 'Discount Fee',
                                    whole_description: '',
                                    debit: discount,
                                    credit: '',
                                    balance: sumBalance,
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (role==='user'){
                                    arr1.link = '#!/reports/discount_fees/' + Authentication.user.Boarding_ID + '/' + initialDate + '/' + $scope.productId.productName;
                                } else if (role==='admin' || role==='super') {
                                    arr1.link ='#!/reports/discount_fees/' + flagBoardingId + '/' + initialDate + '/' + product_id;
                                }
                                if (print === 0){
                                    arr1.time = tempDate;
                                }else{
                                    arr1.time = '';
                                    arr1.merchantId = '';
                                    arr1.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr1);
                                print = 1;
                            }
                            //Auth Fee + Refund Fee + Void Fee
                            if (gotransactionFee !== 0 ) {
                                sumBalance -= transactionFee;
                                var arr2 = {
                                    productName: '',
                                    type:'transactionfee',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'Transaction Fee ',
                                    whole_description: '',
                                    debit: transactionFee,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (role==='user'){
                                    arr2.link = '#!/reports/transaction_fees/' + Authentication.user.Boarding_ID + '/' + initialDate + '/' + $scope.productId.productName;
                                } else if (role==='admin' || role==='super') {
                                    arr2.link ='#!/reports/transaction_fees/' + flagBoardingId + '/' + initialDate + '/' + product_id;
                                }
                                if (print === 0){
                                    arr2.time = tempDate;
                                }else{
                                    arr2.time = '';
                                    arr2.merchantId = '';
                                    arr2.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr2);
                                print = 1;
                            }
                            //Split
                            if (gosplitPayment !== 0 ) {
                                sumBalance -= splitPayment;
                                var arr3 = {
                                    productName: '',
                                    type:'split',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'Commission ',
                                    whole_description: '',
                                    debit: splitPayment,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (print === 0){
                                    arr3.time = tempDate;
                                }else{
                                    arr3.time = '';
                                    arr3.merchantId = '';
                                    arr3.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr3);
                                print = 1;
                            }
                            //Monthly Fee
                            if (gomonthlyFee !== 0 ) {
                                sumBalance -= monthlyFee;
                                var arr4 = {
                                    productName: '',
                                    type:'monthlyfee',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'Monthly Fee ',
                                    whole_description: '',
                                    debit: monthlyFee,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (print === 0){
                                    arr4.time = tempDate;
                                }else{
                                    arr4.time = '';

                                    arr4.merchantId = '';
                                    arr4.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr4);
                                print = 1;
                            }
                            //Chargeback
                            if (gochargebacks !== 0 ) {
                                sumBalance -= chargebacks;
                                var arr5 = {
                                    productName: '',
                                    type:'chargeback',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'Chargeback ',
                                    whole_description: '',
                                    debit: chargebacks,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (role==='user'){
                                    arr5.link = '#!/reports/chargeback/' + Authentication.user.Boarding_ID + '/' + initialDate + '/' + $scope.productId.productName;
                                } else if (role==='admin' || role==='super') {
                                    arr5.link ='#!/reports/chargeback/' + flagBoardingId + '/' + initialDate + '/' + product_id;
                                }
                                if (print === 0){
                                    arr5.time = tempDate;
                                }else{
                                    arr5.time = '';
                                    arr5.merchantId = '';
                                    arr5.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr5);
                                print = 1;
                            }
                            //Chargeback_Fee
                            if (gochargebacksfee !== 0 ) {
                                sumBalance -= chargebacksfee;
                                var arr6 = {
                                    productName: '',
                                    type:'chargebackfee',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'Chargeback Fee ',
                                    whole_description: '',
                                    debit: chargebacksfee,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (print === 0){
                                    arr6.time = tempDate;
                                }else{
                                    arr6.time = '';
                                    arr6.merchantId = '';
                                    arr6.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr6);
                                print = 1;
                            }
                            //Chargeback_Reversal
                            if (gochargebacksreversal !== 0 ) {
                                sumBalance += chargebacksreversal;
                                var arr7 = {
                                    productName: '',
                                    type:'chargebackreversal',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'Chargeback Reversal ',
                                    whole_description: '',
                                    debit: '',
                                    credit: chargebacksreversal,
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (role==='user'){
                                    arr7.link = '#!/reports/chargeback_reversal/' + Authentication.user.Boarding_ID + '/' + initialDate + '/' + $scope.productId.productName;
                                } else if (role==='admin' || role==='super') {
                                    arr7.link ='#!/reports/chargeback_reversal/' + flagBoardingId + '/' + initialDate + '/' + product_id;
                                }
                                if (print === 0){
                                    arr7.time = tempDate;
                                }else{
                                    arr7.time = '';
                                    arr7.merchantId = '';
                                    arr7.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr7);
                                print = 1;
                            }
                            //Chargeback_Reversal_Fee
                            if (gochargebacksreversalfee !== 0 ) {
                                sumBalance -= chargebacksreversalfee;
                                var arr8 = {
                                    productName: '',
                                    type:'chargebackreversalfee',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'Chargeback Reversal Fee ',
                                    whole_description: '',
                                    debit: chargebacksreversalfee,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (print === 0){
                                    arr8.time = tempDate;
                                }else{
                                    arr8.time = '';
                                    arr8.merchantId = '';
                                    arr8.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr8);
                                print = 1;
                            }
                            //Retrieval Fee
                            if (goretrievalfee !== 0 ) {
                                sumBalance -= retrieval;
                                var arr9 = {
                                    productName: '',
                                    type:'retrievalfee',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'Retrieval Fee ',
                                    whole_description: '',
                                    debit: retrieval,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (role==='user'){
                                    arr9.link = '#!/reports/retrieval/' + Authentication.user.Boarding_ID + '/' + initialDate + '/' + $scope.productId.productName;
                                 } else if (role==='admin' || role==='super') {
                                    arr9.link ='#!/reports/retrieval/' + flagBoardingId + '/' + initialDate + '/' + product_id;
                                }
                                if (print === 0){
                                    arr9.time = tempDate;
                                }else{
                                    arr9.time = '';
                                    arr9.merchantId = '';
                                    arr9.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr9);
                                print = 1;
                            }
                            //Ach Reject
                            if (goachreject !== 0 ) {
                                sumBalance -= achreject;
                                var arr10 = {
                                    productName: '',
                                    type:'achreject',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'ACH Reject Fee',
                                    whole_description: '',
                                    debit: achreject,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (role==='user'){
                                    arr10.link = '#!/reports/achreject/' + Authentication.user.Boarding_ID + '/' + initialDate + '/' + $scope.productId.productName;
                                 } else if (role==='admin' || role==='super') {
                                    arr10.link ='#!/reports/achreject/' + flagBoardingId + '/' + initialDate + '/' + product_id;
                                }
                                if (print === 0){
                                    arr10.time = tempDate;
                                }else{
                                    arr10.time = '';
                                    arr10.merchantId = '';
                                    arr10.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr10);
                                print = 1;
                            }
                            //Ach Reject Fee
                            if (goachrejectfee !== 0 ) {
                                sumBalance -= achrejectfee;
                                var arr11 = {
                                    productName: '',
                                    type:'achrejectfee',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'ACH Reject Fee ',
                                    whole_description: '',
                                    debit: achrejectfee,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (print === 0){
                                    arr11.time = tempDate;
                                }else{
                                    arr11.time = '';
                                    arr11.merchantId = '';
                                    arr11.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr11);
                                print = 1;
                            }
                            //Misc
                            if (goadditionalCharges !== 0 ) {
                                var arr12;
                                sumBalance = sumBalance + parseFloat(additionalCharges);
    
                                if(additionalCharges < 0) {
                                    arr12 = {
                                        productName: '',
                                        type:'misc',
                                        merchantId: merchant_id,
                                        merchantName: merchant_name,
                                        short_description: 'Misc. Fee ',
                                        whole_description: '',
                                        debit: additionalCharges,
                                        credit: '',
                                        balance: sumBalance,
                                        link: '',
                                        occurrenceDateModal: '',
                                        filterTime: tempDate
                                    };
                                }else{
                                    arr12 = {
                                        productName: '',
                                        type:'misc',
                                        merchantId: merchant_id,
                                        merchantName: merchant_name,
                                        short_description: 'Misc. Fee ',
                                        whole_description: '',
                                        debit: '',
                                        credit: additionalCharges,
                                        balance: sumBalance,
                                        link: '',
                                        occurrenceDateModal: '',
                                        filterTime: tempDate
                                    };
                                }
                                if (role==='user'){
                                    arr12.link = '#!/reports/misc/' + Authentication.user.Boarding_ID + '/' + initialDate + '/' + $scope.productId.productName;
                                 } else if (role==='admin' || role==='super') {
                                    arr12.link ='#!/reports/misc/' + flagBoardingId + '/' + initialDate + '/' + product_id;
                                }
                                if (print === 0){
                                    arr12.time = tempDate;
                                }else{
                                    arr12.time = '';
                                    arr12.merchantId = '';
                                    arr12.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr12);
                                print = 1;
                            }
                            //Refund
                            if (gorefund !== 0 ) {
                                sumBalance += refund;
                                var arr13 = {
                                    productName: '',
                                    type:'refund',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'Refund ',
                                    whole_description: '',
                                    debit: refund,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (role==='user'){
                                    arr13.link = '#!/reports/refund_transaction_details/' + Authentication.user.Boarding_ID + '/' + initialDate + '/' + $scope.productId.productName;
                                } else if (role==='admin' || role==='super') {
                                    arr13.link ='#!/reports/refund_transaction_details/' + flagBoardingId + '/' + initialDate + '/' + product_id;
                                }
                                if (print === 0) {
                                    arr13.time = tempDate;
                                }else{
                                    arr13.time = '';
                                    arr13.merchantId = '';
                                    arr13.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr13);
                                print = 1;
                            }
                            //RESERVE_ADJUSTMENT
                            if (goreserveadj !== 0 ) {
                                sumBalance += reserveadj;
                                var arr14 = {
                                    productName: '',
                                    type:'reserve_adjustment',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'Reserve Release ',
                                    whole_description: '',
                                    debit: '',
                                    credit: reserveadj,
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (role==='user'){
                                    //arr14.link = '';
                                } else if (role==='admin' || role==='super') {
                                    //arr14.link ='';
                                }
                                if (print === 0) {
                                    arr14.time = tempDate;
                                }else{
                                    arr14.time = '';
                                    arr14.merchantId = '';
                                    arr14.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr14);
                                print = 1;

                            }
                            //OTHER
                            if (gootherCharges !== 0 ) {
                                var arr15;
                                sumBalance = sumBalance + parseFloat(otherCharges);
    
                                if(otherCharges < 0) {
                                    arr15 = {
                                        productName: '',
                                        type:'other',
                                        merchantId: merchant_id,
                                        merchantName: merchant_name,
                                        short_description: 'Other ',
                                        whole_description: '',
                                        debit: otherCharges,
                                        credit: '',
                                        balance: sumBalance,
                                        link: '',
                                        occurrenceDateModal: '',
                                        filterTime: tempDate
                                    };
                                }else{
                                    arr15 = {
                                        productName: '',
                                        type:'other',
                                        merchantId: merchant_id,
                                        merchantName: merchant_name,
                                        short_description: 'Other ',
                                        whole_description: '',
                                        debit: '',
                                        credit: otherCharges,
                                        balance: sumBalance,
                                        link: '',
                                        occurrenceDateModal: '',
                                        filterTime: tempDate
                                    };
                                }
                                if (print === 0){
                                    arr15.time = tempDate;
                                }else{
                                    arr15.time = '';
                                    arr15.merchantId = '';
                                    arr15.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr15);
                                print = 1;
                            }
                            //RESERVE_INCREASE
                            if (goreserveincrease !== 0 ) {
                                sumBalance += reserveincrease;
                                var arr16 = {
                                    productName: '',
                                    type:'reserve_increase',
                                    merchantId: merchant_id,
                                    merchantName: merchant_name,
                                    short_description: 'Reserve Increase ',
                                    whole_description: '',
                                    debit: reserveincrease,
                                    credit: '',
                                    balance: sumBalance,
                                    link: '',
                                    occurrenceDateModal: '',
                                    filterTime: tempDate
                                };
                                if (print === 0) {
                                    arr16.time = tempDate;
                                }else{
                                    arr16.time = '';
                                    arr16.merchantId = '';
                                    arr16.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr16);
                                print = 1;

                            }
                            //ACH_REJECT_AMOUNT
                            if (goachrejectamount !== 0 ) {
                                var arr17;
                                sumBalance = sumBalance + parseFloat(achrejectamount);
    
                                if(achrejectamount < 0) {
                                    arr17 = {
                                        productName: '',
                                        type:'misc',
                                        merchantId: merchant_id,
                                        merchantName: merchant_name,
                                        short_description: 'ACH Reject ',
                                        whole_description: '',
                                        debit: achrejectamount,
                                        credit: '',
                                        balance: sumBalance,
                                        link: '',
                                        occurrenceDateModal: '',
                                        filterTime: tempDate
                                    };
                                }else{
                                    arr17 = {
                                        productName: '',
                                        type:'misc',
                                        merchantId: merchant_id,
                                        merchantName: merchant_name,
                                        short_description: 'ACH Reject ',
                                        whole_description: '',
                                        debit: '',
                                        credit: achrejectamount,
                                        balance: sumBalance,
                                        link: '',
                                        occurrenceDateModal: '',
                                        filterTime: tempDate
                                    };
                                }
                                 if (print === 0){
                                    arr17.time = tempDate;
                                }else{
                                    arr17.time = '';
                                    arr17.merchantId = '';
                                    arr17.merchantName = '';
                                }
                                itemsMerchantActivityArray.push(arr17);
                                print = 1;
                            }
                            priorBalance = sumBalance;                               
//console.log(itemsMerchantActivityArray)                      
                        };


                        var savedDate;
                        // First record in the MAS recordset. Get all the earlier ACH adjustments we might miss otherwise
                        if (calculations[0] === calculation) {
                            savedDate = date;
                            for (var k1 = parseInt(startDate.slice(0,8)); k1 < parseInt(savedDate); k1++) {
                                date = k1;
                                getACHPayments('all');
                            }
                            date = savedDate;
                        }
                        
                        // date changed or merchant changed - display summaries and ACH lines
                        if (date !== firstDate || flagBoardingId !== firstBoardingId){
                            var achPayments;
                            createObjects(); // display summary MAR lines by type
                            getACHPayments(flagBoardingId); // display MAR ach payments for this merchant
                            
                            if (date !== firstDate) { //we changed dates, not just merchants...
                                // Get any interviening ACH adjustments we might miss between dates
                                savedDate = date;
                                for (var k3 = parseInt(savedDate); k3 < parseInt(firstDate); k3++) {
                                    date = k3;
                                    getACHPayments('all');
                                }
                                date = savedDate;
                            }
                            //sumBalance = 0;
                            gosumBalance = 0;
                            settle = 0;
                            gosettle = 0;
                            refund = 0;
                            gorefund = 0;
                            reserve = 0;
                            goreserve = 0;
                            discount= 0;
                            godiscount= 0;
                            transactionFee = 0;
                            gotransactionFee = 0;
                            splitPayment = 0;
                            gosplitPayment = 0;
                            monthlyFee = 0;
                            gomonthlyFee = 0;
                            chargebacks = 0;
                            gochargebacks = 0;
                            chargebacksreversal = 0;
                            gochargebacksreversal = 0;
                            chargebacksfee = 0;
                            gochargebacksfee = 0;
                            chargebacksreversalfee = 0;
                            gochargebacksreversalfee = 0;
                            achrejectfee = 0;
                            goachrejectfee = 0;
                            retrieval = 0;
                            goretrievalfee = 0;
                            achreject = 0;
                            goachreject = 0;
                            additionalCharges = 0;
                            goadditionalCharges = 0;
                            goreserveadj = 0;
                            reserveadj  = 0;
                            otherCharges = 0;
                            gootherCharges = 0;
                            goreserveincrease = 0;
                            reserveincrease  = 0;
                            achrejectamount = 0;
                            goachrejectamount = 0;
                            
                            print = 0;
                            
                            date = firstDate;
                            flagBoardingId = firstBoardingId;
                            flagMerchantId = calculation.Merchant_ID;
                            flagMerchantName = calculation.Merchant_Name;
                            flagProductId = calculation.Product_ID;
                         }
                         
                        if (calculation.Type === 'SALE'){
                            settle+= calculation.Original_Amount;
                            gosettle = 1;
                        }else if (calculation.Type === 'REFUND'){
                            refund-= calculation.Original_Amount;
                            gorefund = 1;
                        }else if (calculation.Type === 'RESERVE'){
                            reserve+= calculation.Amount;
                            goreserve = 1;
                        }else if (calculation.Type === 'DISCOUNT'){
                            godiscount = 1;
                            discount+= calculation.Amount;
                        }else if (calculation.Type === 'TRANSACTION_FEE'){
                            gotransactionFee = 1;
                            transactionFee+= calculation.Amount;
                        }else if (calculation.Type === 'REFUND_FEE'){
                            gotransactionFee = 1;
                            transactionFee+= calculation.Amount;
                        }else if (calculation.Type === 'VOID_FEE'){
                            gotransactionFee = 1;
                            transactionFee+= calculation.Amount;
                        }else if (calculation.Type === 'DECLINED_FEE'){
                            gotransactionFee = 1;
                            transactionFee+= calculation.Amount;
                        }else if (calculation.Type === 'SPLIT'){
                            gosplitPayment = 1;
                            splitPayment+= calculation.Amount;
                        }else if (calculation.Type === 'MONTHLY_FEE'){
                            gomonthlyFee = 1;
                            monthlyFee+= calculation.Amount;
                        }else if (calculation.Type === 'CHARGEBACK'){
                            gochargebacks = 1;
                            chargebacks+= calculation.Amount;
                        }else if (calculation.Type === 'CHARGEBACK_REVERSAL'){
                            gochargebacksreversal = 1;
                            chargebacksreversal+= calculation.Amount;
                        }else if (calculation.Type === 'MISC'){
                            goadditionalCharges = 1;
                            if(calculation.Payment_Type === 'CREDIT') {
                                additionalCharges += calculation.Amount;
                            } else {
                                additionalCharges -= calculation.Amount;
                            }
                        }else if (calculation.Type === 'CHARGEBACK_REVERSAL_FEE'){
                            gochargebacksreversalfee = 1;
                            chargebacksreversalfee+= calculation.Amount;
                        }else if(calculation.Type === 'CHARGEBACK_FEE'){
                            gochargebacksfee = 1;
                            chargebacksfee+= calculation.Amount;
                        }else if (calculation.Type === 'RETRIEVAL_FEE'){
                            goretrievalfee = 1;
                            retrieval+= calculation.Amount;
                        }else if (calculation.Type === 'ACH_REJECT'){
                            goachreject = 1;
                            achreject += calculation.Amount;
                        }else if (calculation.Type === 'ACH_REJECT_FEE'){
                            goachrejectfee = 1;
                            achrejectfee += calculation.Amount;
                        }else if (calculation.Type === 'RESERVE_ADJUSTMENT'){
                            goreserveadj = 1;
                            reserveadj += calculation.Amount;
                        }else if (calculation.Type === 'OTHER'){
                            gootherCharges = 1;
                            if(calculation.Payment_Type === 'CREDIT') {
                                otherCharges += calculation.Amount;
                            } else {
                                otherCharges -= calculation.Amount;
                            }
                        }else if (calculation.Type === 'RESERVE_INCREASE'){
                            goreserveincrease = 1;
                            reserveincrease -= calculation.Amount;
                        }else if (calculation.Type === 'ACH_REJECT_AMOUNT'){
                            goachrejectamount = 1;
                            if(calculation.Payment_Type === 'CREDIT') {
                                achrejectamount += calculation.Amount;
                            } else {
                                achrejectamount -= calculation.Amount;
                            }
                        }
                        // get all the remaining ACH adjustments we might miss
                        if (counter === length) { // only after last MAS item has been processed
                            createObjects();
                            getACHPayments('all');
                            for (var k2 = parseInt(date) + 1; k2 <= parseInt(endDate.slice(0,8)); k2++) {
                                date = k2;
                                getACHPayments('all');
                            }
                        }
                    });
                    if (counter === 0) {
                            getACHPayments('all');
                            for (var k2 = parseInt(date) + 1; k2 <= parseInt(endDate.slice(0,8)); k2++) {
                                date = k2;
                                getACHPayments('all');
                            }
                    }
                }).then(function(array){
                    // make the list viewable by the HTML view
                    $scope.itemsMerchantActivity = itemsMerchantActivityArray;
                    
/*
                   RunningBalanceService.get( {productId:varProductId,boardingId:varBoardingId,endDate:endDate,startDate:startDate}).$promise.then(
                        function(Balance){                        
                            var currentBalance = Balance.Response;
                            $scope.itemsMerchantActivity = [];
                            var length = Object.keys(currentBalance).length;
                            if (length === 0){
                                $scope.noResults = true;
                            }
                            currentBalance.forEach(function(day){
                                var tempDate = day.Date,
                                    tempBalance,
                                    disbursementTotal,
                                    balanceAfter,
                                    arrayDate = '';
                                tempDate = tempDate.slice(0,8);
                                itemsMerchantActivityArray.forEach(function(transaction){
                                    if (transaction.filterTime !== arrayDate){
                                        tempBalance = day.Previous_Net_Plus_Gross;
                                        arrayDate = transaction.filterTime;
                                    }
                                    if (transaction.filterTime === tempDate){
                                        if (transaction.type !== 'settle'){
                                            if (transaction.debit !== ''){
                                                tempBalance -= transaction.debit;
                                            }else{
                                                tempBalance += transaction.credit;
                                            }
                                        }
                                        if((day.Disbursement !== 0)&&(transaction.type === 'settle')){
                                            var disbursementTotal1 = day.Disbursed_Transactions_Amount - day.Disbursed_Fees_Amount;
                                            tempBalance += disbursementTotal1;
                                        }
                                        transaction.balance = tempBalance;
                                        $scope.itemsMerchantActivity.push(transaction);
                                    }
                                });
                                if (day.Disbursement !== 0){
                                    var last = Object.keys($scope.itemsMerchantActivity).length - 1,
                                        balanceLast;
                                    disbursementTotal = day.Disbursed_Transactions_Amount - day.Disbursed_Fees_Amount;
                                    if ($scope.itemsMerchantActivity[last] !== undefined){
                                        balanceLast = $scope.itemsMerchantActivity[last].balance;
                                    }else{
                                        balanceLast = disbursementTotal + day.Previous_Net_Plus_Gross;
                                    }
                                    balanceAfter = balanceLast - disbursementTotal;
                                    balanceAfter = (parseFloat(balanceAfter).toFixed(2)*100)/100;
                                    if (disbursementTotal < 0){
                                        $scope.itemsMerchantActivity.push({
                                            productName: '',
                                            time: tempDate,
                                            merchantId: day.Merchant_ID,
                                            merchantName: day.Merchant_Name,
                                            short_description: 'ACH Credit - Account Funding',
                                            whole_description: '',
                                            debit: '',
                                            credit: disbursementTotal,
                                            balance: balanceAfter,
                                            link: '',
                                            occurrenceDateModal: '',
                                            filterTime: tempDate
                                        });
                                    }else{
                                        $scope.itemsMerchantActivity.push({
                                            productName: '',
                                            time: tempDate,
                                            merchantId: day.Merchant_ID,
                                            merchantName: day.Merchant_Name,
                                            short_description: 'ACH Disbursement',
                                            whole_description: '',
                                            debit: disbursementTotal,
                                            credit: '',
                                            balance: balanceAfter,
                                            link: '',
                                            occurrenceDateModal: '',
                                            filterTime: tempDate
                                        });
                                    }
                                }
                            });
                        });
*/
                });
            });
       });
    };

    $scope.SettlementTransactionDetails = function(){
        $scope.initPaginator();
        $scope.items = [];
        $scope.sumAmountSettleReport = 0;
        var startDate,
            endDate,
            param = $location.path();
        param = param.split('/');
        startDate = param[4].substr(0, 8)+'000000';
        endDate =  param[4].substr(0, 8)+'235959';
        var role = Authentication.user.Roles,
            varProductId = '',
            varBoardingId = '';
        if (role==='user'){
            varProductId = param[5];
            varBoardingId = Authentication.user.Boarding_ID;
        } else if (role==='admin' || role==='super') {
            //varBoardingId = $scope.merchantId.id;
            //varProductId = $scope.products.productName;
            varBoardingId = param[3];
            varProductId = param[5];
       }
//console.log('Performing SettleService with: ',varProductId, varBoardingId, $scope)
        SettleService.get({productId:varProductId,boardingId:varBoardingId,endDate:endDate,startDate:startDate}).$promise.then(
            function(settled){
                var settledTransactions = settled.Response;
//                console.log(settled);
                $scope.items = settledTransactions;
                settledTransactions.forEach(function(item){
                    if (item.Type === 'SALE'){
                        $scope.sumAmountSettleReport += item.Original_Amount;
                    }else if (item.Type === 'REFUND'){
                        $scope.sumAmountSettleReport -= item.Original_Amount;
                    }

                });
            });
    };
    
    $scope.retrievalDetails = function(){
        $scope.initPaginator();
        var param = $location.path(),
            role = Authentication.user.Roles,
            varProductId,
            varBoardingId,
            endDate,
            startDate;
        param = param.split('/');
        startDate = param[4].substr(0, 8)+'000000';
        endDate =  param[4].substr(0, 8)+'235959';
        if (role==='user'){
            varProductId = param[5];
            varBoardingId = Authentication.user.Boarding_ID;
        } else if (role==='admin' || role==='super') {
            //varBoardingId = $scope.merchantId.id;
            //varProductId = $scope.products.productName;
            varBoardingId = param[3];
            varProductId = param[5];
        }
//console.log('Performing LineItemsService with: ',varProductId, varBoardingId, $scope)
        LineItemsService.get({
            productId:varProductId,
            boardingId:varBoardingId,
            endDate:endDate,
            startDate:startDate,
            type:'RETRIEVAL_FEE'}).$promise.then(
            function(transactions){
                $scope.itemsRetrievalDetailByDate = transactions.Response;
            }
        );
    };

    $scope.miscDetails = function(){
        $scope.initPaginator();
        var param = $location.path(),
            role = Authentication.user.Roles,
            varProductId,
            varBoardingId,
            endDate,
            startDate;
        param = param.split('/');
        startDate = param[4].substr(0, 8)+'000000';
        endDate =  param[4].substr(0, 8)+'235959';
        if (role==='user'){
            varProductId = param[5];
            varBoardingId = Authentication.user.Boarding_ID;
        } else if (role==='admin' || role==='super') {
            //varBoardingId = $scope.merchantId.id;
            //varProductId = $scope.products.productName;
            varBoardingId = param[3];
            varProductId = param[5];
        }
//console.log('Performing LineItemsService with: ',varProductId, varBoardingId, $scope)
        LineItemsService.get({
            productId:varProductId,
            boardingId:varBoardingId,
            endDate:endDate,
            startDate:startDate,
            type:'MISC'}).$promise.then(
            function(transactions){
                $scope.itemsMiscDetailByDate = transactions.Response;
            }
        );
    };

    $scope.achRejectDetails = function(){
       $scope.initPaginator();
       var param = $location.path(),
            role = Authentication.user.Roles,
            varProductId,
            varBoardingId,
            endDate,
            startDate;
        param = param.split('/');
        startDate = param[4].substr(0, 8)+'000000';
        endDate =  param[4].substr(0, 8)+'235959';
        if (role==='user'){
            varProductId = param[5];
            varBoardingId = Authentication.user.Boarding_ID;
        } else if (role==='admin' || role==='super') {
            //varBoardingId = $scope.merchantId.id;
            //varProductId = $scope.products.productName;
            varBoardingId = param[3];
            varProductId = param[5];
        }
//console.log('Performing LineItemsService with: ',varProductId, varBoardingId, $scope)
        LineItemsService.get({
            productId:varProductId,
            boardingId:varBoardingId,
            endDate:endDate,
            startDate:startDate,
            type:'ACH_REJECT'}).$promise.then(
            function(transactions){
                $scope.itemsACHDetailByDate = transactions.Response;
            }
        );
    };

    $scope.ChargebackReversalDetails = function(){
        $scope.initPaginator();
        var param = $location.path(),
            role = Authentication.user.Roles,
            varProductId,
            varBoardingId,
            endDate,
            startDate;
        param = param.split('/');
        startDate = param[4].substr(0, 8)+'000000';
        endDate =  param[4].substr(0, 8)+'235959';
        if (role==='user'){
            varProductId = param[5];
            varBoardingId = Authentication.user.Boarding_ID;
        } else if (role==='admin' || role==='super') {
            //varBoardingId = $scope.merchantId.id;
            //varProductId = $scope.products.productName;
            varBoardingId = param[3];
            varProductId = param[5];
        }
//console.log('Performing LineItemsService with: ',varProductId, varBoardingId, $scope)
        LineItemsService.get({
            productId:varProductId,
            boardingId:varBoardingId,
            endDate:endDate,
            startDate:startDate,
            type:'CHARGEBACK_REVERSAL'}).$promise.then(
            function(transactions){
                $scope.itemsChargebackReversalDetailByDate = transactions.Response;
            }
        );
    };

    $scope.ChargebackDetails = function(){
        $scope.initPaginator();
        var param = $location.path(),
            role = Authentication.user.Roles,
            varProductId,
            varBoardingId,
            endDate,
            startDate;
        param = param.split('/');
        startDate = param[4].substr(0, 8)+'000000';
        endDate =  param[4].substr(0, 8)+'235959';
        if (role==='user'){
            varProductId = param[5];
            varBoardingId = Authentication.user.Boarding_ID;
        } else if (role==='admin' || role==='super') {
            //varBoardingId = $scope.merchantId.id;
            //varProductId = $scope.products.productName;
            varBoardingId = param[3];
            varProductId = param[5];            
        }
//console.log('Performing LineItemsService with: ',varProductId, varBoardingId, $scope)
        LineItemsService.get({
            productId:varProductId,
            boardingId:varBoardingId,
            endDate:endDate,
            startDate:startDate,
            type:'CHARGEBACK'}).$promise.then(
            function(transactions){
                $scope.itemsChargebackDetailByDate = transactions.Response;
            }
        );
    };

    $scope.TransactionFees = function(){
        $scope.initPaginator();
        $scope.reportProcessed = false;
        $scope.sumAmountTransactionFeeReport = 0;
        var refundVisa = 0,
            authVisa = 0,
            voidVisa = 0,
            declineVisa = 0,
            refundMastercard = 0,
            authMastercard = 0,
            voidMastercard = 0,
            declineMastercard = 0,
            refundAmex = 0,
            authAmex = 0,
            voidAmex = 0,
            declineAmex = 0,
            refundDiscover = 0,
            authDiscover = 0,
            voidDiscover = 0,
            declineDiscover = 0,
            refundDiners = 0,
            authDiners = 0,
            voidDiners = 0,
            declineDiners = 0,
            refundDiners_carte = 0,
            authDiners_carte = 0,
            voidDiners_carte = 0,
            declineDiners_carte = 0,
            refundJcb = 0,
            authJcb = 0,
            voidJcb = 0,
            declineJcb = 0,
            refundVisa_electron = 0,
            authVisa_electron = 0,
            voidVisa_electron = 0,
            declineVisa_electron = 0,
            refundUnkown = 0,
            authUnkown = 0,
            voidUnkown = 0,
            declineUnkown = 0,
            countrefundVisa = 0,
            countauthVisa = 0,
            countvoidVisa = 0,
            countdeclineVisa = 0,
            countrefundMastercard = 0,
            countauthMastercard = 0,
            countvoidMastercard = 0,
            countdeclineMastercard = 0,
            countrefundAmex = 0,
            countauthAmex = 0,
            countvoidAmex = 0,
            countdeclineAmex = 0,
            countrefundDiscover = 0,
            countauthDiscover = 0,
            countvoidDiscover = 0,
            countdeclineDiscover = 0,
            countrefundDiners = 0,
            countauthDiners = 0,
            countvoidDiners = 0,
            countdeclineDiners = 0,
            countrefundDiners_carte = 0,
            countauthDiners_carte = 0,
            countvoidDiners_carte = 0,
            countdeclineDiners_carte = 0,
            countrefundJcb = 0,
            countauthJcb = 0,
            countvoidJcb = 0,
            countdeclineJcb = 0,
            countrefundVisa_electron = 0,
            countauthVisa_electron = 0,
            countvoidVisa_electron = 0,
            countdeclineVisa_electron = 0,
            countrefundUnkown = 0,
            countauthUnkown = 0,
            countvoidUnkown = 0,
            countdeclineUnkown = 0;

        $scope.items = [];
        var startDate,
            endDate,
            param = $location.path();
        param = param.split('/');
        startDate = param[4].substr(0, 8)+'000000';
        endDate =  param[4].substr(0, 8)+'235959';
        var role = Authentication.user.Roles,
            varProductId = '',
            varBoardingId = '';
        if (role==='user'){
            varProductId = param[5];
            varBoardingId = Authentication.user.Boarding_ID;
        } else if (role==='admin' || role==='super') {
            //varBoardingId = $scope.merchantId.id;
            //varProductId = $scope.products.productName;
            varBoardingId = param[3];
            varProductId = param[5];
        }
//console.log('Performing TransactionFeeService with: ',varProductId, varBoardingId, $scope)
        TransactionFeeService.get({productId:varProductId,boardingId:varBoardingId,endDate:endDate,startDate:startDate}).$promise.then(
            function(Fee){
                var Fees = Fee.Response;
                Fees.forEach(function(item) {
                    item.Brand = $scope.getBrand(item.cc_bin);
                    if (item.Brand==='Visa'){
                        if (item.Type === 'TRANSACTION_FEE'){
                            countauthVisa += item.Count;
                            authVisa += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'REFUND_FEE'){
                            countrefundVisa += item.Count;
                            refundVisa += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'VOID_FEE'){
                            countvoidVisa += item.Count;
                            voidVisa += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'DECLINED_FEE'){
                            countdeclineVisa += item.Count;
                            declineVisa += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }
                    }
                    if (item.Brand==='Mastercard'){
                        if (item.Type === 'TRANSACTION_FEE'){
                            countauthMastercard += item.Count;
                            authMastercard += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'REFUND_FEE'){
                            countrefundMastercard += item.Count;
                            refundMastercard += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'VOID_FEE'){
                            countvoidMastercard += item.Count;
                            voidMastercard += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'DECLINED_FEE'){
                            countdeclineMastercard += item.Count;
                            declineMastercard += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }
                    }
                    if (item.Brand==='AMEX'){
                        if (item.Type === 'TRANSACTION_FEE'){
                            countauthAmex += item.Count;
                            authAmex += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'REFUND_FEE'){
                            countrefundAmex += item.Count;
                            refundAmex += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'VOID_FEE'){
                            countvoidAmex += item.Count;
                            voidAmex += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'DECLINED_FEE'){
                            countdeclineAmex += item.Count;
                            declineAmex += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }
                    }
                    if (item.Brand==='Discover'){
                        if (item.Type === 'TRANSACTION_FEE'){
                            countauthDiscover += item.Count;
                            authDiscover+= (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'REFUND_FEE'){
                            countrefundDiscover += item.Count;
                            refundDiscover+= (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'VOID_FEE'){
                            countvoidDiscover += item.Count;
                            voidDiscover += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'DECLINED_FEE'){
                            countdeclineDiscover += item.Count;
                            declineDiscover += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }
                    }
                    if (item.Brand==='Diners'){
                        if (item.Type === 'TRANSACTION_FEE'){
                            countauthDiners += item.Count;
                            authDiners += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'REFUND_FEE'){
                            countrefundDiners += item.Count;
                            refundDiners += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'VOID_FEE'){
                            countvoidDiners += item.Count;
                            voidDiners += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'DECLINED_FEE'){
                            countdeclineDiners += item.Count;
                            declineDiners += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }
                    }
                    if (item.Brand==='Diners - Carte Blanche'){
                        if (item.Type === 'TRANSACTION_FEE'){
                            countauthDiners_carte += item.Count;
                            authDiners_carte += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'REFUND_FEE'){
                            countrefundDiners_carte += item.Count;
                            refundDiners_carte += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'VOID_FEE'){
                            countvoidDiners_carte += item.Count;
                            voidDiners_carte += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'DECLINED_FEE'){
                            countdeclineDiners_carte += item.Count;
                            declineDiners_carte += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }
                    }
                    if (item.Brand==='JCB'){
                        if (item.Type === 'TRANSACTION_FEE'){
                            countauthJcb += item.Count;
                            authJcb += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'REFUND_FEE'){
                            countrefundJcb += item.Count;
                            refundJcb += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'VOID_FEE'){
                            countvoidJcb += item.Count;
                            voidJcb += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'DECLINED_FEE'){
                            countdeclineJcb += item.Count;
                            declineJcb += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }
                    }
                    if (item.Brand==='Visa Electron'){
                        if (item.Type === 'TRANSACTION_FEE'){
                            countauthVisa_electron += item.Count;
                            authVisa_electron += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'REFUND_FEE'){
                            countrefundVisa_electron += item.Count;
                            refundVisa_electron += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'VOID_FEE'){
                            countvoidVisa_electron += item.Count;
                            voidVisa_electron += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'DECLINED_FEE'){
                            countdeclineVisa_electron += item.Count;
                            declineVisa_electron += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }
                    }
                    if (item.Brand===null){
                        if (item.Type === 'TRANSACTION_FEE'){
                            countauthUnkown += item.Count;
                            authUnkown += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'REFUND_FEE'){
                            countrefundUnkown += item.Count;
                            refundUnkown += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'VOID_FEE'){
                            countvoidUnkown += item.Count;
                            voidUnkown += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }else if (item.Type === 'DECLINED_FEE'){
                            countdeclineUnkown += item.Count;
                            declineUnkown += (parseFloat(item.Amount).toFixed(2)*100)/100;
                        }
                    }
                });
                if (countrefundVisa > 0){
                    $scope.sumAmountTransactionFeeReport += refundVisa;
                    $scope.items.push({
                        brand:'Visa',
                        count:countrefundVisa,
                        type:'Refund',
                        amount:refundVisa,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countauthVisa > 0){
                    $scope.sumAmountTransactionFeeReport += authVisa;
                    $scope.items.push({
                        brand:'Visa',
                        count:countauthVisa,
                        type:'Sale/Auth/Capture',
                        amount:authVisa,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countvoidVisa > 0){
                    $scope.sumAmountTransactionFeeReport += voidVisa;
                    $scope.items.push({
                        brand:'Visa',
                        count:countvoidVisa,
                        type:'Void',
                        amount:voidVisa,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countdeclineVisa > 0){
                    $scope.sumAmountTransactionFeeReport += declineVisa;
                    $scope.items.push({
                        brand:'Visa',
                        count:countdeclineVisa,
                        type:'Decline',
                        amount:declineVisa,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countrefundMastercard > 0){
                    $scope.sumAmountTransactionFeeReport += refundMastercard;
                    $scope.items.push({
                        brand:'Mastercard',
                        count:countrefundMastercard,
                        type:'Refund',
                        amount:refundMastercard,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countauthMastercard > 0){
                    $scope.sumAmountTransactionFeeReport += authMastercard;
                    $scope.items.push({
                        brand:'Mastercard',
                        count:countauthMastercard,
                        type:'Sale/Auth/Capture',
                        amount:authMastercard,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countvoidMastercard > 0){
                    $scope.sumAmountTransactionFeeReport += voidMastercard;
                    $scope.items.push({
                        brand:'Mastercard',
                        count:countvoidMastercard,
                        type:'Void',
                        amount:voidMastercard,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countdeclineMastercard > 0){
                    $scope.sumAmountTransactionFeeReport += declineMastercard;
                    $scope.items.push({
                        brand:'Mastercard',
                        count:countdeclineMastercard,
                        type:'Decline',
                        amount:declineMastercard,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countrefundAmex > 0){
                    $scope.sumAmountTransactionFeeReport += refundAmex;
                    $scope.items.push({
                        brand:'AMEX',
                        count:countrefundAmex,
                        type:'Refund',
                        amount:refundAmex,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countauthAmex > 0){
                    $scope.sumAmountTransactionFeeReport += authAmex;
                    $scope.items.push({
                        brand:'AMEX',
                        count:countauthAmex,
                        type:'Sale/Auth/Capture',
                        amount:authAmex,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countvoidAmex > 0){
                    $scope.sumAmountTransactionFeeReport += voidAmex;
                    $scope.items.push({
                        brand:'AMEX',
                        count:countvoidAmex,
                        type:'Void',
                        amount:voidAmex,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countdeclineAmex > 0){
                    $scope.sumAmountTransactionFeeReport += declineAmex;
                    $scope.items.push({
                        brand:'AMEX',
                        count:countdeclineAmex,
                        type:'Decline',
                        amount:declineAmex,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countrefundDiscover > 0){
                    $scope.sumAmountTransactionFeeReport += refundDiscover;
                    $scope.items.push({
                        brand:'Discover',
                        count:countrefundDiscover,
                        type:'Refund',
                        amount:refundDiscover,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countauthDiscover > 0){
                    $scope.sumAmountTransactionFeeReport += authDiscover;
                    $scope.items.push({
                        brand:'Discover',
                        count:countauthDiscover,
                        type:'Sale/Auth/Capture',
                        amount:authDiscover,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countvoidDiscover > 0){
                    $scope.sumAmountTransactionFeeReport += voidDiscover;
                    $scope.items.push({
                        brand:'Discover',
                        count:countvoidDiscover,
                        type:'Void',
                        amount: voidDiscover,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countdeclineDiscover > 0){
                    $scope.sumAmountTransactionFeeReport += declineDiscover;
                    $scope.items.push({
                        brand:'Discover',
                        count:countdeclineDiscover,
                        type:'Decline',
                        amount: declineDiscover,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countrefundDiners > 0){
                    $scope.sumAmountTransactionFeeReport += refundDiners;
                    $scope.items.push({
                        brand:'Diners',
                        count:countrefundDiners,
                        type:'Refund',
                        amount:refundDiners,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countauthDiners > 0){
                    $scope.sumAmountTransactionFeeReport += authDiners;
                    $scope.items.push({
                        brand:'Diners',
                        count:countauthDiners,
                        type:'Sale/Auth/Capture',
                        amount:authDiners,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countvoidDiners > 0){
                    $scope.sumAmountTransactionFeeReport += voidDiners;
                    $scope.items.push({
                        brand:'Diners',
                        count:countvoidDiners,
                        type:'Void',
                        amount:voidDiners,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countdeclineDiners > 0){
                    $scope.sumAmountTransactionFeeReport += declineDiners;
                    $scope.items.push({
                        brand:'Diners',
                        count:countdeclineDiners,
                        type:'Decline',
                        amount:declineDiners,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countrefundDiners_carte > 0){
                    $scope.sumAmountTransactionFeeReport += refundDiners_carte;
                    $scope.items.push({
                        brand:'Diners - Carte Blanche',
                        count:countrefundDiners_carte,
                        type:'Refund',
                        amount:refundDiners_carte,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countauthDiners_carte > 0){
                    $scope.sumAmountTransactionFeeReport += authDiners_carte;
                    $scope.items.push({
                        brand:'Diners - Carte Blanche',
                        count:countauthDiners_carte,
                        type:'Sale/Auth/Capture',
                        amount:authDiners_carte,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countvoidDiners_carte > 0){
                    $scope.sumAmountTransactionFeeReport += voidDiners_carte;
                    $scope.items.push({
                        brand:'Diners - Carte Blanche',
                        count:countvoidDiners_carte,
                        type:'Void',
                        amount:voidDiners_carte,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countdeclineDiners_carte > 0){
                    $scope.sumAmountTransactionFeeReport += declineDiners_carte;
                    $scope.items.push({
                        brand:'Diners - Carte Blanche',
                        count:countdeclineDiners_carte,
                        type:'Decline',
                        amount:declineDiners_carte,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countrefundJcb > 0){
                    $scope.sumAmountTransactionFeeReport += refundJcb ;
                    $scope.items.push({
                        brand:'JCB',
                        count:countrefundJcb,
                        type:'Refund',
                        amount:refundJcb,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countauthJcb > 0){
                    $scope.sumAmountTransactionFeeReport += authJcb;
                    $scope.items.push({
                        brand:'JCB',
                        count:countauthJcb,
                        type:'Sale/Auth/Capture',
                        amount:authJcb,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countvoidJcb > 0){
                    $scope.sumAmountTransactionFeeReport += voidJcb;
                    $scope.items.push({
                        brand:'JCB',
                        count:countvoidJcb,
                        type:'Void',
                        amount:voidJcb,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countdeclineJcb > 0){
                    $scope.sumAmountTransactionFeeReport += declineJcb;
                    $scope.items.push({
                        brand:'JCB',
                        count:countdeclineJcb,
                        type:'Decline',
                        amount:declineJcb,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countrefundVisa_electron > 0){
                    $scope.sumAmountTransactionFeeReport += refundVisa_electron;
                    $scope.items.push({
                        brand:'Visa Electron',
                        count:countrefundVisa_electron,
                        type:'Refund',
                        amount:refundVisa_electron,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countauthVisa_electron > 0){
                    $scope.sumAmountTransactionFeeReport += authVisa_electron;
                    $scope.items.push({
                        brand:'Visa Electron',
                        count:countauthVisa_electron,
                        type:'Sale/Auth/Capture',
                        amount:authVisa_electron,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countvoidVisa_electron > 0){
                    $scope.sumAmountTransactionFeeReport += voidVisa_electron;
                    $scope.items.push({
                        brand:'Visa Electron',
                        count:countvoidVisa_electron,
                        type:'Void',
                        amount:voidVisa_electron,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countdeclineVisa_electron > 0){
                    $scope.sumAmountTransactionFeeReport += declineVisa_electron;
                    $scope.items.push({
                        brand:'Visa Electron',
                        count:countdeclineVisa_electron,
                        type:'Decline',
                        amount:declineVisa_electron,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countrefundUnkown > 0){
                    $scope.sumAmountTransactionFeeReport += refundUnkown ;
                    $scope.items.push({
                        brand:'Unknown',
                        count:countrefundUnkown ,
                        type:'Refund',
                        amount:refundUnkown,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countauthUnkown > 0){
                    $scope.sumAmountTransactionFeeReport += authUnkown;
                    $scope.items.push({
                        brand:'Unknown',
                        count:countauthUnkown,
                        type:'Sale/Auth/Capture',
                        amount:authUnkown,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countvoidUnkown > 0){
                    $scope.sumAmountTransactionFeeReport += voidUnkown;
                    $scope.items.push({
                        brand:'Unknown',
                        count:countvoidUnkown,
                        type:'Void',
                        amount:voidUnkown,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if (countdeclineUnkown > 0){
                    $scope.sumAmountTransactionFeeReport += declineUnkown;
                    $scope.items.push({
                        brand:'Unknown',
                        count:countdeclineUnkown,
                        type:'Decline',
                        amount:declineUnkown,
                        Merchant_ID: Fees[0].Merchant_ID,
                        Merchant_Name: Fees[0].Merchant_Name
                    });
                }
                if ($scope.items.length > 0) {
                    $scope.reportProcessed = true;
                }
            });
    };

    $scope.DiscountFees = function(){
        $scope.initPaginator();
        $scope.reportProcessed = false;
        $scope.sumVolume = 0;
        $scope.sumAmount = 0;
        var countVisa = 0,
            countMastercard = 0,
            countAmex = 0,
            countDiscover = 0,
            countDiners = 0,
            countDiners_carte = 0,
            countJcb = 0,
            countVisa_electron = 0,
            countUnkown = 0,
            volumeVisa = 0,
            volumeMastercard = 0,
            volumeAmex = 0,
            volumeDiscover = 0,
            volumeDiners = 0,
            volumeDiners_carte = 0,
            volumeJcb = 0,
            volumeVisa_electron = 0,
            volumeUnkown = 0,
            amountVisa = 0,
            amountMastercard = 0,
            amountAmex = 0,
            amountDiscover = 0,
            amountDiners = 0,
            amountDiners_carte = 0,
            amountJcb = 0,
            amountVisa_electron = 0,
            amountUnkown = 0;

        $scope.items = [];
        var startDate,
            endDate,
            param = $location.path();
        param = param.split('/');
        startDate = param[4].substr(0, 8)+'000000';
        endDate =  param[4].substr(0, 8)+'235959';
        var role = Authentication.user.Roles,
            varProductId = '',
            varBoardingId = '';
        if (role==='user'){
            varProductId = param[5];
            varBoardingId = Authentication.user.Boarding_ID;
        } else if (role==='admin' || role==='super') {
            //varBoardingId = $scope.merchantId.id;
            //varProductId = $scope.products.productName;
            varBoardingId = param[3];
            varProductId = param[5];
        }
//console.log('Performing DiscountService with: ',varProductId, varBoardingId, $scope)
        DiscountService.get({productId:varProductId,boardingId:varBoardingId,endDate:endDate,startDate:startDate}).$promise.then(
            function(Discount){
                var Discounts = Discount.Response;
                Discounts.forEach(function(item) {
                    item.Brand = $scope.getBrand(item.cc_bin);
                    if (item.Brand==='Visa'){
                        countVisa+=1;
                        volumeVisa+= (parseFloat(item.Original_Amount).toFixed(2)*100)/100;
                        amountVisa+= (parseFloat(item.Amount).toFixed(2)*100)/100;
                    }
                    if (item.Brand==='Mastercard'){
                        countMastercard+=1;
                        volumeMastercard+= (parseFloat(item.Original_Amount).toFixed(2)*100)/100;
                        amountMastercard+= (parseFloat(item.Amount).toFixed(2)*100)/100;
                    }
                    if (item.Brand==='AMEX'){
                        countAmex+=1;
                        volumeAmex+= (parseFloat(item.Original_Amount).toFixed(2)*100)/100;
                        amountAmex+= (parseFloat(item.Amount).toFixed(2)*100)/100;
                    }
                    if (item.Brand==='Discover'){
                        countDiscover+=1;
                        volumeDiscover+= (parseFloat(item.Original_Amount).toFixed(2)*100)/100;
                        amountDiscover+= (parseFloat(item.Amount).toFixed(2)*100)/100;
                    }
                    if (item.Brand==='Diners'){
                        countDiners+=1;
                        volumeDiners+= (parseFloat(item.Original_Amount).toFixed(2)*100)/100;
                        amountDiners+= (parseFloat(item.Amount).toFixed(2)*100)/100;
                    }
                    if (item.Brand==='Diners - Carte Blanche'){
                        countDiners_carte+=1;
                        volumeDiners_carte+= (parseFloat(item.Original_Amount).toFixed(2)*100)/100;
                        amountDiners_carte+= (parseFloat(item.Amount).toFixed(2)*100)/100;
                    }
                    if (item.Brand==='JCB'){
                        countJcb+=1;
                        volumeJcb+= (parseFloat(item.Original_Amount).toFixed(2)*100)/100;
                        amountJcb+= (parseFloat(item.Amount).toFixed(2)*100)/100;
                    }
                    if (item.Brand==='Visa Electron'){
                        countVisa_electron+=1;
                        volumeVisa_electron+= (parseFloat(item.Original_Amount).toFixed(2)*100)/100;
                        amountVisa_electron+= (parseFloat(item.Amount).toFixed(2)*100)/100;
                    }
                    if (item.Brand===null){
                        countUnkown+=1;
                        volumeUnkown+= (parseFloat(item.Original_Amount).toFixed(2)*100)/100;
                        amountUnkown+= (parseFloat(item.Amount).toFixed(2)*100)/100;
                    }
                });
                if (countVisa > 0){
                    $scope.sumVolume += volumeVisa;
                    $scope.sumAmount += amountVisa;
                    $scope.items.push({
                        brand:'Visa',
                        count:countVisa,
                        volume:volumeVisa,
                        amount:amountVisa,
                        Merchant_ID: Discounts[0].Merchant_ID,
                        Merchant_Name: Discounts[0].Merchant_Name
                    });
                }
                if (countMastercard > 0){
                    $scope.sumVolume += volumeMastercard;
                    $scope.sumAmount += amountMastercard;
                    $scope.items.push({
                        brand:'MasterCard',
                        count:countMastercard,
                        volume:volumeMastercard,
                        amount:amountMastercard,
                        Merchant_ID: Discounts[0].Merchant_ID,
                        Merchant_Name: Discounts[0].Merchant_Name
                    });
                }
                if (countAmex > 0){
                    $scope.sumVolume += volumeAmex;
                    $scope.sumAmount += amountAmex;
                    $scope.items.push({
                        brand:'AMEX',
                        count:countAmex,
                        volume:volumeAmex,
                        amount:amountAmex,
                        Merchant_ID: Discounts[0].Merchant_ID,
                        Merchant_Name: Discounts[0].Merchant_Name
                    });
                }
                if (countDiscover > 0){
                    $scope.sumVolume += volumeDiscover;
                    $scope.sumAmount += amountDiscover;
                    $scope.items.push({
                        brand:'Discover',
                        count:countDiscover,
                        volume:volumeDiscover,
                        amount:amountDiscover,
                        Merchant_ID: Discounts[0].Merchant_ID,
                        Merchant_Name: Discounts[0].Merchant_Name
                    });
                }
                if (countDiners > 0){
                    $scope.sumVolume += volumeDiners;
                    $scope.sumAmount += amountDiners;
                    $scope.items.push({
                        brand:'Diners',
                        count:countDiners,
                        volume:volumeDiners,
                        amount:amountDiners,
                        Merchant_ID: Discounts[0].Merchant_ID,
                        Merchant_Name: Discounts[0].Merchant_Name
                    });
                }
                if (countDiners_carte > 0){
                    $scope.sumVolume += volumeDiners_carte;
                    $scope.sumAmount += amountDiners_carte;
                    $scope.items.push({
                        brand:'Diners - Carte Blanche',
                        count:countDiners_carte,
                        volume:volumeDiners_carte,
                        amount:amountDiners_carte,
                        Merchant_ID: Discounts[0].Merchant_ID,
                        Merchant_Name: Discounts[0].Merchant_Name
                    });
                }
                if (countJcb > 0){
                    $scope.sumVolume += volumeJcb;
                    $scope.sumAmount += amountJcb;
                    $scope.items.push({
                        brand:'JCB',
                        count:countJcb,
                        volume:volumeJcb,
                        amount:amountJcb,
                        Merchant_ID: Discounts[0].Merchant_ID,
                        Merchant_Name: Discounts[0].Merchant_Name
                    });
                }
                if (countVisa_electron > 0){
                    $scope.sumVolume += volumeVisa_electron;
                    $scope.sumAmount += amountVisa_electron;
                    $scope.items.push({
                        brand:'Visa Electron',
                        count:countVisa_electron,
                        volume:volumeVisa_electron,
                        amount:amountVisa_electron,
                        Merchant_ID: Discounts[0].Merchant_ID,
                        Merchant_Name: Discounts[0].Merchant_Name
                    });
                }
                if (countUnkown > 0){
                    $scope.sumVolume += volumeUnkown;
                    $scope.sumAmount += amountUnkown;
                    $scope.items.push({
                        brand:'Unknown',
                        count:countUnkown,
                        volume:volumeUnkown,
                        amount:amountUnkown,
                        Merchant_ID: Discounts[0].Merchant_ID,
                        Merchant_Name: Discounts[0].Merchant_Name
                    });
                }
                if ($scope.items.length > 0) {
                    $scope.reportProcessed = true;
                }
            });
    };
    /**
     *
     * @constructor
     *
     * Main function for Account Balance Report
     *
     * Call "AccountBalance" function.
     */
     
    $scope.AccountBalanceWrap = function(){
        $scope.initPaginator();
        $scope.noResults = false;
        $scope.itemsBalance = [];
        var startDate = $scope.hyphenize_date($scope.oneDate);
        startDate = startDate.split('-');
        startDate = startDate[0]+''+startDate[1]+''+startDate[2]+'000000';
        var role = Authentication.user.Roles,
            varProductId = '',
            varBoardingId = '',
            param = $location.path();
        param = param.split('/');
        if (role==='user'){
            varProductId = $scope.productId.productName;
            varBoardingId = Authentication.user.Boarding_ID;
        } else if (role==='admin' || role==='super') {
            //varBoardingId = $scope.merchantId.id;
            //varProductId = $scope.products.productName;
            varBoardingId = param[3];
            varProductId = param[5];
            if (varBoardingId === undefined) { // ensure boarding_id is avaialble for reports.server.controller  getAccountBalance
                varBoardingId = $scope.merchantId.id;
            }
            if (varProductId === undefined) { // ensure boarding_id is avaialble for reports.server.controller getAccountBalance
                varProductId = $scope.products.productName;
            }
        }
//console.log('Performing AccountBalanceReportService with: ',varProductId, varBoardingId, $scope)
        AccountBalanceReportService.get({productId:varProductId,boardingId:varBoardingId,startDate:startDate}).$promise.then(
            function(results) {
                var accountBalance = results.Response;
                $scope.accountBalanceCurrentBalance = 0;
                $scope.accountBalanceReserveBalance = 0;
                $scope.accountBalanceHeld = 0;
                $scope.itemsBalance = accountBalance;
                accountBalance.forEach(function(item){
                    $scope.accountBalanceCurrentBalance += item.Net_Amount;
                    $scope.accountBalanceReserveBalance += item.Reserve_Amount;
                    $scope.accountBalanceHeld += item.Reserve_Amount + item.Net_Amount;
                });
                
                // Austin - there are rows so show the report div
                $scope.showReportABR = true;

            });
    };
    
    $scope.ACHActivityWrap = function() {
        $scope.initPaginator();
        $scope.showReportACH = true;
        
       toggleLoader(true);
        $scope.noResults = false;
        var role = Authentication.user.Roles,
            startDate = $scope.startDate,
            endDate = $scope.endDate,
            itemsACHActivityArray = [],
            accountSummary_sumBalance = 0,
            accountSummary_sumDebit = 0,
            accountSummary_sumCredit = 0,
            param = $location.path();
        param = param.split('/');
        startDate = startDate.split('-');
        startDate = startDate[0]+''+startDate[1]+''+startDate[2]+'000000';
        endDate = endDate.split('-');
        endDate = endDate[0]+''+endDate[1]+''+endDate[2]+'235959';
        var varProductId = $scope.products.productName,
            varBoardingId = $scope.merchantId.id;
        if (role === 'admin' || role === 'super') {
            if (param[3] !== null && param[3] !== undefined && param[3] !== '') {
                varBoardingId = param[3];
            }
            if (param[5] !== null && param[5] !== undefined && param[5] !== '') {
                varProductId = param[5];
            }
        }
//console.log('Performing ACHReportService with: ',varProductId, varBoardingId, $scope)
        ACHReportService.get({productId:varProductId,boardingId:varBoardingId,endDate:endDate,startDate:startDate}).
            $promise.then(function(results){
                itemsACHActivityArray = [];
                accountSummary_sumBalance = 0;
                accountSummary_sumDebit = 0;
                accountSummary_sumCredit = 0;
                var calculations = results.Response,
                    date = '',
                    counter= 0,
                    tempDate = '',
                    length = Object.keys(calculations).length;

                if (length !== 0){
                    date = calculations[0].Date;
                    date = date.slice(0,8);
                }else{
                    $scope.noResults = true;
                   toggleLoader(false);
                }
                calculations.forEach(function(calculation){
                    counter++;
                    var firstDate = calculation.Date,
                        initialDate;
                    firstDate = firstDate.slice(0,8);
                    var createObjects = function(){
                        tempDate = date;
                        initialDate = tempDate + '000000';
                        accountSummary_sumBalance += calculation.Disbursed_Amount;
                        if (calculation.Disbursed_Amount < 0) {
                            accountSummary_sumCredit += calculation.Disbursed_Amount;
                            itemsACHActivityArray.push({
                                processor: calculation.Processor_ID,
                                merchantName: calculation.Merchant_Name,
                                short_description: 'ACH Credit',
                                whole_description: '',
                                debit: '',
                                credit: calculation.Disbursed_Amount,
                                balance: accountSummary_sumBalance,
                                date: calculation.Date
                            });
                        }else{
                            accountSummary_sumDebit += calculation.Disbursed_Amount;
                            itemsACHActivityArray.push({
                                processor: calculation.Processor_ID,
                                merchantName: calculation.Merchant_Name,
                                short_description: 'ACH Disbursement',
                                whole_description: '',
                                debit: calculation.Disbursed_Amount,
                                credit: '',
                                balance: accountSummary_sumBalance,
                                date: calculation.Date
                            });
                        }
                    };
                    createObjects();
                });
            }).then(function(array){
                $scope.itemsACHActivity = [];
                itemsACHActivityArray.forEach(function(transaction){
                    $scope.itemsACHActivity.push(transaction);
                });
                $scope.sumDebit = accountSummary_sumDebit;
                $scope.sumCredit = accountSummary_sumCredit;
               toggleLoader(false);
            });

    };

    $scope.detailsTransaction = function(){
        $scope.initPaginator();
        var role = Authentication.user.Roles;
        var param = $location.path();
        param = param.split('/');
        var payment_id = Authentication.user.Boarding_ID;
        if (role === 'admin' || role === 'super') {
            payment_id = param[4];
        }
        PaymentService.get({
            paymentId: payment_id,
            productId: param[3]
        }).$promise.then(
            function(results){
                var transactionDetail = new Nmitransactions({
                    transactionId:param[2],
                    actionType:'',
                    orderId:'',
                    condition:'',
                    lastName:'',
                    customerVaultId:'',
                    startDate:'',
                    endDate:'',
                    type: 'treport',
                    processorId:results.Processor_ID,
                    boardingId: Authentication.user.Boarding_ID
                });
                Nmitransactions.get(transactionDetail).$promise.then(
                    function(results){
                        /**
                         * Validating next actions for: Capture, Refund and Void
                         * Reference:
                         * https://secure.total-apps-gateway.com/merchants/resources/integration/integration_portal.php?tid=2268ab67fad9b9f3a2c9052e224fb5d8#transaction_types
                         */
                        var canCapture =  false,
                            canRefund =  false,
                            canVoid =  false;
                        if (results.nm_response.transaction.hasOwnProperty('transaction_id')){
                            //console.log("Single " + results.nm_response.transaction.condition.$t);

                            // JUST ONE TRANSACTION
                            // can Capture?
                            if ( results.nm_response.transaction.condition.$t === 'pending' ) canCapture = true;
                            // can Refund?
                            if ( results.nm_response.transaction.condition.$t.indexOf('complete') !== -1 ) canRefund = true; // complete <=> settled
                            if ( results.nm_response.transaction.condition.$t === 'pendingsettlement' ) canRefund = false;
                            // can Void?
                            if ( (results.nm_response.transaction.condition.$t.indexOf('complete') === '-1') && (results.nm_response.transaction.condition.$t.indexOf('failed') === '-1' && results.nm_response.transaction.condition.$t.indexOf('canceled') === '-1' && results.nm_response.transaction.condition.$t.indexOf('unknown') === '-1') ) canVoid = true;
                            /**
                             * Sending behaviors for Capture, Refund, Void
                             */
                            results.nm_response.transaction.canCapture = canCapture;
                            results.nm_response.transaction.canRefund = canRefund;
                            results.nm_response.transaction.canVoid = canVoid;
                            var parseResult = [];
                            parseResult.push(results.nm_response.transaction);
                            $scope.transaction = parseResult;
                        }else{
                            //MULTIPLE TRANSACTIONS
                            results.nm_response.transaction.forEach(function (list) {
                                //console.log("Mult " + list.condition.$t);
                                if ( list.condition.$t === 'pending' ) canCapture = true;
                                if ( list.condition.$t.indexOf('complete') !== '-1') canRefund = true;
                                if ( list.condition.$t === 'pendingsettlement' ) canRefund = false;
                                if ( (list.condition.$t.indexOf('complete') === '-1') && (list.condition.$t.indexOf('failed') === '-1' && list.condition.$t.indexOf('canceled') === '-1' && list.condition.$t.indexOf('unknown') === '-1') ) canVoid = true;
                            });
                            results.nm_response.transaction[0].canCapture = canCapture;
                            results.nm_response.transaction[0].canRefund = canRefund;
                            results.nm_response.transaction[0].canVoid = canVoid;

                            var productName = "UNDEFINED";
                            ProductService.get({
                                productId:param[3]
                            }).$promise.then(function(resultProduct) {
                                    // console.log(result);
                                    productName = resultProduct.Description;

//                                    console.log(productName);
                                    results.nm_response.transaction[0].productId = productName;
                                    return resultProduct.Name;

                                });
                            //                            results.nm_response.transaction[0].productId = param[3];
                            $scope.transaction = results.nm_response.transaction;
                        }
                    },
                    function(err){
                        console.log(err);
                    }
                );

            },
            function(err){

            }
        );
    };
    /**
     *
     * @constructor
     *
     * Main function for Settlement Details Report.
     * Call "SettlementDetails" function
     *
     */

    $scope.SettlementDetailsWrap = function(){

    };


    /**
     *
     * @constructor
     * Main function for Account Summary Report.
     *
     * Call "AccountSummary" function
     */
    $scope.AccountSummaryWrap = function(){


    };

    /**
     *
     * @constructor
     *
     * Main function for Transaction Detail by Date
     *
     * Call "TransactionDetailByDate" function.
     */
    $scope.TransactionDetailByDateWrap = function(){

    };
    
    $scope.getBrand = function(cc){
        var cc_number = cc;        
        if ((cc_number.substr(1, 1)==='x') || (cc_number.substr(1, 1)==='*')){
            return 'Unknown';
        }else{
            // visa
            var re = new RegExp('^4');
            if (cc_number.match(re) !== null)
                return 'Visa';

            // Mastercard
            re = new RegExp('^5[1-5]');
            if (cc_number.match(re) !== null)
                return 'Mastercard';

            // AMEX
            re = new RegExp('^3[47]');
            if (cc_number.match(re) !== null)
                return 'AMEX';

            // Discover
            re = new RegExp('^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)');
            if (cc_number.match(re) !== null)
                return 'Discover';

            // Diners
            re = new RegExp('^36');
            if (cc_number.match(re) !== null)
                return 'Diners';

            // Diners - Carte Blanche
            re = new RegExp('^30[0-5]');
            if (cc_number.match(re) !== null)
                return 'Diners - Carte Blanche';

            // JCB
            re = new RegExp('^35(2[89]|[3-8][0-9])');
            if (cc_number.match(re) !== null)
                return 'JCB';

            // Visa Electron
            re = new RegExp('^(4026|417500|4508|4844|491(3|7))');
            if (cc_number.match(re) !== null)
                return 'Visa Electron';
        }
    };
    
    $scope.RefundTransactionDetails = function() {
        $scope.initPaginator();
        $scope.items = [];
        $scope.sumAmountRefundedReport = 0;
        var startDate,
            endDate,
            param = $location.path();
        param = param.split('/');
        startDate = param[4].substr(0, 8)+'000000';
        endDate =  param[4].substr(0, 8)+'235959';
        var role = Authentication.user.Roles,
            varProductId = '',
            varBoardingId = '';
        if (role==='user'){
            varProductId = param[5];
            varBoardingId = Authentication.user.Boarding_ID;
        } else if (role==='admin' || role==='super') {
            //varBoardingId = $scope.merchantId.id;
            //varProductId = $scope.products.productName;
            varBoardingId = param[3];
            varProductId = param[5];
       }
//console.log('Performing RefundService with: ',varProductId, varBoardingId, startDate, endDate, $scope)
        RefundService.get({productId:varProductId,boardingId:varBoardingId,endDate:endDate,startDate:startDate}).$promise.then(
            function(refunded){
                var refundedTransactions = refunded.Response;
                $scope.items = refundedTransactions;               
                refundedTransactions.forEach(function(item){
                    $scope.sumAmountRefundedReport += item.Original_Amount;
                });
        });
    
    };
    
    $scope.detailsTSnapshot = function() {
        toggleLoader(true);
        $scope.initPaginator();
        $scope.items = [];
        
        var boardingId,
            reportType,
            startDate,
            endDate,
            param = $location.path(),
            tempItems = [],
            transaction_index = -1;
        param = param.split('/');
        boardingId = param[3];
        reportType = param[2];
        startDate = param[4];
        endDate =  param[5];
    
        var transactionReport = new Nmitransactions({
            boardingId:  boardingId,
            originalId:  true,
            processorId: 'all',
            type:        'treport_all',
            startDate:   startDate,
            endDate:     endDate
        });
        
        Nmitransactions.get(transactionReport).$promise.then(function(results) {
            if (Object.keys(results).length > 2) { // just have $promise and $resolved, but not nm_response
                if (results.nm_response.transaction !== 'undefined') {
                    results.nm_response.transaction.forEach(function(list) {
                        if(list.boarding_id.$t.toString() === boardingId.toString()) {
                            // only process records for this merchant's boarding id
                            switch(reportType) {
                                case 'sales':
                                    if ((list.action.action_type.$t === 'sale' || list.action.action_type.$t === 'capture') && list.action.response_code.$t === '100') {
                                        tempItems.push(list);
                                    } // if ((list.action.action_type.$t === 'sale' || list.action.action_type.$t === 'capture') && list.action.response_code.$t === '100')
                                    break;
                                case 'refunds':
                                    if (list.action.action_type.$t === 'refund' && list.action.response_code.$t === '100') {
                                        tempItems.push(list);
                                    } // if (list.action.action_type.$t === 'refund' && list.action.response_code.$t === '100')
                                    break;
                                case 'returns':
                                    if (list.action.action_type.$t === 'credit' && list.action.response_code.$t === '100') {
                                        tempItems.push(list);
                                    } // if (list.action.action_type.$t === 'credit' && list.action.response_code.$t === '100')
                                    break;
                                case 'returns':
                                    if (list.action.action_type.$t === 'credit' && list.action.response_code.$t === '100') {
                                        tempItems.push(list);
                                    } // if (list.action.action_type.$t === 'credit' && list.action.response_code.$t === '100') 
                                    break;
                                case 'voids':
                                    if (list.action.action_type.$t === 'void' && list.action.response_code.$t === '100') {
                                        tempItems.push(list);
                                    } // if (list.action.action_type.$t === 'void' && list.action.response_code.$t === '100') 
                                    break;
                                case 'declines':
                                    if (list.action.response_code.$t !== '100') {
                                        tempItems.push(list);
                                    } // if (list.action.response_code.$t !== '100')
                                    break;
                                case 'authorizations':
                                    transaction_index = -1;
                                    if (list.action.action_type.$t === 'auth' && list.action.response_code.$t === '100') {
                                        tempItems.push(list);
                                    } // if (list.action.action_type.$t === 'auth' && list.action.response_code.$t === '100') 
                                    if (list.action.action_type.$t === 'capture' && list.action.response_code.$t === '100') {
                                        for(var i = 0; i < tempItems.length; i++) {
                                            transaction_index = tempItems[i].transaction_id.$t.indexOf(list.transaction_id.$t);
                                            if (transaction_index !== -1) {
                                                tempItems.splice(i, 1);
                                            } // if (transaction_index !== -1) 
                                        } // for(var i = 0; i < tempItems.length; i++) 
                                    } // if (list.action.action_type.$t === 'capture' && list.action.response_code.$t === '100')
                                    break;
                            } // switch(reportType) 
                        } // if(list.boarding_id.$t.toString() === boardingId.toString())
                    }); // results.nm_response.transaction.forEach(function(list)
                    $scope.showSeachTransactionsReport = true;
                    $scope.noResults = false;
                    toggleLoader(false);
                } else {
                    toggleLoader(false);
                    $scope.noResults = true;
                } // if (results.nm_response.transaction !== 'undefined')                
            } else {
                toggleLoader(false);
                $scope.noResults = true;
            } // if (Object.keys(results).length > 2)
        });
        $scope.items = tempItems;
    };
};
