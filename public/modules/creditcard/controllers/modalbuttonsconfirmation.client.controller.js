'use strict';

angular.module('creditcard').controller('ModalButtonsConfCtrl',['$scope','$modalInstance', 'message',
    function ($scope, $modalInstance, message) {


        /**
         * ok - should return the passed plan
         */
        $scope.ok = function () {
            $modalInstance.close({id: message.id, delete: message.delete, type: message.type});
        };


        /**
         * Builds the modal view
         */
        $scope.getInformation = function(){
            $scope.title = message.title;
            $scope.message = message.message;
            $scope.message2 = message.message2;
            $scope.code = message.code;
            $scope.accept = message.accept;
            $scope.deny = message.deny;
        };   

        /**
         * cancel - should return nothing
         */
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
            $modalInstance.close();
        };
}]);
