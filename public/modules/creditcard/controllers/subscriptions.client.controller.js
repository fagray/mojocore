'use strict';

angular.module('creditcard').SubscriptionController = function($q,$scope,$location,$locale,$http,$modal,Authentication,VaultService,RecurringPaymentPlanService, PaymentService,SubscriptionService,SubscriptionFindOne,ProductService)
{
    
    var arrayMerchantsProducts = [];
    var params = $location.path();
    var arrayMerchantsPlans = [];
    $scope.param = params.split('/');    
    $scope.dateError = '';
    $scope.customers = [];
    $scope.recurring_plans = []; 
    
    $scope.startDays = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
    var datetime = $locale.DATETIME_FORMATS,
        startYear = new Date(),
        startCurrMonth = startYear.getMonth(),
        startCurrDate = startYear.getDate();
    $scope.startMonths = datetime.SHORTMONTH;
    $scope.startYears = [];
                            

    // populate years for input dropdowns
    for (var i = startYear.getFullYear(); i <= startYear.getFullYear() + 5; i++) {
        $scope.startYears.push(i);
    }
    $scope.startYear = $scope.startYears[0];
    $scope.startMonth = $scope.startMonths[startCurrMonth];
    $scope.startDay = $scope.startDays[startCurrDate - 1];


    var clearSubscriptionForm = function() {
        $scope.customer = '';
        document.getElementById('recurring_plan').selectedIndex = 0;
        $scope.startYear = $scope.startYears[0];
        $scope.startMonth = $scope.startMonths[startCurrMonth];
        $scope.startDay = $scope.startDays[startCurrDate - 1];
        $scope.order_id = '';
        $scope.order_description = '';
        $scope.po_number = '';
        $scope.shipping = '';
        $scope.tax = '';
        $scope.tax_exempt = false;
        $scope.fieldsForm.$setPristine();
    };

 
    /*
     * Show/Hide the spinning icon along with the submit button
     *
     */
    var toggleLoader = function(val){
        if (val) { // show them
            document.getElementById('loadingImg').style.display='block';
            document.getElementById('submitButton').style.display='none';
        } else { // hide them
            document.getElementById('loadingImg').style.display='none';
            document.getElementById('submitButton').style.display='inline-block';
        } // if (val)
    };


    /**
     * Verify the date is valid.
     * 
     */
    function subCheckDate() {  
        var dateToCheck = new Date(parseInt($scope.startYear), parseInt($scope.startMonths.indexOf($scope.startMonth)), parseInt($scope.startDay),23,59,59);

        if (dateToCheck.getFullYear() !== parseInt($scope.startYear) ||
            dateToCheck.getMonth() !== parseInt($scope.startMonths.indexOf($scope.startMonth)) ||
            dateToCheck.getDate() !== parseInt($scope.startDay)) {
            return {value:false, message:'The date is invalid'};
        }
        if (dateToCheck < new Date()) {
            return {value:false, message:'The date is in the past'};
        }
        return {value:true, message:'valid date'};
    }


    /*
     * Verify the input in the form
     *
     */
    function checkSubForm() {
        $scope.error = '';
        if ($scope.customer.id === 0) {
            $scope.error = 'You must choose a customer for this subscription';
            return false;
        }
        if ($scope.recurring_plan.id === 0) {
            $scope.error = 'You must choose a plan for this subscription';
            return false;
        }
        if (!subCheckDate().value) {
            $scope.error = 'The date to start the subscription is invalid';
            return false;
        }
        return true;
    }


    /*
     * Check if this is a create record session or if we are modifying an existing record
     *
     */
    $scope.subCheckCreate = function () {
        if ($scope.param[1] === 'subscriptioncreate' && $scope.param[2] === undefined) {
            // we are creating Subscription from scratch
            return true;
        } else { // ($scope.param[1] === 'subscriptionmodify' && $scope.param[2] !== undefined)
            // we are modifying and existing Subscription
            return false;
        }
    };


    /*
     *  Checks to see if the date entered is valid
     *
     */
    $scope.subCheckDate = function() {
        $scope.dateError = '';
        if ($scope.error === 'The date to start the subscription is invalid') {
            $scope.error = '';
        }
        var result = subCheckDate();
        if (!result.value) {
            $scope.dateError = result.message;
        }
    };


    /*
     * Initialize the input form, taking into account whether or not this is a create or update situation
     *
     */
    $scope.subscriptionInitialize = function() {
        //$scope.customers[0] = {id: 0, value:'Select a Customer for this Subscription'};
        $scope.recurring_plans[0] = {id: 0, value:'Select a Plan for this Subscription'};

        // load the vaulted customers for the select
        VaultService.get({boarding: Authentication.user.Boarding_ID}).$promise.then(function(vaultResults) {
            vaultResults.response.forEach(function(vault) {
                $scope.customers.push({id: vault.customer_vault_id, value: vault.first_name + ' ' + vault.last_name});
            });
        });
        
        // load the recurring plans for the select
        RecurringPaymentPlanService.get({boardingId: Authentication.user.Boarding_ID, productId: 'all'}).$promise.then(
            function(planResults) {
                planResults.response.forEach(function(plan) {
                    $scope.recurring_plans.push({id:plan.Plan_ID, value: plan.Plan_ID + ' - ' + plan.Plan_Name});
                });
            }
        );
         
        if (!$scope.subCheckCreate()) { // updating an existing record
        } else {
            // set select dropdowns to the instructons
            //$scope.customer = $scope.customers[0];
            $scope.recurring_plan = $scope.recurring_plans[0];
        } // if (!$scope.subCheckCreate())
    };


    $scope.getPlanList = function() {
        var deferred = $q.defer();
        arrayMerchantsPlans = [{id:'all', value:'All Plans'}];
        RecurringPaymentPlanService.get({boardingId: Authentication.user.Boarding_ID, productId: 'all'})
        .$promise.then(function(planResults) {
//console.log(planResults);
            for(var i = 0; i < Object.keys(planResults.response).length; i++) {
                arrayMerchantsPlans.push({id:planResults.response[i].Plan_ID, value: planResults.response[i].Plan_Name});
                deferred.resolve(arrayMerchantsPlans);
//console.log(arrayMerchantsPlans)
            }
        });
        return deferred.promise;
    };
    
    
    $scope.subscriptionQueryInitialize = function() {
        arrayMerchantsPlans = [{id:'all', value:'All Plans'}];
        var junk = $scope.getPlanList();
        junk.then(function() {
            $scope.samplePlans = arrayMerchantsPlans;
            $scope.Plan_NameSearchTransactions = arrayMerchantsPlans[0];
        });
        if($scope.samplePlans === undefined) {
            $scope.samplePlans = arrayMerchantsPlans;
            $scope.Plan_NameSearchTransactions = arrayMerchantsPlans[0];
        }
    };
    

    $scope.displaySubConfirmation = function(numSucceeded, numFailed, errorMessage) {
        var message = '';
        
//        document.getElementById('fieldsForm').style.display='none'; // hide the form
//        document.getElementById('transactionCompleted').style.display='block'; // show the modal popup
        
        // create response message
        message += numSucceeded + " subscription(s) created successfully";
        if ( numFailed > 0 ) {
            message += '\n' + numFailed + ' subscription(s) failed';
            message += '\n' + errorMessage;
        } else {
            clearSubscriptionForm();
        }
        
        // display modal
        var infoarray = {
            'title': 'Add Subscriptions',
            'message': message,
            'accept': '',
            'deny': 'OK'
        };
        $scope.confirmationModal('', infoarray); 
    };


    $scope.submitSubscription = function(postData) {
        var promiseArray = [];
        var data = {};
        var message = '';
        var count_success = 0;
        var count_failure = 0;
        var subscriptionObject = {};
        var i;
        if (!checkSubForm()) {
             return false;
        } else {
            // create the database entry for the plan if it is new
            var start_date = $scope.startYear + ("0" + ($scope.startMonths.indexOf($scope.startMonth) + 1)).slice(-2) + ("0" + ($scope.startDay)).slice(-2);
 
            $scope.customer.forEach(function(cust) {
                subscriptionObject = {
                    customer_id: cust.id,
                    plan_id: $scope.recurring_plan.id,
                    start_date: start_date,
                    order_id: $scope.order_id,
                    order_description: $scope.order_description,
                    po_number: $scope.po_number,
                    shipping: $scope.shipping,
                    tax: $scope.tax,
                    tax_exempt: $scope.tax_exempt,
                    next_charge_date: start_date
                };
                
                promiseArray.push($http.post('/subscription', subscriptionObject));
            });
          
            $q.all(promiseArray)
            .then(function(dataArray) {
    // Each element of dataArray corresponds to each result of http request.

                dataArray.forEach(function(results) {

                    if (results.status === 200) {
                        count_success++;
                    } else {
                        count_failure++;
                        for (i = 0; i < $scope.customers.length; i++) {
                            if (results.data.Customer_Token === $scope.customers[i].id) {
                                message += 'Error adding customer ' + $scope.customers[i].value+ ' to database';
                            }
                        }
                    } // if (data.status === 200)
                    
                });
                $scope.displaySubConfirmation(count_success, count_failure, message);
            });
        }
   };


    /*
     * Builds a list of Subscriptions to display on the list screen
     *
     */
    $scope.subscriptionWrap = function() {
        toggleLoader(true);
        $scope.noResults = false;
        $scope.showSearchSubscriptions = false;
        $scope.sItems = []; // the list that will be displayed
        var boardingId = Authentication.user.Boarding_ID,
            product = $scope.productId.productName,
            plan = $scope.Plan_NameSearchTransactions.id,
            firstName = $scope.firstName,
            lastName = $scope.lastName,
            email = $scope.email,
            last4cc = $scope.last4ofCC;
        var billingCycle = '';
        var ordinal = ["th","st","nd","rd"];
        var payments = '';
        
        SubscriptionService.get({
            boarding: boardingId, 
            product: product,
            planId: plan,
            firstName: firstName,
            lastName: lastName,
            email: email,
            last4cc: last4cc
            })
        .$promise.then(function(subscription) {
            subscription.response.forEach(function(item) {
                // build display for billing cycle field
/*
                if ( item.Month_Frequency === 0 && item.Day_Of_Month === 0 ) {
                    billingCycle = 'Runs every ' + item.Day_Frequency + ' day(s)';
                } else {
                    if (item.Month_Frequency > 1) {
                        billingCycle = 'Runs every ' + item.Month_Frequency + ' month(s)';
                    } else {
                        // calculate ordinal (1st, 2nd, etc.)    
                        billingCycle = 'Runs every ' + item.Day_Of_Month + (ordinal[((item.Day_Of_Month%100)-20)%10]||ordinal[(item.Day_Of_Month%100)]||ordinal[0]) + ' day of the month';
                    } // if (item.Month_Frequency > 1) 
                } // if ( item.Month_Frequency === 0 && item.Day_Of_Month === 0 )
*/
                payments = item.Completed_Payments + " @ $" + item.Plan_Amount.toFixed(2);
                payments += "\n" + item.Attempted_Payments + " attempted";
                if (item.Plan_Payments !== 0) {
                    payments += "\n" + ( item.Plan_Payments - item.Completed_Payments) + " left";
                }
                // add this item to the list 
                $scope.sItems.push({
                    id: item.sub_ID,
                    Plan_Name: item.Plan_Name,
                    First_Name: item.first_name,
                    Last_Name: item.last_name,
                    Order_ID: item.Order_ID,
                    CC_Number: item.cc_number,
                    Created: item.Created,
                    Payments: payments,
                    Next_Charge_Date: new Date(item.Next_Charge_Date.substring(0,4),(item.Next_Charge_Date.substring(4,6)) -1,item.Next_Charge_Date.substring(6,8),0,0,0)  
                });
                
                
            }); // subscription.response.forEach(function(item)
            if( $scope.sItems.length === 0) {
                $scope.noResults = true;
                toggleLoader(false);
            } else {
                $scope.noResults = false;
                $scope.showSearchSubscriptions = true;
                toggleLoader(false);
            }
        }); // SubscriptionService.get().promise.then
    };


    $scope.verifySubDelete = function(subId) {
        var infoarray = {};
        infoarray = {
            'title': 'Delete Subscription',
            'message': 'Do you wish to delete this subscription?',
            'accept': 'Yes',
            'deny': 'No',
            'planId': subId,
            'delete': true
        };
//console.log(subId)
        $scope.confirmationSubModal('', infoarray);
    };


    /**
     * Modal calls with separate controllers
     */
    $scope.confirmationSubModal = function (size,message) { // used internally
        var modalInstance = $modal.open({
            templateUrl: 'modules/creditcard/views/modalrecurringdeleteconf.client.view.html',
            controller: 'ModalRecurrDeleteConfCtrl',
            size: size,
            resolve: {
                message: function () {
                    return message;
                }
            }
        });

        /*
         * Perform the delete if the modal returns with a plan ID
         *
         * planId = the Recurring_Plan record id that will be deleted
         */
        modalInstance.result.then(function (message) {
           var subId = message.planId;
//console.log(message)
           if (subId !== '' && subId !== undefined) {
                // remove this record
                
                //first, get this recurring plan
                SubscriptionFindOne.get({subId: subId}).$promise.then(function(subscriptionResult) {
                    var subscription = subscriptionResult.Response;
                    // Create the updated record
                    $http.put('/subscription', {

                    //SubscriptionUpdateService.put({
                        id: subscription.id,
                        Customer_Token: subscription.Customer_Token,
                        Plan_ID: subscription.Plan_ID,
                        Start_Date: subscription.Start_Date,
                        Order_ID: subscription.Order_ID,
                        Order_Description: subscription.Order_Description,
                        PO_Number: subscription.PO_Number,
                        Shipping: subscription.Shipping,
                        Tax: subscription.Tax,
                        Tax_Exempt: subscription.Tax_Exempt,
                        Completed_Payments: subscription.Completed_Payments,
                        Attempted_Payments: subscription.Attempted_Payments,
                        Count_Failed: subscription.Count_Failed,
                        Is_Active: 0 // this is the only field that should be changed
                    }).success(function (response) {
                        // recreate the display
                        $scope.subscriptionWrap();
                    }).error(function (response) {
                        $scope.error = response.message;
                    });

                }); // RecurringPlanService.get({Recurring_Plan_ID: planId}).promise.then
            } // if (planId !== '' && planId !== undefined)
        }, function () {
        });
    };

};

