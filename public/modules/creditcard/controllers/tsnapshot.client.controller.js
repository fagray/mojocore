'use strict';

angular.module('creditcard').tsnapshotController = function($q,$scope,Authentication,PaymentService,Nmitransactions,ReserveService,$location, $anchorScroll,$filter,$parse,$modal,MasService,TransactionsService,ProcessorService,ProductService,MerchantReportService,RunningBalanceService,SettleService,DiscountService,TransactionFeeService,Account_BalanceService,disbursementsService,$http,AccountBalanceReportService,Users,ACHReportService,PaymentCompleteService,LineItemsService,$locale)
{
    var datetime = $locale.DATETIME_FORMATS,
        tsnapYear = new Date(),
        tsnapCurrMonth = tsnapYear.getMonth();
    $scope.tsnapMonths = datetime.SHORTMONTH;
    $scope.tsnapYears = [];
    $scope.tsnapHours = [];
    $scope.tsnapMinutes = [];
    $scope.transSnapshotShow = false;                            

    // populate years for input dropdowns
    for (var i = tsnapYear.getFullYear() - 10; i <= tsnapYear.getFullYear(); i++) {
        $scope.tsnapYears.push(i);
    }
    // populate hours for input dropdowns
    for (i = 0; i <= 23; i++) {
        $scope.tsnapHours.push(("0" + i).slice(-2));
    }
    // populate minutes for input dropdowns
    for (i = 0; i <= 59; i++) {
        $scope.tsnapMinutes.push(("0" + i).slice(-2));
    }

    // set default values
    // MC-718 - appended all start date and end date variable names with "tsnap" - 2016-01-19 - pjf
    $scope.tsnapStartYear = $scope.tsnapYears[10];
    $scope.tsnapStartMonth = $scope.tsnapMonths[tsnapCurrMonth];
    $scope.tsnapStartDay = 1;
    $scope.tsnapStartHour = $scope.tsnapHours[0];
    $scope.tsnapStartMinute = $scope.tsnapMinutes[0];
    $scope.tsnapEndYear = $scope.tsnapYears[10];
    $scope.tsnapEndMonth = $scope.tsnapMonths[tsnapCurrMonth];
    $scope.tsnapEndDay = tsnapYear.getDate();
    $scope.tsnapEndHour = $scope.tsnapHours[23];
    $scope.tsnapEndMinute = $scope.tsnapMinutes[59];
    $scope.tsnapStartDate = '';
    $scope.tsnapEndDate = '';

    var toggleLoader = function(val) {
        if (val) {
            document.getElementById('loadingImg').style.display='block';
            document.getElementById('report-body').style.display='none';
        } else {
            document.getElementById('loadingImg').style.display='none';
            document.getElementById('report-body').style.display='block';
        }
    };


    /**
     * Convert passed info into a date object, and verify the date we get is the date we want.
     * @params year, month, date, hour, minute, second 
     */
    $scope.tsnapCheckDate = function(year, month, date, hour, minute, second) {    
        var dateToCheck = new Date(parseInt(year), parseInt(month), parseInt(date), parseInt(hour), parseInt(minute), parseInt(second));
        if (dateToCheck.getFullYear() !== parseInt(year) ||
            dateToCheck.getMonth() !== parseInt(month) ||
            dateToCheck.getDate() !== parseInt(date) ||
            dateToCheck.getHours() !== parseInt(hour) ||
            dateToCheck.getMinutes() !== parseInt(minute) ||
            dateToCheck.getSeconds() !== parseInt(second)) {
            return "Invalid Date";
        }
        return dateToCheck;
    };
    
    
    /**
     * Check date inputs to enusure:
     *   1. the inputs are valid dates
     *   2. the start date is earlier than the end date
     * @param 
     */
    $scope.tsnapCheckDateRange = function(){
        $scope.alerts = [];
        var startmonth = ($scope.tsnapMonths.indexOf($scope.tsnapStartMonth)).toString(),
            endmonth = ($scope.tsnapMonths.indexOf($scope.tsnapEndMonth)).toString();
        var tsnapCheckStartDate = $scope.tsnapCheckDate($scope.tsnapStartYear, startmonth, $scope.tsnapStartDay, $scope.tsnapStartHour,$scope.tsnapStartMinute,0),
            tsnapCheckEndDate = $scope.tsnapCheckDate($scope.tsnapEndYear, endmonth, $scope.tsnapEndDay, $scope.tsnapEndHour,$scope.tsnapEndMinute,59);

        var tsnapStartDate = $scope.tsnapStartYear + ("0" + (parseInt(startmonth) + 1)).slice(-2) + ("0" + $scope.tsnapStartDay).slice(-2) + $scope.tsnapStartHour + $scope.tsnapStartMinute,
            tsnapEndDate = $scope.tsnapEndYear + ("0" + (parseInt(endmonth) + 1)).slice(-2) + ("0" + $scope.tsnapEndDay).slice(-2) + $scope.tsnapEndHour + $scope.tsnapEndMinute;

        // check that start date is valid
        if (tsnapCheckStartDate === 'Invalid Date') {
            $scope.alerts.push({ type: 'danger', msg: 'Start date is not a proper date' });
            return false;
        }
        // check that end date is valid
        if (tsnapCheckEndDate === 'Invalid Date') {
            $scope.alerts.push({ type: 'danger', msg: 'End date is not a proper date' });
            return false;
        }

        // check start date is before end date
        if (parseInt(tsnapStartDate) >= parseInt(tsnapEndDate)){
            $scope.alerts.push({ type: 'danger', msg: 'Please select the correct date range' });
            return false;
        }

        return true;        
    };
    
    
    /**
     * Change the view according
     * @param status
     */
    $scope.transactionSnapshotWrap = function(){
        toggleLoader(true);
        
        // check to see if alerts are present, if they are do not allow submit
        if (typeof($scope.alerts) !== 'undefined')  {
            if ($scope.alerts.length > 0) {
                // alerts are present - do not allow submit
                return false;
            }
        }
        
        $scope.initPaginator();
        $scope.itemsTransSnapshot = [];
        $scope.totalsTransSnapshot = {
            'merchant_name': 'Totals',
            'authorizations_count': 0,
            'authorizations_amount': 0,
            'sale_count': 0,
            'sale_amount': 0,
            'refunds_count': 0,
            'refunds_amount': 0,
            'returns_count': 0,
            'returns_amount': 0,
            'voids_count': 0,
            'voids_amount': 0,
            'declines_count': 0,
            'declines_amount': 0,
            'totals_count': 0,
            'totals_amount': 0
        };
        $scope.reportProcesed = false;   
        $scope.noResults = false;
        
        // initialize start date and end date
        var startmonth = ($scope.tsnapMonths.indexOf($scope.tsnapStartMonth)).toString(),
            endmonth = ($scope.tsnapMonths.indexOf($scope.tsnapEndMonth)).toString();
        
        $scope.tsnapStartDate = $scope.tsnapStartYear + ("0" + (parseInt(startmonth) + 1)).slice(-2) + ("0" + $scope.tsnapStartDay).slice(-2) + $scope.tsnapStartHour + $scope.tsnapStartMinute + "00";
        $scope.tsnapEndDate = $scope.tsnapEndYear + ("0" + (parseInt(endmonth) + 1)).slice(-2) + ("0" + $scope.tsnapEndDay).slice(-2) + $scope.tsnapEndHour + $scope.tsnapEndMinute + "59";
        var transactionReport = new Nmitransactions({
            boardingId:  'all',
            startDate:   $scope.tsnapStartDate,
            endDate:     $scope.tsnapEndDate
        });
        
        PaymentCompleteService.get({
            boardingId: 'all',
            productId: $scope.products.productName
        }).$promise.then(
            function(results){

            if ($scope.products.productName === 'all') {
                transactionReport.productId = 'all';
                transactionReport.type = 'treport_all';
            } else {
                transactionReport.productId = $scope.products.productName;
                transactionReport.type = 'treport';
            }
     
            var merchantExists = false,
                merchant_index = -1,
                transaction_index = -1,
                transaction_amount = 0,
                capture_index = -1;
            
            Nmitransactions.get(transactionReport).$promise.then(
                function(results) {
                    if (Object.keys(results).length > 2) { // just have $promise and $resolved, but not nm_response
                        if (results.nm_response.hasOwnProperty("transaction")) {
                            $scope.reportProcessorMessage = 'Processing ' + Object.keys(results.nm_response.transaction).length + ' transactions, please wait...';
                            results.nm_response.transaction.forEach(function(list) {
    
                                // CREATE THE INITIAL REPORT LINE IF IT DOESN'T EXIST
                                merchantExists = false;
                                merchant_index = -1;
                                
                                // CHECK IF IT EXISTS
                                for (var i = 0; i < $scope.itemsTransSnapshot.length; i++) {
                                    if ($scope.itemsTransSnapshot[i].merchant_name === list.merchant_name.$t) {
                                        merchantExists = true;  // found it
                                        merchant_index = i;  // save it's location
                                    } // if ($scope.itemsTransSnapshot[i].merchant_name === list.merchant_name.$t) 
                                } // for (var i = 0; i < $scope.itemsTransSnapshot.length; i++) 
    
                                // IF IT DOES NOT EXIST, CREATE IT
                                if (!merchantExists) {
    //console.log('adding ' + list.merchant_name.$t + ' to array') 
                                    $scope.itemsTransSnapshot.push({
                                        'merchant_name': list.merchant_name.$t,
                                        'boarding_id': list.boarding_id.$t,
                                        'authorizations_count': 0,
                                        'authorizations_amount': 0,
                                        'authorizations_id': [],
                                        'authorizations_amt': [],
                                        'sale_count': 0,
                                        'sale_amount': 0,
                                        'captures_id': [],
                                        'captures_amt': [],
                                        'refunds_count': 0,
                                        'refunds_amount': 0,
                                        'returns_count': 0,
                                        'returns_amount': 0,
                                        'voids_count': 0,
                                        'voids_amount': 0,
                                        'declines_count': 0,
                                        'declines_amount': 0,
                                        'totals_count': 0,
                                        'totals_amount': 0
                                    });
                                    merchant_index = $scope.itemsTransSnapshot.length - 1;  // save it's location
                                } // if (!merchantExists)
    
                                // PROCESS THIS LINE ITEM
                                if (list.action.response_code.$t !== '100') {
                                    // It's a declined transaction - mark it as such
                                    $scope.itemsTransSnapshot[merchant_index].declines_count += 1;
                                    $scope.itemsTransSnapshot[merchant_index].declines_amount += parseFloat(list.action.amount.$t);
                                    $scope.itemsTransSnapshot[merchant_index].totals_count += 1;
                                    $scope.itemsTransSnapshot[merchant_index].totals_amount += parseFloat(list.action.amount.$t);
                                    $scope.totalsTransSnapshot.declines_count += 1;
                                    $scope.totalsTransSnapshot.declines_amount += parseFloat(list.action.amount.$t);
                                    $scope.totalsTransSnapshot.totals_count += 1;
                                    $scope.totalsTransSnapshot.totals_amount += parseFloat(list.action.amount.$t);
    //console.log("added sale for " + list.merchant_name.$t + " in the amount of " + parseFloat(list.action.amount.$t))
                                } else {
                                    // it's not a declined transaction
                                    switch (list.action.action_type.$t) {
                                        case 'sale':
                                            $scope.itemsTransSnapshot[merchant_index].sale_count += 1;
                                            $scope.itemsTransSnapshot[merchant_index].sale_amount += parseFloat(list.action.amount.$t);
                                            $scope.itemsTransSnapshot[merchant_index].totals_count += 1;
                                            $scope.itemsTransSnapshot[merchant_index].totals_amount += parseFloat(list.action.amount.$t);
                                            $scope.totalsTransSnapshot.sale_count += 1;
                                            $scope.totalsTransSnapshot.sale_amount += parseFloat(list.action.amount.$t);
                                            $scope.totalsTransSnapshot.totals_count += 1;
                                            $scope.totalsTransSnapshot.totals_amount += parseFloat(list.action.amount.$t);
    //console.log("added sale for " + list.merchant_name.$t + " in the amount of " + parseFloat(list.action.amount.$t))
                                            break;
                                        case 'refund':
                                            $scope.itemsTransSnapshot[merchant_index].refunds_count += 1;
                                            $scope.itemsTransSnapshot[merchant_index].refunds_amount += parseFloat(list.action.amount.$t);
                                            $scope.itemsTransSnapshot[merchant_index].totals_count += 1;
                                            $scope.itemsTransSnapshot[merchant_index].totals_amount += parseFloat(list.action.amount.$t);
                                            $scope.totalsTransSnapshot.refunds_count += 1;
                                            $scope.totalsTransSnapshot.refunds_amount += parseFloat(list.action.amount.$t);
                                            $scope.totalsTransSnapshot.totals_count += 1;
                                            $scope.totalsTransSnapshot.totals_amount += parseFloat(list.action.amount.$t);
    //console.log("added refund for " + list.merchant_name.$t + " in the amount of " + parseFloat(list.action.amount.$t))
                                            break;
                                        case 'credit':
                                            $scope.itemsTransSnapshot[merchant_index].returns_count += 1;
                                            $scope.itemsTransSnapshot[merchant_index].returns_amount += parseFloat(list.action.amount.$t);
                                            $scope.itemsTransSnapshot[merchant_index].totals_count += 1;
                                            $scope.itemsTransSnapshot[merchant_index].totals_amount += parseFloat(list.action.amount.$t);
                                            $scope.totalsTransSnapshot.returns_count += 1;
                                            $scope.totalsTransSnapshot.returns_amount += parseFloat(list.action.amount.$t);
                                            $scope.totalsTransSnapshot.totals_count += 1;
                                            $scope.totalsTransSnapshot.totals_amount += parseFloat(list.action.amount.$t);
    //console.log("added credit for " + list.merchant_name.$t + " in the amount of " + parseFloat(list.action.amount.$t))
                                            break;
                                        case 'auth':
                                            // find the capture id for this transaction if it's available
                                            capture_index = $scope.itemsTransSnapshot[merchant_index].captures_id.indexOf(list.transaction_id.$t);
                                            if (capture_index === -1) {
                                                // didn't find the capture - add it as an authorization
                                                $scope.itemsTransSnapshot[merchant_index].authorizations_count += 1;
                                                $scope.itemsTransSnapshot[merchant_index].authorizations_amount += parseFloat(list.action.amount.$t);
                                                $scope.itemsTransSnapshot[merchant_index].totals_count += 1;
                                                $scope.itemsTransSnapshot[merchant_index].totals_amount += parseFloat(list.action.amount.$t);
                                                $scope.totalsTransSnapshot.authorizations_count += 1;
                                                $scope.totalsTransSnapshot.authorizations_amount += parseFloat(list.action.amount.$t);
                                                $scope.totalsTransSnapshot.totals_count += 1;
                                                $scope.totalsTransSnapshot.totals_amount += parseFloat(list.action.amount.$t);
                                                $scope.itemsTransSnapshot[merchant_index].authorizations_id.push(list.transaction_id.$t);
                                                $scope.itemsTransSnapshot[merchant_index].authorizations_amt.push(list.action.amount.$t);
                                            } // if (capture_index === -1)
    //console.log("added authorization for " + list.merchant_name.$t + " in the amount of " + parseFloat(list.action.amount.$t))
                                            break;
                                        case 'capture':
                                            // find the authorization id for this transaction and clear out it's data
                                            transaction_index = $scope.itemsTransSnapshot[merchant_index].authorizations_id.indexOf(list.transaction_id.$t);
                                            if (transaction_index !== -1) {
                                                transaction_amount = parseFloat($scope.itemsTransSnapshot[merchant_index].authorizations_amt[transaction_index]);
                                                $scope.itemsTransSnapshot[merchant_index].authorizations_count -= 1;
                                                $scope.itemsTransSnapshot[merchant_index].authorizations_amount -= transaction_amount;
                                                $scope.itemsTransSnapshot[merchant_index].totals_count -= 1;
                                                $scope.itemsTransSnapshot[merchant_index].totals_amount -= transaction_amount;
                                                $scope.totalsTransSnapshot.authorizations_count -= 1;
                                                $scope.totalsTransSnapshot.authorizations_amount -= transaction_amount;
                                                $scope.totalsTransSnapshot.totals_count -= 1;
                                                $scope.totalsTransSnapshot.totals_amount -= transaction_amount;
                                            } //if (transaction_index !== -1) 
                                            // then, add it as a sale
                                            $scope.itemsTransSnapshot[merchant_index].sale_count += 1;
                                            $scope.itemsTransSnapshot[merchant_index].sale_amount += parseFloat(list.action.amount.$t);
                                            $scope.itemsTransSnapshot[merchant_index].totals_count += 1;
                                            $scope.itemsTransSnapshot[merchant_index].totals_amount += parseFloat(list.action.amount.$t);
                                            $scope.totalsTransSnapshot.sale_count += 1;
                                            $scope.totalsTransSnapshot.sale_amount += parseFloat(list.action.amount.$t);
                                            $scope.totalsTransSnapshot.totals_count += 1;
                                            $scope.totalsTransSnapshot.totals_amount += parseFloat(list.action.amount.$t);
                                            $scope.itemsTransSnapshot[merchant_index].captures_id.push(list.transaction_id.$t);
                                            $scope.itemsTransSnapshot[merchant_index].captures_amt.push(list.action.amount.$t);
    //console.log("added capture for " + list.merchant_name.$t + " in the amount of " + parseFloat(list.action.amount.$t))
                                            break;
                                        case 'void':
                                            $scope.itemsTransSnapshot[merchant_index].voids_count += 1;
                                            $scope.itemsTransSnapshot[merchant_index].voids_amount -= parseFloat(list.action.amount.$t);
                                            $scope.itemsTransSnapshot[merchant_index].totals_count += 1;
                                            $scope.itemsTransSnapshot[merchant_index].totals_amount -= parseFloat(list.action.amount.$t);
                                            $scope.totalsTransSnapshot.voids_count += 1;
                                            $scope.totalsTransSnapshot.voids_amount -= parseFloat(list.action.amount.$t);
                                            $scope.totalsTransSnapshot.totals_count += 1;
                                            $scope.totalsTransSnapshot.totals_amount -= parseFloat(list.action.amount.$t);
    //console.log("added void for " + list.merchant_name.$t + " in the amount of " + parseFloat(list.action.amount.$t))
                                            break;
                                        case 'settle':
                                            //$scope.itemsTransSnapshot[merchant_index].refund_count += 1;
                                            //$scope.itemsTransSnapshot[merchant_index].refund_amount += parseFloat(list.action.amount.$t);
                                            //$scope.itemsTransSnapshot[merchant_index].totals_count += 1;
                                            //$scope.itemsTransSnapshot[merchant_index].totals_amount += parseFloat(list.action.amount.$t);
                                            //$scope.totalsTransSnapshot.refund_count += 1;
                                            //$scope.totalsTransSnapshot.refund_amount += parseFloat(list.action.amount.$t);
                                            //$scope.totalsTransSnapshot.totals_count += 1;
                                            //$scope.totalsTransSnapshot.totals_amount += parseFloat(list.action.amount.$t);
    //console.log("added settle for " + list.merchant_name.$t + " in the amount of " + parseFloat(list.action.amount.$t))
                                            break;
                                    } // switch (list.action.action_type.$t)
                                } // f (list.action.response_code.$t !== '100') 
                            }); // results.nm_response.transaction.forEach(function(list) 
    //console.log($scope.itemsTransSnapshot)
                            $scope.itemsTransSnapshot = sortByKey($scope.itemsTransSnapshot, 'merchant_name', 'asc');
                            $scope.reportProcessorMessage = "";
                            $scope.transSnapshotShow = true;
                            $scope.reportProcesed = true;
                            $scope.noResults = false;
                            toggleLoader(false);
                        } else {
                            toggleLoader(false);
                            $scope.transSnapshotShow = false;
                            $scope.reportProcesed = true;
                            $scope.noResults = true;
                        } // if (results.nm_response.transaction !== 'undefined')
                    } else {
                        toggleLoader(false);
                        $scope.transSnapshotShow = false;
                        $scope.reportProcesed = true;
                        $scope.noResults = true;
                    } // if (Object.keys(results).length > 2)
                }, // function(results)
                function(err){
                    console.log(err);
                }
            ); // Nmitransactions.get
        });
    };


    /**
     * Sort and object by key
     * @param status
     */
     
    var sortByKey = function (array, key, sortBy) {
        if (sortBy==='asc'){ // before
            return array.sort(function(a, b) {
                var x = a[key];
                var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        }else{
            return array.sort(function(a, b) {
                var x = a[key];
                var y = b[key];
                return ((x > y) ? -1 : ((x < y) ? 1 : 0));
            });
        }
    };

};
