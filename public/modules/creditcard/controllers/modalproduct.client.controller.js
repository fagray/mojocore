/*global $stateParams */
'use strict';

angular.module('creditcard').controller('ModalProCtrl',['$scope','$modalInstance','boardingid','ProductService','isPayment_Plan','$http','PaymentPlanService','Authentication',
  function ($scope, $modalInstance, boardingid,ProductService,isPayment_Plan,$http,PaymentPlanService,Authentication) {

  $scope.addProduct = function() {// called in modalproducts.client.view.html
      ProductService.get({
          productId:'all'
      }).$promise.then(function(resultProduct) {
          $scope.products = resultProduct;
      });
  };

  $scope.createPayment = function(product){ // called in modalproducts.client.view.html
      var array = {
          payment: boardingid,
          product: product.id
      };
      ProductService.get({
          productId:'all'
      }).$promise.then(function(resultProduct) {
          isPayment_Plan.get(array, function(payment) {
              var paymentrespose = payment;

              if (payment.clean){
                  payment.Boarding_ID = boardingid;
                  payment.Product_ID =  product.id;
                  payment.Is_Active = 1;
                  payment.Last_Changed_By = Authentication.user.Username;
                
                  if (product.Marketplace === 1){
                      payment.Marketplace = 1;
                  }
                  $http.post('/paymentplan', payment).success(function(response) {
                  }).error(function(response) {
                      paymentrespose = response;
                      $scope.error = response.message;
                  });
              }
              var productIndex = parseInt(payment.Product_ID) - 1;
              paymentrespose.Name = resultProduct[productIndex].Name;
              paymentrespose.Marketplace = product.Marketplace;
              $modalInstance.close(paymentrespose);
          });
      });
  };
/* cannot find any situation where this is called.  Also, UNIT TEST generates error on $stateParams (it is not in the dependencies above)
 *
  $scope.getProducts = function(){
      PaymentPlanService.get({
          Boarding_ID: $stateParams.boardingId // THIS LINE CAUSING ERRORS IN UNIT TEST
      }, function(payment) {
          $scope.payment_plans = payment.Response;
      });
  };
*/
  $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
      $modalInstance.close();
  };

}]);
