/*global user */
'use strict';

angular.module('creditcard').controller('BoardingCrudController', ['$parse','$scope','$stateParams','$location','disbursementsService','Boardings','AdminUpdate','UsersUpdate','NMIService','PaymentService','Authentication','ProcessorService','NMIUsernameService','$filter','$q','$http','$modal','PaymentPlanService','ProductService','isPayment_Plan','PaymentCompleteService','PaymentSplitService',
  function($parse,$scope,$stateParams,$location,disbursementsService,Boardings,AdminUpdate,UsersUpdate,NMIService,PaymentService,Authentication,ProcessorService,NMIUsernameService,$filter,$q,$http,$modal,PaymentPlanService,ProductService,isPayment_Plan,PaymentCompleteService,PaymentSplitService) {

    if ((!Authentication.user) || ((Authentication.user.Roles !== 'admin') && (Authentication.user.Roles !== 'super'))){
        $location.path('/signin');
        return false;
    }

     var parama = $location.path(),
        param = parama.split('/'),
        sent=true;
    $scope.date = $filter('date')(Date.now(), 'yyyy-MM-dd');
    $scope.step3template = 'modules/creditcard/views/step3_1.client.view.html';
    $scope.Product_ID = 1;
    $scope.flag = 0;
    $scope.datamodelvariable = 'da';
    $scope.emptyPayment = 0;
    $scope.numberdynamic = 1;
    $scope.dynamicdate = 'modules/creditcard/views/step2dynamic1.client.view.html';
    $scope.dispursements  =[
          {id: '1', name: 'Daily'},
          {id: '2', name: 'Mon, Wed'},
          {id: '3', name: '3 Times per week (Mon, Wed, Fri)'},
          {id: '4', name: 'Weekly'},
          {id: '5', name: 'Every 2 weeks'},
          {id: '6', name: 'Every 4 weeks'},
          {id: '7', name: 'Mon, Tue, Wed, Thu'},
          {id: '8', name: 'Tue, Fri'},
          {id: '9', name: 'Tue, Thu'},
          {id: '10', name: 'Wed, Fri'}
    ];

    /**
     * Decide if we are creating a new boarding or reading existing
     */
    $scope.initialize = function () { // used boardingsheet.client.view.html
        if ($stateParams.boardingId !== 'create') {
            $scope.theid = '/' + $stateParams.boardingId;
            $scope.findOne();
            $scope.findUser();
            $scope.findSchedule();
            $scope.findPayment();
            $scope.findAllPayment();
        }
        if ($stateParams.pageNumber === '3') {
            $scope.Product_ID = 1;
            $scope.findPaymentSpecific();
        }
    };

	$scope.showPassword = function(id) {
        document.getElementsByClassName(id)[0].setAttribute('type','text');
	};
	$scope.hidePassword = function(id) {
        document.getElementsByClassName(id)[0].setAttribute('type','password');
	};

    $scope.onBlur = function ($event, product, $index) { // used boardingstep3.client.view.html    
        var processorPendingString = 'processorPending' + $index,
            processorErrorString = 'processorError' + $index,
            modelPending = $parse(processorPendingString),
            modelError = $parse(processorErrorString),
            validProcessor = $scope.validateThisProcessor(product);
        modelPending.assign($scope, true);
        validProcessor.then(function (result) {
            if (result === false) {
                modelPending.assign($scope, false);
                modelError.assign($scope, true);
                return false;
            } else {
                modelPending.assign($scope, false);
                modelError.assign($scope, false);
                return true;
            }
        });
        $scope.processorPending = false;
    };

    $scope.onBlurNMIUsername = function ($event, product, $index) { // used boardingstep3.client.view.html
        var nmiPendingString = 'nmiUsernamePending' + $index,
            nmiErrorString = 'nmiUsernameError' + $index,
            modelnmiPending = $parse(nmiPendingString),
            modelnmiError = $parse(nmiErrorString),
            NMIUsername = $scope.validateThisNMIUsername(product);
        modelnmiPending.assign($scope, true);
        NMIUsername.then(function (result) {
            if (result === false) {
                modelnmiPending.assign($scope, false);
                modelnmiError.assign($scope, true);
                return false;
            } else {
                modelnmiPending.assign($scope, false);
                modelnmiError.assign($scope, false);
                return true;
            }
        });
        $scope.nmiUsernamePending = false;
    };

    $scope.validateThisProcessor = function (product) { // used internally
        var deferred = $q.defer();
        var boardingId = product.Boarding_ID,
            productId = product.Product_ID,
            processor = product.Processor_ID,
            isValid = false;		
        ProcessorService.get({
            processorid: processor,
            boardingid: boardingId,
            Product_ID: productId
        }).$promise.then(function (results) {		
            if (results.response.length !== 0) {
                if (results.response[0].Processor_ID === '' || (results.response[0].Boarding_ID === boardingId && parseInt(results.response[0].Product_ID) === productId)) {
                    // Did not find it - must be unique
                    isValid = true;
                } else {
                    // found it - not unique
                    isValid = false;
                }
            } else {
                // Did not find it - must be unique
                isValid = true;
            } // if (results.response.length !== 0)
            deferred.resolve(isValid);
        }); // ProcessorService.get...$promise.then(function (results)
        return deferred.promise;
    }; // $scope.validateThisProcessor = function ()

    $scope.validateThisNMIUsername = function (product) { // used internally
        var deferred = $q.defer();
        var boardingId = product.Boarding_ID,
            productId = product.Product_ID,
            username = product.Username,
            isValid = false;	
        NMIUsernameService.get({
            username: username,
            boardingid: boardingId,
            productId: productId
        }).$promise.then(function (results) {
            if (results.response.length !== 0) {
                if ((results.response[0].Username === '' || results.response[0].Username === null || results.response[0].Username === undefined) || (results.response[0].Boarding_ID === boardingId && results.response[0].Product_ID === productId)) {
                    // Did not find it - must be unique
                    isValid = true;
                } else {
                    // found it - not unique
                    isValid = false;
                }
            } else {
                // Did not find it - must be unique
                isValid = true;
            } // if (results.response.length !== 0)
            deferred.resolve(isValid);
        }); // ProcessorService.get...$promise.then(function (results}
        return deferred.promise;
    }; // $scope.validateThisProcessor = function ()

    /*
    * Dropdowns in Merchant List
    */
    $scope.initPendingSelect = function (boarding) { // used boardinglist.client.view.html
        var identifier = 'statuspending' + boarding.id,
            infoarray;
        angular.element(document)
        .ready(function () {
            if (boarding.Status === 'deleted') {
                document.getElementById(identifier).value = 'deleted';
            } else if (boarding.Status === 'pending') {
                document.getElementById(identifier).value = 'pending';
            }
        });
    };

    $scope.initPendingSelectx = function (boarding) { // used boardinglist.client.view.html
        var identifier = 'statuspendingx' + boarding.id,
            infoarray;
        angular.element(document)
        .ready(function () {
            if (boarding.Status === 'deleted') {
                document.getElementById(identifier).value = 'deleted';
            } else if (boarding.Status === 'pending') {
                document.getElementById(identifier).value = 'pending';
            }
        });
    };

    $scope.initSelect = function (boarding) { // used boardinglist.client.view.html
        var identifier = 'status' + boarding.id,
            infoarray;
        angular.element(document)
        .ready(function () {
            if (boarding.Status === 'active') {
                document.getElementById(identifier).value = 'active';
            } else if (boarding.Status === 'achreject') {
                document.getElementById(identifier).value = 'achreject';
            } else if (boarding.Status === 'restricted') {
                document.getElementById(identifier).value = 'restricted';
            } else if (boarding.Status === 'suspended') {
                document.getElementById(identifier).value = 'suspended';
            } else if (boarding.Status === 'closed') {
                document.getElementById(identifier).value = 'closed';
            }
        });
    };

    $scope.initSelectx = function (boarding) { // used boardinglist.client.view.html
        var identifier = 'statusx' + boarding.id,
            infoarray;
        angular.element(document)
        .ready(function () {
            if (boarding.Status === 'active') {
                document.getElementById(identifier).value = 'active';
            } else if (boarding.Status === 'achreject') {
                document.getElementById(identifier).value = 'achreject';
            } else if (boarding.Status === 'restricted') {
                document.getElementById(identifier).value = 'restricted';
            } else if (boarding.Status === 'suspended') {
                document.getElementById(identifier).value = 'suspended';
            } else if (boarding.Status === 'closed') {
                document.getElementById(identifier).value = 'closed';
            }
        });
    };

    $scope.changePendingSelect = function (boarding) { // used boardinglist.client.view.html
        var identifier = 'statuspending' + boarding.id,
           infoarray;
        if (document.getElementById(identifier).value === 'deleted') {
            infoarray = {
                'title': 'Delete',
                'message': 'Are you sure you want to Delete ' + boarding.Merchant_Name + '?',
                'accept': 'Delete',
                'deny': 'Cancel',
                'status': 'deleted',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
            };
            $scope.confirmationModal('', infoarray);
        }
    };

    $scope.changePendingSelectx = function (boarding) { // used boardinglist.client.view.html
        var identifier = 'statuspendingx' + boarding.id,
           infoarray;
        if (document.getElementById(identifier).value === 'deleted') {
            infoarray = {
                'title': 'Delete',
                'message': 'Are you sure you want to Delete ' + boarding.Merchant_Name + '?',
                'accept': 'Delete',
                'deny': 'Cancel',
                'status': 'deleted',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
            };
            $scope.confirmationModal('', infoarray);
        }
    };

    $scope.changeStatusSelect = function (boarding) { // used boardinglist.client.view.html
        var identifier = 'status' + boarding.id,
            infoarray;
        if (document.getElementById(identifier).value === 'active') {
            infoarray = {
                'title': 'Activate Account',
                'message': 'Changing the status to Active will activate this Account. Do you want to proceed?',
                'accept': 'Yes',
                'deny': 'No',
                'status': 'active',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
            };
            $scope.confirmationModal('', infoarray);
        } else if (document.getElementById(identifier).value === 'achreject') {
            infoarray = {
                'title': 'ACH Rejected',
                'message': 'Do you want to proceed in changing the status to ACH Rejected?',
                'accept': 'Yes',
                'deny': 'No',
                'status': 'achreject',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
              };
            $scope.confirmationModal('', infoarray);
        } else if (document.getElementById(identifier).value === 'restricted') {
            infoarray = {
                'title': 'Restricted',
                'message': 'Changing the account to restricted will prevent the merchant from processing payments. Do you want to proceed?',
                'accept': 'Yes',
                'deny': 'No',
                'status': 'restricted',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
            };
            $scope.confirmationModal('', infoarray);
        } else if (document.getElementById(identifier).value === 'suspended') {
            infoarray = {
                'title': 'Suspended',
                'message': 'Changing the status to suspended will prevent the merchant from processing payments and freeze disbursements. Do you want to proceed?',
                'accept': 'Yes',
                'deny': 'No',
                'status': 'suspended',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
            };
            $scope.confirmationModal('', infoarray);
        } else if (document.getElementById(identifier).value === 'closed') {
            infoarray = {
                'title': 'Closed',
                'message': 'Changing the status to closed will prevent the merchant from processing payments. Do you want to proceed?',
                'accept': 'Yes',
                'deny': 'No',
                'status': 'closed',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
            };
            $scope.confirmationModal('', infoarray);
        }
    };

    $scope.changeStatusSelectx = function (boarding) { // used boardinglist.client.view.html
        var identifier = 'statusx' + boarding.id,
            infoarray;
        if (document.getElementById(identifier).value === 'active') {
            infoarray = {
                'title': 'Activate Account',
                'message': 'Changing the status to Active will activate this Account. Do you want to proceed?',
                'accept': 'Yes',
                'deny': 'No',
                'status': 'active',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
            };
            $scope.confirmationModal('', infoarray);
        } else if (document.getElementById(identifier).value === 'achreject') {
            infoarray = {
                'title': 'ACH Rejected',
                'message': 'Do you want to proceed in changing the status to ACH Rejected?',
                'accept': 'Yes',
                'deny': 'No',
                'status': 'achreject',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
              };
            $scope.confirmationModal('', infoarray);
        } else if (document.getElementById(identifier).value === 'restricted') {
            infoarray = {
                'title': 'Restricted',
                'message': 'Changing the account to restricted will prevent the merchant from processing payments. Do you want to proceed?',
                'accept': 'Yes',
                'deny': 'No',
                'status': 'restricted',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
            };
            $scope.confirmationModal('', infoarray);
        } else if (document.getElementById(identifier).value === 'suspended') {
            infoarray = {
                'title': 'Suspended',
                'message': 'Changing the status to suspended will prevent the merchant from processing payments and freeze disbursements. Do you want to proceed?',
                'accept': 'Yes',
                'deny': 'No',
                'status': 'suspended',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
            };
            $scope.confirmationModal('', infoarray);
        } else if (document.getElementById(identifier).value === 'closed') {
            infoarray = {
                'title': 'Closed',
                'message': 'Changing the status to closed will prevent the merchant from processing payments. Do you want to proceed?',
                'accept': 'Yes',
                'deny': 'No',
                'status': 'closed',
                'boarding': boarding.id,
                'previousstatus': boarding.Status
            };
            $scope.confirmationModal('', infoarray);
        }
    };

    $scope.radiochange = function () { // used boardingstep2.client.view.html
        if ($scope.schedule.Weekly === '1') {
            $scope.numberdynamic = 2;
            $scope.dynamicdate = 'modules/creditcard/views/step2dynamic' + $scope.numberdynamic + '.client.view.html';
        } else {
            $scope.numberdynamic = 1;
            $scope.dynamicdate = 'modules/creditcard/views/step2dynamic' + $scope.numberdynamic + '.client.view.html';
        }
    };

    $scope.changeSelect = function (object) { // used adminlist.client.view.html
        var identifier = 'superadmin' + object.id,
            identifier1 = 'isActive' + object.id,
            variablerole = object.roles;
        if (document.getElementById(identifier1).value === 'true') {
            object.isActive = true;
        } else {
            object.isActive = false;
        }
        if (document.getElementById(identifier).value === 'super') {
            $scope.findSuperAdmin(object.id, object.isActive);
        } else {
            $scope.findAdmin(object.id, object.isActive);
        }
    };

    $scope.changeActiveSelect = function (object) { // used adminlist.client.view.html
        var identifier = 'superadmin' + object.id,
            identifier1 = 'isActive' + object.id,
            variablerole = object.roles;
        if (document.getElementById(identifier1).value === 'true') {
            object.isActive = true;
        } else {
            object.isActive = false;
        }
        if (document.getElementById(identifier).value === 'super') {
            $scope.findSuperAdmin(object.id, object.isActive);
        } else {
            $scope.findAdmin(object.id, object.isActive);
        }
    };

    $scope.change = function (object) {
        var identifier = 'superadmin',
            variablerole = object.roles;
        if (document.getElementById(identifier).checked === true) {
            $scope.findSuperAdmin(object.id);
        } else {
            $scope.findAdmin(object.id);
        }
    };

    $scope.checkUserRole = function (object) {
        angular.element(document)
        .ready(function () {
            var identifier = 'superadmin',
                variablerole = object.roles;
            if (variablerole === 'super') {
                document.getElementById(identifier).checked = true;
            } else {
                document.getElementById(identifier).checked = false;
            }
        });
    };

    $scope.setTitle = function (viewTitle) { // used adminlist.client.view.html, boardinglist.client.view.html, boardingsheet.client.view.html
        $scope.$parent.$parent.$broadcast('setTitle', viewTitle);
    };

    /**
     * Navigation in components
     * @param pagenumber
     */
    $scope.changePlan = function (pagenumber) {
        $scope.step3template = 'modules/creditcard/views/step3_' + pagenumber + '.client.view.html';
        $scope.Product_ID = pagenumber;
        $scope.findPaymentSpecific();
        $scope.processorPending = false;
        $scope.processorError = false;
    };

    $scope.checkCreate = function () { // used boardingsheet.client.view.html
        if ($stateParams.boardingId !== 'create') {
            return true;
        }else{
            return false;
        }
    };

    /**
     * Modal calls with separate controllers
     */
    $scope.confirmationModal = function (size,email) { // used internally
        var modalInstance = $modal.open({
            templateUrl: 'modules/creditcard/views/modalconfirmation.client.view.html',
            controller: 'ModalConfCtrl',
            size: size,
            resolve: {
                items: function () {
                    return email;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            var boardingid = selectedItem.boarding-1;
            var arraylength = $scope.boardings.length;
            if (selectedItem.status === 'deleted') {
                if (boardingid === arraylength) {
                    $scope.boardings.pop();
                }else{
                    var i;
                    for (i = 0; i < arraylength; i++) {
                        if ($scope.boardings[i].id === selectedItem.boarding) {
                            $scope.boardings.splice(i, 1);
                        }
                    }
                }
            }
        }, function () {
        });
    };

    $scope.showProductModal = function () { // used boardingstep3.client.view.html
        $scope.productModal('', $stateParams.boardingId);
    };

    $scope.productModal = function (size, email) { // used internal
        var modalInstance = $modal.open({
            templateUrl: 'modules/creditcard/views/modalproducts.client.view.html',
            controller: 'ModalProCtrl',
            size: size,
            resolve: {
                boardingid: function () {
                    return email;
                }
            }
        });

        modalInstance.result.then(function (payment) {
            var alreadyexist = false,
                i, 
                arraylength = $scope.payment_plans.length;
            for (i = 0; i < arraylength; i++) {
                if ($scope.payment_plans[i].Product_ID === payment.Product_ID) {
                    alreadyexist = true;
                }
            }
            if (alreadyexist === false) {
                $scope.payment_plans.push({
                    Ach_Reject_Fee: payment.Ach_Reject_Fee,
                    Authorization_Fee: payment.Authorization_Fee,
                    Boarding_ID: payment.Boarding_ID,
                    Capture_Fee: payment.Capture_Fee,
                    Chargeback_Fee: payment.Chargeback_Fee,
                    Chargeback_Reversal_Fee: payment.Chargeback_Reversal_Fee,
                    createdAt: payment.createdAt,
                    Credit_Fee: payment.Credit_Fee,
                    Discount_Rate: payment.Discount_Rate,
                    Declined_Fee: payment.Declined_Fee,
                    id: payment.id,
                    Is_Active: payment.Is_Active,
                    Monthly_Fee: payment.Monthly_Fee,
                    Name: payment.Name,
                    Password: payment.Password,
                    Processor_ID: payment.Processor_ID,
                    Product_ID: payment.Product_ID,
                    Refund_Fee: payment.Refund_Fee,
                    Retrieval_Fee: payment.Retrieval_Fee,
                    Sale_Fee: payment.Sale_Fee,
                    Setup_Fee: payment.Setup_Fee,
                    updatedAt: payment.updatedAt,
                    Username: payment.Username,
                    Void_Fee: payment.Void_Fee,
                    Marketplace: payment.Marketplace,
                    Marketplace_Name: payment.Marketplace_Name,
                    Recipient: payment.Recipient,
                    Recipient_Type: payment.Recipient_Type,
                    Rate: payment.Rate,
                    Type: payment.Type
                });			
				
            }
        }, function (results) {
        });
    };

    /**
     * Modal calls with separate controllers
     */
    $scope.confirmationSaveModal = function (size,message) { // used internally
        var modalInstance = $modal.open({
            templateUrl: 'modules/creditcard/views/modalconfirmation.client.view.html',
            controller: 'ModalConfBoardCtrl',
            size: size,
            resolve: {
                message: function () {
                    return message;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
        }, function () {
        });
    };

    $scope.$on('setTitle', function (event, data) {
        $scope.title = data;
    });

    $scope.open = function (size,email) { // used boardinglist.client.view.html
        var modalInstance = $modal.open({
            templateUrl: 'modules/creditcard/views/modal.client.view.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                items: function () {
                    return email;
                }
            }
        });
        
        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
        });
    };

    $scope.openAdmin = function (size, email) { // used adminlist.client.view.html
        var modalInstance = $modal.open({
            templateUrl: 'modules/creditcard/views/modaladmin.client.view.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                items: function () {
                    return email;
                }
            }
        });
    
        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
        });
    };

    /**
     * New Dynamic Schedule
     * @param boardingId
     * @returns {*}
     */
    $scope.createSchedule = function (boardingId) { // used internally
        var deferred = $q.defer();
        $scope.schedule.Boarding_ID = boardingId;
        $scope.schedule.Last_Changed_By = Authentication.user.Username;
        		
        $http.post('/disbursement_schedule', $scope.schedule)
        .success(function (response) {
            delete $scope.schedule.clean;
            deferred.resolve();
        }).error(function (response) {
            $scope.error = response.message;
        });
        return deferred.promise;
    };

    $scope.updateSchedule = function (boardingId) { // used internally
        var deferred = $q.defer();
        $scope.schedule.Boarding_ID = boardingId;
        $scope.schedule.Last_Changed_By = Authentication.user.Username;
        
        $http.put('/disbursement_schedule/' + boardingId, $scope.schedule)
        .success(function (response) {
            delete $scope.schedule.clean;
            deferred.resolve();
        }).error(function (response) {
            $scope.error = response.message;
        });
        return deferred.promise;
    };

/*
    $scope.updateSchedule = function (boardingId) { // used internally
        var deferred = $q.defer();
        $scope.schedule.boarding_id = boardingId;
        $http.put('/disbursementdate/' + boardingId, $scope.schedule)
        .success(function (response) {
            delete $scope.schedule.clean;
            deferred.resolve();
        }).error(function (response) {
            $scope.error = response.message;
        });
        return deferred.promise;
    };
*/
    /**
     * Create new Merchant Account
     * @param boardingId
     * @returns {*}
     */
    $scope.createmojo = function (boardingId) { // used internally
        var deferred = $q.defer();
        $scope.credentials.Boarding_ID = boardingId;
        $scope.credentials.Last_Changed_By = Authentication.user.Username  ;   
        $http.post('/auth/boardmojo', $scope.credentials)
        .success(function (response) {
            deferred.resolve();
        }).error(function (response) {
            $scope.error = response.message;
        });
        return deferred.promise;
    };

    /**
     * Login as Merchant
     * @param boardingId
     */
    $scope.chameleonate = function (boardingId) { // used boardinglist.client.view.html
        $http.post('/auth/chameleonate', {bid:boardingId})
        .success(function (response) {
            location.reload();
        }).error(function (response) {
            $scope.error = response.message;
        });
    };


    $scope.clearForm = function() {
        $scope.error = '';
        
        $scope.credentials.Username = '';
        $scope.credentials.First_Name = '';
        $scope.credentials.Last_Name = '';
        $scope.credentials.Email = '';
        $scope.checkboxdata = false;
        $scope.inputForm.$setPristine();
    };


    /**
     * Create New Admin Account
     * @param boardingId
     * @returns {*}
     */
    $scope.createmojoadmin = function (boardingId) {
        var deferred = $q.defer();
        if ($scope.checkboxdata) {
            $scope.credentials.Roles = 'super';
        } else {
            $scope.credentials.Roles = 'admin';
        }
        $scope.credentials.Last_Changed_By = Authentication.user.Username;
        $http.post('/auth/boardmojoadmin', $scope.credentials)
        .success(function (response) {
            deferred.resolve();
            var infoarray = {
                'title': 'Create Admin Account',
                'message': 'Admin added',
                'accept': '',
                'deny': 'Ok'
            };
            $scope.confirmationModal('', infoarray);
            $scope.clearForm();

        }).error(function (response) {
            $scope.error = response.message;
        });
        return deferred.promise;
    };

    /**
     * Create New Boarding
     */
    $scope.create = function () { // used internally
        var boarding = new Boardings({
            Date: $scope.date,
            Merchant_Name: $scope.boarding.Merchant_Name,
            Address: $scope.boarding.Address,
            City: $scope.boarding.City,
            State: $scope.boarding.State,
            Zip: $scope.boarding.Zip,
            Phone: $scope.boarding.Phone,
            Last_Changed_By: Authentication.user.Username
        });
        var checking = {
            username: document.getElementById('Username').value,
            email: document.getElementById('Email').value
        };
        $http.post('/auth/boardmojoCheck', checking)
        .success(function (response, status) {
            if (!response.exist) {
                // if username and email do not exist, is created
                boarding.$save(function (response) {
                    var Boarding_ID = response.id;
                    $scope.createmojo(Boarding_ID).then(function () {
                        $location.path('boardingCreate/2/' + Boarding_ID);
                    });
                });
            } else {
                $scope.error = response.message;
            }
        }).error(function (response) {
            console.error(response);
        });
    };

    /**
     * Navigation
     */
    $scope.next = function () { // used boardingsheet.client.view.html
        $scope.page = $stateParams.pageNumber;
        $scope.templateurl = 'modules/creditcard/views/boardingstep' + $scope.page + '.client.view.html';
        if ($stateParams.boardingId!=='create') {
            $scope.theid = '/' + $stateParams.boardingId;
            $scope.findOne();
            $scope.findUser();
            $scope.findSchedule();
        } else {
          $scope.theid = '/create';
        }
        if ($stateParams.pageNumber === '3') {
            $scope.findAllPayment();
        } else {
            $scope.findPayment();
        }
    };

    $scope.createPaymentBase = function () { // used internally
        var array = {
            payment: $stateParams.boardingId,
            product: '1'
        };
        isPayment_Plan.get(array, function (payment) {
            var paymentrespose = payment;
            if (payment.clean) {
                payment.Boarding_ID = $stateParams.boardingId;
                payment.Product_ID =  '1';
                payment.Is_Active = 1;
                payment.Last_Changed_By = Authentication.user.Username;
                
                $http.post('/paymentplan', payment)
                .success(function (response) {
                }).error(function (response) {
                    paymentrespose = response;
                    $scope.error = response.message;
                });
            }
        });
    };

    /**
     * Decides what to update in DB according to Page
     */
    $scope.submit = function (product, displayName) { // used  boardingsheet.client.view.html, boardingstep3.client.view.html
        var message = {};
console.log($stateParams)        
        if ($stateParams.boardingId !== 'create') {
            $scope.theid = '/' + $stateParams.boardingId;
            if ($stateParams.pageNumber === '1') {
                $scope.update(false, product);               

            } else if ($stateParams.pageNumber === '2') {
                if ($scope.schedule.clean) {
                    $scope.createSchedule($stateParams.boardingId);
                } else {
                    $scope.updateSchedule($stateParams.boardingId);
                    //$scope.updatePayment();
                }
                $scope.update(false, product);
            }else if ($stateParams.pageNumber === '3') {
                $scope.Product_ID = product.Product_ID;
                $scope.rates = product;
                if (product.Product_ID !== 1){
                    var validProcessor = $scope.validateThisProcessor(product);
                    validProcessor.then(function (result) {
                        if (result === true) {
                            var validNMIUsername = $scope.validateThisNMIUsername(product);
                            validNMIUsername.then(function (resultNMI) {
                                if (resultNMI === true) {
                                    $scope.updatePayment();
                                    $scope.update(false, product);
                                    message = {
                                        title: 'Boarding Merchant',
                                        message: 'Product ' + product.Name + ' saved',
                                        accept: 'OK',
                                    };
                                    $scope.confirmationSaveModal('', message);
                                } // if (resultNMI === true)
                            }); // validNMIUsername.then(function (resultNMI)
                        } // if (result === true)
                    }); // validProcessor.then(function (result)
                }else{
                    $scope.updatePayment();
                    $scope.update(false, product);
                    message = {
                        title: 'Boarding Merchant',
                        message: 'Product ' + product.Name + ' saved',
                        accept: 'OK'
                    };
                    $scope.confirmationSaveModal('', message);
                }
            }
        } else {
            $scope.create();
            $scope.theid = '/create';
        }
    };

    $scope.remove = function (boarding) {
        if (boarding) {
            boarding.$remove();
            for (var i in $scope.boardings) {
                if ($scope.boardings[i] === boarding) {
                    $scope.boardings.splice(i, 1);
                }
            }
        }
        else {
            $scope.boarding.$remove();
            $location.path('boardinglist');
        }
    };

    /**
     * Paginator
     */
    $scope.find = function () { // used boardinglist.client.view.html
        /*
         * Init paginator
         */
        $scope.searchText = '';
        $scope.itemsPerPage = 10;
        $scope.currentPage = 0;
        $scope.criteria = 'orderTime';
        $scope.direction = true;

        Boardings.query(function (boardings) {
            $scope.boardings = boardings;

            /*
             * Pagination scopes =
             */
            $scope.range = function () { // used boardinglist.client.view.html
                var rangeSize = 5;
                var ret = [];
                var start;
                start = parseInt($scope.currentPage);

                if ($scope.pageCount() < rangeSize) {
                    rangeSize = $scope.pageCount() + 1;
                }

                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                var finish = parseInt(start) + parseInt(rangeSize);
                for (var i = start; i < finish; i++) {
                    ret.push(i);
                }
                return ret;
            };

            $scope.rangePages = function () { // used boardinglist.client.view.html
                var rp_rangeSize = $scope.pageCount();
                var rp_ret = [];
                var rp_start;

                rp_start = $scope.currentPage;
                if (rp_start > $scope.pageCount() - rp_rangeSize) {
                    rp_start = $scope.pageCount() - rp_rangeSize;
                }

                for (var i = rp_start; i < rp_start + rp_rangeSize + 1; i++) {
                    rp_ret.push(i);
                }
                return rp_ret;
            };

            /**
             * Start and Prev
             */
            $scope.startPage = function () { // used boardinglist.client.view.html
                if ($scope.currentPage > 0) {
                    $scope.currentPage=0;
                }
                setItems();
            };

            $scope.startPageDisabled = function () { // used boardinglist.client.view.html
                setItems();
                return $scope.currentPage === 0 ? 'disabled' : '';
            };

            $scope.prevPage = function () { // used boardinglist.client.view.html
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
                setItems();
            };

            $scope.prevPageDisabled = function () { // used boardinglist.client.view.html
                return $scope.currentPage === 0 ? 'disabled' : '';
            };

            /**
             * End and Next
             */
            $scope.nextPage = function () { // used boardinglist.client.view.html
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.endPage = function () { // used boardinglist.client.view.html
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage = Math.ceil($scope.boardings.length / $scope.itemsPerPage) - 1;
                }
            };

            $scope.nextPageDisabled = function () { // used boardinglist.client.view.html
                return $scope.currentPage === $scope.pageCount() ? 'disabled' : '';
            };

            $scope.endPageDisabled = function () { // used boardinglist.client.view.html
                return $scope.currentPage === $scope.pageCount() ? 'disabled' : '';
            };

            /**
             * Page count
             */
            $scope.pageCount = function () {
                return Math.ceil($scope.boardings.length / $scope.itemsPerPage)-1;
            };

            $scope.setPage = function (n) { // used boardinglist.client.view.html
                $scope.currentPage = n;
                setItems();
            };

            var setItems = function () {
                $scope.firstItem = 1 + ($scope.currentPage * $scope.itemsPerPage);
                $scope.lastPage = Math.ceil($scope.boardings.length / $scope.itemsPerPage) - 1;
                if ($scope.currentPage === $scope.lastPage) {
                    $scope.lastItem = $scope.boardings.length;
                } else {
                    $scope.lastItem = $scope.itemsPerPage + ($scope.currentPage * $scope.itemsPerPage);
                }
            };

            $scope.goToPage = function () { // used  boardinglist.client.view.html
                $scope.currentPage = parseInt($scope.pageSelected);
            };
        });
    };

    /**
     * Update Boarding
     */
    $scope.update = function (WelcomeEmailSent, product) { // used internally
        $scope.createPaymentBase();
        $scope.boarding.Last_Changed_By = Authentication.user.Username;
        var boarding = $scope.boarding;
        var internalFunction = function (result) {
            if (result === true) {
                if (WelcomeEmailSent === 'true' || WelcomeEmailSent === true) {
                    boarding.Welcome_Email = WelcomeEmailSent;
                }
                if ($stateParams.pageNumber === '3') {
                    if (boarding.Status === 'pending') {
                        boarding.Status = 'active';
                    }
                }
//                if (!boarding.updated) {
//                    boarding.updated = [];
//                }
//                boarding.updated.push(new Date().getTime());
                 boarding.Last_Changed_By = Authentication.user.Username;
                
                boarding.$update(function () {
                    if ($stateParams.pageNumber === '1') {
                        $scope.updateUserProfile(boarding.id);
                    } else if ($stateParams.pageNumber === '2') {
                        $location.path('boardingCreate/3/' + boarding.id);
                    } else if ($stateParams.pageNumber === '3') {
                        if (WelcomeEmailSent !== true) {
                            $scope.updateUserProfileStep3($scope.boarding.Welcome_Email, product);
                        }
                    }
                });
            } // if ($scope.validateThisProcessor())
        };

        if ($stateParams.pageNumber === '3') {
            var validProcessor = $scope.validateThisProcessor(product);
            validProcessor.then(function (result) {
                internalFunction(result);
            });
        } else {
            internalFunction(true);
        }
    };

    /**
     * Update User
     * @param boardingId
     */
    $scope.updateUserProfile = function (boardingId) { // used internally
        var user = $scope.credentials;      
        //if (!user.updated) {
        //    user.updated = [];
        //}
        //user.updated = new Date().getTime();
        user.Last_Changed_By = Authentication.user.Username;
        
        user.$update(function () {
            $location.path('boardingCreate/2/' + boardingId);
        });
    };

    /**
     * Update User Information Step 3 different table than previous function
     */
    $scope.updateUserProfileStep3 = function (welcomeEmail, product) { // used internally
        var user = $scope.credentials;
        user.emailActivation = false;
        //if (!user.updated) {
        //    user.updated = [];
        //}
        if ((param[1] === 'boardingCreate') && (sent === true)) {
            if (welcomeEmail !== true && $scope.boarding.Welcome_Email !== true) {
                welcomeEmail = true;
                user.emailActivation = true;
                sent = false;
                $scope.update(welcomeEmail, product);
            } else {
                sent = false;
            }
        }
        user.Last_Changed_By = Authentication.user.Username;
        
        user.$update(function () {
        });
    };

    $scope.updatePermissions = function () { // used boardingstep3.client.view.html
        var user = $scope.credentials;     
        //if (!user.updated) {
        //    user.updated = [];
        //}
        //user.updated = new Date().getTime();        
        user.Last_Changed_By = Authentication.user.Username;
        
        user.$update(function () {
            var message = {
                title: 'Boarding Merchant',
                message: 'Merchant Permissions saved',
                accept: 'OK',
            };
            $scope.confirmationSaveModal('', message);
        });
    };

    /**
     * Updates Payment_plan according to product_ID
     */
    $scope.updatePayment = function () { // used internally
        var bpayment = $scope.rates;
	    PaymentCompleteService.get({
            boardingId: bpayment.Boarding_ID,
            productId: bpayment.Product_ID,
            verify: 'getClean'
        }, function (paymentG) {
            var payment = paymentG.Response[0];
            payment.Ach_Reject_Fee = bpayment.Ach_Reject_Fee;
            payment.Authorization_Fee = bpayment.Authorization_Fee;
            payment.Capture_Fee = bpayment.Capture_Fee;
            payment.Chargeback_Fee = bpayment.Chargeback_Fee;
            payment.Chargeback_Reversal_Fee = bpayment.Chargeback_Reversal_Fee;
            payment.Credit_Fee = bpayment.Credit_Fee;
            payment.Discount_Rate = bpayment.Discount_Rate;
            payment.Declined_Fee = bpayment.Declined_Fee;
            payment.Monthly_Fee = bpayment.Monthly_Fee;
            payment.Processor_ID = bpayment.Processor_ID;
            payment.Refund_Fee = bpayment.Refund_Fee;
            payment.Retrieval_Fee = bpayment.Retrieval_Fee;
            payment.Sale_Fee = bpayment.Sale_Fee;
            payment.Setup_Fee = bpayment.Setup_Fee;
            payment.Username = bpayment.Username;
            payment.Password = bpayment.Password;
            payment.Void_Fee = bpayment.Void_Fee;
            payment.Marketplace_Name = bpayment.Marketplace_Name;
            payment.Recipient = bpayment.Recipient;
            payment.Rate = bpayment.Rate;
            payment.Type= bpayment.Type;
            payment.Recipient_Type = bpayment.Recipient_Type;
            payment.Last_Changed_By = Authentication.user.Username;
            
            $http.put('/updatepaymentplan', payment)
            .success(function (response) {
            }).error(function (response) {
                $scope.error = response.message;
            });
        });
    };

    $scope.deactivatePayment = function (product) { // used boardingstep3.client.view.html
        var bpayment = product;	
        PaymentService.get({
            paymentId: bpayment.Boarding_ID,
            productId: bpayment.Product_ID,
            verify: 'getClean'
        }, function (payment) {
            payment.Is_Active = 0;
            payment.$update(function () {
            });
        });
        var i, arraylength = $scope.payment_plans.length;
        for (i = 0; i < arraylength; i++) {
            if ($scope.payment_plans[i].Product_ID === product.Product_ID) {
                $scope.payment_plans.splice(i, 1);
            }
        }
    };

    /**
     * Updates admin user
     */
    $scope.updateAdmin = function () { // used internally
        var AdminUser = $scope.credentials;
        AdminUser.Last_Changed_By = Authentication.user.Username;

        AdminUser.$update(function () {
        });
    };

    /**
     * Looks for all admins and superadmins
     */
    $scope.findAdmins = function () { // used adminlist.client.view.js
        UsersUpdate.query(function (administrators) {
            $scope.administrators = administrators;
        });
    };

    /**
     * Looks for all merchants
     */
    $scope.findUser = function () { // used internally
        UsersUpdate.get({
            boardingId: $stateParams.boardingId
        }, function (user) {
            $scope.credentials = user;
        });
    };

    /**
     * Find the disbursements
     */
    $scope.findSchedule = function () { // used internally
        disbursementsService.get({
            userboardingId: $stateParams.boardingId
        }, function (schedule) {
            if (schedule.clean) {
                schedule.Weekly = 0;
                $scope.disableSchedule = false;
            } else {				
                var thispage = schedule.Weekly + 1;
                $scope.disableSchedule = true;
                $scope.dynamicdate = 'modules/creditcard/views/step2dynamic' + thispage + '.client.view.html';
            }
            $scope.schedule = schedule;
        });
    };

    /**
     * Looks for Super Admins
     * @param userid
     * @param isActive
     */
     $scope.findSuperAdmin = function (userid, isActive) { // used internally
        AdminUpdate.get({
            id: userid
        }, function (user) {
            $scope.credentials = user;
            $scope.credentials.Roles = 'super';
            if (isActive) {
              $scope.credentials.Is_Active = 1;
            } else {
              $scope.credentials.Is_Active = 0;
            }
            $scope.updateAdmin();
        });
    };

    /**
     * Looks for Admins
     * @param userid
     * @param isActive
     */
     $scope.findAdmin = function (userid, isActive) { // used internally
        AdminUpdate.get({
            id: userid
        }, function (user) {
            $scope.credentials = user;
            $scope.credentials.Roles='admin';
            if (isActive) {
                $scope.credentials.Is_Active = 1;
            } else {
                $scope.credentials.Is_Active = 0;
            }
            $scope.updateAdmin();
        });
    };

    /**
     * Finds the first payment_plan created using boardingId
     */
    $scope.findPayment = function () { // used internally
        PaymentService.get({
            paymentId: $stateParams.boardingId
        }, function (user) {
            $scope.rates = user;
            if (user.Is_Active === true) {
                $scope.flag = 1;
            } else {
                $scope.flag = 0;
            }
            if ($scope.rates.clean) {
                $scope.emptyPayment = 1;
                $scope.flag = 0;
            } else {
                $scope.emptyPayment = 0;
            }
/* refactored: BRH
//            if ( $scope.emptyPayment === 1) {
//               $scope.flag = 0;
//            }
*/
        });
    };

    $scope.getAllProduct = function () {
        ProductService.get({
            productId:'all'
        }).$promise.then(function (resultProduct) {

        });
    };

    $scope.findAllPayment = function () { // used internally
        PaymentSplitService.get({
            boardingId: $stateParams.boardingId
        }, function (payment) {
            $scope.payment_plans = payment.Response;
        });
    };

    /**
     * Fund a payment_plan for specific product
     */
    $scope.findPaymentSpecific = function () { // used internally
        PaymentCompleteService.get({
            boardingId: $stateParams.boardingId,
            productId: $scope.Product_ID,
            verify: 'getClean'
        }, function (payment) {
            $scope.rates = payment;
            if (user.Is_Active === true) {
              $scope.flag = 1;
            } else {
              $scope.flag = 0;
            }
            if ($scope.rates.clean) {
                $scope.emptyPayment = 1;
                $scope.flag = 0; // refactored : BRH
            } else {
                $scope.emptyPayment = 0;
            }
/* refactored: BRH
//            if ( $scope.emptyPayment === 1) {
//               $scope.flag = 0;
//            }
*/
        });
    };

    /**
     * Find the boardingId
     */
    $scope.findOne = function () { // used internally
        Boardings.get({
            boardingId: $stateParams.boardingId
        }, function (boarding) {
            $scope.boarding = boarding;
        });
    };

    angular.module('creditcard').miscFuncController($parse,$scope,$stateParams,$location,disbursementsService,Boardings,AdminUpdate,UsersUpdate,NMIService,PaymentService,Authentication,ProcessorService,NMIUsernameService,$filter,$q,$http,$modal,PaymentPlanService,ProductService,isPayment_Plan,PaymentCompleteService,PaymentSplitService);
        

}]).filter('isTrue', function () {
    return function (input) {
        var truffies = [1, '1', true, 'true', 'yes', 'y'];
        if (typeof input === 'string') input = input.toLowerCase();
        return truffies.indexOf(input) > -1;
    };
}).filter('offsetBoardings', function () {
    return function (input, start) {
        if (!input || !input.length) {
            return;
        }
        start = parseInt(start, 10);
        return input.slice(start);
    };
});

