'use strict';

angular.module('creditcard').controller('ButtonController', ['$scope','$http','$stateParams','Authentication','$location','$modal','ProductButtonService','DonationButtonService',
    function($scope,$http,$stateParams,Authentication,$location,$modal,ProductButtonService,DonationButtonService) {
        if (!Authentication.user) {
            $location.path('/signin');
            return false;
        }

        var params = $location.path();
        $scope.param = params.split('/');
        //$scope.page = 1;
        $scope.button_page_1 = 'modules/creditcard/views/mProductCreate.client.view.html';
        $scope.button_page_2 = 'modules/creditcard/views/mDonationCreate.client.view.html';
        $scope.templateurl = '';
        $scope.Donation_Currency_Default = 'USD';
        $scope.Product_Currency_Default = 'USD';
        $scope.Donation_Amount_1_Default = 5.00;
        $scope.Donation_Amount_2_Default = 10.00;
        $scope.Donation_Amount_3_Default = 25.00;
        $scope.Donation_Amount_4_Default = 50.00;
        $scope.Allow_Custom_Donations_default = true;
        $scope.pButton_Type_default = 'buynow150';
        $scope.dButton_Type_default = 'donate150';
        $scope.Allow_Recurring_Donation_default = false;
        $scope.donation_error = '';
        $scope.product_error = '';


        var toggleLoader = function(val) {
            if (val) {
                document.getElementById('loadingImg').style.display='block';
                document.getElementById('report-body').style.display='none';
            } else {
                document.getElementById('loadingImg').style.display='none';
                document.getElementById('report-body').style.display='block';
            }
        };


        var checkCreate = function () {
            if ($stateParams.boardingId === 'create') {
                return true;
            }else{
                return false;
            }
        };


        var createButtonObject = function() {
            // the object will be returned in fieldsButton 
            var fieldsButton = {};
            if ($scope.page === 1) {
                fieldsButton = {
                    Product_Name: $scope.Product_Name,
                    Product_Description: $scope.Product_Description,
                    Product_SKU: $scope.Product_SKU,
                    Product_Price: $scope.Product_Price,
                    Tax: $scope.Tax,
                    Shipping: $scope.Shipping,
                    Button_Size_Display: document.querySelector('input[name="pButtonType"]:checked').value,
                    Product_Currency: $scope.Product_Currency
                };
            } else {
                fieldsButton = {
                    Donation_Name: $scope.Donation_Name,
                    Donation_Description: $scope.Donation_Description,
                    Donation_Amount_1: $scope.Donation_Amount_1,
                    Donation_Amount_2: $scope.Donation_Amount_2,
                    Donation_Amount_3: $scope.Donation_Amount_3,
                    Donation_Amount_4: $scope.Donation_Amount_4,
                    Allow_Custom_Donations: $scope.Allow_Custom_Donations,
                    Button_Size_Display: document.querySelector('input[name="dButtonType"]:checked').value,
                    Donation_Currency: $scope.Donation_Currency,
                    Allow_Recurring_Donation: $scope.Allow_Recurring_Donation
                };
            }
            fieldsButton.Boarding_ID = Authentication.user.Boarding_ID;
            
            return fieldsButton;
        };


        var clearForm = function(page) {
            if (page === 1) {
                $scope.Product_Name = null;
                $scope.Product_Description = null;
                $scope.Product_SKU= null;
                $scope.Product_Currency = $scope.Product_Currency_Default;
                $scope.Product_Price = 0;
                $scope.Tax = 0;
                $scope.Shipping = 0;
                $scope.pButtonType = $scope.pButton_Type_default; 
                $scope.inputForm.$setPristine();
            } else {
                $scope.Donation_Name = null;
                $scope.Donation_Description = null;
                $scope.Donation_Currency = $scope.Donation_Currency_Default;
                $scope.Donation_Amount_1 = $scope.Donation_Amount_1_Default;
                $scope.Donation_Amount_2 = $scope.Donation_Amount_2_Default;
                $scope.Donation_Amount_3 = $scope.Donation_Amount_3_Default;
                $scope.Donation_Amount_4 = $scope.Donation_Amount_4_Default;
                $scope.Allow_Custom_Donations = $scope.Allow_Custom_Donations_default;
                $scope.dButtonType = $scope.dButton_Type_default; 
                $scope.Allow_Recurring_Donation = $scope.Allow_Recurring_Donation_default;

                $scope.inputForm.$setPristine();
                $scope.Allow_Recurring_Donation = $scope.Allow_Recurring_Donation_default;
                
            }
        };


        var createEmbedCode = function(code, type, display) {
            var theURL = '';
            var theImage = '';
            var protocol = $location.protocol();
            var thisHost = $location.host();
            var port = $location.port();
            var newHost = '';
            if (thisHost.indexOf('devcore.') >= 0) {
                newHost = 'securedev.mojopay.com';
            } else if (thisHost.indexOf('localhost') >= 0) {
                newHost = '104.130.130.221';
                protocol = 'http';
            } else {
                newHost = 'secure.mojopay.com';
            } 
            theURL = protocol + '://' + newHost + ':' + port;
            switch (display) {
                case 'buynow150':
                    theImage = theURL + '/img/buynow150px.png';
                    break;
                case 'donate150':
                    theImage = theURL + '/img/donate150px.png';
                    break;
                case 'buynow150cc':
                    theImage = theURL + '/img/buynow150pxcc.png';
                    break;
                case 'donate150cc':
                    theImage = theURL + '/img/donate150pxcc.png';
                    break;
                case 'buynow250':
                    theImage = theURL + '/img/buynow250px.png';
                    break;
                case 'donate250':
                    theImage = theURL + '/img/donate250px.png';
                    break;
                case 'buynow250cc':
                    theImage = theURL + '/img/buynow250pxcc.png';
                    break;
                case 'donate250cc':
                   theImage = theURL + '/img/donate250pxcc.png';
                     break;
            }
            if ($scope.page === 1 || type === 'Product') {
                theURL += '/#!/buttonapi/productbutton?token=' + code;
            } else {
                theURL += '/#!/buttonapi/donationbutton?token=' + code;
            }
            theURL = '<a href="' + theURL + '" style="border:none;" target="_blank"><img src="' + theImage + '" style="border:none;"></a>' ;
            return theURL;
        };


        var sortByKey = function (array, key, sortBy) {
            if (sortBy==='asc'){ // before
                return array.sort(function(a, b) {
                    var x = a[key];
                    var y = b[key];
                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                });
            }else{
                return array.sort(function(a, b) {
                    var x = a[key];
                    var y = b[key];
                    return ((x > y) ? -1 : ((x < y) ? 1 : 0));
                });
            }
        };


        var getSize = function(display) {
            var theImage = '';
            switch (display) {
                case 'buynow150':
                    theImage = '150px wide, no credit card images';
                    break;
                case 'donate150':
                    theImage = '150px wide, no credit card images';
                    break;
                case 'buynow150cc':
                    theImage = '150px wide, with credit card images';
                    break;
                case 'donate150cc':
                    theImage = '150px wide, with credit card images';
                    break;
                case 'buynow250':
                    theImage = '250px wide, no credit card images';
                    break;
                case 'donate250':
                    theImage = '250px wide, no credit card images';
                    break;
                case 'buynow250cc':
                    theImage = '250px wide, with credit card images';
                    break;
                case 'donate250cc':
                   theImage = '250px wide, with credit card images';
                     break;
            }
            
            return theImage;
        };


        /**
         * Pagination scopes
         */
        $scope.initPaginator = function(){
            $scope.itemsPerPage = 25;
            $scope.currentPage = 0;
        };


        $scope.validDonationForm = function () {
            $scope.donation_error = '';
            if ($scope.Donation_Name === '' || $scope.Donation_Name === null || $scope.Donation_Name === undefined)  {
                $scope.donation_error = 'Please Enter a name for this donation';
                return(false);
            } // if ($scope.Donation_Name === '' || $scope.Donation_Name === null || $scope.Donation_Name === undefined)         

            if (isNaN($scope.Donation_Amount_1)) {
                $scope.donation_error = 'Donation Amount 1 is not a number';
                return(false);
            } // if (isNaN($scope.Donation_Amount_1)) 

            if (isNaN($scope.Donation_Amount_2)) {
                $scope.donation_error = 'Donation Amount 2 is not a number';
                return(false);
            } // if (isNaN($scope.Donation_Amount_2)) 

            if (isNaN($scope.Donation_Amount_3)) {
                $scope.donation_error = 'Donation Amount 3 is not a number';
                return(false);
            } // if (isNaN($scope.Donation_Amount_3)) 

            if (isNaN($scope.Donation_Amount_4)) {
                $scope.donation_error = 'Donation Amount 4 is not a number';
                return(false);
            } // if (isNaN($scope.Donation_Amount_4))
            
            return(true);
        };


        $scope.validProductForm = function () {
            $scope.product_error = '';
            if ($scope.Product_Name === '' || $scope.Product_Name === null || $scope.Product_Name === undefined) {
                $scope.product_error = 'Please Enter a name for this product';
                return(false);
            } // if ($scope.Product_Name === '' || $scope.Product_Name === null || $scope.Product_Name === undefined)

            if (isNaN($scope.Product_Price)) {
               $scope.product_error = 'Product Price is not a number';
              return(false);
            } // if (isNaN($scope.Product_Price))

            if (isNaN($scope.Tax) && !($scope.Tax === '' || $scope.Tax === null || $scope.Tax === undefined)) {
                $scope.product_error = 'Tax is not a number';
                return(false);
            } // if (isNaN($scope.Tax))

            if (isNaN($scope.Shipping) && !($scope.Shipping === '' || $scope.Shipping === null || $scope.Shipping === undefined)) {
                $scope.product_error = 'Shipping is not a number';
                return(false);
            } // if (isNaN($scope.Shipping))

            return true;
        };


        $scope.setTitle = function(viewTitle) {
            $scope.$parent.$parent.$broadcast('setTitle', viewTitle);
        };


        $scope.button_init = function () {
            if (checkCreate()) {
                $scope.Donation_Currency = $scope.Donation_Currency_Default;
                $scope.Donation_Amount_1 = $scope.Donation_Amount_1_Default;
                $scope.Donation_Amount_2 = $scope.Donation_Amount_2_Default;
                $scope.Donation_Amount_3 = $scope.Donation_Amount_3_Default;
                $scope.Donation_Amount_4 = $scope.Donation_Amount_4_Default;
                $scope.Allow_Custom_Donations = $scope.Allow_Custom_Donations_default;
                $scope.dButtonType = $scope.dButton_Type_default; 
                $scope.pButtonType = $scope.pButton_Type_default;
                $scope.Allow_Recurring_Donation = $scope.Allow_Recurring_Donation_default;
                 
            } // if (checkCreate())
        };


        $scope.button_edit_init = function () {
             if ($scope.param[1] === 'productbuttonedit') {
                $scope.page = 1;
                $scope.templateurl = $scope.button_page_1;
                ProductButtonService.get({boardingId: Authentication.user.Boarding_ID, id: $scope.param[2], Is_Active: 1}).$promise.then(function(product) {
                    $scope.Product_Name = product.response[0].Product_Name;
                    $scope.Product_Description = product.response[0].Product_Description;
                    $scope.Product_SKU = product.response[0].Product_SKU;
                    $scope.Product_Currency = product.response[0].Product_Currency;
                    $scope.Product_Price = product.response[0].Product_Price;
                    $scope.Tax = product.response[0].Tax;
                    $scope.Shipping = product.response[0].Shipping;
                    $scope.pButtonType = product.response[0].Button_Size_Display;
                });        
            } else if ($scope.param[1] === 'donationbuttonedit') {
                $scope.page = 2;
                $scope.templateurl = $scope.button_page_2;
                DonationButtonService.get({boardingId: Authentication.user.Boarding_ID, id: $scope.param[2]}).$promise.then(function(donation) {
                    $scope.Donation_Name = donation.response[0].Donation_Name;
                    $scope.Donation_Description = donation.response[0].Donation_Description;
                    $scope.Donation_Currency = donation.response[0].Donation_Currency;
                    $scope.Donation_Amount_1 = donation.response[0].Donation_Amount_1;
                    $scope.Donation_Amount_2 = donation.response[0].Donation_Amount_2;
                    $scope.Donation_Amount_3 = donation.response[0].Donation_Amount_3;
                    $scope.Donation_Amount_4 = donation.response[0].Donation_Amount_4;
                    $scope.Allow_Custom_Donations = donation.response[0].Allow_Custom_Donations;
                    $scope.dButtonType = donation.response[0].Button_Size_Display;
                    $scope.Allow_Recurring_Donation = donation.response[0].Allow_Recurring_Donation;
                });
            } else {
                console.log('Error retrieving Button');
            }
        };


        $scope.button_next = function(page) {
            switch (page) {
                case 1:
                       $scope.page = 1;
                       $scope.templateurl = $scope.button_page_1;
                       break;
                   case 2:
                       $scope.page = 2;
                       $scope.templateurl = $scope.button_page_2;
                       
                       break;
                   default:
                       $scope.page = 1;
                       $scope.templateurl = $scope.button_page_1;
                       break;
            } // switch
        };


        /**
         * Modal calls with separate controllers
         */
        $scope.confirmationModal = function (size,message) { // used internally
            var modalInstance = $modal.open({
                templateUrl: 'modules/creditcard/views/modalbuttonsconf.client.view.html',
                controller: 'ModalButtonsConfCtrl',
                size: size,
                resolve: {
                    message: function () {
                        return message;
                    }
                }
            });
    
            modalInstance.result.then(function (data) {
                var buttonParams = {};
                var infoarray = {};

                if (data.type === 'Product') {
                    buttonParams = {
                        id: data.id,
                        boardingId: Authentication.user.Boarding_ID
                    };
                    ProductButtonService.get(buttonParams).$promise.then(function(products) {
                        var product = products.response[0];
                        product.Is_Active = 0;
                        ProductButtonService.update(product).$promise.then(function(changedbutton) {
                            infoarray = {
                                'title': 'Button Delete',
                                'message': 'Button Successfully Deleted.',
                                'accept': '',
                                'deny': 'OK'
                            };
                            $scope.confirmationModal('', infoarray); 
                            $scope.buttonWrap();
                        }); // ProductButtonService.update
                    }); // ProductButtonService.get(buttonParams).$promise.then(function(vault)
                } else if (data.type === 'Donation') {
                    buttonParams = {
                        id: data.id,
                        boardingId: Authentication.user.Boarding_ID
                    };
                    DonationButtonService.get(buttonParams).$promise.then(function(donations) {
                        var donation = donations.response[0];
                        donation.Is_Active = 0;
                        DonationButtonService.update(donation).$promise.then(function(changedbutton) {
                            infoarray = {
                                'title': 'Button Delete',
                                'message': 'Button Successfully Deleted.',
                                'accept': '',
                                'deny': 'OK'
                            };
                            $scope.confirmationModal('', infoarray); 
                            $scope.buttonWrap();
                        }); // ProductButtonService.update
                    }); // ProductButtonService.get(buttonParams).$promise.then(function(vault)
                } else {
                    console.log('Error deleting Button');
                }
            });
        };


        $scope.displayConfirmation = function(returnResponse, URL, name) {
            var results = {};
            var infoarray;
            
            // mock up a response to the display confirmation modal
            if ( returnResponse === 'error' ) {
                infoarray = {
                    'title': 'Error Creating Button',
                    'message': 'Button Was Not Created',
                    'accept': '',
                    'deny': 'Cancel'
                };
            } else if ( returnResponse === 'okay' ) {
                infoarray = {
                    'title': 'Button Successfully Created',
                    'message': 'Button Created. Here is the embed code for ' + name,
                    'message2': 'Please copy and paste into your site.',
                    'accept': '',
                    'deny': 'OK',
                    'code': URL
                };
            } else {
                infoarray = {
                    'title': 'Embed Code',
                    'message': 'Here is the embed code for ' + name,
                    'message2': 'Please copy and paste into your site.',
                    'code': URL,
                    'accept': '',
                    'deny': 'OK'
                };
            }
            $scope.confirmationModal('', infoarray); 
        };


        /*
         * Display modal, verifying if they want to really delete this button
         * 
         */
        $scope.verifyDelete = function(id, type) {
            var infoarray = {
                'title': 'Delete Button',
                'message': 'Do you wish to delete this button?',
                'accept': 'Yes',
                'deny': 'No',
                'id': id,
                'delete': true,
                'type': type
            };
            $scope.confirmationModal('', infoarray);
        };


        $scope.doCreate = function() {
            var url = '';
            if ($scope.page === 2) {
                if ($scope.validDonationForm()) {
                    var donationButtonObjet = createButtonObject();

                    $http.post('/merchant_donation', donationButtonObjet)
                    .success(function(response) {
                        url = createEmbedCode(response.Hash, 'Donation', response.Button_Size_Display);
                        $scope.displayConfirmation('okay', url, response.Donation_Name);
                        clearForm($scope.page);
                    })
                    .error(function(err) {
                        $scope.displayConfirmation('error', {});
                        console.log(err);
                    });
                } else {
                    console.log('Invalid donation form');
                }
            } else {
                if ($scope.validProductForm()) {
                    var productButtonObjet = createButtonObject();
                    $http.post('/merchant_product', productButtonObjet)
                    .success(function(response) {
                        url = createEmbedCode(response.Hash, 'Product', response.Button_Size_Display);
                        $scope.displayConfirmation('okay', url, response.Product_Name);
                        clearForm($scope.page);
                    })
                    .error(function(err) {
                            $scope.displayConfirmation('error', {});
                        console.log(err);
                    });
                } else {
                    console.log('Invalid product form');
                }
            }
        };


        $scope.doEdit = function() {
            var infoarray = {};
            if ($scope.page === 1) {
                if ($scope.validProductForm()) {
                 ProductButtonService.get({boardingId: Authentication.user.Boarding_ID, id: $scope.param[2], Is_Active: 1}).$promise.then(function(products) {
                        var product = products.response[0];
                        product.Product_Name = $scope.Product_Name;
                        product.Product_Description = $scope.Product_Description;
                        product.Product_SKU = $scope.Product_SKU;
                        product.Product_Currency = $scope.Product_Currency;
                        product.Product_Price = $scope.Product_Price;
                        product.Tax = $scope.Tax;
                        product.Shipping = $scope.Shipping;
                        //product.Button_Size_Display = document.querySelector('input[name="pButtonType"]:checked').value;
                        product.Button_Size_Display = document.querySelector('input[name="pButtonType"]:checked').value;
                        ProductButtonService.update(product).$promise.then(function(changedbutton) {
                            infoarray = {
                                'title': 'Button Edit',
                                'message': 'Button Successfully Edited.',
                                'accept': '',
                                'deny': 'OK'
                            };
                            $scope.confirmationModal('', infoarray); 
                        }); // ProductButtonService.update
                    }); // ProductButtonService.get({boardingId: Authentication.user.Boarding_ID, id: $scope.param[2], Is_Active: 1}).$promise.then(function(product) 
                } else {
                    console.log('Invalid product form');
                }
            } else if ($scope.page === 2) {
                if ($scope.validDonationForm()) {
                    DonationButtonService.get({boardingId: Authentication.user.Boarding_ID, id: $scope.param[2], Is_Active: 1}).$promise.then(function(donations) {
                        var donation = donations.response[0];
                        donation.Donation_Name = $scope.Donation_Name;
                        donation.Donation_Description = $scope.Donation_Description;
                        donation.Donation_Currency = $scope.Donation_Currency;
                        donation.Donation_Amount_1 = $scope.Donation_Amount_1;
                        donation.Donation_Amount_2 = $scope.Donation_Amount_2;
                        donation.Donation_Amount_3 = $scope.Donation_Amount_3;
                        donation.Donation_Amount_4 = $scope.Donation_Amount_4;
                        donation.Allow_Custom_Donations = $scope.Allow_Custom_Donations;
                        donation.Allow_Recurring_Donation = $scope.Allow_Recurring_Donation;
                        donation.Button_Size_Display = document.querySelector('input[name="dButtonType"]:checked').value;
                        donation.Allow_Recurring_Donation = $scope.Allow_Recurring_Donation;
                        DonationButtonService.update(donation).$promise.then(function(changedbutton) {
                            infoarray = {
                                'title': 'Button Edit',
                                'message': 'Button Successfully Edited.',
                                'accept': '',
                                'deny': 'OK'
                            };
                            $scope.confirmationModal('', infoarray);
                         }); // DonationButtonService.update
                    }); // DonationButtonService.get(buttonParams).$promise.then(function(donations)
                } else {
                    console.log('Invalid donation form');
                }
            } else {
                console.log('Error editing button');
            }
        };


        $scope.buttonWrap = function() {
            toggleLoader(true);
            
            $scope.initPaginator();
            $scope.itemButtons = [];
            $scope.buttonsShow = false;
            $scope.noResults = true;
            
            var name = document.getElementById('Button_Name').value;
            var type = document.getElementById('Button_Type').value;
            var buttonParams = {
                boardingId: Authentication.user.Boarding_ID
            };
            if (name !== '' && name !== null && name !== undefined) {
                buttonParams.Product_Name = name;
            }
            if (type === 'all' || type === 'products') {
                ProductButtonService.get(buttonParams).$promise.then(function(products){
                    products.response.forEach(function(product) {
                        $scope.itemButtons.push({
                            Button_ID: product.id,
                            Button_Name: product.Product_Name.replace(/'/g, "&#39;"),
                            Button_Type: 'Product',
                            Button_URL: createEmbedCode(product.Hash, 'Product', product.Button_Size_Display),
                            Button_Size: getSize(product.Button_Size_Display)
                        });
                        $scope.noResults = false;
                        $scope.buttonsShow = true;
                        $scope.itemButtons = sortByKey($scope.itemButtons, 'Button_Name', 'asc');
                    });
                },function(err){
                    console.log(err);
                });
            }
            
            if (type === 'all' || type === 'donations') {
                DonationButtonService.get(buttonParams).$promise.then(function(products){
                    products.response.forEach(function(product) {
                        $scope.itemButtons.push({
                            Button_ID: product.id,
                            Button_Name: product.Donation_Name.replace(/'/g, "&#39;"),
                            Button_Type: 'Donation',
                            Button_URL: createEmbedCode(product.Hash, 'Donation', product.Button_Size_Display),
                            Button_Size: getSize(product.Button_Size_Display)
                        });
                        $scope.noResults = false;
                        $scope.buttonsShow = true;
                        $scope.itemButtons = sortByKey($scope.itemButtons, 'Button_Name', 'asc');
                    });
                },function(err){
                    console.log(err);
                });
            }
            toggleLoader(false);
      };

    }
]);
