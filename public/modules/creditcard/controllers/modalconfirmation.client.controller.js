'use strict';

angular.module('creditcard').controller('ModalConfCtrl',['$scope','$modalInstance','items','Boardings','$http','Nmitransactions',
    function ($scope, $modalInstance, items,Boardings,$http,Nmitransactions) {

        var jsonMerchantsProducts = jsonMerchantsProducts || {},
            arrayMerchantsProducts = [],
            arrayMerchantSelected = [],
            arrayProductSelected = [];

        $scope.items = items;

        $scope.updateStatus = function(status,boardingid) {
            Boardings.get({
                boardingId: boardingid
            }, function(boarding) {
                boarding.Status = status;
//              if (!boarding.updated) {
//                  boarding.updated = [];
//              }
//              boarding.updated.push(new Date().getTime());
                boarding.$update(function() {
                });
            });
        };


        /**
         * Updates acording to option
         */
        $scope.ok = function () {
            if (items.status === 'active'){
                $scope.updateStatus('active',items.boarding);
            }else if (items.status === 'achreject'){
                $scope.updateStatus('achreject',items.boarding);
            }else if (items.status === 'restricted'){
                $scope.updateStatus('restricted',items.boarding);
            }else if (items.status === 'suspended'){
                $scope.updateStatus('suspended',items.boarding);
            }else if (items.status === 'closed'){
                $scope.updateStatus('closed',items.boarding);
            }else if (items.status === 'deleted'){
                $scope.updateStatus('deleted',items.boarding);
            }
            $modalInstance.close(items);
        };


        /**
         * Builds the view
         */
        $scope.getInformation = function(){
            $scope.title = items.title;
            $scope.message = items.message;
            $scope.accept = items.accept;
            $scope.deny = items.deny;
        };   


        $scope.cancel = function () {
            var identifier = 'status'+items.boarding;
            $modalInstance.dismiss('cancel');
            $modalInstance.close();
        };
}]);
