'use strict';

angular.module('creditcard').dashboardController = function($q,$scope,Authentication,PaymentService,Nmitransactions,ReserveService,$location, $anchorScroll,$filter,$parse,$modal,MasService,TransactionsService,ProcessorService,ProductService,MerchantReportService,RunningBalanceService,SettleService,DiscountService,TransactionFeeService,Account_BalanceService,disbursementsService,$http,AccountBalanceReportService,Users,ACHReportService,PaymentCompleteService,LineItemsService)
{
    /**
     * Change the view according
     * @param status
     */
/*
    $scope.setCss = function(status){
        var identifier='csslabel';
        angular.element(document).ready(function () {
            if (status === 'active'){
                document.getElementById(identifier).value = 'active';
                $scope.TitleStatus= 'Active';
            }else if (status === 'achreject'){
                document.getElementById(identifier).value = 'achreject';
                $scope.TitleStatus= 'ACH Reject';
            }else if (status === 'restricted'){
                document.getElementById(identifier).value = 'restricted';
                $scope.TitleStatus= 'Restricted';
            }else if (status === 'suspended'){
                document.getElementById(identifier).value = 'suspended';
                $scope.TitleStatus= 'Suspended';
            }else if (status === 'closed'){
                document.getElementById(identifier).value = 'closed';
                $scope.TitleStatus= 'Closed';
            }
        });
    };
*/
    $scope.DashboardActivityWrap = function(duration){
        var whichProduct = document.getElementById('dropdownMenu8').innerHTML;
        if (whichProduct.indexOf('<span class="ng-binding" style="float: left;">{{dashboardData[productIndex].productName}}</span>' ) !== -1) $scope.productId = 0;

        toggleLoader(true);
        $scope.joinedTransactions = [];
        $scope.transApproved = 0;
        $scope.transRefunded = 0;
        $scope.transDeclined = 0;

        // turn the buttons off
        document.getElementById('dayButton').classList.remove('active');
        document.getElementById('weekButton').classList.remove('active');
        document.getElementById('monthButton').classList.remove('active');
        document.getElementById('yearButton').classList.remove('active');

        //graph initialization
        var arrayIndex = '';
        var parallelArray = [];
        $scope.salesLabels = [];
        $scope.salesData = [[]];
        $scope.salesSeries = [''];
        $scope.salesOptions = {'maintainAspectRatio': false, 'responsive': true, 'bezierCurve': false, 'animation': false, 'scaleLabel': "<%= '$' + parseFloat(value).toLocaleString() %>", tooltipTemplate: "<%= '$' + parseFloat(value).toLocaleString() %>"};
        $scope.salesColours = [{
            'fillColor': '#ebebeb',
            'strokeColor': '#3fbaa8',
            'pointColor': '#3fbaa8',
            'pointStrokeColor': '#3fbaa8',
            'pointHighlightFill': '#fff',
            'pointHighlightStroke': '#3fbaa8'
        }];

        $scope.transactionsLabels = [];
        $scope.transactionsData = [[]];
        $scope.transactionsSeries = [''];
        $scope.transactionsOptions = {'maintainAspectRatio': false, 'responsive': true, 'bezierCurve': false, 'animation': false, showScale: false, tooltipTemplate: "<%= parseInt(value).toLocaleString() %>"};
        $scope.transactionsColours = [{
            'fillColor': '#ebebeb',
            'strokeColor': '#3fbaa8',
            'pointColor': '#3fbaa8',
            'pointStrokeColor': '#3fbaa8',
            'pointHighlightFill': '#fff',
            'pointHighlightStroke': '#3fbaa8'
        }];


        // Set date range and make pressed button active
        var highDate = new Date();
        var lowDate = new Date();
        switch (duration) {
            case 'day':
                lowDate = highDate;
                document.getElementById('dayButton').classList.add('active');
                break;
            case 'week':
                lowDate.setDate(lowDate.getDate() - 7);
                document.getElementById('weekButton').classList.add('active');
                break;
            case 'month':
                lowDate.setMonth(lowDate.getMonth() - 1);
                document.getElementById('monthButton').classList.add('active');
                break;
            case 'year':
                lowDate.setYear(lowDate.getFullYear() - 1);
                document.getElementById('yearButton').classList.add('active');
                break;
        } // switch (duration)

        // convert dates for NMI call
        var formattedDate='';
        var d = lowDate;
        formattedDate = formattedDate + d.getFullYear();
        formattedDate = formattedDate + ('0' + (d.getMonth() + 1)).slice(-2);
        formattedDate = formattedDate + ('0' + d.getDate()).slice(-2);
        formattedDate = formattedDate + '000000'; // HHMMSS
        var startDate = formattedDate;

        d = highDate;
        formattedDate = '';
        formattedDate = formattedDate + d.getFullYear();
        formattedDate = formattedDate + ('0' + (d.getMonth() + 1)).slice(-2);
        formattedDate = formattedDate + ('0' + d.getDate()).slice(-2);
        formattedDate = formattedDate + '000000'; // HHMMSS
        var endDate = formattedDate;

        // SET GRAPH LABELS AND INTIALIZE DATA TO ZERO
        var countDate = new Date(lowDate);
        for(var i = countDate;  i <= highDate; i.setDate(i.getDate() + 1)) {
            $scope.salesLabels.push(('0' + (i.getMonth() + 1)).slice(-2) + '/' + ('0' + (i.getDate())).slice(-2));
            $scope.transactionsLabels.push(('0' + (i.getMonth() + 1)).slice(-2) + '/' + ('0' + i.getDate()).slice(-2));
            $scope.salesData[0].push(0);
            $scope.transactionsData[0].push(0);
            d = new Date(i);
            formattedDate = '';
            formattedDate = formattedDate + d.getFullYear();
            formattedDate = formattedDate + ('0' + (d.getMonth() + 1)).slice(-2);
            formattedDate = formattedDate + ('0' + d.getDate()).slice(-2);
            parallelArray.push(formattedDate);
        } // for(var i = countDate;  i <= highDate; i.setDate(i.getDate() + 1))

        if (duration === 'day') {
            $scope.salesLabels.splice(0,0, '');
            $scope.salesData[0].splice(0,0,0);
            $scope.salesLabels.push('');
            $scope.salesData[0].push(0);
            $scope.transactionsLabels.splice(0,0, '');
            $scope.transactionsData[0].splice(0,0,0);
            $scope.transactionsLabels.push('');
            $scope.transactionsData[0].push(0);
        } // if (duration === 'day')

        if (duration === 'year') {
            var tempParallelArray = [];
            var newSalesLabels=[];
            var newSalesData = [[]];
            var newTransactionsLabels=[];
            var newTransactionsData=[[]];
            var newDate = new Date();
            var m = new Date().getMonth();
            var y = new Date().getFullYear();
            
            for (i = m - 12; i <= m; i++) {
                newDate = new Date(y,i,1);
                newSalesLabels.push(newDate.toDateString().substring(4,8) + newDate.toDateString().substring(11,16));
                newTransactionsLabels.push(newDate.toDateString().substring(4,8) + newDate.toDateString().substring(11,16));
                tempParallelArray.push(String(newDate.getFullYear()) + String(('0' + (newDate.getMonth() + 1)).slice(-2)));
                newSalesData[0].push(0);
                newTransactionsData[0].push(0);
            } // for (var i = m - 12; i <= m; i++)
            
            $scope.salesData[0] = newSalesData[0];
            $scope.salesLabels = newSalesLabels;
            $scope.transactionsData[0] = newTransactionsData[0];
            $scope.transactionsLabels = newTransactionsLabels;
        } // if (duration === 'year')

        // grab data for range.
        if ($scope.productId.productId === 0) {
            if (Authentication.user.Roles==='user') {
                var transactionDetail = new Nmitransactions({
                    boardingId:Authentication.user.Boarding_ID,
                    startDate:startDate,
                    endDate:endDate,
                    type: 'get_all_transactions'
                }); // var transactionDetail = new Nmitransactions
                Nmitransactions.get(transactionDetail).$promise.then(
                    function(transResults){
                        if (Object.keys(transResults).length > 2){
                            var transactions = transResults.Response;
                            transactions.forEach(function(item) {
                                if (item.transaction_condition==='complete') {
                                    if( duration !== 'year') {
                                        arrayIndex = parallelArray.indexOf(item.date.substring(0,8));
                                    }else {
                                        arrayIndex = tempParallelArray.indexOf(item.date.substring(0,6));
                                    }
                                    $scope.transactionsData[0][arrayIndex] += 1; // total number of Transactions includes all complete transactions

                                    if (item.success==='1') {
                                        $scope.transApproved += 1; // total number of approved transactions

                                        if (item.action_type === 'sale') {
                                            $scope.salesData[0][arrayIndex] += parseFloat(item.amount) ;  // total number of sales for this date
                                        }else{

                                            if (item.action_type === 'refund') {
                                                $scope.transRefunded += 1;  // total number of refunds
                                            }else{

                                            } // if (item.action.action_type.$t === 'refund')

                                        } // if (item.action.action_type.$t === 'sale')
                                    }else{
                                        $scope.transDeclined += 1; // total number of declines
                                    } //  (item.action.success.$t==='1')

                                } // if (item.condition.$t==='complete')

                            }); // transactions.forEach(function(item)
                        } else {
                            console.log('No transactions found');
                        } // if (Object.keys(transResults).length > 2)
                    });

            } // if (Authentication.user.Roles==='user')

        }else{
            if (Authentication.user.Roles==='user') {
                PaymentService.get({
                    paymentId: Authentication.user.Boarding_ID,
                    productId: $scope.productId.productId
                }).$promise.then(
                    function(results){
                        var transactionDetail = new Nmitransactions({
                            transactionId:'',
                            actionType:'',
                            orderId:'',
                            condition:'',
                            lastName:'',
                            customerVaultId:'',
                            startDate:startDate,
                            endDate:endDate,
                            type: 'treport',
                            processorId:results.Processor_ID,
                            boardingId: Authentication.user.Boarding_ID
                        }); // var transactionDetail = new Nmitransactions
                        Nmitransactions.get(transactionDetail).$promise.then(
                            function(transResults){
                                if (Object.keys(transResults).length > 2){
                                    var transactions = transResults.nm_response.transaction;
                                    transactions.forEach(function(item) {

                                        if (item.condition.$t==='complete') {
                                            if( duration !== 'year') {
                                                arrayIndex = parallelArray.indexOf(item.action.date.$t.substring(0,8));
                                            }else {
                                                arrayIndex = tempParallelArray.indexOf(item.action.date.$t.substring(0,6));
                                            }
                                            $scope.transactionsData[0][arrayIndex] += 1; // total number of Transactions includes all complete transactions

                                            if (item.action.success.$t==='1') {
                                                $scope.transApproved += 1; // total number of approved transactions

                                                if (item.action.action_type.$t === 'sale') {
                                                    $scope.salesData[0][arrayIndex] += parseFloat(item.action.amount.$t) ;  // total number of sales for this date
                                                }else{

                                                    if (item.action.action_type.$t === 'refund') {
                                                        $scope.transRefunded += 1;  // total number of refunds
                                                    }else{

                                                    } // if (item.action.action_type.$t === 'refund')

                                                } // if (item.action.action_type.$t === 'sale')
                                            }else{
                                                $scope.transDeclined += 1; // total number of declines
                                            } //  (item.action.success.$t==='1')

                                        } // if (item.condition.$t==='complete')

                                    }); // transactions.forEach(function(item)
                                } else {
                                    console.log('No transactions found');
                                } // if (Object.keys(transResults).length > 2)
                            }); // Nmitransactions.get
                    }); // PaymentService.get
            }else{

            } // if (Authentication.user.Roles==='user')
        } // if ($scope.Product_ID === 0)
        toggleLoader(false);
        document.getElementById('sales-chart').style.maxHeight = '200px';
        document.getElementById('transactions-chart').style.maxHeight = '84px';
    };

    var param,
        toggleLoader = function(val){
            if (val){
                document.getElementById('loadingImg').style.display='block';
                document.getElementById('report-body').style.display='none';
            }else{
                document.getElementById('loadingImg').style.display='none';
                document.getElementById('report-body').style.display='block';
            }
        },
        sortByKey = function (array, key, sortBy) {
            if (sortBy==='asc'){ // before
                return array.sort(function(a, b) {
                    var x = a[key];
                    var y = b[key];
                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                });
            }else{
                return array.sort(function(a, b) {
                    var x = a[key];
                    var y = b[key];
                    return ((x > y) ? -1 : ((x < y) ? 1 : 0));
                });
            }
        },
        findReverses = function(arr, boarding_id){
            var deferred = $q.defer();
            if (typeof $scope.Product_ID === 'object'){
                // $scope.productId = $scope.productId.Product_ID;// check w/dan
            }

            PaymentService.get({
                paymentId: Authentication.user.Boarding_ID,
                productId:$scope.Product_ID
            }).$promise.then(
                function(results){
                    var find_reverses = new Nmitransactions({
                        type: 'find_reverses',
                        processorId:results.Processor_ID
                    });
                    Nmitransactions.get(find_reverses).$promise.then(
                        function(results){
                            deferred.resolve(results);
                        },
                        function(err){
                            deferred.resolve(err);
                        }
                    );
                },
                function(err){
                    console.log(err);
                }
            );
            return deferred.promise;
        };

    $scope.getDashboard = function(){
        if (Authentication.user.Roles ==='user'){
            var disbursement_schedule = $scope.calculateDisbursementDate(Authentication.user.Boarding_ID);
            disbursement_schedule.then(function(resultingData){
                $scope.disbursement_schedule = resultingData;
                $scope.Product_ID = 0;
                $scope.productIndex = 0;

                var counter = 1;
                $scope.dashboardData = [];
                $scope.dashboardData[0]={
                    processor:'N/A',
                    productId:0,
                    productIndex:0,
                    productName:'All Products',
                    disbursement_schedule: $scope.disbursement_schedule,
                    data: {
                        id:0,
                        Processor_id: 'N/A',
                        Gross_Amount: 0,
                        Net_Amount: 0,
                        Split_Amount: 0,
                        Reserve_Amount: 0,
                        Misc_Fee_Amount: 0,
                        Disbursement: 0,
                        Disbursed_Fees_Amount: 0,
                        Disbursed_Transactions_Amount: 0
                    }
                };
                $scope.Product_NameDASH = $scope.dashboardData[0];
                $scope.Product_NameChangeDASH();

                var formattedDate='';
                var d = new Date();
                formattedDate = formattedDate + d.getFullYear();
                formattedDate = formattedDate + ('0' + (d.getMonth() + 1)).slice(-2);
                formattedDate = formattedDate + ('0' + d.getDate()).slice(-2);
                formattedDate = formattedDate + '000000'; // HHMMSS
//                if (Authentication.user.Roles==='user'){
                    param = $location.path();
                    param = param.split('/');
                    // get each product from the merchant table
                    ProductService.get({
                        productId:'all'
                    }).$promise.then(
                        function(productResults){
                            delete productResults.$promise;
                            delete productResults.$resolved;
                            angular.forEach(productResults, function (list) {

                                // verify whether or not each merchant has this product activated or not
                                PaymentService.get({
                                    paymentId: Authentication.user.Boarding_ID,
                                    productId: list.id
                                }).$promise.then(
                                    function(results){
                                        if (results.Product_ID === list.id && results.Processor_ID !== null) {
//                                            console.log('got a payment plan',results);
                                            // finally, get the most recent balance from the database
                                            Account_BalanceService.get({
                                                processorId: results.Processor_ID,
                                                balanceDate: formattedDate
                                            }).$promise.then(function(balanceResults){
                                                    // Populate view data
                                                    if (balanceResults.response[0] !== undefined) {
                                                        $scope.dashboardData[counter] = { processor: results.Processor_ID,
                                                            productId: list.id,
                                                            productIndex:counter,
                                                            productName: list.Description,
                                                            data: balanceResults.response[0],
                                                            disbursement_schedule: $scope.disbursement_schedule};
                                                        $scope.dashboardData[0].data.Date =  balanceResults.response[0].Date;
                                                        //$scope.dashboardData[0].data.disbursement_schedule = $scope.disbursement_schedule;
                                                        $scope.dashboardData[0].data.Gross_Amount += balanceResults.response[0].Gross_Amount;
                                                        $scope.dashboardData[0].data.Net_Amount += balanceResults.response[0].Net_Amount;
                                                        $scope.dashboardData[0].data.Split_Amount += balanceResults.response[0].Split_Amount;
                                                        $scope.dashboardData[0].data.Reserve_Amount += balanceResults.response[0].Reserve_Amount;
                                                        $scope.dashboardData[0].data.Misc_Fee_Amount += balanceResults.response[0].Misc_Fee_Amount;
                                                        $scope.dashboardData[0].data.Disbursed_Fees_Amount += balanceResults.response[0].Disbursed_Fees_Amount;
                                                        $scope.dashboardData[0].data.Disbursed_Transactions_Amount += balanceResults.response[0].Disbursed_Transactions_Amount;
                                                        counter++;
                                                    }
                                                });
                                        }
                                    });
                            });
                            $scope.dashboardData = sortByKey($scope.dashboardData, 'productId', 'desc');
                        });
                    $scope.config();
                    $scope.getDashboardName();
                    //$scope.AccountBalance();  // replaced

//                }
                $scope.DashboardActivityWrap('week');
            });
        }
    };

    $scope.calculateDisbursementDate = function(boardingId) {
        var deferred = $q.defer();
        var x;
        var disbursementdate1 = new Date();
        var dayOfWeek = disbursementdate1.getDay();
        var dateArray = [];

        if (dayOfWeek !== 1) {
            for (x=new Date(); x.setDate(x.getDate()+1) && x.getDay()!==1; ){} dateArray[1] = x;
        } else {
            dateArray[1] = disbursementdate1;
        }
        if (dayOfWeek !== 2) {
            for (x=new Date(); x.setDate(x.getDate()+1) && x.getDay()!==2; ){} dateArray[2] = x;
        } else {
            dateArray[2] = disbursementdate1;
        }
        if (dayOfWeek !== 3) {
            for (x=new Date(); x.setDate(x.getDate()+1) && x.getDay()!==3; ){} dateArray[3] = x;
        } else {
            dateArray[3] = disbursementdate1;
        }
        if (dayOfWeek !== 4) {
            for (x=new Date(); x.setDate(x.getDate()+1) && x.getDay()!==4; ){} dateArray[4] = x;
        } else {
            dateArray[4] = disbursementdate1;
        }
        if (dayOfWeek !== 5) {
            for (x=new Date(); x.setDate(x.getDate()+1) && x.getDay()!==5; ){} dateArray[5] = x;
        } else {
            dateArray[5] = disbursementdate1;
        }

/*
        disbursementsService.get({
            userboardingId: boardingId
        }, function(schedule) {
console.log($scope.getDisbursementDate(schedule, dayOfWeek, dateArray))
            deferred.resolve($scope.getDisbursementDate(schedule, dayOfWeek, dateArray));
        });
*/
        disbursementsService.get({userboardingId: boardingId}).$promise.then( function(schedule) {
            var disbursementDate = $scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
            deferred.resolve(disbursementDate);
        });


        return deferred.promise;
    };

    $scope.getDashboardName = function(){
        var BoardingSheet = findReverses();
        BoardingSheet.then(function(boarding) {
            $scope.dashboardMerchant_name = boarding.Merchant_Name;
            $scope.dashboardMerchant_status = boarding.Status;
        });
    };
    
    $scope.getDisbursementDate = function(schedule, dayOfWeek, dateArray) {
        var disbursementdate = new Date();

        switch (dayOfWeek) {
            case 0:
            case 1:
            case 6:
                if (schedule.Monday) {
                    disbursementdate = dateArray[1];
                } else if (schedule.Tuesday) {
                    disbursementdate = dateArray[2];
                } else if (schedule.Wednesday) {
                    disbursementdate = dateArray[3];
                } else if (schedule.Thursday) {
                    disbursementdate = dateArray[4];
                } else if (schedule.Friday) {
                    disbursementdate = dateArray[5];
                }
                break;
            case 2:
                if (schedule.Tuesday) {
                    disbursementdate = dateArray[2];
                } else if (schedule.Wednesday) {
                    disbursementdate = dateArray[3];
                } else if (schedule.Thursday) {
                    disbursementdate = dateArray[4];
                } else if (schedule.Friday) {
                    disbursementdate = dateArray[5];
                } else if (schedule.Monday) {
                    disbursementdate = dateArray[1];
                }
                break;
            case 3:
                if (schedule.Wednesday) {
                    disbursementdate = dateArray[3];
                } else if (schedule.Thursday) {
                    disbursementdate = dateArray[4];
                } else if (schedule.Friday) {
                    disbursementdate = dateArray[5];
                } else if (schedule.Monday) {
                    disbursementdate = dateArray[1];
                } else if (schedule.Tuesday) {
                    disbursementdate = dateArray[2];
                }
                break;
            case 4:
                if (schedule.Thursday) {
                    disbursementdate = dateArray[4];
                } else if (schedule.Friday) {
                    disbursementdate = dateArray[5];
                } else if (schedule.Monday) {
                    disbursementdate = dateArray[1];
                } else if (schedule.Tuesday) {
                    disbursementdate = dateArray[2];
                } else if (schedule.Wednesday) {
                    disbursementdate = dateArray[3];
                }
                break;
            case 5:
                if (schedule.Friday) {
                    disbursementdate = dateArray[5];
                } else if (schedule.Monday) {
                    disbursementdate = dateArray[1];
                } else if (schedule.Tuesday) {
                    disbursementdate = dateArray[2];
                } else if (schedule.Wednesday) {
                    disbursementdate = dateArray[3];
                } else if (schedule.Thursday) {
                    disbursementdate = dateArray[4];
                }
                break;
        }
        return disbursementdate;
    };
};
