'use strict';

/**
 * Modal Windows for Reports and Boardings View.
 */

angular.module('creditcard').controller('ModalInstanceCtrl',['$scope','$modalInstance','items','UsersUpdate','$http','Nmitransactions','Line_ItemsService','Authentication',
    function ($scope, $modalInstance, items,UsersUpdate,$http,Nmitransactions,Line_ItemsService,Authentication) {

        var jsonMerchantsProducts = jsonMerchantsProducts || {},
            arrayMerchantsProducts = [],
            arrayMerchantSelected = [],
            arrayProductSelected = [];

        $scope.getLineItems = function(){
            var ProductIdModalSelected = document.getElementById('products').value;
            var processorId = arrayMerchantSelected.products[ProductIdModalSelected].processor;
            
            Line_ItemsService.get({processorId: processorId}).$promise.then(
                function(results){
                    $scope.pendingLineItems = results.response;
                },
                function(err){
                    console.log(err);
                });
        };

        $scope.items = items;

        $scope.ok = function () {
            $scope.success = '';
            $scope.error = null;
            $scope.credentials.Last_Changed_By = Authentication.user.Username;
            $http.post('/auth/forgot', $scope.credentials).success(function(response) {
                // Show user success message and clear form
                $scope.credentials = null;
                $scope.success = response.message;

            }).error(function(response) {
                // Show user error message and clear form
                $scope.credentials = null;
                $scope.success = null;
                $scope.error = response.message;
            });
        };


        $scope.getEmail = function(){
            UsersUpdate.get({
                boardingId: $scope.items
            }, function(user) {
                $scope.email = user.Email;
                $scope.credentials = user;
            });
        };  


        $scope.getEmailAdmin = function(){
            $scope.credentials = $scope.items;
            $scope.email = $scope.items.Email;
        };  


        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };


        /**
         *
         * Below, Time Fees Modal Windows.
         * used by Admin Accounts in Merchant Activity Report.
         *
         */


        $scope.closeModal = function(){
            $modalInstance.close();
        };


        $scope.changeMerchants = function(){
            arrayMerchantSelected = $scope.modalMerchantsProducts;
            var merchantIdModalSelected = parseInt(document.getElementById('merchantIdModal').value);
            var count = Object.keys(arrayMerchantSelected).length;
            if (count>0){
                for (var d = 0, len = count; d < len; d += 1) {
                    if (typeof arrayMerchantSelected[d] !== 'undefined'){
                        if (arrayMerchantSelected[d].id.toString() === merchantIdModalSelected.toString()) {
                            arrayMerchantSelected = arrayMerchantSelected[d];
                        }
                    }
                }
            }
        };
    

        $scope.changeItemType = function(){
            var itemfee = document.getElementById('itemType').value;
            
            /* reset inputs */
            document.getElementById('feeAmount').disabled=false;
            document.getElementById('payment').disabled=false;
            document.getElementById('transaction_id').disabled=false;
            document.getElementById('date').disabled=false;
            document.getElementById('description').disabled=false;
            document.getElementById('payment').selectedIndex = 0;
            
            switch(itemfee) {
                case 'ach_reject':
                    //document.getElementById('payment').selectedIndex = 2;
                    //document.getElementById('payment').disabled=true;
                    $scope.feeAmount = '';
                    document.getElementById('feeAmount').value = '';
                    //document.getElementById('feeAmount').disabled=true;
                    document.getElementById('transaction_id').disabled=true;
                    document.getElementById('description').disabled=true;
                    break;
                case 'chargeback':
                    document.getElementById('payment').selectedIndex = 2;
                    document.getElementById('payment').disabled=true;
                    break;
                case 'misc':
                    document.getElementById('transaction_id').disabled=true;
                    document.getElementById('date').disabled=true;
                    break;
                case 'chargeback_reversal':
                    document.getElementById('payment').selectedIndex = 1;
                    document.getElementById('payment').disabled=true;
                    break;
                case 'retrieval_fee':
                    document.getElementById('payment').selectedIndex = 2;
                    document.getElementById('payment').disabled=true;
                    $scope.feeAmount = '';
                    document.getElementById('feeAmount').value = '';
                    document.getElementById('feeAmount').disabled=true;
                    break;
                case 'reserve_adjustment':
                    document.getElementById('payment').selectedIndex = 1;
                    document.getElementById('payment').disabled=true;
                    document.getElementById('transaction_id').disabled=true;
                    break;
                case 'reserve_increase':
                    document.getElementById('payment').selectedIndex = 2;
                    document.getElementById('payment').disabled=true;
                    document.getElementById('transaction_id').disabled=true;
                    break;
                case 'other':
                    document.getElementById('transaction_id').disabled=true;
                    document.getElementById('date').disabled=true;
                    break;
            }
        
        };
    
    
        $scope.getMerchant = function(){
            $scope.modalMerchantsProducts = $scope.items.merchants_products;
            $scope.products = $scope.items.merchants_products;
            $scope.occurrenceDateModal = $scope.items.occurrenceDateModal;
            $scope.modalAllTransactions = $scope.items.all_transactions;
            $scope.originMerchantId = $scope.items.merchantId.id; // deprecated parseInt(document.getElementById('merchantId').value);
            $scope.products = $scope.items.productId.Product_ID;

            $scope.merchantIdModal = $scope.originMerchantId;
    
            var modalMerchantsProductsFilter = [];
            modalMerchantsProductsFilter = $scope.modalMerchantsProducts;
            modalMerchantsProductsFilter.filter(function (el) {
                if (el.value === $scope.originMerchantId){
                    arrayMerchantSelected = el;
                }
            });
    
            modalMerchantsProductsFilter.filter(function (el) {
                if (el.products){
    //console.log("BRENNAN",el.products);
                    el.products.forEach(function(product){
                        if ((String($scope.merchantIdModal) === String(product.boarding_id)) && (String($scope.products)===String(product.Product_ID))){
                            arrayProductSelected = product;
                        }
                    });
                }
            });

            $scope.arrayMerchantSelected = arrayMerchantSelected;
            $scope.products = arrayProductSelected;
    
            /* modal transaction id */
            $scope.modalTransactionId = $scope.items.transaction_id;
            $scope.modalDescription = $scope.items.description;
    
        };
}]);
