/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('ModalConfBoardCtrl', function() {
        // Initialize global variables
        var ModalConfBoardCtrl,
            scope,
            $modal,
            message,
            modalInstance;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$modal_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            $modal = _$modal_;
            modalInstance = {
                // Create a mock object using spies
                close: jasmine.createSpy('modalInstance.close'),
                dismiss: jasmine.createSpy('modalInstance.dismiss'),
                result: {
                    then: jasmine.createSpy('modalInstance.result.then')
                }
            };
            message = {
                message: "This is the message",
                title: "This is the title",
                accept: "This is the accept",
                deny: "This is the deny"
            };
            
            // Point global variables to injected services
            
            // Initialize the controller
            ModalConfBoardCtrl = $controller('ModalConfBoardCtrl', {
                $scope: scope,
                $modalInstance: modalInstance,
                message: message
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
           it('should not have the variable $scope.title defined', function() {
                expect(scope.title).toBeUndefined();
            }); 
           it('should not have the variable $scope.message defined', function() {
                expect(scope.message).toBeUndefined();
            }); 
           it('should not have the variable $scope.accept defined', function() {
                expect(scope.accept).toBeUndefined();
            }); 
           it('should not have the variable $scope.deny defined', function() {
                expect(scope.deny).toBeUndefined();
            }); 
            it('should have function scope.ok defined', function() {
                expect(scope.ok).toBeDefined();
            });        
            it('should have function scope.getInformation defined', function() {
                expect(scope.getInformation).toBeDefined();
            });        
            it('should have function scope.cancel defined', function() {
                expect(scope.cancel).toBeDefined();
            });        
        });

        describe('Interface', function() {
            it('should recognize ok as a function', function() {
                expect(angular.isFunction(scope.ok)).toBe(true);
            });
            it('should recognize getInformation as a function', function() {
                expect(angular.isFunction(scope.getInformation)).toBe(true);
            });
            it('should recognize cancel as a function', function() {
                expect(angular.isFunction(scope.cancel)).toBe(true);
            });
        });

        describe('scope.ok testing', function() {
            it('should close the $modalInstance', function() {		
                scope.ok();
                expect(modalInstance.close).toHaveBeenCalledWith({ message: 'This is the message', title: 'This is the title', accept: 'This is the accept', deny: 'This is the deny' });
            });
		});

        describe('scope.getInformation testing', function() {
            it('should define and set scope.title', function() {
                expect(scope.title).toBeUndefined();
                scope.getInformation();
                expect(scope.title).toBeDefined();
                expect(scope.title).toBe('This is the title');
            });
            it('should define and set scope.message', function() {
                expect(scope.message).toBeUndefined();
                scope.getInformation();
                expect(scope.message).toBeDefined();
                expect(scope.message).toBe('This is the message');
            });
            it('should define and set scope.accept', function() {
                expect(scope.accept).toBeUndefined();
                scope.getInformation();
                expect(scope.accept).toBeDefined();
                expect(scope.accept).toBe('This is the accept');
            });
            it('should define and set scope.deny', function() {
                expect(scope.deny).toBeUndefined();
                scope.getInformation();
                expect(scope.deny).toBeDefined();
                expect(scope.deny).toBe('This is the deny');
            });
		});

        describe('scope.cancel testing', function() {
            it('should call dismiss on the $modalInstance', function() {
                scope.cancel();
                expect(modalInstance.dismiss).toHaveBeenCalledWith('cancel');
            });
            it('should close the $modalInstance', function() {
                scope.cancel();
                expect(modalInstance.close).toHaveBeenCalledWith();
            });
		});

    });
}());