/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('TransactionsController', function() {
        // Initialize global variables
        var TransactionsController,
            scope,
            $location,
            Authentication;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _Authentication_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            $location = _$location_;
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };
            // Point global variables to injected services
            
            // Initialize the controller
            TransactionsController = $controller('TransactionsController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('Not Logged in', function(){
            it('should not redirect if user is not logged in', function() {
                spyOn($location, 'path');
                expect($location.path).not.toHaveBeenCalledWith('/signin');
	    	});
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });
            it('should have function scope.Initialize defined', function() {
                expect(scope.Initialize).toBeDefined();
            });        
            it('should have function scope.closeMe defined', function() {
                expect(scope.closeMe).toBeDefined();
            });        
        });

        describe('$on function', function() {
            it('should call $scope.Initialize if "successfulResult" is broadcast', function(){
                spyOn(scope,'Initialize').and.returnValue(false);
                scope.$broadcast('succesfullResult');
                scope.$digest();
                expect(scope.Initialize).toHaveBeenCalled();
                expect(scope.Initialize).toHaveBeenCalledWith();
            });
        });

        describe('Initialize function', function() {
            beforeEach(function(){
				this.element = $('<div id="authorizationCodeContainer" class="form-group"></div><div id="transactionIdContainer" class="form-group"></div><div id="responseContainer" class="form-group"></div><div id="responseCodeContainer" class="form-group"></div><div id="avsResponseContainer" class="form-group"></div><div id="cvvResponseContainer" class="form-group"></div><input type="text" id="authorizationCode" /><input type="text" id="transactionId" /><input type="text" id="response" /><input type="text" id="responseCode" /><input type="text" id="avsResponse" /><input type="text" id="cvvResponse" />');
				this.element.appendTo('body');
                
			});
            
            afterEach(function(){
                this.element.remove();
            });

            it('should skip everything if $scope.saletest is not set', function() {
                scope.Initialize();
                scope.$digest();
            });
            it('should hide containers in the Transaction Information if returned empty data and set values to undefined', function() {
                var authorizationCodeContainer = document.getElementById('authorizationCodeContainer'),
                    transactionIdContainer = document.getElementById('transactionIdContainer'),
                    responseContainer = document.getElementById('responseContainer'),
                    responseCodeContainer = document.getElementById('responseCodeContainer'),
                    avsResponseContainer = document.getElementById('avsResponseContainer'),
                    cvvResponseContainer = document.getElementById('cvvResponseContainer'),
                    authorizationCode = document.getElementById('authorizationCode'),
                    transactionId = document.getElementById('transactionId'),
                    response = document.getElementById('response'),
                    responseCode = document.getElementById('responseCode'),
                    avsResponse = document.getElementById('avsResponse'),
                    cvvResponse = document.getElementById('cvvResponse');

                scope.saletest = {};
                scope.Initialize();
                scope.$digest();
                expect(authorizationCodeContainer.style.display).toBe('none');
                expect(transactionIdContainer.style.display).toBe('none');
                expect(responseContainer.style.display).toBe('none');
                expect(responseCodeContainer.style.display).toBe('none');
                expect(avsResponseContainer.style.display).toBe('none');
                expect(cvvResponseContainer.style.display).toBe('none');
                expect(authorizationCode.value).toBe('undefined');
                expect(transactionId.value).toBe('undefined');
                expect(response.value).toBe('undefined');
                expect(responseCode.value).toBe('undefined');
                expect(avsResponse.value).toBe('undefined');
                expect(cvvResponse.value).toBe('undefined');
            });
            it('should hide containers in the Transaction Information if blank is returned and set values appropriately', function() {
                var authorizationCodeContainer = document.getElementById('authorizationCodeContainer'),
                    transactionIdContainer = document.getElementById('transactionIdContainer'),
                    responseContainer = document.getElementById('responseContainer'),
                    responseCodeContainer = document.getElementById('responseCodeContainer'),
                    avsResponseContainer = document.getElementById('avsResponseContainer'),
                    cvvResponseContainer = document.getElementById('cvvResponseContainer'),
                    authorizationCode = document.getElementById('authorizationCode'),
                    transactionId = document.getElementById('transactionId'),
                    response = document.getElementById('response'),
                    responseCode = document.getElementById('responseCode'),
                    avsResponse = document.getElementById('avsResponse'),
                    cvvResponse = document.getElementById('cvvResponse');

                scope.saletest = {
                    authcode: '',
                    transactionid: '',
                    responsetext: '',
                    response_code: '',
                    avsresponse: '',
                    cvvresponse: ''
                };
                scope.Initialize();
                scope.$digest();
                expect(authorizationCodeContainer.style.display).toBe('none');
                expect(transactionIdContainer.style.display).toBe('none');
                expect(responseContainer.style.display).toBe('none');
                expect(responseCodeContainer.style.display).toBe('none');
                expect(avsResponseContainer.style.display).toBe('none');
                expect(cvvResponseContainer.style.display).toBe('none');
                expect(authorizationCode.value).toBe('');
                expect(transactionId.value).toBe('');
                expect(response.value).toBe('');
                expect(responseCode.value).toBe('undefined'); // because of lookup function
                expect(avsResponse.value).toBe('undefined'); // because of lookup function
                expect(cvvResponse.value).toBe('undefined'); // because of lookup function
            });
           it('should show containers in the Transaction Information if populated and set values appropriately', function() {
                var authorizationCodeContainer = document.getElementById('authorizationCodeContainer'),
                    transactionIdContainer = document.getElementById('transactionIdContainer'),
                    responseContainer = document.getElementById('responseContainer'),
                    responseCodeContainer = document.getElementById('responseCodeContainer'),
                    avsResponseContainer = document.getElementById('avsResponseContainer'),
                    cvvResponseContainer = document.getElementById('cvvResponseContainer'),
                    authorizationCode = document.getElementById('authorizationCode'),
                    transactionId = document.getElementById('transactionId'),
                    response = document.getElementById('response'),
                    responseCode = document.getElementById('responseCode'),
                    avsResponse = document.getElementById('avsResponse'),
                    cvvResponse = document.getElementById('cvvResponse');

                scope.saletest = {
                    authcode: '123456',
                    transactionid: '8888888888',
                    responsetext: 'BOLOGNA',
                    response_code: '100',
                    avsresponse: 'X',
                    cvvresponse: 'M'
                };
                scope.Initialize();
                scope.$digest();
                expect(authorizationCodeContainer.style.display).toBe('block');
                expect(transactionIdContainer.style.display).toBe('block');
                expect(responseContainer.style.display).toBe('block');
                expect(responseCodeContainer.style.display).toBe('block');
                expect(avsResponseContainer.style.display).toBe('block');
                expect(cvvResponseContainer.style.display).toBe('block');
                expect(authorizationCode.value).toBe('123456');
                expect(transactionId.value).toBe('8888888888');
                expect(response.value).toBe('BOLOGNA');
                expect(responseCode.value).toBe('Transaction was approved'); // because of lookup function
                expect(avsResponse.value).toBe('Exact match, 9-character numeric ZIP'); // because of lookup function
                expect(cvvResponse.value).toBe('CVV2/CVC2 Match'); // because of lookup function
            });
        });

        describe('closeMe function', function() {
            beforeEach(function(){
				this.element = $('<form id="fieldsForm" style="display:none;"></form><div id="transactionCompleted" style="display:block;"></div>');
				this.element.appendTo('body');
            });
            
            afterEach(function(){
                this.element.remove();
            });

            it('should set the fieldsForm objext to visible', function() {
                var fieldsForm = document.getElementById('fieldsForm'),
                    transactionCompleted = document.getElementById('transactionCompleted');

                expect(fieldsForm.style.display).toBe('none');
                scope.closeMe();
                scope.$digest();
                expect(fieldsForm.style.display).toBe('block');
            });
            it('should set the transactionCompleted objext to hidden', function() {
                var fieldsForm = document.getElementById('fieldsForm'),
                    transactionCompleted = document.getElementById('transactionCompleted');

                expect(transactionCompleted.style.display).toBe('block');
                scope.closeMe();
                scope.$digest();
                expect(transactionCompleted.style.display).toBe('none');
            });
        });
        
    });
}());

(function() {
	describe('TransactionsController II', function() {
		// Initialize global variables
		var TransactionsController,
			scope,
            $location,
            Authentication;

		// Load the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, Authentication) {
			// Set a new global scope
            scope = $rootScope.$new();
            $location = _$location_;
            
            // Point global variables to injected services
			spyOn($location, 'path');
            
    		// Initialize the controller
            TransactionsController = $controller('TransactionsController', {
                $scope: scope,
                Authentication: Authentication
            });

		}));

        describe('Logged in', function(){
           it('should not redirect if user is not logged in', function() {
                expect($location.path).toHaveBeenCalledWith('/signin');
	    	});
        });
    });
}());
