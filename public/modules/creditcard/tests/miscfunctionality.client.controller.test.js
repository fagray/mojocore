/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('miscFuncController ', function() {
        // Initialize global variables
        var SaleController,
            miscFuncController,
            scope,
            $q,
            Authentication,
            PaymentService,
            Nmitransactions,
            ReserveService,
            $location,
            $anchorScroll,
            $filter,
            $parse,
            $modal,
            MasService,
            TransactionsService,
            ProcessorService,
            ProductService,
            MerchantReportService,
            RunningBalanceService,
            SettleService,
            DiscountService,
            TransactionFeeService,
            Account_BalanceService,
            disbursementsService,
            $http,
            AccountBalanceReportService,
            Users,
            ACHReportService,
            PaymentCompleteService,
            LineItemsService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_,_Authentication_,_PaymentService_,_Nmitransactions_,_ReserveService_,_$location_,_$anchorScroll_,_$filter_,_$parse_,_$modal_,_MasService_,_TransactionsService_,_ProcessorService_,_ProductService_,_MerchantReportService_,_RunningBalanceService_,_SettleService_,_DiscountService_,_TransactionFeeService_,_Account_BalanceService_,_disbursementsService_,_$http_,_AccountBalanceReportService_,_Users_,_ACHReportService_,_PaymentCompleteService_,_LineItemsService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();

            $q = _$q_;
            Authentication = _Authentication_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            ReserveService = _ReserveService_;
            $location = _$location_;
            $anchorScroll = _$anchorScroll_;
            $filter = _$filter_;
            $parse = _$parse_;
            $modal = _$modal_;
            MasService = _MasService_;
            TransactionsService = _TransactionsService_;
            ProcessorService = _ProcessorService_;
            ProductService = _ProductService_;
            MerchantReportService = _MerchantReportService_;
            RunningBalanceService = _RunningBalanceService_;
            SettleService = _SettleService_;
            DiscountService = _DiscountService_;
            TransactionFeeService = _TransactionFeeService_;
            Account_BalanceService = _Account_BalanceService_;
            disbursementsService = _disbursementsService_;
            $http = _$http_;
            AccountBalanceReportService = _AccountBalanceReportService_;
            Users = _Users_;
            ACHReportService = _ACHReportService_;
            PaymentCompleteService = _PaymentCompleteService_;
            LineItemsService = _LineItemsService_;
                        
            // Point global variables to injected services
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope
            });
//            
            angular.module('creditcard').miscFuncController( $q,scope,Authentication,PaymentService,Nmitransactions,ReserveService,$location, $anchorScroll,$filter,$parse,$modal,MasService,TransactionsService,ProcessorService,ProductService,MerchantReportService,RunningBalanceService,SettleService,DiscountService,TransactionFeeService,Account_BalanceService,disbursementsService,$http,AccountBalanceReportService,Users,ACHReportService,PaymentCompleteService,LineItemsService);
        }));

        it('should verify true is true', function() {
//console.log(scope);
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });
            it('should have scope.totalCostTransaction defined', function() {
                expect(scope.totalCostTransaction).toBeDefined();
            });
            it('should have scope.humanTitles defined', function() {
                expect(scope.humanTitles).toBeDefined();
            });
            it('should have scope.itemTitles defined', function() {
                expect(scope.itemTitles).toBeDefined();
            });
            it('should have scope.itemList defined', function() {
                expect(scope.itemList).toBeDefined();
            });
            it('should have scope.states defined', function() {
                expect(scope.states ).toBeDefined();
            });
            it('should have scope.countries defined', function() {
                expect(scope.countries).toBeDefined();
            });
            it('should have function scope.verifyCountryCard defined', function() {
                expect(scope.verifyCountryCard).toBeDefined();    
            }); 
            it('should have function scope.verifyCountryShipping defined', function() {
                expect(scope.verifyCountryShipping).toBeDefined();    
            }); 
        });

		describe('Data types/values', function() {
			it('should recognize totalCostTransaction as an object, value [ * ]', function() {
				expect(typeof scope.totalCostTransaction).toBe('number');
				expect(scope.totalCostTransaction).toBe(0);
			});
			it('should recognize humanTitles as an object', function() {
				expect(typeof scope.humanTitles).toBe('object');
                expect(scope.humanTitles).toEqual(['SKU', 'Description', 'Cat.Code', 'Qty.', 'Unit', 'Cost', 'Tax','Disc.','Total']);
			});
			it('should recognize itemTitles as an object', function() {
				expect(typeof scope.itemTitles).toBe('object');
                expect(scope.itemTitles).toEqual(['item_product_code', 'item_description', 'item_commodity_code', 'item_quantity', 'item_unit_of_measure', 'item_unit_cost', 'item_tax_amount','item_discount_amount','item_total_amount']);
			});
			it('should recognize itemList as an object', function() {
				expect(typeof scope.itemList).toBe('object');
                expect(scope.itemList).toEqual([{'item_product_code':'','item_description':'','item_commodity_code':'','item_quantity':'0','item_unit_of_measure':'','item_unit_cost':'0.00','item_tax_amount':'0.00','item_discount_amount':'0.00','item_total_amount':'0.00','item_tax_rate':'','item_tax_total':''}]);
			});
			it('should recognize states as an object', function() {
				expect(typeof scope.states).toBe('object');
                expect(scope.states.length).toBe(59);
			});
			it('should recognize countries as an object', function() {
				expect(typeof scope.countries).toBe('object');
                expect(scope.countries.length).toBe(243);
			});
		});

        describe('Interface', function() {
            it('should recognize verifyCountryCard as a function', function() {
                expect(angular.isFunction(scope.verifyCountryCard)).toBe(true);
            });
            it('should recognize verifyCountryShipping as a function', function() {
                expect(angular.isFunction(scope.verifyCountryShipping)).toBe(true);
            });
        });

        describe('scope.verifyCountryCard function', function(){
            beforeEach(function(){
				this.element = $('<input type="text" id="stateProvinceCardText"><select id="stateProvinceCard"><option value="" selected>--Please Select a State--</option></select>');
				this.element.appendTo('body');
             });
    
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});

            it('should change display style of stateProvinceCardText and stateProvinceCard', function() {
                expect(document.getElementById('stateProvinceCardText').style.display).toBe('');
                expect(document.getElementById('stateProvinceCard').style.display).toBe('');
                scope.countryCard = 'US';
                scope.verifyCountryCard();
                expect(document.getElementById('stateProvinceCardText').style.display).toBe('none');
                expect(document.getElementById('stateProvinceCard').style.display).toBe('block');
                scope.countryCard = 'XX';
                scope.verifyCountryCard();
                expect(document.getElementById('stateProvinceCardText').style.display).toBe('block');
                expect(document.getElementById('stateProvinceCard').style.display).toBe('none');
            });
        });

        describe('scope.verifyCountryShipping function', function(){
            beforeEach(function(){
				this.element = $('<input type="text" id="stateProvinceShippingText"><select id="stateProvinceShipping"> <option value="" selected>--Please Select a State--</option></select>');
				this.element.appendTo('body');
             });
    
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});

            it('should change display style of stateProvinceShippingText and stateProvinceShipping', function() {
                expect(document.getElementById('stateProvinceShippingText').style.display).toBe('');
                expect(document.getElementById('stateProvinceShipping').style.display).toBe('');
                scope.countryShipping = 'US';
                scope.verifyCountryShipping();
                expect(document.getElementById('stateProvinceShippingText').style.display).toBe('none');
                expect(document.getElementById('stateProvinceShipping').style.display).toBe('block');
                scope.countryShipping = 'XX';
                scope.verifyCountryShipping();
                expect(document.getElementById('stateProvinceShippingText').style.display).toBe('block');
                expect(document.getElementById('stateProvinceShipping').style.display).toBe('none');
            });
        });

        
    });
}());