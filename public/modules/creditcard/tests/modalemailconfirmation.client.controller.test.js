/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('ModalEmailConfCtrl', function() {
        // Initialize global variables
        var ModalEmailConfCtrl,
            scope,
            $modal,
            message,
            modalInstance;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$modal_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            $modal = _$modal_;
            modalInstance = {
                // Create a mock object using spies
                close: jasmine.createSpy('modalInstance.close'),
                dismiss: jasmine.createSpy('modalInstance.dismiss'),
                result: {
                    then: jasmine.createSpy('modalInstance.result.then')
                }
            };
            message = {
				title: 'This is the title',
				response: '',
				emailAddress: 'test@email.com'
			};
            
            // Point global variables to injected services
            
            // Initialize the controller
            ModalEmailConfCtrl = $controller('ModalEmailConfCtrl', {
                $scope: scope,
                $modalInstance: modalInstance,
                message: message
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
            it('should have the variable $scope.emailmessage defined', function() {
                expect(scope.emailmessage).toBeDefined();
            }); 
           it('should not have the variable $scope.title defined', function() {
                expect(scope.title).toBeUndefined();
            }); 
           it('should not have the variable $scope.message defined', function() {
                expect(scope.message).toBeUndefined();
            }); 
           it('should not have the variable $scope.accept defined', function() {
                expect(scope.accept).toBeUndefined();
            }); 
            it('should have function scope.ok defined', function() {
                expect(scope.ok).toBeDefined();
            });        
            it('should have function scope.cancel defined', function() {
                expect(scope.cancel).toBeDefined();
            });        
            it('should have function scope.getInformation defined', function() {
                expect(scope.getInformation).toBeDefined();
            });        
        });

        describe('Interface', function() {
            it('should recognize ok as a function', function() {
                expect(angular.isFunction(scope.ok)).toBe(true);
            });
            it('should recognize cancel as a function', function() {
                expect(angular.isFunction(scope.cancel)).toBe(true);
            });
            it('should recognize getInformation as a function', function() {
                expect(angular.isFunction(scope.getInformation)).toBe(true);
            });
        });

        describe('Initialize scope.emailmessage', function() {
            it('should initialize scope.emailmessage to the Authentication.user object', function() {
                expect(scope.emailmessage).toEqual({ title: 'This is the title', response: '', emailAddress: 'test@email.com'});
            });        
		});

        describe('scope.ok testing', function() {
            it('should close the $modalInstance', function() {
				scope.emailAddressModal = 'test@email.com';
                scope.ok();
                expect(modalInstance.close).toHaveBeenCalledWith();
            });
			it('should return an error is there is an invalid e-mail address', function() {
				scope.emailAddressModal = 'test$email.com';
                scope.ok();
                expect(scope.error).toBe('You may have an invalid e-mail address: test$email.com');
				scope.emailAddressModal = 'test1@email.com;test2';
                scope.ok();
                expect(scope.error).toBe('You may have an invalid e-mail address: test2');
			});
			it('should return an error is there are more than 10 e-mail address', function() {
				scope.emailAddressModal = 'test0@email.com;test1@email.com;test2@email.com;test3@email.com;test4@email.com;test5@email.com;test6@email.com;test7@email.com;test8@email.com;test9@email.com;testA@email.com';
                scope.ok();
                expect(scope.error).toBe('You may have a maximum of 10 e-mail addresses, you have 11');
			});
			it('should return an error is a comma is used as a seperator', function() {
				scope.emailAddressModal = 'test0@email.com;test1@email.com,test2@email.com';
                scope.ok();
                expect(scope.error).toBe('There is an illegal character (a comma) in the e-mail list');
			});
		});

        describe('scope.cancel testing', function() {
			it('should call the $modalInstance dismiss with "cancel"', function() {
				scope.cancel();
				expect(modalInstance.dismiss).toHaveBeenCalledWith('cancel');
			});
		});

        describe('scope.getInformation testing', function() {
            it('should define and set scope.title', function() {
                expect(scope.title).toBeUndefined();
                scope.getInformation();
                expect(scope.title).toBeDefined();
                expect(scope.title).toBe('This is the title');
            });

            it('should define and set scope.message', function() {
                expect(scope.emailAddressModal).toBeUndefined();
                scope.getInformation();
                expect(scope.emailAddressModal).toBeDefined();
                expect(scope.emailAddressModal).toBe('test@email.com');
            });

            it('should define and set scope.accept', function() {
                expect(scope.error).toBeUndefined();
                scope.getInformation();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe('');
            });
		});
    });
}());