/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('SaleController', function() {
        // Initialize global variables
        var SaleController,
            CustomerVaultController,
            scope,
            $q,
            Authentication,
            VaultService,
            $location;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_,_$location_,_Authentication_,_VaultService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            $q = _$q_;
            $location = _$location_;
            VaultService = _VaultService_;

            // Point global variables to injected services
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
//console.log(scope);
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
            it('should have scope.param defined', function() {
                expect(scope.param).toBeDefined();
                expect(scope.param).toEqual(['']);
            });        
            it('should have function scope.checkVaultCreate defined', function() {
                expect(scope.checkVaultCreate).toBeDefined();    
            }); 
            it('should have function scope.custVaultInitialize defined', function() {
                expect(scope.custVaultInitialize).toBeDefined();    
            }); 
        });

        describe('Interface', function() {
            it('should recognize checkVaultCreate as a function', function() {
                expect(angular.isFunction(scope.checkVaultCreate)).toBe(true);
            });
            it('should recognize custVaultInitialize as a function', function() {
                expect(angular.isFunction(scope.custVaultInitialize)).toBe(true);
            });
       });

        describe('scope.checkVaultCreate function', function(){
            it('should return true', function() {
               scope.param = ['','create'];
               var returnedValue = scope.checkVaultCreate();
               scope.$digest();
               expect(returnedValue).toBe(true);
            });
            it('should return false', function() {
               scope.param = ['','create','1'];
               var returnedValue = scope.checkVaultCreate();
               scope.$digest();
               expect(returnedValue).toBe(false);
            });
            it('should return false', function() {
               scope.param = ['','update'];
               var returnedValue = scope.checkVaultCreate();
               scope.$digest();
               expect(returnedValue).toBe(false);
            });
         });

        describe('scope.custVaultInitialize I function', function(){
            beforeEach(function() {
                var vaultDeferred,
                    $promise;
                    
                vaultDeferred = $q.defer();
                vaultDeferred.resolve({
	                response: [{
                        "id":"1004916088",
                        "first_name":"John",
                        "last_name":"Doe",
                        "address_1":"Address 1",
                        "address_2":"Address 2",
                        "company":"Company",
                        "city":"City",
                        "state":"ST",
                        "postal_code":"77777",
                        "country":"USA",
                        "email":"name@email.com",
                        "phone":"800-555-1212",
                        "fax":"800-555-3434",
                        "shipping_first_name":"Jane",
                        "shipping_last_name":"Doh",
                        "shipping_address_1":"Shipping Address 1",
                        "shipping_address_2":"Shipping Address 2",
                        "shipping_company":"Shipping Company",
                        "shipping_city":"Shipping City",
                        "shipping_state":"SS",
                        "shipping_postal_code":"99999",
                        "shipping_country":"BGR",
                        "shipping_email":"name2@email.com",
                        "cc_number":"4xxxxxxxxxxx1111",
                        "cc_exp":"1025",
                        "customer_vault_id":"1234567890"
                    }]
                });
                
                 spyOn(VaultService, 'get').and.returnValue({$promise: vaultDeferred.promise});
            });
            
            it('should do nothing, just fall out of IF loop (ELSE branch)', function() {
                scope.param = ['','create'];
                scope.custVaultInitialize();
                scope.$digest();
            });
            it('should call VaultService.get', function() {
                scope.param = ['','update', 25];
                scope.custVaultInitialize();
                scope.$digest();
                expect(VaultService.get).toHaveBeenCalledWith({id: 25});
            });
            it('should not set scope.sameBilling', function() {
                expect(scope.sameBilling).toBeUndefined();
                scope.param = ['','update', 25];
                scope.custVaultInitialize();
                scope.$digest();
                expect(scope.sameBilling).toBeUndefined();
            });
            it('should set scope variables', function() {
                expect(scope.customerVaultId).toBeUndefined();
                expect(scope.creditCardNumber).toBeUndefined();
                expect(scope.expirationDate).toBeUndefined();
                expect(scope.firstNameCard).toBeUndefined();
                expect(scope.lastNameCard).toBeUndefined();
                expect(scope.companyCard).toBeUndefined();
                expect(scope.countryCard).toBeUndefined();
                expect(scope.addressCard).toBeUndefined();
                expect(scope.addressContCard).toBeUndefined();
                expect(scope.cityCard).toBeUndefined();
                expect(scope.stateProvinceCard).toBeUndefined();
                expect(scope.zipPostalCodeCard).toBeUndefined();
                expect(scope.phoneNumberCard).toBeUndefined();
                expect(scope.faxNumberCard).toBeUndefined();
                expect(scope.emailAddressCard).toBeUndefined();
                expect(scope.firstNameShipping).toBeUndefined();
                expect(scope.lastNameShipping).toBeUndefined();
                expect(scope.companyShipping).toBeUndefined();
                expect(scope.countryShipping).toBeUndefined();
                expect(scope.addressShipping).toBeUndefined();
                expect(scope.addressContShipping).toBeUndefined();
                expect(scope.cityShipping).toBeUndefined();
                expect(scope.stateProvinceShipping).toBeUndefined();
                expect(scope.zipPostalCodeShipping).toBeUndefined();
                expect(scope.emailAddressShipping).toBeUndefined();
                
                scope.custVaultInitialize();
                scope.$digest();
                
                expect(scope.customerVaultId).toEqual('1234567890');
                expect(scope.creditCardNumber).toEqual('4xxxxxxxxxxx1111');
                expect(scope.expirationDate).toEqual('1025');
                expect(scope.firstNameCard).toEqual('John');
                expect(scope.lastNameCard).toEqual('Doe');
                expect(scope.companyCard).toEqual('Company');
                expect(scope.countryCard).toEqual('USA');
                expect(scope.addressCard).toEqual('Address 1');
                expect(scope.addressContCard).toEqual('Address 2');
                expect(scope.cityCard).toEqual('City');
                expect(scope.stateProvinceCard).toEqual('ST');
                expect(scope.zipPostalCodeCard).toEqual('77777');
                expect(scope.phoneNumberCard).toEqual('800-555-1212');
                expect(scope.faxNumberCard).toEqual('800-555-3434');
                expect(scope.emailAddressCard).toEqual('name@email.com');
                expect(scope.firstNameShipping).toEqual('Jane');
                expect(scope.lastNameShipping).toEqual('Doh');
                expect(scope.companyShipping).toEqual('Shipping Company');
                expect(scope.countryShipping).toEqual('BGR');
                expect(scope.addressShipping).toEqual('Shipping Address 1');
                expect(scope.addressContShipping).toEqual('Shipping Address 2');
                expect(scope.cityShipping).toEqual('Shipping City');
                expect(scope.stateProvinceShipping).toEqual('SS');
                expect(scope.zipPostalCodeShipping).toEqual('99999');
                expect(scope.emailAddressShipping).toEqual('name2@email.com');
           });
         });

        describe('scope.custVaultInitialize I function', function(){
            beforeEach(function() {
                var vaultDeferred,
                    $promise;
                    
                vaultDeferred = $q.defer();
                vaultDeferred.resolve({
	                response: [{
                        "id":"1004916088",
                        "first_name":"John",
                        "last_name":"Doe",
                        "address_1":"Address 1",
                        "address_2":"Address 2",
                        "company":"Company",
                        "city":"City",
                        "state":"ST",
                        "postal_code":"77777",
                        "country":"USA",
                        "email":"name@email.com",
                        "phone":"800-555-1212",
                        "fax":"800-555-3434",
                        "shipping_first_name":"John",
                        "shipping_last_name":"Doe",
                        "shipping_address_1":"Address 1",
                        "shipping_address_2":"Address 2",
                        "shipping_company":"Company",
                        "shipping_city":"City",
                        "shipping_state":"ST",
                        "shipping_postal_code":"77777",
                        "shipping_country":"USA",
                        "shipping_email":"name@email.com",
                        "cc_number":"4xxxxxxxxxxx1111",
                        "cc_exp":"1025",
                        "customer_vault_id":"1234567890"
                    }]
                });
                
                 spyOn(VaultService, 'get').and.returnValue({$promise: vaultDeferred.promise});
            });
            
            it('should do nothing, just fall out of IF loop (ELSE branch)', function() {
                scope.param = ['','create'];
                scope.custVaultInitialize();
                scope.$digest();
            });
            it('shouldset scope.sameBilling', function() {
                expect(scope.sameBilling).toBeUndefined();
                scope.param = ['','update', 25];
                scope.custVaultInitialize();
                scope.$digest();
                expect(scope.sameBilling).toBe(true);
            });
         });       

         describe('scope.custVaultInitialize III function', function(){
            beforeEach(function() {
                var vaultDeferred,
                    $promise;
                    
                vaultDeferred = $q.defer();
                vaultDeferred.reject('This is an error response');
                
                 spyOn(VaultService, 'get').and.returnValue({$promise: vaultDeferred.promise});
            });
            
            it('should call VaultService.get', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','update', 25];
                scope.custVaultInitialize();
                scope.$digest();
                expect(scope.error).toBe('This is an error response');
            });
         });       
    });

}());
