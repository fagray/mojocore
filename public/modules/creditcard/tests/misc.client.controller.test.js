/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('MiscController', function() {
        // Initialize global variables
        var MiscController,
            scope;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope) {
            // Set a new global scope
            scope = $rootScope.$new().$new();

            // Point global variables to injected services
            
            // Initialize the controller
            MiscController = $controller('MiscController', {
                $scope: scope
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
        });

        describe('$on function', function() {
            it('should define and set the scope.title variable', function(){
                var viewTitle = 'Bogus Title';
                expect(scope.title).toBeUndefined();
                scope.$broadcast('setTitle',viewTitle);
                expect(scope.title).toBeDefined();
                expect(scope.title).toBe(viewTitle);
            });
        });
        
    });
}());