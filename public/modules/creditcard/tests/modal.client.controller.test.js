/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('ModalInstanceCtrl', function() {
        // Initialize global variables
        var $q,
            ModalInstanceCtrl,
            scope,
            $httpBackend,
			$stateParams,
            $location,
            $modal,
            items,
            modalInstance,
            UsersUpdate,
            Line_ItemsService,
            Authentication;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$modal_, _$location_, _$stateParams_, _$httpBackend_, _$q_, _UsersUpdate_,_Line_ItemsService_, _Authentication_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25,
                    Username: 'Updater'
                }
            };
            $location = _$location_;
            $stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
            $q = _$q_;
            $modal = _$modal_;
            UsersUpdate = _UsersUpdate_;
            Line_ItemsService = _Line_ItemsService_;
            
            modalInstance = {
                // Create a mock object using spies
                close: jasmine.createSpy('modalInstance.close'),
                dismiss: jasmine.createSpy('modalInstance.dismiss'),
                result: {
                    then: jasmine.createSpy('modalInstance.result.then')
                }
            };
            items = {
                'transaction_id':'3456789',
                'description': 'theDescription',
                'Email':'email@email.com',
                'merchants_products': 4,
                'occurrenceDateModal': '99999',
                'all_transactions': false,
                'merchant_id': {'id': 25},
                'Product_ID': 1
            };
            
            // Point global variables to injected services
            
            // Initialize the controller
            ModalInstanceCtrl = $controller('ModalInstanceCtrl', {
                $scope: scope,
                $modalInstance: modalInstance,
                items: items,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            }); 
            it('scope.getLineItems', function() {
                expect(scope.getLineItems).toBeDefined();
            }); 
            it('should have the variable $scope.items defined', function() {
                expect(scope.items).toBeDefined();
            }); 
            it('should not have the variable $scope.success defined', function() {
                expect(scope.success).toBeUndefined();
            }); 
            it('should not have the variable $scope.error defined', function() {
                expect(scope.error).toBeUndefined();
            }); 
            it('should not have the variable scope.credentials defined', function() {
                expect(scope.credentials).toBeUndefined();
            }); 
            it('should not have the variable scope.email defined', function() {
                expect(scope.email).toBeUndefined();
            });
            it('should have the variable scope.feeAmount defined', function() {
                expect(scope.feeAmount).toBeUndefined();
            });
            it('should have the variable scope.modalMerchantsProducts defined', function() {
                expect(scope.modalMerchantsProducts).toBeUndefined();
            });
            it('should have the variable scope.products defined', function() {
                expect(scope.products).toBeUndefined();
            });
            it('should have the variable scope.occurrenceDateModal defined', function() {
                expect(scope.occurrenceDateModal).toBeUndefined();
            });
            it('should have the variable scope.modalAllTransactions defined', function() {
                expect(scope.modalAllTransactions).toBeUndefined();
            });
            it('should have the variable scope.originMerchantId defined', function() {
                expect(scope.originMerchantId).toBeUndefined();
            });
            it('should have the variable scope.merchantIdModal defined', function() {
                expect(scope.merchantIdModal).toBeUndefined();
            });
            it('should have the variable scope.arrayMerchantSelected defined', function() {
                expect(scope.arrayMerchantSelected).toBeUndefined();
            });
            it('should have the variable scope.modalTransactionId defined', function() {
                expect(scope.modalTransactionId).toBeUndefined();
            });
            it('should have the variable scope.modalDescription defined', function() {
                expect(scope.modalDescription).toBeUndefined();
            });
            it('should have the function scope.ok defined', function() {
                expect(scope.ok).toBeDefined();
            });
            it('should have the function scope.getEmail defined', function() {
                expect(scope.getEmail).toBeDefined();
            });
            it('should have the function scope.getEmailAdmin defined', function() {
                expect(scope.getEmailAdmin).toBeDefined();
            });
            it('should have the function scope.cancel defined', function() {
                expect(scope.cancel).toBeDefined();
            });
            it('should have the function scope.closeModal defined', function() {
                expect(scope.closeModal).toBeDefined();
            });
            it('should have the function scope.changeMerchants defined', function() {
                expect(scope.changeMerchants).toBeDefined();
            });
            it('should have the function scope.changeItemType defined', function() {
                expect(scope.changeItemType).toBeDefined();
            });
            it('should have the function scope.getMerchant defined', function() {
                expect(scope.getMerchant).toBeDefined();
            });
        });

        describe('Interface', function() {
            it('should recognize getLineItems as a function', function() {
                expect(angular.isFunction(scope.getLineItems)).toBe(true);
            });
             it('should recognize ok as a function', function() {
                expect(angular.isFunction(scope.ok)).toBe(true);
            });
            it('should recognize changeMerchants as a function', function() {
                expect(angular.isFunction(scope.changeMerchants)).toBe(true);
            });
            it('should recognize getEmail as a function', function() {
                expect(angular.isFunction(scope.getEmail)).toBe(true);
            });
            it('should recognize getEmailAdmin as a function', function() {
                expect(angular.isFunction(scope.getEmailAdmin)).toBe(true);
            });
            it('should recognize cancel as a function', function() {
                expect(angular.isFunction(scope.cancel)).toBe(true);
            });
            it('should recognize closeModal as a function', function() {
                expect(angular.isFunction(scope.closeModal)).toBe(true);
            });
            it('should recognize changeMerchants as a function', function() {
                expect(angular.isFunction(scope.changeMerchants)).toBe(true);
            });
            it('should recognize changeItemType as a function', function() {
                expect(angular.isFunction(scope.changeItemType)).toBe(true);
            });
            it('should recognize getMerchant as a function', function() {
                expect(angular.isFunction(scope.getMerchant)).toBe(true);
            });
        });

        describe('scope.getLineItems I testing', function() {
            beforeEach(function() {
				this.element = $('<select id="products"><option value="1" selected>option 1</option></select><select id="merchantIdModal"><option value="2" selected>Product 1</option></select>');
				this.element.appendTo('body');

                var line_itemsDefered,
                    $promise;
                    
                line_itemsDefered = $q.defer();
                line_itemsDefered.resolve({
                    "response":[{
                        "id":99,
                        "Boarding_ID":25,"Processor_ID":"processor1",
                        "Fee_Name":"misc",
                        "Fee_Amount":10,
                        "Debit_Credit":"credit",
                        "Transaction_ID":null,
                        "Occurrence_Date":"20160128",
                        "Description":"test for Karma",
                        "createdAt":"2016-01-28T22:12:00.000Z",
                        "updatedAt":"2016-01-28T22:12:00.000Z"
                }]});
                   
                spyOn(Line_ItemsService, 'get').and.returnValue({$promise: line_itemsDefered.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
                this.element.remove();
            }); 

            it('should call Line_ItemsService.get', function() {
                scope.modalMerchantsProducts = [{id:'1', name:'all', products:[], value:'all'},{id:'2', name:'Product1', products:[{name:'All Products',processor:'all'},{name:'Product 1',processor:'processor1'}], value:'1'}];
                scope.changeMerchants();

                scope.getLineItems();
                scope.$digest();
                expect(Line_ItemsService.get).toHaveBeenCalledWith( {processorId: 'processor1'} );
            });
            it('should set scope.pendingLineItems', function() {
                expect(scope.pendingLineItems).toBeUndefined();
                scope.modalMerchantsProducts = [{id:'1', name:'all', products:[], value:'all'},{id:'2', name:'Product1', products:[{name:'All Products',processor:'all'},{name:'Product 1',processor:'processor1'}], value:'1'}];
                scope.changeMerchants();

                scope.getLineItems();
                scope.$digest();
                expect(scope.pendingLineItems).toEqual([{"id":99,"Boarding_ID":25,"Processor_ID":"processor1","Fee_Name":"misc","Fee_Amount":10,"Debit_Credit":"credit","Transaction_ID":null,"Occurrence_Date":"20160128","Description":"test for Karma","createdAt":"2016-01-28T22:12:00.000Z","updatedAt":"2016-01-28T22:12:00.000Z"}]);
            });
        });

        describe('scope.getLineItems II testing', function() {
            beforeEach(function() {
				this.element = $('<select id="products"><option value="1" selected>option 1</option></select><select id="merchantIdModal"><option value="2" selected>Product 1</option></select>');
				this.element.appendTo('body');
                
                var line_itemsDefered,
                    $promise;
                line_itemsDefered = $q.defer();
                line_itemsDefered.reject('This is an error message');
                   
                spyOn(Line_ItemsService, 'get').and.returnValue({$promise: line_itemsDefered.promise}); //returns a fake promise;
//                spyOn(console, 'log').and.callThrough();
            });
            
            afterEach(function(){
                this.element.remove();
            }); 

            it('should display an error message', function() {
                spyOn(console,'log').and.callThrough();
                scope.modalMerchantsProducts = [{id:'1', name:'all', products:[], value:'all'},{id:'2', name:'Product1', products:[{name:'All Products',processor:'all'},{name:'Product 1',processor:'processor1'}], value:'1'}];
                scope.changeMerchants();

                scope.getLineItems();
                scope.$digest();
                expect(console.log).toHaveBeenCalledWith( 'This is an error message' );
            });
        });

        describe('scope.ok testing', function() {
            it('should define the variable scope.success', function() {
                expect(scope.success).toBeUndefined();
                scope.credentials = {user: 25, name: "testAccount"};
                scope.ok();
                expect(scope.success).toBeDefined();
                expect(scope.success).toBe('');
            }); 
            it('should define the variable $scope.error', function() {
                expect(scope.error).toBeUndefined();
                scope.credentials = {user: 25, name: "testAccount"};
                scope.ok();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe(null);
            }); 
            it('should update scope.success upon successful call to auth.forgot', function() {
                scope.credentials = {user: 25, name: "testAccount"};
                $httpBackend.when('POST', '/auth/forgot').respond(200, {message: 'Success'});
                scope.ok();
                $httpBackend.flush();
                expect(scope.credentials).toBe(null);
                expect(scope.success).toBe('Success');
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should update scope.error upon Unsuccessful call to auth.forgot', function() {
                scope.credentials = {user: 25, name: "testAccount"};
                $httpBackend.when('POST', '/auth/forgot').respond(500, {message: 'ERROR!'});
                scope.ok();
                $httpBackend.flush();
                expect(scope.credentials).toBe(null);
                expect(scope.error).toBe('ERROR!');
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
        });

       describe('scope.getEmail testing', function() {
            beforeEach(function() {
                var usersDeferred,
                    $promise;
                    
                usersDeferred = $q.defer();
                usersDeferred.resolve = {"id":35,"Boarding_ID":25,"First_Name":"Demo","Last_Name":"Account","Display_Name":"Demo Account","Email":"email@email.com","Username":"demoaccount","Roles":"user","Virtual_Terminal":true,"Customer_Vault":true,"Items":null,"Reset_Password_Token":"65dc357a11eb91b9ebabec2147f30514f8913509","Reset_Password_Expires":"2015-06-03T18:53:50.000Z","createdAt":"2015-04-21T20:58:51.000Z","updatedAt":"2015-10-02T16:07:25.000Z","Terminal_Sale":true,"Terminal_Authorize":true,"Terminal_Capture":true,"Terminal_Void":true,"Terminal_Refund":true,"Terminal_Credit":true,"Customer_Add":true,"Customer_List":true,"Transaction_Report":true,"Advanced_Report":true,"Is_Active":true,"Integration":true,"Enable_Nation":false};
                
                spyOn(UsersUpdate, 'get').and.returnValue({$promise: usersDeferred.promise}); //returns a fake promise;
            });

            // unsure how to test UsersUpdate.get(value, function(){}
            it('should call UsersUpdate.get', function() {
                expect(scope.email).toBeUndefined();
                expect(scope.credentials).toBeUndefined();
                scope.items = 25;
                scope.getEmail();
                scope.$digest();
                expect(UsersUpdate.get).toHaveBeenCalled();
            });
            it('should update scope.email and scope.credentials ', function() {
                expect(scope.email).toBeUndefined();
                expect(scope.credentials).toBeUndefined();
                scope.items = 25;
                scope.getEmail();
                scope.$digest();
                expect(UsersUpdate.get).toHaveBeenCalled();
                expect('Able to test modal.client.controller.js function getEmail()').toBe(true);
            });
        });

        describe('scope.getEmailAdmin testing', function() {
            it('should update scope.email and scope.credentials ', function() {
                expect(scope.credentials).toBeUndefined();
                expect(scope.email).toBeUndefined();
                scope.getEmailAdmin();
                scope.$digest();
                
                expect(scope.credentials).toBe(scope.items);
                expect(scope.email).toBe(scope.items.Email);
            });
        });

        describe('scope.cancel testing', function() {
            it('should call modalInstance.dismiss', function() {
                scope.cancel();
                expect(modalInstance.dismiss).toHaveBeenCalledWith('cancel');
            });
		});

        describe('scope.closeModal testing', function() {
            it('should call modalInstance.close', function() {
                scope.closeModal();
                expect(modalInstance.close).toHaveBeenCalledWith();
            });
		});

        describe('scope.changeMerchants testing', function() {
            beforeEach(function(){
				this.element = $('<select id="merchantIdModal"><option value="2" selected>option 1</option></select>');
				this.element.appendTo('body');

                var fieldsForm = document.getElementById('fieldsForm'),
                    transactionCompleted = document.getElementById('transactionCompleted');
            });

            afterEach(function(){
                this.element.remove();
            }); 

            it('should follow the true branches {count > 0, typeof arrayMerchantSelected[d] !== "undefined", arrayMerchantSelected[d].id === merchantIdModalSelected}', function() {
                scope.modalMerchantsProducts = [{id:2}];
                scope.changeMerchants();
            });
            it('should follow the first else branch {count > 0}', function() {
                scope.modalMerchantsProducts = {};
                scope.changeMerchants();
            });
            it('should follow the second else branch {typeof arrayMerchantSelected[d] !== "undefined"}', function() {
                scope.modalMerchantsProducts = [undefined];
                scope.changeMerchants();
            });
            it('should follow the third else branch {arrayMerchantSelected[d].id === merchantIdModalSelected}', function() {
                scope.modalMerchantsProducts = [{id:1}];
                scope.changeMerchants();
            });
		});

        describe('scope.changeItemType testing', function() {
            beforeEach(function(){
				this.element = $('<select id="itemType" ><option value="chargeback">Chargeback</option><option value="retrieval_fee">Retrieval fee</option><option value="chargeback_reversal">Chargeback reversal</option><option value="ach_reject">ACH Reject</option><option value="misc">Misc.</option><option value="reserve_adjustment">Reserve Adjustment</option></select><input type="text" id="feeAmount" value="feeAmount" disabled /><select id="payment" disabled ><option value=""></option><option value="credit" selected>Credit</option><option value="debit">Debit</option></select><input type="text" id="transaction_id" disabled /><input type="date" id="date" disabled><input type="text" id="description" disabled />');
				this.element.appendTo('body');

                var fieldsForm = document.getElementById('fieldsForm'),
                    transactionCompleted = document.getElementById('transactionCompleted');
            });

            afterEach(function(){
                this.element.remove();
            }); 

            it('should set defaults for values and disableds', function() {
                document.getElementById('itemType').value = '';
                scope.changeItemType();
                scope.$digest();
                expect(document.getElementById('feeAmount').disabled).toBe(false);
                expect(document.getElementById('payment').disabled).toBe(false);
                expect(document.getElementById('transaction_id').disabled).toBe(false);
                expect(document.getElementById('date').disabled).toBe(false);
                expect(document.getElementById('description').disabled).toBe(false);
                expect(document.getElementById('payment').selectedIndex).toBe(0);
            });
            it('should set fields dependent on ach_reject', function() {
                document.getElementById('itemType').value = 'ach_reject';
                scope.changeItemType();
                scope.$digest();
                expect(document.getElementById('feeAmount').disabled).toBe(false);
                expect(document.getElementById('payment').disabled).toBe(false);
                expect(document.getElementById('transaction_id').disabled).toBe(true);
                expect(document.getElementById('date').disabled).toBe(false);
                expect(document.getElementById('description').disabled).toBe(true);
                expect(document.getElementById('payment').selectedIndex).toBe(0);
                expect(document.getElementById('feeAmount').value).toBe('');
            });
            it('should set fields dependent on chargeback', function() {
                document.getElementById('itemType').value = 'chargeback';
                scope.changeItemType();
                scope.$digest();
                expect(document.getElementById('feeAmount').disabled).toBe(false);
                expect(document.getElementById('payment').disabled).toBe(true);
                expect(document.getElementById('transaction_id').disabled).toBe(false);
                expect(document.getElementById('date').disabled).toBe(false);
                expect(document.getElementById('description').disabled).toBe(false);
                expect(document.getElementById('payment').selectedIndex).toBe(2);
            });
            it('should set fields dependent on misc', function() {
                document.getElementById('itemType').value = 'misc';
                scope.changeItemType();
                scope.$digest();
                expect(document.getElementById('feeAmount').disabled).toBe(false);
                expect(document.getElementById('payment').disabled).toBe(false);
                expect(document.getElementById('transaction_id').disabled).toBe(true);
                expect(document.getElementById('date').disabled).toBe(true);
                expect(document.getElementById('description').disabled).toBe(false);
                expect(document.getElementById('payment').selectedIndex).toBe(0);
            });
            it('should set fields dependent on misc', function() {
                document.getElementById('itemType').value = 'misc';
                scope.changeItemType();
                scope.$digest();
                expect(document.getElementById('feeAmount').disabled).toBe(false);
                expect(document.getElementById('payment').disabled).toBe(false);
                expect(document.getElementById('transaction_id').disabled).toBe(true);
                expect(document.getElementById('date').disabled).toBe(true);
                expect(document.getElementById('description').disabled).toBe(false);
                expect(document.getElementById('payment').selectedIndex).toBe(0);
            });
            it('should set fields dependent on chargeback_reversal', function() {
                document.getElementById('itemType').value = 'chargeback_reversal';
                scope.changeItemType();
                scope.$digest();
                expect(document.getElementById('feeAmount').disabled).toBe(false);
                expect(document.getElementById('payment').disabled).toBe(true);
                expect(document.getElementById('transaction_id').disabled).toBe(false);
                expect(document.getElementById('date').disabled).toBe(false);
                expect(document.getElementById('description').disabled).toBe(false);
                expect(document.getElementById('payment').selectedIndex).toBe(1);
            });
            it('should set fields dependent on retrieval_fee', function() {
                document.getElementById('itemType').value = 'retrieval_fee';
                expect(scope.feeAmount).toBeUndefined();
                scope.changeItemType();
                scope.$digest();
                expect(document.getElementById('feeAmount').disabled).toBe(true);
                expect(document.getElementById('payment').disabled).toBe(true);
                expect(document.getElementById('transaction_id').disabled).toBe(false);
                expect(document.getElementById('date').disabled).toBe(false);
                expect(document.getElementById('description').disabled).toBe(false);
                expect(document.getElementById('payment').selectedIndex).toBe(2);
                expect(document.getElementById('feeAmount').value).toBe('');
                expect(scope.feeAmount).toBeDefined();
                expect(scope.feeAmount).toBe('');
            });
            it('should set fields dependent on reserve_adjustment', function() {
                document.getElementById('itemType').value = 'reserve_adjustment';
                expect(scope.feeAmount).toBeUndefined();
                scope.changeItemType();
                scope.$digest();
                expect(document.getElementById('feeAmount').disabled).toBe(false);
                expect(document.getElementById('payment').disabled).toBe(true);
                expect(document.getElementById('transaction_id').disabled).toBe(true);
                expect(document.getElementById('date').disabled).toBe(false);
                expect(document.getElementById('description').disabled).toBe(false);
                expect(document.getElementById('payment').selectedIndex).toBe(1);
                expect(scope.feeAmount).toBeUndefined();
            });
		});

        describe('scope.getMerchant testing', function() {
            it('should set scope variables', function() {
                expect(scope.arrayMerchantSelected).toBeUndefined();
                expect(scope.products).toBeUndefined();
                expect(scope.modalTransactionId).toBeUndefined();
                expect(scope.modalDescription).toBeUndefined();
                
                scope.items.merchantId = {id: '55'};
                scope.items.productId = {Product_ID: '100'};
                scope.items.merchants_products = [{test:1, value:'54', products:[{},{}]},{test:2, value:'55', products:[{boarding_id:'55', Product_ID: '99'},{boarding_id:'55', Product_ID: '100'},{boarding_id:'55', Product_ID: '101'}]},{test:3, value:'56', products:[{},{}]}];
                
                scope.getMerchant();
                scope.$digest();
                expect(scope.arrayMerchantSelected).toEqual({ test: 2, value: '55', products: [{ boarding_id: '55', Product_ID: '99' }, { boarding_id: '55', Product_ID: '100' }, { boarding_id: '55', Product_ID: '101' } ] });
                expect(scope.products).toEqual({ boarding_id: '55', Product_ID: '100' });
                expect(scope.modalTransactionId).toEqual('3456789');
                expect(scope.modalDescription).toEqual('theDescription');
            });
            it('should not set scope variables if no products', function() {
                expect(scope.arrayMerchantSelected).toBeUndefined();
                expect(scope.products).toBeUndefined();
                expect(scope.modalTransactionId).toBeUndefined();
                expect(scope.modalDescription).toBeUndefined();
                
                scope.items.merchantId = {id: '54'};
                scope.items.productId = {Product_ID: '9100'};
                scope.items.merchants_products = [{test:1, value:'54'}];
                
                scope.getMerchant();
                scope.$digest();
                expect(scope.arrayMerchantSelected).toEqual({test:1, value:'54'});
                expect(scope.products).toEqual([]);
                expect(scope.modalTransactionId).toEqual('3456789');
                expect(scope.modalDescription).toEqual('theDescription');
            });
		});

    });
}());