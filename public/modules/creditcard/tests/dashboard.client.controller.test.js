/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('dashboardController scope.DashboardActivityWrap [user, productId = 4', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_, _PaymentService_,_Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
//            it('should have scope.param defined', function() {
//                expect(scope.setCss).toBeDefined();
//            }); 
            it('should have scope.DashboardActivityWrap defined', function() {
                expect(scope.DashboardActivityWrap).toBeDefined();
            });       
            it('should have scope.getDashboard defined', function() {
                expect(scope.getDashboard).toBeDefined();
            });       
            it('should have scope.calculateDisbursementDate defined', function() {
                expect(scope.calculateDisbursementDate).toBeDefined();
            });       
            it('should have scope.getDashboardName defined', function() {
                expect(scope.getDashboardName).toBeDefined();
            });
        });

        describe('Interface', function() {
//            it('should recognize setCss as a function', function() {
//                expect(angular.isFunction(scope.setCss)).toBe(true);
//            });
            it('should recognize DashboardActivityWrap as a function', function() {
                expect(angular.isFunction(scope.DashboardActivityWrap)).toBe(true);
            });
            it('should recognize getDashboard as a function', function() {
                expect(angular.isFunction(scope.getDashboard)).toBe(true);
            });
            it('should recognize calculateDisbursementDate as a function', function() {
                expect(angular.isFunction(scope.calculateDisbursementDate)).toBe(true);
            });
            it('should recognize getDashboardName as a function', function() {
                expect(angular.isFunction(scope.getDashboardName)).toBe(true);
            });
         });

        describe('DashboardActivityWrap function', function() {
            beforeEach(function() {
                var thisDate = new Date();
                var yy = thisDate.getFullYear();
                var mm = ("0" + (thisDate.getMonth() + 1)).slice(-2);
                var dd = ("0" + thisDate.getDate()).slice(-2);
                var hh = ("0" + thisDate.getHours()).slice(-2);
                var nn = ("0" + thisDate.getMinutes()).slice(-2);
                var ss = ("0" + thisDate.getSeconds()).slice(-2);
                var formattedDate = yy + mm + dd + hh + nn + ss;

                this.element = $('<div ng-show="dashboardData.length == 2" class="col-md-3 dashboard-products" id="dropdownMenu8"><spanx class="ng-binding" style="float: left;">{{dashboardData[productIndex].productName}}</spanx></div><img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body" style="display:block;"></div><button class="active" id="dayButton">Day</button><button class="active" id="weekButton">Week</button><button class="active" id="monthButton">Month</button><button class="active" id="yearButton">Year</button><div id="sales-chart"  style="maxHeight:10px;display:block;"></div><div id="transactions-chart" style="maxHeight:10px;display:block;">xxxxxxxx</div>');
				this.element.appendTo('body');

                var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({test1:"test1",test2:"test","nm_response":{"transaction":[
                    {"transaction_id":{"$t":"2850078903"},"partial_payment_id":{},"partial_payment_balance":{},"platform_id":{"$t":"-"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"order_id":{"$t":"-"},"authorization_code":{"$t":"123456"},"ponumber":{"$t":"-"},"order_description":{"$t":"-"},"first_name":{"$t":"brennan"},"last_name":{"$t":"harvey"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"77777"},"country":{"$t":"-"},"email":{"$t":"-"},"phone":{"$t":"-"},"fax":{"$t":"-"},"cell_phone":{"$t":"-"},"customertaxid":{"$t":"-"},"website":{"$t":"-"},"shipping_first_name":{"$t":"-"},"shipping_last_name":{"$t":"-"},"shipping_address_1":{"$t":"-"},"shipping_address_2":{"$t":"-"},"shipping_company":{"$t":"-"},"shipping_city":{"$t":"-"},"shipping_state":{"$t":"-"},"shipping_postal_code":{"$t":"-"},"shipping_country":{"$t":"-"},"shipping_email":{"$t":"-"},"shipping_carrier":{"$t":"-"},"tracking_number":{"$t":"-"},"shipping_date":{"$t":"-"},"shipping":{"$t":"0.00"},"shipping_phone":{"$t":"-"},"cc_number":{"$t":"4xxxxxxxxxxx1111"},"cc_hash":{"$t":"f6c609e195d9d4c185dcc8ca662f0180"},"cc_exp":{"$t":"1020"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"Z"},"csc_response":{"$t":"-"},"cardholder_auth":{"$t":"-"},"cc_start_date":{"$t":"-"},"cc_issue_number":{"$t":"-"},"check_account":{"$t":"-"},"check_hash":{"$t":"-"},"check_aba":{"$t":"-"},"check_name":{"$t":"-"},"account_holder_type":{"$t":"-"},"account_type":{"$t":"-"},"sec_code":{"$t":"-"},"drivers_license_number":{"$t":"-"},"drivers_license_state":{"$t":"-"},"drivers_license_dob":{"$t":"-"},"social_security_number":{"$t":"-"},"processor_id":{"$t":"processor1"},"tax":{"$t":"0.00"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{},"merchant_id":{},"boarding_id":{},"action":{"amount":{"$t":"10.00"},"action_type":{"$t":"sale"},"date":{"$t":formattedDate},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"username":{"$t":"UserName"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"},"processor_response_text":{},"processor_response_code":{},"device_license_number":{"$t":"-"},"device_nickname":{"$t":"-"}}},
                    {"transaction_id":{"$t":"2855752411"},"partial_payment_id":{},"partial_payment_balance":{},"platform_id":{"$t":"-"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"order_id":{"$t":"-"},"authorization_code":{"$t":"123456"},"ponumber":{"$t":"-"},"order_description":{"$t":"-"},"first_name":{"$t":"phil"},"last_name":{"$t":"amextest"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"phone":{"$t":"-"},"fax":{"$t":"-"},"cell_phone":{"$t":"-"},"customertaxid":{"$t":"-"},"website":{"$t":"-"},"shipping_first_name":{"$t":"-"},"shipping_last_name":{"$t":"-"},"shipping_address_1":{"$t":"-"},"shipping_address_2":{"$t":"-"},"shipping_company":{"$t":"-"},"shipping_city":{"$t":"-"},"shipping_state":{"$t":"-"},"shipping_postal_code":{"$t":"-"},"shipping_country":{"$t":"US"},"shipping_email":{"$t":"-"},"shipping_carrier":{"$t":"-"},"tracking_number":{"$t":"-"},"shipping_date":{"$t":"-"},"shipping":{"$t":"0.00"},"shipping_phone":{"$t":"-"},"cc_number":{"$t":"3xxxxxxxxxx0005"},"cc_hash":{"$t":"f77ee91c020fe4767c4f21c829eed187"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"cardholder_auth":{"$t":"-"},"cc_start_date":{"$t":"-"},"cc_issue_number":{"$t":"-"},"check_account":{"$t":"-"},"check_hash":{"$t":"-"},"check_aba":{"$t":"-"},"check_name":{"$t":"-"},"account_holder_type":{"$t":"-"},"account_type":{"$t":"-"},"sec_code":{"$t":"-"},"drivers_license_number":{"$t":"-"},"drivers_license_state":{"$t":"-"},"drivers_license_dob":{"$t":"-"},"social_security_number":{"$t":"-"},"processor_id":{"$t":"processor1"},"tax":{"$t":"0.00"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"378282"},"merchant_name":{},"merchant_id":{},"boarding_id":{},"action":{"amount":{"$t":"1.00"},"action_type":{"$t":"sale"},"date":{"$t":formattedDate},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"username":{"$t":"UserName"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"},"processor_response_text":{},"processor_response_code":{},"device_license_number":{"$t":"-"},"device_nickname":{"$t":"-"}}}
                    ]}});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                expect(scope.joinedTransactions).toEqual([]); // from sale controller
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.joinedTransactions).toBeDefined();
                expect(scope.joinedTransactions).toEqual([]);
            });
            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                expect(scope.transApproved).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transApproved).toBeDefined();
                expect(scope.transApproved).toEqual(2);
            });
            it('should set scope.transRefunded', function() {
                var duration = 'day';
                expect(scope.transRefunded).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transRefunded).toBeDefined();
                expect(scope.transRefunded).toEqual(0);
            });
            it('should set scope.transDeclined', function() {
                var duration = 'day';
                expect(scope.transDeclined).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transDeclined).toBeDefined();
                expect(scope.transDeclined).toEqual(0);
            });
            it('should set all buttons to inactive', function() {
                var duration = '';
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('active');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('');
            });
            it('should set day button to active', function() {
                var duration = 'day';
                document.getElementById('dayButton').className = '';
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('active');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('');
            });
            it('should set scope.salesLabels', function() {
                var duration = 'day';
                var today = new Date();
                var displayToday = ("0" + (today.getMonth() + 1)).slice(-2) + "/" +("0" + today.getDate()).slice(-2);
                expect(scope.salesLabels).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesLabels).toBeDefined();
                expect(scope.salesLabels).toEqual([ '', displayToday, '' ]);
            });
            it('should set scope.salesData', function() {
                var duration = 'day';
                expect(scope.salesData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesData).toBeDefined();
                expect(scope.salesData).toEqual([ [ 11 , 0, 0 ] ]);
            });
            it('should set scope.salesSeries', function() {
                var duration = 'day';
                expect(scope.salesSeries).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesSeries).toBeDefined();
                expect(scope.salesSeries).toEqual([ '' ]);
            });
            it('should set scope.salesOptions', function() {
                var duration = 'day';
                expect(scope.salesOptions).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesOptions).toBeDefined();
                expect(scope.salesOptions).toEqual({ maintainAspectRatio: false, responsive: true, bezierCurve: false, animation: false, scaleLabel: '<%= \'$\' + parseFloat(value).toLocaleString() %>', tooltipTemplate: '<%= \'$\' + parseFloat(value).toLocaleString() %>' });
            });
            it('should set scope.salesColours', function() {
                var duration = 'day';
                expect(scope.salesColours).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesColours).toBeDefined();
                expect(scope.salesColours).toEqual([{ fillColor: '#ebebeb', strokeColor: '#3fbaa8', pointColor: '#3fbaa8', pointStrokeColor: '#3fbaa8', pointHighlightFill: '#fff', pointHighlightStroke: '#3fbaa8' }]);
            });
            it('should set scope.transactionsLabels', function() {
                var duration = 'day';
                var today = new Date();
                var displayToday = ("0" + (today.getMonth() + 1)).slice(-2) + "/" +("0" + today.getDate()).slice(-2);
                expect(scope.transactionsLabels).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsLabels).toBeDefined();
                expect(scope.transactionsLabels).toEqual([ '', displayToday, '' ]);
            });
            it('should set scope.transactionsData', function() {
                var duration = 'day';
                expect(scope.transactionsData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsData).toBeDefined();
                expect(scope.transactionsData).toEqual([ [ 2, 0, 0 ] ]);
            });
            it('should set scope.transactionsSeries', function() {
                var duration = 'day';
                expect(scope.transactionsSeries).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsSeries).toBeDefined();
                expect(scope.transactionsSeries).toEqual([ '' ]);
            });
            it('should set scope.transactionsOptions', function() {
                var duration = 'day';
                expect(scope.transactionsOptions).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsOptions).toBeDefined();
                expect(scope.transactionsOptions).toEqual({ maintainAspectRatio: false, responsive: true, bezierCurve: false, animation: false, showScale: false, tooltipTemplate: '<%= parseInt(value).toLocaleString() %>' });
            });
            it('should set scope.transactionsColours', function() {
                var duration = 'day';
                expect(scope.transactionsColours).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsColours).toBeDefined();
                expect(scope.transactionsColours).toEqual([{ fillColor: '#ebebeb', strokeColor: '#3fbaa8', pointColor: '#3fbaa8', pointStrokeColor: '#3fbaa8', pointHighlightFill: '#fff', pointHighlightStroke: '#3fbaa8' }]);
            });
            it('should set day button to active', function() {
                var duration = 'day';
                document.getElementById('dayButton').className = '';
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('active');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('');
            });
            it('should set week button to active', function() {
                var duration = 'week';
                document.getElementById('weekButton').className = '';
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('active');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('');
            });
            it('should set month button to active', function() {
                var duration = 'month';
                document.getElementById('monthButton').className = '';
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('active');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('');
            });
            it('should set year button to active', function() {
                var duration = 'year';
                document.getElementById('yearButton').className = '';
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('active');
            });
            it('should call PaymentService.get with parameters', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 4 });
            });
            it('should call Nmitransactions.get', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(Nmitransactions.get).toHaveBeenCalled();
            });
            it('should not display that no transactions have been found', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(console.log).not.toHaveBeenCalledWith('No transactions found');
            });
            it('should set the height of the sales chart', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                document.getElementById("sales-chart").style.maxHeight = '10px';
                expect(document.getElementById("sales-chart").style.maxHeight).toBe('10px');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById("sales-chart").style.maxHeight).toBe('200px');
            });
            it('should set the height of the transactions chart', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                document.getElementById("transactions-chart").style.maxHeight = '10px';
                expect(document.getElementById("transactions-chart").style.maxHeight).toBe('10px');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById("transactions-chart").style.maxHeight).toBe('84px');
            });
       });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController scope.DashboardActivityWrap [admin, productId = 4]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_, _PaymentService_,_Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'admin'
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));
        
       describe('DashboardActivityWrap function [admin, productId = 4]', function() {
            beforeEach(function() {
                this.element = $('<div ng-show="dashboardData.length == 2" class="col-md-3 dashboard-products" id="dropdownMenu8"><spanx class="ng-binding" style="float: left;">{{dashboardData[productIndex].productName}}</spanx></div><img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body" style="display:block;"></div><button class="active" id="dayButton">Day</button><button class="active" id="weekButton">Week</button><button class="active" id="monthButton">Month</button><button class="active" id="yearButton">Year</button><div id="sales-chart"  style="maxHeight:10px;display:block;"></div><div id="transactions-chart" style="maxHeight:10px;display:block;">xxxxxxxx</div>');
				this.element.appendTo('body');

                var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should set the height of the sales chart', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                document.getElementById("sales-chart").style.maxHeight = '10px';
                expect(document.getElementById("sales-chart").style.maxHeight).toBe('10px');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById("sales-chart").style.maxHeight).toBe('200px');
            });
            it('should set the height of the transactions chart', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                document.getElementById("transactions-chart").style.maxHeight = '10px';
                expect(document.getElementById("transactions-chart").style.maxHeight).toBe('10px');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById("transactions-chart").style.maxHeight).toBe('84px');
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController scope.DashboardActivityWrap [admin, productId = 0]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_, _PaymentService_,_Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'admin'
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));
        
       describe('DashboardActivityWrap function [admin, productId = 0]', function() {
            beforeEach(function() {
                this.element = $('<div ng-show="dashboardData.length == 2" class="col-md-3 dashboard-products" id="dropdownMenu8"><spanx class="ng-binding" style="float: left;">{{dashboardData[productIndex].productName}}</spanx></div><img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body" style="display:block;"></div><button class="active" id="dayButton">Day</button><button class="active" id="weekButton">Week</button><button class="active" id="monthButton">Month</button><button class="active" id="yearButton">Year</button><div id="sales-chart"  style="maxHeight:10px;display:block;"></div><div id="transactions-chart" style="maxHeight:10px;display:block;">xxxxxxxx</div>');
				this.element.appendTo('body');

                var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.joinedTransactions).toEqual([]); // from sale controller
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.joinedTransactions).toBeDefined();
                expect(scope.joinedTransactions).toEqual([]);
            });
            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transApproved).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transApproved).toBeDefined();
                expect(scope.transApproved).toEqual(0);
            });
            it('should set scope.transRefunded', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transRefunded).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transRefunded).toBeDefined();
                expect(scope.transRefunded).toEqual(0);
            });
            it('should set scope.transDeclined', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transDeclined).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transDeclined).toBeDefined();
                expect(scope.transDeclined).toEqual(0);
            });
            it('should set scope.salesData', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.salesData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesData).toBeDefined();
                expect(scope.salesData).toEqual([ [ 0 , 0, 0 ] ]);
            });
            it('should set scope.transactionsData', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transactionsData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsData).toBeDefined();
                expect(scope.transactionsData).toEqual([ [ 0, 0, 0 ] ]);
            });
            it('should not call Nmitransactions.get', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(Nmitransactions.get).not.toHaveBeenCalled();
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController scope.DashboardActivityWrap [user, productId = 0]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_, _PaymentService_,_Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('DashboardActivityWrap III function', function() {
            beforeEach(function() {
                var thisDate = new Date();
                var yy = thisDate.getFullYear();
                var mm = ("0" + (thisDate.getMonth() + 1)).slice(-2);
                var dd = ("0" + thisDate.getDate()).slice(-2);
                var hh = ("0" + thisDate.getHours()).slice(-2);
                var nn = ("0" + thisDate.getMinutes()).slice(-2);
                var ss = ("0" + thisDate.getSeconds()).slice(-2);
                var formattedDate = yy + mm + dd + hh + nn + ss;

                this.element = $('<div ng-show="dashboardData.length == 2" class="col-md-3 dashboard-products" id="dropdownMenu8"><spanx class="ng-binding" style="float: left;">{{dashboardData[productIndex].productName}}</spanx></div><img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body" style="display:block;"></div><button class="active" id="dayButton">Day</button><button class="active" id="weekButton">Week</button><button class="active" id="monthButton">Month</button><button class="active" id="yearButton">Year</button><div id="sales-chart"  style="maxHeight:10px;display:block;"></div><div id="transactions-chart" style="maxHeight:10px;display:block;">xxxxxxxx</div>');
				this.element.appendTo('body');

                var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({test1:"test1",test2:"test2","Response":[
                    {"transaction_id":"2850078903","platform_id":"-","transaction_type":"cc","transaction_condition":"complete","order_id":"-","authorization_code":"123456","ponumber":"-","order_description":"-","first_name":"brennan","last_name":"harvey","address_1":"-","address_2":"-","company":"-","city":"-","state":"-","postal_code":"77777","country":"-","email":"-","phone":"-","fax":"-","cell_phone":"-","customertaxid":"-","customerid":"-","website":"-","shipping_first_name":"-","shipping_last_name":"-","shipping_address_1":"-","shipping_address_2":"-","shipping_company":"-","shipping_city":"-","shipping_state":"-","shipping_postal_code":"-","shipping_country":"-","shipping_email":"-","shipping_carrier":"-","tracking_number":"-","shipping_date":"-","Shipping":"0.00","shipping_phone":"-","cc_number":"4xxxxxxxxxxx1111","cc_hash":"f6c609e195d9d4c185dcc8ca662f0180","cc_exp":"1020","cavv":"-","cavv_result":"-","xid":"-","avs_response":"Z","csc_response":"-","cardholder_auth":"-","cc_start_date":"-","cc_issue_number":"-","check_account":"-","check_hash":"-","check_aba":"-","check_name":"-","account_holder_type":"-","account_type":"-","sec_code":"-","drivers_license_number":"-","drivers_license_state":"-","drivers_license_dob":"-","social_security_number":"-","processor_id":"processor1","tax":"0.00","currency":"USD","surcharge":"-","tip":"-","cc_bin":"411111","amount":"10.00","action_type":"sale","date":formattedDate,"success":"1","ip_address":"23.253.225.245","source":"api","username":"UserName","response_text":"SUCCESS","batch_id":"0","processor_batch_id":"-","response_code":"100","device_license_number":"-","device_nickname":"-","merchantId":"1","id":78,"Original_Transaction_ID":"-","Boarding_ID":25,"Product_ID":4,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-09T23:23:51.000Z","updatedAt":"2015-10-09T17:50:30.000Z","Is_Active":1,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"UserName","Processor_ID":"processor1","Password":"p@55w0rd","Ach_Reject_Fee":0,"Marketplace_Name":null},
                    {"transaction_id":"2855752411","platform_id":"-","transaction_type":"cc","transaction_condition":"complete","order_id":"-","authorization_code":"123456","ponumber":"-","order_description":"-","first_name":"phil","last_name":"amextest","address_1":"-","address_2":"-","company":"-","city":"-","state":"-","postal_code":"92677","country":"US","email":"-","phone":"-","fax":"-","cell_phone":"-","customertaxid":"-","customerid":"-","website":"-","shipping_first_name":"-","shipping_last_name":"-","shipping_address_1":"-","shipping_address_2":"-","shipping_company":"-","shipping_city":"-","shipping_state":"-","shipping_postal_code":"-","shipping_country":"US","shipping_email":"-","shipping_carrier":"-","tracking_number":"-","shipping_date":"-","Shipping":"0.00","shipping_phone":"-","cc_number":"3xxxxxxxxxx0005","cc_hash":"f77ee91c020fe4767c4f21c829eed187","cc_exp":"1025","cavv":"-","cavv_result":"-","xid":"-","avs_response":"N","csc_response":"N","cardholder_auth":"-","cc_start_date":"-","cc_issue_number":"-","check_account":"-","check_hash":"-","check_aba":"-","check_name":"-","account_holder_type":"-","account_type":"-","sec_code":"-","drivers_license_number":"-","drivers_license_state":"-","drivers_license_dob":"-","social_security_number":"-","processor_id":"processor1","tax":"0.00","currency":"USD","surcharge":"-","tip":"-","cc_bin":"378282","amount":"1.00","action_type":"sale","date":formattedDate,"success":"1","ip_address":"23.253.225.245","source":"api","username":"UserName","response_text":"SUCCESS","batch_id":"0","processor_batch_id":"-","response_code":"100","device_license_number":"-","device_nickname":"-","merchantId":"1","id":78,"Original_Transaction_ID":"-","Boarding_ID":25,"Product_ID":4,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-09T23:23:51.000Z","updatedAt":"2015-10-09T17:50:30.000Z","Is_Active":1,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"UserName","Processor_ID":"processor1","Password":"p@55w0rd","Ach_Reject_Fee":0,"Marketplace_Name":null}]});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.joinedTransactions).toEqual([]); // from sale controller
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.joinedTransactions).toBeDefined();
                expect(scope.joinedTransactions).toEqual([]);
            });
            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transApproved).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transApproved).toBeDefined();
                expect(scope.transApproved).toEqual(2);
            });
            it('should set scope.transRefunded', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transRefunded).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transRefunded).toBeDefined();
                expect(scope.transRefunded).toEqual(0);
            });
            it('should set scope.transDeclined', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transDeclined).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transDeclined).toBeDefined();
                expect(scope.transDeclined).toEqual(0);
            });
            it('should set all buttons to inactive', function() {
                var duration = '';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('active');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('');
            });
            it('should set day button to active', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                document.getElementById('dayButton').className = '';
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('active');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('');
            });
            it('should set scope.salesLabels', function() {
                var duration = 'day';
                var today = new Date();
                var displayToday = ("0" + (today.getMonth() + 1)).slice(-2) + "/" +("0" + today.getDate()).slice(-2);
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.salesLabels).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesLabels).toBeDefined();
                expect(scope.salesLabels).toEqual([ '', displayToday, '' ]);
            });
            it('should set scope.salesData', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.salesData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesData).toBeDefined();
                expect(scope.salesData).toEqual([ [ 11 , 0, 0 ] ]);
            });
            it('should set scope.salesSeries', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.salesSeries).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesSeries).toBeDefined();
                expect(scope.salesSeries).toEqual([ '' ]);
            });
            it('should set scope.salesOptions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.salesOptions).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesOptions).toBeDefined();
                expect(scope.salesOptions).toEqual({ maintainAspectRatio: false, responsive: true, bezierCurve: false, animation: false, scaleLabel: '<%= \'$\' + parseFloat(value).toLocaleString() %>', tooltipTemplate: '<%= \'$\' + parseFloat(value).toLocaleString() %>' });
            });
            it('should set scope.salesColours', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.salesColours).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesColours).toBeDefined();
                expect(scope.salesColours).toEqual([{ fillColor: '#ebebeb', strokeColor: '#3fbaa8', pointColor: '#3fbaa8', pointStrokeColor: '#3fbaa8', pointHighlightFill: '#fff', pointHighlightStroke: '#3fbaa8' }]);
            });
            it('should set scope.transactionsLabels', function() {
                var duration = 'day';
                var today = new Date();
                var displayToday = ("0" + (today.getMonth() + 1)).slice(-2) + "/" +("0" + today.getDate()).slice(-2);
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transactionsLabels).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsLabels).toBeDefined();
                expect(scope.transactionsLabels).toEqual([ '', displayToday, '' ]);
            });
            it('should set scope.transactionsData', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transactionsData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsData).toBeDefined();
                expect(scope.transactionsData).toEqual([ [ 2, 0, 0 ] ]);
            });
            it('should set scope.transactionsSeries', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transactionsSeries).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsSeries).toBeDefined();
                expect(scope.transactionsSeries).toEqual([ '' ]);
            });
            it('should set scope.transactionsOptions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transactionsOptions).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsOptions).toBeDefined();
                expect(scope.transactionsOptions).toEqual({ maintainAspectRatio: false, responsive: true, bezierCurve: false, animation: false, showScale: false, tooltipTemplate: '<%= parseInt(value).toLocaleString() %>' });
            });
            it('should set scope.transactionsColours', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transactionsColours).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsColours).toBeDefined();
                expect(scope.transactionsColours).toEqual([{ fillColor: '#ebebeb', strokeColor: '#3fbaa8', pointColor: '#3fbaa8', pointStrokeColor: '#3fbaa8', pointHighlightFill: '#fff', pointHighlightStroke: '#3fbaa8' }]);
            });
            it('should set day button to active', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                document.getElementById('dayButton').className = '';
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('active');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('');
            });
            it('should set week button to active', function() {
                var duration = 'week';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                document.getElementById('weekButton').className = '';
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('active');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('');
            });
            it('should set month button to active', function() {
                var duration = 'month';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                document.getElementById('monthButton').className = '';
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('active');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('');
            });
            it('should set year button to active', function() {
                var duration = 'year';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                document.getElementById('yearButton').className = '';
                expect(document.getElementById('dayButton').className).toBe('active');
                expect(document.getElementById('weekButton').className).toBe('active');
                expect(document.getElementById('monthButton').className).toBe('active');
                expect(document.getElementById('yearButton').className).toBe('');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById('dayButton').className).toBe('');
                expect(document.getElementById('weekButton').className).toBe('');
                expect(document.getElementById('monthButton').className).toBe('');
                expect(document.getElementById('yearButton').className).toBe('active');
            });
            it('should call Nmitransactions.get', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(Nmitransactions.get).toHaveBeenCalled();
            });
            it('should not display that no transactions have been found', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(console.log).not.toHaveBeenCalledWith('No transactions found');
            });
            it('should set the height of the sales chart', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                document.getElementById("sales-chart").style.maxHeight = '10px';
                expect(document.getElementById("sales-chart").style.maxHeight).toBe('10px');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById("sales-chart").style.maxHeight).toBe('200px');
            });
            it('should set the height of the transactions chart', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                document.getElementById("transactions-chart").style.maxHeight = '10px';
                expect(document.getElementById("transactions-chart").style.maxHeight).toBe('10px');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById("transactions-chart").style.maxHeight).toBe('84px');
            });
            it('should set the height of the sales chart', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                document.getElementById("sales-chart").style.maxHeight = '10px';
                expect(document.getElementById("sales-chart").style.maxHeight).toBe('10px');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById("sales-chart").style.maxHeight).toBe('200px');
            });
            it('should set the height of the transactions chart', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                document.getElementById("transactions-chart").style.maxHeight = '10px';
                expect(document.getElementById("transactions-chart").style.maxHeight).toBe('10px');
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(document.getElementById("transactions-chart").style.maxHeight).toBe('84px');
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController scope.DashboardActivityWrap [user, productId = 4, no NMI return]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_, _PaymentService_,_Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('DashboardActivityWrap function [user, productId = 4, no NMI return]', function() {
            beforeEach(function() {
                this.element = $('<div ng-show="dashboardData.length == 2" class="col-md-3 dashboard-products" id="dropdownMenu8"><span class="ng-binding" style="float: left;">{{dashboardData[productIndex].productName}}</span><img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body" style="display:block;"></div><button class="active" id="dayButton">Day</button><button class="active" id="weekButton">Week</button><button class="active" id="monthButton">Month</button><button class="active" id="yearButton">Year</button><div id="sales-chart"  style="maxHeight:10px;display:block;"></div><div id="transactions-chart" style="maxHeight:10px;display:block;">xxxxxxxx</div>');
				this.element.appendTo('body');

               var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({test1:"test1",test2:"test2"});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should set $scope.productId = 0', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.productId).toBeDefined();
                expect(scope.productId).toBe(0);
            });
            it('should display that no transactions have been found', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(console.log).toHaveBeenCalledWith('No transactions found');
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController scope.DashboardActivityWrap [user, productId = SET TO 0, no NMI return]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_, _PaymentService_,_Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('DashboardActivityWrap function [user, productId = SET TO 0, no NMI return]', function() {
            beforeEach(function() {
                this.element = $('<div ng-show="dashboardData.length == 2" class="col-md-3 dashboard-products" id="dropdownMenu8"><spanx class="ng-binding" style="float: left;">{{dashboardData[productIndex].productName}}</xspan><img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body" style="display:block;"></div><button class="active" id="dayButton">Day</button><button class="active" id="weekButton">Week</button><button class="active" id="monthButton">Month</button><button class="active" id="yearButton">Year</button><div id="sales-chart"  style="maxHeight:10px;display:block;"></div><div id="transactions-chart" style="maxHeight:10px;display:block;">xxxxxxxx</div>');
				this.element.appendTo('body');

               var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({test1:"test1",test2:"test2"});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should display that no transactions have been found', function() {
                spyOn(console,'log').and.callThrough();
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(console.log).toHaveBeenCalledWith('No transactions found');
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController scope.DashboardActivityWrap [user, productId = 0, refund present & credit]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_, _PaymentService_,_Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('DashboardActivityWrap function [user, productId = 0, refund present & credit]', function() {
            beforeEach(function() {
                var thisDate = new Date();
                var yy = thisDate.getFullYear();
                var mm = ("0" + (thisDate.getMonth() + 1)).slice(-2);
                var dd = ("0" + thisDate.getDate()).slice(-2);
                var hh = ("0" + thisDate.getHours()).slice(-2);
                var nn = ("0" + thisDate.getMinutes()).slice(-2);
                var ss = ("0" + thisDate.getSeconds()).slice(-2);
                var formattedDate = yy + mm + dd + hh + nn + ss;

this.element = $('<div ng-show="dashboardData.length == 2" class="col-md-3 dashboard-products" id="dropdownMenu8"><spanx class="ng-binding" style="float: left;">{{dashboardData[productIndex].productName}}</spanx></div><img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body" style="display:block;"></div><button class="active" id="dayButton">Day</button><button class="active" id="weekButton">Week</button><button class="active" id="monthButton">Month</button><button class="active" id="yearButton">Year</button><div id="sales-chart"  style="maxHeight:10px;display:block;"></div><div id="transactions-chart" style="maxHeight:10px;display:block;">xxxxxxxx</div>');
				this.element.appendTo('body');

                var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({test1:"test1",test2:"test2","Response":[
                {"transaction_id":"2850078903","platform_id":"-","transaction_type":"cc","transaction_condition":"complete","order_id":"-","authorization_code":"123456","ponumber":"-","order_description":"-","first_name":"brennan","last_name":"harvey","address_1":"-","address_2":"-","company":"-","city":"-","state":"-","postal_code":"77777","country":"-","email":"-","phone":"-","fax":"-","cell_phone":"-","customertaxid":"-","customerid":"-","website":"-","shipping_first_name":"-","shipping_last_name":"-","shipping_address_1":"-","shipping_address_2":"-","shipping_company":"-","shipping_city":"-","shipping_state":"-","shipping_postal_code":"-","shipping_country":"-","shipping_email":"-","shipping_carrier":"-","tracking_number":"-","shipping_date":"-","Shipping":"0.00","shipping_phone":"-","cc_number":"4xxxxxxxxxxx1111","cc_hash":"f6c609e195d9d4c185dcc8ca662f0180","cc_exp":"1020","cavv":"-","cavv_result":"-","xid":"-","avs_response":"Z","csc_response":"-","cardholder_auth":"-","cc_start_date":"-","cc_issue_number":"-","check_account":"-","check_hash":"-","check_aba":"-","check_name":"-","account_holder_type":"-","account_type":"-","sec_code":"-","drivers_license_number":"-","drivers_license_state":"-","drivers_license_dob":"-","social_security_number":"-","processor_id":"processor1","tax":"0.00","currency":"USD","surcharge":"-","tip":"-","cc_bin":"411111","amount":"10.00","action_type":"credit","date":formattedDate,"success":"1","ip_address":"23.253.225.245","source":"api","username":"UserName","response_text":"SUCCESS","batch_id":"0","processor_batch_id":"-","response_code":"100","device_license_number":"-","device_nickname":"-","merchantId":"1","id":78,"Original_Transaction_ID":"-","Boarding_ID":25,"Product_ID":4,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-09T23:23:51.000Z","updatedAt":"2015-10-09T17:50:30.000Z","Is_Active":1,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"UserName","Processor_ID":"processor1","Password":"p@55w0rd","Ach_Reject_Fee":0,"Marketplace_Name":null},
                {"transaction_id":"2855752411","platform_id":"-","transaction_type":"cc","transaction_condition":"complete","order_id":"-","authorization_code":"123456","ponumber":"-","order_description":"-","first_name":"phil","last_name":"amextest","address_1":"-","address_2":"-","company":"-","city":"-","state":"-","postal_code":"92677","country":"US","email":"-","phone":"-","fax":"-","cell_phone":"-","customertaxid":"-","customerid":"-","website":"-","shipping_first_name":"-","shipping_last_name":"-","shipping_address_1":"-","shipping_address_2":"-","shipping_company":"-","shipping_city":"-","shipping_state":"-","shipping_postal_code":"-","shipping_country":"US","shipping_email":"-","shipping_carrier":"-","tracking_number":"-","shipping_date":"-","Shipping":"0.00","shipping_phone":"-","cc_number":"3xxxxxxxxxx0005","cc_hash":"f77ee91c020fe4767c4f21c829eed187","cc_exp":"1025","cavv":"-","cavv_result":"-","xid":"-","avs_response":"N","csc_response":"N","cardholder_auth":"-","cc_start_date":"-","cc_issue_number":"-","check_account":"-","check_hash":"-","check_aba":"-","check_name":"-","account_holder_type":"-","account_type":"-","sec_code":"-","drivers_license_number":"-","drivers_license_state":"-","drivers_license_dob":"-","social_security_number":"-","processor_id":"processor1","tax":"0.00","currency":"USD","surcharge":"-","tip":"-","cc_bin":"378282","amount":"1.00","action_type":"refund","date":formattedDate,"success":"1","ip_address":"23.253.225.245","source":"api","username":"UserName","response_text":"SUCCESS","batch_id":"0","processor_batch_id":"-","response_code":"100","device_license_number":"-","device_nickname":"-","merchantId":"1","id":78,"Original_Transaction_ID":"-","Boarding_ID":25,"Product_ID":4,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-09T23:23:51.000Z","updatedAt":"2015-10-09T17:50:30.000Z","Is_Active":1,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"UserName","Processor_ID":"processor1","Password":"p@55w0rd","Ach_Reject_Fee":0,"Marketplace_Name":null}]});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.joinedTransactions).toEqual([]); // from sale controller
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.joinedTransactions).toBeDefined();
                expect(scope.joinedTransactions).toEqual([]);
            });
            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transApproved).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transApproved).toBeDefined();
                expect(scope.transApproved).toEqual(2);
            });
            it('should set scope.transRefunded', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transRefunded).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transRefunded).toBeDefined();
                expect(scope.transRefunded).toEqual(1);
            });
            it('should set scope.transDeclined', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transDeclined).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transDeclined).toBeDefined();
                expect(scope.transDeclined).toEqual(0);
            });
            it('should set scope.salesData', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.salesData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesData).toBeDefined();
                expect(scope.salesData).toEqual([ [ 0 , 0, 0 ] ]);
            });
            it('should set scope.transactionsData', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transactionsData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsData).toBeDefined();
                expect(scope.transactionsData).toEqual([ [ 2, 0, 0 ] ]);
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController scope.DashboardActivityWrap [user, productId = 0, unsuccessful transaction and pending transaction]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_, _PaymentService_,_Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('DashboardActivityWrap function [user, productId = 0, unsuccessful transaction and pending transaction]', function() {
            beforeEach(function() {
                var thisDate = new Date();
                var yy = thisDate.getFullYear();
                var mm = ("0" + (thisDate.getMonth() + 1)).slice(-2);
                var dd = ("0" + thisDate.getDate()).slice(-2);
                var hh = ("0" + thisDate.getHours()).slice(-2);
                var nn = ("0" + thisDate.getMinutes()).slice(-2);
                var ss = ("0" + thisDate.getSeconds()).slice(-2);
                var formattedDate = yy + mm + dd + hh + nn + ss;

                this.element = $('<div ng-show="dashboardData.length == 2" class="col-md-3 dashboard-products" id="dropdownMenu8"><spanx class="ng-binding" style="float: left;">{{dashboardData[productIndex].productName}}</spanx></div><img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body" style="display:block;"></div><button class="active" id="dayButton">Day</button><button class="active" id="weekButton">Week</button><button class="active" id="monthButton">Month</button><button class="active" id="yearButton">Year</button><div id="sales-chart"  style="maxHeight:10px;display:block;"></div><div id="transactions-chart" style="maxHeight:10px;display:block;">xxxxxxxx</div>');
				this.element.appendTo('body');

                var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({test1:"test1",test2:"test2","Response":[
                    {"transaction_id":"2850078903","platform_id":"-","transaction_type":"cc","transaction_condition":"pending","order_id":"-","authorization_code":"123456","ponumber":"-","order_description":"-","first_name":"brennan","last_name":"harvey","address_1":"-","address_2":"-","company":"-","city":"-","state":"-","postal_code":"77777","country":"-","email":"-","phone":"-","fax":"-","cell_phone":"-","customertaxid":"-","customerid":"-","website":"-","shipping_first_name":"-","shipping_last_name":"-","shipping_address_1":"-","shipping_address_2":"-","shipping_company":"-","shipping_city":"-","shipping_state":"-","shipping_postal_code":"-","shipping_country":"-","shipping_email":"-","shipping_carrier":"-","tracking_number":"-","shipping_date":"-","Shipping":"0.00","shipping_phone":"-","cc_number":"4xxxxxxxxxxx1111","cc_hash":"f6c609e195d9d4c185dcc8ca662f0180","cc_exp":"1020","cavv":"-","cavv_result":"-","xid":"-","avs_response":"Z","csc_response":"-","cardholder_auth":"-","cc_start_date":"-","cc_issue_number":"-","check_account":"-","check_hash":"-","check_aba":"-","check_name":"-","account_holder_type":"-","account_type":"-","sec_code":"-","drivers_license_number":"-","drivers_license_state":"-","drivers_license_dob":"-","social_security_number":"-","processor_id":"processor1","tax":"0.00","currency":"USD","surcharge":"-","tip":"-","cc_bin":"411111","amount":"10.00","action_type":"credit","date":formattedDate,"success":"1","ip_address":"23.253.225.245","source":"api","username":"UserName","response_text":"SUCCESS","batch_id":"0","processor_batch_id":"-","response_code":"100","device_license_number":"-","device_nickname":"-","merchantId":"1","id":78,"Original_Transaction_ID":"-","Boarding_ID":25,"Product_ID":4,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-09T23:23:51.000Z","updatedAt":"2015-10-09T17:50:30.000Z","Is_Active":1,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"UserName","Processor_ID":"processor1","Password":"p@55w0rd","Ach_Reject_Fee":0,"Marketplace_Name":null},
                    {"transaction_id":"2855752411","platform_id":"-","transaction_type":"cc","transaction_condition":"complete","order_id":"-","authorization_code":"123456","ponumber":"-","order_description":"-","first_name":"phil","last_name":"amextest","address_1":"-","address_2":"-","company":"-","city":"-","state":"-","postal_code":"92677","country":"US","email":"-","phone":"-","fax":"-","cell_phone":"-","customertaxid":"-","customerid":"-","website":"-","shipping_first_name":"-","shipping_last_name":"-","shipping_address_1":"-","shipping_address_2":"-","shipping_company":"-","shipping_city":"-","shipping_state":"-","shipping_postal_code":"-","shipping_country":"US","shipping_email":"-","shipping_carrier":"-","tracking_number":"-","shipping_date":"-","Shipping":"0.00","shipping_phone":"-","cc_number":"3xxxxxxxxxx0005","cc_hash":"f77ee91c020fe4767c4f21c829eed187","cc_exp":"1025","cavv":"-","cavv_result":"-","xid":"-","avs_response":"N","csc_response":"N","cardholder_auth":"-","cc_start_date":"-","cc_issue_number":"-","check_account":"-","check_hash":"-","check_aba":"-","check_name":"-","account_holder_type":"-","account_type":"-","sec_code":"-","drivers_license_number":"-","drivers_license_state":"-","drivers_license_dob":"-","social_security_number":"-","processor_id":"processor1","tax":"0.00","currency":"USD","surcharge":"-","tip":"-","cc_bin":"378282","amount":"1.00","action_type":"refund","date":formattedDate,"success":"0","ip_address":"23.253.225.245","source":"api","username":"UserName","response_text":"SUCCESS","batch_id":"0","processor_batch_id":"-","response_code":"100","device_license_number":"-","device_nickname":"-","merchantId":"1","id":78,"Original_Transaction_ID":"-","Boarding_ID":25,"Product_ID":4,"Discount_Rate":0,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":0,"Retrieval_Fee":0,"createdAt":"2015-06-09T23:23:51.000Z","updatedAt":"2015-10-09T17:50:30.000Z","Is_Active":1,"Sale_Fee":0,"Capture_Fee":0,"Void_Fee":1,"Chargeback_Reversal_Fee":0,"Refund_Fee":0,"Credit_Fee":0,"Declined_Fee":1.15,"Username":"UserName","Processor_ID":"processor1","Password":"p@55w0rd","Ach_Reject_Fee":0,"Marketplace_Name":null}]});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.joinedTransactions).toEqual([]); // from sale controller
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.joinedTransactions).toBeDefined();
                expect(scope.joinedTransactions).toEqual([]);
            });
            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transApproved).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transApproved).toBeDefined();
                expect(scope.transApproved).toEqual(0);
            });
            it('should set scope.transRefunded', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transRefunded).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transRefunded).toBeDefined();
                expect(scope.transRefunded).toEqual(0);
            });
            it('should set scope.transDeclined', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transDeclined).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transDeclined).toBeDefined();
                expect(scope.transDeclined).toEqual(1);
            });
            it('should set scope.transactionsData', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 0};
                expect(scope.transactionsData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsData).toBeDefined();
                expect(scope.transactionsData).toEqual([ [ 1, 0, 0 ] ]);
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController scope.DashboardActivityWrap [user, productId = 4, refund present & credit]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_, _PaymentService_,_Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('DashboardActivityWrap function [user, productId = 4, refund present & credit]', function() {
            beforeEach(function() {
                var thisDate = new Date();
                var yy = thisDate.getFullYear();
                var mm = ("0" + (thisDate.getMonth() + 1)).slice(-2);
                var dd = ("0" + thisDate.getDate()).slice(-2);
                var hh = ("0" + thisDate.getHours()).slice(-2);
                var nn = ("0" + thisDate.getMinutes()).slice(-2);
                var ss = ("0" + thisDate.getSeconds()).slice(-2);
                var formattedDate = yy + mm + dd + hh + nn + ss;

                this.element = $('<div ng-show="dashboardData.length == 2" class="col-md-3 dashboard-products" id="dropdownMenu8"><spanx class="ng-binding" style="float: left;">{{dashboardData[productIndex].productName}}</spanx></div><img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body" style="display:block;"></div><button class="active" id="dayButton">Day</button><button class="active" id="weekButton">Week</button><button class="active" id="monthButton">Month</button><button class="active" id="yearButton">Year</button><div id="sales-chart"  style="maxHeight:10px;display:block;"></div><div id="transactions-chart" style="maxHeight:10px;display:block;">xxxxxxxx</div>');
				this.element.appendTo('body');

                var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({test1:"test1",test2:"test","nm_response":{"transaction":[
                    {"transaction_id":{"$t":"2850078903"},"partial_payment_id":{},"partial_payment_balance":{},"platform_id":{"$t":"-"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"order_id":{"$t":"-"},"authorization_code":{"$t":"123456"},"ponumber":{"$t":"-"},"order_description":{"$t":"-"},"first_name":{"$t":"brennan"},"last_name":{"$t":"harvey"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"77777"},"country":{"$t":"-"},"email":{"$t":"-"},"phone":{"$t":"-"},"fax":{"$t":"-"},"cell_phone":{"$t":"-"},"customertaxid":{"$t":"-"},"website":{"$t":"-"},"shipping_first_name":{"$t":"-"},"shipping_last_name":{"$t":"-"},"shipping_address_1":{"$t":"-"},"shipping_address_2":{"$t":"-"},"shipping_company":{"$t":"-"},"shipping_city":{"$t":"-"},"shipping_state":{"$t":"-"},"shipping_postal_code":{"$t":"-"},"shipping_country":{"$t":"-"},"shipping_email":{"$t":"-"},"shipping_carrier":{"$t":"-"},"tracking_number":{"$t":"-"},"shipping_date":{"$t":"-"},"shipping":{"$t":"0.00"},"shipping_phone":{"$t":"-"},"cc_number":{"$t":"4xxxxxxxxxxx1111"},"cc_hash":{"$t":"f6c609e195d9d4c185dcc8ca662f0180"},"cc_exp":{"$t":"1020"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"Z"},"csc_response":{"$t":"-"},"cardholder_auth":{"$t":"-"},"cc_start_date":{"$t":"-"},"cc_issue_number":{"$t":"-"},"check_account":{"$t":"-"},"check_hash":{"$t":"-"},"check_aba":{"$t":"-"},"check_name":{"$t":"-"},"account_holder_type":{"$t":"-"},"account_type":{"$t":"-"},"sec_code":{"$t":"-"},"drivers_license_number":{"$t":"-"},"drivers_license_state":{"$t":"-"},"drivers_license_dob":{"$t":"-"},"social_security_number":{"$t":"-"},"processor_id":{"$t":"processor1"},"tax":{"$t":"0.00"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{},"merchant_id":{},"boarding_id":{},"action":{"amount":{"$t":"10.00"},"action_type":{"$t":"credit"},"date":{"$t":formattedDate},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"username":{"$t":"UserName"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"},"processor_response_text":{},"processor_response_code":{},"device_license_number":{"$t":"-"},"device_nickname":{"$t":"-"}}},
                    {"transaction_id":{"$t":"2855752411"},"partial_payment_id":{},"partial_payment_balance":{},"platform_id":{"$t":"-"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"order_id":{"$t":"-"},"authorization_code":{"$t":"123456"},"ponumber":{"$t":"-"},"order_description":{"$t":"-"},"first_name":{"$t":"phil"},"last_name":{"$t":"amextest"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"phone":{"$t":"-"},"fax":{"$t":"-"},"cell_phone":{"$t":"-"},"customertaxid":{"$t":"-"},"website":{"$t":"-"},"shipping_first_name":{"$t":"-"},"shipping_last_name":{"$t":"-"},"shipping_address_1":{"$t":"-"},"shipping_address_2":{"$t":"-"},"shipping_company":{"$t":"-"},"shipping_city":{"$t":"-"},"shipping_state":{"$t":"-"},"shipping_postal_code":{"$t":"-"},"shipping_country":{"$t":"US"},"shipping_email":{"$t":"-"},"shipping_carrier":{"$t":"-"},"tracking_number":{"$t":"-"},"shipping_date":{"$t":"-"},"shipping":{"$t":"0.00"},"shipping_phone":{"$t":"-"},"cc_number":{"$t":"3xxxxxxxxxx0005"},"cc_hash":{"$t":"f77ee91c020fe4767c4f21c829eed187"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"cardholder_auth":{"$t":"-"},"cc_start_date":{"$t":"-"},"cc_issue_number":{"$t":"-"},"check_account":{"$t":"-"},"check_hash":{"$t":"-"},"check_aba":{"$t":"-"},"check_name":{"$t":"-"},"account_holder_type":{"$t":"-"},"account_type":{"$t":"-"},"sec_code":{"$t":"-"},"drivers_license_number":{"$t":"-"},"drivers_license_state":{"$t":"-"},"drivers_license_dob":{"$t":"-"},"social_security_number":{"$t":"-"},"processor_id":{"$t":"processor1"},"tax":{"$t":"0.00"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"378282"},"merchant_name":{},"merchant_id":{},"boarding_id":{},"action":{"amount":{"$t":"1.00"},"action_type":{"$t":"refund"},"date":{"$t":formattedDate},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"username":{"$t":"UserName"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"},"processor_response_text":{},"processor_response_code":{},"device_license_number":{"$t":"-"},"device_nickname":{"$t":"-"}}}]}});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                expect(scope.joinedTransactions).toEqual([]); // from sale controller
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.joinedTransactions).toBeDefined();
                expect(scope.joinedTransactions).toEqual([]);
            });
            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                expect(scope.transApproved).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transApproved).toBeDefined();
                expect(scope.transApproved).toEqual(2);
            });
            it('should set scope.transRefunded', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                expect(scope.transRefunded).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transRefunded).toBeDefined();
                expect(scope.transRefunded).toEqual(1);
            });
            it('should set scope.transDeclined', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                expect(scope.transDeclined).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transDeclined).toBeDefined();
                expect(scope.transDeclined).toEqual(0);
            });
            it('should set scope.salesData', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                expect(scope.salesData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesData).toBeDefined();
                expect(scope.salesData).toEqual([ [ 0 , 0, 0 ] ]);
            });
            it('should set scope.transactionsData', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                expect(scope.transactionsData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsData).toBeDefined();
                expect(scope.transactionsData).toEqual([ [ 2, 0, 0 ] ]);
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController scope.DashboardActivityWrap [user, productId = 4, unsuccessful transaction and pending transaction]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_, _PaymentService_,_Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

         describe('DashboardActivityWrap function [user, productId = 4, unsuccessful transaction and pending transaction]', function() {
            beforeEach(function() {
                var thisDate = new Date();
                var yy = thisDate.getFullYear();
                var mm = ("0" + (thisDate.getMonth() + 1)).slice(-2);
                var dd = ("0" + thisDate.getDate()).slice(-2);
                var hh = ("0" + thisDate.getHours()).slice(-2);
                var nn = ("0" + thisDate.getMinutes()).slice(-2);
                var ss = ("0" + thisDate.getSeconds()).slice(-2);
                var formattedDate = yy + mm + dd + hh + nn + ss;

                this.element = $('<div ng-show="dashboardData.length == 2" class="col-md-3 dashboard-products" id="dropdownMenu8"><spanx class="ng-binding" style="float: left;">{{dashboardData[productIndex].productName}}</spanx></div><img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body" style="display:block;"></div><button class="active" id="dayButton">Day</button><button class="active" id="weekButton">Week</button><button class="active" id="monthButton">Month</button><button class="active" id="yearButton">Year</button><div id="sales-chart"  style="maxHeight:10px;display:block;"></div><div id="transactions-chart" style="maxHeight:10px;display:block;">xxxxxxxx</div>');
				this.element.appendTo('body');

                var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({test1:"test1",test2:"test","nm_response":{"transaction":[
                 {"transaction_id":{"$t":"2850078903"},"partial_payment_id":{},"partial_payment_balance":{},"platform_id":{"$t":"-"},"transaction_type":{"$t":"cc"},"condition":{"$t":"pending"},"order_id":{"$t":"-"},"authorization_code":{"$t":"123456"},"ponumber":{"$t":"-"},"order_description":{"$t":"-"},"first_name":{"$t":"brennan"},"last_name":{"$t":"harvey"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"77777"},"country":{"$t":"-"},"email":{"$t":"-"},"phone":{"$t":"-"},"fax":{"$t":"-"},"cell_phone":{"$t":"-"},"customertaxid":{"$t":"-"},"website":{"$t":"-"},"shipping_first_name":{"$t":"-"},"shipping_last_name":{"$t":"-"},"shipping_address_1":{"$t":"-"},"shipping_address_2":{"$t":"-"},"shipping_company":{"$t":"-"},"shipping_city":{"$t":"-"},"shipping_state":{"$t":"-"},"shipping_postal_code":{"$t":"-"},"shipping_country":{"$t":"-"},"shipping_email":{"$t":"-"},"shipping_carrier":{"$t":"-"},"tracking_number":{"$t":"-"},"shipping_date":{"$t":"-"},"shipping":{"$t":"0.00"},"shipping_phone":{"$t":"-"},"cc_number":{"$t":"4xxxxxxxxxxx1111"},"cc_hash":{"$t":"f6c609e195d9d4c185dcc8ca662f0180"},"cc_exp":{"$t":"1020"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"Z"},"csc_response":{"$t":"-"},"cardholder_auth":{"$t":"-"},"cc_start_date":{"$t":"-"},"cc_issue_number":{"$t":"-"},"check_account":{"$t":"-"},"check_hash":{"$t":"-"},"check_aba":{"$t":"-"},"check_name":{"$t":"-"},"account_holder_type":{"$t":"-"},"account_type":{"$t":"-"},"sec_code":{"$t":"-"},"drivers_license_number":{"$t":"-"},"drivers_license_state":{"$t":"-"},"drivers_license_dob":{"$t":"-"},"social_security_number":{"$t":"-"},"processor_id":{"$t":"processor1"},"tax":{"$t":"0.00"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{},"merchant_id":{},"boarding_id":{},"action":{"amount":{"$t":"10.00"},"action_type":{"$t":"credit"},"date":{"$t":formattedDate},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"username":{"$t":"UserName"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"},"processor_response_text":{},"processor_response_code":{},"device_license_number":{"$t":"-"},"device_nickname":{"$t":"-"}}},
                 {"transaction_id":{"$t":"2855752411"},"partial_payment_id":{},"partial_payment_balance":{},"platform_id":{"$t":"-"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"order_id":{"$t":"-"},"authorization_code":{"$t":"123456"},"ponumber":{"$t":"-"},"order_description":{"$t":"-"},"first_name":{"$t":"phil"},"last_name":{"$t":"amextest"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"phone":{"$t":"-"},"fax":{"$t":"-"},"cell_phone":{"$t":"-"},"customertaxid":{"$t":"-"},"website":{"$t":"-"},"shipping_first_name":{"$t":"-"},"shipping_last_name":{"$t":"-"},"shipping_address_1":{"$t":"-"},"shipping_address_2":{"$t":"-"},"shipping_company":{"$t":"-"},"shipping_city":{"$t":"-"},"shipping_state":{"$t":"-"},"shipping_postal_code":{"$t":"-"},"shipping_country":{"$t":"US"},"shipping_email":{"$t":"-"},"shipping_carrier":{"$t":"-"},"tracking_number":{"$t":"-"},"shipping_date":{"$t":"-"},"shipping":{"$t":"0.00"},"shipping_phone":{"$t":"-"},"cc_number":{"$t":"3xxxxxxxxxx0005"},"cc_hash":{"$t":"f77ee91c020fe4767c4f21c829eed187"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"cardholder_auth":{"$t":"-"},"cc_start_date":{"$t":"-"},"cc_issue_number":{"$t":"-"},"check_account":{"$t":"-"},"check_hash":{"$t":"-"},"check_aba":{"$t":"-"},"check_name":{"$t":"-"},"account_holder_type":{"$t":"-"},"account_type":{"$t":"-"},"sec_code":{"$t":"-"},"drivers_license_number":{"$t":"-"},"drivers_license_state":{"$t":"-"},"drivers_license_dob":{"$t":"-"},"social_security_number":{"$t":"-"},"processor_id":{"$t":"processor1"},"tax":{"$t":"0.00"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"378282"},"merchant_name":{},"merchant_id":{},"boarding_id":{},"action":{"amount":{"$t":"1.00"},"action_type":{"$t":"refund"},"date":{"$t":formattedDate},"success":{"$t":"0"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"username":{"$t":"UserName"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"},"processor_response_text":{},"processor_response_code":{},"device_license_number":{"$t":"-"},"device_nickname":{"$t":"-"}}}]}});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = {productId: 4};
                expect(scope.joinedTransactions).toEqual([]); // from sale controller
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.joinedTransactions).toBeDefined();
                expect(scope.joinedTransactions).toEqual([]);
            });
            it('should set scope.joinedTransactions', function() {
                var duration = 'day';
                scope.productId = {productId: 4};
                expect(scope.transApproved).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transApproved).toBeDefined();
                expect(scope.transApproved).toEqual(0);
            });
            it('should set scope.transRefunded', function() {
                var duration = 'day';
                scope.productId = {productId: 4};
                expect(scope.transRefunded).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transRefunded).toBeDefined();
                expect(scope.transRefunded).toEqual(0);
            });
            it('should set scope.transDeclined', function() {
                var duration = 'day';
                scope.productId = {productId: 4};
                expect(scope.transDeclined).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transDeclined).toBeDefined();
                expect(scope.transDeclined).toEqual(1);
            });
            it('should set scope.salesData', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                expect(scope.salesData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.salesData).toBeDefined();
                expect(scope.salesData).toEqual([ [ 0 , 0, 0 ] ]);
            });

            it('should set scope.transactionsData', function() {
                var duration = 'day';
                scope.productId = undefined;
                scope.productId = {productId: 4};
                expect(scope.transactionsData).toBeUndefined();
                scope.DashboardActivityWrap(duration);
                scope.$digest();
                expect(scope.transactionsData).toBeDefined();
                expect(scope.transactionsData).toEqual([ [ 1, 0, 0 ] ]);
            });
       });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController getDashboard  [user=user]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            ProductService,
            PaymentService,
            Account_BalanceService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _Authentication_, _$q_, _ProductService_, _PaymentService_, _Account_BalanceService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            ProductService = _ProductService_;
            PaymentService = _PaymentService_;
            Account_BalanceService = _Account_BalanceService_;

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

         describe('getDashboard function', function() {
            beforeEach(function() {
                var disbursementDateDeferred,
                    productDeferred,
                    paymentDeferred,
                    account_BalanceDeferred,
                    $promise;
                

                disbursementDateDeferred = $q.defer();
                disbursementDateDeferred.resolve("Fri Nov 13 2015 10:06:32 GMT-0800 (Pacific Standard Time)");
               
                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                
                paymentDeferred = $q.defer();
                paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});

                account_BalanceDeferred = $q.defer();
                account_BalanceDeferred.resolve({"response":[{"id":293705,"Processor_ID":"processor1","Date":"20151112000000","Gross_Amount":0,"Net_Amount"
:0,"Split_Amount":0,"Reserve_Amount":0,"Misc_Fee_Amount":0,"Disbursement":0,"Disbursed_Fees_Amount":0,"Disbursed_Transactions_Amount":0,"Current_Reserve_Amount":0,"Current_Split_Amount":0,"Previous_Net_Plus_Gross":0,"Disbursed_Amount":0}]});

                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Account_BalanceService, 'get').and.returnValue({$promise: account_BalanceDeferred.promise}); //returns a fake promise;
                
                spyOn(scope,'calculateDisbursementDate').and.returnValue(disbursementDateDeferred.promise);
                spyOn(scope,'config').and.returnValue(false);
                spyOn(scope,'getDashboardName').and.returnValue(false);
                spyOn(scope,'DashboardActivityWrap').and.returnValue(false);
                spyOn(scope,'Product_NameChangeDASH').and.returnValue(false);
           });
            it('should call scope.calculateDisbursementDate', function() {
                scope.getDashboard();
                scope.$digest();
                expect(scope.calculateDisbursementDate).toHaveBeenCalledWith(25);
            });
            it('should set scope.disbursement_schedule', function() {
                expect(scope.disbursement_schedule).toBeUndefined();
                scope.getDashboard();
                scope.$digest();
                expect(scope.disbursement_schedule).toBe("Fri Nov 13 2015 10:06:32 GMT-0800 (Pacific Standard Time)");
            });
            it('should set scope.Product_ID', function() {
                expect(scope.Product_ID).toBe(2); // from sale controller
                scope.getDashboard();
                scope.$digest();
                expect(scope.Product_ID).toBe(0);
            });
            it('should set scope.productIndex', function() {
                expect(scope.productIndex).toBeUndefined();
                scope.getDashboard();
                scope.$digest();
                expect(scope.productIndex).toBe(0);
            });
            it('should set scope.dashboardData', function() {
                expect(scope.dashboardData).toBeUndefined();
                scope.getDashboard();
                scope.$digest();
                expect(scope.dashboardData).toEqual([ { processor: 'N/A', productId: 0, productIndex: 0, productName: 'All Products', disbursement_schedule: 'Fri Nov 13 2015 10:06:32 GMT-0800 (Pacific Standard Time)', data: { id: 0, Processor_id: 'N/A', Gross_Amount: 0, Net_Amount: 0, Split_Amount: 0, Reserve_Amount: 0, Misc_Fee_Amount: 0, Disbursement: 0, Disbursed_Fees_Amount: 0, Disbursed_Transactions_Amount: 0, Date: '20151112000000' }}, { processor: 'processor1', productId: 1, productIndex: 1, productName: null, data: { id: 293705, Processor_ID: 'processor1', Date: '20151112000000', Gross_Amount: 0, Net_Amount: 0, Split_Amount: 0, Reserve_Amount: 0, Misc_Fee_Amount: 0, Disbursement: 0, Disbursed_Fees_Amount: 0, Disbursed_Transactions_Amount: 0, Current_Reserve_Amount: 0, Current_Split_Amount: 0, Previous_Net_Plus_Gross: 0, Disbursed_Amount: 0 }, disbursement_schedule: 'Fri Nov 13 2015 10:06:32 GMT-0800 (Pacific Standard Time)' } ]);
            });
            it('should set scope.Product_NameDASH', function() {
                expect(scope.Product_NameDASH).toBeUndefined();
                scope.getDashboard();
                scope.$digest();
                expect(scope.Product_NameDASH).toEqual({ processor: 'N/A', productId: 0, productIndex: 0, productName: 'All Products', disbursement_schedule: 'Fri Nov 13 2015 10:06:32 GMT-0800 (Pacific Standard Time)', data: { id: 0, Processor_id: 'N/A', Gross_Amount: 0, Net_Amount: 0, Split_Amount: 0, Reserve_Amount: 0, Misc_Fee_Amount: 0, Disbursement: 0, Disbursed_Fees_Amount: 0, Disbursed_Transactions_Amount: 0, Date: '20151112000000' }});
            });
            it('should call scope.Product_NameChangeDASH', function() {
                scope.getDashboard();
                scope.$digest();
                expect(scope.Product_NameChangeDASH).toHaveBeenCalledWith();
            });
            it('should call ProductService.get', function() {
                scope.getDashboard();
                scope.$digest();
                expect(ProductService.get).toHaveBeenCalledWith({ productId: 'all' });
            });
            it('should call PaymentService.get', function() {
                scope.getDashboard();
                scope.$digest();
                expect(PaymentService.get.calls.count()).toBe(5);
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 1 });
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 2 });
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 3 });
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 4 });
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 5 });
            });

       });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController getDashboard  [user=admin]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            ProductService,
            PaymentService,
            Account_BalanceService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _Authentication_, _$q_, _ProductService_, _PaymentService_, _Account_BalanceService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'admin'
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            ProductService = _ProductService_;
            PaymentService = _PaymentService_;
            Account_BalanceService = _Account_BalanceService_;

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

         describe('getDashboard function', function() {
            beforeEach(function() {
                var disbursementDateDeferred,
                    productDeferred,
                    paymentDeferred,
                    account_BalanceDeferred,
                    $promise;
                

                disbursementDateDeferred = $q.defer();
                disbursementDateDeferred.resolve("Fri Nov 13 2015 10:06:32 GMT-0800 (Pacific Standard Time)");
               
                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                
                paymentDeferred = $q.defer();
                paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});

                account_BalanceDeferred = $q.defer();
                account_BalanceDeferred.resolve({"response":[{"id":293705,"Processor_ID":"processor1","Date":"20151112000000","Gross_Amount":0,"Net_Amount":0,"Split_Amount":0,"Reserve_Amount":0,"Misc_Fee_Amount":0,"Disbursement":0,"Disbursed_Fees_Amount":0,"Disbursed_Transactions_Amount":0,"Current_Reserve_Amount":0,"Current_Split_Amount":0,"Previous_Net_Plus_Gross":0,"Disbursed_Amount":0}]});

                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Account_BalanceService, 'get').and.returnValue({$promise: account_BalanceDeferred.promise}); //returns a fake promise;
                
                spyOn(scope,'calculateDisbursementDate').and.returnValue(disbursementDateDeferred.promise);
                spyOn(scope,'config').and.returnValue(false);
                spyOn(scope,'getDashboardName').and.returnValue(false);
                spyOn(scope,'DashboardActivityWrap').and.returnValue(false);
                spyOn(scope,'Product_NameChangeDASH').and.returnValue(false);
           });
            it('should call scope.calculateDisbursementDate', function() {
                scope.getDashboard();
                scope.$digest();
                expect(scope.calculateDisbursementDate).not.toHaveBeenCalled();
            });
            it('should set scope.disbursement_schedule', function() {
                expect(scope.disbursement_schedule).toBeUndefined();
                scope.getDashboard();
                scope.$digest();
                expect(scope.disbursement_schedule).toBeUndefined();
            });
            it('should set scope.Product_ID', function() {
                expect(scope.Product_ID).toBe(2); // from sale controller
                scope.getDashboard();
                scope.$digest();
                expect(scope.Product_ID).toBe(2);
            });
            it('should set scope.productIndex', function() {
                expect(scope.productIndex).toBeUndefined();
                scope.getDashboard();
                scope.$digest();
                expect(scope.productIndex).toBeUndefined();
            });
            it('should set scope.dashboardData', function() {
                expect(scope.dashboardData).toBeUndefined();
                scope.getDashboard();
                scope.$digest();
                expect(scope.dashboardData).toBeUndefined();
            });
            it('should set scope.Product_NameDASH', function() {
                expect(scope.Product_NameDASH).toBeUndefined();
                scope.getDashboard();
                scope.$digest();
                expect(scope.Product_NameDASH).toBeUndefined();
            });
            it('should call scope.Product_NameChangeDASH', function() {
                scope.getDashboard();
                scope.$digest();
                expect(scope.Product_NameChangeDASH).not.toHaveBeenCalled();
            });
            it('should call ProductService.get', function() {
                scope.getDashboard();
                scope.$digest();
                expect(ProductService.get).not.toHaveBeenCalledWith();
            });
            it('should call PaymentService.get', function() {
                scope.getDashboard();
                scope.$digest();
                expect(PaymentService.get).not.toHaveBeenCalled();
            });

       });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController getDashboard  [user=user, no account balance record]', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            ProductService,
            PaymentService,
            Account_BalanceService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _Authentication_, _$q_, _ProductService_, _PaymentService_, _Account_BalanceService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            ProductService = _ProductService_;
            PaymentService = _PaymentService_;
            Account_BalanceService = _Account_BalanceService_;

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

         describe('getDashboard function', function() {
            beforeEach(function() {
                var disbursementDateDeferred,
                    productDeferred,
                    paymentDeferred,
                    account_BalanceDeferred,
                    $promise;
                

                disbursementDateDeferred = $q.defer();
                disbursementDateDeferred.resolve("Fri Nov 13 2015 10:06:32 GMT-0800 (Pacific Standard Time)");
               
                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                
                paymentDeferred = $q.defer();
                paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});

                account_BalanceDeferred = $q.defer();
                account_BalanceDeferred.resolve({"response":{}});

                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Account_BalanceService, 'get').and.returnValue({$promise: account_BalanceDeferred.promise}); //returns a fake promise;
                
                spyOn(scope,'calculateDisbursementDate').and.returnValue(disbursementDateDeferred.promise);
                spyOn(scope,'config').and.returnValue(false);
                spyOn(scope,'getDashboardName').and.returnValue(false);
                spyOn(scope,'DashboardActivityWrap').and.returnValue(false);
                spyOn(scope,'Product_NameChangeDASH').and.returnValue(false);
            });

            it('should set scope.dashboardData', function() {
                expect(scope.dashboardData).toBeUndefined();
                scope.getDashboard();
                scope.$digest();
                expect(scope.dashboardData).toEqual([{ processor: 'N/A', productId: 0, productIndex: 0, productName: 'All Products', disbursement_schedule: 'Fri Nov 13 2015 10:06:32 GMT-0800 (Pacific Standard Time)', data:{ id: 0, Processor_id: 'N/A', Gross_Amount: 0, Net_Amount: 0, Split_Amount: 0, Reserve_Amount: 0, Misc_Fee_Amount: 0, Disbursement: 0, Disbursed_Fees_Amount: 0, Disbursed_Transactions_Amount: 0 }}]);
            });
            it('should set scope.Product_NameDASH', function() {
                expect(scope.Product_NameDASH).toBeUndefined();
                scope.getDashboard();
                scope.$digest();
                expect(scope.Product_NameDASH).toEqual({ processor: 'N/A', productId: 0, productIndex: 0, productName: 'All Products', disbursement_schedule: 'Fri Nov 13 2015 10:06:32 GMT-0800 (Pacific Standard Time)', data:{ id: 0, Processor_id: 'N/A', Gross_Amount: 0, Net_Amount: 0, Split_Amount: 0, Reserve_Amount: 0, Misc_Fee_Amount: 0, Disbursement: 0, Disbursed_Fees_Amount: 0, Disbursed_Transactions_Amount: 0 }});
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController calculateDisbursementDate', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            disbursementsService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _Authentication_, _$q_, _disbursementsService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            disbursementsService = _disbursementsService_;

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('getDashboard function', function() {
            beforeEach(function() {
                var disbursementDeferred,
                    $promise;
                

                disbursementDeferred = $q.defer();
                disbursementDeferred.resolve({"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"});
               
                spyOn(disbursementsService, 'get').and.returnValue({$promise: disbursementDeferred.promise}); //returns a fake promise;
            });

            it('should call disbursementsService.get', function() {
                scope.calculateDisbursementDate(25);
                scope.$digest();
                expect(disbursementsService.get).toHaveBeenCalledWith({ userboardingId: 25 });
            });
        });
        
        describe('getDashboard function Day of Week = 1', function() {
            beforeEach(function() {
                var disbursementDeferred,
                    $promise;              

                disbursementDeferred = $q.defer();
                disbursementDeferred.resolve({"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"});
               
                spyOn(disbursementsService, 'get').and.returnValue({$promise: disbursementDeferred.promise}); //returns a fake promise;
            });

            it('should call scope.getDisbursementDate with the correct Params', function() {
                spyOn(scope,'getDisbursementDate').and.callThrough();
                var result;
                var mockDate = new Date();
                var mockDate1 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate2 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate3 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate4 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate5 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate6 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate7 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                
                jasmine.clock().mockDate(mockDate1);
                result = scope.calculateDisbursementDate(25);
                scope.$digest();
                
                expect(result).toEqual({then:jasmine.any(Function), catch:jasmine.any(Function), finally:jasmine.any(Function)});
                expect(scope.getDisbursementDate.calls.argsFor(0)[0]).toEqual({ id: 18, Boarding_ID: 25, Weekly: false, Monday: true, Tuesday: false, Wednesday: true, Thursday: false, Friday: true, recurweekly: 1, daymonth: false, datemonth: null, everydate: null, dayorder: null, dayname: null, everymonth: null, Hold: 0, createdAt: '2015-04-21T21:00:28.000Z', updatedAt: '2015-04-21T21:00:28.000Z' });
                expect(scope.getDisbursementDate.calls.argsFor(0)[1]).toEqual(3);
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][0]).toBeUndefined();
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][1].toString()).toBe(mockDate6.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][2].toString()).toBe(mockDate7.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][3].toString()).toBe(mockDate1.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][4].toString()).toBe(mockDate2.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][5].toString()).toBe(mockDate3.toString());
            });
        });

        describe('getDashboard function Day of Week = 0', function() {
            beforeEach(function() {
                var disbursementDeferred,
                    $promise;              

                disbursementDeferred = $q.defer();
                disbursementDeferred.resolve({"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"});
               
                spyOn(disbursementsService, 'get').and.returnValue({$promise: disbursementDeferred.promise}); //returns a fake promise;
            });

            it('should call scope.getDisbursementDate with the correct Params', function() {
                spyOn(scope,'getDisbursementDate').and.callThrough();
                var result;
                var mockDate = new Date(2016,4,8,7,0,0); // Sunday 7:00 am
                var mockDate1 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate2 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate3 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate4 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate5 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate6 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate7 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                
                jasmine.clock().mockDate(mockDate1);
                result = scope.calculateDisbursementDate(25);
                scope.$digest();
                
                expect(result).toEqual({then:jasmine.any(Function), catch:jasmine.any(Function), finally:jasmine.any(Function)});
                expect(scope.getDisbursementDate.calls.argsFor(0)[0]).toEqual({ id: 18, Boarding_ID: 25, Weekly: false, Monday: true, Tuesday: false, Wednesday: true, Thursday: false, Friday: true, recurweekly: 1, daymonth: false, datemonth: null, everydate: null, dayorder: null, dayname: null, everymonth: null, Hold: 0, createdAt: '2015-04-21T21:00:28.000Z', updatedAt: '2015-04-21T21:00:28.000Z' });
                expect(scope.getDisbursementDate.calls.argsFor(0)[1]).toEqual(1);
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][0]).toBeUndefined();
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][1].toString()).toBe(mockDate1.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][2].toString()).toBe(mockDate2.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][3].toString()).toBe(mockDate3.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][4].toString()).toBe(mockDate4.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][5].toString()).toBe(mockDate5.toString());
            });
        });

        describe('getDashboard function Day of Week = 1', function() {
            beforeEach(function() {
                var disbursementDeferred,
                    $promise;              

                disbursementDeferred = $q.defer();
                disbursementDeferred.resolve({"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"});
               
                spyOn(disbursementsService, 'get').and.returnValue({$promise: disbursementDeferred.promise}); //returns a fake promise;
            });

            it('should call scope.getDisbursementDate with the correct Params', function() {
                spyOn(scope,'getDisbursementDate').and.callThrough();
                var result;
                var mockDate = new Date(2016,4,9,7,0,0); // Monday 7:00 am
                var mockDate1 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate2 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate3 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate4 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate5 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate6 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate7 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                
                jasmine.clock().mockDate(mockDate1);
                result = scope.calculateDisbursementDate(25);
                scope.$digest();
                
                expect(result).toEqual({then:jasmine.any(Function), catch:jasmine.any(Function), finally:jasmine.any(Function)});
                expect(scope.getDisbursementDate.calls.argsFor(0)[0]).toEqual({ id: 18, Boarding_ID: 25, Weekly: false, Monday: true, Tuesday: false, Wednesday: true, Thursday: false, Friday: true, recurweekly: 1, daymonth: false, datemonth: null, everydate: null, dayorder: null, dayname: null, everymonth: null, Hold: 0, createdAt: '2015-04-21T21:00:28.000Z', updatedAt: '2015-04-21T21:00:28.000Z' });
                expect(scope.getDisbursementDate.calls.argsFor(0)[1]).toEqual(2);
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][0]).toBeUndefined();
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][1].toString()).toBe(mockDate7.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][2].toString()).toBe(mockDate1.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][3].toString()).toBe(mockDate2.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][4].toString()).toBe(mockDate3.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][5].toString()).toBe(mockDate4.toString());
            });
        });

        describe('getDashboard function Day of the Week = 2', function() {
            beforeEach(function() {
                var disbursementDeferred,
                    $promise;
                

                disbursementDeferred = $q.defer();
                disbursementDeferred.resolve({"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"});
               
                spyOn(disbursementsService, 'get').and.returnValue({$promise: disbursementDeferred.promise}); //returns a fake promise;
            });


            it('should call scope.getDisbursementDate with the correct Params', function() {
                spyOn(scope,'getDisbursementDate').and.callThrough();
                var result;
                var mockDate = new Date(2016,4,10,7,0,0); // Tuesday 7:00 am
                var mockDate1 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate2 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate3 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate4 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate5 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate6 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate7 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                
                jasmine.clock().mockDate(mockDate1);
                result = scope.calculateDisbursementDate(25);
                scope.$digest();
                
                expect(result).toEqual({then:jasmine.any(Function), catch:jasmine.any(Function), finally:jasmine.any(Function)});
                expect(scope.getDisbursementDate.calls.argsFor(0)[0]).toEqual({ id: 18, Boarding_ID: 25, Weekly: false, Monday: true, Tuesday: false, Wednesday: true, Thursday: false, Friday: true, recurweekly: 1, daymonth: false, datemonth: null, everydate: null, dayorder: null, dayname: null, everymonth: null, Hold: 0, createdAt: '2015-04-21T21:00:28.000Z', updatedAt: '2015-04-21T21:00:28.000Z' });
                expect(scope.getDisbursementDate.calls.argsFor(0)[1]).toEqual(3);
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][0]).toBeUndefined();
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][1].toString()).toBe(mockDate6.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][2].toString()).toBe(mockDate7.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][3].toString()).toBe(mockDate1.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][4].toString()).toBe(mockDate2.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][5].toString()).toBe(mockDate3.toString());
            });
        });

        describe('getDashboard function Day of the Week = 3', function() {
            beforeEach(function() {
                var disbursementDeferred,
                    $promise;
                

                disbursementDeferred = $q.defer();
                disbursementDeferred.resolve({"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"});
               
                spyOn(disbursementsService, 'get').and.returnValue({$promise: disbursementDeferred.promise}); //returns a fake promise;
            });

            it('should call scope.getDisbursementDate with the correct Params', function() {
                spyOn(scope,'getDisbursementDate').and.callThrough();
                var result;
                var mockDate = new Date(2016,4,11,7,0,0); // Wednesday 7:00 am
                var mockDate1 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate2 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate3 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate4 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate5 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate6 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate7 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                
                jasmine.clock().mockDate(mockDate1);
                result = scope.calculateDisbursementDate(25);
                scope.$digest();
                
                expect(result).toEqual({then:jasmine.any(Function), catch:jasmine.any(Function), finally:jasmine.any(Function)});
                expect(scope.getDisbursementDate.calls.argsFor(0)[0]).toEqual({ id: 18, Boarding_ID: 25, Weekly: false, Monday: true, Tuesday: false, Wednesday: true, Thursday: false, Friday: true, recurweekly: 1, daymonth: false, datemonth: null, everydate: null, dayorder: null, dayname: null, everymonth: null, Hold: 0, createdAt: '2015-04-21T21:00:28.000Z', updatedAt: '2015-04-21T21:00:28.000Z' });
                expect(scope.getDisbursementDate.calls.argsFor(0)[1]).toEqual(4);
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][0]).toBeUndefined();
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][1].toString()).toBe(mockDate5.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][2].toString()).toBe(mockDate6.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][3].toString()).toBe(mockDate7.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][4].toString()).toBe(mockDate1.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][5].toString()).toBe(mockDate2.toString());
            });
        });

        describe('getDashboard function Day of the Week = 4', function() {
            beforeEach(function() {
                var disbursementDeferred,
                    $promise;
                

                disbursementDeferred = $q.defer();
                disbursementDeferred.resolve({"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"});
               
                spyOn(disbursementsService, 'get').and.returnValue({$promise: disbursementDeferred.promise}); //returns a fake promise;
            });

            it('should call scope.getDisbursementDate with the correct Params', function() {
                spyOn(scope,'getDisbursementDate').and.callThrough();
                var result;
                var mockDate = new Date(2016,4,12,7,0,0); // Thursday 7:00 am
                var mockDate1 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate2 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate3 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate4 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate5 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate6 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate7 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                
                jasmine.clock().mockDate(mockDate1);
                result = scope.calculateDisbursementDate(25);
                scope.$digest();
                
                expect(result).toEqual({then:jasmine.any(Function), catch:jasmine.any(Function), finally:jasmine.any(Function)});
                expect(scope.getDisbursementDate.calls.argsFor(0)[0]).toEqual({ id: 18, Boarding_ID: 25, Weekly: false, Monday: true, Tuesday: false, Wednesday: true, Thursday: false, Friday: true, recurweekly: 1, daymonth: false, datemonth: null, everydate: null, dayorder: null, dayname: null, everymonth: null, Hold: 0, createdAt: '2015-04-21T21:00:28.000Z', updatedAt: '2015-04-21T21:00:28.000Z' });
                expect(scope.getDisbursementDate.calls.argsFor(0)[1]).toEqual(5);
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][0]).toBeUndefined();
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][1].toString()).toBe(mockDate4.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][2].toString()).toBe(mockDate5.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][3].toString()).toBe(mockDate6.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][4].toString()).toBe(mockDate7.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][5].toString()).toBe(mockDate1.toString());
            });
        });

        describe('getDashboard function Day of the Week = 5', function() {
            beforeEach(function() {
                var disbursementDeferred,
                    $promise;
                

                disbursementDeferred = $q.defer();
                disbursementDeferred.resolve({"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"});
               
                spyOn(disbursementsService, 'get').and.returnValue({$promise: disbursementDeferred.promise}); //returns a fake promise;
            });

            it('should call scope.getDisbursementDate with the correct Params', function() {
                spyOn(scope,'getDisbursementDate').and.callThrough();
                var result;
                var mockDate = new Date(2016,4,13,7,0,0); // Friday 7:00 am
                var mockDate1 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate2 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate3 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate4 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate5 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate6 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate7 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                
                jasmine.clock().mockDate(mockDate1);
                result = scope.calculateDisbursementDate(25);
                scope.$digest();
                
                expect(result).toEqual({then:jasmine.any(Function), catch:jasmine.any(Function), finally:jasmine.any(Function)});
                expect(scope.getDisbursementDate.calls.argsFor(0)[0]).toEqual({ id: 18, Boarding_ID: 25, Weekly: false, Monday: true, Tuesday: false, Wednesday: true, Thursday: false, Friday: true, recurweekly: 1, daymonth: false, datemonth: null, everydate: null, dayorder: null, dayname: null, everymonth: null, Hold: 0, createdAt: '2015-04-21T21:00:28.000Z', updatedAt: '2015-04-21T21:00:28.000Z' });
                expect(scope.getDisbursementDate.calls.argsFor(0)[1]).toEqual(6);
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][0]).toBeUndefined();
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][1].toString()).toBe(mockDate3.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][2].toString()).toBe(mockDate4.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][3].toString()).toBe(mockDate5.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][4].toString()).toBe(mockDate6.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][5].toString()).toBe(mockDate7.toString());
            });
        });

        describe('getDashboard function Day of the Week = 6', function() {
            beforeEach(function() {
                var disbursementDeferred,
                    $promise;
                

                disbursementDeferred = $q.defer();
                disbursementDeferred.resolve({"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"});
               
                spyOn(disbursementsService, 'get').and.returnValue({$promise: disbursementDeferred.promise}); //returns a fake promise;
            });

            it('should call scope.getDisbursementDate with the correct Params', function() {
                spyOn(scope,'getDisbursementDate').and.callThrough();
                var result;
                var mockDate = new Date(2016,4,14,7,0,0); // Saturday 7:00 am
                var mockDate1 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate2 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate3 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate4 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate5 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate6 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                var mockDate7 = new Date(mockDate.setDate(mockDate.getDate() + 1));
                
                jasmine.clock().mockDate(mockDate1);
                result = scope.calculateDisbursementDate(25);
                scope.$digest();
                
                expect(result).toEqual({then:jasmine.any(Function), catch:jasmine.any(Function), finally:jasmine.any(Function)});
                expect(scope.getDisbursementDate.calls.argsFor(0)[0]).toEqual({ id: 18, Boarding_ID: 25, Weekly: false, Monday: true, Tuesday: false, Wednesday: true, Thursday: false, Friday: true, recurweekly: 1, daymonth: false, datemonth: null, everydate: null, dayorder: null, dayname: null, everymonth: null, Hold: 0, createdAt: '2015-04-21T21:00:28.000Z', updatedAt: '2015-04-21T21:00:28.000Z' });
                expect(scope.getDisbursementDate.calls.argsFor(0)[1]).toEqual(0);
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][0]).toBeUndefined();
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][1].toString()).toBe(mockDate2.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][2].toString()).toBe(mockDate3.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][3].toString()).toBe(mockDate4.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][4].toString()).toBe(mockDate5.toString());
                expect(scope.getDisbursementDate.calls.argsFor(0)[2][5].toString()).toBe(mockDate6.toString());
            });
        });

    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController getDashboardName no errors', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _Authentication_, _$q_, _PaymentService_, _Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('getDashboardName', function() {
            beforeEach(function() {
                var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});

                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({"id":25,"Date":"2015-04-21","Merchant_Name":"Demo Account","Address":"123 Demo Street","City":"Demoville","Phone":"555-555-5555","Merchant_ID":"1","Bank_ID":"1","Terminal_ID":"1","MCC":"8396","Classification":"0","Visa":true,"Mastercard":true,"Discover":true,"American_Express":true,"Diners_Club":null,"JCB":null,"Maestro":null,"Max_Ticket_Amount":1500,"Max_Monthly_Amount":100,"Precheck":"nopreauth","Duplicate_Checking":null,"Allow_Merchant":null,"Duplicate_Threshold":1200,"Account_Description":null,"createdAt":"2015-04-21T20:58:50.000Z","updatedAt":"2015-11-13T18:21:40.000Z","Location_Number":"1","Aquire_Bin":"1","Store_Number":"1","Vital_Number":"1","Agent_Chain":"1","Required_Name":true,"Required_Company":null,"Required_Address":null,"Required_City":true,"Required_State":true,"Required_Zip":false,"Required_Country":null,"Required_Phone":null,"Required_Email":null,"Reserve_Rate":0,"Reserve_Condition":false,"Cap_Amount":null,"Disbursement":null,"Status":"active","Welcome_Email":true,"State":"CA","Zip":"92656"});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;                
            });

            it('should call PaymentService.get', function() {
                scope.getDashboardName();
                scope.$digest();
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 2 });
            });
            it('should call Nmitransactions.get', function() {
                scope.getDashboardName();
                scope.$digest();
                expect(Nmitransactions.get).toHaveBeenCalled();
            });
            it('should set scope.dashboardMerchant_name', function() {
                expect(scope.dashboardMerchant_name).toBeUndefined();
                scope.getDashboardName();
                scope.$digest();
                expect(scope.dashboardMerchant_name).toBe('Demo Account');
            });
            it('should set scope.dashboardMerchant_status', function() {
                expect(scope.dashboardMerchant_status).toBeUndefined();
                scope.getDashboardName();
                scope.$digest();
                expect(scope.dashboardMerchant_status).toBe('active');
            });
            it('should check if typeof scope.Product_ID === object', function() {
                scope.Product_ID = {ProductId:4};
                expect(scope.dashboardMerchant_status).toBeUndefined();
                scope.getDashboardName();
                scope.$digest();
                expect(scope.dashboardMerchant_status).toBe('active');
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController getDashboardName PaymentService.get Error', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _Authentication_, _$q_, _PaymentService_, _Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('getDashboardName PaymentService.get Error', function() {
            beforeEach(function() {
                var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.reject('This is an error message');

                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({"id":25,"Date":"2015-04-21","Merchant_Name":"Demo Account","Address":"123 Demo Street","City":"Demoville","Phone":"555-555-5555","Merchant_ID":"1","Bank_ID":"1","Terminal_ID":"1","MCC":"8396","Classification":"0","Visa":true,"Mastercard":true,"Discover":true,"American_Express":true,"Diners_Club":null,"JCB":null,"Maestro":null,"Max_Ticket_Amount":1500,"Max_Monthly_Amount":100,"Precheck":"nopreauth","Duplicate_Checking":null,"Allow_Merchant":null,"Duplicate_Threshold":1200,"Account_Description":null,"createdAt":"2015-04-21T20:58:50.000Z","updatedAt":"2015-11-13T18:21:40.000Z","Location_Number":"1","Aquire_Bin":"1","Store_Number":"1","Vital_Number":"1","Agent_Chain":"1","Required_Name":true,"Required_Company":null,"Required_Address":null,"Required_City":true,"Required_State":true,"Required_Zip":false,"Required_Country":null,"Required_Phone":null,"Required_Email":null,"Reserve_Rate":0,"Reserve_Condition":false,"Cap_Amount":null,"Disbursement":null,"Status":"active","Welcome_Email":true,"State":"CA","Zip":"92656"});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;                
            });

            it('should call PaymentService.get', function() {
                scope.getDashboardName();
                scope.$digest();
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 2 });
            });
            it('should call Nmitransactions.get', function() {
                scope.getDashboardName();
                scope.$digest();
                expect(Nmitransactions.get).not.toHaveBeenCalled();
            });
            it('should not set scope.dashboardMerchant_name', function() {
                expect(scope.dashboardMerchant_name).toBeUndefined();
                scope.getDashboardName();
                scope.$digest();
                expect(scope.dashboardMerchant_name).toBeUndefined();
                 expect(scope.dashboardMerchant_name).not.toBe('Demo Account');
            });
            it('should not set scope.dashboardMerchant_status', function() {
                expect(scope.dashboardMerchant_status).toBeUndefined();
                scope.getDashboardName();
                scope.$digest();
                expect(scope.dashboardMerchant_status).toBeUndefined();
                expect(scope.dashboardMerchant_status).not.toBe('active');
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController getDashboardName Nmitransactions.get Error', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            PaymentService,
            Nmitransactions;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _Authentication_, _$q_, _PaymentService_, _Nmitransactions_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('getDashboardName Nmitransactions.get Error', function() {
            beforeEach(function() {
                var paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});

                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.reject('This is an error message');
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;                
            });

            it('should call PaymentService.get', function() {
                scope.getDashboardName();
                scope.$digest();
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 2 });
            });
            it('should call Nmitransactions.get', function() {
                scope.getDashboardName();
                scope.$digest();
                expect(Nmitransactions.get).toHaveBeenCalled();
            });
            it('should not set scope.dashboardMerchant_name', function() {
                expect(scope.dashboardMerchant_name).toBeUndefined();
                scope.getDashboardName();
                scope.$digest();
                expect(scope.dashboardMerchant_name).toBeUndefined();
                 expect(scope.dashboardMerchant_name).not.toBe('Demo Account');
            });
            it('should not set scope.dashboardMerchant_status', function() {
                expect(scope.dashboardMerchant_status).toBeUndefined();
                scope.getDashboardName();
                scope.$digest();
                expect(scope.dashboardMerchant_status).toBeUndefined();
                expect(scope.dashboardMerchant_status).not.toBe('active');
            });
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('dashboardController getDisbursementDate', function() {
        // Initialize global variables
        var SaleController,
            scope,
            Authentication;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _Authentication_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('getDisbursementDate function', function() {
            
            it('should return proper disbursement dates if today is Sat, Sun, or Mon', function() {
                var schedule = {};
                var dayOfWeek = 0; // Sun: also works for Mon (1) and Sat (6)
                var dateArray = [];
                var returnedValue;
                
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                dateArray = [
                    undefined, 
                    'Mon Nov 16 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Tue Nov 17 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Wed Nov 18 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Thu Nov 19 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Fri Nov 13 2015 15:41:09 GMT-0800 (Pacific Standard Time)'];
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[1]);

                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":true,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[2]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[3]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":true,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[4]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[5]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toEqual(new Date());
            });
            it('should return proper disbursement dates if today is Tues', function() {
                var schedule = {};
                var dayOfWeek = 2; // Tuesday
                var dateArray = [];
                var returnedValue;
                
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                dateArray = [
                    undefined,
                    'Mon Nov 23 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Tue Nov 17 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Wed Nov 18 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Thu Nov 19 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Fri Nov 20 2015 15:41:09 GMT-0800 (Pacific Standard Time)'
                ];
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[1]);

                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":true,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[2]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[3]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":true,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[4]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[5]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue.toString()).toBe(new Date().toString());
            });
            it('should return proper disbursement dates if today is Wed', function() {
                var schedule = {};
                var dayOfWeek = 3; // Wednesday
                var dateArray = [];
                var returnedValue;
                
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                 dateArray = [
                    undefined,
                    'Mon Nov 23 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Tue Nov 24 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Wed Nov 18 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Thu Nov 19 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Fri Nov 20 2015 15:41:09 GMT-0800 (Pacific Standard Time)'
                ];
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue.toString()).toBe(dateArray[1].toString());

                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":true,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[2]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[3]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":true,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[4]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[5]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue.toString()).toEqual(new Date().toString());
            });
            it('should return proper disbursement dates if today is Thursday', function() {
                var schedule = {};
                var dayOfWeek = 4; // Thursday
                var dateArray = [];
                var returnedValue;
                
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                 dateArray = [
                    undefined,
                    'Mon Nov 23 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Tue Nov 24 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Wed Nov 25 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Thu Nov 19 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Fri Nov 20 2015 15:41:09 GMT-0800 (Pacific Standard Time)'
                ];
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[1]);

                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":true,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[2]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[3]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":true,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[4]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[5]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue.toString()).toEqual(new Date().toString());
            });
           it('should return proper disbursement dates if today is Friday', function() {
                var schedule = {};
                var dayOfWeek = 5; // Friday
                var dateArray = [];
                var returnedValue;
                
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":true,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                 dateArray = [
                    undefined,
                    'Mon Nov 23 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Tue Nov 24 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Wed Nov 25 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Thu Nov 26 2015 15:41:09 GMT-0800 (Pacific Standard Time)', 
                    'Fri Nov 20 2015 15:41:09 GMT-0800 (Pacific Standard Time)'
                ];
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[1]);

                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":true,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[2]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":true,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[3]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":true,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[4]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":true,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue).toBe(dateArray[5]);
 
                schedule = {"id":18,"Boarding_ID":25,"Weekly":false,"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"recurweekly":1,"daymonth":false,"datemonth":null,"everydate":null,"dayorder":null,"dayname":null,"everymonth":null,"Hold":0,"createdAt":"2015-04-21T21:00:28.000Z","updatedAt":"2015-04-21T21:00:28.000Z"};
                returnedValue = scope.getDisbursementDate(schedule, dayOfWeek, dateArray);
                scope.$digest();
                expect(returnedValue.toString()).toEqual(new Date().toString());
            });
        });
    });
}());


