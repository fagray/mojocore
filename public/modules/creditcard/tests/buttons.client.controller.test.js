/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('ButtonController', function() {
        // Initialize global variables
        var $q,
            ButtonController,
            scope,
            $httpBackend,
            $location,
            $modal,
            $stateParams,
            Authentication,
            ProductButtonService,
            DonationButtonService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope,_$httpBackend_,_$location_, _$modal_,_$stateParams_,_Authentication_,_ProductButtonService_,_DonationButtonService_, _$q_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $httpBackend = _$httpBackend_;
            $location = _$location_;
            $modal = _$modal_;
            $stateParams = _$stateParams_;
            $q = _$q_;
            
            ProductButtonService = _ProductButtonService_;
            DonationButtonService = _DonationButtonService_;
            
            // Initialize the controller
            ButtonController = $controller('ButtonController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });
            it('should have variable scope.param defined', function() {
                expect(scope.param).toBeDefined();
            });
            it('should have variable scope.button_page_1 defined', function() {
                expect(scope.button_page_1).toBeDefined();
            });
            it('should have variable scope.button_page_2 defined', function() {
                expect(scope.button_page_2).toBeDefined();
            });
            it('should have variable scope.templateurl defined', function() {
                expect(scope.templateurl).toBeDefined();
            });
            it('should have variable scope.Donation_Currency_Default defined', function() {
                expect(scope.Donation_Currency_Default).toBeDefined();
            });
            it('should have variable scope.Product_Currency_Default defined', function() {
                expect(scope.Product_Currency_Default).toBeDefined();
            });
            it('should have variable scope.Donation_Amount_1_Default defined', function() {
                expect(scope.Donation_Amount_1_Default).toBeDefined();
            });
            it('should have variable scope.Donation_Amount_2_Default defined', function() {
                expect(scope.Donation_Amount_2_Default).toBeDefined();
            });
            it('should have variable scope.Donation_Amount_3_Default defined', function() {
                expect(scope.Donation_Amount_3_Default).toBeDefined();
            });
            it('should have variable scope.Donation_Amount_4_Default defined', function() {
                expect(scope.Donation_Amount_4_Default).toBeDefined();
            });
            it('should have variable scope.pButton_Type_default defined', function() {
                expect(scope.pButton_Type_default).toBeDefined();
            });
            it('should have variable scope.dButton_Type_default defined', function() {
                expect(scope.dButton_Type_default).toBeDefined();
            });
            it('should have variable scope.donation_error defined', function() {
                expect(scope.donation_error).toBeDefined();
            });
            it('should have variable scope.product_error defined', function() {
                expect(scope.product_error).toBeDefined();
            });
            
            it('should have function scope.initPaginator defined', function() {
                expect(scope.initPaginator).toBeDefined();
            });
            it('should have function scope.validDonationForm defined', function() {
                expect(scope.validDonationForm).toBeDefined();
            });
            it('should have function scope.validProductForm defined', function() {
                expect(scope.validProductForm).toBeDefined();
            });
            it('should have function scope.setTitle defined', function() {
                expect(scope.setTitle).toBeDefined();
            });
            it('should have function scope.button_init defined', function() {
                expect(scope.button_init).toBeDefined();
            });
            it('should have function scope.button_edit_init defined', function() {
                expect(scope.button_edit_init).toBeDefined();
            });
            it('should have function scope.button_next defined', function() {
                expect(scope.button_next).toBeDefined();
            });
            it('should have function scope.confirmationModal defined', function() {
                expect(scope.confirmationModal).toBeDefined();
            });
            it('should have function scope.displayConfirmation defined', function() {
                expect(scope.displayConfirmation).toBeDefined();
            });
            it('should have function scope.verifyDelete defined', function() {
                expect(scope.verifyDelete).toBeDefined();
            });
            it('should have function scope.doCreate defined', function() {
                expect(scope.doCreate).toBeDefined();
            });
            it('should have function scope.doEdit defined', function() {
                expect(scope.doEdit).toBeDefined();
            });
            it('should have function scope.buttonWrap defined', function() {
                expect(scope.buttonWrap).toBeDefined();
            });
         });

        describe('Interface', function() {
            it('should recognize initPaginator as a function', function() {
                expect(angular.isFunction(scope.initPaginator)).toBe(true);
            });
            it('should recognize validDonationForm as a function', function() {
                expect(angular.isFunction(scope.validDonationForm)).toBe(true);
            });
            it('should recognize validProductForm as a function', function() {
                expect(angular.isFunction(scope.validProductForm)).toBe(true);
            });
            it('should recognize setTitle as a function', function() {
                expect(angular.isFunction(scope.setTitle)).toBe(true);
            });
            it('should recognize button_init as a function', function() {
                expect(angular.isFunction(scope.button_init)).toBe(true);
            });
            it('should recognize button_edit_init as a function', function() {
                expect(angular.isFunction(scope.button_edit_init)).toBe(true);
            });
            it('should recognize button_next as a function', function() {
                expect(angular.isFunction(scope.button_next)).toBe(true);
            });
            it('should recognize confirmationModal as a function', function() {
                expect(angular.isFunction(scope.confirmationModal)).toBe(true);
            });
            it('should recognize displayConfirmation as a function', function() {
                expect(angular.isFunction(scope.displayConfirmation)).toBe(true);
            });
            it('should recognize verifyDelete as a function', function() {
                expect(angular.isFunction(scope.verifyDelete)).toBe(true);
            });
            it('should recognize doCreate as a function', function() {
                expect(angular.isFunction(scope.doCreate)).toBe(true);
            });
            it('should recognize doEdit as a function', function() {
                expect(angular.isFunction(scope.doEdit)).toBe(true);
            });
            it('should recognize buttonWrap as a function', function() {
                expect(angular.isFunction(scope.buttonWrap)).toBe(true);
            });
        });
        
        describe('Initial Values', function() {
            it('should have variable scope.button_page_1 defined', function() {
                expect(scope.button_page_1).toBe('modules/creditcard/views/mProductCreate.client.view.html');
            });
            it('should have variable scope.button_page_2 defined', function() {
                expect(scope.button_page_2).toBe('modules/creditcard/views/mDonationCreate.client.view.html');
            });
            it('should have variable scope.templateurl defined', function() {
                expect(scope.templateurl).toBe('');
            });
            it('should have variable scope.Donation_Currency_Default defined', function() {
                expect(scope.Donation_Currency_Default).toBe('USD');
            });
            it('should have variable scope.Donation_Currency_Default defined', function() {
                expect(scope.Product_Currency_Default).toBe('USD');
            });
            it('should have variable scope.Donation_Amount_1_Default defined', function() {
                expect(scope.Donation_Amount_1_Default).toBe(5);
            });
            it('should have variable scope.Donation_Amount_2_Default defined', function() {
                expect(scope.Donation_Amount_2_Default).toBe(10);
            });
            it('should have variable scope.Donation_Amount_3_Default defined', function() {
                expect(scope.Donation_Amount_3_Default).toBe(25);
            });
            it('should have variable scope.Donation_Amount_4_Default defined', function() {
                expect(scope.Donation_Amount_4_Default).toBe(50);
            });
            it('should have variable scope.Allow_Custom_Donations_default defined', function() {
                expect(scope.Allow_Custom_Donations_default).toBe(true);
            });
            it('should have variable scope.pButton_Type_default defined', function() {
                expect(scope.pButton_Type_default).toBe('buynow150');
            });
            it('should have variable scope.dButton_Type_default defined', function() {
                expect(scope.dButton_Type_default).toBe('donate150');
            });
            it('should have variable scope.donation_error defined', function() {
                expect(scope.donation_error).toBe('');
            });
            it('should have variable scope.product_error defined', function() {
                expect(scope.product_error).toBe('');
            });
        });
        
        describe('initPaginator function', function() {
            it('should initialize scope.itemsPerPage', function() {
                expect(scope.itemsPerPage).toBeUndefined();
                scope.initPaginator();
                expect(scope.itemsPerPage).toBe(25);
            });
            it('should initialize scope.currentPage', function() {
                expect(scope.currentPage).toBeUndefined();
                scope.initPaginator();
                expect(scope.currentPage).toBe(0);
            });
        });
        
        describe('validDonationForm function', function() {
            it('should set scope.donation_error if scope.Donation_Name is blank, null, or undefined', function() {
                scope.Donation_Name = '';
                scope.Donation_Amount_1 = 5;
                scope.Donation_Amount_2 = 10;
                scope.Donation_Amount_3 = 25;
                scope.Donation_Amount_4 = 50;
                
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Please Enter a name for this donation');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Name = null;
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Please Enter a name for this donation');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Name = undefined;
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Please Enter a name for this donation');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Name = 'donation';
                scope.validDonationForm();
                expect(scope.donation_error).not.toBe('Please Enter a name for this donation');
                expect(scope.validDonationForm()).toBe(true);
            });
            it('should set scope.donation_error if scope.Donation_Amount_1 is not a number', function() {
                scope.Donation_Name = 'donation';
                scope.Donation_Amount_1 = NaN;
                scope.Donation_Amount_2 = 10;
                scope.Donation_Amount_3 = 25;
                scope.Donation_Amount_4 = 50;
                
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 1 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_1 = 'five';
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 1 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_1 = undefined;
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 1 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_1 = 5;
                scope.validDonationForm();
                expect(scope.donation_error).not.toBe('Donation Amount 1 is not a number');
                expect(scope.validDonationForm()).toBe(true);
            });
            it('should set scope.donation_error if scope.Donation_Amount_2 is not a number', function() {
                scope.Donation_Name = 'donation';
                scope.Donation_Amount_1 = 5;
                scope.Donation_Amount_2 = NaN;
                scope.Donation_Amount_3 = 25;
                scope.Donation_Amount_4 = 50;
                
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 2 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_2 = 'ten';
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 2 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_2 = undefined;
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 2 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_2 = 10;
                scope.validDonationForm();
                expect(scope.donation_error).not.toBe('Donation Amount 2 is not a number');
                expect(scope.validDonationForm()).toBe(true);
            });
            it('should set scope.donation_error if scope.Donation_Amount_3 is not a number', function() {
                scope.Donation_Name = 'donation';
                scope.Donation_Amount_1 = 5;
                scope.Donation_Amount_2 = 10;
                scope.Donation_Amount_3 = NaN;
                scope.Donation_Amount_4 = 50;
                
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 3 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_3 = 'twenty-five';
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 3 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_3 = undefined;
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 3 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_3 = 25;
                scope.validDonationForm();
                expect(scope.donation_error).not.toBe('Donation Amount 3 is not a number');
                expect(scope.validDonationForm()).toBe(true);
            });
            it('should set scope.donation_error if scope.Donation_Amount_3 is not a number', function() {
                scope.Donation_Name = 'donation';
                scope.Donation_Amount_1 = 5;
                scope.Donation_Amount_2 = 10;
                scope.Donation_Amount_3 = 25;
                scope.Donation_Amount_4 = NaN;
                
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 4 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_4 = 'fifty';
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 4 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_4 = undefined;
                scope.validDonationForm();
                expect(scope.donation_error).toBe('Donation Amount 4 is not a number');
                expect(scope.validDonationForm()).toBe(false);
                scope.Donation_Amount_4 = 50;
                scope.validDonationForm();
                expect(scope.donation_error).not.toBe('Donation Amount 4 is not a number');
                expect(scope.validDonationForm()).toBe(true);
            });
            it('should not set scope.donation_error if valid form values', function() {
                scope.Donation_Name = 'donation';
                scope.Donation_Amount_1 = 5;
                scope.Donation_Amount_2 = 10;
                scope.Donation_Amount_3 = 25;
                scope.Donation_Amount_4 = 50;
                
                scope.validDonationForm();
                expect(scope.donation_error).toBe('');
                expect(scope.validDonationForm()).toBe(true);
            });
        });
        
        describe('validProductForm function', function() {
             it('should set scope.donation_error if scope.Product_Name is blank, null, or undefined', function() {
                scope.Product_Name = '';
                scope.Product_Price = 9.99;
                scope.Tax = 1.11;
                scope.Shipping = 2.22;
                
                scope.validProductForm();
                expect(scope.product_error).toBe('Please Enter a name for this product');
                expect(scope.validProductForm()).toBe(false);
                scope.Product_Name = null;
                scope.validProductForm();
                expect(scope.product_error).toBe('Please Enter a name for this product');
                expect(scope.validProductForm()).toBe(false);
                scope.Product_Name = undefined;
                scope.validProductForm();
                expect(scope.product_error).toBe('Please Enter a name for this product');
                expect(scope.validProductForm()).toBe(false);
                scope.Product_Name = 'product';
                scope.validProductForm();
                expect(scope.product_error).not.toBe('Please Enter a name for this product');
                expect(scope.validProductForm()).toBe(true);
            });
            it('should set scope.donation_error if scope.Product_Price is not a number', function() {
                scope.Product_Name = 'product';
                scope.Product_Price = NaN;
                scope.Tax = 1.11;
                scope.Shipping = 2.22;
                
                scope.validProductForm();
                expect(scope.product_error).toBe('Product Price is not a number');
                expect(scope.validProductForm()).toBe(false);
                scope.Product_Price = 'nine point nine nine';
                scope.validProductForm();
                expect(scope.product_error).toBe('Product Price is not a number');
                expect(scope.validProductForm()).toBe(false);
                scope.Product_Price = undefined;
                scope.validProductForm();
                expect(scope.product_error).toBe('Product Price is not a number');
                expect(scope.validProductForm()).toBe(false);
                scope.Product_Price = 9.99;
                scope.validProductForm();
                expect(scope.product_error).not.toBe('Product Price is not a number');
                expect(scope.validProductForm()).toBe(true);
            });
            it('should set scope.donation_error if scope.Tax is not a number (and it is not blank, null, or undefined', function() {
                scope.Product_Name = 'product';
                scope.Product_Price = 9.99;
                scope.Tax = NaN;
                scope.Shipping = 2.22;
                
                scope.validProductForm();
                expect(scope.product_error).toBe('Tax is not a number');
                expect(scope.validProductForm()).toBe(false);
                scope.Tax = 'one point one one';
                scope.validProductForm();
                expect(scope.product_error).toBe('Tax is not a number');
                expect(scope.validProductForm()).toBe(false);
                scope.Tax = '';
                scope.validProductForm();
                expect(scope.product_error).not.toBe('Tax is not a number');
                expect(scope.validProductForm()).toBe(true);
                scope.Tax = null;
                scope.validProductForm();
                expect(scope.product_error).not.toBe('Tax is not a number');
                expect(scope.validProductForm()).toBe(true);
                scope.Tax = undefined;
                scope.validProductForm();
                expect(scope.product_error).not.toBe('Tax is not a number');
                expect(scope.validProductForm()).toBe(true);
                scope.Tax = 1.11;
                scope.validProductForm();
                expect(scope.product_error).not.toBe('Tax is not a number');
                expect(scope.validProductForm()).toBe(true);
            });
            it('should set scope.donation_error if scope.Shipping is not a number (and it is not blank, null, or undefined', function() {
                scope.Product_Name = 'product';
                scope.Product_Price = 9.99;
                scope.Tax = 1.11;
                scope.Shipping = NaN;
                
                scope.validProductForm();
                expect(scope.product_error).toBe('Shipping is not a number');
                expect(scope.validProductForm()).toBe(false);
                scope.Shipping = 'two point two two';
                scope.validProductForm();
                expect(scope.product_error).toBe('Shipping is not a number');
                expect(scope.validProductForm()).toBe(false);
                scope.Shipping = '';
                scope.validProductForm();
                expect(scope.product_error).not.toBe('Shipping is not a number');
                expect(scope.validProductForm()).toBe(true);
                scope.Shipping = null;
                scope.validProductForm();
                expect(scope.product_error).not.toBe('Shipping is not a number');
                expect(scope.validProductForm()).toBe(true);
                scope.Shipping = undefined;
                scope.validProductForm();
                expect(scope.product_error).not.toBe('Shipping is not a number');
                expect(scope.validProductForm()).toBe(true);
                scope.Shipping = 2.22;
                scope.validProductForm();
                expect(scope.product_error).not.toBe('Shipping is not a number');
                expect(scope.validProductForm()).toBe(true);
            });
            it('should not et scope.donation_error if form is valid', function() {
                scope.Product_Name = 'product';
                scope.Product_Price = 9.99;
                scope.Tax = 1.11;
                scope.Shipping = 2.22;
                
                scope.validProductForm();
                expect(scope.product_error).toBe('');
                expect(scope.validProductForm()).toBe(true);
            });
        });

        describe('setTitle function', function() {
            it('should call $broadcast on $scope.$parent.$parent',function() {
                var value = 'Foxtrot Oscar Oscar';
                var parent = scope.$parent.$parent;
                spyOn(parent, '$broadcast').and.callThrough();
                scope.setTitle(value);            
                expect(parent.$broadcast).toHaveBeenCalledWith('setTitle', value);
                expect(parent.$broadcast.calls.mostRecent().args[0]).toEqual('setTitle');
                expect(parent.$broadcast.calls.mostRecent().args[1]).toEqual(value);
            });
        });

        describe('button_init function', function() {
            it('should not initialize values if button is being not created',function() {
                scope.Donation_Currency = 'XYZ';
                scope.Donation_Amount_1 = -1;
                scope.Donation_Amount_2 = -2;
                scope.Donation_Amount_3 = -3;
                scope.Donation_Amount_4 = -4;
                scope.Allow_Custom_Donations = false;
                scope.dButtonType = 'donate150pxcc'; 
                scope.pButtonType = 'buynow150pxcc';
                
                $stateParams.boardingId = '25';
                scope.button_init();
                expect(scope.Donation_Currency).toBe('XYZ');
                expect(scope.Donation_Amount_1).toBe(-1);
                expect(scope.Donation_Amount_2).toBe(-2);
                expect(scope.Donation_Amount_3).toBe(-3);
                expect(scope.Donation_Amount_4).toBe(-4);
                expect(scope.Allow_Custom_Donations).toBe(false);
                expect(scope.dButtonType).toBe('donate150pxcc'); 
                expect(scope.pButtonType).toBe('buynow150pxcc');
            });
           it('should not initialize values if button is being not created',function() {
                scope.Donation_Currency = 'XYZ';
                scope.Donation_Amount_1 = -1;
                scope.Donation_Amount_2 = -2;
                scope.Donation_Amount_3 = -3;
                scope.Donation_Amount_4 = -4;
                scope.Allow_Custom_Donations = false;
                scope.dButtonType = 'donate150pxcc'; 
                scope.pButtonType = 'buynow150pxcc';
                
                $stateParams.boardingId = 'create';
                scope.button_init();
                expect(scope.Donation_Currency).toBe('USD');
                expect(scope.Donation_Amount_1).toBe(5);
                expect(scope.Donation_Amount_2).toBe(10);
                expect(scope.Donation_Amount_3).toBe(25);
                expect(scope.Donation_Amount_4).toBe(50);
                expect(scope.Allow_Custom_Donations).toBe(true);
                expect(scope.dButtonType).toBe('donate150'); 
                expect(scope.pButtonType).toBe('buynow150');
            });
        });

        describe('button_edit_init function', function() {
            var productButtonDeferred,
                donationButtonDeferred,
                $promise;
                    
            beforeEach(function() {
                productButtonDeferred = $q.defer();
                productButtonDeferred.resolve({response: [{
                    Product_Name: 'theProductName',
                    Product_Description: 'theProductDescription',
                    Product_SKU: 'theProductSKU',
                    Product_Currency: 'XYZ',
                    Product_Price: 99.99,
                    Tax: 8.88,
                    Shipping: 7.77,
                    Button_Size_Display: 'productSizeLARGE' 
                }]});
                
                donationButtonDeferred = $q.defer();
                donationButtonDeferred.resolve({response: [{
                    Donation_Name: 'theDonationName',
                    Donation_Description: 'theDonationDescription',
                    Donation_Currency: 'ABC',
                    Donation_Amount_1: 1.22,
                    Donation_Amount_2: 2.33,
                    Donation_Amount_3: 3.44,
                    Donation_Amount_4: 4.55,
                    Allow_Custom_Donations: false,
                    Button_Size_Display: 'DonationSizeSMALL' 
                }]});
                
                spyOn(ProductButtonService, 'get').and.returnValue({$promise: productButtonDeferred.promise});
                spyOn(DonationButtonService, 'get').and.returnValue({$promise: donationButtonDeferred.promise});
            });
            
            it('should genererate and error message if the first GET parameters is invalid', function() {
                spyOn(console,'log').and.callThrough();
                scope.param = ['', 'invalidParameter', ''];
                scope.button_edit_init();
                expect(console.log).toHaveBeenCalledWith('Error retrieving Button');
            });
            it('should call the ProductButtonService.get service if editing a product button', function() {
                scope.param = ['', 'productbuttonedit', '665'];
                scope.button_edit_init();
                scope.$digest();
                expect(ProductButtonService.get).toHaveBeenCalledWith({ boardingId: 25, id: '665', Is_Active: 1 });
            });
            it('should set scope.page to 1 for product edit', function() {
                scope.param = ['', 'productbuttonedit', '665'];
                scope.button_edit_init();
                scope.$digest();
                expect(scope.page).toBe(1);
            });
            it('should set form variables on product edit', function() {
                scope.Product_Name = 'junk';
                scope.Product_Description = 'garbage';
                scope.Product_SKU = 'trash';
                scope.Product_Currency = 'ABC';
                scope.Product_Price = 1.11;
                scope.Tax = 2.22;
                scope.Shipping = 3.33;
                scope.pButtonType = 'refuse'; 
                
                scope.param = ['', 'productbuttonedit', '665'];
                scope.button_edit_init();
                scope.$digest();
                expect(scope.Product_Name).toBe('theProductName');
                expect(scope.Product_Description).toBe('theProductDescription');
                expect(scope.Product_SKU).toBe('theProductSKU');
                expect(scope.Product_Currency).toBe('XYZ');
                expect(scope.Product_Price).toBe(99.99);
                expect(scope.Tax).toBe(8.88);
                expect(scope.Shipping).toBe(7.77);
                expect(scope.pButtonType).toBe('productSizeLARGE');
            });
            it('should call the DonationButtonService.get service if editing a donation button', function() {
                scope.param = ['', 'donationbuttonedit', '667'];
                scope.button_edit_init();
                scope.$digest();
                expect(DonationButtonService.get).toHaveBeenCalledWith({ boardingId: 25, id: '667' });
            });
            it('should set scope.page to 2 for donation edit', function() {
                scope.param = ['', 'donationbuttonedit', '667'];
                scope.button_edit_init();
                scope.$digest();
                expect(scope.page).toBe(2);
            });
            it('should set form variables for donation edit', function() {
                scope.Donation_Name = 'junk';
                scope.Donation_Description = 'garbage';
                scope.Donation_Currency = 'ABC';
                scope.Donation_Amount_1 = 5.66;
                scope.Donation_Amount_2 = 6.77;
                scope.Donation_Amount_3 = 7.88;
                scope.Donation_Amount_4 = 8.99;
                scope.Allow_Custom_Donations = true;
                scope.dButtonType = 'refuse'; 
                
                scope.param = ['', 'donationbuttonedit', '667'];
                scope.button_edit_init();
                scope.$digest();
                expect(scope.Donation_Name).toBe('theDonationName');
                expect(scope.Donation_Description).toBe('theDonationDescription');
                expect(scope.Donation_Currency).toBe('ABC');
                expect(scope.Donation_Amount_1).toBe(1.22);
                expect(scope.Donation_Amount_2).toBe(2.33);
                expect(scope.Donation_Amount_3).toBe(3.44);
                expect(scope.Donation_Amount_4).toBe(4.55);
                expect(scope.Allow_Custom_Donations).toBe(false);
                expect(scope.dButtonType).toBe('DonationSizeSMALL');
            }); 
        });

        describe('button_next function', function() {
            it('should set scope.page to 1', function() {
                scope.page = 9;
                scope.templateurl = '';
                scope.button_next(1);
                expect(scope.page).toBe(1);
                expect(scope.templateurl).toBe('modules/creditcard/views/mProductCreate.client.view.html');
            });
            it('should set scope.page to 2', function() {
                scope.page = 9;
                scope.templateurl = '';
                scope.button_next(2);
                expect(scope.page).toBe(2);
                expect(scope.templateurl).toBe('modules/creditcard/views/mDonationCreate.client.view.html');
            });
            it('should set scope.page to 1', function() {
                scope.page = 9;
                scope.templateurl = '';
                scope.button_next();
                expect(scope.page).toBe(1);
                expect(scope.templateurl).toBe('modules/creditcard/views/mProductCreate.client.view.html');
            });
            it('should set scope.templateurl to modules/creditcard/views/mProductCreate.client.view.html', function() {
                scope.templateurl = '';
                scope.button_next(1);
                expect(scope.templateurl).toBe('modules/creditcard/views/mProductCreate.client.view.html');
            });
            it('should set scope.templateurl to modules/creditcard/views/mDonationCreate.client.view.html', function() {
                scope.templateurl = '';
                scope.button_next(2);
                expect(scope.templateurl).toBe('modules/creditcard/views/mDonationCreate.client.view.html');
            });
            it('should set scope.templateurl to modules/creditcard/views/mProductCreate.client.view.html', function() {
                scope.templateurl = '';
                scope.button_next();
                expect(scope.templateurl).toBe('modules/creditcard/views/mProductCreate.client.view.html');
            });
        });

/*
        xdescribe('confirmationModal function', function() {
        });
*/

        describe('displayConfirmation function', function() {
            it('should call displayConfirmation with error parameters', function() {
                spyOn(scope,'confirmationModal').and.returnValue(true);
                scope.displayConfirmation('error', 'https://server.comm/?parameter', 'theButtonName');
                scope.$digest();
                expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Error Creating Button', message: 'Button Was Not Created', accept: '', deny: 'Cancel' });
            });
            it('should call displayConfirmation with success information', function() {
                spyOn(scope,'confirmationModal').and.returnValue(true);
                scope.displayConfirmation('okay', 'https://server.comm/?parameter', 'theButtonName');
                scope.$digest();
                expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Button Successfully Created', message: 'Button Created. Here is the embed code for theButtonName', message2: 'Please copy and paste into your site.', accept: '', deny: 'OK', code: 'https://server.comm/?parameter' });
            });
            it('should call displayConfirmation with information', function() {
                spyOn(scope,'confirmationModal').and.returnValue(true);
                scope.displayConfirmation('', 'https://server.comm/?parameter', 'theButtonName');
                scope.$digest();
                expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Embed Code', message: 'Here is the embed code for theButtonName', message2: 'Please copy and paste into your site.', code: 'https://server.comm/?parameter', accept: '', deny: 'OK' });
            });
        });

        describe('verifyDelete function', function() {
            it('should call displayConfirmation for delete confirmation', function() {
                spyOn(scope,'confirmationModal').and.returnValue(true);
                scope.verifyDelete(1024, 'deleting');
                scope.$digest();
                expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Delete Button', message: 'Do you wish to delete this button?', accept: 'Yes', deny: 'No', id: 1024, delete: true, type: 'deleting' });
            });
        });

        describe('doCreate function', function() {
            describe('doCreate I function (donation, button = donate150', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="dButtonType" value="donate150" ng_model="dButtonType" checked/><input type="radio" name="dButtonType" value="donate150cc" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250cc" ng_model="dButtonType" /><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should post to /merchant_donation with the appropriate variables', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    scope.page = 2;
                    scope.Donation_Name = 'theDonationName';
                    scope.Donation_Description = 'theDonationDescription';
                    scope.Donation_Currency ='ABC';
                    scope.Donation_Amount_1 = 1.22;
                    scope.Donation_Amount_2 = 2.33;
                    scope.Donation_Amount_3 = 3.44;
                    scope.Donation_Amount_4 = 4.55;
                    scope.Allow_Custom_Donations = false;
                    scope.Button_Size_Display = 'DonationSizeSMALL'; 
    
                    $httpBackend.when('POST', '/merchant_donation', {
                        Donation_Name: 'theDonationName', 
                        Donation_Description: 'theDonationDescription', 
                        Donation_Amount_1: 1.22, 
                        Donation_Amount_2: 2.33, 
                        Donation_Amount_3: 3.44, 
                        Donation_Amount_4: 4.55, 
                        Allow_Custom_Donations: false, 
                        Button_Size_Display: 'donate150', 
                        Donation_Currency: 'ABC', 
                        Boarding_ID: 25
                    }).respond(200, {Donation_Name: 'theDonationName', Hash: '0A1B2C3D4E5F6G7H8I9',Button_Size_Display: 'donate150'});
                    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                });
    
                it('should call $scope.displayConfirmation with the appropriate message', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    scope.page = 2;
                    scope.Donation_Name = 'theDonationName';
                    scope.Donation_Description = 'theDonationDescription';
                    scope.Donation_Currency ='ABC';
                    scope.Donation_Amount_1 = 1.22;
                    scope.Donation_Amount_2 = 2.33;
                    scope.Donation_Amount_3 = 3.44;
                    scope.Donation_Amount_4 = 4.55;
                    scope.Allow_Custom_Donations = false;
                    scope.Button_Size_Display = 'DonationSizeSMALL'; 
    
                    $httpBackend.when('POST', '/merchant_donation', {
                        Donation_Name: 'theDonationName', 
                        Donation_Description: 'theDonationDescription', 
                        Donation_Amount_1: 1.22, 
                        Donation_Amount_2: 2.33, 
                        Donation_Amount_3: 3.44, 
                        Donation_Amount_4: 4.55, 
                        Allow_Custom_Donations: false, 
                        Button_Size_Display: 'donate150', 
                        Donation_Currency: 'ABC', 
                        Boarding_ID: 25
                    }).respond(200, {Donation_Name: 'theDonationName', Hash: '0A1B2C3D4E5F6G7H8I9',Button_Size_Display: 'donate150'});
                    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.displayConfirmation).toHaveBeenCalledWith('okay', '<a href="http://secure.mojopay.com:80/#!/buttonapi/donationbutton?token=0A1B2C3D4E5F6G7H8I9" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/donate150px.png" style="border:none;"></a>', 'theDonationName');
                });
            });
    
            describe('doCreate II function (donation, button = donate150cc', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="dButtonType" value="donate150" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate150cc" ng_model="dButtonType" checked /><input type="radio" name="dButtonType" value="donate250" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250cc" ng_model="dButtonType" /><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should call $scope.displayConfirmation with the appropriate message', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    scope.page = 2;
                    scope.Donation_Name = 'theDonationName';
                    scope.Donation_Description = 'theDonationDescription';
                    scope.Donation_Currency ='ABC';
                    scope.Donation_Amount_1 = 1.22;
                    scope.Donation_Amount_2 = 2.33;
                    scope.Donation_Amount_3 = 3.44;
                    scope.Donation_Amount_4 = 4.55;
                    scope.Allow_Custom_Donations = false;
                    scope.Button_Size_Display = 'DonationSizeSMALL'; 
    
                    $httpBackend.when('POST', '/merchant_donation', {
                        Donation_Name: 'theDonationName', 
                        Donation_Description: 'theDonationDescription', 
                        Donation_Amount_1: 1.22, 
                        Donation_Amount_2: 2.33, 
                        Donation_Amount_3: 3.44, 
                        Donation_Amount_4: 4.55, 
                        Allow_Custom_Donations: false, 
                        Button_Size_Display: 'donate150cc', 
                        Donation_Currency: 'ABC', 
                        Boarding_ID: 25
                    }).respond(200, {Donation_Name: 'theDonationName', Hash: '0A1B2C3D4E5F6G7H8I9',Button_Size_Display: 'donate150cc'});
                    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.displayConfirmation).toHaveBeenCalledWith('okay', '<a href="http://secure.mojopay.com:80/#!/buttonapi/donationbutton?token=0A1B2C3D4E5F6G7H8I9" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/donate150pxcc.png" style="border:none;"></a>', 'theDonationName');
                });
            });
    
            describe('doCreate III function (donation, button = donate250', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="dButtonType" value="donate150" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate150cc" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250" ng_model="dButtonType" checked /><input type="radio" name="dButtonType" value="donate250cc" ng_model="dButtonType" /><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should call $scope.displayConfirmation with the appropriate message', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    scope.page = 2;
                    scope.Donation_Name = 'theDonationName';
                    scope.Donation_Description = 'theDonationDescription';
                    scope.Donation_Currency ='ABC';
                    scope.Donation_Amount_1 = 1.22;
                    scope.Donation_Amount_2 = 2.33;
                    scope.Donation_Amount_3 = 3.44;
                    scope.Donation_Amount_4 = 4.55;
                    scope.Allow_Custom_Donations = false;
                    scope.Button_Size_Display = 'DonationSizeSMALL'; 
    
                    $httpBackend.when('POST', '/merchant_donation', {
                        Donation_Name: 'theDonationName', 
                        Donation_Description: 'theDonationDescription', 
                        Donation_Amount_1: 1.22, 
                        Donation_Amount_2: 2.33, 
                        Donation_Amount_3: 3.44, 
                        Donation_Amount_4: 4.55, 
                        Allow_Custom_Donations: false, 
                        Button_Size_Display: 'donate250', 
                        Donation_Currency: 'ABC', 
                        Boarding_ID: 25
                    }).respond(200, {Donation_Name: 'theDonationName', Hash: '0A1B2C3D4E5F6G7H8I9',Button_Size_Display: 'donate250'});
                    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.displayConfirmation).toHaveBeenCalledWith('okay', '<a href="http://secure.mojopay.com:80/#!/buttonapi/donationbutton?token=0A1B2C3D4E5F6G7H8I9" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/donate250px.png" style="border:none;"></a>', 'theDonationName');
                });
            });
            
            describe('doCreate IV function (donation, button = donate250cc', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="dButtonType" value="donate150" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate150cc" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250cc" ng_model="dButtonType" checked/><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should call $scope.displayConfirmation with the appropriate message', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    scope.page = 2;
                    scope.Donation_Name = 'theDonationName';
                    scope.Donation_Description = 'theDonationDescription';
                    scope.Donation_Currency ='ABC';
                    scope.Donation_Amount_1 = 1.22;
                    scope.Donation_Amount_2 = 2.33;
                    scope.Donation_Amount_3 = 3.44;
                    scope.Donation_Amount_4 = 4.55;
                    scope.Allow_Custom_Donations = false;
                    scope.Button_Size_Display = 'DonationSizeSMALL'; 
    
                    $httpBackend.when('POST', '/merchant_donation', {
                        Donation_Name: 'theDonationName', 
                        Donation_Description: 'theDonationDescription', 
                        Donation_Amount_1: 1.22, 
                        Donation_Amount_2: 2.33, 
                        Donation_Amount_3: 3.44, 
                        Donation_Amount_4: 4.55, 
                        Allow_Custom_Donations: false, 
                        Button_Size_Display: 'donate250cc', 
                        Donation_Currency: 'ABC', 
                        Boarding_ID: 25
                    }).respond(200, {Donation_Name: 'theDonationName', Hash: '0A1B2C3D4E5F6G7H8I9',Button_Size_Display: 'donate250cc'});
                    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.displayConfirmation).toHaveBeenCalledWith('okay', '<a href="http://secure.mojopay.com:80/#!/buttonapi/donationbutton?token=0A1B2C3D4E5F6G7H8I9" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/donate250pxcc.png" style="border:none;"></a>', 'theDonationName');
                });
            });
    
            describe('doCreate V function (donation, button = donate250cc error on /merchant_donation', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="dButtonType" value="donate150" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate150cc" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250cc" ng_model="dButtonType" checked/><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should call $scope.displayConfirmation with the appropriate message', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    spyOn(console,'log').and.callThrough();
                    scope.page = 2;
                    scope.Donation_Name = 'theDonationName';
                    scope.Donation_Description = 'theDonationDescription';
                    scope.Donation_Currency ='ABC';
                    scope.Donation_Amount_1 = 1.22;
                    scope.Donation_Amount_2 = 2.33;
                    scope.Donation_Amount_3 = 3.44;
                    scope.Donation_Amount_4 = 4.55;
                    scope.Allow_Custom_Donations = false;
                    scope.Button_Size_Display = 'DonationSizeSMALL'; 
    
                    $httpBackend.when('POST', '/merchant_donation', {
                        Donation_Name: 'theDonationName', 
                        Donation_Description: 'theDonationDescription', 
                        Donation_Amount_1: 1.22, 
                        Donation_Amount_2: 2.33, 
                        Donation_Amount_3: 3.44, 
                        Donation_Amount_4: 4.55, 
                        Allow_Custom_Donations: false, 
                        Button_Size_Display: 'donate250cc', 
                        Donation_Currency: 'ABC', 
                        Boarding_ID: 25
                    }).respond(500, 'This is an error response on /merchant_donation');
                    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(console.log).toHaveBeenCalledWith('This is an error response on /merchant_donation');
                    expect(scope.displayConfirmation).toHaveBeenCalledWith('error', {}); 
                });
            });
    
            describe('doCreate VI function (donation, invalid form)', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="dButtonType" value="donate150" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate150cc" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250cc" ng_model="dButtonType" checked/><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(false);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should call $scope.displayConfirmation with the appropriate message', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    spyOn(console,'log').and.callThrough();
                    scope.page = 2;
                    scope.Donation_Name = 'theDonationName';
                    scope.Donation_Description = 'theDonationDescription';
                    scope.Donation_Currency ='ABC';
                    scope.Donation_Amount_1 = 1.22;
                    scope.Donation_Amount_2 = 2.33;
                    scope.Donation_Amount_3 = 3.44;
                    scope.Donation_Amount_4 = 4.55;
                    scope.Allow_Custom_Donations = false;
                    scope.Button_Size_Display = 'DonationSizeSMALL'; 
    
                    scope.doCreate();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('Invalid donation form');
                });
            });
    
            describe('doCreate VII function (product, button = buynow150', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="pButtonType" value="buynow150" ng-model="pButtonType" checked /><input type="radio" name="pButtonType" value="buynow150cc" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250cc" ng-model="pButtonType" /><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should post to /merchant_donation with the appropriate variables', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    scope.page = 1;
                    scope.Product_Name = 'theProductName';
                    scope.Product_Description = 'theProductDescription';
                    scope.Product_SKU = 'theProductSKU';
                    scope.Product_Currency ='XYZ';
                    scope.Product_Price = 5.66;
                    scope.Tax = 6.77;
                    scope.Shipping = 7.88;
                    scope.Button_Size_Display = 'PaymentSizeLARGE'; 
    
                    $httpBackend.when('POST', '/merchant_product', {
                        Product_Name: 'theProductName', 
                        Product_Description: 'theProductDescription', 
                        Product_SKU: 'theProductSKU', 
                        Product_Price: 5.66, 
                        Tax: 6.77, 
                        Shipping: 7.88, 
                        Button_Size_Display: 'buynow150', 
                        Product_Currency: 'XYZ', 
                        Boarding_ID: 25
                    }).respond(200, {Product_Name: 'theProductName', Hash: 'Z9Y8X7W6V5U4T3S2R1Q0',Button_Size_Display: 'buynow150'});
                    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                });
    
                it('should call $scope.displayConfirmation with the appropriate message', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    scope.page = 1;
                    scope.Product_Name = 'theProductName';
                    scope.Product_Description = 'theProductDescription';
                    scope.Product_SKU = 'theProductSKU';
                    scope.Product_Currency ='XYZ';
                    scope.Product_Price = 5.66;
                    scope.Tax = 6.77;
                    scope.Shipping = 7.88;
                    scope.Button_Size_Display = 'PaymentSizeLARGE'; 
    
                    $httpBackend.when('POST', '/merchant_product', {
                        Product_Name: 'theProductName', 
                        Product_Description: 'theProductDescription', 
                        Product_SKU: 'theProductSKU', 
                        Product_Price: 5.66, 
                        Tax: 6.77, 
                        Shipping: 7.88, 
                        Button_Size_Display: 'buynow150', 
                        Product_Currency: 'XYZ', 
                        Boarding_ID: 25
                    }).respond(200, {Product_Name: 'theProductName', Hash: 'Z9Y8X7W6V5U4T3S2R1Q0',Button_Size_Display: 'buynow150'});
                    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.displayConfirmation).toHaveBeenCalledWith('okay', '<a href="http://secure.mojopay.com:80/#!/buttonapi/productbutton?token=Z9Y8X7W6V5U4T3S2R1Q0" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/buynow150px.png" style="border:none;"></a>', 'theProductName');
                });
            });
    
            describe('doCreate VIII function (product, button = buynow150cc', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="pButtonType" value="buynow150" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow150cc" ng-model="pButtonType" checked /><input type="radio" name="pButtonType" value="buynow250" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250cc" ng-model="pButtonType" /><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should post to /merchant_donation with the appropriate variables', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    scope.page = 1;
                    scope.Product_Name = 'theProductName';
                    scope.Product_Description = 'theProductDescription';
                    scope.Product_SKU = 'theProductSKU';
                    scope.Product_Currency ='XYZ';
                    scope.Product_Price = 5.66;
                    scope.Tax = 6.77;
                    scope.Shipping = 7.88;
                    scope.Button_Size_Display = 'PaymentSizeLARGE'; 
    
                    $httpBackend.when('POST', '/merchant_product', {
                        Product_Name: 'theProductName', 
                        Product_Description: 'theProductDescription', 
                        Product_SKU: 'theProductSKU', 
                        Product_Price: 5.66, 
                        Tax: 6.77, 
                        Shipping: 7.88, 
                        Button_Size_Display: 'buynow150cc', 
                        Product_Currency: 'XYZ', 
                        Boarding_ID: 25
                    }).respond(200, {Product_Name: 'theProductName', Hash: 'Z9Y8X7W6V5U4T3S2R1Q0',Button_Size_Display: 'buynow150cc'});
                    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                  expect(scope.displayConfirmation).toHaveBeenCalledWith('okay', '<a href="http://secure.mojopay.com:80/#!/buttonapi/productbutton?token=Z9Y8X7W6V5U4T3S2R1Q0" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/buynow150pxcc.png" style="border:none;"></a>', 'theProductName');
                });
            });
    
            describe('doCreate IX function (product, button = buynow250', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="pButtonType" value="buynow150" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow150cc" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250" ng-model="pButtonType" checked /><input type="radio" name="pButtonType" value="buynow250cc" ng-model="pButtonType" /><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should post to /merchant_donation with the appropriate variables', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    scope.page = 1;
                    scope.Product_Name = 'theProductName';
                    scope.Product_Description = 'theProductDescription';
                    scope.Product_SKU = 'theProductSKU';
                    scope.Product_Currency ='XYZ';
                    scope.Product_Price = 5.66;
                    scope.Tax = 6.77;
                    scope.Shipping = 7.88;
                    scope.Button_Size_Display = 'PaymentSizeLARGE'; 
    
                    $httpBackend.when('POST', '/merchant_product', {
                        Product_Name: 'theProductName', 
                        Product_Description: 'theProductDescription', 
                        Product_SKU: 'theProductSKU', 
                        Product_Price: 5.66, 
                        Tax: 6.77, 
                        Shipping: 7.88, 
                        Button_Size_Display: 'buynow250', 
                        Product_Currency: 'XYZ', 
                        Boarding_ID: 25
                    }).respond(200, {Product_Name: 'theProductName', Hash: 'Z9Y8X7W6V5U4T3S2R1Q0',Button_Size_Display: 'buynow250'});
                    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                  expect(scope.displayConfirmation).toHaveBeenCalledWith('okay', '<a href="http://secure.mojopay.com:80/#!/buttonapi/productbutton?token=Z9Y8X7W6V5U4T3S2R1Q0" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/buynow250px.png" style="border:none;"></a>', 'theProductName');
                });
            });
    
            describe('doCreate X function (product, button = buynow250cc', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="pButtonType" value="buynow150" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow150cc" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250cc" ng-model="pButtonType" checked /><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should post to /merchant_donation with the appropriate variables', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    scope.page = 1;
                    scope.Product_Name = 'theProductName';
                    scope.Product_Description = 'theProductDescription';
                    scope.Product_SKU = 'theProductSKU';
                    scope.Product_Currency ='XYZ';
                    scope.Product_Price = 5.66;
                    scope.Tax = 6.77;
                    scope.Shipping = 7.88;
                    scope.Button_Size_Display = 'PaymentSizeLARGE'; 
    
                    $httpBackend.when('POST', '/merchant_product', {
                        Product_Name: 'theProductName', 
                        Product_Description: 'theProductDescription', 
                        Product_SKU: 'theProductSKU', 
                        Product_Price: 5.66, 
                        Tax: 6.77, 
                        Shipping: 7.88, 
                        Button_Size_Display: 'buynow250cc', 
                        Product_Currency: 'XYZ', 
                        Boarding_ID: 25
                    }).respond(200, {Product_Name: 'theProductName', Hash: 'Z9Y8X7W6V5U4T3S2R1Q0',Button_Size_Display: 'buynow250cc'});
                    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                  expect(scope.displayConfirmation).toHaveBeenCalledWith('okay', '<a href="http://secure.mojopay.com:80/#!/buttonapi/productbutton?token=Z9Y8X7W6V5U4T3S2R1Q0" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/buynow250pxcc.png" style="border:none;"></a>', 'theProductName');
                });
            });
    
            describe('doCreate XI function (product, button = buynow250cc  error on /merchant_product', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="pButtonType" value="buynow150" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow150cc" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250cc" ng-model="pButtonType" checked /><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should post to /merchant_donation with the appropriate variables', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    spyOn(console,'log').and.callThrough();
                    scope.page = 1;
                    scope.Product_Name = 'theProductName';
                    scope.Product_Description = 'theProductDescription';
                    scope.Product_SKU = 'theProductSKU';
                    scope.Product_Currency ='XYZ';
                    scope.Product_Price = 5.66;
                    scope.Tax = 6.77;
                    scope.Shipping = 7.88;
                    scope.Button_Size_Display = 'PaymentSizeLARGE'; 
    
                    $httpBackend.when('POST', '/merchant_product', {
                        Product_Name: 'theProductName', 
                        Product_Description: 'theProductDescription', 
                        Product_SKU: 'theProductSKU', 
                        Product_Price: 5.66, 
                        Tax: 6.77, 
                        Shipping: 7.88, 
                        Button_Size_Display: 'buynow250cc', 
                        Product_Currency: 'XYZ', 
                        Boarding_ID: 25
                    }).respond(500, 'This is an error response on /merchant_product');
                                  
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(console.log).toHaveBeenCalledWith('This is an error response on /merchant_product');
                    expect(scope.displayConfirmation).toHaveBeenCalledWith('error', {});
                });
            });
    
            describe('doCreate VI function (donation, invalid form)', function() {
                beforeEach(function() {
                    this.element = $('<input type="radio" name="dButtonType" value="donate150" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate150cc" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250cc" ng_model="dButtonType" checked/><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
                    
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(false);
                    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should call $scope.displayConfirmation with the appropriate message', function() {
                    spyOn(scope,'displayConfirmation').and.returnValue(true);
                    spyOn(console,'log').and.callThrough();
                    scope.page = 1;
                    scope.Product_Name = 'theProductName';
                    scope.Product_Description = 'theProductDescription';
                    scope.Product_SKU = 'theProductSKU';
                    scope.Product_Currency ='XYZ';
                    scope.Product_Price = 5.66;
                    scope.Tax = 6.77;
                    scope.Shipping = 7.88;
                    scope.Button_Size_Display = 'PaymentSizeLARGE'; 
    
                    scope.doCreate();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('Invalid product form');
                });
            });
        });
        
        describe('doEdit function', function() {
            describe('doEdit function I (product)', function() {
                var productButtonDeferred,
                    donationButtonDeferred,
                    $promise;
                
                beforeEach(function() {
                    this.element = $('<input type="radio" name="pButtonType" value="buynow150"   ng-model="pButtonType" checked /><input type="radio" name="pButtonType" value="buynow150cc" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250cc" ng-model="pButtonType" />');
                    this.element.appendTo('body');
                    
                    productButtonDeferred = $q.defer();
                    productButtonDeferred.resolve({response: [{
                        Product_Name: 'theProductName',
                        Product_Description: 'theProductDescription',
                        Product_SKU: 'theProductSKU',
                        Product_Currency: 'XYZ',
                        Product_Price: 99.99,
                        Tax: 8.88,
                        Shipping: 7.77,
                        Button_Size_Display: 'buynow250',
                        Boarding_ID: 25
                    }]});
                    
                    donationButtonDeferred = $q.defer();
                    donationButtonDeferred.resolve({response: [{
                        Donation_Name: 'theDonationName',
                        Donation_Description: 'theDonationDescription',
                        Donation_Currency: 'ABC',
                        Donation_Amount_1: 1.22,
                        Donation_Amount_2: 2.33,
                        Donation_Amount_3: 3.44,
                        Donation_Amount_4: 4.55,
                        Allow_Custom_Donations: false,
                        Button_Size_Display: 'DonationSizeSMALL' 
                    }]});
                    
                    spyOn(ProductButtonService, 'get').and.returnValue({$promise: productButtonDeferred.promise});
                    spyOn(DonationButtonService, 'get').and.returnValue({$promise: donationButtonDeferred.promise});
                
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should call ProductButtonService.get', function() {
                    spyOn(scope,'confirmationModal').and.returnValue(true);
                    $httpBackend.when('PUT', 'merchant_product').respond(200, {Donation_Name: 'theDonationName', Hash: '0A1B2C3D4E5F6G7H8I9',Button_Size_Display: 'donate150'});
                    
                    
                    scope.param = ['', 'productbuttonedit', '665'];
                    scope.page = 1;
                    scope.Product_Name = 'junk';
                    scope.Product_Description = 'garbage';
                    scope.Product_SKU = 'trash';
                    scope.Product_Currency = 'ABC';
                    scope.Product_Price = 1.11;
                    scope.Tax = 2.22;
                    scope.Shipping = 3.33;
                    
                    scope.doEdit();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(ProductButtonService.get).toHaveBeenCalledWith({boardingId: 25, id: '665', Is_Active: 1});
                 });
                 it('should call ProductButtonService.put (update)', function() {
                    spyOn(scope,'confirmationModal').and.returnValue(true);
                    $httpBackend.when('PUT', 'merchant_product', {
                        Product_Name: 'junk',
                        Product_Description: 'garbage',
                        Product_SKU: 'trash',
                        Product_Currency: 'ABC',
                        Product_Price: 1.11,
                        Tax: 2.22,
                        Shipping: 3.33,
                        Button_Size_Display: 'buynow150',
                        Boarding_ID: 25
                    }).respond(200, {Donation_Name: 'theDonationName', Hash: '0A1B2C3D4E5F6G7H8I9',Button_Size_Display: 'donate150'});
                    
                    scope.param = ['', 'productbuttonedit', '665'];
                    scope.page = 1;
                    scope.Product_Name = 'junk';
                    scope.Product_Description = 'garbage';
                    scope.Product_SKU = 'trash';
                    scope.Product_Currency = 'ABC';
                    scope.Product_Price = 1.11;
                    scope.Tax = 2.22;
                    scope.Shipping = 3.33;
                    
                    scope.doEdit();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                 });
                 it('should call scope.confirmationModal', function() {
                    spyOn(scope,'confirmationModal').and.returnValue(true);
                    $httpBackend.when('PUT', 'merchant_product', {
                        Product_Name: 'junk',
                        Product_Description: 'garbage',
                        Product_SKU: 'trash',
                        Product_Currency: 'ABC',
                        Product_Price: 1.11,
                        Tax: 2.22,
                        Shipping: 3.33,
                        Button_Size_Display: 'buynow150',
                        Boarding_ID: 25
                    }).respond(200, {Donation_Name: 'theDonationName', Hash: '0A1B2C3D4E5F6G7H8I9',Button_Size_Display: 'donate150'});
                    
                    scope.param = ['', 'productbuttonedit', '665'];
                    scope.page = 1;
                    scope.Product_Name = 'junk';
                    scope.Product_Description = 'garbage';
                    scope.Product_SKU = 'trash';
                    scope.Product_Currency = 'ABC';
                    scope.Product_Price = 1.11;
                    scope.Tax = 2.22;
                    scope.Shipping = 3.33;
                    
                    scope.doEdit();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Button Edit', message: 'Button Successfully Edited.', accept: '', deny: 'OK' });
                 });
            });
            
            describe('doEdit function II (product) invalid product form', function() {
                 var productButtonDeferred,
                    donationButtonDeferred,
                    $promise;
                
                beforeEach(function() {
                    this.element = $('<input type="radio" name="pButtonType" value="buynow150"   ng-model="pButtonType" checked /><input type="radio" name="pButtonType" value="buynow150cc" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250"   ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250cc" ng-model="pButtonType" />');
                    this.element.appendTo('body');
                    
                    productButtonDeferred = $q.defer();
                    productButtonDeferred.resolve({response: [{
                        Product_Name: 'theProductName',
                        Product_Description: 'theProductDescription',
                        Product_SKU: 'theProductSKU',
                        Product_Currency: 'XYZ',
                        Product_Price: 99.99,
                        Tax: 8.88,
                        Shipping: 7.77,
                        Button_Size_Display: 'buynow250',
                        Boarding_ID: 25
                    }]});
                    
                    donationButtonDeferred = $q.defer();
                    donationButtonDeferred.resolve({response: [{
                        Donation_Name: 'theDonationName',
                        Donation_Description: 'theDonationDescription',
                        Donation_Currency: 'ABC',
                        Donation_Amount_1: 1.22,
                        Donation_Amount_2: 2.33,
                        Donation_Amount_3: 3.44,
                        Donation_Amount_4: 4.55,
                        Allow_Custom_Donations: false,
                        Button_Size_Display: 'DonationSizeSMALL' 
                    }]});
                    
                    spyOn(ProductButtonService, 'get').and.returnValue({$promise: productButtonDeferred.promise});
                    spyOn(DonationButtonService, 'get').and.returnValue({$promise: donationButtonDeferred.promise});
                
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(false);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should log an invaid form error', function() {
                    spyOn(scope,'confirmationModal').and.returnValue(true);
                    spyOn(console,'log').and.callThrough();
                    
                    scope.param = ['', 'productbuttonedit', '665'];
                    scope.page = 1;
                    scope.Product_Name = 'junk';
                    scope.Product_Description = 'garbage';
                    scope.Product_SKU = 'trash';
                    scope.Product_Currency = 'ABC';
                    scope.Product_Price = 1.11;
                    scope.Tax = 2.22;
                    scope.Shipping = 3.33;
                    
                    scope.doEdit();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('Invalid product form');
                 });
            });

            describe('doEdit function III (donation)', function() {
                var productButtonDeferred,
                    donationButtonDeferred,
                    $promise;
                
                beforeEach(function() {
                    this.element = $('<input type="radio" name="dButtonType" value="donate150" ng_model="dButtonType" checked/><input type="radio" name="dButtonType" value="donate150cc" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250" ng_model="dButtonType" /><input type="radio" name="dButtonType" value="donate250cc" ng_model="dButtonType" />');
                    this.element.appendTo('body');
                    
                    productButtonDeferred = $q.defer();
                    productButtonDeferred.resolve({response: [{
                        Product_Name: 'theProductName',
                        Product_Description: 'theProductDescription',
                        Product_SKU: 'theProductSKU',
                        Product_Currency: 'XYZ',
                        Product_Price: 99.99,
                        Tax: 8.88,
                        Shipping: 7.77,
                        Button_Size_Display: 'buynow250',
                        Boarding_ID: 25
                    }]});
                    
                    donationButtonDeferred = $q.defer();
                    donationButtonDeferred.resolve({response: [{
                        Donation_Name: 'theDonationName',
                        Donation_Description: 'theDonationDescription',
                        Donation_Currency: 'ABC',
                        Donation_Amount_1: 1.22,
                        Donation_Amount_2: 2.33,
                        Donation_Amount_3: 3.44,
                        Donation_Amount_4: 4.55,
                        Allow_Custom_Donations: false,
                        Button_Size_Display: 'donate150',
                        Boarding_ID: 25 
                    }]});
                    
                    spyOn(ProductButtonService, 'get').and.returnValue({$promise: productButtonDeferred.promise});
                    spyOn(DonationButtonService, 'get').and.returnValue({$promise: donationButtonDeferred.promise});
                
                    spyOn(scope,'validDonationForm').and.returnValue(true);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should call DonationButtonService.get', function() {
                    spyOn(scope,'confirmationModal').and.returnValue(true);
                    $httpBackend.when('PUT', 'merchant_donation').respond(200, {Donation_Name: 'theDonationName', Hash: '0A1B2C3D4E5F6G7H8I9',Button_Size_Display: 'donate150'});

                    scope.param = ['', 'donationbuttonedit', '665'];
                    scope.page = 2;
                    scope.Donation_Name = 'junk';
                    scope.Donation_Description = 'garbage';
                    scope.Donation_Currency = 'JKL';
                    scope.Donation_Amount_1 = 99.99;
                    scope.Donation_Amount_2 = 88.88;
                    scope.Donation_Amount_3 = 77.77;
                    scope.Donation_Amount_4 = 66.66;
                    scope.Allow_Custom_Donations = true;
                    
                    scope.doEdit();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(DonationButtonService.get).toHaveBeenCalledWith({boardingId: 25, id: '665', Is_Active: 1});
                 });
                it('should call DonationButtonService.put (update)', function() {
                    spyOn(scope,'confirmationModal').and.returnValue(true);
                    $httpBackend.when('PUT', 'merchant_donation', {
                        Donation_Name: 'junk',
                        Donation_Description: 'garbage',
                        Donation_Currency: 'JKL',
                        Donation_Amount_1: 99.99,
                        Donation_Amount_2: 88.88,
                        Donation_Amount_3: 77.77,
                        Donation_Amount_4: 66.66,
                        Allow_Custom_Donations: true,
                        Button_Size_Display: 'donate150',
                        Boarding_ID: 25
                    }).respond(200, {Donation_Name: 'theDonationName', Hash: '0A1B2C3D4E5F6G7H8I9',Button_Size_Display: 'donate150'});
                    
                    scope.param = ['', 'donationbuttonedit', '665'];
                    scope.page = 2;
                    scope.Donation_Name = 'junk';
                    scope.Donation_Description = 'garbage';
                    scope.Donation_Currency = 'JKL';
                    scope.Donation_Amount_1 = 99.99;
                    scope.Donation_Amount_2 = 88.88;
                    scope.Donation_Amount_3 = 77.77;
                    scope.Donation_Amount_4 = 66.66;
                    scope.Allow_Custom_Donations = true;
                    
                    scope.doEdit();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                 });
                it('should call scope.confirmationModal', function() {
                    spyOn(scope,'confirmationModal').and.returnValue(true);
                    $httpBackend.when('PUT', 'merchant_donation', {
                        Donation_Name: 'junk',
                        Donation_Description: 'garbage',
                        Donation_Currency: 'JKL',
                        Donation_Amount_1: 99.99,
                        Donation_Amount_2: 88.88,
                        Donation_Amount_3: 77.77,
                        Donation_Amount_4: 66.66,
                        Allow_Custom_Donations: true,
                        Button_Size_Display: 'donate150',
                        Boarding_ID: 25
                    }).respond(200, {Donation_Name: 'theDonationName', Hash: '0A1B2C3D4E5F6G7H8I9',Button_Size_Display: 'donate150'});
                    
                    scope.param = ['', 'donationbuttonedit', '665'];
                    scope.page = 2;
                    scope.Donation_Name = 'junk';
                    scope.Donation_Description = 'garbage';
                    scope.Donation_Currency = 'JKL';
                    scope.Donation_Amount_1 = 99.99;
                    scope.Donation_Amount_2 = 88.88;
                    scope.Donation_Amount_3 = 77.77;
                    scope.Donation_Amount_4 = 66.66;
                    scope.Allow_Custom_Donations = true;
                    
                    scope.doEdit();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Button Edit', message: 'Button Successfully Edited.', accept: '', deny: 'OK' });
                 });
            });
            
            describe('doEdit function IV (donation) invalid donation form', function() {
                 var productButtonDeferred,
                    donationButtonDeferred,
                    $promise;
                
                beforeEach(function() {
                    this.element = $('<input type="radio" name="pButtonType" value="buynow150"   ng-model="pButtonType" checked /><input type="radio" name="pButtonType" value="buynow150cc" ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250"   ng-model="pButtonType" /><input type="radio" name="pButtonType" value="buynow250cc" ng-model="pButtonType" />');
                    this.element.appendTo('body');
                    
                    productButtonDeferred = $q.defer();
                    productButtonDeferred.resolve({response: [{
                        Product_Name: 'theProductName',
                        Product_Description: 'theProductDescription',
                        Product_SKU: 'theProductSKU',
                        Product_Currency: 'XYZ',
                        Product_Price: 99.99,
                        Tax: 8.88,
                        Shipping: 7.77,
                        Button_Size_Display: 'buynow250',
                        Boarding_ID: 25,
                        id: 2046
                    }]});
                    
                    donationButtonDeferred = $q.defer();
                    donationButtonDeferred.resolve({response: [{
                        Donation_Name: 'theDonationName',
                        Donation_Description: 'theDonationDescription',
                        Donation_Currency: 'ABC',
                        Donation_Amount_1: 1.22,
                        Donation_Amount_2: 2.33,
                        Donation_Amount_3: 3.44,
                        Donation_Amount_4: 4.55,
                        Allow_Custom_Donations: false,
                        Button_Size_Display: 'DonationSizeSMALL' ,
                        Boarding_ID: 25 
                    }]});
                    
                    spyOn(ProductButtonService, 'get').and.returnValue({$promise: productButtonDeferred.promise});
                    spyOn(DonationButtonService, 'get').and.returnValue({$promise: donationButtonDeferred.promise});
                
                    spyOn(scope,'validDonationForm').and.returnValue(false);
                    spyOn(scope,'validProductForm').and.returnValue(true);
                });
    
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should log an invaid form error', function() {
                    spyOn(scope,'confirmationModal').and.returnValue(true);
                    spyOn(console,'log').and.callThrough();
                    
                    scope.param = ['', 'productbuttonedit', '665'];
                    scope.page = 2;
                    scope.Donation_Name = 'junk';
                    scope.Donation_Description = 'garbage';
                    scope.Donation_Currency = 'trash';
                    scope.Donation_Amount_1 = 1.22;
                    scope.Donation_Amount_2 = 2.33;
                    scope.Donation_Amount_3 = 3.44;
                    scope.Donation_Amount_4 = 4.55;
                    scope.Allow_Custom_Donations = false;
                    
                    scope.doEdit();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('Invalid donation form');
                 });
            });

            describe('doEdit function V (donation) Error editing button', function() {
                it('should log an Error editing button', function() {
                    spyOn(console,'log').and.callThrough();
                    
                    scope.param = ['', 'productbuttonedit', '665'];
                    scope.page = 0;
                    scope.doEdit();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('Error editing button');
                 });
            });
         });
    
        describe('buttonWrap function', function() {
            describe('buttonWrap I function', function() {
                var productButtonDeferred,
                    donationButtonDeferred,
                    $promise;
                    
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body"></div><input type="text" id="Button_Name" name="Button_Name" class="form-control" /><select type="text" id="Button_Type" name="Button_Type" class="form-control"><option value="all" selected>All Types</option><option value="products">Products</option><option value="donations">Donations</option></select>');
                    this.element.appendTo('body');
    
                    productButtonDeferred = $q.defer();
                    productButtonDeferred.resolve({response: [{
                        id: 1024,
                        Product_Name: 'theProductName',
                        Product_Description: 'theProductDescription',
                        Product_SKU: 'theProductSKU',
                        Product_Currency: 'XYZ',
                        Product_Price: 99.99,
                        Tax: 8.88,
                        Shipping: 7.77,
                        Button_Size_Display: 'buynow250',
                        Boarding_ID: 25
                    }]});
                    
                    donationButtonDeferred = $q.defer();
                    donationButtonDeferred.resolve({response: [{
                        id: 2046,
                        Donation_Name: 'theDonationName',
                        Donation_Description: 'theDonationDescription',
                        Donation_Currency: 'ABC',
                        Donation_Amount_1: 1.22,
                        Donation_Amount_2: 2.33,
                        Donation_Amount_3: 3.44,
                        Donation_Amount_4: 4.55,
                        Allow_Custom_Donations: false,
                        Button_Size_Display: 'donate250' 
                    }]});
                    
                    spyOn(ProductButtonService, 'get').and.returnValue({$promise: productButtonDeferred.promise});
                    spyOn(DonationButtonService, 'get').and.returnValue({$promise: donationButtonDeferred.promise});
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should call toggleLoader() to hide/show spinner', function() {
                    expect(document.getElementById('loadingImg').style.display).toBe('none');
                    expect(document.getElementById('report-body').style.display).toBe('');
                    scope.buttonWrap();
                    expect(document.getElementById('loadingImg').style.display).toBe('none');
                    expect(document.getElementById('report-body').style.display).toBe('block');
                });
                it('should call initPaginator()', function() {
                    spyOn(scope,'initPaginator').and.returnValue(true);
                    scope.buttonWrap();
                    expect(scope.initPaginator).toHaveBeenCalledWith();
                });
                it('should initialize working variables and flags', function() {
                    expect(scope.itemButtons).toBeUndefined();
                    expect(scope.buttonsShow).toBeUndefined();
                    expect(scope.noResults).toBeUndefined();
                    scope.buttonWrap();
                    expect(scope.itemButtons).toEqual([]);
                    expect(scope.buttonsShow).toBe(false);
                    expect(scope.noResults).toBe(true);
                });
                it('should call both ProductButtonService.get and DonationButtonService', function() {
                    document.getElementById('Button_Type').value = 'all';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(ProductButtonService.get).toHaveBeenCalledWith({ boardingId: 25 });
                    expect(DonationButtonService.get).toHaveBeenCalledWith({ boardingId: 25 });
                });
                it('should only call ProductButtonService.get', function() {
                    document.getElementById('Button_Type').value = 'products';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(ProductButtonService.get).toHaveBeenCalledWith({ boardingId: 25 });
                    expect(DonationButtonService.get).not.toHaveBeenCalledWith({ boardingId: 25 });
                });
                it('should only call DonationButtonService.get', function() {
                    document.getElementById('Button_Type').value = 'donations';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(ProductButtonService.get).not.toHaveBeenCalledWith({ boardingId: 25 });
                    expect(DonationButtonService.get).toHaveBeenCalledWith({ boardingId: 25 });
                });
                it('should call both ProductButtonService.get and DonationButtonService with a product name', function() {
                    document.getElementById('Button_Type').value = 'all';
                    document.getElementById('Button_Name').value = 'theName';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(ProductButtonService.get).toHaveBeenCalledWith({ boardingId: 25, Product_Name: 'theName' });
                    expect(DonationButtonService.get).toHaveBeenCalledWith({ boardingId: 25, Product_Name: 'theName' });
                });
                it('should not call either ProductButtonService.get or DonationButtonService if the product name is bland, null, or undefined', function() {
                    document.getElementById('Button_Type').value = 'all';
                    document.getElementById('Button_Name').value = '';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(ProductButtonService.get).toHaveBeenCalledWith({ boardingId: 25});
                    expect(DonationButtonService.get).toHaveBeenCalledWith({ boardingId: 25});
                    document.getElementById('Button_Name').value = null;
                    scope.buttonWrap();
                    scope.$digest();
                    expect(ProductButtonService.get).toHaveBeenCalledWith({ boardingId: 25});
                    expect(DonationButtonService.get).toHaveBeenCalledWith({ boardingId: 25});
                    document.getElementById('Button_Name').value = undefined;
                    scope.buttonWrap();
                    scope.$digest();
                    expect(ProductButtonService.get).toHaveBeenCalledWith({ boardingId: 25});
                    expect(DonationButtonService.get).toHaveBeenCalledWith({ boardingId: 25});
                });
                it('should set flags when only call ProductButtonService.get is called', function() {
                    document.getElementById('Button_Type').value = 'products';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(scope.noResults).toBe(false);
                    expect(scope.buttonsShow).toBe(true);
                });
                it('should set flags when only call DonationButtonService.get is called', function() {
                    document.getElementById('Button_Type').value = 'donations';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(scope.noResults).toBe(false);
                    expect(scope.buttonsShow).toBe(true);
                });
                it('should set scope.itemButtons when both ProductButtonService.get and DonationButtonService.get are called', function() {
                    document.getElementById('Button_Type').value = 'all';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(scope.itemButtons).toEqual([ { Button_ID: 2046, Button_Name: 'theDonationName', Button_Type: 'Donation', Button_URL: '<a href="http://secure.mojopay.com:80/#!/buttonapi/donationbutton?token=undefined" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/donate250px.png" style="border:none;"></a>', Button_Size: '250px wide, no credit card images' }, { Button_ID: 1024, Button_Name: 'theProductName', Button_Type: 'Product', Button_URL: '<a href="http://secure.mojopay.com:80/#!/buttonapi/productbutton?token=undefined" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/buynow250px.png" style="border:none;"></a>', Button_Size: '250px wide, no credit card images' } ]);
                });
                it('should set scope.itemButtons when only ProductButtonService.get is called', function() {
                    document.getElementById('Button_Type').value = 'products';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(scope.itemButtons).toEqual([ { Button_ID: 1024, Button_Name: 'theProductName', Button_Type: 'Product', Button_URL: '<a href="http://secure.mojopay.com:80/#!/buttonapi/productbutton?token=undefined" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/buynow250px.png" style="border:none;"></a>', Button_Size: '250px wide, no credit card images' } ]);
                });
                it('should set scope.itemButtons when only DonationButtonService.get is called', function() {
                    document.getElementById('Button_Type').value = 'donations';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(scope.itemButtons).toEqual([ { Button_ID: 2046, Button_Name: 'theDonationName', Button_Type: 'Donation', Button_URL: '<a href="http://secure.mojopay.com:80/#!/buttonapi/donationbutton?token=undefined" style="border:none;" target="_blank"><img src="http://secure.mojopay.com:80/img/donate250px.png" style="border:none;"></a>', Button_Size: '250px wide, no credit card images' } ]);
                });
            });

            describe('buttonWrap II function (ProductButtonService.get error)', function() {
                var productButtonDeferred,
                    donationButtonDeferred,
                    $promise;
                    
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body"></div><input type="text" id="Button_Name" name="Button_Name" class="form-control" /><select type="text" id="Button_Type" name="Button_Type" class="form-control"><option value="all" selected>All Types</option><option value="products">Products</option><option value="donations">Donations</option></select>');
                    this.element.appendTo('body');
    
                    productButtonDeferred = $q.defer();
                    productButtonDeferred.reject('This is an error response from ProductButtonService.get');
                    
                    donationButtonDeferred = $q.defer();
                    donationButtonDeferred.resolve({response: [{
                        id: 2046,
                        Donation_Name: 'theDonationName',
                        Donation_Description: 'theDonationDescription',
                        Donation_Currency: 'ABC',
                        Donation_Amount_1: 1.22,
                        Donation_Amount_2: 2.33,
                        Donation_Amount_3: 3.44,
                        Donation_Amount_4: 4.55,
                        Allow_Custom_Donations: false,
                        Button_Size_Display: 'donate250' 
                    }]});
                    
                    spyOn(ProductButtonService, 'get').and.returnValue({$promise: productButtonDeferred.promise});
                    spyOn(DonationButtonService, 'get').and.returnValue({$promise: donationButtonDeferred.promise});
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should log an error on ProductButtonService.get error', function() {
                    spyOn(console,'log').and.callThrough();
                    document.getElementById('Button_Type').value = 'products';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('This is an error response from ProductButtonService.get');
                 });
            });

            describe('buttonWrap III function (DonationButtonService.get error)', function() {
                var productButtonDeferred,
                    donationButtonDeferred,
                    $promise;
                    
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body"></div><input type="text" id="Button_Name" name="Button_Name" class="form-control" /><select type="text" id="Button_Type" name="Button_Type" class="form-control"><option value="all" selected>All Types</option><option value="products">Products</option><option value="donations">Donations</option></select>');
                    this.element.appendTo('body');
    
                    productButtonDeferred = $q.defer();
                    productButtonDeferred.resolve({response: [{
                        id: 1024,
                        Product_Name: 'theProductName',
                        Product_Description: 'theProductDescription',
                        Product_SKU: 'theProductSKU',
                        Product_Currency: 'XYZ',
                        Product_Price: 99.99,
                        Tax: 8.88,
                        Shipping: 7.77,
                        Button_Size_Display: 'buynow250',
                        Boarding_ID: 25
                    }]});
                    
                    donationButtonDeferred = $q.defer();
                    donationButtonDeferred.reject('This is an error response from DonationButtonService.get');
                    
                    spyOn(ProductButtonService, 'get').and.returnValue({$promise: productButtonDeferred.promise});
                    spyOn(DonationButtonService, 'get').and.returnValue({$promise: donationButtonDeferred.promise});
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
    
                it('should log an error on ProductButtonService.get error', function() {
                    spyOn(console,'log').and.callThrough();
                    document.getElementById('Button_Type').value = 'donations';
                    scope.buttonWrap();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('This is an error response from DonationButtonService.get');
                });
            });
        });
    });
}());

(function() {
   describe('ButtonController', function() {
        // Initialize global variables
        var $q,
            ButtonController,
            scope,
            $httpBackend,
            $location,
            $modal,
            $stateParams,
            Authentication,
            ProductButtonService,
            DonationButtonService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope,_$httpBackend_,_$location_, _$modal_,_$stateParams_,_Authentication_,_ProductButtonService_,_DonationButtonService_, _$q_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {};

            // Point global variables to injected services
            $httpBackend = _$httpBackend_;
            $location = _$location_;
            $modal = _$modal_;
            $stateParams = _$stateParams_;
            $q = _$q_;
            
            ProductButtonService = _ProductButtonService_;
            DonationButtonService = _DonationButtonService_;

            spyOn($location, 'path');
            
            // Initialize the controller
            ButtonController = $controller('ButtonController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        describe('Logged in', function(){
           it('should not redirect if user is not logged in', function() {
                expect($location.path).toHaveBeenCalledWith('/signin');
            });
        });
    });
}());
