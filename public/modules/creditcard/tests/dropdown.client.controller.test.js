/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('SaleController', function() {
        // Initialize global variables
        var SaleController,
            dropdownController,
            scope,
            $q,
            Authentication,
            PaymentService,
            Nmitransactions,
            ReserveService,
            $location,
            $anchorScroll,
            $filter,
            $parse,
            $modal,
            MasService,
            TransactionsService,
            ProcessorService,
            ProductService,
            MerchantReportService,
            RunningBalanceService,
            SettleService,
            DiscountService,
            TransactionFeeService,
            Account_BalanceService,
            disbursementsService,
            $http,
            AccountBalanceReportService,
            Users,
            ACHReportService,
            PaymentCompleteService,
            LineItemsService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_,_Authentication_,_PaymentService_,_Nmitransactions_,_ReserveService_,_$location_,_$anchorScroll_,_$filter_,_$parse_,_$modal_,_MasService_,_TransactionsService_,_ProcessorService_,_ProductService_,_MerchantReportService_,_RunningBalanceService_,_SettleService_,_DiscountService_,_TransactionFeeService_,_Account_BalanceService_,_disbursementsService_,_$http_,_AccountBalanceReportService_,_Users_,_ACHReportService_,_PaymentCompleteService_,_LineItemsService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            ReserveService = _ReserveService_;
            $location = _$location_;
            $anchorScroll = _$anchorScroll_;
            $filter = _$filter_;
            $parse = _$parse_;
            $modal = _$modal_;
            MasService = _MasService_;
            TransactionsService = _TransactionsService_;
            ProcessorService = _ProcessorService_;
            ProductService = _ProductService_;
            MerchantReportService = _MerchantReportService_;
            RunningBalanceService = _RunningBalanceService_;
            SettleService = _SettleService_;
            DiscountService = _DiscountService_;
            TransactionFeeService = _TransactionFeeService_;
            Account_BalanceService = _Account_BalanceService_;
            disbursementsService = _disbursementsService_;
            $http = _$http_;
            AccountBalanceReportService = _AccountBalanceReportService_;
            Users = _Users_;
            ACHReportService = _ACHReportService_;
            PaymentCompleteService = _PaymentCompleteService_;
            LineItemsService = _LineItemsService_;
                        
            // Point global variables to injected services
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
//console.log(scope);
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have function scope.setMerchantId defined', function() {
                expect(scope.setMerchantId).toBeDefined();    
            }); 
            it('should have function scope.setProducts defined', function() {
                expect(scope.setProducts).toBeDefined();    
            }); 
            it('should have function scope.setProductId defined', function() {
                expect(scope.setProductId).toBeDefined();    
            }); 
            it('should have function scope.setProductId8 defined', function() {
                expect(scope.setProductId8).toBeDefined();    
            }); 
        });

        describe('Interface', function() {
            it('should recognize setMerchantId as a function', function() {
                expect(angular.isFunction(scope.setMerchantId)).toBe(true);
            });
            it('should recognize setProducts as a function', function() {
                expect(angular.isFunction(scope.setProducts)).toBe(true);
            });
            it('should recognize setProductId as a function', function() {
                expect(angular.isFunction(scope.setProductId)).toBe(true);
            });
            it('should recognize setProductId8 as a function', function() {
                expect(angular.isFunction(scope.setProductId8)).toBe(true);
            });
        });

        describe('scope.setMerchantId function', function(){
            beforeEach(function(){
				this.element = $('<ul id="theId"><button class="" type="button" id="dropdownMenu1" /></ul>');
				this.element.appendTo('body');
             });
    
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});

            it('should change the class of #dropdownMenu1  parent ', function() {
                var merchantid = {
                    products: [{},{},{}]
                };
                expect(document.getElementById('theId').className).toBe('');
                scope.setMerchantId(merchantid);
                expect(document.getElementById('theId').className).toBe('open');
            });
            it('should define scope.query', function() {
                var merchantid = {
                    products: [{},{},{}]
                };
                expect(scope.query).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.query).toBeDefined();
                expect(scope.query).toBe('');
            });
            it('should define scope.merchantId', function() {
                var merchantid = {
                    products: [{},{},{}]
                };
                expect(scope.merchantId).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.merchantId).toBeDefined();
                expect(scope.merchantId).toEqual(merchantid);
            });
            it('should define scope.products', function() {
                var merchantid = {
                    products: [{test:1},{test:2},{test:3}]
                };
                expect(scope.products).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.products).toBeDefined();
                expect(scope.products).toEqual(merchantid.products[0]);
            });
            it('should define scope.Product_NameMAR if there are two items in merchantid', function() {
                spyOn(scope,'setProducts').and.returnValue(false);
                var merchantid = {
                    products: [{test:1},{test:2}]
                };
                expect(scope.Product_NameMAR).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.Product_NameMAR).toBeDefined();
                expect(scope.Product_NameMAR).toEqual(merchantid.products[0]);
                expect(scope.setProducts).toHaveBeenCalledWith(merchantid.products[1]);
            });
            it('should define call functions Product_NameChangeMAR, Product_NameChangeUserMAR, Product_NameChangeABR, Product_NameChangeACH if there are two items in merchantid', function() {
                var merchantid = {
                    products: [{test:1},{test:2}]
                };
                spyOn(scope,'Product_NameChangeMAR').and.returnValue(true);
                spyOn(scope,'Product_NameChangeUserMAR').and.returnValue(true);
                spyOn(scope,'Product_NameChangeABR').and.returnValue(true);
                spyOn(scope,'Product_NameChangeACH').and.returnValue(true);
                scope.setMerchantId(merchantid);
                expect(scope.Product_NameChangeMAR).toHaveBeenCalledWith();
                expect(scope.Product_NameChangeUserMAR).toHaveBeenCalledWith();
                expect(scope.Product_NameChangeABR).toHaveBeenCalledWith();
                expect(scope.Product_NameChangeACH).toHaveBeenCalledWith();
            });
            it('should define scope.showReportMAR', function() {
                var merchantid = {
                    products: [{test:1},{test:2},{test:3}]
                };
                expect(scope.showReportMAR).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.showReportMAR).toBeDefined();
                expect(scope.showReportMAR).toBe(false);
            });
            it('should define scope.showReportABR', function() {
                var merchantid = {
                    products: [{test:1},{test:2},{test:3}]
                };
                expect(scope.showReportABR).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.showReportABR).toBeDefined();
                expect(scope.showReportABR).toBe(false);
            });
            it('should define scope.showReportACH', function() {
                var merchantid = {
                    products: [{test:1},{test:2},{test:3}]
                };
                expect(scope.showReportACH).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.showReportACH).toBeDefined();
                expect(scope.showReportACH).toBe(false);
            });
            it('should define scope.showSeachTransactionsReport', function() {
                var merchantid = {
                    products: [{test:1},{test:2},{test:3}]
                };
                expect(scope.showSeachTransactionsReport).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.showSeachTransactionsReport).toBeDefined();
                expect(scope.showSeachTransactionsReport).toBe(false);
            });
            it('should define scope.Product_NameMAR', function() {
                var merchantid = {
                    products: [{test:1},{test:2},{test:3}]
                };
                expect(scope.Product_NameMAR).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.Product_NameMAR).toBeDefined();
                expect(scope.Product_NameMAR).toBe(merchantid.products[0]);
            });
            it('should define scope.Product_NameUserMAR', function() {
                var merchantid = {
                    products: [{test:1},{test:2},{test:3}]
                };
                expect(scope.Product_NameUserMAR).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.Product_NameUserMAR).toBeDefined();
                expect(scope.Product_NameUserMAR).toBe(merchantid.products[0]);
            });
            it('should define scope.Product_NameABR', function() {
                var merchantid = {
                    products: [{test:1},{test:2},{test:3}]
                };
                expect(scope.Product_NameABR).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.Product_NameABR).toBeDefined();
                expect(scope.Product_NameABR).toBe(merchantid.products[0]);
            });
            it('should define scope.Product_NameACH', function() {
                var merchantid = {
                    products: [{test:1},{test:2},{test:3}]
                };
                expect(scope.Product_NameACH).toBeUndefined();
                scope.setMerchantId(merchantid);
                expect(scope.Product_NameACH).toBeDefined();
                expect(scope.Product_NameACH).toBe(merchantid.products[0]);
            });
         });

        describe('scope.setProducts function', function(){
            beforeEach(function(){
				this.element = $('<ul id="theId"><button class="" type="button" id="dropdownMenu3" /></ul>');
				this.element.appendTo('body');
             });
    
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});

            it('should change the class of #dropdownMenu3  parent ', function() {
                var productid = {};
                expect(document.getElementById('theId').className).toBe('');
                scope.setProducts(productid);
                expect(document.getElementById('theId').className).toBe('open');
            });
            it('should define scope.queryproduct', function() {
                var productid = {};
                expect(scope.queryproduct).toBeUndefined();
                scope.setProducts(productid);
                expect(scope.queryproduct).toBeDefined();
                expect(scope.queryproduct).toBe('');
            });
            it('should define scope.products', function() {
                var productid = {};
                expect(scope.products).toBeUndefined();
                scope.setProducts(productid);
                expect(scope.products).toBeDefined();
                expect(scope.products).toEqual(productid);
            });
            it('should define scope.showReportMAR', function() {
                var productid = {};
                expect(scope.showReportMAR).toBeUndefined();
                scope.setProducts(productid);
                expect(scope.showReportMAR).toBeDefined();
                expect(scope.showReportMAR).toBe(false);
            });
            it('should define scope.showReportABR', function() {
                var productid = {};
                expect(scope.showReportABR).toBeUndefined();
                scope.setProducts(productid);
                expect(scope.showReportABR).toBeDefined();
                expect(scope.showReportABR).toBe(false);
            });
            it('should define scope.showReportACH', function() {
                var productid = {};
                expect(scope.showReportACH).toBeUndefined();
                scope.setProducts(productid);
                expect(scope.showReportACH).toBeDefined();
                expect(scope.showReportACH).toBe(false);
            });
            it('should define scope.showReportABR_Merchant', function() {
                var productid = {};
                expect(scope.showReportABR_Merchant).toBeUndefined();
                scope.setProducts(productid);
                expect(scope.showReportABR_Merchant).toBeDefined();
                expect(scope.showReportABR_Merchant).toBe(false);
            });
         });

        describe('scope.setProductId function', function(){
            beforeEach(function(){
				this.element = $('<ul id="theId"><button class="" type="button" id="dropdownMenu2" /></ul>');
				this.element.appendTo('body');
             });
    
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});

            it('should change the class of #dropdownMenu3  parent ', function() {
                var productid = {};
                expect(document.getElementById('theId').className).toBe('');
                scope.setProductId(productid);
                expect(document.getElementById('theId').className).toBe('open');
            });
            it('should define scope.queryproductid', function() {
                var productid = {};
                expect(scope.queryproductid).toBeUndefined();
                scope.setProductId(productid);
                expect(scope.queryproductid).toBeDefined();
                expect(scope.queryproductid).toBe('');
            });
            it('should change the value of scope.productId ', function() {
                var productid = {};
                expect(scope.productId).toBeDefined();
                expect(scope.productId).not.toEqual(productid);
                scope.setProductId(productid);
                expect(scope.productId).toEqual(productid);
            });
            it('should define scope.showReportMAR', function() {
                var productid = {};
                expect(scope.showReportMAR).toBeUndefined();
                scope.setProductId(productid);
                expect(scope.showReportMAR).toBeDefined();
                expect(scope.showReportMAR).toBe(false);
            });
            it('should define scope.showReportABR', function() {
                var productid = {};
                expect(scope.showReportABR).toBeUndefined();
                scope.setProductId(productid);
                expect(scope.showReportABR).toBeDefined();
                expect(scope.showReportABR).toBe(false);
            });
            it('should define scope.showReportACH', function() {
                var productid = {};
                expect(scope.showReportACH).toBeUndefined();
                scope.setProductId(productid);
                expect(scope.showReportACH).toBeDefined();
                expect(scope.showReportACH).toBe(false);
            });
            it('should define scope.showSeachTransactionsReport', function() {
                var productid = {};
                expect(scope.showSeachTransactionsReport).toBeUndefined();
                scope.setProductId(productid);
                expect(scope.showSeachTransactionsReport).toBeDefined();
                expect(scope.showSeachTransactionsReport).toBe(false);
            });
         });

        describe('scope.setProductId8 function', function(){
            beforeEach(function(){
				this.element = $('<ul id="theId"><button class="" type="button" id="dropdownMenu8" /></ul>                <button id="dayButton" class="">Day</button><button id="weekButton"  class="">Week</button><button id="monthButton" class="">Month</button><button id="yearButton" class="">Year</button>');
				this.element.appendTo('body');
             });
    
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});

            it('should change the class of #setProductId8 parent ', function() {
                var productid = {};
                scope.dashboardData = '';
                expect(document.getElementById('theId').className).toBe('');
                scope.setProductId8(productid);
                expect(document.getElementById('theId').className).toBe('open');
            });
            it('should define scope.queryproductid ', function() {
                var productid = {};
                scope.dashboardData = '';
                expect(scope.queryproductid).toBeUndefined();
                scope.setProductId8(productid);
                expect(scope.queryproductid).toBeDefined();
                expect(scope.queryproductid).toBe('');
            });
            it('should define scope.productId ', function() {
                var productid = {};
                scope.dashboardData = '';
                expect(scope.productId).toBeDefined();
                expect(scope.productId).not.toBe(productid);
                scope.setProductId8(productid);
                expect(scope.productId).toBeDefined();
                expect(scope.productId).toBe(productid);
            });
            it('should define scope.productIndex based on productId ', function() {
                var productid = 2;
                scope.dashboardData = [{productId:1},{productId:2},{productId:3}];
                expect(scope.productIndex).toBeUndefined();
                scope.setProductId8(productid);
                expect(scope.productIndex).toBeDefined();
                expect(scope.productIndex).toBe(1);
            });
            it('should not define scope.productIndex based on productId ', function() {
                var productid = 9;
                scope.dashboardData = [{productId:1},{productId:2},{productId:3}];
                expect(scope.productIndex).toBeUndefined();
                scope.setProductId8(productid);
                expect(scope.productIndex).toBeUndefined();
            });
            it('should call scope.DashboardActivityWrap with Parameter "day" if the class is set', function() {
                document.getElementById('dayButton').className = "active" ;
                var productid = {};
                spyOn(scope,'DashboardActivityWrap').and.returnValue(true);
                scope.dashboardData = '';
                scope.setProductId8(productid);
                expect(scope.DashboardActivityWrap).toHaveBeenCalledWith ('day');
            });
            it('should call scope.DashboardActivityWrap with Parameter "week" if the class is set', function() {
                document.getElementById('weekButton').className = "active" ;
                var productid = {};
                spyOn(scope,'DashboardActivityWrap').and.returnValue(true);
                scope.dashboardData = '';
                scope.setProductId8(productid);
                expect(scope.DashboardActivityWrap).toHaveBeenCalledWith ('week');
            });
            it('should call scope.DashboardActivityWrap with Parameter "month" if the class is set', function() {
                document.getElementById('monthButton').className = "active" ;
                var productid = {};
                spyOn(scope,'DashboardActivityWrap').and.returnValue(true);
                scope.dashboardData = '';
                scope.setProductId8(productid);
                expect(scope.DashboardActivityWrap).toHaveBeenCalledWith ('month');
            });
            it('should call scope.DashboardActivityWrap with Parameter "year" if the class is set', function() {
                document.getElementById('yearButton').className = "active" ;
                var productid = {};
                spyOn(scope,'DashboardActivityWrap').and.returnValue(true);
                scope.dashboardData = '';
                scope.setProductId8(productid);
                expect(scope.DashboardActivityWrap).toHaveBeenCalledWith ('year');
            });
       });        
    });

}());

(function() {
    // Authentication controller Spec
    describe('SaleController II', function() {
        // Initialize global variables
        var SaleController,
            dropdownController,
            scope,
            $q,
            Authentication,
            PaymentService,
            Nmitransactions,
            ReserveService,
            $location,
            $anchorScroll,
            $filter,
            $parse,
            $modal,
            MasService,
            TransactionsService,
            ProcessorService,
            ProductService,
            MerchantReportService,
            RunningBalanceService,
            SettleService,
            DiscountService,
            TransactionFeeService,
            Account_BalanceService,
            disbursementsService,
            $http,
            AccountBalanceReportService,
            Users,
            ACHReportService,
            PaymentCompleteService,
            LineItemsService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_,_Authentication_,_PaymentService_,_Nmitransactions_,_ReserveService_,_$location_,_$anchorScroll_,_$filter_,_$parse_,_$modal_,_MasService_,_TransactionsService_,_ProcessorService_,_ProductService_,_MerchantReportService_,_RunningBalanceService_,_SettleService_,_DiscountService_,_TransactionFeeService_,_Account_BalanceService_,_disbursementsService_,_$http_,_AccountBalanceReportService_,_Users_,_ACHReportService_,_PaymentCompleteService_,_LineItemsService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            $q = _$q_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            ReserveService = _ReserveService_;
            $location = _$location_;
            $anchorScroll = _$anchorScroll_;
            $filter = _$filter_;
            $parse = _$parse_;
            $modal = _$modal_;
            MasService = _MasService_;
            TransactionsService = _TransactionsService_;
            ProcessorService = _ProcessorService_;
            ProductService = _ProductService_;
            MerchantReportService = _MerchantReportService_;
            RunningBalanceService = _RunningBalanceService_;
            SettleService = _SettleService_;
            DiscountService = _DiscountService_;
            TransactionFeeService = _TransactionFeeService_;
            Account_BalanceService = _Account_BalanceService_;
            disbursementsService = _disbursementsService_;
            $http = _$http_;
            AccountBalanceReportService = _AccountBalanceReportService_;
            Users = _Users_;
            ACHReportService = _ACHReportService_;
            PaymentCompleteService = _PaymentCompleteService_;
            LineItemsService = _LineItemsService_;
                        
            // Point global variables to injected services
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));
 
         describe('scope.setProductId8 function', function(){
            beforeEach(function(){
				this.element = $('<ul id="theId"><button class="" type="button" id="dropdownMenu8" /></ul>');
				this.element.appendTo('body');
             });
    
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});

            it('should not call scope.DashboardActivityWrap ', function() {
                var productid = {};
                spyOn(scope,'DashboardActivityWrap').and.returnValue(true);
                scope.dashboardData = '';
                scope.setProductId8(productid);
                expect(scope.DashboardActivityWrap).not.toHaveBeenCalled();
            });
         });
    });
}());