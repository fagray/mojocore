/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('IntegrationController', function() {
        // Initialize global variables
        var $q,
            IntegrationController,
            scope,
            $httpBackend,
			$stateParams,
            $location,
            $timeout,
            Authentication,
            ApikeyService,
            PaymentService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, _$timeout_, _$q_, _Authentication_, _ApikeyService_, _PaymentService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            $location = _$location_;
            $stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
            $timeout = _$timeout_;
            $q = _$q_;

            // Point global variables to injected services
            Authentication = _Authentication_;
            ApikeyService = _ApikeyService_;
            PaymentService = _PaymentService_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25,
                    Enable_Nation: true,
                    Username: 'Test Account'
                }
            };
            
            // Initialize the controller
            IntegrationController = $controller('IntegrationController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });
            it('should have variable scope.pagenumber defined', function() {
                expect(scope.pagenumber).toBeDefined();
            });        
            it('should have variable scope.documentpdf defined', function() {
                expect(scope.documentpdf).toBeDefined();
            });        
            it('should have variable scope.integrationdocument defined', function() {
                expect(scope.integrationdocument).toBeDefined();
            });        
            it('should have variable scope.page defined', function() {
                expect(scope.page).toBeDefined();
            });        
            it('should have variable scope.integration_page_1 defined', function() {
                expect(scope.integration_page_1).toBeDefined();
            });        
            it('should have variable scope.integration_page_2 defined', function() {
                expect(scope.integration_page_2).toBeDefined();
            });        
            it('should have variable scope.templateurl defined', function() {
                expect(scope.templateurl).toBeDefined();
            });        
            it('should have variable scope.showNationBuilderTab defined', function() {
                expect(scope.showNationBuilderTab).toBeDefined();
            });
            it('should not have variable scope.apikeys defined', function() {
                expect(scope.apikeys).toBeUndefined();
            });
            it('should not have variable scope.error defined', function() {
                expect(scope.error).toBeUndefined();
            });
             it('should not have variable scope.Processor_ID defined', function() {
                expect(scope.Processor_ID).toBeUndefined();
            });
            it('should have function scope.integration_init defined', function() {
                expect(scope.integration_init).toBeDefined();
            });        
            it('should have function scope.integration_next defined', function() {
                expect(scope.integration_next).toBeDefined();
            });        
            it('should have function scope.changeDocument defined', function() {
                expect(scope.changeDocument).toBeDefined();
            });        
            it('should have function scope.setTitle defined', function() {
                expect(scope.setTitle).toBeDefined();
            });        
            it('should have function scope.refreshApi defined', function() {
                expect(scope.refreshApi).toBeDefined();
            });        
            it('should have function scope.copyApi defined', function() {
                expect(scope.copyApi).toBeDefined();
            });        
            it('should have function scope.copyProcessor defined', function() {
                expect(scope.copyProcessor).toBeDefined();
            });        
            it('should have function scope.initialize defined', function() {
                expect(scope.initialize).toBeDefined();
            });        
        });

        describe('Interface', function() {
            it('should recognize integration_init as a function', function() {
                expect(angular.isFunction(scope.integration_init)).toBe(true);
            });
            it('should recognize integration_next as a function', function() {
                expect(angular.isFunction(scope.integration_next)).toBe(true);
            });
            it('should recognize changeDocument as a function', function() {
                expect(angular.isFunction(scope.changeDocument)).toBe(true);
            });
            it('should recognize setTitle as a function', function() {
                expect(angular.isFunction(scope.setTitle)).toBe(true);
            });
            it('should recognize refreshApi as a function', function() {
                expect(angular.isFunction(scope.refreshApi)).toBe(true);
            });
            it('should recognize copyApi as a function', function() {
                expect(angular.isFunction(scope.copyApi)).toBe(true);
            });
            it('should recognize copyProcessor as a function', function() {
                expect(angular.isFunction(scope.copyProcessor)).toBe(true);
            });
            it('should recognize initialize as a function', function() {
                expect(angular.isFunction(scope.initialize)).toBe(true);
            });
        });

        describe('Logged in', function(){
            it('should not redirect if user is not logged in', function() {
                spyOn($location, 'path');
                expect($location.path).not.toHaveBeenCalledWith('/signin');
	    	});
        });

        describe('integration_init function', function(){
            it('should set the scope.showNationBuilderTab to the value of Authentication.user.Enable_Nation', function(){
                expect(scope.showNationBuilderTab).toBe(true);
                scope.showNationBuilderTab = false;
                scope.integration_init();
                expect(scope.showNationBuilderTab).toBe(true);
                Authentication.user.Enable_Nation = false;
                scope.integration_init();
                expect(scope.showNationBuilderTab).toBe(false);
            });
        });

        describe('integration_init function', function(){
            it('should set the scope.page and scope.templateurl based on the value of the passed parameter', function(){
                var passedParameter = 1;
                expect(scope.page).toBe(1);
                expect(scope.templateurl).toBe('');
                scope.page = 0;
                scope.integration_next(passedParameter);
                expect(scope.page).toBe(1);
                expect(scope.templateurl).toBe(scope.integration_page_1);
                
                passedParameter = 2;
                scope.page = 0;
                scope.templateurl = '';
                scope.integration_next(passedParameter);
                expect(scope.page).toBe(2);
                expect(scope.templateurl).toBe(scope.integration_page_2);

                passedParameter = undefined;
                scope.page = 0;
                scope.templateurl = '';
                scope.integration_next(passedParameter);
                expect(scope.page).toBe(1);
                expect(scope.templateurl).toBe(scope.integration_page_1);
            });
        });

        describe('changeDocument function', function(){
            it('should set the scope.integrationdocument and scope.documentpdf based on scope.pagenumber', function(){
                expect(scope.integrationdocument).toBe('modules/creditcard/views/integrationdoc1.client.view.html');
                expect(scope.documentpdf).toBe('MojoCore_API_Merchant_Api_Transactions.pdf');
                scope.pagenumber = '1';
                scope.integrationdocument = '';
                scope.documentpdf = '';
                scope.changeDocument();
                expect(scope.integrationdocument).toBe('modules/creditcard/views/integrationdoc1.client.view.html');
                expect(scope.documentpdf).toBe('MojoCore_API_Merchant_Api_Transactions.pdf');

                scope.pagenumber = '2';
                scope.integrationdocument = '';
                scope.documentpdf = '';
                scope.changeDocument();
                expect(scope.integrationdocument).toBe('modules/creditcard/views/integrationdoc2.client.view.html');
                expect(scope.documentpdf).toBe('MojoCore_API_Merchant_Api_Customer.pdf');
                
                scope.pagenumber = '3';
                scope.integrationdocument = '';
                scope.documentpdf = '';
                scope.changeDocument();
                expect(scope.integrationdocument).toBe('modules/creditcard/views/integrationdoc3.client.view.html');
                expect(scope.documentpdf).toBe('MojoCore_API_Merchant_Api_Plans.pdf');
                
                scope.pagenumber = '4';
                scope.integrationdocument = '';
                scope.documentpdf = '';
                scope.changeDocument();
                expect(scope.integrationdocument).toBe('modules/creditcard/views/integrationdoc4.client.view.html');
                expect(scope.documentpdf).toBe('MojoCore_API_Merchant_Api_Subscriptions.pdf');
                
                scope.pagenumber = '5';
                scope.integrationdocument = '';
                scope.documentpdf = '';
                scope.changeDocument();
                expect(scope.integrationdocument).toBe('modules/creditcard/views/integrationdoc5.client.view.html');
                expect(scope.documentpdf).toBe('MojoCore_API_Merchant_Api_Appendicies.pdf');
                
                scope.pagenumber = '6'; // invalid
                scope.integrationdocument = '';
                scope.documentpdf = '';
                scope.changeDocument();
                expect(scope.integrationdocument).toBe('modules/creditcard/views/integrationdoc6.client.view.html'); // does not exist
                expect(scope.documentpdf).toBe('');
                
            });
        });

		describe('setTitle function', function() {
            it('should call $broadcast on $scope.$parent.$parent',function() {
                var value = 'Foxtrot Oscar Oscar';
                var parent = scope.$parent.$parent;
                spyOn(parent, '$broadcast').and.callThrough();
                scope.setTitle(value);    		
                expect(parent.$broadcast).toHaveBeenCalledWith('setTitle', value);
                expect(parent.$broadcast.calls.mostRecent().args[0]).toEqual('setTitle');
                expect(parent.$broadcast.calls.mostRecent().args[1]).toEqual(value);
			});
        });

		describe('refreshApi function I', function() {
            it('should set scope.apikeys and leave scope.error undefined',function() {
                var apiResponse = '34258ffeed76dacbbb876daddd145686eadf908908f';
                $httpBackend.when('POST', '/apikey').respond(200, apiResponse);
                expect(scope.apikeys).toBeUndefined();
                expect(scope.error).toBeUndefined();
                scope.refreshApi();
                $httpBackend.flush();
                expect(scope.apikeys).toBe(apiResponse);
                expect(scope.error).toBeUndefined();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
        });

		describe('refreshApi function II', function() {
            it('should not set scope.apikeys but should set scope.error',function() {
                var apiResponse = '34258ffeed76dacbbb876daddd145686eadf908908f';
                $httpBackend.when('POST', '/apikey').respond(500, {message: 'Error!'});
                expect(scope.apikeys).toBeUndefined();
                expect(scope.error).toBeUndefined();
                scope.refreshApi();
                $httpBackend.flush();
                expect(scope.apikeys).toBeUndefined();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe('Error!');
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
        });

		describe('copyApi function I', function() {
			beforeEach(function(){
                this.element = $('<input readonly type="text" id="Token" value="ReadonlyInput"/><div id="themessageA" style="display:none">Message</div>');
				this.element.appendTo('body');
			});
			
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});
			
            it('should highlight the Token and display a message',function() {
                jasmine.clock().uninstall();
                jasmine.clock().install();
                spyOn(window, 'setTimeout').and.callThrough();
                expect(window.getSelection().toString()).toBe('');
                expect(document.getElementById('themessageA').style.display).toBe('none');
                scope.copyApi();
                scope.$digest();
                expect(window.getSelection().toString()).toBe("ReadonlyInput");
                expect(document.getElementById('themessageA').style.display).toBe('block');
                expect(window.setTimeout).toHaveBeenCalled();
                expect(typeof window.setTimeout.calls.mostRecent().args[0]).toEqual("function"); 
                expect(window.setTimeout.calls.mostRecent().args[1]).toEqual(2500); 
                jasmine.clock().tick(2501);
                expect(document.getElementById('themessageA').style.display).toBe('none');
                jasmine.clock().uninstall();
            });
        });

        describe('copyApi function II', function() {
			beforeEach(function(){
                this.element = $('<input readonly type="text" id="Token" value="ReadonlyInput"/><div id="themessageB" style="display:none">Message</div>');
				this.element.appendTo('body');
			});
			
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});
			
           it('should not display a message',function() {
                jasmine.clock().uninstall();
                jasmine.clock().install();
                spyOn(window, 'setTimeout').and.callThrough();
                expect(window.getSelection().toString()).toBe('');
                expect(document.getElementById('themessageA')).toBe(null);
                scope.copyApi();
                scope.$digest();
                expect(window.getSelection().toString()).toBe("ReadonlyInput");
                expect(document.getElementById('themessageA')).toBe(null);
                expect(window.setTimeout).toHaveBeenCalled();
                expect(typeof window.setTimeout.calls.mostRecent().args[0]).toEqual("function"); 
                expect(window.setTimeout.calls.mostRecent().args[1]).toEqual(2500); 
                jasmine.clock().tick(2501);
                expect(document.getElementById('themessageA')).toBe(null);
                jasmine.clock().uninstall();
            });
        });

		describe('copyProcessor I function', function() {
			beforeEach(function(){
                this.element = $('<input readonly type="text" id="Processor_ID" value="ReadonlyInput"/><div id="themessageP" style="display:none">Message</div>');
				this.element.appendTo('body');
			});
			
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});
			
            it('should highlight the ProcessorID and display a message',function() {
                jasmine.clock().uninstall(); // clear custom clock
                jasmine.clock().install();
                spyOn(window, 'setTimeout').and.callThrough();
                expect(document.getElementById('themessageP').style.display).toBe('none');
                scope.copyProcessor();
                scope.$digest();
                expect(document.getElementById('Processor_ID')).not.toBe(false);
                expect(document.getElementById('themessageP').style.display).toBe('block');
                expect(window.setTimeout).toHaveBeenCalled();
                expect(typeof window.setTimeout.calls.mostRecent().args[0]).toEqual("function") ;
                expect(window.setTimeout.calls.mostRecent().args[1]).toEqual(2500); 
                jasmine.clock().tick(2500);
                expect(document.getElementById('themessageP').style.display).toBe('none');
                jasmine.clock().uninstall();
            });
        });

		describe('copyProcessor II function', function() {
			beforeEach(function(){
                this.element = $('<input readonly type="text" id="Processor_ID" value="ReadonlyInput"/><div id="themessageQ" style="display:none">Message</div>');
				this.element.appendTo('body');
			});
			
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});
			
            it('should highlight the ProcessorID and display a message',function() {
                jasmine.clock().uninstall(); // clear custom clock
                jasmine.clock().install();
                spyOn(window, 'setTimeout').and.callThrough();
                expect(document.getElementById('themessageP')).toBe(null);
                scope.copyProcessor();
                scope.$digest();
                expect(document.getElementById('Processor_ID')).not.toBe(false);
                expect(document.getElementById('themessageP')).toBe(null);
                expect(window.setTimeout).toHaveBeenCalled();
                expect(typeof window.setTimeout.calls.mostRecent().args[0]).toEqual("function") ;
                expect(window.setTimeout.calls.mostRecent().args[1]).toEqual(2500); 
                jasmine.clock().tick(2500);
                expect(document.getElementById('themessageP')).toBe(null);
                jasmine.clock().uninstall();
            });
        });

		describe('initialize function', function() {
            var paymentDeferred,
                NmitransactionsDeferred,
                $promise;
                    
            beforeEach(function() {
                paymentDeferred = $q.defer();
                paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
            });
            
            it('should call ApikeyService.get',function() {
                $httpBackend.when('GET', 'apikey/25').respond(200, {"id":40,"Token":"03aa01640b398fbad462e4af718b2590970defe1","Is_Active":true,"Boarding_ID":25,"createdAt":"2015-04-22T09:16:57.000Z","updatedAt":"2015-04-22T09:16:57.000Z"});
                scope.initialize();
                $httpBackend.flush(1);
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should call PaymentService.get',function() {
                 $httpBackend.when('GET', 'apikey/25').respond(200, {"id":40,"Token":"03aa01640b398fbad462e4af718b2590970defe1","Is_Active":true,"Boarding_ID":25,"createdAt":"2015-04-22T09:16:57.000Z","updatedAt":"2015-04-22T09:16:57.000Z"});
                scope.initialize();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(PaymentService.get).toHaveBeenCalled();
                expect(PaymentService.get).toHaveBeenCalledWith({paymentId:25, productId:4});
            });
            it('should set scope.apikeys',function() {
                var apikeyResult = {"id":40,"Token":"03aa01640b398fbad462e4af718b2590970defe1","Is_Active":true,"Boarding_ID":25,"createdAt":"2015-04-22T09:16:57.000Z","updatedAt":"2015-04-22T09:16:57.000Z"};
                $httpBackend.when('GET', 'apikey/25').respond(200, apikeyResult);
                expect(scope.apikeys).toBeUndefined();
                scope.initialize();
                $httpBackend.flush();
                expect(scope.apikeys).toBeDefined();
            });
            it('should set scope.Processor_ID',function() {
                var apikeyResult = {"id":40,"Token":"03aa01640b398fbad462e4af718b2590970defe1","Is_Active":true,"Boarding_ID":25,"createdAt":"2015-04-22T09:16:57.000Z","updatedAt":"2015-04-22T09:16:57.000Z"};
                $httpBackend.when('GET', 'apikey/25').respond(200, apikeyResult);
                expect(scope.Processor_ID).toBeUndefined();
                scope.initialize();
                $httpBackend.flush();
                expect(scope.Processor_ID).toBeDefined();
                expect(scope.Processor_ID).toBe('processor1');
            });
        });

		describe('initialize function', function() {
            var paymentDeferred,
                NmitransactionsDeferred,
                $promise;
                    
            beforeEach(function() {
                paymentDeferred = $q.defer();
                paymentDeferred.reject("This is an error response");
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
            });
            
            it('should handle errors on PaymentService.get',function() {
                var apikeyResult = {"id":40,"Token":"03aa01640b398fbad462e4af718b2590970defe1","Is_Active":true,"Boarding_ID":25,"createdAt":"2015-04-22T09:16:57.000Z","updatedAt":"2015-04-22T09:16:57.000Z"};
                $httpBackend.when('GET', 'apikey/25').respond(200, apikeyResult);
                expect(scope.Processor_ID).toBeUndefined();
                scope.initialize();
                $httpBackend.flush();
                expect(scope.Processor_ID).toBeUndefined();
            });
        });
    });
}());


(function() {
    // Authentication controller Spec
    describe('IntegrationController II', function() {
        // Initialize global variables
        var IntegrationController,
            scope,
            $httpBackend,
			$stateParams,
            $location,
            Authentication;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_,  _Authentication_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            $location = _$location_;
            $stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;

            // Point global variables to injected services
            Authentication = _Authentication_;
            Authentication = {};
            spyOn($location, 'path');
            
            // Initialize the controller
            IntegrationController = $controller('IntegrationController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));
        
        describe('Not Logged in', function(){
            it('should  redirect if user is not logged in', function() {
                expect($location.path).toHaveBeenCalledWith('/signin');
	    	});
        });
    });
}());
