/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('tsnapshotController', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            Authentication,
            Nmitransactions,
            PaymentCompleteService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope,_$q_, _Authentication_, _Nmitransactions_, _PaymentCompleteService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };
            // Point global variables to injected services
            $q = _$q_;
            Nmitransactions= _Nmitransactions_;
            PaymentCompleteService = _PaymentCompleteService_;

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });
            it('should have scope.tsnapMonths', function() {
                expect(scope.tsnapMonths).toBeDefined();
            });
            it('should have scope.tsnapYears defined', function() {
                expect(scope.tsnapYears).toBeDefined();
            });
            it('should have scope.tsnapHours defined', function() {
                expect(scope.tsnapHours).toBeDefined();
            });
            it('should have scope.tsnapMinutes defined', function() {
                expect(scope.tsnapMinutes).toBeDefined();
            });
            it('should have scope.transSnapshotShow defined', function() {
                expect(scope.transSnapshotShow).toBeDefined();
            });
            it('should have scope.tsnapStartYear defined', function() {
                expect(scope.tsnapStartYear).toBeDefined();
            });
            it('should have scope.tsnapStartMonth defined', function() {
                expect(scope.tsnapStartMonth).toBeDefined();
            });
            it('should have scop.tsnapStartDay defined', function() {
                expect(scope.tsnapStartDay).toBeDefined();
            });
            it('should have scope.tsnapStartHour defined', function() {
                expect(scope.tsnapStartHour).toBeDefined();
            });
            it('should have scope.tsnapStartMinute defined', function() {
                expect(scope.tsnapStartMinute).toBeDefined();
            });
            it('should have scope.tsnapEndYear defined', function() {
                expect(scope.tsnapEndYear).toBeDefined();
            });
            it('should have scope.tsnapEndMonth defined', function() {
                expect(scope.tsnapEndMonth).toBeDefined();
            });
            it('should have scop.tsnapEndDay defined', function() {
                expect(scope.tsnapEndDay).toBeDefined();
            });
            it('should have scope.tsnapEndHour defined', function() {
                expect(scope.tsnapEndHour).toBeDefined();
            });
            it('should have scope.tsnapEndMinute defined', function() {
                expect(scope.tsnapEndMinute).toBeDefined();
            });
            it('should have scope.tsnapStartDate defined', function() {
                expect(scope.tsnapStartDate).toBeDefined();
            });
            it('should have scope.tsnapEndDate defined', function() {
                expect(scope.tsnapEndDate).toBeDefined();
            });
            it('should have function scope.tsnapCheckDate defined', function() {
                expect(scope.tsnapCheckDate).toBeDefined();
            });
            it('should have function scope.tsnapCheckDateRange defined', function() {
                expect(scope.tsnapCheckDateRange).toBeDefined();
            });
            it('should have function scope.transactionSnapshotWrap defined', function() {
                expect(scope.transactionSnapshotWrap).toBeDefined();
            });
        });

        describe('Interface', function() {
            it('should recognize tsnapCheckDate as a function', function() {
                expect(angular.isFunction(scope.tsnapCheckDate)).toBe(true);
            });
            it('should recognize tsnapCheckDateRange as a function', function() {
                expect(angular.isFunction(scope.tsnapCheckDateRange)).toBe(true);
            });
            it('should recognize transactionSnapshotWrap as a function', function() {
                expect(angular.isFunction(scope.transactionSnapshotWrap)).toBe(true);
            });
        });

        describe('Data types/values', function() {
            it('should recognize scope.tsnapMonths as a object, value [ "Jan" - "Dec" ]', function() {
                expect(typeof scope.tsnapMonths).toBe('object');
                expect(scope.tsnapMonths.length).toBe(12);
                expect(scope.tsnapMonths).toEqual([ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]);
            });
            it('should recognize scope.tsnapYears as a object, value []', function() {
                expect(typeof scope.tsnapYears).toBe('object');
                expect(scope.tsnapYears.length).toBe(11);
                expect(scope.tsnapYears[10]).toBe(new Date().getFullYear());
            });
            it('should recognize scope.tsnapHours as a object, value [ "00" - "23" ]', function() {
                expect(typeof scope.tsnapHours).toBe('object');
                expect(scope.tsnapHours.length).toBe(24);
                expect(scope.tsnapHours).toEqual([ '00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23' ]);
            });
            it('should recognize scope.tsnapMinutes as a object, value [ "00" - "59" ]', function() {
                expect(typeof scope.tsnapMinutes).toBe('object');
                expect(scope.tsnapMinutes.length).toBe(60);
                expect(scope.tsnapMinutes).toEqual([ '00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59' ]);
            });
            it('should recognize scope.transSnapshotShow as a boolean, value false', function() {
                expect(typeof scope.transSnapshotShow).toBe('boolean');
                expect(scope.transSnapshotShow).toBe(false);
            });
            it('should recognize scope.tsnapStartYear as a number, value "Current Year"', function() {
                expect(typeof scope.tsnapStartYear).toBe('number');
                expect(scope.tsnapStartYear).toBe(new Date().getFullYear());
            });
            it('should recognize scope.tsnapStartMonth as a string, value "Current Month"', function() {
                var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
                expect(typeof scope.tsnapStartMonth).toBe('string');
                expect(scope.tsnapStartMonth).toBe(months[new Date().getMonth()]);
            });
            it('should recognize scope.tsnapStartDay as a number, value 1', function() {
                expect(typeof scope.tsnapStartDay).toBe('number');
                expect(scope.tsnapStartDay).toBe(1);
            });
            it('should recognize scope.tsnapStartHour as a string, value 00', function() {
                expect(typeof scope.tsnapStartHour).toBe('string');
                expect(scope.tsnapStartHour).toBe('00');
            });
            it('should recognize scope.tsnapStartMinute as a string, value 00', function() {
                expect(typeof scope.tsnapStartMinute).toBe('string');
                expect(scope.tsnapStartMinute).toBe('00');
            });
            it('should recognize scope.tsnapEndYear as a number, value "Current Year"', function() {
                expect(typeof scope.tsnapEndYear).toBe('number');
                expect(scope.tsnapEndYear).toBe(new Date().getFullYear());
            });
            it('should recognize scope.tsnapEndMonth as a string, value "Current Month"', function() {
                var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
                expect(typeof scope.tsnapEndMonth).toBe('string');
                expect(scope.tsnapEndMonth).toBe(months[new Date().getMonth()]);
            });
            it('should recognize scope.tsnapEndDay as a number, value "last day of the month', function() {
                var lastDay = new Date().getDate();
                var thisMonth = new Date().getMonth(); 
                var thisYear = new Date().getFullYear();  
                
                expect(typeof scope.tsnapEndDay).toBe('number');
                expect(scope.tsnapEndDay).toBe(lastDay);
            });
            it('should recognize scope.tsnapEndHour as a string, value 00', function() {
                expect(typeof scope.tsnapEndHour).toBe('string');
                expect(scope.tsnapEndHour).toBe('23');
            });
            it('should recognize scope.tsnapEndMinute as a string, value 00', function() {
                expect(typeof scope.tsnapEndMinute).toBe('string');
                expect(scope.tsnapEndMinute).toBe('59');
            });
            it('should recognize scope.tsnapStartDate as a string, value blank', function() {
                expect(typeof scope.tsnapStartDate).toBe('string');
                expect(scope.tsnapStartDate).toBe('');
            });
            it('should recognize scope.tsnapEndDate as a string, value blank', function() {
                expect(typeof scope.tsnapEndDate).toBe('string');
                expect(scope.tsnapEndDate).toBe('');
            });
        });

        describe('scope.tsnapCheckDate function', function(){
            it('should create a valid date if passed valid data', function() {
                expect(scope.tsnapCheckDate(2015, 8, 23, 4, 55, 0)).toEqual(new Date(2015, 8, 23, 4, 55, 0)); 
            });
            it('should return "Invalid Date" if passed invalid data', function() {
                expect(scope.tsnapCheckDate(15, 8, 23, 4, 55, 0)).toEqual('Invalid Date'); // year
                expect(scope.tsnapCheckDate(2015, 12, 23, 4, 55, 0)).toEqual('Invalid Date'); // 13/23/2015
                expect(scope.tsnapCheckDate(2015, 1, 30, 4, 55, 0)).toEqual('Invalid Date'); // feb 30 
                expect(scope.tsnapCheckDate(2015, 8, 31, 4, 55, 0)).toEqual('Invalid Date'); // Sep 31
                expect(scope.tsnapCheckDate(2015, 8, 23, 24, 55, 0)).toEqual('Invalid Date'); // 24:55:00
                expect(scope.tsnapCheckDate(15, 8, 23, 4, 60, 0)).toEqual('Invalid Date'); // 04:60:00
                expect(scope.tsnapCheckDate(15, 8, 23, 4, 55, 60)).toEqual('Invalid Date'); // 04:55:60
            });
        });

        describe('scope.tsnapCheckDateRange function', function(){
            it('should define scope.alerts', function() {
                expect(scope.alerts).toBeUndefined();
                scope.tsnapCheckDateRange();
                scope.$digest();
                expect(scope.alerts).toBeDefined();
                expect(scope.alerts).toEqual([]);
            });
            it('should not set an error if start date, end date, and date range is valid', function() {
                expect(scope.alerts).toBeUndefined();
                scope.tsnapStartYear = 2015;
                scope.tsnapStartMonth = 'Sep';
                scope.tsnapStartDay = 23;
                scope.tsnapStartHour = 4;
                scope.tsnapStartMinute = 55;
                scope.tsnapEndYear = '2015';
                scope.tsnapEndMonth = 'Oct';
                scope.tsnapEndDay = '31';
                scope.tsnapEndHour = '4';
                scope.tsnapEndMinute = '55';
                scope.tsnapCheckDateRange();
                scope.$digest();
                expect(scope.alerts).toBeDefined();
                expect(scope.alerts).toEqual([]);
            });
            it('should set an error if start date is invalid', function() {
                expect(scope.alerts).toBeUndefined();
                scope.tsnapStartYear = 2015;
                scope.tsnapStartMonth = 'Sep';
                scope.tsnapStartDay = 31;
                scope.tsnapStartHour = 4;
                scope.tsnapStartMinute = 55;
                scope.tsnapEndYear = '2015';
                scope.tsnapEndMonth = 'Oct';
                scope.tsnapEndDay = '31';
                scope.tsnapEndHour = '4';
                scope.tsnapEndMinute = '55';
                scope.tsnapCheckDateRange();
                scope.$digest();
                expect(scope.alerts).toBeDefined();
                expect(scope.alerts).not.toEqual([]);
                expect(scope.alerts).toEqual([{ type: 'danger', msg: 'Start date is not a proper date' }]);
            });
            it('should set an error if end date is invalid', function() {
                expect(scope.alerts).toBeUndefined();
                scope.tsnapStartYear = 2015;
                scope.tsnapStartMonth = 'Sep';
                scope.tsnapStartDay = 23;
                scope.tsnapStartHour = 4;
                scope.tsnapStartMinute = 55;
                scope.tsnapEndYear = '2015';
                scope.tsnapEndMonth = 'Sep';
                scope.tsnapEndDay = '31';
                scope.tsnapEndHour = '4';
                scope.tsnapEndMinute = '55';
                scope.tsnapCheckDateRange();
                scope.$digest();
                expect(scope.alerts).toBeDefined();
                expect(scope.alerts).not.toEqual([]);
                expect(scope.alerts).toEqual([{ type: 'danger', msg: 'End date is not a proper date' }]);
            });
            it('should set an error if end date is earlier than start date', function() {
                expect(scope.alerts).toBeUndefined();
                scope.tsnapStartYear = 2015;
                scope.tsnapStartMonth = 'Oct';
                scope.tsnapStartDay = 23;
                scope.tsnapStartHour = 4;
                scope.tsnapStartMinute = 55;
                scope.tsnapEndYear = '2015';
                scope.tsnapEndMonth = 'Sep';
                scope.tsnapEndDay = '30';
                scope.tsnapEndHour = '4';
                scope.tsnapEndMinute = '55';
                scope.tsnapCheckDateRange();
                scope.$digest();
                expect(scope.alerts).toBeDefined();
                expect(scope.alerts).not.toEqual([]);
                expect(scope.alerts).toEqual([{ type: 'danger', msg: 'Please select the correct date range' }]);
            });
        });

        describe('scope.transactionSnapshotWrap function I ', function(){
            beforeEach(function() {
                scope.products = {productName: '4'};
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body">');
				this.element.appendTo('body');
                
                var Nmitransactionsdefer,
                    paymentCompletedefer,
                    $promise;
                
                Nmitransactionsdefer = $q.defer();
                Nmitransactionsdefer.resolve({test:"test", nm_response: {}});
                
                paymentCompletedefer = $q.defer();
                paymentCompletedefer.resolve({response:"test"});
                
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: Nmitransactionsdefer.promise}); //returns a fake promise;
                spyOn(PaymentCompleteService, 'get').and.returnValue({$promise: paymentCompletedefer.promise}); //returns a fake promise;
                spyOn(scope,'initPaginator').and.callThrough();
                
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should run the report if scope.alerts is defined, but empty', function() {
                scope.alerts = [];
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.initPaginator).toHaveBeenCalled();
            });
            it('should  run the report if scope.alerts is Undefined', function() {
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.initPaginator).toHaveBeenCalled();
            });
            it('should not run the report if there are alerts present', function() {
                scope.alerts = [{test:"test"}];
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.initPaginator).not.toHaveBeenCalled();
            });
            it('should call initPaginator', function() {
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.initPaginator).toHaveBeenCalled();
            });
            it('should set scope.itemsTransSnapshot', function() {
                expect(scope.itemsTransSnapsho).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.itemsTransSnapshot).toBeDefined();
                expect(scope.itemsTransSnapshot).toEqual([]);
            });
            it('should set scope.totalsTransSnapshot', function() {
                expect(scope.totalsTransSnapshot ).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.totalsTransSnapshot ).toBeDefined();
                expect(scope.totalsTransSnapshot ).toEqual({ merchant_name: 'Totals', authorizations_count: 0, authorizations_amount: 0, sale_count: 0, sale_amount: 0, refunds_count: 0, refunds_amount: 0, returns_count: 0, returns_amount: 0, voids_count: 0, voids_amount: 0, declines_count: 0, declines_amount: 0, totals_count: 0, totals_amount: 0 });
            });
            it('should set scope.reportProcesed', function() {
                expect(scope.reportProcesed).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.reportProcesed).toBeDefined();
                expect(scope.reportProcesed).toBe(true);
            });
            it('should set scope.noResults ', function() {
                expect(scope.noResults ).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.noResults ).toBeDefined();
                expect(scope.noResults ).toBe(true);
            });
            it('should display the spinner image and hide the report body', function() {
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('report-body').style.display).toBe('');
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('report-body').style.display).toBe('block');
            });
        });

        describe('scope.transactionSnapshotWrap function II - NmiTransactions.get returns no transaction records', function(){
            beforeEach(function() {
                scope.products = {productName: '4'};
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body">');
				this.element.appendTo('body');
                
                var Nmitransactionsdefer,
                    paymentCompletedefer,
                    $promise;
                
                Nmitransactionsdefer = $q.defer();
                Nmitransactionsdefer.resolve({ test:"test",test1:"test1","nm_response":{}});
                
                paymentCompletedefer = $q.defer();
                paymentCompletedefer.resolve({response:"test"});
                
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: Nmitransactionsdefer.promise}); //returns a fake promise;
                spyOn(PaymentCompleteService, 'get').and.returnValue({$promise: paymentCompletedefer.promise}); //returns a fake promise;
                spyOn(scope,'initPaginator').and.callThrough();
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should not build scope.itemsTransSnapshot if Nmitransactions.get does not find transactions', function() {
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.itemsTransSnapshot).toEqual([]);
            });
            it('should set scope.reportProcesed', function() {
                expect(scope.reportProcesed).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.reportProcesed).toBeDefined();
                expect(scope.reportProcesed).toBe(true);
            });
            it('should set scope.noResults', function() {
                expect(scope.noResults).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.noResults).toBeDefined();
                expect(scope.noResults).toBe(true);
            });
            it('should set scope.transSnapshotShow', function() {
                expect(scope.transSnapshotShow).toBe(false);
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.transSnapshotShow).toBeDefined();
                expect(scope.transSnapshotShow).toBe(false);
            });
            it('should display the spinner image and hide the report body', function() {
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('report-body').style.display).toBe('');
                scope.transactionSnapshotWrap();
                expect(document.getElementById('loadingImg').style.display).toBe('block');
                expect(document.getElementById('report-body').style.display).toBe('none');
            });
        });

        describe('scope.transactionSnapshotWrap function III - NmiTransactions.get returns an error', function(){
            beforeEach(function() {
                scope.products = {productName: '4'};
                this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body">');
				this.element.appendTo('body');
                
                var Nmitransactionsdefer,
                    paymentCompletedefer,
                    $promise;
                
                Nmitransactionsdefer = $q.defer();
                Nmitransactionsdefer.reject('This is an error response');
                
                paymentCompletedefer = $q.defer();
                paymentCompletedefer.resolve({response:"test"});
                
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: Nmitransactionsdefer.promise}); //returns a fake promise;
                spyOn(PaymentCompleteService, 'get').and.returnValue({$promise: paymentCompletedefer.promise}); //returns a fake promise;
                spyOn(scope,'initPaginator').and.callThrough();
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

           it('should display an error if Nmitransactions.get returns an error', function() {
                spyOn(console,'log').and.callThrough();
                expect(scope.reportProcesed).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.reportProcesed).toBeDefined();
                expect(console.log).toHaveBeenCalledWith('This is an error response');
            });
            it('should set scope.reportProcesed', function() {
                expect(scope.reportProcesed).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.reportProcesed).toBeDefined();
                expect(scope.reportProcesed).toBe(false);
            });
            it('should set scope.noResults ', function() {
                expect(scope.noResults ).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.noResults ).toBeDefined();
                expect(scope.noResults ).toBe(false);
            });
            it('should display the spinner image and hide the report body', function() {
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('report-body').style.display).toBe('');
                scope.transactionSnapshotWrap();
                expect(document.getElementById('loadingImg').style.display).toBe('block');
                expect(document.getElementById('report-body').style.display).toBe('none');
            });
       });
    
        describe('scope.transactionSnapshotWrap function IV  - NmiTransactions.get returns some transaction records', function(){
            beforeEach(function() {
                scope.products = {productName: '4'};
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body">');
				this.element.appendTo('body');
                
                var NmitransactionsDeferred,
                    paymentCompletedefer,
                    $promise;
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({test:"test", test1: "test1", "nm_response":{"transaction":[
                {"transaction_id":{"$t":"2835737551"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"authorization_code":{"$t":"123456"},"first_name":{"$t":"phil"},"last_name":{"$t":"test10012015"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"cc_number":{"$t":"4xxxxxxxxxxx1111"},"cc_hash":{"$t":"f6c609e195d9d4c185dcc8ca662f0180"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"processor_id":{"$t":"processor1"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{"$t":"Demo Account1"},"merchant_id":{"$t":"1"},"boarding_id":{"$t":25},"action":{"amount":{"$t":"5.00"},"action_type":{"$t":"sale"},"date":{"$t":"20151001103042"},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"}}},
                {"transaction_id":{"$t":"2841708559"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"authorization_code":{"$t":"123456"},"first_name":{"$t":"phil"},"last_name":{"$t":"testtransaction"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"cc_number":{"$t":"4xxxxxxxxxxx1111"},"cc_hash":{"$t":"f6c609e195d9d4c185dcc8ca662f0180"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"processor_id":{"$t":"processor1"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{"$t":"Demo Account2"},"merchant_id":{"$t":"1"},"boarding_id":{"$t":25},"action":{"amount":{"$t":"1.00"},"action_type":{"$t":"refund"},"date":{"$t":"20151006131318"},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"}}},
                {"transaction_id":{"$t":"2850078903"},"transaction_type":{"$t":"cc"},"condition":{"$t":"pending"},"authorization_code":{"$t":"123456"},"first_name":{"$t":"brennan"},"last_name":{"$t":"harvey"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"77777"},"country":{"$t":"-"},"email":{"$t":"-"},"cc_number":{"$t":"4xxxxxxxxxxx1111"},"cc_hash":{"$t":"f6c609e195d9d4c185dcc8ca662f0180"},"cc_exp":{"$t":"1020"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"Z"},"csc_response":{"$t":"-"},"processor_id":{"$t":"processor1"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{"$t":"Demo Account"},"merchant_id":{"$t":"1"},"boarding_id":{"$t":25},"action":{"amount":{"$t":"10.00"},"action_type":{"$t":"auth"},"date":{"$t":"20151014095344"},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"}}},
                {"transaction_id":{"$t":"2855752411"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"authorization_code":{"$t":"123456"},"first_name":{"$t":"phil"},"last_name":{"$t":"amextest"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"cc_number":{"$t":"3xxxxxxxxxx0005"},"cc_hash":{"$t":"f77ee91c020fe4767c4f21c829eed187"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"processor_id":{"$t":"processor1"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{"$t":"Demo Account"},"merchant_id":{"$t":"1"},"boarding_id":{"$t":25},"action":{"amount":{"$t":"1.00"},"action_type":{"$t":"sale"},"date":{"$t":"20151019112317"},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"200"}}},
                {"transaction_id":{"$t":"2855752496"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"authorization_code":{"$t":"123456"},"first_name":{"$t":"phil"},"last_name":{"$t":"amextest"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"cc_number":{"$t":"3xxxxxxxxxx0005"},"cc_hash":{"$t":"f77ee91c020fe4767c4f21c829eed187"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"processor_id":{"$t":"processor1"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{"$t":"Demo Account"},"merchant_id":{"$t":"1"},"boarding_id":{"$t":25},"action":{"amount":{"$t":"1.00"},"action_type":{"$t":"credit"},"date":{"$t":"20151019112317"},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"}}},
                {"transaction_id":{"$t":"2850078903"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"authorization_code":{"$t":"123456"},"first_name":{"$t":"phil"},"last_name":{"$t":"amextest"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"cc_number":{"$t":"3xxxxxxxxxx0005"},"cc_hash":{"$t":"f77ee91c020fe4767c4f21c829eed187"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"processor_id":{"$t":"processor1"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{"$t":"Demo Account"},"merchant_id":{"$t":"1"},"boarding_id":{"$t":25},"action":{"amount":{"$t":"1.00"},"action_type":{"$t":"capture"},"date":{"$t":"20151019112317"},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"}}},
                {"transaction_id":{"$t":"2850078903"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"authorization_code":{"$t":"123456"},"first_name":{"$t":"phil"},"last_name":{"$t":"amextest"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"cc_number":{"$t":"3xxxxxxxxxx0005"},"cc_hash":{"$t":"f77ee91c020fe4767c4f21c829eed187"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"processor_id":{"$t":"processor1"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{"$t":"Demo Account"},"merchant_id":{"$t":"1"},"boarding_id":{"$t":25},"action":{"amount":{"$t":"1.00"},"action_type":{"$t":"void"},"date":{"$t":"20151019112317"},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"}}},
                {"transaction_id":{"$t":"2581076676"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"authorization_code":{"$t":"123456"},"first_name":{"$t":"phil"},"last_name":{"$t":"amextest"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"cc_number":{"$t":"3xxxxxxxxxx0005"},"cc_hash":{"$t":"f77ee91c020fe4767c4f21c829eed187"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"processor_id":{"$t":"processor1"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{"$t":"Demo Account"},"merchant_id":{"$t":"1"},"boarding_id":{"$t":25},"action":{"amount":{"$t":"1.00"},"action_type":{"$t":"capture"},"date":{"$t":"20151019112317"},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"}}},
                {"transaction_id":{"$t":"2581076676"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"authorization_code":{"$t":"123456"},"first_name":{"$t":"phil"},"last_name":{"$t":"amextest"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"cc_number":{"$t":"3xxxxxxxxxx0005"},"cc_hash":{"$t":"f77ee91c020fe4767c4f21c829eed187"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"processor_id":{"$t":"processor1"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{"$t":"Demo Account"},"merchant_id":{"$t":"1"},"boarding_id":{"$t":25},"action":{"amount":{"$t":"1.00"},"action_type":{"$t":"auth"},"date":{"$t":"20151019112317"},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"}}},
                {"transaction_id":{"$t":"2850076676"},"transaction_type":{"$t":"cc"},"condition":{"$t":"complete"},"authorization_code":{"$t":"123456"},"first_name":{"$t":"phil"},"last_name":{"$t":"amextest"},"address_1":{"$t":"-"},"address_2":{"$t":"-"},"company":{"$t":"-"},"city":{"$t":"-"},"state":{"$t":"-"},"postal_code":{"$t":"92677"},"country":{"$t":"US"},"email":{"$t":"-"},"cc_number":{"$t":"3xxxxxxxxxx0005"},"cc_hash":{"$t":"f77ee91c020fe4767c4f21c829eed187"},"cc_exp":{"$t":"1025"},"cavv":{"$t":"-"},"cavv_result":{"$t":"-"},"xid":{"$t":"-"},"avs_response":{"$t":"N"},"csc_response":{"$t":"N"},"processor_id":{"$t":"processor1"},"currency":{"$t":"USD"},"surcharge":{"$t":"-"},"tip":{"$t":"-"},"card_balance":{},"card_available_balance":{},"entry_mode":{},"original_transaction_id":{},"cc_bin":{"$t":"411111"},"merchant_name":{"$t":"Demo Account"},"merchant_id":{"$t":"1"},"boarding_id":{"$t":25},"action":{"amount":{"$t":"1.00"},"action_type":{"$t":"settle"},"date":{"$t":"20151019112317"},"success":{"$t":"1"},"ip_address":{"$t":"23.253.225.245"},"source":{"$t":"api"},"response_text":{"$t":"SUCCESS"},"batch_id":{"$t":"0"},"processor_batch_id":{"$t":"-"},"response_code":{"$t":"100"}}}
                ]}});
               
                paymentCompletedefer = $q.defer();
                paymentCompletedefer.resolve({response:"test"});
                
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise, $resolved: {}}); //returns a fake promise;
                spyOn(PaymentCompleteService, 'get').and.returnValue({$promise: paymentCompletedefer.promise}); //returns a fake promise;
                spyOn(scope,'initPaginator').and.callThrough();
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

           it('should call Nmitransactions.get', function() {
                expect(scope.reportProcessorMessage).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(Nmitransactions.get).toHaveBeenCalled();

            });
            
            it('should set scope.itemsTransSnapshot', function() {
                expect(scope.itemsTransSnapshot).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.itemsTransSnapshot).toBeDefined();
                expect(scope.itemsTransSnapshot).toEqual([{ merchant_name: 'Demo Account', boarding_id: 25, authorizations_count: 0, authorizations_amount: 0, authorizations_id: [ '2850078903' ], authorizations_amt: [ '10.00' ], sale_count: 2, sale_amount: 2, captures_id: [ '2850078903', '2581076676' ], captures_amt: [ '1.00', '1.00' ], refunds_count: 0, refunds_amount: 0, returns_count: 1, returns_amount: 1, voids_count: 1, voids_amount: -1, declines_count: 1, declines_amount: 1, totals_count: 5, totals_amount: 3 }, { merchant_name: 'Demo Account1', boarding_id: 25, authorizations_count: 0, authorizations_amount: 0, authorizations_id: [  ], authorizations_amt: [  ], sale_count: 1, sale_amount: 5, captures_id: [  ], captures_amt: [  ], refunds_count: 0, refunds_amount: 0, returns_count: 0, returns_amount: 0, voids_count: 0, voids_amount: 0, declines_count: 0, declines_amount: 0, totals_count: 1, totals_amount: 5 }, { merchant_name: 'Demo Account2', boarding_id: 25, authorizations_count: 0, authorizations_amount: 0, authorizations_id: [  ], authorizations_amt: [  ], sale_count: 0, sale_amount: 0, captures_id: [  ], captures_amt: [  ], refunds_count: 1, refunds_amount: 1, returns_count: 0, returns_amount: 0, voids_count: 0, voids_amount: 0, declines_count: 0, declines_amount: 0, totals_count: 1, totals_amount: 1 }]);
            });
            
            
            it('should set scope.reportProcessorMessage', function() {
                expect(scope.reportProcessorMessage).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.reportProcessorMessage).toBeDefined();
                expect(scope.reportProcessorMessage).toBe('');
            });
            it('should set scope.reportProcesed', function() {
                expect(scope.reportProcesed).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.reportProcesed).toBeDefined();
                expect(scope.reportProcesed).toBe(true);
            });
            it('should set scope.noResults ', function() {
                expect(scope.noResults ).toBeUndefined();
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(scope.noResults ).toBeDefined();
                expect(scope.noResults ).toBe(false);
            });
            it('should display the spinner image and hide the report body', function() {
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('report-body').style.display).toBe('');
                scope.transactionSnapshotWrap();
                expect(document.getElementById('loadingImg').style.display).toBe('block');
                expect(document.getElementById('report-body').style.display).toBe('none');
            });
        });

        describe('scope.transactionSnapshotWrap function III - NmiTransactions.get returns an error', function(){
            beforeEach(function() {
                scope.products = {productName: 'all'};
                this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><div id="report-body">');
				this.element.appendTo('body');
                
                var Nmitransactionsdefer,
                    paymentCompletedefer,
                    $promise;
                
                Nmitransactionsdefer = $q.defer();
                Nmitransactionsdefer.resolve({});
                
                paymentCompletedefer = $q.defer();
                paymentCompletedefer.resolve({response:"test"});
                
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: Nmitransactionsdefer.promise}); //returns a fake promise;
                spyOn(PaymentCompleteService, 'get').and.returnValue({$promise: paymentCompletedefer.promise}); //returns a fake promise;
                spyOn(scope,'initPaginator').and.callThrough();
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

           it('should call NMITransactions with treport_all', function() {
                scope.transactionSnapshotWrap();
                scope.$digest();
                expect(Nmitransactions.get).toHaveBeenCalled();
                expect(Nmitransactions.get.calls.argsFor(0)[0]).toEqual(jasmine.objectContaining({type : 'treport_all'}));
            });
    
       });

    });
}());