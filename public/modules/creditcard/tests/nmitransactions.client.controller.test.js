/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('SaleController', function() {
        // Initialize global variables
        var $q,
            SaleController,
            scope,
            $location,
            $modal,
            $httpBackend,
            $anchorScroll,
            $parse,
            $filter,
            PaymentService,
            Nmitransactions,
            Authentication,
            TransactionsService,
            ProcessorService,
            VaultService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_,_$modal_,_$anchorScroll_,_PaymentService_,_Nmitransactions_,_$parse_,_$filter_,_Authentication_,_TransactionsService_,_ProcessorService_,_$httpBackend_,_VaultService_,_$q_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            $location = _$location_;
            $q = _$q_;
            $modal = _$modal_;
            $anchorScroll = _$anchorScroll_;
            $parse = _$parse_;
            $filter = _$filter_;
            $httpBackend = _$httpBackend_;

            // Point global variables to injected services
            PaymentService = _PaymentService_;
            Nmitransactions= _Nmitransactions_;
            TransactionsService = _TransactionsService_;
            ProcessorService = _ProcessorService_;
            VaultService = _VaultService_;
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25,
                    Items: [{id:123},{id:234}]
                }
            };
            

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
            it('should have scope.x defined', function() {
                expect(scope.x).toBeDefined();
            });        
            it('should have variable scope.Product_ID defined', function() {
                expect(scope.Product_ID).toBeDefined();
            });        
            it('should have function scope.itemsView defined', function() {
                expect(scope.itemsView).toBeDefined();
            });
            it('should have function scope.cleanUser defined', function() {
                expect(scope.cleanUser).toBeDefined();
            });        
            it('should have function scope.cleanSmallForm defined', function() {
                expect(scope.cleanSmallForm).toBeDefined();
            });        
            it('should have function scope.cleanForm defined', function() {
                expect(scope.cleanForm).toBeDefined();
            });        
            it('should have function scope.doCreate defined', function() {
                expect(scope.doCreate).toBeDefined();
            });        
            it('should have function scope.doSale defined', function() {
                expect(scope.doSale).toBeDefined();
            });        
            it('should have function scope.doAuthorize defined', function() {
                expect(scope.doAuthorize).toBeDefined();
            });        
            it('should have function scope.doCredit defined', function() {
                expect(scope.doCredit).toBeDefined();
            });        
            it('should have function scope.doCapture defined', function() {
                expect(scope.doCapture).toBeDefined();
            });        
            it('should have function scope.doRefund defined', function() {
                expect(scope.doRefund).toBeDefined();
            });        
            it('should have function scope.doVoid defined', function() {
                expect(scope.doVoid).toBeDefined();
            });        
            it('should have function scope.updateItemTotal defined', function() {
                expect(scope.updateItemTotal).toBeDefined();
            });        
            it('should have function scope.roundNumbers defined', function() {
                expect(scope.roundNumbers).toBeDefined();
            });        
            it('should have function scope.setAmount defined', function() {
                expect(scope.setAmount).toBeDefined();
            });        
            it('should have function scope.addRow defined', function() {
                expect(scope.addRow).toBeDefined();
            });        
            it('should have function scope.removeRow defined', function() {
                expect(scope.removeRow).toBeDefined();
            });        
            it('should have function scope.setTitle defined', function() {
                expect(scope.setTitle).toBeDefined();
            });        
            it('should have function scope.initialize defined', function() {
                expect(scope.initialize).toBeDefined();
            });        
            it('should have function scope.confirmationSubModal defined', function() {
                expect(scope.confirmationSubModal).toBeDefined();
            });        
        });

        describe('Interface', function() {
            it('should recognize cleanUser as a function', function() {
                expect(angular.isFunction(scope.cleanUser)).toBe(true);
            });
            it('should recognize cleanSmallForm as a function', function() {
                expect(angular.isFunction(scope.cleanSmallForm)).toBe(true);
            });
            it('should recognize cleanForm as a function', function() {
                expect(angular.isFunction(scope.cleanForm)).toBe(true);
            });
            it('should recognize doCreate as a function', function() {
                expect(angular.isFunction(scope.doCreate)).toBe(true);
            });
            it('should recognize doSale as a function', function() {
                expect(angular.isFunction(scope.doSale)).toBe(true);
            });
            it('should recognize doAuthorize as a function', function() {
                expect(angular.isFunction(scope.doAuthorize)).toBe(true);
            });
            it('should recognize doCredit as a function', function() {
                expect(angular.isFunction(scope.doCredit)).toBe(true);
            });
            it('should recognize doCapture as a function', function() {
                expect(angular.isFunction(scope.doCapture)).toBe(true);
            });
            it('should recognize doRefund as a function', function() {
                expect(angular.isFunction(scope.doRefund)).toBe(true);
            });
            it('should recognize doVoid as a function', function() {
                expect(angular.isFunction(scope.doVoid)).toBe(true);
            });
            it('should recognize updateItemTotal as a function', function() {
                expect(angular.isFunction(scope.updateItemTotal)).toBe(true);
            });
            it('should recognize roundNumbers as a function', function() {
                expect(angular.isFunction(scope.roundNumbers)).toBe(true);
            });
            it('should recognize setAmount as a function', function() {
                expect(angular.isFunction(scope.setAmount)).toBe(true);
            });
            it('should recognize addRow as a function', function() {
                expect(angular.isFunction(scope.addRow)).toBe(true);
            });
            it('should recognize removeRow as a function', function() {
                expect(angular.isFunction(scope.removeRow)).toBe(true);
            });
            it('should recognize setTitle as a function', function() {
                expect(angular.isFunction(scope.setTitle)).toBe(true);
            });
            it('should recognize initialize as a function', function() {
                expect(angular.isFunction(scope.initialize)).toBe(true);
            });
            it('should recognize confirmationSubModal as a function', function() {
                expect(angular.isFunction(scope.confirmationSubModal)).toBe(true);
            });
        });

        describe('Initial Values', function() {
            it('should have initial value of variable scope.x set', function() {
                expect(scope.x).toBe(0);
            });        
            it('should have  initial value of variable scope.Product_ID set', function() {
                expect(scope.Product_ID).toBe(2);
            });        
            it('should have initial value of variable scope.itemsView set', function() {
                expect(scope.itemsView).toEqual([ { id: 123 }, { id: 234 } ]);
            });        
        });

        describe('cleanUser function', function() {
            it('should clear form variables', function() {
                spyOn(scope,'verifyCountryCard').and.returnValue(true);
                spyOn(scope,'verifyCountryShipping').and.returnValue(true);
                scope.customerVaultId = '1234657890';
                scope.billingId = '654321';
                scope.creditCardNumber = '4111111111111111';
                scope.expirationDate = '11/21';
                scope.currency = 'ABC';
                scope.firstNameCard = 'fName';
                scope.lastNameCard = 'lName';
                scope.companyCard = 'cName';
                scope.countryCard = 'XX';
                scope.addressCard = '123 Main Street';
                scope.addressContCard = 'suite 250';
                scope.cityCard = 'Anytown';
                scope.stateProvinceCard = 'CA';
                scope.zipPostalCodeCard = '99999';
                scope.phoneNumberCard = '714-555-1212';
                scope.faxNumberCard = '949-555-2323';
                scope.emailAddressCard = 'email@email.com';
                scope.websiteAddressCard = 'www.host.com';
                scope.shippingId = '123';
                scope.firstNameShipping = 'fNameShip';
                scope.lastNameShipping = 'lNameShip';
                scope.companyShipping = 'cNameShip';
                scope.countryShipping = 'SU';
                scope.addressShipping = '456 1st Street';
                scope.addressContShipping = 'Box 9';
                scope.cityShipping = 'Everytown';
                scope.stateProvinceShipping = 'CO';
                scope.zipPostalCodeShipping = '88888';
                scope.emailAddressShipping = 'emailShip@email.com';
                
                scope.cleanUser();
                expect(scope.customerVaultId).toBe('');
                expect(scope.billingId).toBe('');
                expect(scope.creditCardNumber).toBe('');
                expect(scope.expirationDate).toBe('');
                expect(scope.currency).toBe('USD');
                expect(scope.firstNameCard).toBe('');
                expect(scope.lastNameCard).toBe('');
                expect(scope.companyCard).toBe('');
                expect(scope.countryCard).toBe('US');
                expect(scope.addressCard).toBe('');
                expect(scope.addressContCard).toBe('');
                expect(scope.cityCard).toBe('');
                expect(scope.stateProvinceCard).toBe('');
                expect(scope.zipPostalCodeCard).toBe('');
                expect(scope.phoneNumberCard).toBe('');
                expect(scope.faxNumberCard).toBe('');
                expect(scope.websiteAddressCard).toBe('');
                expect(scope.emailAddressCard).toBe('');
                expect(scope.shippingId).toBe('');
                expect(scope.firstNameShipping).toBe('');
                expect(scope.lastNameShipping).toBe('');
                expect(scope.companyShipping).toBe('');
                expect(scope.countryShipping).toBe('US');
                expect(scope.addressShipping).toBe('');
                expect(scope.addressContShipping).toBe('');
                expect(scope.cityShipping).toBe('');
                expect(scope.stateProvinceShipping).toBe('');
                expect(scope.zipPostalCodeShipping).toBe('');
                expect(scope.emailAddressShipping).toBe('');
                expect(scope.verifyCountryCard).toHaveBeenCalledWith();
                expect(scope.verifyCountryShipping).toHaveBeenCalledWith();    
            });
        });

        describe('cleanSmallForm function', function() {
            it('should clear small form variables', function() {
                scope.transactionsId = '1234567890';
                scope.amount = 10.10;
                scope.TransError = 'This is an error';
                
                scope.cleanSmallForm();
                expect(scope.transactionsId).toBe('');
                expect(scope.amount).toBe('');
                expect(scope.TransError).toBe('');
            });
        });
       
        describe('cleanForm function', function() {
            it('should clear small form variables', function() {
                spyOn(scope,'verifyCountryCard').and.returnValue(true);
                spyOn(scope,'verifyCountryShipping').and.returnValue(true);
                scope.creditCardNumber = '4111111111111111';
                scope.expirationDate = '11/21';
                scope.amount = 10.99;
                scope.currency = 'XYZ';
                scope.cardSecurityCode = '123';
                scope.orderId = '123465789';
                scope.orderDescription = 'theOrderDesc';
                scope.poNumber = '13579';
                scope.shipping = 5.55;
                scope.tax = 1.99;
                scope.taxExempt = false;
                scope.firstNameCard = 'fName';
                scope.lastNameCard = 'lName';
                scope.companyCard = 'cName';
                scope.countryCard = 'XX';
                scope.addressCard = '123 Main Street';
                scope.addressContCard = 'suite 250';
                scope.cityCard = 'Anytown';
                scope.stateProvinceCard = 'CA';
                scope.zipPostalCodeCard = '99999';
                scope.phoneNumberCard = '714-555-1212';
                scope.faxNumberCard = '949-555-2323';
                scope.emailAddressCard = 'email@email.com';
                scope.websiteAddressCard = 'www.host.com';
                scope.firstNameShipping = 'fNameShip';
                scope.lastNameShipping = 'lNameShip';
                scope.companyShipping = 'cNameShip';
                scope.countryShipping = 'SU';
                scope.addressShipping = '456 1st Street';
                scope.addressContShipping = 'Box 9';
                scope.cityShipping = 'Everytown';
                scope.stateProvinceShipping = 'CO';
                scope.zipPostalCodeShipping = '88888';
                scope.emailAddressShipping = 'emailShip@email.com';

                scope.cleanForm();
                expect(scope.creditCardNumber).toBe('');
                expect(scope.expirationDate).toBe('');
                expect(scope.amount).toBe('');
                expect(scope.currency).toBe('USD');
                expect(scope.cardSecurityCode).toBe('');
                expect(scope.orderId).toBe('');
                expect(scope.orderDescription).toBe('');
                expect(scope.poNumber).toBe('');
                expect(scope.shipping).toBe('');
                expect(scope.tax).toBe('');
                expect(scope.taxExempt).toBe('');
                expect(scope.firstNameCard).toBe('');
                expect(scope.lastNameCard).toBe('');
                expect(scope.companyCard).toBe('');
                expect(scope.countryCard).toBe('US');
                expect(scope.addressCard).toBe('');
                expect(scope.addressContCard).toBe('');
                expect(scope.cityCard).toBe('');
                expect(scope.stateProvinceCard).toBe('');
                expect(scope.zipPostalCodeCard).toBe('');
                expect(scope.phoneNumberCard).toBe('');
                expect(scope.faxNumberCard).toBe('');
                expect(scope.emailAddressCard).toBe('');
                expect(scope.websiteAddressCard).toBe('');
                expect(scope.firstNameShipping).toBe('');
                expect(scope.lastNameShipping).toBe('');
                expect(scope.companyShipping).toBe('');
                expect(scope.countryShipping).toBe('US');
                expect(scope.addressShipping).toBe('');
                expect(scope.addressContShipping).toBe('');
                expect(scope.cityShipping).toBe('');
                expect(scope.stateProvinceShipping).toBe('');
                expect(scope.zipPostalCodeShipping).toBe('');
                expect(scope.emailAddressShipping).toBe('');
                expect(scope.verifyCountryCard).toHaveBeenCalledWith();
                expect(scope.verifyCountryShipping).toHaveBeenCalledWith();    
                
            });
        });
        
        describe('doCreate function', function() {
       
            describe('doCreate I function - scope.checkVaultCreate()===true, successes', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                    spyOn(scope,'confirmationSubModal').and.returnValue(true);
    
                    var paymentDeferred,
                        RecurringPaymentPlanDeferred,
                        productDeferred,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should initialize scope.ccvError to blank', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(true);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=add_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=create&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'100',customer_vault_id:'4433221100'}});
                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'100'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('POST', '/customervault').respond(true);
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    expect(scope.ccvError).toBeUndefined();
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.ccvError).toBe('');
                });
                it('should generate scope.ccvError message if scope.ccv is blank, null, or undefined', function() {
                    scope.creditCardNumber = '4111111111111111';
                     
                    expect(scope.ccvError).toBeUndefined();
                    
                    scope.ccv = '';
                    scope.doCreate();
                    scope.$digest();
                    expect(scope.ccvError).toBe('Card Security Code is required');
                    scope.ccv = null;
                    scope.doCreate();
                    scope.$digest();
                    expect(scope.ccvError).toBe('Card Security Code is required');
                    scope.ccv = undefined;
                    scope.doCreate();
                    scope.$digest();
                    expect(scope.ccvError).toBe('Card Security Code is required');
                });
                it('should call PaymentService.get', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(true);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=add_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=create&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'200',customer_vault_id:'4433221100'}});
                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'100'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('POST', '/customervault').respond(true);
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 4 });
                });
                it('should call Nmitransactions.get (type = create, customerVault = add_customer)', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(true);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=add_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=create&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'200',customer_vault_id:'4433221100'}});
    
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                });
                it('should display an error if Nmitransactions.get (type = create, customerVault = add_customer) returns response_code = 200)', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(true);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=add_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=create&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'200',customer_vault_id:'4433221100'}});
    
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.confirmationSubModal).toHaveBeenCalledWith('', { title: 'Create Customer', message: 'Error adding the customer to the vault - undefined', accept: '', deny: 'OK' });
                });
                it('should initialize call Nmitransactions.get (type = salevault)', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(true);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=add_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=create&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'100',customer_vault_id:'4433221100'}});
                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'200'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('POST', '/customervault').respond(true);
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                });
                it('should display an error if Nmitransactions.get (type = salevault) returns response_code = 200)', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(true);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=add_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=create&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'100',customer_vault_id:'4433221100'}});
                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'200'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('POST', '/customervault').respond(true);
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.confirmationSubModal).toHaveBeenCalledWith('', { title: 'Create Customer', message: 'Customer Credit Card Failed Validation.', accept: '', deny: 'OK' });
                });
                it('should call confirmationSubModal on successful customer creation', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(true);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=add_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=create&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'100',customer_vault_id:'4433221100'}});
                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'100'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('POST', '/customervault').respond(true);
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.confirmationSubModal).toHaveBeenCalledWith('', { title: 'Create Customer', message: 'Customer successfully created.', accept: '', deny: 'OK' });
                });
                it('should set varibales depending on countryCard and countryShipping values not being US', function() {
                   spyOn(scope,'checkVaultCreate').and.returnValue(true);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=CA&countryShipping=CA&creditCardNumber=4111111111111111&currency=XYZ&customerVault=add_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=create&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'100',customer_vault_id:'4433221100'}});
                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'100'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('POST', '/customervault').respond(true);
                    
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'CA';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCardText = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'CA';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShippingText = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.confirmationSubModal).toHaveBeenCalledWith('', { title: 'Create Customer', message: 'Customer successfully created.', accept: '', deny: 'OK' });
                });
                it('should set varibales depending on countryCard and countryShipping values being US and states being blank', function() {
                   spyOn(scope,'checkVaultCreate').and.returnValue(true);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=add_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&type=create&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'100',customer_vault_id:'4433221100'}});
                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'100'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('POST', '/customervault').respond(true);
                    
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = '';
                    scope.stateProvinceCardText = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = '';
                    scope.stateProvinceShippingText = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.confirmationSubModal).toHaveBeenCalledWith('', { title: 'Create Customer', message: 'Customer successfully created.', accept: '', deny: 'OK' });
                });
            });

            describe('doCreate II function -  scope.checkVaultCreate()===true, PaymentService.get error', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                       inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                    spyOn(scope,'confirmationSubModal').and.returnValue(true);
    
                    var paymentDeferred,
                        RecurringPaymentPlanDeferred,
                        productDeferred,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.reject('this is an error message on PaymentService.get');
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
               it('should log an error if PaymentService.get fails', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(true);
                    spyOn(console, 'log').and.callThrough();
 
                    $httpBackend.when('POST', '/customervault').respond(true);
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('this is an error message on PaymentService.get');
                });
            });

            describe('doCreate III function -  scope.checkVaultCreate()===true, NMI errors', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                       inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                    spyOn(scope,'confirmationSubModal').and.returnValue(true);
    
                    var paymentDeferred,
                        RecurringPaymentPlanDeferred,
                        productDeferred,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(console, 'log').and.callThrough();

                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error on Nmitransactions.get; type=create, customerVault=add_customer', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(true);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=add_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=create&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond(500, 'This is an error on Nmitransactions.get; type=create, customerVault=add_customer' );
                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'100'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('POST', '/customervault').respond(true);
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(console.log).toHaveBeenCalled();
                    expect(console.log.calls.first().args[0].data).toBe('This is an error on Nmitransactions.get; type=create, customerVault=add_customer');
                    expect(console.log.calls.first().args[0].status).toBe(500);
                });
            });
            
            describe('doCreate IV function - scope.checkVaultCreate()===false, successes', function() {
                 beforeEach(function() {
                    this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form><form id="fieldsForm" name="fieldsForm"></form><div id="transactionCompleted"></div>');
                    this.element.appendTo('body');
                       inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                    spyOn(scope,'confirmationSubModal').and.returnValue(true);
    
                    var paymentDeferred,
                        RecurringPaymentPlanDeferred,
                        productDeferred,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                it('should call PaymentService.get', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(false);
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=update_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=custvaultupdate&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'100',customer_vault_id:'4433221100'}});
//                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'100'}});

                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('PUT', 'customervault').respond(true);

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
   
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 4 });
                });
                it('should call Nmitransactions.get; type=custvaultupdate, customerVault=update_customer', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(false);
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=update_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=custvaultupdate&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'200',customer_vault_id:'4433221100'}});
 //                   $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'100'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('PUT', 'customervault').respond(true);

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
   
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                });
                it('should set scope.saletest on successful update', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(false);
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=update_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=custvaultupdate&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'100',customer_vault_id:'4433221100'}});
//                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'100'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('PUT', 'customervault').respond(true);

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
   
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.saletest).toEqual({ response_code: '100', customer_vault_id: '4433221100', Title: 'Created User' });
                });
                it('should  all scope.$broadcast on successful update', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(false);
                    spyOn(scope,'$broadcast').and.returnValue(true);
                    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=update_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=custvaultupdate&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'100',customer_vault_id:'4433221100'}});
//                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'100'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('PUT', 'customervault').respond(true);

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
   
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(scope.$broadcast).toHaveBeenCalledWith( 'succesfullResult');
                });
               
            });

            describe('doCreate V function -  scope.checkVaultCreate()===false, PaymentService.get error', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                       inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                    spyOn(scope,'confirmationSubModal').and.returnValue(true);
    
                    var paymentDeferred,
                        RecurringPaymentPlanDeferred,
                        productDeferred,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.reject('this is an error message on PaymentService.get 2');
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
               it('should log an error if PaymentService.get fails', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(false);
                    spyOn(console, 'log').and.callThrough();
 
                    $httpBackend.when('POST', '/customervault').respond(true);
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('this is an error message on PaymentService.get 2');
                });
            });

            describe('doCreate VI function -  scope.checkVaultCreate()===false, first NMI error', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="fieldsForm" name="inputForm"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
    
                    var inputForm = scope.inputForm;
   
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                    spyOn(scope,'confirmationSubModal').and.returnValue(true);
    
                    var paymentDeferred,
                        RecurringPaymentPlanDeferred,
                        productDeferred,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(console, 'log').and.callThrough();

                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error on Nmitransactions.get; type=create, customerVault=add_customer', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(false);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=update_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=custvaultupdate&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond({Response:{response_code:'100',customer_vault_id:'4433221100'}});
//                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond({Response:{response_code:'100'}});
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('POST', '/customervault').respond(true);
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(console.log).toHaveBeenCalled();
                    expect(console.log.calls.first().args[0].data).toBe('This is an error on Nmitransactions.get; type=create, customerVault=add_customer 2');
                    expect(console.log.calls.first().args[0].status).toBe(500);
                });
            });

            describe('doCreate VIi function -  scope.checkVaultCreate()===false, second NMI error', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                    this.element.appendTo('body');
                       inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
    
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                    spyOn(scope,'confirmationSubModal').and.returnValue(true);
    
                    var paymentDeferred,
                        RecurringPaymentPlanDeferred,
                        productDeferred,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(console, 'log').and.callThrough();

                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error on Nmitransactions.get; type=create, customerVault=add_customer', function() {
                    spyOn(scope,'checkVaultCreate').and.returnValue(false);
    
                    $httpBackend.whenGET('nmitransactions?addressCard=123+Main+Street&addressContCard=suite+250&addressContShipping=Box+9&addressShipping=456+1st+Street&billingId=987654321&boardingId=25&cityCard=Anytown&cityShipping=Everytown&companyCard=cName&companyShipping=cNameShip&countryCard=US&countryShipping=US&creditCardNumber=4111111111111111&currency=XYZ&customerVault=update_customer&customerVaultId=99887766555&emailAddressCard=email@email.com&emailAddressShipping=emailShip@email.com&expirationDate=11%2F21&faxNumberCard=949-555-2323&firstNameCard=fName&firstNameShipping=fNameShip&lastNameCard=lName&lastNameShipping=lNameShip&phoneNumberCard=714-555-1212&processorId=processor1&stateProvinceCard=CA&stateProvinceShipping=CO&type=custvaultupdate&zipPostalCodeCard=99999&zipPostalCodeShipping=88888').respond(500, 'This is an error on Nmitransactions.get; type=create, customerVault=add_customer 2' );
                    $httpBackend.whenGET('nmitransactions?amount=0.00&boardingId=25&cardSecurityCode=123&customerHash=4433221100&orderDescription=&orderId=&poNumber=&processorId=processor1&shipping=0&tax=0&type=salevault').respond(500, 'This is an error on Nmitransactions.get; type=salevault, customerVault=add_customer 2' );
                    $httpBackend.whenGET('nmitransactions?boardingId=25&customerVaultId=4433221100&processorId=processor1&type=creport').respond({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}}); 
                    $httpBackend.whenGET('nmitransactions?CC_Number=4111111111111111&Hash=ABCDEFGHIJKLMNOP&Transaction_ID=&boardingId=25&processorId=processor1&type=create_customer_card_hash').respond('response');
                    $httpBackend.when('POST', '/customervault').respond(true);
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doCreate();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(console.log).toHaveBeenCalled();
                    expect(console.log.calls.first().args[0].data).toBe('This is an error on Nmitransactions.get; type=create, customerVault=add_customer 2');
                    expect(console.log.calls.first().args[0].status).toBe(500);
                });
            });
        });
        
        describe('doSale function', function() {

            describe('doSale function I (validateMask() function)', function() {
                describe('doSale function (validateMask() function I)', function() {
                    beforeEach(function() {
                        this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                        this.element.appendTo('body');
                        inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
        
                        var paymentDeferred,
                            RecurringPaymentPlanDeferred,
                            productDeferred,
                            $promise;
        
                        paymentDeferred = $q.defer();
                        paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                           
                        spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    });
                    
                    afterEach(function(){
                        this.element.remove(); 
                    });
                    
                    it('should set validData to false and not call PaymentService.get', function() {
                        scope.inputForm = {
                            'lastNameCard':{'$error':{'pattern':true}},
                            'firstNameCard':{'$error':{'pattern':false}},
                            'amount':{'$error':{'pattern':false}},
                            'shipping':{'$error':{'pattern':false}},
                            'tax':{'$error':{'pattern':false}},
                            'zipPostalCodeCard':{'$error':{'pattern':false}},
                            'phoneNumberCard':{'$error':{'pattern':false}},
                            'faxNumberCard':{'$error':{'pattern':false}},
                            'zipPostalCodeShipping':{'$error':{'pattern':false}}
                        };   
                        scope.doSale();
                        scope.$digest();
                        expect(PaymentService.get).not.toHaveBeenCalled();
                    });
                });
        
                describe('doSale function (validateMask() function II)', function() {
                    beforeEach(function() {
                        this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                        this.element.appendTo('body');
                        inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
        
                        var paymentDeferred,
                            RecurringPaymentPlanDeferred,
                            productDeferred,
                            $promise;
        
                        paymentDeferred = $q.defer();
                        paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                           
                        spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    });
                    
                    afterEach(function(){
                        this.element.remove(); 
                    });
                    
                    it('should set validData to false and not call PaymentService.get', function() {
                        scope.inputForm = {
                            'lastNameCard':{'$error':{'pattern':false}},
                            'firstNameCard':{'$error':{'pattern':true}},
                            'amount':{'$error':{'pattern':false}},
                            'shipping':{'$error':{'pattern':false}},
                            'tax':{'$error':{'pattern':false}},
                            'zipPostalCodeCard':{'$error':{'pattern':false}},
                            'phoneNumberCard':{'$error':{'pattern':false}},
                            'faxNumberCard':{'$error':{'pattern':false}},
                            'zipPostalCodeShipping':{'$error':{'pattern':false}}
                       };   
                        scope.doSale();
                        scope.$digest();
                        expect(PaymentService.get).not.toHaveBeenCalled();
                    });
                });
        
                describe('doSale function (validateMask() function III)', function() {
                    beforeEach(function() {
                        this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                        this.element.appendTo('body');
                        inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
        
                        var paymentDeferred,
                            RecurringPaymentPlanDeferred,
                            productDeferred,
                            $promise;
        
                        paymentDeferred = $q.defer();
                        paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                           
                        spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    });
                    
                    afterEach(function(){
                        this.element.remove(); 
                    });
                    
                    it('should set validData to false and not call PaymentService.get', function() {
                        scope.inputForm = {
                            'lastNameCard':{'$error':{'pattern':false}},
                            'firstNameCard':{'$error':{'pattern':false}},
                            'amount':{'$error':{'pattern':true}},
                            'shipping':{'$error':{'pattern':false}},
                            'tax':{'$error':{'pattern':false}},
                            'zipPostalCodeCard':{'$error':{'pattern':false}},
                            'phoneNumberCard':{'$error':{'pattern':false}},
                            'faxNumberCard':{'$error':{'pattern':false}},
                            'zipPostalCodeShipping':{'$error':{'pattern':false}}
                        };   
                        scope.doSale();
                        scope.$digest();
                        expect(PaymentService.get).not.toHaveBeenCalled();
                    });
                });
        
                describe('doSale function (validateMask() function IV)', function() {
                    beforeEach(function() {
                        this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                        this.element.appendTo('body');
                        inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
        
                        var paymentDeferred,
                            RecurringPaymentPlanDeferred,
                            productDeferred,
                            $promise;
        
                        paymentDeferred = $q.defer();
                        paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                           
                        spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    });
                    
                    afterEach(function(){
                        this.element.remove(); 
                    });
                    
                    it('should set validData to false and not call PaymentService.get', function() {
                        scope.inputForm = {
                            'lastNameCard':{'$error':{'pattern':false}},
                            'firstNameCard':{'$error':{'pattern':false}},
                            'amount':{'$error':{'pattern':false}},
                            'shipping':{'$error':{'pattern':true}},
                            'tax':{'$error':{'pattern':false}},
                            'zipPostalCodeCard':{'$error':{'pattern':false}},
                            'phoneNumberCard':{'$error':{'pattern':false}},
                            'faxNumberCard':{'$error':{'pattern':false}},
                            'zipPostalCodeShipping':{'$error':{'pattern':false}}
                        };   
                        scope.doSale();
                        scope.$digest();
                        expect(PaymentService.get).not.toHaveBeenCalled();
                    });
                });
        
                describe('doSale function (validateMask() function V)', function() {
                    beforeEach(function() {
                        this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                        this.element.appendTo('body');
                        inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
        
                        var paymentDeferred,
                            RecurringPaymentPlanDeferred,
                            productDeferred,
                            $promise;
        
                        paymentDeferred = $q.defer();
                        paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                           
                        spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    });
                    
                    afterEach(function(){
                        this.element.remove(); 
                    });
                    
                    it('should set validData to false and not call PaymentService.get', function() {
                        scope.inputForm = {
                            'lastNameCard':{'$error':{'pattern':false}},
                            'firstNameCard':{'$error':{'pattern':false}},
                            'amount':{'$error':{'pattern':false}},
                            'shipping':{'$error':{'pattern':false}},
                            'tax':{'$error':{'pattern':true}},
                            'zipPostalCodeCard':{'$error':{'pattern':false}},
                            'phoneNumberCard':{'$error':{'pattern':false}},
                            'faxNumberCard':{'$error':{'pattern':false}},
                            'zipPostalCodeShipping':{'$error':{'pattern':false}}
                        };   
                        scope.doSale();
                        scope.$digest();
                        expect(PaymentService.get).not.toHaveBeenCalled();
                     });
                });
        
                describe('doSale function (validateMask() function VI)', function() {
                    beforeEach(function() {
                        this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                        this.element.appendTo('body');
                        inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
        
                        var paymentDeferred,
                            RecurringPaymentPlanDeferred,
                            productDeferred,
                            $promise;
        
                        paymentDeferred = $q.defer();
                        paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                           
                        spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    });
                    
                    afterEach(function(){
                        this.element.remove(); 
                    });
                    
                    it('should set validData to false and not call PaymentService.get', function() {
                       scope.inputForm = {
                            'lastNameCard':{'$error':{'pattern':false}},
                            'firstNameCard':{'$error':{'pattern':false}},
                            'amount':{'$error':{'pattern':false}},
                            'shipping':{'$error':{'pattern':false}},
                            'tax':{'$error':{'pattern':false}},
                            'zipPostalCodeCard':{'$error':{'pattern':true}},
                            'phoneNumberCard':{'$error':{'pattern':false}},
                            'faxNumberCard':{'$error':{'pattern':false}},
                            'zipPostalCodeShipping':{'$error':{'pattern':false}}
                        };   
                        scope.doSale();
                        scope.$digest();
                        expect(PaymentService.get).not.toHaveBeenCalled();
                    });
                });
        
                describe('doSale function (validateMask() function VII)', function() {
                    beforeEach(function() {
                        this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                        this.element.appendTo('body');
                        inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
        
                        var paymentDeferred,
                            RecurringPaymentPlanDeferred,
                            productDeferred,
                            $promise;
        
                        paymentDeferred = $q.defer();
                        paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                           
                        spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    });
                    
                    afterEach(function(){
                        this.element.remove(); 
                    });
                    
                    it('should set validData to false and not call PaymentService.get', function() {
                        scope.inputForm = {
                            'lastNameCard':{'$error':{'pattern':false}},
                            'firstNameCard':{'$error':{'pattern':false}},
                            'amount':{'$error':{'pattern':false}},
                            'shipping':{'$error':{'pattern':false}},
                            'tax':{'$error':{'pattern':false}},
                            'zipPostalCodeCard':{'$error':{'pattern':false}},
                            'phoneNumberCard':{'$error':{'pattern':true}},
                            'faxNumberCard':{'$error':{'pattern':false}},
                            'zipPostalCodeShipping':{'$error':{'pattern':false}}
                        };   
                        scope.doSale();
                        scope.$digest();
                        expect(PaymentService.get).not.toHaveBeenCalled();
                    });
                });
        
                describe('doSale function (validateMask() function VIII)', function() {
                    beforeEach(function() {
                        this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                        this.element.appendTo('body');
                        inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
        
                        var paymentDeferred,
                            RecurringPaymentPlanDeferred,
                            productDeferred,
                            $promise;
        
                        paymentDeferred = $q.defer();
                        paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                           
                        spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    });
                    
                    afterEach(function(){
                        this.element.remove(); 
                    });
                    
                    it('should set validData to false and not call PaymentService.get', function() {
                        scope.inputForm = {
                            'lastNameCard':{'$error':{'pattern':false}},
                            'firstNameCard':{'$error':{'pattern':false}},
                            'amount':{'$error':{'pattern':false}},
                            'shipping':{'$error':{'pattern':false}},
                            'tax':{'$error':{'pattern':false}},
                            'zipPostalCodeCard':{'$error':{'pattern':false}},
                            'phoneNumberCard':{'$error':{'pattern':false}},
                            'faxNumberCard':{'$error':{'pattern':true}},
                            'zipPostalCodeShipping':{'$error':{'pattern':false}}
                        };   
                        scope.doSale();
                        scope.$digest();
                        expect(PaymentService.get).not.toHaveBeenCalled();
                    });
                });
        
                describe('doSale function (validateMask() function IX)', function() {
                    beforeEach(function() {
                        this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm"></form>');
                        this.element.appendTo('body');
                        inject(function ($rootScope, $compile) {
                            this.element = $compile(this.element)(scope);
                        }); 
        
                        var paymentDeferred,
                            RecurringPaymentPlanDeferred,
                            productDeferred,
                            $promise;
        
                        paymentDeferred = $q.defer();
                        paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                                           
                        spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    });
                    
                    afterEach(function(){
                        this.element.remove(); 
                    });
                    
                    it('should set validData to false and not call PaymentService.get', function() {
                        scope.inputForm = {
                            'lastNameCard':{'$error':{'pattern':false}},
                            'firstNameCard':{'$error':{'pattern':false}},
                            'amount':{'$error':{'pattern':false}},
                            'shipping':{'$error':{'pattern':false}},
                            'tax':{'$error':{'pattern':false}},
                            'zipPostalCodeCard':{'$error':{'pattern':false}},
                            'phoneNumberCard':{'$error':{'pattern':false}},
                            'faxNumberCard':{'$error':{'pattern':false}},
                            'zipPostalCodeShipping':{'$error':{'pattern':true}}
                        };   
                        scope.doSale();
                        scope.$digest();
                        expect(PaymentService.get).not.toHaveBeenCalled();
                    });
                });
        
            });
             
            describe('doSale function II', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    });
    
                    var paymentDeferred,
                        NMITransactionsDeferred,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred = $q.defer();
                    NMITransactionsDeferred.resolve({Response:{response_code:'200',customer_vault_id:'4433221100'}});
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.returnValue({$promise: NMITransactionsDeferred.promise}); //returns a fake promise;

                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                 });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should call PaymentService.get', function() {
                   scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   
                    scope.doSale();
                    scope.$digest();
                    expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 4 });
                });
                xit('should hide the input form and show the transactionCompleted div', function() {
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   
                    expect(document.getElementById('fieldsForm').style.display).toBe('block');                  
                    expect(document.getElementById('transactionCompleted').style.display).toBe('none');                  
                    scope.doSale();
                    scope.$digest();
                    expect(document.getElementById('fieldsForm').style.display).toBe('none');                  
                    expect(document.getElementById('transactionCompleted').style.display).toBe('block');                  
                });
                xit('should set scope.saletest', function() {
                    spyOn(scope,'$broadcast').and.returnValue(true);
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   
                    expect(scope.saletest).toBeUndefined();                  
                    scope.doSale();
                    scope.$digest();
                    expect(scope.saletest).toEqual({ response_code: '200', customer_vault_id: '4433221100', Title: 'Sale' });                  
                });
                xit('should call scope.$broadcast on to display results of NMI call', function() {
                    spyOn(scope,'$broadcast').and.returnValue(true);
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   
                    scope.doSale();
                    scope.$digest();
                    expect(scope.$broadcast).toHaveBeenCalledWith('succesfullResult');
                });
            });

            xdescribe('doSale function III', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    });
    
                    var paymentDeferred,
                        NMITransactionsDeferred_sale,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred_sale = $q.defer();
                    NMITransactionsDeferred_sale.resolve({Response:{response_code:'100',customer_vault_id:'4433221100',transactionid:'12346578901234657890'}});
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );

                    NMITransactionsDeferred_creport = $q.defer();
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'sale') {
                            return {$promise: NMITransactionsDeferred_sale.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });
 
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
               });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should call PaymentService.get three times', function() {
                    $httpBackend.when('POST','/customervault').respond(200, true);
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doSale();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(PaymentService.get).toHaveBeenCalled();
                    expect(PaymentService.get.calls.count()).toBe(3);
                    expect(PaymentService.get.calls.argsFor(0)).toEqual([{ paymentId: 25, productId: 4 }]);  // doSale
                    expect(PaymentService.get.calls.argsFor(1)).toEqual([{ paymentId: 25, productId: 4 }]);  // recordHash()
                    expect(PaymentService.get.calls.argsFor(2)).toEqual([{ paymentId: 25, productId: 4 }]);  // createCustomerVault()
                });
                it('should call Nmitransactions.get four times', function() {
                    $httpBackend.when('POST','/customervault').respond(200, true);
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doSale();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(Nmitransactions.get).toHaveBeenCalled();
                    expect(Nmitransactions.get.calls.count()).toBe(4);
                    expect(JSON.stringify(Nmitransactions.get.calls.argsFor(0))).toEqual(JSON.stringify([{ expirationDate: '11/21', amount: undefined, currency: 'XYZ', cardSecurityCode: '123', orderId: undefined, orderDescription: undefined, poNumber: undefined, shipping: undefined, tax: undefined, firstNameCard: 'fName', lastNameCard: 'lName', companyCard: 'cName', countryCard: 'US', addressCard: '123 Main Street', addressContCard: 'suite 250', cityCard: 'Anytown', zipPostalCodeCard: '99999', phoneNumberCard: '714-555-1212', faxNumberCard: '949-555-2323', emailAddressCard: 'email@email.com', websiteAddressCard: undefined, firstNameShipping: 'fNameShip', lastNameShipping: 'lNameShip', companyShipping: 'cNameShip', countryShipping: 'US', addressShipping: '456 1st Street', addressContShipping: 'Box 9', cityShipping: 'Everytown', zipPostalCodeShipping: '88888', emailAddressShipping: 'emailShip@email.com', creditCardNumber: '4111111111111111', stateProvinceCard: 'CA', stateProvinceShipping: 'CO', item_product_code_1: '', item_description_1: '', item_commodity_code_1: '', item_quantity_1: '0', item_unit_of_measure_1: '', item_unit_cost_1: '0.00', item_tax_amount_1: '0.00', item_discount_amount_1: '0.00', item_total_amount_1: '0.00', item_tax_rate_1: '', item_tax_total_1: '', type: 'sale', processorId: 'processor1', boardingId: 25 }]));  // doSale()
                    expect(JSON.stringify(Nmitransactions.get.calls.argsFor(1))).toEqual(JSON.stringify([{ transactionId: '12346578901234657890', type: 'treport' }])); // recordHash() #1=transactionDetail
                    expect(JSON.stringify(Nmitransactions.get.calls.argsFor(2))).toEqual(JSON.stringify([{ customerVaultId: '4433221100', processorId: 'processor1', boardingId: 25, type: 'creport' }])); // recordHash() #2=create_customer_card_hash
                    expect(JSON.stringify(Nmitransactions.get.calls.argsFor(3))).toEqual(JSON.stringify([{ Transaction_id: '12346578901234657890', CC_Number: '4111111111111111', Hash: 'ABCDEFGHIJKLMNOP', processorId: 'processor1', boardingId: 25, type: 'create_customer_card_hash' }])); // createCustomerVault()
                    expect(Nmitransactions.get.calls.argsFor(4)).toEqual([]);
                    
              });
            });

            xdescribe('doSale function IV - PaymentService.get errors', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="fieldsForm" name="inputForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
    
                    var paymentDeferred,
                        NMITransactionsDeferred_sale,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.reject('Error response on PaymentService.get');
                    
                    NMITransactionsDeferred_sale = $q.defer();
                    NMITransactionsDeferred_sale.resolve({Response:{response_code:'100',customer_vault_id:'4433221100',transactionid:'12346578901234657890'}});
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );

                    NMITransactionsDeferred_creport = $q.defer();
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'sale') {
                            return {$promise: NMITransactionsDeferred_sale.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error on PaymentService.get', function() {
                    spyOn(console,'log').and.callThrough();
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doSale();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('Error response on PaymentService.get');
                });
            });

            xdescribe('doSale function V - first Nmitransactions.get error', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="fieldsForm" name="inputForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
    
                    var paymentDeferred,
                        NMITransactionsDeferred_sale,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred_sale = $q.defer();
                    //NMITransactionsDeferred_sale.resolve({Response:{response_code:'100',customer_vault_id:'4433221100',transactionid:'12346578901234657890'}});
                    
                    NMITransactionsDeferred_sale.reject('Error on NMITransactions in doSale function');
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );

                    NMITransactionsDeferred_creport = $q.defer();
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'sale') {
                            return {$promise: NMITransactionsDeferred_sale.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error on Nmitransactions.get (sale)', function() {
                    spyOn(console,'log').and.callThrough();
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
    
                    scope.doSale();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('Error on NMITransactions in doSale function');
                });
            });

            xdescribe('doSale function VI - second Nmitransactions.get error', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    });
    
                    var paymentDeferred,
                        NMITransactionsDeferred_sale,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred_sale = $q.defer();
                    NMITransactionsDeferred_sale.resolve({Response:{response_code:'100',customer_vault_id:'4433221100',transactionid:'12346578901234657890'}});
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    //NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );
                    NMITransactionsDeferred_treport.reject('Error on NMITransactions in recordHash function (first one=transactionDetail)');
                    
                    NMITransactionsDeferred_creport = $q.defer();
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'sale') {
                            return {$promise: NMITransactionsDeferred_sale.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });

                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
               });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error on Nmitransactions.get (treport)', function() {
                    spyOn(console,'log').and.callThrough();
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
                    
                    $httpBackend.when('POST', '/customervault').respond(true);
 
                    scope.doSale();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(console.log).toHaveBeenCalledWith('Error on NMITransactions in recordHash function (first one=transactionDetail)');
                });
            });

            xdescribe('doSale function VII - third Nmitransactions.get error', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    });
    
                    var paymentDeferred,
                        NMITransactionsDeferred_sale,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred_sale = $q.defer();
                    NMITransactionsDeferred_sale.resolve({Response:{response_code:'100',customer_vault_id:'4433221100',transactionid:'12346578901234657890'}});
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );
                    
                    NMITransactionsDeferred_creport = $q.defer();
                    /*
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                    */
                    NMITransactionsDeferred_creport.reject('Error on NMITransactions in recordHash function (second one=create_customer_card_hash)');
                    
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'sale') {
                            return {$promise: NMITransactionsDeferred_sale.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });
 
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
               });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error on Nmitransactions.get (treport)', function() {
                    spyOn(console,'log').and.callThrough();
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
                    
                    scope.doSale();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('Error on NMITransactions in recordHash function (second one=create_customer_card_hash)');
                });
            });

            xdescribe('doSale function VIII - fourth Nmitransactions.get error', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    });
    
                    var paymentDeferred,
                        NMITransactionsDeferred_sale,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred_sale = $q.defer();
                    NMITransactionsDeferred_sale.resolve({Response:{response_code:'100',customer_vault_id:'4433221100',transactionid:'12346578901234657890'}});
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );
                    
                    NMITransactionsDeferred_creport = $q.defer();
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    //NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                    NMITransactionsDeferred_create_customer_card_hash.reject('Error on NMITransactions in createCustomerVault');
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'sale') {
                            return {$promise: NMITransactionsDeferred_sale.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });
 
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
               });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error on Nmitransactions.get (treport)', function() {
                    spyOn(console,'log').and.callThrough();
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
                    
                    $httpBackend.when('POST', '/customervault').respond(true);
 
                    scope.doSale();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(console.log).toHaveBeenCalledWith('Error on NMITransactions in createCustomerVault');
                });
            });
            
            xdescribe('doSale function IX - post to /customervault error', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                     inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    });   
                    var paymentDeferred,
                        NMITransactionsDeferred_sale,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred_sale = $q.defer();
                    NMITransactionsDeferred_sale.resolve({Response:{response_code:'100',customer_vault_id:'4433221100',transactionid:'12346578901234657890'}});
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );
                    
                    NMITransactionsDeferred_creport = $q.defer();
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'sale') {
                            return {$promise: NMITransactionsDeferred_sale.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });

                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error on Nmitransactions.get (treport)', function() {
                    spyOn(console,'log').and.callThrough();
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
                    
                    $httpBackend.when('POST', '/customervault').respond(500,'This is an error on /customervault');
 
                    scope.doSale();
                    scope.$digest();
                    $httpBackend.flush();
                    $httpBackend.verifyNoOutstandingExpectation();
                    $httpBackend.verifyNoOutstandingRequest();
                    expect(console.log).toHaveBeenCalledWith('This is an error on /customervault');
                });
            });
            
            xdescribe('doSale function X - No Customer Vault ID in sale', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    });
    
                    var paymentDeferred,
                        NMITransactionsDeferred_sale,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred_sale = $q.defer();
                    NMITransactionsDeferred_sale.resolve({Response:{response_code:'100',customer_vault_id:undefined,transactionid:'12346578901234657890'}});
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );
                    
                    NMITransactionsDeferred_creport = $q.defer();
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'sale') {
                            return {$promise: NMITransactionsDeferred_sale.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });

                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error on Nmitransactions.get (treport)', function() {
                    spyOn(console,'log').and.callThrough();
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   

                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';
                    
                    scope.doSale();
                    scope.$digest();
                    expect(Nmitransactions.get).toHaveBeenCalled();
                    expect(Nmitransactions.get.calls.count()).toBe(3);
                    expect(JSON.stringify(Nmitransactions.get.calls.argsFor(0))).toEqual(JSON.stringify([{ expirationDate: '11/21', amount: undefined, currency: 'XYZ', cardSecurityCode: '123', orderId: undefined, orderDescription: undefined, poNumber: undefined, shipping: undefined, tax: undefined, firstNameCard: 'fName', lastNameCard: 'lName', companyCard: 'cName', countryCard: 'US', addressCard: '123 Main Street', addressContCard: 'suite 250', cityCard: 'Anytown', zipPostalCodeCard: '99999', phoneNumberCard: '714-555-1212', faxNumberCard: '949-555-2323', emailAddressCard: 'email@email.com', websiteAddressCard: undefined, firstNameShipping: 'fNameShip', lastNameShipping: 'lNameShip', companyShipping: 'cNameShip', countryShipping: 'US', addressShipping: '456 1st Street', addressContShipping: 'Box 9', cityShipping: 'Everytown', zipPostalCodeShipping: '88888', emailAddressShipping: 'emailShip@email.com', creditCardNumber: '4111111111111111', stateProvinceCard: 'CA', stateProvinceShipping: 'CO', item_product_code_1: '', item_description_1: '', item_commodity_code_1: '', item_quantity_1: '0', item_unit_of_measure_1: '', item_unit_cost_1: '0.00', item_tax_amount_1: '0.00', item_discount_amount_1: '0.00', item_total_amount_1: '0.00', item_tax_rate_1: '', item_tax_total_1: '', type: 'sale', processorId: 'processor1', boardingId: 25 }]));  // doSale()
                    expect(JSON.stringify(Nmitransactions.get.calls.argsFor(1))).toEqual(JSON.stringify([{ transactionId: '12346578901234657890', type: 'treport' }])); // recordHash() #1=transactionDetail
                    expect(JSON.stringify(Nmitransactions.get.calls.argsFor(2))).toEqual(JSON.stringify([{ Transaction_id: '12346578901234657890', CC_Number: '4111111111111111', Hash: 'ABCDEFGHIJKLMNOP', processorId: 'processor1', boardingId: 25, type: 'create_customer_card_hash' }])); // createCustomerVault()
                    expect(Nmitransactions.get.calls.argsFor(3)).toEqual([]);
                });
            });
            
        });
        
        xdescribe('doAuthorize function', function() {

            describe('doAuthorize function I', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
    
                    var paymentDeferred,
                        NMITransactionsDeferred,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred = $q.defer();
                    NMITransactionsDeferred.resolve({Response:{response_code:'200',customer_vault_id:'4433221100'}});
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.returnValue({$promise: NMITransactionsDeferred.promise}); //returns a fake promise;
                    spyOn($location, 'path').and.returnValue('/authorize/99887766555');

                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should set parms (also testing longFieldObject function)', function() {
                    scope.param = ['','authorize','99887766555'];
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };
                       
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4xxxxxxxxxxx1111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';

                    scope.doAuthorize();
                    scope.$digest();
                    expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 4 });
                    
                    scope.taxExempt = true;
                    scope.sendReceiptEmail = true;
                    scope.addCustomerVault = true;
                    scope.countryCard = 'US';
                    scope.stateProvinceCard = '';
                    scope.countryShipping = 'US';
                    scope.stateProvinceShipping = '';
                    scope.creditCardNumber = '4111111111111111';
                    scope.doAuthorize();
                    scope.$digest();
                    expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 4 });
                });
                it('should not call PaymentService.get', function() {
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':true}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };
                       
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';

                    scope.doAuthorize();
                    scope.$digest();
                    expect(PaymentService.get).not.toHaveBeenCalled();
                });
                it('should call PaymentService.get', function() {
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };
                       
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4xxxxxxxxxxx1111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';

                    scope.doAuthorize();
                    scope.$digest();
                    expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 4 });
                });
                it('should call Nmitransactions.get', function() {
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   
                       
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';

                    scope.doAuthorize();
                    scope.$digest();
                    expect(JSON.stringify(Nmitransactions.get.calls.argsFor(0))).toEqual(JSON.stringify([{ expirationDate: '11/21', amount: undefined, currency: 'XYZ', cardSecurityCode: '123', orderId: undefined, orderDescription: undefined, poNumber: undefined, shipping: undefined, tax: undefined, firstNameCard: 'fName', lastNameCard: 'lName', companyCard: 'cName', countryCard: 'US', addressCard: '123 Main Street', addressContCard: 'suite 250', cityCard: 'Anytown', zipPostalCodeCard: '99999', phoneNumberCard: '714-555-1212', faxNumberCard: '949-555-2323', emailAddressCard: 'email@email.com', websiteAddressCard: undefined, firstNameShipping: 'fNameShip', lastNameShipping: 'lNameShip', companyShipping: 'cNameShip', countryShipping: 'US', addressShipping: '456 1st Street', addressContShipping: 'Box 9', cityShipping: 'Everytown', zipPostalCodeShipping: '88888', emailAddressShipping: 'emailShip@email.com', creditCardNumber: '4111111111111111', stateProvinceCard: 'CA', stateProvinceShipping: 'CO', item_product_code_1: '', item_description_1: '', item_commodity_code_1: '', item_quantity_1: '0', item_unit_of_measure_1: '', item_unit_cost_1: '0.00', item_tax_amount_1: '0.00', item_discount_amount_1: '0.00', item_total_amount_1: '0.00', item_tax_rate_1: '', item_tax_total_1: '', type: 'auth', processorId: 'processor1', boardingId: 25 }]));  // doAuthorize()
                });
                it('should set scope.saletest', function() {
                    expect(scope.saletest).toBeUndefined();
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   
                       
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';

                    scope.doAuthorize();
                    scope.$digest();
                    expect(scope.saletest).toEqual({ response_code: '200', customer_vault_id: '4433221100', Title: 'Authorize' });
                });
            });

            describe('doAuthorize function II', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
    
                    var paymentDeferred,
                        NMITransactionsDeferred_auth,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred_auth = $q.defer();
                    NMITransactionsDeferred_auth.resolve({Response:{response_code:'100',customer_vault_id:'4433221100',transactionid:'12346578901234657890'}});
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );

                    NMITransactionsDeferred_creport = $q.defer();
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'auth') {
                            return {$promise: NMITransactionsDeferred_auth.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });
 
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
               });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should call NMI and customerVault if response_code = 100', function() {
                    $httpBackend.when('POST','/customervault').respond(200, true);
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   
                       
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'CA';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCardText = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'CA';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShippingText = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';

                    scope.doAuthorize();
                    scope.$digest();
                });
            });

            describe('doAuthorize function III (no cust vault)', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
    
                    var paymentDeferred,
                        NMITransactionsDeferred_auth,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred_auth = $q.defer();
                    NMITransactionsDeferred_auth.resolve({Response:{response_code:'100',transactionid:'12346578901234657890'}});
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );

                    NMITransactionsDeferred_creport = $q.defer();
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'auth') {
                            return {$promise: NMITransactionsDeferred_auth.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });

                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
                });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should not call createCustomerVault', function() {
                    $httpBackend.when('POST','/customervault').respond(200, true);
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   
                       
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';

                    scope.doAuthorize();
                    scope.$digest();
                    expect(PaymentService.get.calls.count()).toBe(2);
                    expect(Nmitransactions.get.calls.count()).toBe(3);
                });
            });

            describe('doAuthorize function III (PaymentService.get error)', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
    
                    var paymentDeferred,
                        NMITransactionsDeferred_auth,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.reject('This is an error response on PaymentService.get');
                    
                    NMITransactionsDeferred_auth = $q.defer();
                    NMITransactionsDeferred_auth.resolve({Response:{response_code:'100',transactionid:'12346578901234657890'}});
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );

                    NMITransactionsDeferred_creport = $q.defer();
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'auth') {
                            return {$promise: NMITransactionsDeferred_auth.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });
 
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
               });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error if PaymentService.get fails', function() {
                    $httpBackend.when('POST','/customervault').respond(200, true);
                    spyOn(console,'log').and.callThrough();
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   
                       
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';

                    scope.doAuthorize();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('This is an error response on PaymentService.get');
                });
            });

            describe('doAuthorize function IV (Nmitransactions.get error)', function() {
                beforeEach(function() {
                    this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search"><form id="inputForm" name="inputForm" style="display:block;"></form><form id="fieldsForm" name="fieldsForm" style="display:block;"></form><div id="transactionCompleted" style="display:none;"></div>');
                    this.element.appendTo('body');
                    inject(function ($rootScope, $compile) {
                        this.element = $compile(this.element)(scope);
                    }); 
    
                    var paymentDeferred,
                        NMITransactionsDeferred_auth,
                        NMITransactionsDeferred_treport,
                        NMITransactionsDeferred_creport,
                        NMITransactionsDeferred_create_customer_card_hash,
                        $promise;
    
                    paymentDeferred = $q.defer();
                    paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                    
                    NMITransactionsDeferred_auth = $q.defer();
                    NMITransactionsDeferred_auth.reject('This is an error response on doAuthorize NmiTransaction.get call');
                    
                    NMITransactionsDeferred_treport = $q.defer();
                    NMITransactionsDeferred_treport.resolve( {nm_response:{transaction:{cc_hash:{'$t':'ABCDEFGHIJKLMNOP'}}}} );

                    NMITransactionsDeferred_creport = $q.defer();
                    NMITransactionsDeferred_creport.resolve({nm_response:{customer_vault:{customer:{
                        customer_vault_id:{'$t':''},
                        first_name:{'$t':''},
                        last_name:{'$t':''},
                        address_1:{'$t':''},
                        address_2:{'$t':''},
                        company:{'$t':''},
                        city:{'$t':''},
                        state:{'$t':''},
                        postal_code:{'$t':''},
                        country:{'$t':''},
                        email:{'$t':''},
                        phone:{'$t':''},
                        fax:{'$t':''},
                        cell_phone:{'$t':''},
                        website:{'$t':''},
                        shipping_first_name:{'$t':''},
                        shipping_last_name:{'$t':''},
                        shipping_address_1:{'$t':''},
                        shipping_address_2:{'$t':''},
                        shipping_company:{'$t':''},
                        shipping_city:{'$t':''},
                        shipping_state:{'$t':''},
                        shipping_postal_code:{'$t':''},
                        shipping_country:{'$t':''},
                        shipping_email:{'$t':''},
                        shipping_carrier:{'$t':''},
                        tracking_number:{'$t':''},
                        shipping_date:{'$t':''},
                        shipping:{'$t':''},
                        cc_number:{'$t':''},
                        cc_hash:{'$t':'ABCDEFGHIJKLMNOP'},
                        cc_exp:{'$t':''},
                        cc_start_date:{'$t':''},
                        cc_issue_number:{'$t':''},
                        check_account:{'$t':''},
                        check_hash:{'$t':''},
                        check_aba:{'$t':''},
                        check_name:{'$t':''},
                        account_type:{'$t':''},
                        sec_code:{'$t':''},
                        processor_id:{'$t':''},
                        cc_bin:{'$t':''}}}}});
                        
                    NMITransactionsDeferred_create_customer_card_hash = $q.defer();
                    NMITransactionsDeferred_create_customer_card_hash.resolve( true );
                                       
                    spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                    spyOn(Nmitransactions, 'get').and.callFake(function(parameters) {
                        if (parameters.type === 'auth') {
                            return {$promise: NMITransactionsDeferred_auth.promise}; //returns a fake promise;
                        } else if (parameters.type === 'treport') {
                            return {$promise: NMITransactionsDeferred_treport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'creport') {
                            return {$promise: NMITransactionsDeferred_creport.promise}; //returns a fake promise;
                        } else if (parameters.type === 'create_customer_card_hash') {
                            return {$promise: NMITransactionsDeferred_create_customer_card_hash.promise}; //returns a fake promise;
                        }
                    });
 
                    var inputForm = scope.inputForm;
                    spyOn(inputForm,'$setPristine').and.returnValue(true);
               });
                
                afterEach(function(){
                    this.element.remove(); 
                });
                
                it('should log an error on Nmitransactions.get failure', function() {
                    spyOn(console,'log').and.callThrough();
                    $httpBackend.when('POST','/customervault').respond(200, true);
                    scope.inputForm = {
                        'lastNameCard':{'$error':{'pattern':false}},
                        'firstNameCard':{'$error':{'pattern':false}},
                        'amount':{'$error':{'pattern':false}},
                        'shipping':{'$error':{'pattern':false}},
                        'tax':{'$error':{'pattern':false}},
                        'zipPostalCodeCard':{'$error':{'pattern':false}},
                        'phoneNumberCard':{'$error':{'pattern':false}},
                        'faxNumberCard':{'$error':{'pattern':false}},
                        'zipPostalCodeShipping':{'$error':{'pattern':false}}
                    };   
                       
                    scope.customerVaultId = '99887766555';
                    scope.billingId = '987654321';
                    scope.creditCardNumber = '4111111111111111';
                    scope.expirationDate = '11/21';
                    scope.currency = 'XYZ';
                    scope.cardSecurityCode = '123';
                    scope.ccv = '123';
                    scope.firstNameCard = 'fName';
                    scope.lastNameCard = 'lName';
                    scope.companyCard = 'cName';
                    scope.countryCard = 'US';
                    scope.addressCard = '123 Main Street';
                    scope.addressContCard = 'suite 250';
                    scope.cityCard = 'Anytown';
                    scope.stateProvinceCard = 'CA';
                    scope.zipPostalCodeCard = '99999';
                    scope.phoneNumberCard = '714-555-1212';
                    scope.faxNumberCard = '949-555-2323';
                    scope.emailAddressCard = 'email@email.com';
                    scope.firstNameShipping = 'fNameShip';
                    scope.lastNameShipping = 'lNameShip';
                    scope.companyShipping = 'cNameShip';
                    scope.countryShipping = 'US';
                    scope.addressShipping = '456 1st Street';
                    scope.addressContShipping = 'Box 9';
                    scope.cityShipping = 'Everytown';
                    scope.stateProvinceShipping = 'CO';
                    scope.zipPostalCodeShipping = '88888';
                    scope.emailAddressShipping = 'emailShip@email.com';

                    scope.doAuthorize();
                    scope.$digest();
                    expect(console.log).toHaveBeenCalledWith('This is an error response on doAuthorize NmiTransaction.get call');
                });
            });

        });
     });
}());

