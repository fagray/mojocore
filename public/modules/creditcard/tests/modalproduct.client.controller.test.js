/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('ModalProCtrl', function() {
        // Initialize global variables
        var $q,
            ModalProCtrl,
            scope,
            $httpBackend,
			$stateParams,
			$location,
            $modal,
            boardingid,
            modalInstance,
            ProductService,
            isPayment_Plan,
            Authentication;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, _$modal_, _$q_, _ProductService_, _isPayment_Plan_, _Authentication_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25,
                    Username: 'Updater'
                }
            };
            $q = _$q_;
            $stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;
			$modal = _$modal_;
            modalInstance = {
                // Create a mock object using spies
                close: jasmine.createSpy('modalInstance.close'),
                dismiss: jasmine.createSpy('modalInstance.dismiss'),
                result: {
                    then: jasmine.createSpy('modalInstance.result.then')
                }
            };
            boardingid = 25;
            
            // Point global variables to injected services
            ProductService = _ProductService_;
            isPayment_Plan = _isPayment_Plan_;

            // Initialize the controller
            ModalProCtrl = $controller('ModalProCtrl', {
                $scope: scope,
                $modalInstance: modalInstance,
                boardingid: boardingid,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
            it('should not have the variable $scope.products defined', function() {
                expect(scope.products).toBeUndefined();
            }); 
            it('should not have the variable $scope.products defined', function() {
                expect(scope.error).toBeUndefined();
            }); 
            it('should not have the variable $scope.payment_plans defined', function() {
                expect(scope.payment_plans).toBeUndefined();
            }); 
            it('should have function scope.addProduct defined', function() {
                expect(scope.addProduct).toBeDefined();
            });        
            it('should have function scope.createPayment defined', function() {
                expect(scope.createPayment).toBeDefined();
            });
/*
            it('should have function scope.getProducts defined', function() {
                expect(scope.getProducts).toBeDefined();
            });        
*/
            it('should have function scope.cancel defined', function() {
                expect(scope.cancel).toBeDefined();
            });        
        });

        describe('Interface', function() {
            it('should recognize addProduct as a function', function() {
                expect(angular.isFunction(scope.addProduct)).toBe(true);
            });
            it('should recognize createPayment as a function', function() {
                expect(angular.isFunction(scope.createPayment)).toBe(true);
            });
/*
            it('should recognize getProducts as a function', function() {
                expect(angular.isFunction(scope.getProducts)).toBe(true);
            });
*/
            it('should recognize cancel as a function', function() {
                expect(angular.isFunction(scope.cancel)).toBe(true);
            });
        });

        describe('scope.addProduct testing', function() {
            beforeEach(function() {

                var productDeferred,
                    $promise;

                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
 //returns a fake promise;
            });
            
            it ('should call ProductService.get service', function() {
                scope.addProduct();
                scope.$digest();
                expect(ProductService.get).toHaveBeenCalled();
                expect(ProductService.get).toHaveBeenCalledWith({productId: 'all'});
            });
            it ('should initialize and set scope.products', function() {
                expect(scope.products).toBeUndefined();
                scope.addProduct();
                scope.$digest();
                expect(scope.products).toBeDefined();
            });
        });

        describe('scope.createPayment testing', function() {
            beforeEach(function() {

                var productDeferred,
                    $promise;

                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
            });

            it ('should call ProductService.get service', function() {
                var paymentRecord = {"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0};
                $httpBackend.when('GET', 'isPayment_Plan/25/1').respond(200, {'clean':'clean'});
                $httpBackend.when('POST', '/paymentplan').respond(200, "Success");
                
                scope.createPayment(paymentRecord);
                $httpBackend.flush();
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(ProductService.get).toHaveBeenCalled();
                expect(ProductService.get).toHaveBeenCalledWith({productId: 'all'});
             });
            it ('should call isPayment_Plan.get and /paymentplan services when isPayment_Plan/25/1 call returns clean', function() {
                var paymentRecord = {"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0};
                $httpBackend.expect('GET', 'isPayment_Plan/25/1').respond(200, {'clean':'clean'});
                $httpBackend.expect('POST', '/paymentplan').respond(200, "Success");
                
                scope.createPayment(paymentRecord);
                $httpBackend.flush(2);
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it ('should call isPayment_Plan.get only when isPayment_Plan/25/1 call returns not clean', function() {
                var paymentRecord = {"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0};
                $httpBackend.expect('GET', 'isPayment_Plan/25/1').respond(200, {"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-10-13T15:47:18.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":null,"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                //$httpBackend.expect('POST', '/paymentplan').respond(200, "Success");
                
                scope.createPayment(paymentRecord);
                $httpBackend.flush(1);
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it ('should set scope.error if /paymentplan call generates the error', function() {
                var paymentRecord = {"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0};
                $httpBackend.expect('GET', 'isPayment_Plan/25/1').respond(200, {'clean':'clean'});
                $httpBackend.expect('POST', '/paymentplan').respond(500, {'message':'Error!'});
                
                expect(scope.error).toBeUndefined(); 
                scope.createPayment(paymentRecord);
                $httpBackend.flush(2);
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                
                expect(scope.error).toBeDefined(); 
                expect(scope.error).toBe('Error!'); 
            });
            it ('should recognize and generate Marketplace flag based on isPayment_Plan call returning Marketplace:1', function() {
                var paymentRecord = {"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1};
                $httpBackend.expect('GET', 'isPayment_Plan/25/1').respond(200, {'clean':'clean'});
                $httpBackend.expect('POST', '/paymentplan').respond(500, {'message':'Error!'});
                
                expect(scope.error).toBeUndefined(); 
                scope.createPayment(paymentRecord);
                $httpBackend.flush(2);
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                
                expect(scope.error).toBeDefined(); 
                expect(scope.error).toBe('Error!'); 
            });
        });

        describe('scope.cancel testing', function() {
            it('should call dismiss on the $modalInstance', function() {
                scope.cancel();
                expect(modalInstance.dismiss).toHaveBeenCalledWith('cancel');
            });
            it('should close the $modalInstance', function() {
                scope.cancel();
                expect(modalInstance.close).toHaveBeenCalledWith();
            });
        });
    });
}());