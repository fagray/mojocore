/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('SubscriptionController', function() {
        // Initialize global variables
        var SaleController,
            scope,
            $q,
            $location,
            $locale,
            $httpBackend,
            $modal,
            Authentication,
            VaultService,
            RecurringPaymentPlanService,
            PaymentService,
            SubscriptionService,
            SubscriptionFindOne,
            ProductService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$locale_, _$q_, _$httpBackend_, _$modal_, _Authentication_,_VaultService_, _RecurringPaymentPlanService_,_PaymentService_,_SubscriptionService_,_SubscriptionFindOne_,_ProductService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            $q = _$q_;
            $location = _$location_;
            $locale = _$locale_;
            $httpBackend = _$httpBackend_;
            $modal = _$modal_;
            
            // Point global variables to injected services
            VaultService = _VaultService_;
            RecurringPaymentPlanService = _RecurringPaymentPlanService_;
            PaymentService = _PaymentService_;
            SubscriptionService = _SubscriptionService_;
            SubscriptionFindOne = _SubscriptionFindOne_;
            ProductService = _ProductService_;
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });
            it('should have variable scope.param defined', function() {
                expect(scope.param).toBeDefined();
            });
            it('should have variable scope.dateError defined', function() {
                expect(scope.dateError).toBeDefined();
            });
            it('should have variable scope.customers defined', function() {
                expect(scope.customers).toBeDefined();
            });
            it('should have variable scope.recurring_plans defined', function() {
                expect(scope.recurring_plans).toBeDefined();
            });
            it('should have variable scope.startDays defined', function() {
                expect(scope.startDays).toBeDefined();
            });
            it('should have variable scope.startMonths defined', function() {
                expect(scope.startMonths).toBeDefined();
            });
            it('should have variable scope.startYears defined', function() {
                expect(scope.startYears).toBeDefined();
            });
            it('should have variable scope.startYear defined', function() {
                expect(scope.startYear).toBeDefined();
            });
           it('should have variable scope.startMonth defined', function() {
                expect(scope.startMonth).toBeDefined();
            });
            it('should have variable scope.startDay defined', function() {
                expect(scope.startDay).toBeDefined();
            });
            it('should have function scope.subCheckCreate defined', function() {
                expect(scope.subCheckCreate).toBeDefined();
            });
            it('should have function scope.subCheckDate defined', function() {
                expect(scope.subCheckCreate).toBeDefined();
            });
            it('should have function scope.subscriptionInitialize defined', function() {
                expect(scope.subscriptionInitialize).toBeDefined();
            });
            it('should have function scope.getPlanList defined', function() {
                expect(scope.getPlanList).toBeDefined();
            });
            it('should have function scope.subscriptionQueryInitialize defined', function() {
                expect(scope.subscriptionQueryInitialize).toBeDefined();
            });
            it('should have function scope.submitSubscription defined', function() {
                expect(scope.submitSubscription).toBeDefined();
            });
            it('should have function scope.displaySubConfirmation defined', function() {
                expect(scope.displaySubConfirmation).toBeDefined();
            });
            it('should have function scope.subscriptionWrap defined', function() {
                expect(scope.subscriptionWrap).toBeDefined();
            });
            it('should have function scope.verifySubDelete defined', function() {
                expect(scope.verifySubDelete).toBeDefined();
            });
            it('should have function scope.confirmationSubModal defined', function() {
                expect(scope.confirmationSubModal).toBeDefined();
            });
        });

        describe('Interface', function() {
            it('should recognize subCheckCreate as a function', function() {
                expect(angular.isFunction(scope.subCheckCreate)).toBe(true);
            });
            it('should recognize subCheckDate as a function', function() {
                expect(angular.isFunction(scope.subCheckDate)).toBe(true);
            });
            it('should recognize subscriptionInitialize as a function', function() {
                expect(angular.isFunction(scope.subscriptionInitialize)).toBe(true);
            });
            it('should recognize getPlanList as a function', function() {
                expect(angular.isFunction(scope.getPlanList)).toBe(true);
            });
            it('should recognize subscriptionQueryInitialize as a function', function() {
                expect(angular.isFunction(scope.subscriptionQueryInitialize)).toBe(true);
            });
            it('should recognize submitSubscription as a function', function() {
                expect(angular.isFunction(scope.submitSubscription)).toBe(true);
            });
            it('should recognize displaySubConfirmation as a function', function() {
                expect(angular.isFunction(scope.displaySubConfirmation)).toBe(true);
            });
            it('should recognize subscriptionWrap as a function', function() {
                expect(angular.isFunction(scope.subscriptionWrap)).toBe(true);
            });
            it('should recognize verifySubDelete as a function', function() {
                expect(angular.isFunction(scope.verifySubDelete)).toBe(true);
            });
            it('should recognize confirmationSubModal as a function', function() {
                expect(angular.isFunction(scope.confirmationSubModal)).toBe(true);
            });
        });

        describe('Initial Values', function() {
            it('should have variable scope.param defined', function() {
                expect(scope.param).toEqual(['']);
            });
            it('should have variable scope.dateError defined', function() {
                expect(scope.dateError).toBe('');
            });
            it('should have variable scope.customers defined', function() {
                expect(scope.customers).toEqual([]);
            });
            it('should have variable scope.recurring_plans defined', function() {
                expect(scope.recurring_plans).toEqual([]);
            });
            it('should have variable scope.startDays defined', function() {
                expect(scope.startDays).toEqual([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]);
            });
            it('should have variable scope.startMonths defined', function() {
                expect(scope.startMonths).toEqual([ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]);
            });
            it('should have variable scope.startYears defined', function() {
                var years = [];
                var thisYear = new Date().getFullYear();
                years.push(thisYear);
                for (var i = 1; i <= 5; i++) {
                    years.push(thisYear + i);
                }
                expect(scope.startYears).toEqual(years);
            });
            it('should have variable scope.startYear defined', function() {
                var thisYear = new Date().getFullYear();
                expect(scope.startYear).toBe(thisYear);
            });
           it('should have variable scope.startMonth defined', function() {
                var thisMonthNum = new Date().getMonth();
                var thisMonth = '';
                switch(thisMonthNum) {
                    case 0: 
                        thisMonth = 'Jan';
                        break;
                    case 1: 
                        thisMonth = 'Feb';
                        break;
                    case 2: 
                        thisMonth = 'Mar';
                        break;
                    case 3: 
                        thisMonth = 'Apr';
                        break;
                    case 4: 
                        thisMonth = 'May';
                        break;
                    case 5: 
                        thisMonth = 'Jun';
                        break;
                    case 6: 
                        thisMonth = 'Jul';
                        break;
                    case 7: 
                        thisMonth = 'Aug';
                        break;
                    case 8: 
                        thisMonth = 'Sep';
                        break;
                    case 9: 
                        thisMonth = 'Oct';
                        break;
                    case 10: 
                        thisMonth = 'Nov';
                        break;
                    case 11: 
                        thisMonth = 'Dec';
                        break;
                }
                expect(scope.startMonth).toBe(thisMonth);
            });
            it('should have variable scope.startDay defined', function() {
                var thisDate = new Date().getDate();
                expect(scope.startDay).toBe(thisDate);
            });
         });

        describe('subCheckCreate function', function() {
            it('should return true', function() {
                scope.param = ['','subscriptioncreate'];
                expect(scope.subCheckCreate()).toBe(true);
                scope.param = ['','subscriptioncreate','25'];
                expect(scope.subCheckCreate()).toBe(false);
                scope.param = ['','subscriptionedit'];
                expect(scope.subCheckCreate()).toBe(false);
                scope.param = ['','subscriptionedit', '25'];
                expect(scope.subCheckCreate()).toBe(false);
            });
        });

        describe('subCheckDate function', function() {
            it('should clear scope.error if it is set', function() {
                scope.error = 'The date to start the subscription is invalid';
                scope.subCheckDate();
                scope.$digest();
                expect(scope.error).toBe('');
            });
            it('should clear scope.dateError if the form entry returns valid', function() {
                scope.subCheckDate();
                scope.$digest();
                expect(scope.dateError).toBe('');
            });
            it('should return scope.dateError if the year entered is in the past', function() {
                scope.startYear = 2000; 
                scope.subCheckDate();
                scope.$digest();
                expect(scope.dateError).toBe('The date is in the past');
            });
            it('should return scope.dateError if the month entered is in the past', function() {
                var theMonths = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
                var theMonthNumber = new Date().getMonth() - 1;
                if (theMonthNumber < 0) {
                    theMonthNumber = 11;
                }
                scope.startMonth = theMonths[theMonthNumber];
                scope.subCheckDate();
                scope.$digest();
                expect(scope.dateError).toBe('The date is in the past');
            });
            it('should return scope.dateError if the day entered is in the past', function() {
                var theDate = new Date().getDate() - 1;
                if (theDate < 1) {
                    var theMonths = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
                    var theMonthNumber = new Date().getMonth() - 1;
                    if (theMonthNumber < 0) {
                        theMonthNumber = 11;
                    }
                    scope.startMonth = theMonths[theMonthNumber];
                
                    theDate = 28;
                }
                scope.startDay = theDate;
                scope.subCheckDate();
                scope.$digest();
                expect(scope.dateError).toBe('The date is in the past');
            });
            it('should return scope.dateError if the entered date is invalid', function() {
                scope.startYear = 2016;
                scope.startmonth = 'Feb';
                scope.startYear = 30;
                scope.subCheckDate();
                scope.$digest();
                expect(scope.dateError).toBe('The date is invalid');
            });
        });

        describe('subscriptionInitialize function', function() {
            var vaultDeferred,
                recurringPaymentPlanDeferred,
                $promise;
                    
            beforeEach(function() {
                vaultDeferred = $q.defer();
                vaultDeferred.resolve({response: [{"id":"1004916088","first_name":"John","last_name":"Doe","customer_vault_id":"1004916088"},{"id":"1102934846","first_name":"Jane","last_name":"Doe","customer_vault_id":"1102934846"}]});

                recurringPaymentPlanDeferred = $q.defer();
                recurringPaymentPlanDeferred.resolve({response: [{"Plan_ID":"40222","Plan_Name":"The First Plan"},{"Plan_ID":"40223","Plan_Name":"The Second Plan"}]});

                spyOn(VaultService, 'get').and.returnValue({$promise: vaultDeferred.promise});
                spyOn(RecurringPaymentPlanService, 'get').and.returnValue({$promise: recurringPaymentPlanDeferred.promise});
            });
            
            it('should set scope.recurring_plans', function() {
                scope.subscriptionInitialize();
                expect(scope.recurring_plans).toEqual([{ id: 0, value: 'Select a Plan for this Subscription' }]);
            });
            it('should call VaultService.get', function() {
                scope.subscriptionInitialize();
                scope.$digest();
                expect(VaultService.get).toHaveBeenCalledWith({ boarding: 25 });
            });
            it('should call VaultService.get', function() {
                scope.subscriptionInitialize();
                scope.$digest();
                expect(RecurringPaymentPlanService.get).toHaveBeenCalledWith({ boardingId: 25, productId: 'all' });
            });
            it('should set scope.customers', function() {
                scope.subscriptionInitialize();
                scope.$digest();
                expect(scope.customers).toEqual([ { id: '1004916088', value: 'John Doe' }, { id: '1102934846', value: 'Jane Doe' } ]);
            });
            it('should modify scope.recurring_plans', function() {
                scope.subscriptionInitialize();
                scope.$digest();
                expect(scope.recurring_plans).toEqual([{ id: 0, value: 'Select a Plan for this Subscription' }, { id: '40222', value: '40222 - The First Plan' }, { id: '40223', value: '40223 - The Second Plan' }]);
            });
            it('should not set scope.recurring_plan', function() {
                scope.param = ['','subscriptionedit','2022'];
                scope.subscriptionInitialize();
                scope.$digest();
                expect(scope.recurring_plan).toBeUndefined();
            });
            it('should not set scope.recurring_plan', function() {
                scope.param = ['','subscriptioncreate'];
                scope.subscriptionInitialize();
                scope.$digest();
                expect(scope.recurring_plan).toEqual({ id: 0, value: 'Select a Plan for this Subscription' });
            });
        });

        describe('getPlanList function', function() {
            var recurringPaymentPlanDeferred,
                $promise;
                    
            beforeEach(function() {
                recurringPaymentPlanDeferred = $q.defer();
                recurringPaymentPlanDeferred.resolve({response: [{"Plan_ID":"40222","Plan_Name":"The First Plan"},{"Plan_ID":"40223","Plan_Name":"The Second Plan"}]});

                spyOn(RecurringPaymentPlanService, 'get').and.returnValue({$promise: recurringPaymentPlanDeferred.promise});
            });
            
            it('should call RecurringPaymentPlanService.get', function() {
                var result = scope.getPlanList();
                scope.$digest();
                expect(RecurringPaymentPlanService.get).toHaveBeenCalledWith({ boardingId: 25, productId: 'all' });
            });
//            it('should return a list of plans', function() {
//                var result = scope.getPlanList();
//                scope.$digest();
//                expect(result).toEqual({ boardingId: 25, productId: 'all' });
//            });
        });

        describe('subscriptionQueryInitialize function', function() {
            var recurringPaymentPlanDeferred,
                $promise;
                    
            beforeEach(function() {
                recurringPaymentPlanDeferred = $q.defer();
                recurringPaymentPlanDeferred.resolve({response: [{"Plan_ID":"40222","Plan_Name":"The First Plan"},{"Plan_ID":"40223","Plan_Name":"The Second Plan"}]});

                spyOn(RecurringPaymentPlanService, 'get').and.returnValue({$promise: recurringPaymentPlanDeferred.promise});
            });
            
            it('should set scope.samplePlans', function() {
                expect(scope.samplePlans).toBeUndefined();
                scope.subscriptionQueryInitialize();
                scope.$digest();
                expect(scope.samplePlans).toBeDefined();
                expect(scope.samplePlans).toEqual([{ id: 'all', value: 'All Plans' }, { id: '40222', value: 'The First Plan' }, { id: '40223', value: 'The Second Plan' }]);
            });
            
            it('should set scope.Plan_NameSearchTransactions', function() {
                expect(scope.Plan_NameSearchTransactions).toBeUndefined();
                scope.subscriptionQueryInitialize();
                scope.$digest();
                expect(scope.Plan_NameSearchTransactions).toBeDefined();
                expect(scope.Plan_NameSearchTransactions).toEqual({ id: 'all', value: 'All Plans' });
            });
        });

        describe('displaySubConfirmation function', function() {
			beforeEach(function(){
                this.element = $('<form id="fieldsForm" name="fieldsForm"><select id="recurring_plan"><option value="0">Value 0</option><option value="1">Value 1</option></select></form><div id="themessageP" style="display:none">Message</div>');
				this.element.appendTo('body');
                
                inject(function ($rootScope, $compile) {
                    this.element = $compile(this.element)(scope);
                });
                
                var fieldsForm = scope.fieldsForm;
                spyOn(fieldsForm,'$setPristine').and.returnValue(true);
			});
			
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});
			
            it('should call clearSubscriptionForm() function', function() {
                scope.customer = [{id: 1}];
                document.getElementById('recurring_plan').selectedIndex = 1;
                scope.startYear = 1000;
                scope.startMonth = "blue";
                scope.startDay = -55;
                scope.order_id = '1234567890';
                scope.order_description = 'theOrderDescription';
                scope.po_number = '987654311';
                scope.shipping = '10';
                scope.tax = '4';
                scope.tax_exempt = true;
                scope.displaySubConfirmation(1,0,'');
                var recurr_plan_index = document.getElementById('recurring_plan').selectedIndex;
                expect(scope.customer).toEqual('');
                expect(recurr_plan_index).toBe(0);
                expect(scope.startYear).toBe(scope.startYears[0]);
                expect(scope.startMonth).toBe(scope.startMonths[new Date().getMonth()]);
                expect(scope.startDay).toBe(scope.startDays[new Date().getDate() - 1]);
                expect(scope.order_id).toBe('');
                expect(scope.order_description).toBe('');
                expect(scope.po_number).toBe('');
                expect(scope.shipping).toBe('');
                expect(scope.tax).toBe('');
                expect(scope.tax_exempt).toBe(false);
                var fieldsForm = scope.fieldsForm;
                expect(fieldsForm.$setPristine).toHaveBeenCalled();
			});
			
            it('should call scope.confirmationModal() function', function() {
                spyOn(scope,'confirmationModal').and.returnValue(true);
                scope.displaySubConfirmation(1,1,'This is an error message');
                scope.$digest();
                expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Add Subscriptions', message: '1 subscription(s) created successfully\n1 subscription(s) failed\nThis is an error message', accept: '', deny: 'OK' });   
            });
        });
        
        describe('submitSubscription I function', function() {
            it('should return false if scope.customer.id === 0', function() {
                scope.customer = {id: 0};
                var postData = '';
                expect(scope.submitSubscription(postData)).toBe(false);
                expect(scope.error).toBe('You must choose a customer for this subscription');
            });
            it('should return false if scope.recurring_plan.id === 0', function() {
                scope.customer = {id: 1};
                scope.recurring_plan = {id: 0};
                var postData = '';
                expect(scope.submitSubscription(postData)).toBe(false);
                expect(scope.error).toBe('You must choose a plan for this subscription');
            });
            it('should return false if scope.subCheckDate() fails', function() {
                scope.customer = {id: 1};
                scope.recurring_plan = {id: 1};
                scope.startYear = 2000; 
                var postData = '';
                expect(scope.submitSubscription(postData)).toBe(false);
                expect(scope.error).toBe('The date to start the subscription is invalid');
            });
            it('should post to /subscription if form is valid', function() {
                spyOn(scope,'displaySubConfirmation').and.returnValue(true);
                $httpBackend.when('POST', '/subscription').respond(200, {response:''});
                scope.customer = [{id: 1}];
                scope.recurring_plan = {id: 1};
                var postData = '';
                scope.submitSubscription(postData);
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should call scope.displaySubConfirmation with no message if post returns with status 200', function() {
                spyOn(scope,'displaySubConfirmation').and.returnValue(true);
                $httpBackend.when('POST', '/subscription').respond(200, {response:''});
                scope.customer = [{id: 1}];
                scope.recurring_plan = {id: 1};
                var postData = '';
                scope.submitSubscription(postData);
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.displaySubConfirmation).toHaveBeenCalledWith( 1, 0, '');
            });
            it('should call scope.displaySubConfirmation with more than one seccess value', function() {
                spyOn(scope,'displaySubConfirmation').and.returnValue(true);
                $httpBackend.when('POST', '/subscription').respond(200, {response:''});
                scope.customer = [{id: 1},{id: 2}];
                scope.recurring_plan = {id: 1};
                var postData = '';
                scope.submitSubscription(postData);
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.displaySubConfirmation).toHaveBeenCalledWith( 2, 0, '');
            });
        });

        describe('submitSubscription II function', function() {
            it('should call scope.displaySubConfirmation', function() {
                spyOn(scope,'displaySubConfirmation').and.returnValue(true);
                $httpBackend.when('POST', '/subscription').respond(500, 'Error');
                scope.customer = [{id: 1}];
                scope.recurring_plan = {id: 1};
                var postData = '';
                scope.submitSubscription(postData);
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect('Able to test subscription.client.controller.js submitSubscription() function with failled database insert').toBe(true);
                //expect(scope.displaySubConfirmation).toHaveBeenCalledWith( 0, 1, '');
            });
        });

        describe('subscriptionWrap I function', function() {
            var subscriptionDeferred,
                $promise;

			beforeEach(function(){
                this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search">');
				this.element.appendTo('body');

                subscriptionDeferred = $q.defer();
                subscriptionDeferred.resolve({response: []});

                spyOn(SubscriptionService, 'get').and.returnValue({$promise: subscriptionDeferred.promise});
			});
			
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});
			
            it('should define and initialize working variables', function() {
                expect(scope.noResults).toBeUndefined();
                expect(scope.showSearchSubscriptions).toBeUndefined();
                expect(scope.sItems).toBeUndefined();
                scope.productId = {productName: 4};
                scope.Plan_NameSearchTransactions = {id: 99};
                scope.firstName = 'John';
                scope.lastName = 'Doe';
                scope.email = 'email@email.com';
                scope.last4ofCC = '1111';
                
                scope.subscriptionWrap();
                expect(scope.noResults).toBeDefined();
                expect(scope.noResults).toBe(false);
                expect(scope.showSearchSubscriptions).toBeDefined();
                expect(scope.showSearchSubscriptions).toBe(false);
                expect(scope.sItems).toBeDefined();
                expect(scope.sItems).toEqual([]);
			});
            it('should call function toggleLoader() and hide the submit button while showing the loader image', function() {
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('');
                scope.productId = {productName: 4};
                scope.Plan_NameSearchTransactions = {id: 99};
                scope.firstName = 'John';
                scope.lastName = 'Doe';
                scope.email = 'email@email.com';
                scope.last4ofCC = '1111';
                
                scope.subscriptionWrap();
                expect(document.getElementById('loadingImg').style.display).toBe('block');
                expect(document.getElementById('submitButton').style.display).toBe('none');
			});
            it('should call function toggleLoader() and show the submit button and hide the loader image when function completes with no records', function() {
                scope.productId = {productName: 4};
                scope.Plan_NameSearchTransactions = {id: 99};
                scope.firstName = 'John';
                scope.lastName = 'Doe';
                scope.email = 'email@email.com';
                scope.last4ofCC = '1111';
                
                scope.subscriptionWrap();
                scope.$digest();
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('inline-block');
            });
            it('should call SubscriptionService.get', function() {
                scope.productId = {productName: 4};
                scope.Plan_NameSearchTransactions = {id: 99};
                scope.firstName = 'John';
                scope.lastName = 'Doe';
                scope.email = 'email@email.com';
                scope.last4ofCC = '1111';
                
                scope.subscriptionWrap();
                scope.$digest();
                expect(SubscriptionService.get).toHaveBeenCalledWith({ boarding: 25, product: 4, planId: 99, firstName: 'John', lastName: 'Doe', email: 'email@email.com', last4cc: '1111' });
            });
            it('should call leave scope.items undefined', function() {
                scope.productId = {productName: 4};
                scope.Plan_NameSearchTransactions = {id: 99};
                scope.firstName = 'John';
                scope.lastName = 'Doe';
                scope.email = 'email@email.com';
                scope.last4ofCC = '1111';
                
                scope.subscriptionWrap();
                scope.$digest();
                 expect(scope.sItems).toEqual([]);
                
            });
            it('should call set scope.noResults to true if there are no records returned', function() {
                scope.productId = {productName: 4};
                scope.Plan_NameSearchTransactions = {id: 99};
                scope.firstName = 'John';
                scope.lastName = 'Doe';
                scope.email = 'email@email.com';
                scope.last4ofCC = '1111';
                
                scope.subscriptionWrap();
                scope.$digest();
                expect(scope.noResults).toBe(true);
            });
        });

        describe('subscriptionWrap II function', function() {
            var subscriptionDeferred,
                $promise;

			beforeEach(function(){
                this.element = $('<img id="loadingImg" style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search">');
				this.element.appendTo('body');

                subscriptionDeferred = $q.defer();
                subscriptionDeferred.resolve({response: [
                    {sub_ID: '12',Plan_Name: 'plan 1',first_name: 'John',last_name: 'Doe', Order_ID: '111',cc_number: '4xxxxxxxxxxx1111',Created:'2015-12-14T00:00:00.000Z',Month_Frequency:0,Day_Of_Month:0,Day_Frequency:10,Completed_Payments:0,Plan_Amount:110,Attempted_Payments:4,Plan_Payments:0, Next_Charge_Date: '20160510'},
                    {sub_ID: '13',Plan_Name: 'plan 2',first_name: 'Tom',last_name: 'Slick', Order_ID: '112',cc_number: '4xxxxxxxxxxx1111',Created:'2015-12-14T00:00:00.000Z',Month_Frequency:2,Day_Of_Month:15,Day_Frequency:0,Completed_Payments:1,Plan_Amount:120,Attempted_Payments:3,Plan_Payments:0, Next_Charge_Date: '20160510'},
                    {sub_ID: '14',Plan_Name: 'plan 3',first_name: 'Dick',last_name: 'Trixter', Order_ID: '113',cc_number: '4xxxxxxxxxxx1111',Created:'2015-12-14T00:00:00.000Z',Month_Frequency:1,Day_Of_Month:15,Day_Frequency:0,Completed_Payments:2,Plan_Amount:130,Attempted_Payments:2,Plan_Payments:0, Next_Charge_Date: '20160510'},
                    {sub_ID: '15',Plan_Name: 'plan 4',first_name: 'Harry',last_name: 'Solomon', Order_ID: '114',cc_number: '4xxxxxxxxxxx1111',Created:'2015-12-14T00:00:00.000Z',Month_Frequency:2,Day_Of_Month:15,Day_Frequency:10,Completed_Payments:3,Plan_Amount:140,Attempted_Payments:1,Plan_Payments:99, Next_Charge_Date: '20160510'},
                    {sub_ID: '16',Plan_Name: 'plan 5',first_name: 'Mortimer',last_name: 'Mouse', Order_ID: '115',cc_number: '4xxxxxxxxxxx1111',Created:'2015-12-14T00:00:00.000Z',Month_Frequency:2,Day_Of_Month:15,Day_Frequency:10,Completed_Payments:4,Plan_Amount:150,Attempted_Payments:0,Plan_Payments:99, Next_Charge_Date: '20160510'}
                ]});

                spyOn(SubscriptionService, 'get').and.returnValue({$promise: subscriptionDeferred.promise});
			});
			
			// clean up
			afterEach(function(){
				this.element.remove(); 
			});
			
            it('should add subscriptions to the sItems array if SubscriptionService.get returned something', function() {
                expect(scope.sItems).toBeUndefined();
                var thedate = new Date(2016,4,10,0,0,0)
                scope.productId = {productName: 4};
                scope.Plan_NameSearchTransactions = {id: 99};
                scope.firstName = 'John';
                scope.lastName = 'Doe';
                scope.email = 'email@email.com';
                scope.last4ofCC = '1111';
                
                scope.subscriptionWrap();
                scope.$digest();
                expect(scope.sItems).toBeDefined();
                expect(scope.sItems[0]).toEqual({ id: '12', Plan_Name: 'plan 1', First_Name: 'John', Last_Name: 'Doe', Order_ID: '111', CC_Number: '4xxxxxxxxxxx1111', Created: '2015-12-14T00:00:00.000Z', Payments: '0 @ $110.00\n4 attempted', Next_Charge_Date: thedate });
                expect(scope.sItems[1]).toEqual({ id: '13', Plan_Name: 'plan 2', First_Name: 'Tom', Last_Name: 'Slick', Order_ID: '112', CC_Number: '4xxxxxxxxxxx1111', Created: '2015-12-14T00:00:00.000Z', Payments: '1 @ $120.00\n3 attempted', Next_Charge_Date: thedate });
                expect(scope.sItems[2]).toEqual({ id: '14', Plan_Name: 'plan 3', First_Name: 'Dick', Last_Name: 'Trixter', Order_ID: '113', CC_Number: '4xxxxxxxxxxx1111', Created: '2015-12-14T00:00:00.000Z', Payments: '2 @ $130.00\n2 attempted', Next_Charge_Date: thedate });
                expect(scope.sItems[3]).toEqual({ id: '15', Plan_Name: 'plan 4', First_Name: 'Harry', Last_Name: 'Solomon', Order_ID: '114', CC_Number: '4xxxxxxxxxxx1111', Created: '2015-12-14T00:00:00.000Z', Payments: '3 @ $140.00\n1 attempted\n96 left', Next_Charge_Date: thedate });
                expect(scope.sItems[4]).toEqual({ id: '16', Plan_Name: 'plan 5', First_Name: 'Mortimer', Last_Name: 'Mouse', Order_ID: '115', CC_Number: '4xxxxxxxxxxx1111', Created: '2015-12-14T00:00:00.000Z', Payments: '4 @ $150.00\n0 attempted\n95 left', Next_Charge_Date: thedate });
            });
        });

        describe('verifySubDelete function', function() {
            it('should call scope.displaySubConfirmation', function() {
                spyOn(scope,'confirmationSubModal').and.returnValue(true);
                scope.verifySubDelete(9090);
                expect(scope.confirmationSubModal).toHaveBeenCalledWith( '', { title: 'Delete Subscription', message: 'Do you wish to delete this subscription?', accept: 'Yes', deny: 'No', planId: 9090, delete: true });
            });
        });

        describe('confirmationSubModal function', function() {
            it('should do something', function() {
                scope.confirmationSubModal('med', {message:'test', planId:9090}); 
            });
        });

    });
}());