/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('AuditRecurrPlanController', function() {
        // Initialize global variables
        var $q,
            SaleController,
            AuditRecurrPlanController,
            AuditRecurrPlanHeaderService,
            AuditRecurrPlanDetailService,
            scope,
            $location,
            Authentication,
            AuditRecurrPlanService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope,_$location_, _$q_, _Authentication_,_AuditRecurrPlanService_,_AuditRecurrPlanHeaderService_,_AuditRecurrPlanDetailService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $q = _$q_;
            $location = _$location_;
            AuditRecurrPlanService = _AuditRecurrPlanService_;
            AuditRecurrPlanHeaderService = _AuditRecurrPlanHeaderService_;
            AuditRecurrPlanDetailService = _AuditRecurrPlanDetailService_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });
            it('should have scope.convertDate', function() {
                expect(scope.convertDate).toBeDefined();
            });
            it('should have scope.Recurring_Changes_ActivityWrap', function() {
                expect(scope.Recurring_Changes_ActivityWrap).toBeDefined();
            });
            it('should have scope.Recurring_Changes_Detail_ActivityWrap', function() {
                expect(scope.Recurring_Changes_Detail_ActivityWrap).toBeDefined();
            });
         });

        describe('Interface', function() {
            it('should recognize convertDate as a function', function() {
                expect(angular.isFunction(scope.convertDate)).toBe(true);
            });
            it('should recognize Recurring_Changes_ActivityWrap as a function', function() {
                expect(angular.isFunction(scope.Recurring_Changes_ActivityWrap)).toBe(true);
            });
            it('should recognize Recurring_Changes_Detail_ActivityWrap as a function', function() {
                expect(angular.isFunction(scope.Recurring_Changes_Detail_ActivityWrap)).toBe(true);
            });
        });

        describe('scope.convertDate function', function() {
            var dates = ['2015-02-03','02-03-2015','02/03/2015','02.03.2015','2/3/2015'];
            var desiredDate1 = '2015-02-03T00:00:00';
            var desiredDate2 = '2015-02-03T23:59:59';

            function testdate(date, formatted_date1, formatted_date2) {
                it('should return proper date formats given various inputs', function() {
                    var returnedDate1 = scope.convertDate('start', date);
                    expect(returnedDate1).toEqual(new Date(formatted_date1));
                    var returnedDate2 = scope.convertDate('end', date);
                    expect(returnedDate2).toEqual(new Date(formatted_date2));
                });
            }

            for( var i = 0; i < dates.length; i++) {
                testdate(dates[i], desiredDate1, desiredDate2);
            }

            it('should return Invalid Date if full year is not put in', function() {
                var returnedDate = scope.convertDate('start', '02/03/15');
                expect(returnedDate.toString()).toBe('Invalid Date');
            });
        });

        describe('scope.Recurring_Changes_ActivityWrap function', function() {
            beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search">');
				this.element.appendTo('body');
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should display the loader image and hide the submit button ', function() {
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('');
                scope.Recurring_Changes_ActivityWrap();
                expect(document.getElementById('loadingImg').style.display).toBe('block');
                expect(document.getElementById('submitButton').style.display).toBe('none');
            });
            it('should initalize scope.auditItems', function() {
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                expect(scope.auditItems).toBeUndefined();
                scope.Recurring_Changes_ActivityWrap();
                expect(scope.auditItems).toBeDefined();
                expect(scope.auditItems).toEqual([]);
            });
            it('should initalize scope.alerts', function() {
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                expect(scope.alerts).toBeUndefined();
                scope.Recurring_Changes_ActivityWrap();
                expect(scope.alerts).toBeDefined();
                expect(scope.alerts).toEqual([]);
            });
            it('should initalize scope.showReport', function() {
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                expect(scope.showReport).toBeUndefined();
                scope.Recurring_Changes_ActivityWrap();
                expect(scope.showReport).toBeDefined();
                expect(scope.showReport).toBe(false);
            });
            it('should initalize scope.noResults', function() {
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                expect(scope.noResults).toBeUndefined();
                scope.Recurring_Changes_ActivityWrap();
                expect(scope.noResults).toBeDefined();
                expect(scope.noResults).toBe(false);
            });
            it('should initalize scope.error', function() {
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                expect(scope.error).toBeUndefined();
                scope.Recurring_Changes_ActivityWrap();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe('');
            });
            it('should generate an error if start date is invalid', function() {
                scope.startDate = '15-01-01';
                scope.endDate = '2015-01-31';
                scope.Recurring_Changes_ActivityWrap();
                expect(scope.alerts).toBeDefined();
                expect(scope.alerts).toEqual([{type: 'danger', msg: 'The Start Date is invalid'}]);
            });
            it('should generate an error if end date is invalid', function() {
                scope.startDate = '2015-01-01';
                scope.endDate = '15-01-31';
                scope.Recurring_Changes_ActivityWrap();
                expect(scope.alerts).toBeDefined();
                expect(scope.alerts).toEqual([{type: 'danger', msg: 'The End Date is invalid'}]);
            });
            it('should close the loading image and show the submit button if there is a date error', function() {
                scope.startDate = '2015-01-01';
                scope.endDate = '15-01-31';
                scope.Recurring_Changes_ActivityWrap();
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('inline-block');
            });
            it('should call AuditRecurrPlanService.get with specific parameters if this is an regular user', function() {
                var AuditRecurrPlanDeferred,
                $promise;
                
                AuditRecurrPlanDeferred = $q.defer();
                AuditRecurrPlanDeferred.resolve({"Response":[{"id":1,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":1,"createdAt":"2015-10-20T16:33:47.000Z","updatedAt":"2015-10-20T16:33:47.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"},{"id":2,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":2,"createdAt":"2015-10-20T16:35:33.000Z","updatedAt":"2015-10-20T16:35:33.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"}]});
                
                spyOn(AuditRecurrPlanService, 'get').and.returnValue({$promise: AuditRecurrPlanDeferred.promise}); //returns a fake promise;
                
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                scope.productId = {productName: 22};
                scope.products = {productName: 44};
                scope.merchantId = {id: 88};
                scope.Recurring_Changes_ActivityWrap();
                scope.$digest();
                expect(AuditRecurrPlanService.get.calls.count()).toEqual(1);
                expect(AuditRecurrPlanService.get.calls.argsFor(0)[0]).toEqual(jasmine.objectContaining({productId : 22}));
                expect(AuditRecurrPlanService.get.calls.argsFor(0)[0]).toEqual(jasmine.objectContaining({boardingId: 25}));
            });
            it('should turn off loader image if no results are returned from AuditRecurrPlanService.get', function() {
                var AuditRecurrPlanDeferred,
                $promise;
                
                AuditRecurrPlanDeferred = $q.defer();
                AuditRecurrPlanDeferred.resolve({"Response":[]});
                
                spyOn(AuditRecurrPlanService, 'get').and.returnValue({$promise: AuditRecurrPlanDeferred.promise}); //returns a fake promise;
                
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                scope.productId = {productName: 22};
                scope.products = {productName: 44};
                scope.merchantId = {id: 88};
                scope.Recurring_Changes_ActivityWrap();
                scope.$digest();
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('inline-block');
            });
            it('should set view parameters if no results are returned from AuditRecurrPlanService.get', function() {
                var AuditRecurrPlanDeferred,
                $promise;
                
                AuditRecurrPlanDeferred = $q.defer();
                AuditRecurrPlanDeferred.resolve({"Response":[]});
                
                spyOn(AuditRecurrPlanService, 'get').and.returnValue({$promise: AuditRecurrPlanDeferred.promise}); //returns a fake promise;
                
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                scope.productId = {productName: 22};
                scope.products = {productName: 44};
                scope.merchantId = {id: 88};
                scope.Recurring_Changes_ActivityWrap();
                scope.$digest();
                expect(scope.noResults).toBe(true);
                expect(scope.showReport).toBe(false);
            });
            it('should set scope.auditItems if data is returned from  AuditRecurrPlanService.get', function() {
                var AuditRecurrPlanDeferred,
                $promise;
                
                AuditRecurrPlanDeferred = $q.defer();
                AuditRecurrPlanDeferred.resolve({"Response":[{"id":1,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":1,"createdAt":"2015-10-20T16:33:47.000Z","updatedAt":"2015-10-20T16:33:47.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"},{"id":2,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":2,"createdAt":"2015-10-20T16:35:33.000Z","updatedAt":"2015-10-20T16:35:33.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"}]});
                
                spyOn(AuditRecurrPlanService, 'get').and.returnValue({$promise: AuditRecurrPlanDeferred.promise}); //returns a fake promise;
                
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                scope.productId = {productName: 22};
                scope.products = {productName: 44};
                scope.merchantId = {id: 88};
                expect(scope.auditItems).toBeUndefined();
                scope.Recurring_Changes_ActivityWrap();
                scope.$digest();
                expect(scope.auditItems).toBeDefined();
                expect(scope.auditItems.length).toBe(2);
                expect(scope.auditItems).toEqual([{ id: 1, Boarding_ID: 25, Processor_ID: 'processor1', Recurring_Plan_ID: 1, createdAt: '20151020163347.000Z', updatedAt: '2015-10-20T16:33:47.000Z', Plan_Name: 'Plan1', Display_Name: 'First Last', Description: 'ProductName', Merchant_Name: 'Demo Account' },{ id: 2, Boarding_ID: 25, Processor_ID: 'processor1', Recurring_Plan_ID: 2, createdAt: '20151020163533.000Z', updatedAt: '2015-10-20T16:35:33.000Z', Plan_Name: 'Plan1', Display_Name: 'First Last', Description: 'ProductName', Merchant_Name: 'Demo Account' }]);
            });
            it('should turn off loader image and show submit button after scope.auditItems is built', function() {
                var AuditRecurrPlanDeferred,
                $promise;
                
                AuditRecurrPlanDeferred = $q.defer();
                AuditRecurrPlanDeferred.resolve({"Response":[{"id":1,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":1,"createdAt":"2015-10-20T16:33:47.000Z","updatedAt":"2015-10-20T16:33:47.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"},{"id":2,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":2,"createdAt":"2015-10-20T16:35:33.000Z","updatedAt":"2015-10-20T16:35:33.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"}]});
                
                spyOn(AuditRecurrPlanService, 'get').and.returnValue({$promise: AuditRecurrPlanDeferred.promise}); //returns a fake promise;
                
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                scope.productId = {productName: 22};
                scope.products = {productName: 44};
                scope.merchantId = {id: 88};
                scope.Recurring_Changes_ActivityWrap();
                scope.$digest();
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('inline-block');
            });
            it('should set view parameters after scope.auditItems is built', function() {
                var AuditRecurrPlanDeferred,
                $promise;
                
                AuditRecurrPlanDeferred = $q.defer();
                AuditRecurrPlanDeferred.resolve({"Response":[{"id":1,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":1,"createdAt":"2015-10-20T16:33:47.000Z","updatedAt":"2015-10-20T16:33:47.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"},{"id":2,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":2,"createdAt":"2015-10-20T16:35:33.000Z","updatedAt":"2015-10-20T16:35:33.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"}]});
                
                spyOn(AuditRecurrPlanService, 'get').and.returnValue({$promise: AuditRecurrPlanDeferred.promise}); //returns a fake promise;
                
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                scope.productId = {productName: 22};
                scope.products = {productName: 44};
                scope.merchantId = {id: 88};
                scope.Recurring_Changes_ActivityWrap();
                scope.$digest();
                expect(scope.noResults).toBe(false);
                expect(scope.showReport).toBe(true);
            });
            it('should set scope.err if AuditRecurrPlanService.get returns an error', function() {
                var AuditRecurrPlanDeferred,
                $promise;
                
                AuditRecurrPlanDeferred = $q.defer();
                AuditRecurrPlanDeferred.reject('This is an error response');
                
                spyOn(AuditRecurrPlanService, 'get').and.returnValue({$promise: AuditRecurrPlanDeferred.promise}); //returns a fake promise;
                
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                scope.productId = {productName: 22};
                scope.products = {productName: 44};
                scope.merchantId = {id: 88};
                expect(scope.error).toBeUndefined(); 
                scope.Recurring_Changes_ActivityWrap();
                scope.$digest();
                expect(scope.error).toBeDefined(); 
                expect(scope.error).toBe('This is an error response'); 
            });
        });

        describe('scope.Recurring_Changes_Detail_ActivityWrap function', function() {
            it('should initialize scope.auditItems', function() {
                expect(scope.auditItems).toBeUndefined();
                scope.Recurring_Changes_Detail_ActivityWrap();
                expect(scope.auditItems).toBeDefined();
                expect(scope.auditItems).toEqual([]);
            });
            it('should initialize scope.detailItems', function() {
                expect(scope.detailItems).toBeUndefined();
                scope.Recurring_Changes_Detail_ActivityWrap();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems).toEqual([]);
            });
            it('should initalize scope.error', function() {
                expect(scope.error).toBeUndefined();
                scope.Recurring_Changes_Detail_ActivityWrap();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe('');
            });
            it('should initialize scope.header_id', function() {
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');
                expect(scope.header_id).toBeUndefined();
                scope.Recurring_Changes_Detail_ActivityWrap();
                expect(scope.header_id).toBeDefined();
                expect(scope.header_id).toBe('102');
            });
            it('should call the AuditRecurrPlanHeaderService.get service', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise;
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":[{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_ID","Old_Value":null,"New_Value":"123456789","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}]});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(AuditRecurrPlanHeaderService.get).toHaveBeenCalledWith({ Header_ID: '102' });
            });
            it('should set scope.error if call the AuditRecurrPlanHeaderService.get service fails', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise;
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.reject('This is an error response');
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":[{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_ID","Old_Value":null,"New_Value":"123456789","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}]});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                expect(scope.error).toBeUndefined(); 
                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.error).toBe('This is an error response'); 
            });
            it('should set view flags upon successful call to AuditRecurrPlanHeaderService.get service', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise;
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":[{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_ID","Old_Value":null,"New_Value":"123456789","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}]});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                expect(scope.showReport).toBeUndefined();
                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.showReport).toBeDefined();
                expect(scope.showReport).toBe(true);
            });
            it('should set scope.auditItems successful call to AuditRecurrPlanHeaderService.get service', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    header = [{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":header});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":[{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_ID","Old_Value":null,"New_Value":"123456789","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}]});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.auditItems).toBeDefined();
                expect(scope.auditItems).toEqual(header);
            });
            it('should not set scope.auditItems if AuditRecurrPlanHeaderService.get service returns no records', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    header = [];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":header});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":[{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_ID","Old_Value":null,"New_Value":"123456789","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}]});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.auditItems).toBeDefined();
                expect(scope.auditItems).toEqual([]);
            });


            it('should call the AuditRecurrPlanDetailService.get service', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise;
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt"
:"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name"
:"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":[{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_ID","Old_Value":null,"New_Value":"123456789","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}]});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(AuditRecurrPlanDetailService.get).toHaveBeenCalledWith({ Audit_Header_ID: 102 });
            });
            it('should set scope.error if call the AuditRecurrPlanDetailService.get service fails', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise;
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt"
:"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name"
:"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.reject('This is an error response');
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                expect(scope.error).toBeUndefined(); 
                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.error).toBe('This is an error response'); 
            });
            it('should set scope.error if call the AuditRecurrPlanDetailService.get service fails', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise;
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt"
:"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name"
:"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.reject('This is an error response');
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                expect(scope.detailItems).toBeUndefined(); 
                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined(); 
                expect(scope.detailItems).toEqual([]); 
            });
            it('should set scope.auditItems on successful call to AuditRecurrPlanHeaderService.get service', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_ID","Old_Value":null,"New_Value":"123456789","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems).toEqual(detail);
            });
        });

        describe('Readable messages regarding changes to some of the fields', function() {
            
            // Created Plan
            it('should notify if this is a created Plan', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_ID","Old_Value":null,"New_Value":"123456789","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Plan created');
                expect(scope.detailItems[0].Old_Value).toEqual(null);
            });
            
            // Deleted Plan
            it('should notify if this is a deleted Plan', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Is_Active","Old_Value":"1","New_Value":"0","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Plan deleted');
            });
            
            // Plan Payments
            it('should notify if plan changed to "Charge Until Canceled"', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_Payments","Old_Value":"10","New_Value":"0","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Charge customer until cancelled');
                expect(scope.detailItems[0].Old_Value).toEqual('Charge customer a total of 10 times');
                expect(scope.detailItems[0].New_Value).toEqual('Charge customer until plan cancelled');
            });
            it('should notify if plan if changed from "Charge Until Canceled"', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_Payments","Old_Value":"0","New_Value":"10","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Charge customer 10 times');
                expect(scope.detailItems[0].Old_Value).toEqual('Stop charging customer until plan cancelled');
                expect(scope.detailItems[0].New_Value).toEqual('Charge customer a total of 10 times');
            });
            it('should notify if plan if changed from "Charge Until Canceled"', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_Payments","Old_Value":"20","New_Value":"10","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Charge customer 10 times');
                expect(scope.detailItems[0].Old_Value).toEqual('Charge customer 20 times');
                expect(scope.detailItems[0].New_Value).toEqual('Charge customer a total of 10 times');
            });
            
            // Day Frequency, New_Value = 0
            it('should notify if plan changed to "Charge the Customer Every x Days"', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Day_Frequency","Old_Value":"10","New_Value":"0","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Don\'t charge customer every 10 days');
                expect(scope.detailItems[0].Old_Value).toEqual('Charge customer every 10 days');
                expect(scope.detailItems[0].New_Value).toEqual(' ');
            });
            it('should notify if plan changed to "Charge the Customer Every x Days with new value"', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Day_Frequency","Old_Value":null,"New_Value":"0","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Plan created');
                expect(scope.detailItems[0].Old_Value).toEqual('');
                expect(scope.detailItems[0].New_Value).toEqual('Don\'t charge every "x" days');
            });
            it('should notify if plan changed to "Charge the Customer Every x Days with new value"', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Day_Frequency","Old_Value":"10","New_Value":"0","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Don\'t charge customer every 10 days');
                expect(scope.detailItems[0].Old_Value).toEqual('Charge customer every 10 days');
                expect(scope.detailItems[0].New_Value).toEqual(' ');
            });


            // Day Frequency, New_Value != 0
            it('should notify if plan changed to "Charge the Customer Every x Days with new value"', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Day_Frequency","Old_Value":"0","New_Value":"10","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Charge customer every 10 days');
                expect(scope.detailItems[0].Old_Value).toEqual('');
                expect(scope.detailItems[0].New_Value).toEqual('Charge customer every 10 days');
            });

            // Day Of Month, New_Value = 0
            it('should notify if plan changed fron "Charge the Customer Every x Days with new value"', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Day_Of_Month","Old_Value":"10","New_Value":"0","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Don\'t charge customer on day 10 of every "x" Months');
                expect(scope.detailItems[0].Old_Value).toEqual('');
                expect(scope.detailItems[0].New_Value).toEqual('Don\'t charge customer on day 10 of every "x" Months');
            });
            it('should notify if plan changed from "Charge the Customer Every x Days with new value"', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Day_Of_Month","Old_Value":null,"New_Value":"0","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Plan created');
                expect(scope.detailItems[0].Old_Value).toEqual('');
                expect(scope.detailItems[0].New_Value).toEqual('Don\'t charge customer on day "x" of every "y" Months');
            });

            // Day Of Month, New_Value != 0
            it('should notify if plan changed', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Day_Of_Month","Old_Value":"5","New_Value":"10","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Charge customer on day 10 of every "x" Months');
                expect(scope.detailItems[0].Old_Value).toEqual('Don\'t charge customer on day 10 of every "x" Months');
                expect(scope.detailItems[0].New_Value).toEqual('Charge customer on day 10 of every "x" Months');
            });
            it('should notify if plan changed', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Day_Of_Month","Old_Value":null,"New_Value":"10","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Plan created');
                expect(scope.detailItems[0].Old_Value).toEqual('');
                expect(scope.detailItems[0].New_Value).toEqual('Charge customer on day 10 of every "x" Months');
            });

            // Month Frequency, New_Value = 0
            it('should notify if plan changed', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Month_Frequency","Old_Value":"10","New_Value":"0","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Don\'t charge customer on day "x" of every 10 Months');
                expect(scope.detailItems[0].Old_Value).toEqual('');
                expect(scope.detailItems[0].New_Value).toEqual('Don\'t charge customer on day "x" of every 10 Months');
            });
            it('should notify if plan changed', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Month_Frequency","Old_Value":null,"New_Value":"0","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Plan created');
                expect(scope.detailItems[0].Old_Value).toEqual('');
                expect(scope.detailItems[0].New_Value).toEqual('Don\'t charge customer on day "x" of every "y" Months');
            });

            // Month Frequency, New_Value != 0
            it('should notify if plan changed', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Month_Frequency","Old_Value":"5","New_Value":"10","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Charge customer on day "x" of every 10 Months');
                expect(scope.detailItems[0].Old_Value).toEqual('Don\'t charge customer on day "x" of every 5 Months');
                expect(scope.detailItems[0].New_Value).toEqual('Charge customer on day "x" of every 10 Months');
            });
            it('should notify if plan created', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Month_Frequency","Old_Value":"0","New_Value":"10","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Charge customer on day "x" of every 10 Months');
                expect(scope.detailItems[0].Old_Value).toEqual('');
                expect(scope.detailItems[0].New_Value).toEqual('Charge customer on day "x" of every 10 Months');
            });
            it('should notify if plan created', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Month_Frequency","Old_Value":null,"New_Value":"10","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Description).toEqual('Plan created');
                expect(scope.detailItems[0].Old_Value).toEqual('');
                expect(scope.detailItems[0].New_Value).toEqual('Charge customer on day "x" of every 10 Months');
            });

            // Plan Amount
            it('should change plan amount into number with 2 decimal places', function() {
                var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_Amount","Old_Value":"5","New_Value":"10","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Old_Value).toEqual('5.00');
                expect(scope.detailItems[0].New_Value).toEqual('10.00');
            });
            it('should change plan amount New_Value into number with 2 decimal places', function() {
               var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_Amount","Old_Value":null,"New_Value":"10","createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Old_Value).toEqual(null);
                expect(scope.detailItems[0].New_Value).toEqual('10.00');
            });
            it('should change plan amount Old_Value into number with 2 decimal places', function() {
               var AuditRecurrPlanHeaderDeferred,
                    AuditRecurrPlanDetailDeferred,
                    $promise,
                    detail = [{"id":70,"Audit_Header_ID":13,"Field_Name":"Plan_Amount","Old_Value":"5","New_Value":null,"createdAt":"2015-10-30T19:11:39.000Z","updatedAt":"2015-10-30T19:11:39.000Z"}];
                
                AuditRecurrPlanHeaderDeferred = $q.defer();
                AuditRecurrPlanHeaderDeferred.resolve({"Response":[{"id":102,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":5,"createdAt":"2015-10-30T19:11:38.000Z","updatedAt":"2015-10-30T19:11:38.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"Plan","Merchant_Name":"Demo Account"}]});
                
                AuditRecurrPlanDetailDeferred = $q.defer();
                AuditRecurrPlanDetailDeferred.resolve({"Response":detail});
                
                spyOn(AuditRecurrPlanHeaderService, 'get').and.returnValue({$promise: AuditRecurrPlanHeaderDeferred.promise}); //returns a fake promise;
                spyOn(AuditRecurrPlanDetailService, 'get').and.returnValue({$promise: AuditRecurrPlanDetailDeferred.promise}); //returns a fake promise;
                spyOn($location,'path').and.returnValue('/reports/recurringplanauditdetails/102');

                scope.Recurring_Changes_Detail_ActivityWrap();
                scope.$digest();
                expect(scope.detailItems).toBeDefined();
                expect(scope.detailItems[0].Old_Value).toEqual('5.00');
                expect(scope.detailItems[0].New_Value).toEqual(null);
            });
        });

    });
}());

(function() {
    // Authentication controller Spec
    describe('AuditRecurrPlanController II', function() {
        // Initialize global variables
        var $q,
            SaleController,
            AuditRecurrPlanController,
            scope,
            Authentication,
            AuditRecurrPlanService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_,_AuditRecurrPlanService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'admin',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            AuditRecurrPlanService = _AuditRecurrPlanService_;
            $q = _$q_;
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));
        describe('scope.Recurring_Changes_ActivityWrap function', function() {
            beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search">');
				this.element.appendTo('body');
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should call AuditRecurrPlanService.get with specific parameters if this is an admin user', function() {
                var AuditRecurrPlanDeferred,
                $promise;
                
                AuditRecurrPlanDeferred = $q.defer();
                AuditRecurrPlanDeferred.resolve({"Response":[{"id":1,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":1,"createdAt":"2015-10-20T16:33:47.000Z","updatedAt":"2015-10-20T16:33:47.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"},{"id":2,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":2,"createdAt":"2015-10-20T16:35:33.000Z","updatedAt":"2015-10-20T16:35:33.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"}]});
                
                spyOn(AuditRecurrPlanService, 'get').and.returnValue({$promise: AuditRecurrPlanDeferred.promise}); //returns a fake promise;
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                scope.productId = {productName: 22};
                scope.products = {productName: 44};
                scope.merchantId = {id: 88};
                scope.Recurring_Changes_ActivityWrap();
                scope.$digest();
                expect(AuditRecurrPlanService.get.calls.count()).toEqual(1);
                expect(AuditRecurrPlanService.get.calls.argsFor(0)[0]).toEqual(jasmine.objectContaining({productId : 44}));
                expect(AuditRecurrPlanService.get.calls.argsFor(0)[0]).toEqual(jasmine.objectContaining({boardingId: 88}));
            });
            
        });
    });
}());

(function() {
    // Authentication controller Spec
    describe('AuditRecurrPlanController III', function() {
        // Initialize global variables
        var $q,
            SaleController,
            AuditRecurrPlanController,
            scope,
            Authentication,
            AuditRecurrPlanService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_,_AuditRecurrPlanService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'super',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            AuditRecurrPlanService = _AuditRecurrPlanService_;
            $q = _$q_;

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));
        describe('scope.Recurring_Changes_ActivityWrap function', function() {
            beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search">');
				this.element.appendTo('body');
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should call AuditRecurrPlanService.get with specific parameters if this is an admin user', function() {
                 var AuditRecurrPlanDeferred,
                $promise;
                
                AuditRecurrPlanDeferred = $q.defer();
                AuditRecurrPlanDeferred.resolve({"Response":[{"id":1,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":1,"createdAt":"2015-10-20T16:33:47.000Z","updatedAt":"2015-10-20T16:33:47.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"},{"id":2,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":2,"createdAt":"2015-10-20T16:35:33.000Z","updatedAt":"2015-10-20T16:35:33.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"}]});
                
                spyOn(AuditRecurrPlanService, 'get').and.returnValue({$promise: AuditRecurrPlanDeferred.promise}); //returns a fake promise;
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                scope.productId = {productName: 22};
                scope.products = {productName: 44};
                scope.merchantId = {id: 88};
                scope.Recurring_Changes_ActivityWrap();
                scope.$digest();
                expect(AuditRecurrPlanService.get.calls.count()).toEqual(1);
                expect(AuditRecurrPlanService.get.calls.argsFor(0)[0]).toEqual(jasmine.objectContaining({productId : 44}));
                expect(AuditRecurrPlanService.get.calls.argsFor(0)[0]).toEqual(jasmine.objectContaining({boardingId: 88}));
            });
            
        });
    });
}());
(function() {
    // Authentication controller Spec
    describe('AuditRecurrPlanController IV', function() {
        // Initialize global variables
        var $q,
            SaleController,
            AuditRecurrPlanController,
            scope,
            Authentication,
            AuditRecurrPlanService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _Authentication_,_AuditRecurrPlanService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'junk',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            AuditRecurrPlanService = _AuditRecurrPlanService_;
            $q = _$q_;

            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));
        describe('scope.Recurring_Changes_ActivityWrap function', function() {
            beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><input type="submit" id="submitButton" value="Search">');
				this.element.appendTo('body');
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should call AuditRecurrPlanService.get with specific parameters if this is an admin user', function() {
                 var AuditRecurrPlanDeferred,
                $promise;
                
                AuditRecurrPlanDeferred = $q.defer();
                AuditRecurrPlanDeferred.resolve({"Response":[{"id":1,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":1,"createdAt":"2015-10-20T16:33:47.000Z","updatedAt":"2015-10-20T16:33:47.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"},{"id":2,"Boarding_ID":25,"Processor_ID":"processor1","Recurring_Plan_ID":2,"createdAt":"2015-10-20T16:35:33.000Z","updatedAt":"2015-10-20T16:35:33.000Z","Plan_Name":"Plan1","Display_Name":"First Last","Description":"ProductName","Merchant_Name":"Demo Account"}]});
                
                spyOn(AuditRecurrPlanService, 'get').and.returnValue({$promise: AuditRecurrPlanDeferred.promise}); //returns a fake promise;
                scope.startDate = '2015-01-01';
                scope.endDate = '2015-01-31';
                scope.productId = {productName: 22};
                scope.products = {productName: 44};
                scope.merchantId = {id: 88};
                scope.Recurring_Changes_ActivityWrap();
                scope.$digest();
                expect(AuditRecurrPlanService.get.calls.count()).toEqual(1);
                expect(AuditRecurrPlanService.get.calls.argsFor(0)[0]).toEqual(jasmine.objectContaining({productId : ''}));
                expect(AuditRecurrPlanService.get.calls.argsFor(0)[0]).toEqual(jasmine.objectContaining({boardingId: ''}));
            });
            
        });
    });
}());
