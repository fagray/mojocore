/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('RecurringPaymentController', function() {
        // Initialize global variables
        var $q,
            SaleController, 
            scope,
            $httpBackend,
            $location,
            Authentication,
            ProductService,
            PaymentService,
            Nmitransactions,
            RecurringPlanService,
            CheckRecurringPlanService,
            RecurringPaymentPlanService,
            SubscriptionService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$httpBackend_, _$q_, _Authentication_, _ProductService_, _PaymentService_, _Nmitransactions_, _RecurringPlanService_, _CheckRecurringPlanService_, _RecurringPaymentPlanService_, _SubscriptionService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };

            // Point global variables to injected services
            $httpBackend = _$httpBackend_;
			$location = _$location_;
            $q = _$q_;
            ProductService = _ProductService_;
            PaymentService = _PaymentService_;
            Nmitransactions = _Nmitransactions_;
            RecurringPlanService = _RecurringPlanService_;
            CheckRecurringPlanService = _CheckRecurringPlanService_;
            RecurringPaymentPlanService = _RecurringPaymentPlanService_;
            SubscriptionService = _SubscriptionService_;
            
            // Initialize the controller
            SaleController = $controller('SaleController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
            it('should have variable scope.param defined', function() {
                expect(scope.param).toBeDefined();
            });        
            it('should have variable scope.sampleProductCategories defined', function() {
                expect(scope.sampleProductCategories).toBeDefined();
            });        
            it('should have variable scope.originalPaymentPlan defined', function() {
                expect(scope.originalPaymentPlan).toBeDefined();
            });        
            it('should have variable scope.recurrdays defined', function() {
                expect(scope.recurrdays).toBeDefined();
            });        
            it('should have variable scope.frequencies defined', function() {
                expect(scope.frequencies).toBeDefined();
            });        
            it('should have variable scope.frequency defined', function() {
                expect(scope.frequency).toBeDefined();
            });
            it('should have function scope.displayConfirmation defined', function() {
                expect(scope.displayConfirmation).toBeDefined();
            });
            it('should have function scope.checkCreate defined', function() {
                expect(scope.checkCreate).toBeDefined();
            });
            it('should have function scope.auditRecurringPlanChange defined', function() {
                expect(scope.auditRecurringPlanChange).toBeDefined();
            });
            it('should have function scope.verifyDelete defined', function() {
                expect(scope.verifyDelete).toBeDefined();
            });
            it('should have function scope.verifyEdit defined', function() {
                expect(scope.verifyEdit).toBeDefined();
            });
            it('should have function scope.changeNumberOfTimes defined', function() {
                expect(scope.changeNumberOfTimes).toBeDefined();
            });
            it('should have function scope.toggleNumberOfTimes defined', function() {
                expect(scope.toggleNumberOfTimes).toBeDefined();
            });
            it('should have function scope.changeFrequency defined', function() {
                expect(scope.changeFrequency).toBeDefined();
            });
            it('should have function scope.toggleFrequency defined', function() {
                expect(scope.toggleFrequency).toBeDefined();
            });
            it('should have function scope.confirmationModal defined', function() {
                expect(scope.confirmationModal).toBeDefined();
            });
            it('should have function scope.getRecurrPlanMerchantProductDropdown defined', function() {
                expect(scope.getRecurrPlanMerchantProductDropdown).toBeDefined();
            });
            it('should have function scope.setRecurrPlanMerchant defined', function() {
                expect(scope.setRecurrPlanMerchant).toBeDefined();
            });
            it('should have function scope.recurringInitialize defined', function() {
                expect(scope.recurringInitialize).toBeDefined();
            });
            it('should have function scope.validForm defined', function() {
                expect(scope.validForm).toBeDefined();
            });
            it('should have function scope.checkPlanExists defined', function() {
                expect(scope.checkPlanExists).toBeDefined();
            });
            it('should have function scope.submitRecurringPlan defined', function() {
                expect(scope.submitRecurringPlan).toBeDefined();
            });
            it('should have function scope.doCreateRecurringPlan defined', function() {
                expect(scope.doCreateRecurringPlan).toBeDefined();
            });
            it('should have function scope.doUpdateRecurringPlan defined', function() {
                expect(scope.doUpdateRecurringPlan).toBeDefined();
            });
            it('should have function scope.recurringPlansWrap defined', function() {
                expect(scope.recurringPlansWrap).toBeDefined();
            });
            it('should have function scope.getRecurringPlanList defined', function() {
                expect(scope.getRecurringPlanList).toBeDefined();
            });
            it('should have function scope.recurringPlansQueryInitialize defined', function() {
                expect(scope.recurringPlansQueryInitialize).toBeDefined();
            });
        });

        describe('Interface', function() {
            it('should recognize displayConfirmation as a function', function() {
                expect(angular.isFunction(scope.displayConfirmation)).toBe(true);
            });
            it('should recognize checkCreate as a function', function() {
                expect(angular.isFunction(scope.checkCreate)).toBe(true);
            });
            it('should recognize auditRecurringPlanChange as a function', function() {
                expect(angular.isFunction(scope.auditRecurringPlanChange)).toBe(true);
            });
            it('should recognize verifyDelete as a function', function() {
                expect(angular.isFunction(scope.verifyDelete)).toBe(true);
            });
            it('should recognize verifyEdit as a function', function() {
                expect(angular.isFunction(scope.verifyEdit)).toBe(true);
            });
            it('should recognize changeNumberOfTimes as a function', function() {
                expect(angular.isFunction(scope.changeNumberOfTimes)).toBe(true);
            });
            it('should recognize toggleNumberOfTimes as a function', function() {
                expect(angular.isFunction(scope.toggleNumberOfTimes)).toBe(true);
            });
            it('should recognize changeFrequency as a function', function() {
                expect(angular.isFunction(scope.changeFrequency)).toBe(true);
            });
            it('should recognize toggleFrequency as a function', function() {
                expect(angular.isFunction(scope.toggleFrequency)).toBe(true);
            });
            it('should recognize confirmationModal as a function', function() {
                expect(angular.isFunction(scope.confirmationModal)).toBe(true);
            });
            it('should recognize getRecurrPlanMerchantProductDropdown as a function', function() {
                expect(angular.isFunction(scope.getRecurrPlanMerchantProductDropdown)).toBe(true);
            });
            it('should recognize setRecurrPlanMerchant as a function', function() {
                expect(angular.isFunction(scope.setRecurrPlanMerchant)).toBe(true);
            });
            it('should recognize recurringInitialize as a function', function() {
                expect(angular.isFunction(scope.recurringInitialize)).toBe(true);
            });
            it('should recognize validForm as a function', function() {
                expect(angular.isFunction(scope.validForm)).toBe(true);
            });
            it('should recognize checkPlanExists as a function', function() {
                expect(angular.isFunction(scope.checkPlanExists)).toBe(true);
            });
            it('should recognize submitRecurringPlan as a function', function() {
                expect(angular.isFunction(scope.submitRecurringPlan)).toBe(true);
            });
            it('should recognize doCreateRecurringPlan as a function', function() {
                expect(angular.isFunction(scope.doCreateRecurringPlan)).toBe(true);
            });
            it('should recognize doUpdateRecurringPlan as a function', function() {
                expect(angular.isFunction(scope.doUpdateRecurringPlan)).toBe(true);
            });
            it('should recognize recurringPlansWrap as a function', function() {
                expect(angular.isFunction(scope.recurringPlansWrap)).toBe(true);
            });
            it('should recognize getRecurringPlanList as a function', function() {
                expect(angular.isFunction(scope.getRecurringPlanList)).toBe(true);
            });
            it('should recognize recurringPlansQueryInitialize as a function', function() {
                expect(angular.isFunction(scope.recurringPlansQueryInitialize)).toBe(true);
            });
        });

        describe('Data types/values', function() {
            it('should recognize scope.param as a object, value [ ]', function() {
                expect(typeof scope.param).toBe('object');
                expect(scope.param).toEqual(['']);
            });
            it('should recognize scope.sampleProductCategories as a object, value [ ]', function() {
                expect(typeof scope.sampleProductCategories).toBe('object');
                expect(scope.sampleProductCategories).toEqual([]);
            });
            it('should recognize scope.originalPaymentPlan as a object, value {}', function() {
                expect(typeof scope.originalPaymentPlan).toBe('object');
                expect(scope.originalPaymentPlan).toEqual({});
            });
            it('should recognize scope.recurrdays as a object, value [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]', function() {
                expect(typeof scope.recurrdays).toBe('object');
                expect(scope.recurrdays).toEqual([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]);
            });
            it('should recognize scope.recurrdays as a object, value [{value: 1, desc: "1"},{value: 2, desc: "2"},{value: 3, desc: "3 (Quarterly)"},{value: 4, desc: "4"},{value: 5, desc: "5"},{value: 6, desc: "6 (Bi-Annually)"},{value: 7, desc: "7"},{value: 8, desc: "8"},{value: 9, desc: "9"},{value: 10, desc: "10"},{value: 11, desc: "11"},{value: 12, desc: "12 (Annually)"},{value: 13, desc: "13"},{value: 14, desc: "14"},{value: 15, desc: "15"},{value: 16, desc: "16"},{value: 17, desc: "17"},{value: 18, desc: "18"},{value: 19, desc: "19"},{value: 20, desc: "20"},{value: 21, desc: "21"},{value: 22, desc: "22"},{value: 23, desc: "23"},{value: 24, desc: "24"}]', function() {
                expect(typeof scope.frequencies ).toBe('object');
                expect(scope.frequencies ).toEqual([{value: 1, desc: '1'},{value: 2, desc: '2'},{value: 3, desc: '3 (Quarterly)'},{value: 4, desc: '4'},{value: 5, desc: '5'},{value: 6, desc: '6 (Bi-Annually)'},{value: 7, desc: '7'},{value: 8, desc: '8'},{value: 9, desc: '9'},{value: 10, desc: '10'},{value: 11, desc: '11'},{value: 12, desc: '12 (Annually)'},{value: 13, desc: '13'},{value: 14, desc: '14'},{value: 15, desc: '15'},{value: 16, desc: '16'},{value: 17, desc: '17'},{value: 18, desc: '18'},{value: 19, desc: '19'},{value: 20, desc: '20'},{value: 21, desc: '21'},{value: 22, desc: '22'},{value: 23, desc: '23'},{value: 24, desc: '24'}]);
            });
            it('should recognize scope.frequency as a string, value "everySoManyDays"', function() {
                expect(typeof scope.frequency).toBe('string');
                expect(scope.frequency).toBe('everySoManyDays');
            });
        });

        describe('scope.displayConfirmation function', function() {
            beforeEach(function() {
                spyOn(scope, 'confirmationModal').and.callThrough();
                $httpBackend.whenGET('modules/creditcard/views/modalrecurringdeleteconf.client.view.html').respond(true);
            });

            it('should call scope.confirmationModal with "Plan Created" message', function() {
               var passedParameter = ''; 
               //expect(document.getElementById('fieldsForm').style.display).toBe('');
               //expect(document.getElementById('transactionCompleted').style.display).toBe('none');
               scope.displayConfirmation(passedParameter);
               scope.$digest();
               expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Created Recurring Plan', message: 'Plan Created.', accept: '', deny: 'OK' });
			});
            it('should call scope.confirmationModal with "Plan Was Not Created" message', function() {
               var passedParameter = 'error'; 
               //expect(document.getElementById('fieldsForm').style.display).toBe('');
               //expect(document.getElementById('transactionCompleted').style.display).toBe('none');
               scope.displayConfirmation(passedParameter);
               scope.$digest();
               expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Error Creating Recurring Plan', message: 'Plan Was Not Created', accept: '', deny: 'Cancel' });
			});
        });

        describe('scope.checkCreate function', function() {
            it('should return true if the url is "/recurringplancreate" ', function() {
                scope.param = ['','recurringplancreate'];
                var testVar = scope.checkCreate();
                scope.$digest();
                expect(testVar).toBe(true);
            });
            it('should return false if the url is "/recurringplancreate" but a planId was passed', function() {
                scope.param = ['','recurringplancreate', '99'];
                var testVar = scope.checkCreate();
                scope.$digest();
                expect(testVar).toBe(false);
            });
            it('should return false if modifying the record (the url is "/recurringplanmodify/##")', function() {
                scope.param = ['','recurringplanmodify', '99'];
                var testVar = scope.checkCreate();
                scope.$digest();
                expect(testVar).toBe(false);
            });
            it('should return false if modifying the record (the url is "/recurringplanmodify")', function() {
                scope.param = ['','recurringplanmodify'];
                var testVar = scope.checkCreate();
                scope.$digest();
                expect(testVar).toBe(false);
            });
        });

        describe('scope.auditRecurringPlanChange function', function() {
            it('should post to /auditrecurringplanheader', function() {
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                var original_record = {};
                var new_record = {};
                scope.auditRecurringPlanChange(original_record, new_record);
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should post to /auditrecurringplanheader with info from new_record', function() {
                $httpBackend.expect('POST', '/auditrecurringplanheader', {Boarding_ID: 10, Processor_ID: 'processor1', Recurring_Plan_ID:5}).respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                var original_record = {};
                var new_record = {
                    Boarding_ID: 10, 
                    Processor_ID: 'processor1', 
                    id: 5
                };
                expect(scope.originalPaymentPlan).not.toBeUndefined();
                scope.auditRecurringPlanChange(original_record, new_record);
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should post to /auditrecurringplandetail', function() {
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                var original_record = {};
                var new_record = {};
                scope.auditRecurringPlanChange(original_record, new_record);
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should post to /auditrecurringplandetail with specific info from new_record on create', function() {
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, {id: 10, Plan_Name: 'bologna'});
                $httpBackend.expect('POST', '/auditrecurringplandetail',{Audit_Header_ID: 10, Field_Name: 'Boarding_ID', Old_Value: undefined, New_Value: 10}).respond(200, 'Response');
                var original_record = 'create';
                var new_record = {
                    Boarding_ID: 10, 
                    id: 5
                };
                expect(scope.originalPaymentPlan).not.toBeUndefined();
                scope.auditRecurringPlanChange(original_record, new_record);
                $httpBackend.flush();
                scope.$digest();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should post to /auditrecurringplandetail with specific info from new_record on changed data', function() {
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, {id: 10, Plan_Name: 'bologna'});
                $httpBackend.expect('POST', '/auditrecurringplandetail',{Audit_Header_ID: 10, Field_Name: 'Boarding_ID', Old_Value: 5, New_Value: 10}).respond(200, 'Response');
                
                var original_record = {
                    Boarding_ID: 5, 
                    id: 5
                };
                var new_record = {
                    Boarding_ID: 10, 
                    id: 5
                };
                expect(scope.originalPaymentPlan).not.toBeUndefined();
                scope.auditRecurringPlanChange(original_record, new_record);
                $httpBackend.flush();
                scope.$digest();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should not post to /auditrecurringplandetail with specific info from new_record on unchanged changed data', function() {
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, {id: 10, Plan_Name: 'bologna'});
                //$httpBackend.expect('POST', '/auditrecurringplandetail',{Audit_Header_ID: 10, Field_Name: 'Boarding_ID', Old_Value: 10, New_Value: 10}).respond(200, 'Response');
                var original_record = {
                    Boarding_ID: 10, 
                    id: 5
                };
                var new_record = {
                    Boarding_ID: 10, 
                    id: 5
                };
                expect(scope.originalPaymentPlan).not.toBeUndefined();
                scope.auditRecurringPlanChange(original_record, new_record);
                $httpBackend.flush();
                scope.$digest();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should set scope.originalPaymentPlan', function() {
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                var original_record = {};
                var new_record = {
                    Boarding_ID: 10, 
                    id: 5
                };
                expect(scope.originalPaymentPlan).not.toBeUndefined();
                scope.auditRecurringPlanChange(original_record, new_record);
                $httpBackend.flush();
                scope.$digest();
                expect(scope.originalPaymentPlan).toEqual(new_record);
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should display an error if post to /auditrecurringplanheader generates an error', function() {
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(500, 'This Is An Error Message on /auditrecurringplanheader');
                spyOn(console,'log').and.callThrough();
                scope.auditRecurringPlanChange("a", "b");
                $httpBackend.flush();
                expect(console.log).toHaveBeenCalledWith('This Is An Error Message on /auditrecurringplanheader');
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should display an error if post to /auditrecurringplandetail generates an error', function() {
                spyOn(console,'log').and.callThrough();
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(500, 'This Is An Error Message on /auditrecurringplandetail');
                scope.auditRecurringPlanChange("a", "b");
                $httpBackend.flush();
                expect(console.log).toHaveBeenCalledWith('This Is An Error Message on /auditrecurringplandetail');
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
        });

        describe('scope.verifyDelete I function', function() {
            beforeEach(function() {
                var subscriptionDeferred,
                    $promise;

                subscriptionDeferred = $q.defer();
                subscriptionDeferred.resolve({
                       response:[]
                 });
                 
                spyOn(SubscriptionService, 'get').and.returnValue({$promise: subscriptionDeferred.promise}); //returns a fake promise;
            });

            it('should call SubscriptionService.get', function() {
                var plan_id = 10;
                spyOn(scope, 'confirmationModal').and.callThrough();
                $httpBackend.whenGET('modules/creditcard/views/modalrecurringdeleteconf.client.view.html').respond(true);
                scope.verifyDelete(plan_id);
                scope.$digest();
                expect(SubscriptionService.get).toHaveBeenCalledWith({planId:10});
            });
            it('should call modal confirmation the deletion the plan', function() {
                var plan_id = 10;
                spyOn(scope, 'confirmationModal').and.callThrough();
                $httpBackend.whenGET('modules/creditcard/views/modalrecurringdeleteconf.client.view.html').respond(true);
                scope.verifyDelete(plan_id);
                scope.$digest();
                expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Delete Recurring Plan', message: 'Do you wish to delete this plan?', accept: 'Yes', deny: 'No', planId: 10, delete: true, id: undefined });
            });
        });        

        describe('scope.verifyDelete II function', function() {
            beforeEach(function() {
                var subscriptionDeferred,
                    $promise;

                subscriptionDeferred = $q.defer();
                subscriptionDeferred.resolve({
                       response:[{}]
                 });
                 
                spyOn(SubscriptionService, 'get').and.returnValue({$promise: subscriptionDeferred.promise}); //returns a fake promise;
            });

            it('should call modal confirmation explaining you cannot delete the plan', function() {
                var plan_id = 10;
                spyOn(scope, 'confirmationModal').and.callThrough();
                $httpBackend.whenGET('modules/creditcard/views/modalrecurringdeleteconf.client.view.html').respond(true);
                scope.verifyDelete(plan_id);
                scope.$digest();
                expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Delete Recurring Plan', message: 'You cannot delete this plan.  There are customers associated with this plan.', accept: '', deny: 'Cancel', delete: true });
            });
        });
        
        describe('scope.verifyEdit I function', function() {
            beforeEach(function() {
                var subscriptionDeferred,
                    $promise;

                subscriptionDeferred = $q.defer();
                subscriptionDeferred.resolve({
                       response:[{}]
                 });
                 
                spyOn(SubscriptionService, 'get').and.returnValue({$promise: subscriptionDeferred.promise}); //returns a fake promise;
            });
            
             it('should call SubscriptionService.get', function() {
                 var plan_id = 10;
                 spyOn(scope, 'confirmationModal').and.callThrough();
                $httpBackend.whenGET('modules/creditcard/views/modalrecurringdeleteconf.client.view.html').respond(true);
                 scope.verifyEdit(plan_id);
                 scope.$digest();
                 expect(SubscriptionService.get).toHaveBeenCalledWith({planId:10});
             });
             
             it('should call modal confirmation explaining there are customers associated', function() {
                 var plan_id = 10;
                 spyOn(scope, 'confirmationModal').and.callThrough();
                $httpBackend.whenGET('modules/creditcard/views/modalrecurringdeleteconf.client.view.html').respond(true);
                 scope.verifyEdit(plan_id);
                 scope.$digest();
                 expect(scope.confirmationModal).toHaveBeenCalledWith('',{ title: 'Modify Recurring Plan', message: 'There are customers associated with this plan. Any changes to this plan will affect those customers.', accept: 'OK', deny: 'Cancel', planId: 10, delete: false });
            });
        });
        
        describe('scope.verifyEdit II function', function() {
            beforeEach(function() {
                var subscriptionDeferred,
                    $promise;

                subscriptionDeferred = $q.defer();
                subscriptionDeferred.resolve({
                       response:[]
                 });
                 
                spyOn(SubscriptionService, 'get').and.returnValue({$promise: subscriptionDeferred.promise}); //returns a fake promise;
            });
            
             it('should redirect to modify screen if there are no subscriptions are using this this plan', function() {
                 spyOn($location, 'path');
                 var plan_id = 10;
                 scope.verifyEdit(plan_id);
                 scope.$digest();
                 expect($location.path).toHaveBeenCalledWith('recurringplanmodify/10');
            });
        });
        
        describe('scope.changeNumberOfTimes function', function() {
            beforeEach(function() {
                this.element = $('<form id="fieldsForm" name="fieldsForm"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" /></form>');
				this.element.appendTo('body');
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should call call scope.toggleNumberOfTimes with the parameter false', function() {
                spyOn(scope, 'toggleNumberOfTimes').and.returnValue(false);
                document.getElementById('plan_payments_0').checked = false;
                scope.changeNumberOfTimes();
                scope.$digest();
                expect(scope.toggleNumberOfTimes).toHaveBeenCalledWith(false);
            });
            it('should call call scope.toggleNumberOfTimes with the parameter true', function() {
                spyOn(scope, 'toggleNumberOfTimes').and.returnValue(false);
                document.getElementById('plan_payments_0').checked = 'checked';
                scope.changeNumberOfTimes();
                scope.$digest();
                expect(scope.toggleNumberOfTimes).toHaveBeenCalledWith(true);
            });
        });        

        describe('scope.toggleNumberOfTimes function I (true case)', function() {
            beforeEach(function() {
                this.element = $('<form id="fieldsForm" name="fieldsForm"><input id="cancelTest" class="chargeUntilCancelled" disabled style="opacity:0.5;">Charge Until Canceled</input><input id="quantityTest" class="chargeANumberOfTimes" style="Opacity:1.0;">Charge This Many Times</input></form>');
				this.element.appendTo('body');
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should change opacity and disabled values of form elements and set scope.plan_payments_1 to blank',function() {
                scope.plan_payments_1 = 0;

                expect(scope.plan_payments_1).toBe(0);
                expect(document.getElementById('cancelTest').disabled).toBe(true);
                expect(document.getElementById('cancelTest').style.opacity).toBe('0.5');
                expect(document.getElementById('quantityTest').disabled).toBe(false);
                expect(document.getElementById('quantityTest').style.opacity).toBe('1');
                scope.toggleNumberOfTimes(true);
                scope.$digest();
                expect(scope.plan_payments_1).toBe('');
                expect(document.getElementById('cancelTest').disabled).toBe(false);
                expect(document.getElementById('cancelTest').style.opacity).toBe('1');
                expect(document.getElementById('quantityTest').disabled).toBe(true);
                expect(document.getElementById('quantityTest').style.opacity).toBe('0.5');
            });

        });        

        describe('scope.toggleNumberOfTimes function II (false case)', function() {
            beforeEach(function() {
                this.element = $('<form id="fieldsForm" name="fieldsForm"><input id="cancelTest" class="chargeUntilCancelled" style="opacity:1.0;">Charge Until Canceled</input><input id="quantityTest" class="chargeANumberOfTimes" disabled style="Opacity:0.5;">Charge This Many Times</input></form>');
				this.element.appendTo('body');
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should change opacity and disabled values of form elements',function() {
                expect(document.getElementById('cancelTest').disabled).toBe(false);
                expect(document.getElementById('cancelTest').style.opacity).toBe('1');
                expect(document.getElementById('quantityTest').disabled).toBe(true);
                expect(document.getElementById('quantityTest').style.opacity).toBe('0.5');
                scope.toggleNumberOfTimes(false);
                scope.$digest();
                expect(document.getElementById('cancelTest').disabled).toBe(true);
                expect(document.getElementById('cancelTest').style.opacity).toBe('0.5');
                expect(document.getElementById('quantityTest').disabled).toBe(false);
                expect(document.getElementById('quantityTest').style.opacity).toBe('1');
            });
        });        

        describe('scope.changeFrequency I function', function() {
            beforeEach(function() {
                this.element = $('<form id="fieldsForm" name="fieldsForm"><input name="frequency" type="radio" value="everySoManyDays"  checked/><input name="frequency" type="radio" value="onDayOfMonth" /></form>');
				this.element.appendTo('body');
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should call call scope.toggleNumberOfTimes with the parameter "everySoManyDays"', function() {
                spyOn(scope, 'toggleFrequency').and.returnValue(false);
                scope.changeFrequency();
                scope.$digest();
                expect(scope.toggleFrequency).toHaveBeenCalledWith('everySoManyDays');
            });
        });        

        describe('scope.changeFrequency II function', function() {
            beforeEach(function() {
                this.element = $('<form id="fieldsForm" name="fieldsForm"><input name="frequency" type="radio" value="everySoManyDays"/><input name="frequency" checked type="radio" value="onDayOfMonth" /></form>');
				this.element.appendTo('body');
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should call call scope.toggleNumberOfTimes with the parameter "onDayOfMonth"', function() {
                spyOn(scope, 'toggleFrequency').and.returnValue(false);
                scope.changeFrequency();
                scope.$digest();
                expect(scope.toggleFrequency).toHaveBeenCalledWith('onDayOfMonth');
            });
        });        

        describe('scope.toggleFrequency function I (everySoManyDays case)', function() {
           beforeEach(function() {
                this.element = $('<form id="fieldsForm" name="fieldsForm"><input id="everyTest" class="onDayOfMonth" style="opacity:1.0;">Charge the Customer Every</input><input id="onTest" class="everySoManyDays" disabled style="Opacity:0.5;">Charge the Customer On Day</input></form>');
				this.element.appendTo('body');
            });
            
            afterEach(function() {
                this.element.remove(); 
			});
            
            it('should change opacity and disabled values of form elements and set appropriate scope values to undefined', function() {
                scope.day_of_month = '10';
                scope.month_frequency = '2';
                expect(document.getElementById('everyTest').disabled).toBe(false);
                expect(document.getElementById('everyTest').style.opacity).toBe('1');
                expect(document.getElementById('onTest').disabled).toBe(true);
                expect(document.getElementById('onTest').style.opacity).toBe('0.5');
                scope.toggleFrequency('everySoManyDays');
                expect(scope.day_of_month).toBeUndefined();
                expect(scope.month_frequency).toBeUndefined();
                expect(document.getElementById('everyTest').disabled).toBe(true);
                expect(document.getElementById('everyTest').style.opacity).toBe('0.5');
                expect(document.getElementById('onTest').disabled).toBe(false);
                expect(document.getElementById('onTest').style.opacity).toBe('1');
            });
        });
        
        describe('scope.toggleFrequency function II (onDayOfMonth case)', function() {
           beforeEach(function() {
                this.element = $('<form id="fieldsForm" name="fieldsForm"><input id="everyTest" class="onDayOfMonth" style="opacity:0.5;" disabled>Charge the Customer Every</input><input id="onTest" class="everySoManyDays" style="Opacity:1.0;">Charge the Customer On Day</input></form>');
				this.element.appendTo('body');
            });
            
            afterEach(function() {
                this.element.remove(); 
			});
            
            it('should change opacity and disabled values of form elements and set appropriate scope values to undefined', function() {
                scope.day_frequency = '30';
                expect(document.getElementById('everyTest').disabled).toBe(true);
                expect(document.getElementById('everyTest').style.opacity).toBe('0.5');
                expect(document.getElementById('onTest').disabled).toBe(false);
                expect(document.getElementById('onTest').style.opacity).toBe('1');
                scope.toggleFrequency('onDayOfMonth');
                expect(scope.day_frequency).toBe('');
                expect(document.getElementById('everyTest').disabled).toBe(false);
                expect(document.getElementById('everyTest').style.opacity).toBe('1');
                expect(document.getElementById('onTest').disabled).toBe(true);
                expect(document.getElementById('onTest').style.opacity).toBe('0.5');
            });
        });

        describe('scope.confirmationModal function', function() {
            it('should do something', function() {
 //               $httpBackend.whenGET('modules/creditcard/views/modalrecurringdeleteconf.client.view.html').respond(true);
 //               scope.confirmationModal('med', {message:'text'});
 //               scope.$digest();
                expect('Able to test recurringpayments.client.controller.js confirmationModal() function').toBe(true);
            });
            
        });

        describe('scope.getRecurrPlanMerchantProductDropdown I function', function() {
            beforeEach(function() {
                var productDeferred,
                    paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                 
                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},{"id":47,"Boarding_ID":25,"Product_ID":2,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":"testUser","Processor_ID":'processor2',"Password":"TestPass","Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({"id":25,"Date":"2015-04-21","Merchant_Name":"Demo Account","Address":"123 Demo Street","City":"Demoville","Phone":"555-555-5555","Merchant_ID":"1","Bank_ID":"1","Terminal_ID":"1","MCC":"8396","Classification":"0","Visa":true,"Mastercard":true,"Discover":true,"American_Express":true,"Diners_Club":null,"JCB":null,"Maestro":null,"Max_Ticket_Amount":1500,"Max_Monthly_Amount":100,"Precheck":"nopreauth","Duplicate_Checking":null,"Allow_Merchant":null,"Duplicate_Threshold":1200,"Account_Description":null,"createdAt":"2015-04-21T20:58:50.000Z","updatedAt":"2015-11-10T17:26:44.000Z","Location_Number":"1","Aquire_Bin":"1","Store_Number":"1","Vital_Number":"1","Agent_Chain":"1","Required_Name":true,"Required_Company":null,"Required_Address":null,"Required_City":true,"Required_State":true,"Required_Zip":false,"Required_Country":null,"Required_Phone":null,"Required_Email":null,"Reserve_Rate":0,"Reserve_Condition":false,"Cap_Amount":null,"Disbursement":null,"Status":"active","Welcome_Email":true,"State":"CA","Zip":"92656"});
                
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
                spyOn(scope,'setRecurrPlanMerchant').and.returnValue(true);
            });
            
            it('should call ProductService.get', function() {
                scope.getRecurrPlanMerchantProductDropdown();
                scope.$digest();
                expect(ProductService.get).toHaveBeenCalledWith({productId:'all'});
            });
            it('should call PaymentService.get', function() {
                scope.getRecurrPlanMerchantProductDropdown();
                scope.$digest();
                expect(PaymentService.get).toHaveBeenCalledWith({paymentId:25,productId:'all'});
            });
            it('should call Nmitransactions.get', function() {
                scope.getRecurrPlanMerchantProductDropdown();
                scope.$digest();
                expect(Nmitransactions.get).toHaveBeenCalledWith({type: 'getMerchantInfo',boardingId: 25});
            });
            it('should call scope.setRecurrPlanMerchant', function() {
                scope.getRecurrPlanMerchantProductDropdown();
                scope.$digest();
                expect(scope.setRecurrPlanMerchant).toHaveBeenCalledWith([ ({ id: 25, Name: 'Demo Account', value: 25, products: [({ Name: '#Twt2Pay', Boarding_ID: 25, Product_ID: 2, Processor_ID: 'processor2' }) ] }) ]);
            });
        });
        
        describe('scope.getRecurrPlanMerchantProductDropdown II function', function() {
            beforeEach(function() {
                var productDeferred,
                    paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                 
                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.reject({"Error":"This is an error message on NmiTransactions.get"});
                
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            it('should Display an error if Nmitransactions.get returns an error', function() {
                spyOn(console,'log').and.callThrough();
                scope.getRecurrPlanMerchantProductDropdown();
                scope.$digest();
                expect(console.log).toHaveBeenCalledWith({ Error: 'This is an error message on NmiTransactions.get' });
            });
        });        

        describe('scope.getRecurrPlanMerchantProductDropdown III function', function() {
            beforeEach(function() {
                var productDeferred,
                    paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                 
                paymentDeferred = $q.defer();
                paymentDeferred.reject({"Error":"This is an error message on PaymentService.get"});
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({"id":25,"Date":"2015-04-21","Merchant_Name":"Demo Account","Address":"123 Demo Street","City":"Demoville","Phone":"555-555-5555","Merchant_ID":"1","Bank_ID":"1","Terminal_ID":"1","MCC":"8396","Classification":"0","Visa":true,"Mastercard":true,"Discover":true,"American_Express":true,"Diners_Club":null,"JCB":null,"Maestro":null,"Max_Ticket_Amount":1500,"Max_Monthly_Amount":100,"Precheck":"nopreauth","Duplicate_Checking":null,"Allow_Merchant":null,"Duplicate_Threshold":1200,"Account_Description":null,"createdAt":"2015-04-21T20:58:50.000Z","updatedAt":"2015-11-10T17:26:44.000Z","Location_Number":"1","Aquire_Bin":"1","Store_Number":"1","Vital_Number":"1","Agent_Chain":"1","Required_Name":true,"Required_Company":null,"Required_Address":null,"Required_City":true,"Required_State":true,"Required_Zip":false,"Required_Country":null,"Required_Phone":null,"Required_Email":null,"Reserve_Rate":0,"Reserve_Condition":false,"Cap_Amount":null,"Disbursement":null,"Status":"active","Welcome_Email":true,"State":"CA","Zip":"92656"});
                
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            it('should Display an error if PaymentService.get returns an error', function() {
                spyOn(console,'log').and.callThrough();
                scope.getRecurrPlanMerchantProductDropdown();
                scope.$digest();
                expect(console.log).toHaveBeenCalledWith({ Error: 'This is an error message on PaymentService.get' });
            });
        });        

        describe('scope.getRecurrPlanMerchantProductDropdown IV function', function() {
            beforeEach(function() {
                var productDeferred,
                    paymentDeferred,
                    NmitransactionsDeferred,
                    $promise;

                productDeferred = $q.defer();
                productDeferred.reject({"Error":"This is an error message on ProductService.get"});
                 
                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                NmitransactionsDeferred = $q.defer();
                NmitransactionsDeferred.resolve({"id":25,"Date":"2015-04-21","Merchant_Name":"Demo Account","Address":"123 Demo Street","City":"Demoville","Phone":"555-555-5555","Merchant_ID":"1","Bank_ID":"1","Terminal_ID":"1","MCC":"8396","Classification":"0","Visa":true,"Mastercard":true,"Discover":true,"American_Express":true,"Diners_Club":null,"JCB":null,"Maestro":null,"Max_Ticket_Amount":1500,"Max_Monthly_Amount":100,"Precheck":"nopreauth","Duplicate_Checking":null,"Allow_Merchant":null,"Duplicate_Threshold":1200,"Account_Description":null,"createdAt":"2015-04-21T20:58:50.000Z","updatedAt":"2015-11-10T17:26:44.000Z","Location_Number":"1","Aquire_Bin":"1","Store_Number":"1","Vital_Number":"1","Agent_Chain":"1","Required_Name":true,"Required_Company":null,"Required_Address":null,"Required_City":true,"Required_State":true,"Required_Zip":false,"Required_Country":null,"Required_Phone":null,"Required_Email":null,"Reserve_Rate":0,"Reserve_Condition":false,"Cap_Amount":null,"Disbursement":null,"Status":"active","Welcome_Email":true,"State":"CA","Zip":"92656"});
                
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(Nmitransactions, 'get').and.returnValue({$promise: NmitransactionsDeferred.promise}); //returns a fake promise;
            });
            
            it('should Display an error if PaymentService.get returns an error', function() {
                spyOn(console,'log').and.callThrough();
                scope.getRecurrPlanMerchantProductDropdown();
                scope.$digest();
                expect(console.log).toHaveBeenCalledWith({ Error: 'This is an error message on ProductService.get' });
            });
        });        

        describe('scope.setRecurrPlanMerchant function', function() {
            it('should set scope.setRecurrPlanMerchant', function() {
                expect(scope.sampleProductCategories).toEqual([]);
                expect(scope.Product_ID).toBe(2);
                scope.setRecurrPlanMerchant([ ({ id: 25, Name: 'Demo Account', value: 25, products: [({ Name: '#Twt2Pay', Boarding_ID: 25, Product_ID: 2, Processor_ID: 'processor2' }) ] }) ]);
                scope.$digest();
                expect(scope.sampleProductCategories).toEqual([ { id: 25, Name: 'Demo Account', value: 25, products: [ { Name: '#Twt2Pay', Boarding_ID: 25, Product_ID: 2, Processor_ID: 'processor2' } ] } ]);
                expect(scope.Product_ID).toEqual({ Name: '#Twt2Pay', Boarding_ID: 25, Product_ID: 2, Processor_ID: 'processor2' });
            });
        });
        
        describe('scope.recurringInitialize I function [update, Charge Until Cancelled, everySoManyDays ]', function() {
            beforeEach(function() {
                this.element = $('<form id="fieldsForm" name="fieldsForm"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" /><span id="productControl" style="float: left;">Name</span><input name="frequency" type="radio" value="everySoManyDays" /><input name="frequency" type="radio" value="onDayOfMonth" /></form>');
				this.element.appendTo('body');
                
                var recurringPlanDeferred,
                    $promise;

                recurringPlanDeferred = $q.defer();
                recurringPlanDeferred.resolve({"Response":[{"id":9,"Boarding_ID":25,"Processor_ID":"processor1","Plan_Name":"plan name","Plan_ID":10,"Plan_Amount":5,"Plan_Payments":0,"Day_Frequency":20,"Month_Frequency":0,"Day_Of_Month":0,"createdAt":"2015-10-20T18:54:59.000Z","updatedAt":"2015-10-20T18:54:59.000Z","Is_Active":1,"Description":"MojoPay"}]});
                 
                spyOn(RecurringPlanService, 'get').and.returnValue({$promise: recurringPlanDeferred.promise}); //returns a fake promise;                
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should call scope.getRecurrPlanMerchantProductDropdown', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate'];
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.getRecurrPlanMerchantProductDropdown).toHaveBeenCalledWith();
            });
            it('should bot call RecurringPlanService.get if this is a create situation', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate'];
                scope.recurringInitialize();
                scope.$digest();
                expect(RecurringPlanService.get).not.toHaveBeenCalled();
            });
            it('should call RecurringPlanService.get if this is a create situation', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                scope.recurringInitialize();
                scope.$digest();
                expect(RecurringPlanService.get).toHaveBeenCalledWith({ Recurring_Plan_ID: '99' });
            });
            it('should set scope.originalPaymentPlan', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                expect(scope.originalPaymentPlan).toEqual({});
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.originalPaymentPlan).toEqual({ id: 9, Boarding_ID: 25, Processor_ID: 'processor1', Plan_Name: 'plan name', Plan_ID: 10, Plan_Amount: 5, Plan_Payments: 0, Day_Frequency: 20, Month_Frequency: 0, Day_Of_Month: 0, createdAt: '2015-10-20T18:54:59.000Z', updatedAt: '2015-10-20T18:54:59.000Z', Is_Active: 1, Description: 'MojoPay' });
            });
            it('should set scope.plan_id', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                expect(scope.plan_id).toBeUndefined();
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.plan_id).toBe(10);
            });
            it('should set scope.plan_name', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                expect(scope.plan_name).toBeUndefined();
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.plan_name).toBe('plan name');
            });
            it('should set scope.plan_amount', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                expect(scope.plan_amount).toBeUndefined();
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.plan_amount).toBe('5.00');
            });
            it('should set scope.plan_payments_0 to true', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                scope.plan_payments_0 = false;
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.plan_payments_0).toBe(true);
            });
            it('should set scope.plan_payments_0 to true and turn on checkbox', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                spyOn(scope, 'toggleNumberOfTimes').and.callThrough();
                document.getElementById('plan_payments_0').checked = false;
                scope.param = ['','recurringplancreate', '99'];
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.toggleNumberOfTimes).toHaveBeenCalledWith(true);
                expect(document.getElementById('plan_payments_0').checked).toBe(true);
            });
            it('should set scope.frequency', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                scope.frequency = 'onDayOfMonth';
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.frequency).toBe('everySoManyDays');
            });
            it('should set scope.day_of_month', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                expect(scope.day_of_month).toBeUndefined();
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.day_of_month).toBe(0);
            });
            it('should set scope.month_frequency', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                expect(scope.month_frequency).toBeUndefined();
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.month_frequency).toBe(0);
            });
            it('should set scope.day_frequency', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                expect(scope.day_frequency).toBeUndefined();
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.day_frequency).toBe(20);
            });
            it('should call function toggleFrequency() with parameter "everySoManyDays"', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                spyOn(scope, 'toggleFrequency').and.returnValue(true);
                scope.param = ['','recurringplancreate', '99'];
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.toggleFrequency).toHaveBeenCalledWith('everySoManyDays');
            });
            it('should set productControl span value', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                scope.frequency = 'onDayOfMonth';
                scope.recurringInitialize();
                scope.$digest();
                expect(document.getElementById('productControl').innerHTML).toBe('MojoPay');
            });
        });        

        describe('scope.recurringInitialize II function [update, Charge A Certain Number of times, every x months on day z]', function() {
            beforeEach(function() {
                this.element = $('<form id="fieldsForm" name="fieldsForm"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" /><span id="productControl" style="float: left;">Name</span><input name="frequency" type="radio" value="everySoManyDays" /><input name="frequency" type="radio" value="onDayOfMonth" /></form>');
				this.element.appendTo('body');
                
                var recurringPlanDeferred,
                    $promise;

                recurringPlanDeferred = $q.defer();
                recurringPlanDeferred.resolve({"Response":[{"id":9,"Boarding_ID":25,"Processor_ID":"processor1","Plan_Name":"plan name","Plan_ID":10,"Plan_Amount":5,"Plan_Payments":10,"Day_Frequency":0,"Month_Frequency":2,"Day_Of_Month":28,"createdAt":"2015-10-20T18:54:59.000Z","updatedAt":"2015-10-20T18:54:59.000Z","Is_Active":1,"Description":"Description 2"}]});
                 
                spyOn(RecurringPlanService, 'get').and.returnValue({$promise: recurringPlanDeferred.promise}); //returns a fake promise;                
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should set scope.plan_payments_1', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                scope.plan_payments_1 = 0;
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.plan_payments_1).toBe(10);
            });
            it('should call scope.toggleNumberOfTimes with parameter false', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                spyOn(scope, 'toggleNumberOfTimes').and.returnValue(true);
                scope.param = ['','recurringplancreate', '99'];
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.toggleNumberOfTimes).toHaveBeenCalledWith(false);
            });
            it('should set scope.frequency', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                scope.frequency = 'everySoManyDays';
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.frequency).toBe('onDayOfMonth');
            });
            it('should set form checkbox elements', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                document.getElementsByName('frequency')[0].checked = true;
                document.getElementsByName('frequency')[1].checked = false;
                        scope.recurringInitialize();
                scope.$digest();
                expect(scope.frequency).toBe('onDayOfMonth');
                expect(document.getElementsByName('frequency')[0].checked).toBe(false);
                expect(document.getElementsByName('frequency')[1].checked).toBe(true);
                        
            });
            it('should set scope.day_of_month', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                expect(scope.day_of_month).toBeUndefined();
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.day_of_month).toBe(28);
            });
            it('should set scope.month_frequency', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                expect(scope.month_frequency).toBeUndefined();
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.month_frequency).toEqual(({ value: 2, desc: '2' }));
            });
            it('should set scope.day_frequency', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                scope.param = ['','recurringplancreate', '99'];
                expect(scope.day_frequency).toBeUndefined();
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.day_frequency).toBe(0);
            });
           it('should call scope.toggleFrequency with the parameter onDayOfMonth', function() {
                spyOn(scope, 'getRecurrPlanMerchantProductDropdown').and.returnValue(false);
                spyOn(scope, 'toggleFrequency').and.callThrough();
                scope.param = ['','recurringplancreate', '99'];
                scope.frequency = 'everySoManyDays';
                scope.recurringInitialize();
                scope.$digest();
                expect(scope.toggleFrequency).toHaveBeenCalledWith('onDayOfMonth');
            });
        });

        describe('scope.validForm function', function() {
            it('should define scope.error', function() {
                expect(scope.error).toBeUndefined();
                scope.param=['','recurringplancreate'];
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBeDefined();
            });
            
            // Product ID
            it('should return error if product Id is blank, null, or undefined', function() {
                expect(scope.error).toBeUndefined();
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: ''};
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please Choose a Product for this plan');
                scope.productId = {Product_ID: null};
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please Choose a Product for this plan');
                scope.productId = {Product_ID: undefined};
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please Choose a Product for this plan');
            });
            it('should call scope.checkPlanExists() if all parametes are good', function() {
                spyOn(scope,'checkPlanExists').and.returnValue(false);
                expect(scope.error).toBeUndefined();
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: '10'};
                expect(scope.error).toBeUndefined();
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 1;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = 2;
                scope.day_of_month = 28;
                scope.month_frequency = 2;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('');
            });
            
            // Plan Name
            it('should return error if scope.plan_name is blank, null, or undefined', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = '' ;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter the Plan Name');
                scope.plan_name = null ;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter the Plan Name');
                scope.plan_name = undefined ;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter the Plan Name');
            });

            // Plan Id
            it('should return error if scope.plan_id is blank, null, or undefined', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = '';
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter the Plan ID');
                scope.plan_id = null;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter the Plan ID');
                scope.plan_id = undefined;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter the Plan ID');
            });
            it('should return error if scope.plan_id is not a decimal', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10.6;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Plan ID must be a positive number');
            });
            it('should return error if scope.plan_id is less than 0', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = -1;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Plan ID must be a positive number');
            });
            it('should return error if scope.plan_id is not a decimal', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.validForm();
                scope.$digest();
                expect(scope.error).not.toBe('Please enter the Plan ID');
                expect(scope.error).not.toBe('Plan ID must be a positive number');
            });

            // Plan Amount
            it('should return error if scope.plan_amount is blank, null, or undefined', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_payments_0 = false;
                scope.plan_amount = '';
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter a proper Plan Amount');
                scope.plan_amount = null;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter a proper Plan Amount');
                scope.plan_amount = undefined;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter a proper Plan Amount');
            });
            it('should return error if scope.plan_amount is less than or equal to 0', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_payments_0 = false;
                scope.plan_amount = 0;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Plan Amount must be greater than zero');
            });
            it('should return error if scope.plan_amount has more than 2 decimals', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_payments_0 = false;
                scope.plan_amount = 1.001;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Plan Amount must be a dollar amount');
            });
            it('should not return plan_payments error if scope.plan_payments_0 is false', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = true;
                scope.validForm();
                scope.$digest();
                expect(scope.error).not.toBe('Please enter a proper Plan Amount');
                expect(scope.error).not.toBe('Plan Amount must be greater than zero');
                expect(scope.error).not.toBe('Plan Amount must be a dollar amount');
            });

            // Charge This Many Times / Charge Until Canceled
            it('should return error if scope.plan_payments_0 = false and scope.plan_payments_1 is blank, null, or undefined ', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = '';
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter a valid value for Charge This Many Times');
                scope.plan_payments_1 = null;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter a valid value for Charge This Many Times');
                scope.plan_payments_1 = undefined;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter a valid value for Charge This Many Times');
            });
            it('should return error if scope.plan_payments_0 = false and scope.plan_payments_1 is negative', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = -1;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Charge This Many Times must be a positive number');
            });
            it('should return error if scope.plan_payments_0 = false and scope.plan_payments_1 is not an integer', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 10.1;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Charge This Many Times must be a positive number');
            });
            it('should return no error if scope.plan_payments_0 = false and scope.plan_payments_1 positive integer', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 1;
                scope.validForm();
                scope.$digest();
                expect(scope.error).not.toBe('Please enter a valid value for Charge This Many Times');
                expect(scope.error).not.toBe('Charge This Many Times must be a positive number');
            });

            // Charge Every So Many Days / Charge On Day Of Month(s)
            it('should return error if scope.day_frequency is blank, null, or undefined on everySoManyDays', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 1;
                scope.frequency = 'everySoManyDays';
                scope.day_frequency = '';
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter a valid value for Charge the Customer Every X Days');
                scope.frequency = 'everySoManyDays';
                scope.day_frequency = null;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter a valid value for Charge the Customer Every X Days');
                scope.day_frequency = undefined;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please enter a valid value for Charge the Customer Every X Days');
            });
            it('should return error if scope.day_frequency is negative on everySoManyDays', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 1;
                scope.frequency = 'everySoManyDays';
                scope.day_frequency = -1;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Charge the Customer Every X Days must be a positive number');
            });
            it('should pass all day_frequency tests on everySoManyDays', function() {
                spyOn(scope,'doUpdateRecurringPlan').and.returnValue(false);
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 1;
                scope.frequency = 'everySoManyDays';
                scope.day_frequency = 1;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('');
                expect(scope.doUpdateRecurringPlan).toHaveBeenCalledWith();
            });
            it('should return error if scope.day_frequency is blank, null, or undefined on onDayOfMonth', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 1;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = '';
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please choose a proper day for Charge the Customer On Day');
                scope.day_frequency = null;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please choose a proper day for Charge the Customer On Day');
                scope.day_frequency = undefined;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please choose a proper day for Charge the Customer On Day');
            });
            it('should return error if scope.day_frequency is negative on onDayOfMonth', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 1;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = -1;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please choose a proper day for Charge the Customer On Day');
            });
            it('should return error if scope.month_frequency is blank on onDayOfMonth', function() {
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 1;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = 2;
                scope.day_of_month = 28;
                scope.month_frequency = '';
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please choose a proper month interval for Charge the Customer On Day x of every Months');
                scope.month_frequency = null;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please choose a proper month interval for Charge the Customer On Day x of every Months');
                scope.month_frequency = undefined;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('Please choose a proper month interval for Charge the Customer On Day x of every Months');
            });
            it('pass on test with scope.frequency equal to non-existent value, for completeness of ELSE statement', function() {
                spyOn(scope,'doUpdateRecurringPlan').and.returnValue(false);
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 1;
                scope.frequency = 'onEveryBlueMoon';
                scope.day_frequency = 1;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('');
            });

            // tests passed
            it('should call scope.doUpdateRecurringPlan if editing an existing plan', function() {
                spyOn(scope,'doUpdateRecurringPlan').and.returnValue(false);
                expect(scope.error).toBeUndefined();
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 1;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = 2;
                scope.day_of_month = 28;
                scope.month_frequency = 2;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('');
                expect(scope.doUpdateRecurringPlan).toHaveBeenCalledWith();
            });
            it('should call scope.checkPlanExists if creating a new plan', function() {
                spyOn(scope,'checkPlanExists').and.returnValue(false);
                expect(scope.error).toBeUndefined();
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 1;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = 2;
                scope.day_of_month = 28;
                scope.month_frequency = 2;
                scope.validForm();
                scope.$digest();
                expect(scope.error).toBe('');
                expect(scope.checkPlanExists).toHaveBeenCalledWith();
            });
        });

        describe('scope.checkPlanExists I function', function() {
            beforeEach(function() {
                var CheckRrecurringPlanDeferred,
                    $promise;

                CheckRrecurringPlanDeferred = $q.defer();
                CheckRrecurringPlanDeferred.resolve({'clean':'clean'});
                 
                spyOn(CheckRecurringPlanService, 'get').and.returnValue({$promise: CheckRrecurringPlanDeferred.promise}); //returns a fake promise;
                spyOn(scope,'doCreateRecurringPlan').and.returnValue(false);                
                spyOn(scope,'doUpdateRecurringPlan').and.returnValue(false);                
            });
            
            it('should call CheckRecurringPlanService.get with specified parameters', function() {
                scope.productId = {Product_ID: 2};
                scope.plan_id = 10;
                scope.checkPlanExists();
                expect(CheckRecurringPlanService.get).toHaveBeenCalledWith({ boardingId: 25, productId: 2, planId: 10 });
            });
            it('should call scope.doCreateRecurringPlan if creating a record that does not exist', function() {
                scope.param = ['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_id = 10;
                scope.checkPlanExists();
                scope.$digest();
                expect(scope.doCreateRecurringPlan).toHaveBeenCalled();
            });
            it('should call scope.doUpdateRecurringPlan if modifying a record that does not exist', function() {
                scope.param = ['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 2};
                scope.plan_id = 10;
                scope.checkPlanExists();
                scope.$digest();
                expect(scope.doUpdateRecurringPlan).toHaveBeenCalled();
            });
        });
       
        describe('scope.checkPlanExists II function', function() {
            beforeEach(function() {
                var CheckRrecurringPlanDeferred,
                    $promise;

                CheckRrecurringPlanDeferred = $q.defer();
                CheckRrecurringPlanDeferred.resolve({});
                 
                spyOn(CheckRecurringPlanService, 'get').and.returnValue({$promise: CheckRrecurringPlanDeferred.promise}); //returns a fake promise;
                spyOn(scope,'doCreateRecurringPlan').and.returnValue(false);                
                spyOn(scope,'doUpdateRecurringPlan').and.returnValue(false);                
            });
            
            it('should set an error if creating a plan with an ID that already exists', function() {
                scope.param = ['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_id = 10;
                expect(scope.error).toBeUndefined();
                scope.checkPlanExists();
                scope.$digest();
                expect(scope.error).toBe('There is already an active or previously deleted plan with this Plan ID');
            });
        });
       
        describe('scope.checkPlanExists III function', function() {
            beforeEach(function() {
                var CheckRrecurringPlanDeferred,
                    $promise;

                CheckRrecurringPlanDeferred = $q.defer();
                CheckRrecurringPlanDeferred.reject('this is an error response');
                 
                spyOn(CheckRecurringPlanService, 'get').and.returnValue({$promise: CheckRrecurringPlanDeferred.promise}); //returns a fake promise;
                spyOn(scope,'doCreateRecurringPlan').and.returnValue(false);                
                spyOn(scope,'doUpdateRecurringPlan').and.returnValue(false);                
            });
            
            it('should set an error if creating a plan with an ID that already exists', function() {
                spyOn(console,'log').and.callThrough();
                scope.param = ['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_id = 10;
                expect(scope.error).toBeUndefined();
                scope.checkPlanExists();
                scope.$digest();
                expect(console.log).toHaveBeenCalledWith('this is an error response');
            });
        });
       
        describe('scope.submitRecurringPlan function', function() {
            it('should call scope.validForm', function() {
                spyOn(scope,'validForm').and.returnValue(false);
                scope.submitRecurringPlan();
                scope.$digest();
                expect(scope.validForm).toHaveBeenCalled();
            });
        });
       
        describe('scope.doCreateRecurringPlan I function', function() {
            beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><form id="fieldsForm" name="fieldsForm"><input type="submit" id="submitButton" value="Search"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" checked/><input name="frequency" type="radio" value="everySoManyDays"  checked/><input name="frequency" type="radio" value="onDayOfMonth" /></form>');
				this.element.appendTo('body');
                inject(function ($rootScope, $compile) {
                    this.element = $compile(this.element)(scope);
                });
                
                
                var paymentDeferred,
                    paymentUpDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve({"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should display the loader image and hide the submit button ', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('');
                scope.param=['','recurringplancreate'];
                scope.doCreateRecurringPlan();
                expect(document.getElementById('loadingImg').style.display).toBe('block');
                expect(document.getElementById('submitButton').style.display).toBe('none');
            });
            it('should call PaymentService.get with specific parameters', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                $httpBackend.when('POST', '/recurringplan').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 1};
                scope.doCreateRecurringPlan();
                scope.$digest();
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 1 });
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should hide the loader and show the submit button', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                $httpBackend.when('POST', '/recurringplan').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 1};
                scope.doCreateRecurringPlan();
                scope.$digest();
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('inline-block');
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should POST to /recurringplan ( recordRecurringPlan() ) with expected parameters', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                var fieldsForm = scope.fieldsForm;
                spyOn(fieldsForm,'$setPristine').and.returnValue(true);

                $httpBackend.when('POST', '/recurringplan', {
                    plan_id: undefined, 
                    plan_name: 'plan 1', 
                    plan_amount: 10, 
                    plan_payments: 0, 
                    day_frequency: 15, 
                    day_of_month: '', 
                    month_frequency: '', 
                    type: 'add_recurring_plan', 
                    processorId: 'processor1', 
                    boardingId: 25
                }).respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 1};
                document.getElementById('plan_payments_0').checked = true;
                scope.plan_payments_1 = 10;
                document.querySelector('input[name="frequency"]:checked').value = 'everySoManyDays';
                scope.day_frequency = 15;
                scope.day_of_month = 28;
                scope.month_frequency = {value: '2'};
                scope.originalPaymentPlan = 100;
                scope.plan_name = 'plan 1';
                scope.plan_amount = 10;
                
                scope.doCreateRecurringPlan();
                scope.$digest();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                
            });
            it('should call scope.auditRecurringPlanChange() ( recordRecurringPlan() ) ', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                var fieldsForm = scope.fieldsForm;
                spyOn(fieldsForm,'$setPristine').and.returnValue(true);

                $httpBackend.when('POST', '/recurringplan', {
                    plan_id: undefined, 
                    plan_name: 'plan 1', 
                    plan_amount: 10, 
                    plan_payments: 0, 
                    day_frequency: 15, 
                    day_of_month: '', 
                    month_frequency: '', 
                    type: 'add_recurring_plan', 
                    processorId: 'processor1', 
                    boardingId: 25
                }).respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 1};
                document.getElementById('plan_payments_0').checked = true;
                scope.plan_payments_1 = 10;
                document.querySelector('input[name="frequency"]:checked').value = 'everySoManyDays';
                scope.day_frequency = 15;
                scope.day_of_month = 28;
                scope.month_frequency = 2;
                scope.originalPaymentPlan = 100;
                scope.plan_name = 'plan 1';
                scope.plan_amount = 10;
                
                scope.doCreateRecurringPlan();
                scope.$digest();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.displayConfirmation).toHaveBeenCalledWith('okay');
            });
            it('should call clear form values ( clearRecurringPlanForm() )', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                var fieldsForm = scope.fieldsForm;
                spyOn(fieldsForm,'$setPristine').and.returnValue(true);
                spyOn(scope,'toggleNumberOfTimes').and.returnValue(true);
                spyOn(scope,'toggleFrequency').and.returnValue(true);
                
                $httpBackend.when('POST', '/recurringplan', {
                    plan_id: undefined, 
                    plan_name: 'plan 1', 
                    plan_amount: 10, 
                    plan_payments: 0, 
                    day_frequency: 15, 
                    day_of_month: '', 
                    month_frequency: '', 
                    type: 'add_recurring_plan', 
                    processorId: 'processor1', 
                    boardingId: 25
                }).respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 1};
                document.getElementById('plan_payments_0').checked = true;
                scope.plan_payments_1 = 10;
                document.querySelector('input[name="frequency"]:checked').value = 'everySoManyDays';
                scope.day_frequency = 15;
                scope.day_of_month = 28;
                scope.month_frequency = 2;
                scope.originalPaymentPlan = 100;
                scope.plan_name = 'plan 1';
                scope.plan_amount = 10;
                
                scope.doCreateRecurringPlan();
                scope.$digest();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.dropdownMenu2).toBe('');
                expect(scope.plan_name).toBe('');
                expect(scope.plan_id).toBe('');
                expect(scope.plan_amount).toBe('');
                expect(scope.plan_payments_1).toBe('');
                expect(scope.plan_payments_0).toBe(false);
                expect(scope.frequency).toBe('everySoManyDays');
                expect(scope.day_frequency).toBe('');
                expect(scope.day_of_month).toBe('');
                expect(scope.month_frequency).toBe('');
                expect(scope.toggleNumberOfTimes).toHaveBeenCalledWith(false);
                expect(scope.toggleFrequency).toHaveBeenCalledWith('everySoManyDays');
            });
            it('should POST to /recurringplan ( recordRecurringPlan() ) with expected parameters', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                var fieldsForm = scope.fieldsForm;
                spyOn(fieldsForm,'$setPristine').and.returnValue(true);
                spyOn(scope,'toggleNumberOfTimes').and.returnValue(true);
                spyOn(scope,'toggleFrequency').and.returnValue(true);
                
                $httpBackend.when('POST', '/recurringplan', {
                    plan_id: undefined, 
                    plan_name: 'plan 2', 
                    plan_amount: 10, 
                    plan_payments: 10, 
                    day_frequency: '', 
                    day_of_month: 28, 
                    month_frequency: 2, 
                    type: 'add_recurring_plan', 
                    processorId: 'processor1', 
                    boardingId: 25    
                }).respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 1};
                document.getElementById('plan_payments_0').checked = false;
                scope.plan_payments_1 = 10;
                document.querySelector('input[name="frequency"]:checked').value = 'onDayOfMonth';
                scope.day_frequency = 15;
                scope.day_of_month = 28;
                scope.month_frequency = {value:2};
                scope.plan_name = 'plan 2';
                scope.plan_amount = 10;
                
                scope.doCreateRecurringPlan();
                scope.$digest();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
            it('should display an error if post to POST to /recurringplan fails ( recordRecurringPlan() )', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                var fieldsForm = scope.fieldsForm;
                spyOn(fieldsForm,'$setPristine').and.returnValue(true);
                spyOn(scope,'toggleNumberOfTimes').and.returnValue(true);
                spyOn(scope,'toggleFrequency').and.returnValue(true);
                spyOn(console,'log').and.callThrough();
                
                $httpBackend.when('POST', '/recurringplan', {
                    plan_id: undefined, 
                    plan_name: 'plan 1', 
                    plan_amount: 10, 
                    plan_payments: 0, 
                    day_frequency: 15, 
                    day_of_month: '', 
                    month_frequency: '', 
                    type: 'add_recurring_plan', 
                    processorId: 'processor1', 
                    boardingId: 25
                }).respond(500, 'This is an HTTP POST error on /recurringplan');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 1};
                document.getElementById('plan_payments_0').checked = true;
                scope.plan_payments_1 = 10;
                document.querySelector('input[name="frequency"]:checked').value = 'everySoManyDays';
                scope.day_frequency = 15;
                scope.day_of_month = 28;
                scope.month_frequency = 2;
                scope.originalPaymentPlan = 100;
                scope.plan_name = 'plan 1';
                scope.plan_amount = 10;
                
                scope.doCreateRecurringPlan();
                scope.$digest();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(console.log).toHaveBeenCalledWith('This is an HTTP POST error on /recurringplan');
                expect(scope.plan_name).toBe('plan 1');
                expect(scope.plan_id).toBeUndefined();
                expect(scope.plan_amount).toBe(10);
                expect(scope.plan_payments_1).toBe(10);
                expect(document.getElementById('plan_payments_0').checked).toBe(true);
                expect(scope.frequency).toBe('everySoManyDays');
                expect(scope.day_frequency).toBe(15);
                expect(scope.day_of_month).toBe(28);
                expect(scope.month_frequency).toBe(2);
                expect(scope.toggleNumberOfTimes).not.toHaveBeenCalled();
                expect(scope.toggleFrequency).not.toHaveBeenCalled();                
            });
        });
       
        describe('scope.doCreateRecurringPlan II function', function() {
            beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><form id="fieldsForm" name="fieldsForm"><input type="submit" id="submitButton" value="Search"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" checked/><input name="frequency" type="radio" value="everySoManyDays"  checked/><input name="frequency" type="radio" value="onDayOfMonth" /></form>');
				this.element.appendTo('body');

                var paymentDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.reject('This is an error response');
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should call PaymentService.get with specific parameters', function() {
                spyOn(console,'log').and.callThrough();
                $httpBackend.when('PUT', 'recurringplan').respond(200, 'Response');
                scope.productId = {Product_ID: 1};
                scope.doCreateRecurringPlan();
                scope.$digest();
                expect(console.log).toHaveBeenCalledWith('This is an error response'); 
            });
            it('should hide the loader and show the submit button', function() {
                $httpBackend.when('PUT', 'recurringplan').respond(200, 'Response');
                scope.productId = {Product_ID: 1};
                scope.doCreateRecurringPlan();
                scope.$digest();
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('inline-block');
            });
        });
        
/* Record Recurring Plan */
/*
        xdescribe('scope.doCreateRecurringPlan III function (testing createRecurringPlanObject ', function() {
            beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><form id="fieldsForm" name="fieldsForm"><input type="submit" id="submitButton" value="Search"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" checked/><input name="frequency" type="radio" value="everySoManyDays"  checked/><input name="frequency" type="radio" value="onDayOfMonth" /></form>');
				this.element.appendTo('body');

                var paymentDeferred,
                    recurringPlanDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},{"id":47,"Boarding_ID":25,"Product_ID":2,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":"testUser","Processor_ID":'processor2',"Password":"TestPass","Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                recurringPlanDeferred = $q.defer();
                recurringPlanDeferred.resolve({});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(RecurringPlanService, 'update').and.returnValue({$promise: recurringPlanDeferred.promise}); //returns a fake promise;
                
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should set an error if Post to /recurringplan returns an error', function() {
                $httpBackend.expect('POST', '/recurringplan',{"plan_id":10,"plan_name":"The Plan","plan_amount":10,"plan_payments":0,"day_frequency":2,"day_of_month":"","month_frequency":"","type":"add_recurring_plan","boardingId":25}).respond({'status':500, 'response':'Error'});
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(500, 'Error');
                spyOn(scope, 'auditRecurringPlanChange').and.returnValue(false);
                spyOn(scope, 'displayConfirmation').and.returnValue(false);
                
                scope.productId = {Product_ID: 1};
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = true;
                scope.plan_payments_1 = 0;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = 2;
                scope.day_of_month = 0;
                scope.month_frequency = 0;
                document.getElementById('plan_payments_0').checked = true;

                scope.doCreateRecurringPlan();
                scope.$digest();

                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 2 });
                
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();

                expect(scope.displayConfirmation).toHaveBeenCalledWith('error');
            });
                
        });
*/

        describe('scope.doUpdateRecurringPlan I function', function() {
            beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><form id="fieldsForm" name="fieldsForm"><input type="submit" id="submitButton" value="Search"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" checked/><input name="frequency" type="radio" value="everySoManyDays"  checked/><input name="frequency" type="radio" value="onDayOfMonth" /></form><form id="transactionCompleted" style="display:none;"></form>');
				this.element.appendTo('body');

                var paymentDeferred,
//                    recurringPlanDeferred,
                    $promise;
//
                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},{"id":47,"Boarding_ID":25,"Product_ID":2,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":"testUser","Processor_ID":'processor2',"Password":"TestPass","Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
 //               recurringPlanDeferred = $q.defer();
 //               recurringPlanDeferred.resolve({});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
 //               spyOn(RecurringPlanService, 'update').and.returnValue({$promise: recurringPlanDeferred.promise}); //returns a fake promise;
 
                spyOn(scope, "confirmationModal").and.callThrough();
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should display the loader image and hide the submit button ', function() {
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('');
                scope.param=['','recurringplanmodify', '99'];
                scope.doUpdateRecurringPlan();
                expect(document.getElementById('loadingImg').style.display).toBe('block');
                expect(document.getElementById('submitButton').style.display).toBe('none');
            });
            it('should call PaymentService.get with specific parameters', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                $httpBackend.when('GET', 'modules/creditcard/views/modalrecurringdeleteconf.client.view.html').respond(200, 'Response');
                $httpBackend.when('PUT', 'recurringplan').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                
                scope.param=['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 1};
                
                scope.doUpdateRecurringPlan();
                scope.$digest();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 1 });
            });
            it('should call scope.ConfirmationModal', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                $httpBackend.when('GET', 'modules/creditcard/views/modalrecurringdeleteconf.client.view.html').respond(200, 'Response');
                $httpBackend.when('PUT', 'recurringplan').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                
                scope.param=['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 1};
                
                scope.doUpdateRecurringPlan();
                scope.$digest();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Updated Recurring Plan', message: 'Plan Updated.', accept: '', deny: 'OK' });
            });
            it('should hide the loader image and display the submit button ', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                $httpBackend.when('GET', 'modules/creditcard/views/modalrecurringdeleteconf.client.view.html').respond(200, 'Response');
                $httpBackend.when('PUT', 'recurringplan').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                
                scope.param=['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 1};
                scope.doUpdateRecurringPlan();
                scope.$digest();
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('inline-block');
            });
        });

        describe('scope.doUpdateRecurringPlan II function', function() {
            beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><form id="fieldsForm" name="fieldsForm"><input type="submit" id="submitButton" value="Search"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" checked/><input name="frequency" type="radio" value="everySoManyDays"  checked/><input name="frequency" type="radio" value="onDayOfMonth" /></form><form id="transactionCompleted" style="display:none;"></form>');
				this.element.appendTo('body');

                var paymentDeferred,
                    recurringPlanDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.reject('This is an error response');
                
                recurringPlanDeferred = $q.defer();
                recurringPlanDeferred.reject('This ia an error message');
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(RecurringPlanService, 'update').and.returnValue({$promise: recurringPlanDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});

            it('should hide the loader image and display the submit button ', function() {
                $httpBackend.when('PUT', 'recurringplan').respond(200, 'Response');
                scope.doUpdateRecurringPlan();
                scope.$digest();
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('inline-block');
            });
            it('should generate an error if PaymentService.get fails', function() {
                spyOn(scope,'displayConfirmation').and.returnValue(true);
                spyOn(console,'log').and.callThrough();
                $httpBackend.when('GET', 'modules/creditcard/views/modalrecurringdeleteconf.client.view.html').respond(200, 'Response');
                $httpBackend.when('PUT', 'recurringplan').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplandetail').respond(200, 'Response');
                
                scope.param=['','recurringplanmodify', '99'];
                scope.productId = {Product_ID: 1};
                
                scope.doUpdateRecurringPlan();
                scope.$digest();
                expect(console.log).toHaveBeenCalledWith('This is an error response');
            });
        });

/* createRecurringPlanObject */
/*
        xdescribe('scope.doCreateRecurringPlan III function (testing createRecurringPlanObject) ', function() {
            beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><form id="fieldsForm" name="fieldsForm"><input type="submit" id="submitButton" value="Search"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" checked/><input name="frequency" type="radio" value="everySoManyDays"  checked/><input name="frequency" type="radio" value="onDayOfMonth" /></form>');
				this.element.appendTo('body');
                inject(function ($rootScope, $compile) {
                    this.element = $compile(this.element)(scope);
                });
                
                var paymentDeferred,
                    recurringPlanDeferred,
                    $promise,
                    fieldsForm;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},{"id":47,"Boarding_ID":25,"Product_ID":2,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":"testUser","Processor_ID":'processor2',"Password":"TestPass","Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                recurringPlanDeferred = $q.defer();
                recurringPlanDeferred.resolve({});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(RecurringPlanService, 'update').and.returnValue({$promise: recurringPlanDeferred.promise}); //returns a fake promise;
                fieldsForm = scope.fieldsForm;
                spyOn(fieldsForm,'$setPristine').and.returnValue(true);
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should Post to /recurringplan with specific parameters on create with Charge Until Canceled', function() {
                $httpBackend.expect('POST', '/recurringplan',{"plan_id":10,"plan_name":"The Plan","plan_amount":10,"plan_payments":0,"day_frequency":2,"day_of_month":"","month_frequency":"","type":"add_recurring_plan","boardingId":25}).respond(200, {"Plan_ID":10,"Plan_Name":"The Plan","Plan_Amount":10,"Plan_Payments":0,"Day_Of_Month":0,"Day_Frequency":2,"Month_Frequency":0,"Processor_ID":"processor1","Boarding_ID":25,"id":144,"Is_Active":true,"updatedAt":"2015-11-12T17:56:06.000Z","createdAt":"2015-11-12T17:56:06.000Z"});
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(500, 'Error');
                spyOn(scope, 'auditRecurringPlanChange').and.returnValue(false);
                spyOn(scope, 'displayConfirmation').and.returnValue(false);
                
                scope.productId = {Product_ID: 1};
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = true;
                scope.plan_payments_1 = 0;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = 2;
                scope.day_of_month = 0;
                scope.month_frequency = 0;
                document.getElementById('plan_payments_0').checked = true;

                scope.doCreateRecurringPlan();
                scope.$digest();

                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 2 });
                
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                
                expect(scope.auditRecurringPlanChange).toHaveBeenCalledWith('create', { Plan_ID: 10, Plan_Name: 'The Plan', Plan_Amount: 10, Plan_Payments: 0, Day_Of_Month: 0, Day_Frequency: 2, Month_Frequency: 0, Processor_ID: 'processor1', Boarding_ID: 25, id: 144, Is_Active: true, updatedAt: '2015-11-12T17:56:06.000Z', createdAt: '2015-11-12T17:56:06.000Z' });
                expect(scope.displayConfirmation).toHaveBeenCalledWith('okay');
            });
            it('should should Post to /recurringplan with specific parameters on Charge This Many Times', function() {
                $httpBackend.expect('POST', '/recurringplan',{"plan_id":10,"plan_name":"The Plan","plan_amount":10,"plan_payments":10,"day_frequency":2,"day_of_month":"","month_frequency":"","type":"add_recurring_plan","boardingId":25}).respond(200, {"Plan_ID":10,"Plan_Name":"The Plan","Plan_Amount":10,"Plan_Payments":10,"Day_Of_Month":0,"Day_Frequency":2,"Month_Frequency":0,"Processor_ID":"processor1","Boarding_ID":25,"id":144,"Is_Active":true,"updatedAt":"2015-11-12T17:56:06.000Z","createdAt":"2015-11-12T17:56:06.000Z"});
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(500, 'Error');
                spyOn(scope, 'auditRecurringPlanChange').and.returnValue(false);
                spyOn(scope, 'displayConfirmation').and.returnValue(false);
                
                scope.productId = {Product_ID: 1};
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 10;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = 2;
                scope.day_of_month = 0;
                scope.month_frequency = 0;
                document.getElementById('plan_payments_0').checked = false;

                scope.doCreateRecurringPlan();
                scope.$digest();

                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 2 });

                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                
                expect(scope.auditRecurringPlanChange).toHaveBeenCalledWith('create', { Plan_ID: 10, Plan_Name: 'The Plan', Plan_Amount: 10, Plan_Payments: 10, Day_Of_Month: 0, Day_Frequency: 2, Month_Frequency: 0, Processor_ID: 'processor1', Boarding_ID: 25, id: 144, Is_Active: true, updatedAt: '2015-11-12T17:56:06.000Z', createdAt: '2015-11-12T17:56:06.000Z' });
                expect(scope.displayConfirmation).toHaveBeenCalledWith('okay');
            });
            it('should should Post to /recurringplan with specific parameters on Charge the Customer Every SoMany Days', function() {
                $httpBackend.expect('POST', '/recurringplan',{"plan_id":10,"plan_name":"The Plan","plan_amount":10,"plan_payments":10,"day_frequency":2,"day_of_month":"","month_frequency":"","type":"add_recurring_plan","boardingId":25}).respond(200, {"Plan_ID":10,"Plan_Name":"The Plan","Plan_Amount":10,"Plan_Payments":10,"Day_Of_Month":0,"Day_Frequency":2,"Month_Frequency":0,"Processor_ID":"processor1","Boarding_ID":25,"id":144,"Is_Active":true,"updatedAt":"2015-11-12T17:56:06.000Z","createdAt":"2015-11-12T17:56:06.000Z"});
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(500, 'Error');
                spyOn(scope, 'auditRecurringPlanChange').and.returnValue(false);
                spyOn(scope, 'displayConfirmation').and.returnValue(false);
                
                scope.productId = {Product_ID: 1};
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = false;
                scope.plan_payments_1 = 10;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = 2;
                scope.day_of_month = 0;
                scope.month_frequency = 0;
                document.getElementById('plan_payments_0').checked = false;

                scope.doCreateRecurringPlan();
                scope.$digest();

                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 2 });

                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                
                expect(scope.auditRecurringPlanChange).toHaveBeenCalledWith('create', { Plan_ID: 10, Plan_Name: 'The Plan', Plan_Amount: 10, Plan_Payments: 10, Day_Of_Month: 0, Day_Frequency: 2, Month_Frequency: 0, Processor_ID: 'processor1', Boarding_ID: 25, id: 144, Is_Active: true, updatedAt: '2015-11-12T17:56:06.000Z', createdAt: '2015-11-12T17:56:06.000Z' });
                expect(scope.displayConfirmation).toHaveBeenCalledWith('okay');
            });
            it('should hide the loader and show the submit button', function() {
                $httpBackend.when('PUT', 'recurringplan').respond(200, 'Response');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(200, 'Response');
                scope.productId = {Product_ID: 1};
                scope.doCreateRecurringPlan();
                scope.$digest();
                
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('inline-block');
            });

        });

        xdescribe('scope.doCreateRecurringPlan IV function (testing createRecurringPlanObject) ', function() {
            beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><form id="fieldsForm" name="fieldsForm"><input type="submit" id="submitButton" value="Search"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" checked/><input name="frequency" type="radio" value="everySoManyDays"/><input name="frequency" type="radio" value="onDayOfMonth" checked/></form>');
				this.element.appendTo('body');
                inject(function ($rootScope, $compile) {
                    this.element = $compile(this.element)(scope);
                });
                var paymentDeferred,
                    recurringPlanDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},{"id":47,"Boarding_ID":25,"Product_ID":2,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":"testUser","Processor_ID":'processor2',"Password":"TestPass","Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                recurringPlanDeferred = $q.defer();
                recurringPlanDeferred.resolve({});
                
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(RecurringPlanService, 'update').and.returnValue({$promise: recurringPlanDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should Post to /recurringplan with specific parameters on create with Charge Until Canceled', function() {
                $httpBackend.expect('POST', '/recurringplan', {"plan_id":10,"plan_name":"The Plan","plan_amount":10,"plan_payments":0,"day_frequency":"","day_of_month":28,"type":"add_recurring_plan","boardingId":25}).respond(200, {"Plan_ID":10,"Plan_Name":"The Plan","Plan_Amount":10,"Plan_Payments":0,"Day_Of_Month":28,"Day_Frequency":"","Month_Frequency":1,"Processor_ID":"processor1","Boarding_ID":25,"id":144,"Is_Active":true,"updatedAt":"2015-11-12T17:56:06.000Z","createdAt":"2015-11-12T17:56:06.000Z"});
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(500, 'Error');
                spyOn(scope, 'auditRecurringPlanChange').and.returnValue(false);
                spyOn(scope, 'displayConfirmation').and.returnValue(false);
                
                scope.productId = {Product_ID: 1};
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = true;
                scope.plan_payments_1 = 0;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = '';
                scope.day_of_month = 28;
                scope.month_frequency = 1;
                document.getElementById('plan_payments_0').checked = true;

                scope.doCreateRecurringPlan();
                scope.$digest();

                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 2 });
                
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                
                expect(scope.auditRecurringPlanChange).toHaveBeenCalledWith('create', { Plan_ID: 10, Plan_Name: 'The Plan', Plan_Amount: 10, Plan_Payments: 0, Day_Of_Month: 28, Day_Frequency: '', Month_Frequency: 1, Processor_ID: 'processor1', Boarding_ID: 25, id: 144, Is_Active: true, updatedAt: '2015-11-12T17:56:06.000Z', createdAt: '2015-11-12T17:56:06.000Z' });
                expect(scope.displayConfirmation).toHaveBeenCalledWith('okay');

            });
            it('should log an error on Post to /recurringplan wreturning an error', function() {
                $httpBackend.expect('POST', '/recurringplan', {"plan_id":10,"plan_name":"The Plan","plan_amount":10,"plan_payments":0,"day_frequency":"","day_of_month":28,"type":"add_recurring_plan","boardingId":25}).respond(500, 'This is an error message');
                $httpBackend.when('POST', '/auditrecurringplanheader').respond(500, 'Error');
                spyOn(scope, 'auditRecurringPlanChange').and.returnValue(false);
                spyOn(scope, 'displayConfirmation').and.returnValue(false);
                spyOn(console,'log').and.callThrough();
                
                scope.productId = {Product_ID: 1};
                scope.param=['','recurringplancreate'];
                scope.productId = {Product_ID: 2};
                scope.plan_name = 'The Plan';
                scope.plan_id = 10;
                scope.plan_amount = 10;
                scope.plan_payments_0 = true;
                scope.plan_payments_1 = 0;
                scope.frequency = 'onDayOfMonth';
                scope.day_frequency = '';
                scope.day_of_month = 28;
                scope.month_frequency = 1;
                document.getElementById('plan_payments_0').checked = true;

                scope.doCreateRecurringPlan();
                scope.$digest();

                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 2 });
                
                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
                
                expect(console.log).toHaveBeenCalledWith('This is an error message');
            });
        });
*/
        describe('scope.recurringPlansWrap I function', function() {
           beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><form id="fieldsForm" name="fieldsForm"><input type="submit" id="submitButton" value="Search"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" checked/><input name="frequency" type="radio" value="everySoManyDays"/><input name="frequency" type="radio" value="onDayOfMonth" checked/></form>');
				this.element.appendTo('body');
                inject(function ($rootScope, $compile) {
                    this.element = $compile(this.element)(scope);
                });
                var paymentDeferred,
                    RecurringPaymentPlanDeferred,
                    productDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},{"id":47,"Boarding_ID":25,"Product_ID":2,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":"testUser","Processor_ID":'processor2',"Password":"TestPass","Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                RecurringPaymentPlanDeferred = $q.defer(); 
                RecurringPaymentPlanDeferred.resolve({"response":[{"id":9,"Boarding_ID":25,"Processor_ID":"processor1","Plan_Name":"phils other plan","Plan_ID":654,"Plan_Amount":5,"Plan_Payments":0,"Day_Frequency":0,"Month_Frequency":1,"Day_Of_Month":28,"createdAt":"2015-10-20T18:54:59.000Z","updatedAt":"2015-10-20T18:54:59.000Z","Is_Active":true},{"id":10,"Boarding_ID":25,"Processor_ID":"processor2","Plan_Name":"the plan to end all plans","Plan_ID":777,"Plan_Amount":13,"Plan_Payments":17,"Day_Frequency":23,"Month_Frequency":0,"Day_Of_Month":0,"createdAt":"2015-10-21T21:28:05.000Z","updatedAt":"2015-11-12T16:29:25.000Z","Is_Active":true},{"id":9,"Boarding_ID":25,"Processor_ID":"processor1","Plan_Name":"phils other plan","Plan_ID":654,"Plan_Amount":5,"Plan_Payments":0,"Day_Frequency":0,"Month_Frequency":2,"Day_Of_Month":28,"createdAt":"2015-10-20T18:54:59.000Z","updatedAt":"2015-10-20T18:54:59.000Z","Is_Active":true}]});
 
                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                   
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(RecurringPaymentPlanService, 'get').and.returnValue({$promise: RecurringPaymentPlanDeferred.promise}); //returns a fake promise;
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should display the loader image and hide the submit button ', function() {
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('');
                scope.recurringPlansWrap();
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                expect(document.getElementById('submitButton').style.display).toBe('inline-block');
            });
            it('should set working variables', function() {
                expect(scope.noResults).toBeUndefined();
                expect(scope.showRecurringPlans).toBeUndefined();
                expect(scope.items).toEqual([]); // set in sale controller
                scope.recurringPlansWrap();
                expect(scope.noResults).toBe(false);
                expect(scope.showRecurringPlans).toBe(false);
                expect(scope.items).toEqual([]);
            });
            it('should call PaymentService.get', function() {
                expect(scope.items).toEqual([]);
                scope.recurringPlansWrap();
                scope.$digest();
                expect(PaymentService.get).toHaveBeenCalledWith({ paymentId: 25, productId: 'all' });
            });
            it('should call RecurringPaymentPlanService.get', function() {
                expect(scope.items).toEqual([]);
                scope.recurringPlansWrap();
                scope.$digest();
                expect(RecurringPaymentPlanService.get).toHaveBeenCalledWith({ boarding: 25, product: undefined, plan: undefined });
            });
            it('should call ProductService.get', function() {
                expect(scope.items).toEqual([]);
                scope.recurringPlansWrap();
                scope.$digest();
                expect(ProductService.get).toHaveBeenCalledWith({ productId: 1 });
            });
            it('should set scope.items', function() {
                expect(scope.items).toEqual([]);
                scope.recurringPlansWrap();
                scope.$digest();
                expect(scope.items).toEqual([{ id: 9, Product_Description: undefined, Plan_Name: 'phils other plan', Plan_ID: 654, description: 'Runs every 28th day of the month', frequency: 'Charge until cancelled', Plan_Amount: 5, Recurr_ID: undefined } ,{ id: 10, Product_Description: undefined, Plan_Name: 'the plan to end all plans', Plan_ID: 777, description: 'Runs every 23 day(s)', frequency: 'Charge a total of 17 times', Plan_Amount: 13, Recurr_ID: undefined }, { id: 9, Product_Description: undefined, Plan_Name: 'phils other plan', Plan_ID: 654, description: 'Runs every 2 month(s)', frequency: 'Charge until cancelled', Plan_Amount: 5, Recurr_ID: undefined } ]);
            });
            it('should set completion flage', function() {
                expect(scope.noResults).toBeUndefined();
                expect(scope.showRecurringPlans).toBeUndefined();
                scope.recurringPlansWrap();
                scope.$digest();
                expect(scope.noResults).toBe(false);
                expect(scope.showRecurringPlans).toBe(true);
            });
        });

        describe('scope.recurringPlansWrap II function', function() {
           beforeEach(function() {
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" /><form id="fieldsForm" name="fieldsForm"><input type="submit" id="submitButton" value="Search"><input type="checkbox" id="plan_payments_0" name="plan_payments_0" checked/><input name="frequency" type="radio" value="everySoManyDays"/><input name="frequency" type="radio" value="onDayOfMonth" checked/></form>');
				this.element.appendTo('body');
                inject(function ($rootScope, $compile) {
                    this.element = $compile(this.element)(scope);
                });
                var paymentDeferred,
                    RecurringPaymentPlanDeferred,
                    productDeferred,
                    $promise;

                paymentDeferred = $q.defer();
                paymentDeferred.resolve([{"id":37,"Boarding_ID":25,"Product_ID":1,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":null,"Processor_ID":'processor1',"Password":null,"Ach_Reject_Fee":25,"Marketplace_Name":null},{"id":47,"Boarding_ID":25,"Product_ID":2,"Discount_Rate":3.49,"Monthly_Fee":0,"Setup_Fee":0,"Authorization_Fee":0,"Chargeback_Fee":35,"Retrieval_Fee":15,"createdAt":"2015-04-21T21:02:27.000Z","updatedAt":"2015-08-05T23:30:30.000Z","Is_Active":true,"Sale_Fee":0.35,"Capture_Fee":0.35,"Void_Fee":1,"Chargeback_Reversal_Fee":5,"Refund_Fee":0.35,"Credit_Fee":0.35,"Declined_Fee":1.15,"Username":"testUser","Processor_ID":'processor2',"Password":"TestPass","Ach_Reject_Fee":25,"Marketplace_Name":null}]);
                
                RecurringPaymentPlanDeferred = $q.defer(); 
                RecurringPaymentPlanDeferred.resolve({"response":[]});
 
                productDeferred = $q.defer();
                productDeferred.resolve({
                        "0":{"id":1,"Name":"MojoPay Base","Description":null,"createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "1":{"id":2,"Name":"Twt2pay","Description":"#Twt2Pay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "2":{"id":3,"Name":"VideoCheckout","Description":"Video Checkout","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "3":{"id":4,"Name":"E-Commerce","Description":"MojoPay","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":0},
                        "4":{"id":5,"Name":"Marketplace","Description":"Marketplace","createdAt":"0000-00-00 00:00:00","updatedAt":"0000-00-00 00:00:00","Active":1,"Marketplace":1}
                 });
                   
                spyOn(PaymentService, 'get').and.returnValue({$promise: paymentDeferred.promise}); //returns a fake promise;
                spyOn(RecurringPaymentPlanService, 'get').and.returnValue({$promise: RecurringPaymentPlanDeferred.promise}); //returns a fake promise;
                spyOn(ProductService, 'get').and.returnValue({$promise: productDeferred.promise}); //returns a fake promise;
            });
            
            afterEach(function(){
				this.element.remove(); 
			});
            
            it('should set scope.items', function() {
                expect(scope.items).toEqual([]);
                scope.recurringPlansWrap();
                scope.$digest();
                expect(scope.items).toEqual([]);
            });
            it('should set completion flage', function() {
                expect(scope.noResults).toBeUndefined();
                expect(scope.showRecurringPlans).toBeUndefined();
                scope.recurringPlansWrap();
                scope.$digest();
                expect(scope.noResults).toBe(true);
                expect(scope.showRecurringPlans).toBe(false);
            });
        });

        describe('scope.getRecurringPlanList function', function() {
          beforeEach(function() {
                var RecurringPaymentPlanDeferred,
                    $promise;

                RecurringPaymentPlanDeferred = $q.defer(); 
                RecurringPaymentPlanDeferred.resolve({"response":[{"id":9,"Boarding_ID":25,"Processor_ID":"processor1","Plan_Name":"phils other plan","Plan_ID":654,"Plan_Amount":5,"Plan_Payments":0,"Day_Frequency":0,"Month_Frequency":1,"Day_Of_Month":28,"createdAt":"2015-10-20T18:54:59.000Z","updatedAt":"2015-10-20T18:54:59.000Z","Is_Active":true},{"id":10,"Boarding_ID":25,"Processor_ID":"processor2","Plan_Name":"the plan to end all plans","Plan_ID":777,"Plan_Amount":13,"Plan_Payments":17,"Day_Frequency":23,"Month_Frequency":0,"Day_Of_Month":0,"createdAt":"2015-10-21T21:28:05.000Z","updatedAt":"2015-11-12T16:29:25.000Z","Is_Active":true},{"id":9,"Boarding_ID":25,"Processor_ID":"processor1","Plan_Name":"phils other plan","Plan_ID":654,"Plan_Amount":5,"Plan_Payments":0,"Day_Frequency":0,"Month_Frequency":2,"Day_Of_Month":28,"createdAt":"2015-10-20T18:54:59.000Z","updatedAt":"2015-10-20T18:54:59.000Z","Is_Active":true}]});  
                spyOn(RecurringPaymentPlanService, 'get').and.returnValue({$promise: RecurringPaymentPlanDeferred.promise}); //returns a fake promise;
            });
            
            it('should call RecurringPaymentPlanService.get with correct parameters', function() {
                scope.getRecurringPlanList();
                scope.$digest();
                expect(RecurringPaymentPlanService.get).toHaveBeenCalledWith({ boardingId: 25, productId: 'all' });
            });
            it('should return a value', function() {
                var junk;
                expect(junk).toBeUndefined();
                junk = scope.getRecurringPlanList();
                scope.$digest();
                expect(junk).toBeDefined();
                expect(junk).toBeTruthy();
            });
        });

        describe('scope.recurringPlansQueryInitialize function', function() {
          beforeEach(function() {
                var RecurringPaymentPlanDeferred,
                    $promise;

                RecurringPaymentPlanDeferred = $q.defer(); 
                RecurringPaymentPlanDeferred.resolve({
                    "response":[
                        {"id":9,"Boarding_ID":25,"Processor_ID":"processor1","Plan_Name":"phils other plan","Plan_ID":654,"Plan_Amount":5,"Plan_Payments":0,"Day_Frequency":0,"Month_Frequency":1,"Day_Of_Month":28,"createdAt":"2015-10-20T18:54:59.000Z","updatedAt":"2015-10-20T18:54:59.000Z","Is_Active":true},
                        {"id":10,"Boarding_ID":25,"Processor_ID":"processor2","Plan_Name":"the plan to end all plans","Plan_ID":777,"Plan_Amount":13,"Plan_Payments":17,"Day_Frequency":23,"Month_Frequency":0,"Day_Of_Month":0,"createdAt":"2015-10-21T21:28:05.000Z","updatedAt":"2015-11-12T16:29:25.000Z","Is_Active":true},
                        {"id":11,"Boarding_ID":25,"Processor_ID":"processor1","Plan_Name":"phils other plan","Plan_ID":654,"Plan_Amount":5,"Plan_Payments":0,"Day_Frequency":0,"Month_Frequency":2,"Day_Of_Month":28,"createdAt":"2015-10-20T18:54:59.000Z","updatedAt":"2015-10-20T18:54:59.000Z","Is_Active":true}]});  
                spyOn(RecurringPaymentPlanService, 'get').and.returnValue({$promise: RecurringPaymentPlanDeferred.promise}); //returns a fake promise;
                spyOn(scope, 'getRecurringPlanList').and.callThrough();
            });
            
            xit('should call scope.getRecurringPlanList', function() {
                scope.recurringPlansQueryInitialize();
                scope.$digest();
                expect(scope.getRecurringPlanList).toHaveBeenCalledWith();
            });
            it('should set scope.samplePlans', function() {
                scope.recurringPlansQueryInitialize();
                scope.$digest();
                expect(scope.samplePlans).toEqual([{ id: 'all', value: 'All Plans' }, { id: 9, value: 'phils other plan' }, { id: 10, value: 'the plan to end all plans' }, { id: 11, value: 'phils other plan' }]);
            });
        });

    });
}());