/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('ModalConfCtrl', function() {
        // Initialize global variables
        var $q,
        ModalConfCtrl,
            scope,
            $httpBackend,
			$stateParams,
			$location,
            $modal,
            items,
            modalInstance,
            Boardings;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$q_, _$httpBackend_, _$stateParams_, _$location_, _$modal_, _Boardings_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            $q = _$q_;
            $stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;
			$modal = _$modal_;
            modalInstance = {
                // Create a mock object using spies
                close: jasmine.createSpy('modalInstance.close'),
                dismiss: jasmine.createSpy('modalInstance.dismiss'),
                result: {
                    then: jasmine.createSpy('modalInstance.result.then')
                }
            };
            Boardings = _Boardings_;
            items = {
                boarding: 25,
                message: "This is the message",
                title: "This is the title",
                accept: "This is the accept",
                deny: "This is the deny",
                status: ''
            };
            
            // Point global variables to injected services
            
            // Initialize the controller
            ModalConfCtrl = $controller('ModalConfCtrl', {
                $scope: scope,
                $modalInstance: modalInstance,
                items: items
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
           it('should  have the variable scope.items defined', function() {
                expect(scope.items).toBeDefined();
            }); 
           it('should not have the variable $scope.title defined', function() {
                expect(scope.title).toBeUndefined();
            }); 
           it('should not have the variable $scope.message defined', function() {
                expect(scope.message).toBeUndefined();
            }); 
           it('should not have the variable $scope.accept defined', function() {
                expect(scope.accept).toBeUndefined();
            }); 
           it('should not have the variable $scope.deny defined', function() {
                expect(scope.deny).toBeUndefined();
            });
            it('should have function scope.updateStatus defined', function() {
                expect(scope.updateStatus).toBeDefined();
            });        
            it('should have function scope.ok defined', function() {
                expect(scope.ok).toBeDefined();
            });        
            it('should have function scope.getInformation defined', function() {
                expect(scope.getInformation).toBeDefined();
            });        
            it('should have function scope.cancel defined', function() {
                expect(scope.cancel).toBeDefined();
            });        
        });

        describe('Interface', function() {
            it('should recognize updateStatus as a function', function() {
                expect(angular.isFunction(scope.updateStatus)).toBe(true);
            });
            it('should recognize ok as a function', function() {
                expect(angular.isFunction(scope.ok)).toBe(true);
            });
            it('should recognize getInformation as a function', function() {
                expect(angular.isFunction(scope.getInformation )).toBe(true);
            });
            it('should recognize cancel as a function', function() {
                expect(angular.isFunction(scope.cancel)).toBe(true);
            });
        });

        describe('scope.updateStatus testing ', function() {
            it('should call Boardings.get and boarding.$update', function() {
                var status = 'active',
                    boardingid = 25;
                $httpBackend.when('GET', 'boardings/25').respond(200, {"id":25,"Date":"2015-04-21","Merchant_Name":"Demo Account","Address":"123 Demo Street","City":"Demoville","Phone":"555-555-5555","Merchant_ID":"1","Bank_ID":"1","Terminal_ID":"1","MCC":"8396","Classification":"0","Visa":true,"Mastercard":true,"Discover":true,"American_Express":false,"Diners_Club":null,"JCB":null,"Maestro":null,"Max_Ticket_Amount":1500,"Max_Monthly_Amount":100,"Precheck":"nopreauth","Duplicate_Checking":null,"Allow_Merchant":null,"Duplicate_Threshold":1200,"Account_Description":null,"createdAt":"2015-04-21T20:58:50.000Z","updatedAt":"2015-08-05T23:32:54.000Z","Location_Number":"1","Aquire_Bin":"1","Store_Number":"1","Vital_Number":"1","Agent_Chain":"1","Required_Name":true,"Required_Company":null,"Required_Address":null,"Required_City":true,"Required_State":true,"Required_Zip":false,"Required_Country":null,"Required_Phone":null,"Required_Email":null,"Reserve_Rate":0,"Reserve_Condition":false,"Cap_Amount":null,"Disbursement":null,"Status":"active","Welcome_Email":true,"State":"CA","Zip":"92656"});
                $httpBackend.when('PUT', 'boardings/25').respond(200, "success");
                
                scope.updateStatus(status, boardingid);
                $httpBackend.flush(2);
                
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
        });

        describe('scope.ok testing', function() {
            beforeEach(function() {
                spyOn(scope, 'updateStatus').and.returnValue(false);
            });
            it('should close the $modalInstance', function() {
                scope.ok();
                expect(modalInstance.close).toHaveBeenCalledWith({boarding: 25, message: "This is the message", title: "This is the title", accept: "This is the accept", deny: "This is the deny", status: ''});
            });
            it('should call scope.updateStatus with the active parameter if status = "active"', function() {
                items.status = 'active';
                scope.ok();
                expect(scope.updateStatus).toHaveBeenCalledWith('active', 25);
            });
            it('should call scope.updateStatus with the achreject parameter if status = "achreject"', function() {
                items.status = 'achreject';
                scope.ok();
                expect(scope.updateStatus).toHaveBeenCalledWith('achreject', 25);
            });
            it('should call scope.updateStatus with the restricted parameter if status = "restricted"', function() {
                items.status = 'restricted';
                scope.ok();
                expect(scope.updateStatus).toHaveBeenCalledWith('restricted', 25);
            });
            it('should call scope.updateStatus with the suspended parameter if status = "suspended"', function() {
                items.status = 'suspended';
                scope.ok();
                expect(scope.updateStatus).toHaveBeenCalledWith('suspended', 25);
            });
            it('should call scope.updateStatus with the closed parameter if status = "closed"', function() {
                items.status = 'closed';
                scope.ok();
                expect(scope.updateStatus).toHaveBeenCalledWith('closed', 25);
            });
            it('should call scope.updateStatus with the deleted parameter if status = "deleted"', function() {
                items.status = 'deleted';
                scope.ok();
                expect(scope.updateStatus).toHaveBeenCalledWith('deleted', 25);
            });
		});

        describe('scope.getInformation testing', function() {
            it('should define and set scope.title', function() {
                expect(scope.title).toBeUndefined();
                scope.getInformation();
                expect(scope.title).toBeDefined();
                expect(scope.title).toBe('This is the title');
            });
            it('should define and set scope.message', function() {
                expect(scope.message).toBeUndefined();
                scope.getInformation();
                expect(scope.message).toBeDefined();
                expect(scope.message).toBe('This is the message');
            });
            it('should define and set scope.accept', function() {
                expect(scope.accept).toBeUndefined();
                scope.getInformation();
                expect(scope.accept).toBeDefined();
                expect(scope.accept).toBe('This is the accept');
            });
            it('should define and set scope.deny', function() {
                expect(scope.deny).toBeUndefined();
                scope.getInformation();
                expect(scope.deny).toBeDefined();
                expect(scope.deny).toBe('This is the deny');
            });
		});

        describe('scope.cancel testing', function() {
            it('should call dismiss on the $modalInstance', function() {
                scope.cancel();
                expect(modalInstance.dismiss).toHaveBeenCalledWith('cancel');
            });
            it('should close the $modalInstance', function() {
                scope.cancel();
                expect(modalInstance.close).toHaveBeenCalledWith();
            });
		});

    });
}());