/*jslint node: true */ /*jslint jasmine: true */
'use strict';

describe('Ccntroller: SaleController', function() {
	var SaleController,
		scope,
		$resource,
		modal,
		authentication,
		location;

	beforeEach(function() {
		module('ngResource');
		module('ui.router');
		module('creditcard');
		module('core');
		module('users');
		
		module(function($provide) {
			$provide.value('Authentication', {'user': { roles:'user'}} );
		});
		
		inject(function($filter, $rootScope, $controller,$location){
			location = $location;
			scope = $rootScope.$new();
			SaleController = $controller('SaleController', {
				$scope: scope, 
				$modal: modal,
			});
			modal = {};
		});
//		browser().navigateTo('/#!/');
	});
	
	
	
	it('true should return true', function() {
		expect(true).toBe(true);
	});
	
	it('should Exist', function() {
		expect(SaleController).toBeTruthy();
	});
	
	it('redirects to SignOn Screen if not logged in', function() {
		spyOn(location, 'path');
		var loginURL = "/signin";
		expect(location.path()).toBe(loginURL);
	});
});