/*jslint node: true */ /*jslint jasmine: true */
'use strict';

(function() {
    // Authentication controller Spec
    describe('BoardingController', function() {
        // Initialize global variables
        var $q,
            BoardingController,
            scope,
            $httpBackend,
            $stateParams,
            $location,
            Authentication,
            PaymentService,
            Nmitransactions,
            VaultService,
            SubscriptionService;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));
        
        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$q_, _$stateParams_, _$httpBackend_, _Authentication_, _PaymentService_, _Nmitransactions_,_VaultService_,_SubscriptionService_) {
            // Set a new global scope
            scope = $rootScope.$new().$new();
            $location = _$location_;
            $q = _$q_;
            $stateParams = _$stateParams_;
            $httpBackend = _$httpBackend_;
            
            // Point global variables to injected services
            PaymentService = _PaymentService_;
            Nmitransactions= _Nmitransactions_;
            VaultService = _VaultService_;
            SubscriptionService = _SubscriptionService_;
            Authentication = _Authentication_;
            Authentication = {
                user: {
                    Roles: 'user',
                    Boarding_ID: 25
                }
            };
            

            // Initialize the controller
            BoardingController = $controller('BoardingController', {
                $scope: scope,
                Authentication: Authentication
            });
        }));

        it('should verify true is true', function() {
            expect(true).toBe(true);
        });

        describe('defined parameters', function (){
            it('should have scope defined', function() {
                expect(scope).toBeDefined();
            });        
            it('should have function scope.setTitle defined', function() {
                expect(scope.setTitle).toBeDefined();
            });
            it('should have function scope.customerVaultWrap defined', function() {
                expect(scope.customerVaultWrap).toBeDefined();
            });        
            it('should have function scope.verifyDelete defined', function() {
                expect(scope.verifyDelete).toBeDefined();
            });        
            it('should have function scope.confirmationModal defined', function() {
                expect(scope.confirmationModal).toBeDefined();
            });        
            it('should have function scope.customerExpiryWrap defined', function() {
                expect(scope.customerExpiryWrap).toBeDefined();
            });        
        });

        describe('Interface', function() {
            it('should recognize setTitle as a function', function() {
                expect(angular.isFunction(scope.setTitle)).toBe(true);
            });
            it('should recognize customerVaultWrap as a function', function() {
                expect(angular.isFunction(scope.customerVaultWrap)).toBe(true);
            });
            it('should recognize verifyDelete as a function', function() {
                expect(angular.isFunction(scope.verifyDelete)).toBe(true);
            });
            it('should recognize confirmationModal as a function', function() {
                expect(angular.isFunction(scope.confirmationModal)).toBe(true);
            });
            it('should recognize customerExpiryWrap as a function', function() {
                expect(angular.isFunction(scope.customerExpiryWrap)).toBe(true);
            });
         });

       
        describe('Not Logged in', function(){
            it('should not redirect if user is not logged in', function() {
                spyOn($location, 'path');
                expect($location.path).not.toHaveBeenCalledWith('/signin');
            });
        });

        describe('SetTitle function testing I', function() {
            it('should call $broadcast on $scope.$parent.$parent',function() {
                var value = 'Foxtrot Oscar Oscar';
                var parent = scope.$parent.$parent;
                spyOn(parent, '$broadcast').and.callThrough();
                scope.setTitle(value);            
                expect(parent.$broadcast).toHaveBeenCalledWith('setTitle', value);
                expect(parent.$broadcast.calls.mostRecent().args[0]).toEqual('setTitle');
                expect(parent.$broadcast.calls.mostRecent().args[1]).toEqual(value);
            });
        });

        describe('customerVaultWrap function testing', function() {
            var vaultDeferred,
                $promise;
                    
            beforeEach(function() {
                this.element = $('<img id="loadingImg" src="modules/creditcard/img/loader.gif" />');
                this.element.appendTo('body');
                
                vaultDeferred = $q.defer();
                vaultDeferred.resolve({response: [{"id":"1004916088","first_name":{"$t":"John"},"last_name":{"$t":"Doe"},"address_1":{},"address_2":{},"company":{},"city":{},"state":{},"postal_code":{"$t":"77777"},"country":{},"email":{},"phone":{},"fax":{},"cell_phone":{},"customertaxid":{},"website":{},"shipping_first_name":{},"shipping_last_name":{},"shipping_address_1":{},"shipping_address_2":{},"shipping_company":{},"shipping_city":{},"shipping_state":{},"shipping_postal_code":{},"shipping_country":{},"shipping_email":{},"shipping_carrier":{},"tracking_number":{},"shipping_date":{},"shipping":{},"cc_number":{"$t":"4xxxxxxxxxxx1111"},"cc_hash":{"$t":"f6c609e195d9d4c185dcc8ca662f0180"},"cc_exp":{"$t":"1025"},"cc_start_date":{},"cc_issue_number":{},"check_account":{},"check_hash":{},"check_aba":{},"check_name":{},"account_holder_type":{},"account_type":{},"sec_code":{},"processor_id":{},"cc_bin":{"$t":"411111"},"customer_vault_id":{"$t":"1004916088"},"merchant_defined_field":{"id":"1","$t":"f6cd571b370ccf2116a6760a8"}},{"id":"1102934846","first_name":{"$t":"John"},"last_name":{"$t":"Doe"},"address_1":{},"address_2":{},"company":{},"city":{},"state":{},"postal_code":{"$t":"77777"},"country":{},"email":{},"phone":{},"fax":{},"cell_phone":{},"customertaxid":{},"website":{},"shipping_first_name":{},"shipping_last_name":{},"shipping_address_1":{},"shipping_address_2":{},"shipping_company":{},"shipping_city":{},"shipping_state":{},"shipping_postal_code":{},"shipping_country":{},"shipping_email":{},"shipping_carrier":{},"tracking_number":{},"shipping_date":{},"shipping":{},"cc_number":{"$t":"4xxxxxxxxxxx1111"},"cc_hash":{"$t":"f6c609e195d9d4c185dcc8ca662f0180"},"cc_exp":{"$t":"1025"},"cc_start_date":{},"cc_issue_number":{},"check_account":{},"check_hash":{},"check_aba":{},"check_name":{},"account_holder_type":{},"account_type":{},"sec_code":{},"processor_id":{},"cc_bin":{"$t":"411111"},"customer_vault_id":{"$t":"1102934846"},"merchant_defined_field":{"id":"1","$t":"23dcc8c3c6488bac373017268"}}]});
                
                 spyOn(VaultService, 'get').and.returnValue({$promise: vaultDeferred.promise});
            });
            
            afterEach(function(){
                this.element.remove(); 
            });

            it('should define and set scope.showSearchVault', function() {
                expect(scope.showSearchVault).toBeUndefined();
                scope.customerVaultWrap();
                expect(scope.showSearchVault).toBeDefined();
                expect(scope.showSearchVault).toEqual(false);
            });
            it('should define and set scope.noResults', function() {
                expect(scope.noResults).toBeUndefined();
                scope.customerVaultWrap();
                expect(scope.noResults).toBeDefined();
                expect(scope.noResults).toEqual(true);
            });
            it('should define and set scope.items', function() {
                expect(scope.items).toBeUndefined();
                scope.customerVaultWrap();
                expect(scope.items).toBeDefined();
                expect(scope.items).toEqual([]);
            });
            it('should define and set scope.error', function() {
                expect(scope.error).toBeUndefined();
                scope.customerVaultWrap();
                expect(scope.error).toBeDefined();
                expect(scope.error).toBe('');
            });
            it('should define and set scope.root', function() {
                expect(scope.root).toBeUndefined();
                scope.customerVaultWrap();
                expect(scope.root).toBeDefined();
                expect(scope.root).toBe('http://localhost:9876');

            });
            it ('should call VaultService.get service', function() {
                scope.customerVaultWrap();
                scope.$digest();
                expect(VaultService.get).toHaveBeenCalled();
                expect(VaultService.get).toHaveBeenCalledWith({ boarding: 25, firstName: undefined, lastName: undefined, email: undefined, last4cc: undefined });
            });
            it ('should set scope.items', function() {
                var vault = [{"id":"1004916088","first_name":{"$t":"John"},"last_name":{"$t":"Doe"},"address_1":{},"address_2":{},"company":{},"city":{},"state":{},"postal_code":{"$t":"77777"},"country":{},"email":{},"phone":{},"fax":{},"cell_phone":{},"customertaxid":{},"website":{},"shipping_first_name":{},"shipping_last_name":{},"shipping_address_1":{},"shipping_address_2":{},"shipping_company":{},"shipping_city":{},"shipping_state":{},"shipping_postal_code":{},"shipping_country":{},"shipping_email":{},"shipping_carrier":{},"tracking_number":{},"shipping_date":{},"shipping":{},"cc_number":{"$t":"4xxxxxxxxxxx1111"},"cc_hash":{"$t":"f6c609e195d9d4c185dcc8ca662f0180"},"cc_exp":{"$t":"1025"},"cc_start_date":{},"cc_issue_number":{},"check_account":{},"check_hash":{},"check_aba":{},"check_name":{},"account_holder_type":{},"account_type":{},"sec_code":{},"processor_id":{},"cc_bin":{"$t":"411111"},"customer_vault_id":{"$t":"1004916088"},"merchant_defined_field":{"id":"1","$t":"f6cd571b370ccf2116a6760a8"}},{"id":"1102934846","first_name":{"$t":"John"},"last_name":{"$t":"Doe"},"address_1":{},"address_2":{},"company":{},"city":{},"state":{},"postal_code":{"$t":"77777"},"country":{},"email":{},"phone":{},"fax":{},"cell_phone":{},"customertaxid":{},"website":{},"shipping_first_name":{},"shipping_last_name":{},"shipping_address_1":{},"shipping_address_2":{},"shipping_company":{},"shipping_city":{},"shipping_state":{},"shipping_postal_code":{},"shipping_country":{},"shipping_email":{},"shipping_carrier":{},"tracking_number":{},"shipping_date":{},"shipping":{},"cc_number":{"$t":"4xxxxxxxxxxx1111"},"cc_hash":{"$t":"f6c609e195d9d4c185dcc8ca662f0180"},"cc_exp":{"$t":"1025"},"cc_start_date":{},"cc_issue_number":{},"check_account":{},"check_hash":{},"check_aba":{},"check_name":{},"account_holder_type":{},"account_type":{},"sec_code":{},"processor_id":{},"cc_bin":{"$t":"411111"},"customer_vault_id":{"$t":"1102934846"},"merchant_defined_field":{"id":"1","$t":"23dcc8c3c6488bac373017268"}}];
                scope.customerVaultWrap();
                scope.$digest();
                expect(scope.items).toEqual(vault);
            });
            it('should and set scope.showSearchVault', function() {
                expect(scope.showSearchVault).toBeUndefined();
                scope.customerVaultWrap();
                scope.$digest();
                expect(scope.showSearchVault).toBeDefined();
                expect(scope.showSearchVault).toEqual(true);
            });
            it('should and set scope.noResults', function() {
                expect(scope.noResults).toBeUndefined();
                scope.customerVaultWrap();
                scope.$digest();
                expect(scope.noResults).toBeDefined();
                expect(scope.noResults).toEqual(false);
            });
        });

        describe('customerVaultWrap function testing II', function() {
            var vaultDeferred,
                $promise;
                    
            beforeEach(function() {
                this.element = $('<img id="loadingImg" src="modules/creditcard/img/loader.gif" />');
                this.element.appendTo('body');
                
                vaultDeferred = $q.defer();
                vaultDeferred.resolve({response: []});
                
                spyOn(VaultService, 'get').and.returnValue({$promise: vaultDeferred.promise});
            });
            
            afterEach(function(){
                this.element.remove(); 
            });

            it('should and set scope.showSearchVault', function() {
                expect(scope.showSearchVault).toBeUndefined();
                scope.customerVaultWrap();
                scope.$digest();
                expect(scope.showSearchVault).toBeDefined();
                expect(scope.showSearchVault).toEqual(false);
            });
            it('should and set scope.noResults', function() {
                expect(scope.noResults).toBeUndefined();
                scope.customerVaultWrap();
                scope.$digest();
                expect(scope.noResults).toBeDefined();
                expect(scope.noResults).toEqual(true);
            });
        });

        describe('customerVaultWrap function testing III', function() {
            var vaultDeferred,
                $promise;
                    
            beforeEach(function() {
                this.element = $('<img id="loadingImg" src="modules/creditcard/img/loader.gif" />');
                this.element.appendTo('body');
                
                vaultDeferred = $q.defer();
                vaultDeferred.reject('This is an error response');
                
                spyOn(VaultService, 'get').and.returnValue({$promise: vaultDeferred.promise});
            });
            
            afterEach(function(){
                this.element.remove(); 
            });

            it('should set scope.error', function() {
                scope.customerVaultWrap();
                scope.$digest();
                expect(scope.error).toBeDefined();
                expect(scope.error).toEqual('This is an error response');
            });
        });

        describe('verifyDelete function testing I', function() {
            var subscriptionDeferred,
                $promise;
                    
            beforeEach(function() {
                subscriptionDeferred = $q.defer();
                subscriptionDeferred.resolve({response: []});
                
                spyOn(SubscriptionService, 'get').and.returnValue({$promise: subscriptionDeferred.promise});
                spyOn(scope,'confirmationModal').and.callThrough();
                $httpBackend.whenGET("modules/creditcard/views/modalrecurringdeleteconf.client.view.html").respond(true);
            });

            it('should call SubscriptionService.get', function() {
                scope.verifyDelete(50, 25);
                scope.$digest();
                expect(SubscriptionService.get).toHaveBeenCalledWith({cust: 25});
            });
            it('should call scope.confirmationModal with confirmation asking if user wants to delete the customer', function() {
                scope.verifyDelete(50, 25);
                scope.$digest();
                expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Delete Customer Vault', message: 'Do you wish to delete this customer?', accept: 'Yes', deny: 'No', planId: 25 });
            });
        });

        describe('verifyDelete function testing II', function() {
            var subscriptionDeferred,
                $promise;
                    
            beforeEach(function() {
                subscriptionDeferred = $q.defer();
                subscriptionDeferred.resolve({response: [{id:10}]});
                
                 spyOn(SubscriptionService, 'get').and.returnValue({$promise: subscriptionDeferred.promise});
                 spyOn(scope,'confirmationModal').and.callThrough();
                 $httpBackend.whenGET("modules/creditcard/views/modalrecurringdeleteconf.client.view.html").respond(true);
             });
            
            it('should call SubscriptionService.get', function() {
                scope.verifyDelete(50, 25);
                scope.$digest();
                expect(SubscriptionService.get).toHaveBeenCalledWith({cust: 25});
            });
            it('should call scope.confirmationModal with warning you cannot delete the customer', function() {
                scope.verifyDelete(50, 25);
                scope.$digest();
                expect(scope.confirmationModal).toHaveBeenCalledWith('', { title: 'Delete Customer Vault', message: 'You cannot delete this Customer.  The customer is associated with a subscription.', accept: '', deny: 'Cancel' });

            });
        });

        describe('customerExpiryWrap I', function() {
            var vaultDeferred,
                $promise;
            beforeEach(function(){
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" />');
				this.element.appendTo('body');
                
			    vaultDeferred = $q.defer();
                vaultDeferred.resolve({response: [{cc_exp: '0016'},{cc_exp: '1015'},{cc_exp: '1022'}]});
                
                spyOn(VaultService, 'get').and.returnValue({$promise: vaultDeferred.promise});
            });
            afterEach(function(){
                this.element.remove();
            });

            it('should display the loader image and hide the submit button ', function() {
                expect(document.getElementById('loadingImg').style.display).toBe('none');
                scope.customerExpiryWrap();
                expect(document.getElementById('loadingImg').style.display).toBe('block');
            });

           it('should set working variables', function() {
                expect(scope.showSearchExpiry).toBeUndefined();
                expect(scope.noResults).toBeUndefined();
                expect(scope.items).toBeUndefined();
                expect(scope.error).toBeUndefined();
                scope.customerExpiryWrap();
                expect(scope.showSearchExpiry).toBe(false);
                expect(scope.noResults).toBe(true);
                expect(scope.items).toEqual([]);
                expect(scope.error).toBe('');
            });
            it('should call VaultService.get', function() {
                scope.customerExpiryWrap();
                scope.$digest();
                expect(VaultService.get).toHaveBeenCalledWith({boarding: 25});
            });
            it('should set scope.items', function() {
                scope.customerExpiryWrap();
                scope.$digest();
                expect(scope.items[0]).toEqual({ cc_exp: '1015', year: '15' });
                expect(scope.items[1]).toEqual({ cc_exp: '0016', year: '16' });
                expect(scope.items[2]).toBeUndefined();
            });
           it('should set scope.showSearchExpiry and scope.noResults flage', function() {
                scope.customerExpiryWrap();
                scope.$digest();
                expect(scope.showSearchExpiry).toBe(true);
                expect(scope.noResults).toBe(false);
            });
        });

        describe('customerExpiryWrap II', function() {
            var vaultDeferred,
                $promise;
            beforeEach(function(){
                this.element = $('<img id="loadingImg"  style="height:40px;display:none;" src="modules/creditcard/img/loader.gif" />');
				this.element.appendTo('body');
                
			    vaultDeferred = $q.defer();
                vaultDeferred.resolve({response: [{cc_exp: '1022'}]});
                
                spyOn(VaultService, 'get').and.returnValue({$promise: vaultDeferred.promise});
            });
            afterEach(function(){
                this.element.remove();
            });

            it('should not set scope.items', function() {
                scope.customerExpiryWrap();
                scope.$digest();
                expect(scope.items).toEqual([]);
            });
            
            it('should set scope.showSearchExpiry and scope.noResults flage', function() {
                scope.customerExpiryWrap();
                scope.$digest();
                expect(scope.showSearchExpiry).toBe(false);
                expect(scope.noResults).toBe(true);
            });
        });

     });
}());

(function() {
    describe('BoardingController II', function() {
        // Initialize global variables
        var BoardingController,
            scope,
            $location,
            Authentication;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));

        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, Authentication) {
            // Set a new global scope
            scope = $rootScope.$new();
            $location = _$location_;
            
            // Point global variables to injected services
            spyOn($location, 'path');
            
            // Initialize the controller
            BoardingController = $controller('BoardingController', {
                $scope: scope,
                Authentication: Authentication
            });

        }));

        describe('Logged in', function(){
           it('should not redirect if user is not logged in', function() {
                expect($location.path).toHaveBeenCalledWith('/signin');
            });
        });
    });
}());

