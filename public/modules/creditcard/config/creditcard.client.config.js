'use strict';

// Creditcard module config
angular.module('creditcard').run(['Menus','Authentication',
	function(Menus,Authentication) {
		// Config logic
        // menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position,dynamicItem,icon,extra
        Menus.addMenuItem('topbar','Home','home','dropdown','','','*','01','','icon-home','true');
		
        Menus.addMenuItem('topbar','Super Admin','superadmin','dropdown','','','super','02','','icon-super-admin','false');
		Menus.addSubMenuItem('topbar','superadmin','Create Admin User','admincreate','');	
		Menus.addSubMenuItem('topbar','superadmin','Manage Admin Users','adminlist','');	
		
        Menus.addMenuItem('topbar','Merchant Accounts','boardingsheet','dropdown','','','admin','03','','icon-merchant-accounts','false');
		Menus.addSubMenuItem('topbar','boardingsheet','Board a Merchant','boardingCreate/1/create','');
		Menus.addSubMenuItem('topbar','boardingsheet','List Merchants','boardinglist','');
		
        Menus.addMenuItem('topbar','Virtual Terminal','creditcard','dropdown','','','','04','Virtual_Terminal','icon-virtual-terminal','false');
		Menus.addSubMenuItem('topbar','creditcard','Sale','sale','','','','','Terminal_Sale','');
		Menus.addSubMenuItem('topbar','creditcard','Authorize','authorize','','','','','Terminal_Authorize','');
		Menus.addSubMenuItem('topbar','creditcard','Capture','capture','','','','','Terminal_Capture','');
		Menus.addSubMenuItem('topbar','creditcard','Void','void','','','','','Terminal_Void','');
		Menus.addSubMenuItem('topbar','creditcard','Refund','refund','','','','','Terminal_Refund','');
		Menus.addSubMenuItem('topbar','creditcard','Credit','credit','','','','','Terminal_Credit','');
		
        Menus.addMenuItem('topbar','Customer Vault','customer','dropdown','customer','','','05','Customer_Vault','icon-customer-vault','false');
		Menus.addSubMenuItem('topbar','customer','Add Customer','create','','','','','Customer_Add','');
		Menus.addSubMenuItem('topbar','customer','List Customer','customerexport','','','','','Customer_List','');
        Menus.addSubMenuItem('topbar','customer','Customer Expiry Report','customerexpiry','','','','','Customer_Expiry','');
        
        Menus.addMenuItem('topbar','Recurring Payments','recurring','dropdown','recurring','','','06','Recurring_Payments','icon-recurring-payments','false');
		Menus.addSubMenuItem('topbar','recurring','Add Plan','recurringplancreate','','','','','Plan_Add','');
		Menus.addSubMenuItem('topbar','recurring','List Plan','recurringplanlist','','','','','Plan_List','');
		Menus.addSubMenuItem('topbar','recurring','Add Subscription','subscriptioncreate','','','','','Subscription_Add','');
		Menus.addSubMenuItem('topbar','recurring','List Subscription','subscriptionlist','','','','','Subscription_List','');
        
        Menus.addMenuItem('topbar','Easy Buttons','easybuttons','dropdown','easybuttons','','','07','Easy_Buttons','icon-easy-buttons','false');
		Menus.addSubMenuItem('topbar','easybuttons','Add Button','buttonCreate/1/create','','','','','Button_Add','');
		Menus.addSubMenuItem('topbar','easybuttons','List Buttons','buttonlist','','','','','Button_List','');

        Menus.addMenuItem('topbar','Transaction History','treport','dropdown','treport','','','08','Transaction_Report','icon-transaction-history','true');
        
        Menus.addMenuItem('topbar','Transaction Snapshot','tsnapshot','dropdown','tsnapshot','','admin','09','','icon-transaction-snapshot','true');
		
        Menus.addMenuItem('topbar','Advanced Reports','reports','dropdown','reports','','','10','Advanced_Report','icon-advanced-reports','true');
 		
        Menus.addMenuItem('topbar','Integration','integration','dropdown','integration','','','11','Integration','icon-integration','true');
	}
]);
