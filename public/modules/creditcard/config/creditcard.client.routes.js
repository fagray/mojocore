'use strict';
//Setting up route
angular.module('creditcard').config(['$stateProvider',
	function($stateProvider) {
		// Creditcard Transactions
		$stateProvider.
		//Virtual Terminal		
		state('sale', {
			url: '/sale',
			templateUrl: 'modules/creditcard/views/sale.client.view.html'
		}).
		state('salevault', {
			url: '/sale/:vaultid',
			templateUrl: 'modules/creditcard/views/sale.client.view.html'
		}).
		state('create', {
			url: '/create',
			templateUrl: 'modules/creditcard/views/create.client.view.html'
		}).
		state('modvault', {
			url: '/create/:vaultid',
			templateUrl: 'modules/creditcard/views/create.client.view.html'
		}).
		state('authorize', {
			url: '/authorize',
			templateUrl: 'modules/creditcard/views/auth.client.view.html'
		}).
		state('authorizevault', {
			url: '/authorize/:vaultid',
			templateUrl: 'modules/creditcard/views/auth.client.view.html'
		}).
		state('capture', {
			url: '/capture',
			templateUrl: 'modules/creditcard/views/capture.client.view.html'
		}).
		state('captureid', {
			url: '/capture/:transactionid',
			templateUrl: 'modules/creditcard/views/capture.client.view.html'
		}).
		state('void', {
			url: '/void',
			templateUrl: 'modules/creditcard/views/void.client.view.html'
		}).
		state('voidid', {
			url: '/void/:transactionid',
			templateUrl: 'modules/creditcard/views/void.client.view.html'
		}).
		state('refund', {
			url: '/refund',
			templateUrl: 'modules/creditcard/views/refund.client.view.html'
		}).
		state('refundid', {
			url: '/refund/:transactionid',
			templateUrl: 'modules/creditcard/views/refund.client.view.html'
		}).
		state('credit', {
			url: '/credit',
			templateUrl: 'modules/creditcard/views/credit.client.view.html'
		}).
		state('creditvault', {
			url: '/credit/:vaultid',
			templateUrl: 'modules/creditcard/views/credit.client.view.html'
		}).
		state('transactioncompleted', {
			url: '/transactioncompleted',
			templateUrl: 'modules/creditcard/views/transactioncompleted.client.view.html'
		}).
		//Other Routes
		state('integration', {
			url: '/integration',
			templateUrl: 'modules/creditcard/views/integrationsheet.client.view.html'
		}).
		state('APIintegration', {
			url: '/APIintegration',
			templateUrl: 'modules/creditcard/views/integration.client.view.html'
		}).
		state('customer', {
			url: '/customer',
			templateUrl: 'modules/creditcard/views/customer.client.view.html'
		}).
		state('customerexport', {
			url: '/customerexport',
			templateUrl: 'modules/creditcard/views/customerexport.client.view.html'
		}).
		state('customerexpiry', {
			url: '/customerexpiry',
			templateUrl: 'modules/creditcard/views/customerexpiry.client.view.html'
		}).
		state('treport', {
			url: '/treport',
			templateUrl: 'modules/creditcard/views/treport.client.view.html'
		}).
		state('tdetails', {
			url: '/tdetails/:transactionId/:Product_ID/:Boarding_ID',
			templateUrl: 'modules/creditcard/views/tdetails.client.view.html'
		}).
        state('boardinglist', {
        	url: '/boardinglist',
            templateUrl: 'modules/creditcard/views/boardinglist.client.view.html'
        }).
  		state('boardingsheet', {
			url: '/boardingCreate/:pageNumber/:boardingId',
			templateUrl: 'modules/creditcard/views/boardingsheet.client.view.html'
		}).
		state('help', {
			url: '/help',
			templateUrl: 'modules/creditcard/views/help.client.view.html'
		}).
        //Admin
		state('admincreate', {
			url: '/admincreate',
			templateUrl: 'modules/creditcard/views/createadmin.client.view.html'
		}).
		state('adminedit', {
			url: '/adminedit/{userId}',
			templateUrl: 'modules/creditcard/views/editadmin.client.view.html'
		}).
		state('adminlist', {
			url: '/adminlist',
			templateUrl: 'modules/creditcard/views/adminlist.client.view.html'
		}).
        //Reports
		state('reports', {
			url: '/reports',
			templateUrl: 'modules/creditcard/views/reports.client.view.html'
		}).
		state('reports/merchant_activity', {
			url: '/reports/merchant_activity',
			templateUrl: 'modules/creditcard/views/merchant_activity.client.view.html'
		}).
		state('reports/settlement_details', {
			url: '/reports/settlement_details',
			templateUrl: 'modules/creditcard/views/settlement_details.client.view.html'
		}).
		state('reports/settlement_transaction_details', {
			url: '/reports/settlement_transaction_details/:merchantId/:transactionDate/:Product_ID',
			templateUrl: 'modules/creditcard/views/settlement_transaction_details.client.view.html'
		}).
		state('reports/refund_transaction_details', {
			url: '/reports/refund_transaction_details/:merchantId/:transactionDate/:Product_ID',
			templateUrl: 'modules/creditcard/views/refund_transaction_details.client.view.html'
		}).
		state('reports/discount_fees', {
			url: '/reports/discount_fees/:merchantId/:transactionDate/:Product_ID',
			templateUrl: 'modules/creditcard/views/discount_fees.client.view.html'
		}).
		state('reports/transaction_fees', {
			url: '/reports/transaction_fees/:merchantId/:transactionDate/:Product_ID',
			templateUrl: 'modules/creditcard/views/transaction_fees.client.view.html'
		}).
		state('reports/settlement_transaction_detailsadmin', {
			url: '/reports/settlement_transaction_details/:merchantId/:transactionDate/:Product_ID/:boardingid',
			templateUrl: 'modules/creditcard/views/settlement_transaction_details.client.view.html'
		}).
		state('reports/refund_transaction_detailsadmin', {
			url: '/reports/refund_transaction_details/:merchantId/:transactionDate/:Product_ID/:boardingid',
			templateUrl: 'modules/creditcard/views/refund_transaction_details.client.view.html'
		}).
		state('reports/discount_feesadmin', {
			url: '/reports/discount_fees/:merchantId/:transactionDate/:Product_ID/:boardingid',
			templateUrl: 'modules/creditcard/views/discount_fees.client.view.html'
		}).
		state('reports/transaction_feesadmin', {
			url: '/reports/transaction_fees/:merchantId/:transactionDate/:Product_ID/:boardingid',
			templateUrl: 'modules/creditcard/views/transaction_fees.client.view.html'
		}).
		state('reports/account_summary', {
			url: '/reports/account_summary',
			templateUrl: 'modules/creditcard/views/account_summary.client.view.html'
		}).
		state('reports/transaction_detail_by_date', {
			url: '/reports/transaction_detail_by_date',
			templateUrl: 'modules/creditcard/views/transaction_detail_by_date.client.view.html'
		}).
		state('reports/account_balance_report', {
			url: '/reports/account_balance_report',
			templateUrl: 'modules/creditcard/views/account_balance_report.client.view.html'
		}).
		state('reports/ach_report', {
			url: '/reports/ach_report',
			templateUrl: 'modules/creditcard/views/ach_report.client.view.html'
		}).
        state('reports/achreject', {
            url: '/reports/achreject/:merchantId/:transactionDate/:Product_ID',
            templateUrl: 'modules/creditcard/views/achreject_detail.client.view.html'
        }).
        state('reports/misc', {
            url: '/reports/misc/:merchantId/:transactionDate/:Product_ID',
            templateUrl: 'modules/creditcard/views/misc_detail.client.view.html'
        }).
        state('reports/retrieval', {
            url: '/reports/retrieval/:merchantId/:transactionDate/:Product_ID',
            templateUrl: 'modules/creditcard/views/retrieval_detail.client.view.html'
        }).
        state('reports/chargeback', {
            url: '/reports/chargeback/:merchantId/:transactionDate/:Product_ID',
            templateUrl: 'modules/creditcard/views/chargeback_detail.client.view.html'
        }).
        state('reports/chargeback_reversal', {
            url: '/reports/chargeback_reversal/:merchantId/:transactionDate/:Product_ID',
            templateUrl: 'modules/creditcard/views/chargeback_reversal_detail.client.view.html'
        }).
   		state('tsnapshot', {
			url: '/tsnapshot',
			templateUrl: 'modules/creditcard/views/tsnapshot.client.view.html'
		}).
   		state('tsnapdetails', {
			url: '/tsnapdetails/:boarding_id/:type/:startDate/:endDate',
			templateUrl: 'modules/creditcard/views/tsnapdetails.client.view.html'
		}).
        // Recurring Payments
		state('recurringplancreate', {
			url: '/recurringplancreate',
			templateUrl: 'modules/creditcard/views/recurring_plan_create.client.view.html'
		}).
		state('recurringplanlist', {
			url: '/recurringplanlist',
			templateUrl: 'modules/creditcard/views/recurring_plan_list.client.view.html'
		}).
		state('recurringplanmodify', {
			url: '/recurringplanmodify/:Plan_ID',
			templateUrl: 'modules/creditcard/views/recurring_plan_create.client.view.html'
		}).
        state('reports/recurring_plan_audit_report', {
			url: '/reports/recurring_plan_audit_report',
			templateUrl: 'modules/creditcard/views/audit_recurring_plan_report.client.view.html'
		}).
		state('reports/recurringplanauditdetails', {
			url: '/reports/recurringplanauditdetails/:plan_id',
			templateUrl: 'modules/creditcard/views/audit_recurring_plan_details.client.view.html'
		}).
		state('subscriptioncreate', {
			url: '/subscriptioncreate',
			templateUrl: 'modules/creditcard/views/subscription_create.client.view.html'
		}).
		state('subscriptionlist', {
			url: '/subscriptionlist',
			templateUrl: 'modules/creditcard/views/subscription_list.client.view.html'
		}).
		state('subscriptionmodify', {
			url: '/subscriptionmodify/:Subscription_ID',
			templateUrl: 'modules/creditcard/views/subscription_create.client.view.html'
		}).
        // easy buttons
        state('buttonCreate', {
			url: '/buttonCreate/:pageNumber/:boardingId',
			templateUrl: 'modules/creditcard/views/buttonsheet.client.view.html'
		}).
        state('buttonList', {
			url: '/buttonlist',
			templateUrl: 'modules/creditcard/views/buttonlist.client.view.html'
		}).
        state('productbuttonedit', {
			url: '/productbuttonedit/:productId',
			templateUrl: 'modules/creditcard/views/buttonedit.client.view.html'
		}).
        state('donationbuttonedit', {
			url: '/donationbuttonedit/:productId',
			templateUrl: 'modules/creditcard/views/buttonedit.client.view.html'
		});
	}
]);
