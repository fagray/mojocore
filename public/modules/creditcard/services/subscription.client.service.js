'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('creditcard').factory('SubscriptionService', ['$resource',
	function($resource) {
		return $resource('subscription/:boardingId/:productId', {
	        boardingId: '@boardingId',
	        productId: '@productId'

	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('creditcard').factory('SubscriptionFindOne', ['$resource',
	function($resource) {
		return $resource('subscription/:subId', {
            subId: '@subId'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
