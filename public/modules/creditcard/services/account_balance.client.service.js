'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('creditcard').factory('Account_BalanceService', ['$resource',
	function($resource) {
		return $resource('account_balance/:processorId/:balanceDate', {
	        processorId: '@processorId',
	        balanceDate: '@balanceDate'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
