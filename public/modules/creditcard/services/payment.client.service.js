'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('PaymentService', ['$resource',
	function($resource) {
		return $resource('paymentplan/:paymentId/:productId', {
	        paymentId: '@boarding_Id',
	        productId: '@Product_ID'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('creditcard').factory('PaymentPlanService', ['$resource',
    function($resource) {
        return $resource('allPaymentPlans/');
    }
]);

angular.module('creditcard').factory('isPayment_Plan', ['$resource',
    function($resource) {
        return $resource('isPayment_Plan/:payment/:product', {
            payment: '@boarding_Id',
            product: '@Product_ID'
        },  {
            update: {
                method: 'PUT'
            }
        });
    }
]);

angular.module('creditcard').factory('PaymentCompleteService', ['$resource',
    function($resource) {
        return $resource('paymentPlanComplete/');
    }
]);

angular.module('creditcard').factory('PaymentSplitService', ['$resource',
    function($resource) {
        return $resource('paymentSplit/');
    }
]);

