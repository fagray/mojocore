'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('creditcard').factory('MasService', ['$resource',
	function($resource) {
		return $resource('mas/:masprocessorid/:paymentdate', {
	        masprocessorid: '@Processor_ID',
	        paymentdate: '@Payment_Date'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
