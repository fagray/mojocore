'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('creditcard').factory('Line_ItemsService', ['$resource',
	function($resource) {
		return $resource('line_items/:processorId', {
	        processorId: '@processorId'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
