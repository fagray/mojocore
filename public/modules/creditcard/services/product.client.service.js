'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('creditcard').factory('ProductService', ['$resource',
	function($resource) {
		return $resource('product/:productId', {
	        productId: '@productId'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
