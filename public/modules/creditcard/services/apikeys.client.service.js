'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('creditcard').factory('ApikeyService', ['$resource',
	function($resource) {
		return $resource('apikey/:boardingapi', {
	        boardingapi: '@boarding_id'
	    }, {
	        update: {
	            method: 'PUT'
	        }
	    });
}]);