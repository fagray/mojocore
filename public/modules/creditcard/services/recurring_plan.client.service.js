'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('creditcard').factory('RecurringPaymentPlanService', ['$resource',
	function($resource) {
		return $resource('recurringplan/:boardingId/:productId', {
	        productId: '@productId',
            boardingId: '@boardingId'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
angular.module('creditcard').factory('RecurringPlanService', ['$resource',
    function($resource) {
        return $resource('recurringplan/:Recurring_Plan_ID', {
	        Plan_ID: '@Recurring_Plan_ID'
        },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
angular.module('creditcard').factory('CheckRecurringPlanService', ['$resource',
    function($resource) {
        return $resource('checkrecurringplan/', {
        },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);

