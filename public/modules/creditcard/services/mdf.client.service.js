'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('creditcard').factory('MdfService', ['$resource',
	function($resource) {
		return $resource('mdf/:historyId', {
	        historyId: '@historyId'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
