'use strict';


angular.module('creditcard').factory('MerchantReportService', ['$resource',
    function($resource) {
        return $resource('merchantActivity/');
    }
]);
angular.module('creditcard').factory('AccountBalanceReportService', ['$resource',
    function($resource) {
        return $resource('accountBalance/');
    }
]);
angular.module('creditcard').factory('RunningBalanceService', ['$resource',
    function($resource) {
        return $resource('paymentDates/');
    }
]);
angular.module('creditcard').factory('DisbursmentAmountService', ['$resource',
    function($resource) {
        return $resource('paymentAmount/');
    }
]);
angular.module('creditcard').factory('SettleService', ['$resource',
    function($resource) {
        return $resource('settleTransactions/');
    }
]);
angular.module('creditcard').factory('DiscountService', ['$resource',
    function($resource) {
        return $resource('discountTransactions/');
    }
]);
angular.module('creditcard').factory('TransactionFeeService', ['$resource',
    function($resource) {
        return $resource('feeTransactions/');
    }
]);
angular.module('creditcard').factory('ACHReportService', ['$resource',
    function($resource) {
        return $resource('ACHActivity/');
    }
]);
angular.module('creditcard').factory('LineItemsService', ['$resource',
    function($resource) {
        return $resource('lineItemsTransactions/');
    }
]);
angular.module('creditcard').factory('RefundService', ['$resource',
    function($resource) {
        return $resource('refundTransactions/');
    }
]);

