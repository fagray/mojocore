'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('creditcard').factory('disbursementsService', ['$resource',
	function($resource) {
		return $resource('disbursement_schedule/:userboardingId', {
	        boarding_id: '@userboardingId'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
