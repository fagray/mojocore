'use strict';

// Users service used for communicating with the vauly REST endpoint
angular.module('creditcard').factory('VaultService', ['$resource',
	function($resource) {
		return $resource('customervault', {
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
