'use strict';


angular.module('creditcard').factory('TransactionsService', ['$resource',
    function($resource) {
        return $resource('transactions/');
    }
]);
angular.module('creditcard').factory('ProcessorService', ['$resource',
    function($resource) {
        return $resource('processor/');
    }
]);
angular.module('creditcard').factory('NMIUsernameService', ['$resource',
    function($resource) {
        return $resource('nmiusername/');
    }
]);
