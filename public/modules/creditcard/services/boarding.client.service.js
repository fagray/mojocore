'use strict';
//Articles service used for articles REST endpoint
angular.module('creditcard').factory('Boardings', ['$resource',
 function($resource) {
	    return $resource('boardings/:boardingId', {
	        boardingId: '@id'
	    }, {
	        update: {
	            method: 'PUT'
	        }
	    });
}]);