'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('creditcard').factory('AuditRecurrPlanService', ['$resource',
	function($resource) {
		return $resource('auditrecurringplanrecord', {
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
angular.module('creditcard').factory('AuditRecurrPlanHeaderService', ['$resource',
	function($resource) {
		return $resource('auditrecurringplanheaderrecord/:Header_ID', {
	        Plan_ID: '@Header_ID'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
angular.module('creditcard').factory('AuditRecurrPlanDetailService', ['$resource',
    function($resource) {
        return $resource('auditrecurringplandetailrecords/:Audit_Header_ID', {
	        Plan_ID: '@Audit_Header_ID'
        },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
