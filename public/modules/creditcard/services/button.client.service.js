'use strict';

// Users service used for communicating with the vauly REST endpoint
angular.module('creditcard').factory('ProductButtonService', ['$resource',
	function($resource) {
		return $resource('merchant_product/:boardingId', {
            boardingId: '@boardingId'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
angular.module('creditcard').factory('DonationButtonService', ['$resource',
	function($resource) {
		return $resource('merchant_donation/:boardingId', {
            boardingId: '@boardingId'
	    },  {
			update: {
				method: 'PUT'
			}
		});
	}
]);
