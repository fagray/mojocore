'use strict';

//Testapis service used to communicate Testapis REST endpoints
angular.module('creditcard').factory('ReserveService', ['$resource',
	function($resource) {
		return $resource('reserve/');
	}
]);
