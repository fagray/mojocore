'use strict';

//Testapis service used to communicate Testapis REST endpoints
angular.module('creditcard').factory('Nmitransactions', ['$resource',
	function($resource) {
		return $resource('nmitransactions/');
	}
]);
