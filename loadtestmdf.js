var fs = require('fs'),
	request = require('request'),
	log4js = require('log4js');


var logging = {
    	appenders: [
	        {
	            type: 'file',
	            filename: 'log/loadtest.log',
	            category: [ 'loadtest','console' ]
	        },
	        {
	            type: 'console'
	        }
	    ]
	};

log4js.configure(logging);
var Log = log4js.getLogger('loadtest');

var productMDF1 = '',
    productMDF2 = '',
    productMDF3 = '',
    productMDF4 = '',
    productMDF5 = '',
    productMDF6 = '',
    productMDF7 = '',
    productMDF8 = '',
    productMDF9 = '',
    productMDF10 = '',
    adminMDF1 = '',
    adminMDF2 = '',
    adminMDF3 = '',
    adminMDF4 = '',
    adminMDF5 = '',
    merchantMDF1 = '',
    merchantMDF2 = '',
    merchantMDF3 = '',
    merchantMDF4 = '',
    merchantMDF5 = '';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

/* global vars */

var root = "https://localhost:3000/merchantapi?",
	url,
	json,
	request,
	timelimit,
	amount,
	jsonNameFile,
	numberTransactions,
	text = '';

/* convert json file to json var*/
function fileToVar(csvString) {
    var json = [];
    var csvArray = csvString.split("\n");
    var csvColumns = JSON.parse("[" + csvArray.shift().replace(/'/g, '"') + "]");
    csvArray.forEach(function(csvRowString) {
        var csvRow = csvRowString.split(",");
        jsonRow = new Object();
        for ( var colNum = 0; colNum < csvRow.length; colNum++) {
            var colData = csvRow[colNum].replace(/^['"]|['"]$/g, "");
            jsonRow[csvColumns[colNum]] = colData;
        }
        json.push(jsonRow);
    });
    return JSON.stringify(json);
};

/* get arguments */
process.argv.forEach(function (val, index, array) {
  if (index===2) timelimit = val;
  if (index===3) numberTransactions = val;
  if (index===4) jsonNameFile = val;
});

if (typeof timelimit==='undefined'){
	timelimit = 300;
	console.log('Time between Transactions was not defined, we will set at 300 mili seconds');
}

if (typeof numberTransactions==='undefined'){
	numberTransactions = 1000000;
	console.log('Transactions items was not defined, we will do 1 Millon');
}

if (typeof jsonNameFile==='undefined'){
	console.log('A file test is required');
}else{
	fs.exists(jsonNameFile, function(exists) {
		if (!exists) {
			console.log('File do not exist');
		}else{
			var jsonFile = fs.readFileSync(jsonNameFile, 'utf8');
				json = fileToVar(jsonFile);
				json = JSON.parse(json);

			// closure function, resolve timeout between requests
			for (var i = 1; i <= numberTransactions; i++) {
			    setTimeout(function(x) {
			    	return function() {
			    		amount = (Math.random() * 1000).toFixed(2);

                        productMDF1 = 'productMDF1' + String((Math.random() * 1000).toFixed(0));
                        productMDF2 = 'productMDF2' + String((Math.random() * 1000).toFixed(0));
                        productMDF3 = 'productMDF3' + String((Math.random() * 1000).toFixed(0));
                        productMDF4 = 'productMDF4' + String((Math.random() * 1000).toFixed(0));
                        productMDF5 = 'productMDF5' + String((Math.random() * 1000).toFixed(0));
                        productMDF6 = 'productMDF6' + String((Math.random() * 1000).toFixed(0));
                        productMDF7 = 'productMDF7' + String((Math.random() * 1000).toFixed(0));
                        productMDF8 = 'productMDF8' + String((Math.random() * 1000).toFixed(0));
                        productMDF9 = 'productMDF9' + String((Math.random() * 1000).toFixed(0));
                        productMDF10 = 'productMDF10' + String((Math.random() * 1000).toFixed(0));
                        adminMDF1 = 'adminMDF1' + String((Math.random() * 1000).toFixed(0));
                        adminMDF2 = 'adminMDF2' + String((Math.random() * 1000).toFixed(0));
                        adminMDF3 = 'adminMDF3' + String((Math.random() * 1000).toFixed(0));
                        adminMDF4 = 'adminMDF4' + String((Math.random() * 1000).toFixed(0));
                        adminMDF5 = 'adminMDF5' + String((Math.random() * 1000).toFixed(0));
                        merchantMDF1 = 'merchantMDF1' + String((Math.random() * 1000).toFixed(0));
                        merchantMDF2 = 'merchantMDF2' + String((Math.random() * 1000).toFixed(0));
                        merchantMDF3 = 'merchantMDF3' + String((Math.random() * 1000).toFixed(0));
                        merchantMDF4 = 'merchantMDF4' + String((Math.random() * 1000).toFixed(0));
                        merchantMDF5 = 'merchantMDF5' + String((Math.random() * 1000).toFixed(0));

                            url = root +
                        'type=' + json[0].type +
                        '&processorId=' + json[0].processorId +
                        '&customerHash=' + json[0].customerHash +
                        '&amount=' + amount +
                        '&token=' + json[0].token +
                        '&productMDF1=' + productMDF1 +
                        '&productMDF2=' + productMDF2 +
                        '&productMDF3=' + productMDF3 +
                        '&productMDF4=' + productMDF4 +
                        '&productMDF5=' + productMDF5 +
                        '&productMDF6=' + productMDF6 +
                        '&productMDF7=' + productMDF7 +
                        '&productMDF8=' + productMDF8 +
                        '&productMDF9=' + productMDF9 +
                        '&productMDF10=' + productMDF10 +
                        '&adminMDF1=' + adminMDF1 +
                        '&adminMDF2=' + adminMDF2 +
                        '&adminMDF3=' + adminMDF3 +
                        '&adminMDF4=' + adminMDF4 +
                        '&adminMDF5=' + adminMDF5 +
                        '&merchantMDF1=' + merchantMDF1 +
                        '&merchantMDF2=' + merchantMDF2 +
                        '&merchantMDF3=' + merchantMDF3 +
                        '&merchantMDF4=' + merchantMDF4 +
                        '&merchantMDF5=' + merchantMDF5;
			    		console.log(x + ' ' + url);
                        console.log('');
			    		//
			    		request({url:url,async:true});
						/*request({url:url,async:true}, function (error, response, body) {
						    if (error || response.statusCode !== 200) {
						     	Log.debug(error+'\r\n');
						     }
						});*/
			    	};
			    }(i), parseInt(timelimit)*i);
			}
		}
	});
}

