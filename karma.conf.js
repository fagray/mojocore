'use strict';

/**
 * Module dependencies.
 */
// #gb var applicationConfiguration = require('./config/config');
// Karma configuration

module.exports = function(config) {
    config.set({
        // Frameworks to use
        frameworks: ['jasmine-jquery', 'jasmine'],

        // List of files / patterns to load in the browser
        // #gb files: applicationConfiguration.assets.lib.js.concat(applicationConfiguration.assets.js, applicationConfiguration.assets.tests),
        files: [
            'public/lib/angular/angular.js',
            'public/lib/angular-resource/angular-resource.js', 
            'public/lib/angular-cookies/angular-cookies.js', 
            'public/lib/angular-animate/angular-animate.js', 
            'public/lib/angular-touch/angular-touch.js', 
            'public/lib/angular-sanitize/angular-sanitize.js', 
            'public/lib/angular-ui-router/release/angular-ui-router.js',
            'public/lib/angular-ui-utils/ui-utils.js',
            'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
            'public/lib/exportFiles/FileSaver.js',
            'public/lib/exportFiles/FileSaverCSV.js',
            'public/lib/jquery/dist/jquery.js',
            'public/lib/jasmine-jquery/jasmine-jquery.js',
            'public/lib/chart/Chart.js',
            'public/lib/angular-chart/angular-chart.js',
            'public/lib/angular-mocks/angular-mocks.js',
            'public/config.js',
            'public/application.js',
            'public/modules/*/*.js',
            'public/modules/*/*[!tests]*/*.js',
            'public/modules/core/tests/header.client.controller.test.js',
            'public/modules/core/tests/home.client.controller.test.js',
            'public/modules/core/tests/menus.client.service.test.js',
            'public/modules/users/tests/authentication.client.controller.test.js',
            'public/modules/users/tests/nation-builder-oauth-callback.client.controller.test.js',
            'public/modules/users/tests/nation_builder_integration.client.controller.test.js',
            'public/modules/users/tests/password.client.controller.test.js',
            'public/modules/users/tests/settings.client.controller.test.js',
            'public/modules/creditcard/tests/misc.client.controller.test.js',
            'public/modules/creditcard/tests/modalemailconfirmation.client.controller.test.js',
            'public/modules/creditcard/tests/modalconf_nation_builder.client.controller.test.js',
            'public/modules/creditcard/tests/boarding.client.controller.test.js',
            'public/modules/creditcard/tests/modalproduct.client.controller.test.js',
            'public/modules/creditcard/tests/modalconfirmation.client.controller.test.js',
            'public/modules/creditcard/tests/transactions.client.controller.test.js',
            'public/modules/creditcard/tests/integration.client.controller.test.js',
            'public/modules/creditcard/tests/modal.client.controller.test.js',
			'public/modules/creditcard/tests/modalrecurringdeleteconfirmation.client.controller.test.js',
			'public/modules/creditcard/tests/miscfunctionality.client.controller.test.js',
			'public/modules/creditcard/tests/dropdown.client.controller.test.js',
            'public/modules/creditcard/tests/auditrecurringplancontroller.client.controller.test.js',
            'public/modules/creditcard/tests/tsnapshot.client.controller.test.js',
            'public/modules/creditcard/tests/dashboard.client.controller.test.js',
            'public/modules/creditcard/tests/modalconfboard.client.controller.test.js',
            'public/modules/creditcard/tests/modalbuttonsconfirmation.client.controller.test.js',
            'public/modules/creditcard/tests/recurringpayments.client.controller.test.js',
            'public/modules/creditcard/tests/customervault.client.controller.test.js',
            'public/modules/creditcard/tests/subscription.client.controller.test.js',
            'public/modules/creditcard/tests/buttons.client.controller.test.js',
            'public/modules/creditcard/tests/nmitransactions.client.controller.test.js'
            
//          'public/modules/creditcard/tests/reserve.spec.js'
        ],
        // Test results reporter to use
        // Possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        //reporters: ['progress'],
        reporters: ['progress', 'coverage'],

        // Web server port
        port: 9876,

        // Enable / disable colors in the output (reporters and logs)
        colors: true,

        // Level of logging
        // Possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        client: {
            captureConsole: true
        },

        // Enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        /// #gb browsers: ['PhantomJS'],
        browsers: ['Chrome'],

        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,

        // Continuous Integration mode
        // If true, it capture browsers, run tests and exit
        singleRun: true,
        
        preprocessors: {
            'public/modules/*/*.js': ['coverage'],
            'public/modules/*/*[!tests]*/*.js': ['coverage']
        },
        
        coverageReporter: {
            type : 'html',
            dir : 'coverage/'
        }
        
    });
};