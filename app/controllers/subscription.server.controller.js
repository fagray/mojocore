'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
	config = require('./../../config/config'),
    Q =  require('q'),
    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    };


/**
 * Create a Subscription
 */
exports.create = function(req, res) {
    var subscriptionObject = {
        Customer_Token: req.body.customer_id,
        Plan_ID: req.body.plan_id,
        Start_Date: req.body.start_date,
        Order_ID: req.body.order_id,
        Order_Description: req.body.order_description,
        PO_Number: req.body.po_number,
        Shipping: req.body.shipping,
        Tax: req.body.tax,
        Tax_Exempt: req.body.tax_exempt,
        Next_Charge_Date: req.body.next_charge_date,
        Is_Active: 1
    };

    db.Subscription.create(subscriptionObject).success(function(subscription,err){
        if(!subscription){
            return res.send('error', {errors: err});
        } else {
            return res.jsonp(subscription);
        }
    }).error(function(err){
        return res.send('error', {
        error: err,
        status: 500
        });
    });
};

exports.subscription = function(req, res, next, id) {
    var product = req.params.productId;
    if (product) {
        if(product === 'all') {
            db.sequelize.query('SELECT sub.id as sub_ID, recurr.id as Recurr_ID, sub.createdAt as Created, sub.*, recurr.*, Vaults.* FROM Subscriptions sub INNER JOIN Recurring_Plans recurr ON recurr.Plan_ID = sub.Plan_ID JOIN Vaults ON sub.customer_token = Vaults.customer_vault_id WHERE sub.Is_Active = 1 AND Boarding_ID = :boardingId',null,{ raw: true },
            { boardingId: id })
            .success(function(subscription){
                if(!subscription) {
                    req.subscription = {clean:'clean'};
                    return next();
                } else {
                    req.subscription = {response:subscription};
                    return next();
                }
            }).error(function(err){
                return next(err);
            });

        } else {
            db.sequelize.query('SELECT sub.id as sub_ID, recurr.id as Recurr_ID, sub.createdAt as Created, sub.*, recurr.*, Vaults.* FROM Subscriptions sub INNER JOIN Recurring_Plans recurr ON recurr.Plan_ID = sub.Plan_id JOIN Vaults ON sub.customer_token = Vaults.customer_vault_id WHERE sub.Is_Active = 1 AND Boarding_ID = :boardingId AND Product_ID = :product',null,{ raw: true },
            { boardingId: id, product: product })
            .success(function(subscription){
                if(!subscription) {
                    req.subscription = {clean:'clean'};
                    return next();
                } else {
                    req.subscription = {response:subscription};
                    return next();
                }
            }).error(function(err){
                return next(err);
            });
        }
    } else {
        req.subscription = {clean:'clean'};
        return next();
    }
};


/**
 * Update a recurring_plan
 */
exports.update = function(req, res) {
    db.Subscription.find({ where: {id: req.body.id}}).success(function(subscription_record) {
        if(subscription_record) {
            var subscription = subscription_record; 
            subscription.updateAttributes({
                Customer_Token: req.body.Customer_Token,
                Plan_ID: req.body.Plan_ID,
                Start_Date: req.body.Start_Date,
                Order_ID: req.body.Order_ID,
                Order_Description: req.body.Order_Description,
                PO_Number: req.body.PO_Number,
                Shipping: req.body.Shipping,
                Tax: req.body.Tax,
                Tax_Exempt: req.body.Tax_Exempt,
                Completed_Payments: req.body.Completed_Payments,
                Attempted_Payments: req.body.Attempted_Payments,
                Count_Failed: req.body.Count_Failed,
                Next_Charge_Date: req.body.Next_Charge_Date,
                Is_Active: req.body.Is_Active
            }).success(function(a){
                return res.jsonp(a);
            }).error(function(err){
                return res.render('error', {
                    error: err, 
                    status: 500
                });
            });
        } // if(subscription_record) {
    }).error(function(err){
        console.error(err);
    });

};


exports.findOne = function(req, res) {
    var subId = req.params.subId;
    db.Subscription.find({where: {id: subId}}).success(function(subscription, err) {
        if(!subscription) {
            return res.jsonp({NoResults:true });
        } else {
            return res.jsonp({Response: subscription});
        }
    }).error(function(err){
        return err;
    });
};

exports.all = function(req, res) {
    var query = '';
    var arg = {};
    var boardingId = req.param('boarding'),
        productId  = req.param('product'),
        planId     = req.param('planId'),
        firstName  = req.param('firstName'),
        lastName   = req.param('lastName'),
        email      = req.param('email'),
        last4cc    = req.param('last4cc'),
        custId     = req.param('cust');
        
        query = 'SELECT sub.id as sub_ID, recurr.id as Recurr_ID, sub.createdAt as Created, sub.*, recurr.*, Vaults.*, pp.* FROM Subscriptions sub INNER JOIN Recurring_Plans recurr ON recurr.Plan_ID = sub.Plan_id JOIN Vaults ON sub.customer_token = Vaults.customer_vault_id join Payment_Plans pp on recurr.Processor_ID = pp.Processor_ID WHERE sub.Is_Active = 1';
        if ((typeof req.param('boarding') !== 'undefined') && (req.param('boarding') !== '') && (req.param('boarding') !== 'all')) {
            query += ' AND recurr.Boarding_ID = :boardingId';
			arg.boardingId = req.param('boarding');
        }
        if ((typeof req.param('product') !== 'undefined') && (req.param('product') !== '') && (req.param('product') !== 'all')) {
            query += ' AND Product_ID = :productId';
			arg.productId = req.param('product');
        }
        if ((typeof req.param('planId') !== 'undefined') && (req.param('planId') !== '') && (req.param('planId') !== 'all')) {
            query += ' AND sub.Plan_ID = :planId';
			arg.planId = req.param('planId');
        }
        if ((typeof req.param('firstName') !== 'undefined') && (req.param('firstName') !== '') && (req.param('firstName') !== 'all')) {
            query += ' AND first_name LIKE :firstName';
			arg.firstName = '%' + req.param('firstName') + '%';
        }
        if ((typeof req.param('lastName') !== 'undefined') && (req.param('lastName') !== '') && (req.param('lastName') !== 'all')) {
            query += ' AND last_name LIKE :lastName';
			arg.lastName =  '%' + req.param('lastName') + '%';
        }
        if ((typeof req.param('email') !== 'undefined') && (req.param('email') !== '') && (req.param('email') !== 'all')) {
            query += ' AND email LIKE :email';
			arg.email =  '%' + req.param('email') + '%';
        }
        if ((typeof req.param('last4cc') !== 'undefined') && (req.param('last4cc') !== '') && (req.param('last4cc') !== 'all')) {
            query += ' AND cc_number LIKE :last4cc';
			arg.last4cc =  '%' + req.param('last4cc');
        }
        if ((typeof req.param('cust') !== 'undefined') && (req.param('cust') !== '') && (req.param('cust') !== 'all')) {
            query += ' AND Vaults.id = :cust';
			arg.cust =  req.param('cust');
        }
        
        db.sequelize.query(query,null,{ raw: true },
		    arg
        ).success(function(subscription){
            if(!subscription) {
                return res.jsonp({response:null});
            } else {
                return res.jsonp({response: subscription});
            }
        }).error(function(err){
            return res.jsonp({response:err});
        });
};


/**
 * Show a subscription
 */
exports.show = function(req, res) {
    // Sending down the subscription that was just preloaded by the subscription.subscription function
    // and saves subscription on the req object.
    return res.jsonp(req.subscription);
};
