'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize');

/**
 * Find boarding by id
 * Note: This is called every time that the parameter :boardingId is used in a URL. 
 * Its purpose is to preload the boarding on the req object then call the next function. 
 */
exports.disbursements = function(req, res, next, id) {
    db.Disbursements.find({ where: {id: id}}).success(function(disbursements){
        if(!disbursements) {
            return next(new Error('Failed to load disbursements ' + id));
        } else {
            req.disbursements = disbursements;
            return next();            
        }
    }).error(function(err){
        return next(err);
    });
};

/**
 * Create a boarding
 */
exports.create = function(req, res) {
    // augment the boarding by adding the UserId
    req.body.UserId = req.user.id;
    // save and return and instance of boarding on the res object. 
    db.Disbursements.create(req.body).success(function(disbursements,err){
        if(!disbursements){
            return res.send('users/signup', {errors: err});
        } else {
            return res.jsonp(disbursements);
        }
    }).error(function(err){
        return res.send('users/signup', { 
            errors: err,
            status: 500
        });
    });
};

/**
 * Update a boarding
 */
exports.update = function(req, res) {

    // create a new variable to hold the boarding that was placed on the req object.

    var disbursements = req.disbursements;
    disbursements.updateAttributes({
    
    }).success(function(a){
        return res.jsonp(a);
    }).error(function(err){
        return res.render('error', {
            error: err, 
            status: 500
        });
    });
};

/**
 * Delete an boarding
 */
exports.destroy = function(req, res) {

    // create a new variable to hold the boarding that was placed on the req object.
    var disbursements = req.disbursements;

    disbursements.destroy().success(function(){
        return res.jsonp(disbursements);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

/**
 * Show an boarding
 */
exports.show = function(req, res) {
    // Sending down the boarding that was just preloaded by the boardings.boarding function
    // and saves boarding on the req object.
    return res.jsonp(req.disbursements);
};

/**
 * List of boardings
 */
exports.all = function(req, res) {
    db.Disbursements.findAll().success(function(disbursementss){
        return res.jsonp(disbursementss);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};
