'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
	config = require('./../../config/config'),
    Q =  require('q'),
    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    };
/**
 * Find Pending Line Items
 * Note: This is called from the Add Line Item modal once Merchant and Product are selected. 
 * Its purpose is to show pending Line Items not yet in the MAR/mas table. 
 *
 * MC-645
 */

 exports.Line_Items = function(req, res) {

    var deferred = Q.defer();
    var processorId = req.params.processorId;
 
	db.sequelize.query('select * from Line_Items where Line_Items.id not in (select Line_Item_ID from MAS where Line_Item_ID is not null) and Processor_ID = :processorId',null,{ raw: true },
        { processorId: processorId }
    ).then(function(lineItems) {
        if (!lineItems){
            return res.jsonp({});
        }else{
            return res.jsonp({response:lineItems});
        }
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};
