'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
	config = require('./../../config/config'),
    Q =  require('q'),
    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    };
/**
 * Find Product by id
 * Note: This is called every time that the parameter :productId is used in a URL. 
 * Its purpose is to preload the products on the req object then call the next function. 
 */
exports.product = function(req, res, next, id) {
    var deferred = Q.defer();
    var productId = req.params.productId;
    if (productId){
        if (productId==='all'){
            db.Product.findAll().success(function(product){
                if(!product) {
                    req.product = {clean:'clean'};
                    return next();   
                } else {
                    req.product = toObject(product);
                    return next();            
                }
            }).error(function(err){
                return next(err);
            });
        }else{
            if (productId!=='all'){
                db.Product.find({ where: {id: productId}}).success(function(product){
                    if(!product) {
                        req.product = {clean:'clean'};
                        return next();
                    } else {
                        req.product = product;
                        return next();            
                    }
                }).error(function(err){
                    return next(err);
                });
            }
        }
    }
    return deferred.promise;
};

exports.show = function(req, res) {
    // Sending down the product that was just preloaded by the product.product function
    // and saves product on the req object.
    return res.jsonp(req.product);
};
