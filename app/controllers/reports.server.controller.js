'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    Q =  require('q'),
	config = require('./../../config/config');

exports.getAccountBalance = function(req,res){
    var productId = req.param('productId'),
        boardingId = req.param('boardingId'),
        startDate = req.param('startDate');

	var sql = '',
		arg = {};
	if (productId && productId !== '' && productId !== 'all')
	{
		if (sql === '')
		{
			sql = 'WHERE ';
		} else {
			sql += 'AND ';
		}
		sql += 'p.Product_ID = :productid ';
		arg.productid = productId;
	}
	if (boardingId && boardingId !== '' && boardingId !== 'all')
	{
		if (sql === '')
		{
			sql = 'WHERE ';
		} else {
			sql += 'AND ';
		}
		sql += 'p.Boarding_ID = :boardingId ';
		arg.boardingId = boardingId;
	}
	if (startDate && startDate !== '')
	{
		if (sql === '')
		{
			sql = 'WHERE ';
		} else {
			sql += 'AND ';
		}
		sql += 'acc.Date = :startDate ';
		arg.startDate = startDate;
	}
	sql = 'SELECT acc.*,p.Boarding_ID,b.Merchant_Name,b.Merchant_ID,pro.Name,pro.Description '+
			'FROM Account_Balance as acc ' +
			'JOIN Payment_Plans as p on acc.Processor_ID = p.Processor_ID ' +
			'JOIN Boardings as b on p.Boarding_ID = b.id ' +
			'JOIN Products as pro on p.Product_ID = pro.id ' + sql;

	db.sequelize.query(sql, null, { raw: true }, arg
	).success(function(results) {
			if (!results){
				return res.jsonp({NoResults:true });
			}else{
				return res.jsonp({Response: results});
			}
		}
	).error(function(err) {
		console.log(err);
	});

//    if ((product === 'all')&&(merchant === 'all')){
//        db.sequelize.query('select acc.*,p.Boarding_ID,b.Merchant_Name,b.Merchant_ID,pro.Name,pro.Description from Account_Balance as acc join Payment_Plans as p on acc.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Products as pro on p.Product_ID = pro.id where acc.Date = :startDate group by Date,Processor_ID;',null,{ raw: true },
//            {startDate: startDate}
//        ).success(function(results) {
//                if (!results){
//                    return res.jsonp({NoResults:true });
//                }else {
//                    return res.jsonp({Response: results});
//                }
//            });
//    }else if((product === 'all')&&(merchant !== 'all')){
//        db.sequelize.query('select acc.*,p.Boarding_ID,b.Merchant_Name,b.Merchant_ID,pro.Name,pro.Description from Account_Balance as acc join Payment_Plans as p on acc.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Products as pro on p.Product_ID = pro.id where p.Boarding_ID = :boarding and acc.Date = :startDate group by Date,Processor_ID;',null,{ raw: true },
//            { boarding: merchant,startDate: startDate }
//        ).success(function(results) {
//                if (!results){
//                    return res.jsonp({NoResults:true });
//                }else{
//                    return res.jsonp({Response: results});
//                }
//            });
//    }else if((product !== 'all')&&(merchant === 'all')){
//        db.sequelize.query('select acc.*,p.Boarding_ID,b.Merchant_Name,b.Merchant_ID,pro.Name,pro.Description from Account_Balance as acc join Payment_Plans as p on acc.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Products as pro on p.Product_ID = pro.id where p.Product_ID = :productid and acc.Date = :startDate group by Date,Processor_ID;',null,{ raw: true },
//            { productid: product,startDate: startDate }
//        ).success(function(results) {
//                if (!results){
//                    return res.jsonp({NoResults:true });
//                }else{
//                    return res.jsonp({Response: results});
//                }
//            });
//    }else if((product !== 'all')&&(merchant !== 'all')){
//        db.sequelize.query('select acc.*,p.Boarding_ID,b.Merchant_Name,b.Merchant_ID,pro.Name,pro.Description from Account_Balance as acc join Payment_Plans as p on acc.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Products as pro on p.Product_ID = pro.id where p.Product_ID = :productid and p.Boarding_ID = :boarding AND acc.Date = :startDate group by by Date,Processor_ID;',null,{ raw: true },
//            { productid: product,boarding: merchant,startDate: startDate }
//        ).success(function(results) {
//                if (!results){
//                    return res.jsonp({NoResults:true });
//                }else{
//                    return res.jsonp({Response: results});
//                }
//            }
//		).error(function(err) {
//			console.log(err);
//		});
//    }
};

exports.getMerchantActivity = function(req, res) {
    var product = req.param('productId'),
        merchant = req.param('boardingId'),
        startDate = req.param('startDate'),
        endDate = req.param('endDate');
    if ((product === 'all')&&(merchant === 'all')){
        db.sequelize.query('select m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,pro.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Products as pro on pro.id = p.Product_ID where m.Original_Date <= :endDate and m.Original_Date >= :startDate group by Original_Date,Boarding_ID,Processor_ID,Payment_Date,Type,Payment_type;',null,{ raw: true },
            {endDate: endDate,startDate: startDate}
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else {
                    return res.jsonp({Response: results});
                }
            });
    }else if((product === 'all')&&(merchant !== 'all')){
        db.sequelize.query('select m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,pro.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Products as pro on pro.id = p.Product_ID where p.Boarding_ID = :boarding and m.Original_Date <= :endDate and m.Original_Date >= :startDate group by Original_Date,Boarding_ID,Processor_ID,Payment_Date,Type,Payment_type',null,{ raw: true },
            { boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                   return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant === 'all')){
        db.sequelize.query('select m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,pro.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Products as pro on pro.id = p.Product_ID where p.Product_ID = :productid AND m.Original_Date <= :endDate and m.Original_Date >= :startDate group by Original_Date,Boarding_ID,Processor_ID,Payment_Date,Type,Payment_type;',null,{ raw: true },
            { productid: product,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant !== 'all')){
        db.sequelize.query('select m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,pro.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Products as pro on pro.id = p.Product_ID where p.Product_ID = :productid and p.Boarding_ID = :boarding AND m.Original_Date <= :endDate and m.Original_Date >= :startDate group by Original_Date,Boarding_ID,Processor_ID,Payment_Date,Type,Payment_type;',null,{ raw: true },
            { productid: product,boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }
};

exports.getPaymentDates = function(req,res){
    var product = req.param('productId'),
        merchant = req.param('boardingId'),
        startDate = req.param('startDate'),
        endDate = req.param('endDate');
    if ((product === 'all')&&(merchant === 'all')){
        db.sequelize.query('select a.*,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,pro.Description from Account_Balance as a join Payment_Plans as p on p.Processor_ID = a.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Products as pro on pro.id = p.Product_ID where a.Date <= :endDate and a.Date >= :startDate order by Date, Boarding_ID;',null,{ raw: true },
            {endDate: endDate,startDate: startDate}
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else {
                    return res.jsonp({Response: results});
                }
            });
    }else if((product === 'all')&&(merchant !== 'all')){
        db.sequelize.query('select sum(a.Gross_Amount) as Gross_Amount,sum(a.Net_Amount) as Net_Amount,sum(a.Split_Amount) as Split_Amount,sum(a.Reserve_Amount) as Reserve_Amount,sum(a.Misc_Fee_Amount) as Misc_Fee_Amount,sum(a.Disbursed_Transactions_Amount) as Disbursed_Transactions_Amount,sum(a.Disbursed_Fees_Amount) as Disbursed_Fees_Amount,a.Date,sum(a.Disbursement) as Disbursement,sum(a.Previous_Net_Plus_Gross) as Previous_Net_Plus_Gross,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,pro.Description from Account_Balance as a join Payment_Plans as p on p.Processor_ID = a.Processor_ID  join Boardings as b on p.Boarding_ID = b.id join Products as pro on pro.id = p.Product_ID where p.Boarding_ID = :boarding AND a.Date <= :endDate and a.Date >= :startDate group by p.Boarding_ID,a.Date order by Date, Boarding_ID;',null,{ raw: true },
            { boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant === 'all')){
        db.sequelize.query('select a.*,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,pro.Description  from Account_Balance as a join Payment_Plans as p on p.Processor_ID = a.Processor_ID  join Boardings as b on p.Boarding_ID = b.id join Products as pro on pro.id = p.Product_ID where p.Product_ID = :productid AND a.Date <= :endDate and a.Date >= :startDate order by Date;',null,{ raw: true },
            { productid: product,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant !== 'all')){
        db.sequelize.query('select a.*,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,pro.Description from Account_Balance as a join Payment_Plans as p on p.Processor_ID = a.Processor_ID  join Boardings as b on p.Boarding_ID = b.id join Products as pro on pro.id = p.Product_ID where p.Product_ID = :productid and p.Boarding_ID = :boarding AND a.Date <= :endDate and a.Date >= :startDate order by Date, Boarding_ID;',null,{ raw: true },
            { productid:product, boarding:merchant, endDate:endDate, startDate:startDate }
        ).success(function(results){
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }
};

exports.getLineItemsTransactions = function(req,res){
    var product = req.param('productId'),
        merchant = req.param('boardingId'),
        startDate = req.param('startDate'),
        endDate = req.param('endDate'),
        type = req.param('type');
    if ((product === 'all')&&(merchant === 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.Description,t.Occurrence_date as occurrencedate,t.Debit_Credit as drcr,x.Description as productdescription from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id left join Line_Items as t on t.id = m.Line_Item_ID where m.Original_Date <= :endDate and m.Original_Date >= :startDate and m.Type = :type;',null,{ raw: true },
            {endDate: endDate,startDate: startDate,type:type}
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else {
                    return res.jsonp({Response: results});
                }
            });
    }else if((product === 'all')&&(merchant !== 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.Description,t.Occurrence_Date as occurrencedate,t.Debit_Credit as drcr,x.Description as productdescription from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id left join Line_Items as t on t.id = m.Line_Item_ID where p.Boarding_ID = :boarding and m.Original_Date <= :endDate and m.Original_Date >= :startDate and m.Type = :type;',null,{ raw: true },
            { boarding: merchant,endDate: endDate,startDate: startDate,type:type}
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant === 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.Description,t.Occurrence_Date as occurrencedate,t.Debit_Credit as drcr,x.Description as productdescription from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id left join Line_Items as t on t.id = m.Line_Item_ID where p.Product_ID = :productid AND m.Original_Date <= :endDate and m.Original_Date >= :startDate and m.Type = :type;',null,{ raw: true },
            { productid: product,endDate: endDate,startDate: startDate,type:type}
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant !== 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.Description,t.Occurrence_Date as occurrencedate,t.Debit_Credit as drcr,x.Description as productdescription from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id left join Line_Items as t on t.id = m.Line_Item_ID where p.Product_ID = :productid and p.Boarding_ID = :boarding AND m.Original_Date <= :endDate and m.Original_Date >= :startDate and m.Type = :type;',null,{ raw: true },
            { productid: product,boarding: merchant,endDate: endDate,startDate: startDate,type:type}
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }
};

exports.getSettleTransactions = function(req, res) {
    var product = req.param('productId'),
        merchant = req.param('boardingId'),
        startDate = req.param('startDate'),
        endDate = req.param('endDate');
    if ((product === 'all')&&(merchant === 'all')){
          db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin,x.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where m.Original_Date <= :endDate and m.Original_Date >= :startDate and (Type = "SALE") and t.action_type <> "auth";',null,{ raw: true },
            {endDate: endDate,startDate: startDate}
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else {
                    return res.jsonp({Response: results});
                }
            });
    }else if((product === 'all')&&(merchant !== 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin,x.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where p.Boarding_ID = :boarding and m.Original_Date <= :endDate and m.Original_Date >= :startDate and (Type = "SALE") and t.action_type <> "auth";',null,{ raw: true },
            { boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant === 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin,x.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where p.Product_ID = :productid AND m.Original_Date <= :endDate and m.Original_Date >= :startDate and (Type = "SALE") and t.action_type <> "auth";',null,{ raw: true },
            { productid: product,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant !== 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin,x.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where p.Product_ID = :productid and p.Boarding_ID = :boarding AND m.Original_Date <= :endDate and m.Original_Date >= :startDate and (Type = "SALE") and t.action_type <> "auth";',null,{ raw: true },

            { productid: product,boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }
};

exports.getDiscounts = function(req, res) {
    var product = req.param('productId'),
        merchant = req.param('boardingId'),
        startDate = req.param('startDate'),
        endDate = req.param('endDate');
    if ((product === 'all')&&(merchant === 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where m.Original_Date <= :endDate and m.Original_Date >= :startDate and Type = "DISCOUNT" group by Original_Date,Payment_Date,Type,Payment_type,Processor_ID;',null,{ raw: true },
            {endDate: endDate,startDate: startDate}
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else {
                    return res.jsonp({Response: results});
                }
            });
    }else if((product === 'all')&&(merchant !== 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where p.Boarding_ID = :boarding and m.Original_Date <= :endDate and m.Original_Date >= :startDate and Type = "DISCOUNT" group by Original_Date,Payment_Date,Type,Payment_type,Processor_ID;',null,{ raw: true },
            { boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant === 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where p.Product_ID = :productid AND m.Original_Date <= :endDate and m.Original_Date >= :startDate and Type = "DISCOUNT" group by Original_Date,Payment_Date,Type,Payment_type,Processor_ID;',null,{ raw: true },
            { productid: product,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant !== 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where p.Product_ID = :productid and p.Boarding_ID = :boarding AND m.Original_Date <= :endDate and m.Original_Date >= :startDate and Type = "DISCOUNT" group by Original_Date,Payment_Date,Type,Payment_type,Processor_ID;',null,{ raw: true },
            { productid: product,boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }
};

exports.getTransactionFee = function(req, res) {
    var product = req.param('productId'),
        merchant = req.param('boardingId'),
        startDate = req.param('startDate'),
        endDate = req.param('endDate');
    if ((product === 'all')&&(merchant === 'all')){
        db.sequelize.query('select count(m.transaction_ID) as Count,m.Transaction_ID,m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join (SELECT first_name,last_name,currency,cc_number,cc_bin FROM Transaction_History WHERE transaction_id = Transaction_ID LIMIT 1) as t where m.Original_Date <= :endDate and m.Original_Date >= :startDate and (Type = "TRANSACTION_FEE" or Type = "REFUND_FEE" or Type = "VOID_FEE" or Type = "DECLINED_FEE") group by Original_Date,Payment_Date,t.cc_number,Type,Payment_type,Processor_ID;',null,{ raw: true },
            {endDate: endDate,startDate: startDate}
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else {
                    return res.jsonp({Response: results});
                }
            });
    }else if((product === 'all')&&(merchant !== 'all')){
        db.sequelize.query('select count(m.transaction_ID) as Count,m.Transaction_ID,m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join (SELECT first_name,last_name,currency,cc_number,cc_bin FROM Transaction_History WHERE transaction_id = Transaction_ID LIMIT 1) as t where p.Boarding_ID = :boarding and m.Original_Date <= :endDate and m.Original_Date >= :startDate and (Type = "TRANSACTION_FEE" or Type = "REFUND_FEE" or Type = "VOID_FEE" or Type = "DECLINED_FEE") group by Original_Date,Payment_Date,t.cc_number,Type,Payment_type,Processor_ID;',null,{ raw: true },
            { boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant === 'all')){
        db.sequelize.query('select count(m.transaction_ID) as Count,m.Transaction_ID,m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join (SELECT first_name,last_name,currency,cc_number,cc_bin FROM Transaction_History WHERE transaction_id = Transaction_ID LIMIT 1) as t where p.Product_ID = :productid AND m.Original_Date <= :endDate and m.Original_Date >= :startDate and (Type = "TRANSACTION_FEE" or Type = "REFUND_FEE" or Type = "VOID_FEE" or Type = "DECLINED_FEE") group by Original_Date,Payment_Date,t.cc_number,Type,Payment_type,Processor_ID;',null,{ raw: true },
            { productid: product,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant !== 'all')){
        db.sequelize.query('select count(m.transaction_ID) as Count,m.Transaction_ID,m.Original_Date,m.Type,sum(m.Adjusted_Amount) as Amount,sum(m.Original_Amount) as Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Boardings as b on p.Boarding_ID = b.id join (SELECT first_name,last_name,currency,cc_number,cc_bin FROM Transaction_History WHERE transaction_id = Transaction_ID LIMIT 1) as t where p.Product_ID = :productid and p.Boarding_ID = :boarding AND m.Original_Date <= :endDate and m.Original_Date >= :startDate and (Type = "TRANSACTION_FEE" or Type = "REFUND_FEE" or Type = "VOID_FEE" or Type = "DECLINED_FEE") group by Original_Date,Payment_Date,t.cc_number,Type,Payment_type,Processor_ID;',null,{ raw: true },
            { productid: product,boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }
};

exports.createBrand = function (req,res){
    db.sequelize.query('select * from Customer_Card_Hash',null,{ raw: true }
    ).success(function(results) {
            results.forEach(function(item) {
                var cc_number = item.cc_number,
                    brandName;
                if ((cc_number.substr(1, 1)==='x') || (cc_number.substr(1, 1)==='*')){
                    brandName = 'Unknown';
                }else{
                    // visa
                    var re = new RegExp('^4');
                    if (cc_number.match(re) !== null)
                        brandName = 'Visa';

                    // Mastercard
                    re = new RegExp('^5[1-5]');
                    if (cc_number.match(re) !== null)
                        brandName = 'Mastercard';

                    // AMEX
                    re = new RegExp('^3[47]');
                    if (cc_number.match(re) !== null)
                        brandName = 'AMEX';

                    // Discover
                    re = new RegExp('^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)');
                    if (cc_number.match(re) !== null)
                        brandName = 'Discover';

                    // Diners
                    re = new RegExp('^36');
                    if (cc_number.match(re) !== null)
                        brandName = 'Diners';

                    // Diners - Carte Blanche
                    re = new RegExp('^30[0-5]');
                    if (cc_number.match(re) !== null)
                        brandName = 'Diners - Carte Blanche';

                    // JCB
                    re = new RegExp('^35(2[89]|[3-8][0-9])');
                    if (cc_number.match(re) !== null)
                        brandName = 'JCB';

                    // Visa Electron
                    re = new RegExp('^(4026|417500|4508|4844|491(3|7))');
                    if (cc_number.match(re) !== null)
                        brandName = 'Visa Electron';
                }
                db.sequelize.query('UPDATE Customer_Card_Hash SET Brand = :bname WHERE id = :id',null,{ raw: true },
                    { id: item.id,bname: brandName }
                ).success(function(results) {
//                       console.log('Success');
                    });
            });
        });
};

exports.getACHActivity = function (req,res){
    var product = req.param('productId'),
        merchant = req.param('boardingId'),
        startDate = req.param('startDate'),
        endDate = req.param('endDate');
    if ((product === 'all')&&(merchant === 'all')){
        db.sequelize.query('SELECT ab.Processor_ID, bb.Merchant_Name, ab.Date, ab.Disbursed_Amount FROM Account_Balance ab INNER JOIN Payment_Plans pp on pp.Processor_ID = ab.Processor_ID INNER JOIN Boardings bb on bb.id = pp.Boarding_ID WHERE ab.Disbursement = 1 AND ab.Date >= :startDate AND ab.Date <= :endDate ORDER BY ab.Date, bb.Merchant_Name, ab.Processor_ID;',null,{ raw: true },
            {endDate: endDate,startDate: startDate}
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else {
                    return res.jsonp({Response: results});
                }
            });
    }else if((product === 'all')&&(merchant !== 'all')){
        db.sequelize.query('SELECT ab.Processor_ID, bb.Merchant_Name, ab.Date, ab.Disbursed_Amount FROM Account_Balance ab INNER JOIN Payment_Plans pp on pp.Processor_ID = ab.Processor_ID INNER JOIN Boardings bb on bb.id = pp.Boarding_ID WHERE ab.Disbursement = 1 AND ab.Date >= :startDate AND ab.Date <= :endDate AND bb.id = :boarding ORDER BY ab.Date, bb.Merchant_Name, ab.Processor_ID;',null,{ raw: true },
            { boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                   return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant === 'all')){
        db.sequelize.query('SELECT ab.Processor_ID, bb.Merchant_Name, ab.Date, ab.Disbursed_Amount FROM Account_Balance ab INNER JOIN Payment_Plans pp on pp.Processor_ID = ab.Processor_ID INNER JOIN Boardings bb on bb.id = pp.Boarding_ID WHERE ab.Disbursement = 1 AND ab.Date >= :startDate AND ab.Date <= :endDate AND pp.Product_ID = :productid ORDER BY ab.Date, bb.Merchant_Name, ab.Processor_ID;',null,{ raw: true },
            { productid: product,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant !== 'all')){
        db.sequelize.query('SELECT ab.Processor_ID, bb.Merchant_Name, ab.Date, ab.Disbursed_Amount FROM Account_Balance ab INNER JOIN Payment_Plans pp on pp.Processor_ID = ab.Processor_ID INNER JOIN Boardings bb on bb.id = pp.Boarding_ID WHERE ab.Disbursement = 1 AND ab.Date >= :startDate AND ab.Date <= :endDate AND pp.Product_ID = :productid AND bb.id = :boarding ORDER BY ab.Date, bb.Merchant_Name, ab.Processor_ID;',null,{ raw: true },
            { productid: product,boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }

};

exports.getRefundActivity = function (req,res) {
    var product = req.param('productId'),
        merchant = req.param('boardingId'),
        startDate = req.param('startDate'),
        endDate = req.param('endDate');
    if ((product === 'all')&&(merchant === 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin,x.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where m.Original_Date <= :endDate and m.Original_Date >= :startDate and Type = "REFUND";',null,{ raw: true },
            {endDate: endDate,startDate: startDate}
        ).success(function(results) {
            if (!results){
                return res.jsonp({NoResults:true });
            }else {
                return res.jsonp({Response: results});
            }
        });
    }else if((product === 'all')&&(merchant !== 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin,x.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where p.Boarding_ID = :boarding and m.Original_Date <= :endDate and m.Original_Date >= :startDate and Type = "REFUND";',null,{ raw: true },
            { boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            });
    }else if((product !== 'all')&&(merchant === 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin,x.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where p.Product_ID = :productid AND m.Original_Date <= :endDate and m.Original_Date >= :startDate and Type = "REFUND";',null,{ raw: true },
            { productid: product,endDate: endDate,startDate: startDate }
        ).success(function(results) {
            if (!results){
                return res.jsonp({NoResults:true });
            }else{
                return res.jsonp({Response: results});
            }
        });
    }else if((product !== 'all')&&(merchant !== 'all')){
        db.sequelize.query('select m.Transaction_ID,m.Original_Date,m.Type,m.Adjusted_Amount as Amount,m.Original_Amount,m.Payment_Date,m.Payment_Type,m.Processor_ID,p.Boarding_ID,p.Product_ID,b.Merchant_Name,b.Merchant_ID,t.first_name,t.last_name,t.currency,t.cc_number,t.cc_bin,x.Description from MAS as m join Payment_Plans as p on m.Processor_ID = p.Processor_ID join Products as x on x.id = p.Product_ID join Boardings as b on p.Boarding_ID = b.id join Transaction_History as t on t.transaction_id = m.Transaction_ID where p.Product_ID = :productid and p.Boarding_ID = :boarding AND m.Original_Date <= :endDate and m.Original_Date >= :startDate and Type = "REFUND";',null,{ raw: true },
            { productid: product,boarding: merchant,endDate: endDate,startDate: startDate }
        ).success(function(results) {
            if (!results){
                return res.jsonp({NoResults:true });
            }else{
                return res.jsonp({Response: results});
            }
        });
    }
};