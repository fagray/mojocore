'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    config = require('./../../config/config');


/**
 * Find merchant_product by id
 *  
 * Its purpose is to preload the mas on the req object then call the next function. 
 */
exports.merchant_product = function(req, res, next, id) {
    var params = {'Boarding_ID': id, 'Is_Active': 1};
    var recId = req.param('id');
    var prodName = req.param('Product_Name');
    if (recId) {
        params.id = recId;
    }
    if (prodName) {
        params.Product_Name = {like: "%" + prodName + "%"};
    }
// console.log('merchant_product findAll server body', params)
    db.Merchant_Product.findAll({ where: params}).success(function(merchant_product){
        if(!merchant_product) {
            req.merchant_product = {clean:'clean'};
            return next();   
        } else {
            req.merchant_product = {response:merchant_product};
            return next();            
        }
    }).error(function(err){
        return next(err);
    });
};


exports.create = function(req, res) {
// console.log('create merchant_product create body', req.body)
    // save and return and instance of merchant_product on the res object.
    var merchant_product = db.Merchant_Product.build(req.body);
    // Generate Hash 
    merchant_product.Hash = merchant_product.generateHash();
    merchant_product.save().success(function(merchant_product,err){
        if(!merchant_product){
            return res.send('users/signup', {errors: err});
        } else {
            return res.jsonp(merchant_product);
        }
    }).error(function(err){
        return res.send('users/signup', { 
            errors: err,
            status: 500
        });
    });
};


exports.update = function(req, res) {
// console.log('update merchant_product body', req.body)
    db.Merchant_Product.find({where: {id: req.body.id}}).success(function(merchant_product){
        if (merchant_product) {
            merchant_product.updateAttributes({
                Boarding_ID: req.body.Boarding_ID,
			    Is_Active: req.body.Is_Active,
			    Product_Name: req.body.Product_Name,
			    Product_Description: req.body.Product_Description,
                Product_SKU: req.body.Product_SKU, 
                Product_Price: req.body.Product_Price, 
                Tax: req.body.Tax, 
                Shipping: req.body.Shipping, 
                Hash: req.body.Hash,
                Button_Size_Display: req.body.Button_Size_Display
            }).success(function(a){
                return res.jsonp(a);
            }).error(function(err){
                return res.render('error', {
                    error: err, 
                    status: 500
                });
            });
        } // if (merchant_product)
    }).error(function(err){
        console.error(err);
    });
};


/**
 * Show a merchant_product
 */
exports.show = function(req, res) {
// console.log('merchant_product show body', req.body)
    // Sending down the merchant_product that was just preloaded by the merchant_product.merchant_product function
    // and saves merchant_product on the req object.
    return res.jsonp(req.merchant_product);
};
