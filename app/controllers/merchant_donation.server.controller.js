'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    config = require('./../../config/config');


/**
 * Find merchant_donation by id
 *  
 * Its purpose is to preload the mas on the req object then call the next function. 
 */
exports.merchant_donation = function(req, res, next, id) {
    var params = {'Boarding_ID': id, 'Is_Active': 1};
    var recId = req.param('id');
    var prodName = req.param('Product_Name');
    if (recId) {
        params.id = recId;
    }
    if (prodName) {
        params.Donation_Name = {like: "%" + prodName + "%"};
    }
// console.log('merchant_donation findAll server body', params)
    db.Merchant_Donation.findAll({ where: params}).success(function(merchant_donation){
        if(!merchant_donation) {
            req.merchant_donation = {clean:'clean'};
            return next();   
        } else {
            req.merchant_donation = {response:merchant_donation};
            return next();            
        }
    }).error(function(err){
        return next(err);
    });
};


exports.create = function(req, res) {
//console.log('create merchant_donation create body', req.body)
    // save and return and instance of merchant_donation on the res object. 
    var merchant_donation = db.Merchant_Donation.build(req.body);
    // Generate Hash 
    merchant_donation.Hash = merchant_donation.generateHash();
    merchant_donation.save().success(function(merchant_donation,err){
        if(!merchant_donation){
            return res.send('users/signup', {errors: err});
        } else {
            return res.jsonp(merchant_donation);
        }
    }).error(function(err){
        return res.send('users/signup', { 
            errors: err,
            status: 500
        });
    });
};


exports.update = function(req, res) {
//console.log('update merchant_donation body', req.body)
    db.Merchant_Donation.find({where: {id: req.body.id}}).success(function(merchant_donation){
        if (merchant_donation) {
            merchant_donation.updateAttributes({
                Boarding_ID: req.body.Boarding_ID,
			    Is_Active: req.body.Is_Active,
			    Donation_Name: req.body.Donation_Name,
			    Donation_Description: req.body.Donation_Description,
                Donation_Amount_1: req.body.Donation_Amount_1, 
                Donation_Amount_2: req.body.Donation_Amount_2, 
                Donation_Amount_3: req.body.Donation_Amount_3, 
                Donation_Amount_4: req.body.Donation_Amount_4, 
                Allow_Custom_Donations: req.body.Allow_Custom_Donations,
                Hash: req.body.Hash,
                Button_Size_Display: req.body.Button_Size_Display,
                Allow_Recurring_Donation: req.body.Allow_Recurring_Donation
            }).success(function(a){
                return res.jsonp(a);
            }).error(function(err){
                return res.jsonp({Response:err});
            });
        } // if (merchant_donation)
    }).error(function(err){
        console.error(err);
    });
};


/**
 * Show a merchant_donation
 */
exports.show = function(req, res) {
// console.log('merchant_donation show body', req.body)
    // Sending down the merchant_donation that was just preloaded by the merchant_donation.merchant_donation function
    // and saves merchant_donation on the req object.
    return res.jsonp(req.merchant_donation);
};
