'use strict';
/**
 * Module dependencies.
 */
 
var express    = require("express");
var mysql      = require("sequelize-mysql/node_modules/mysql");
var config    = require("./../../config/config");


exports.newrelic_mysql_query = function(req, res) {

    var connection = mysql.createConnection({
        host: config.db1.host,
        port: config.db1.port,
        user:config.db1.username,
        password: config.db1.password,
        database: config.db1.name
    });
    
    connection.query('SELECT * from Products LIMIT 2', function(err, rows, fields) {
        connection.end();
        if (!err) {
            if (rows.length === 2) {
                return res.jsonp('Query executed properly');
            } else {
                return res.jsonp('Error: Query returned wrong number of rows');
            }
        } else {
            return res.jsonp('Error while performing query');
        }
    });

};
