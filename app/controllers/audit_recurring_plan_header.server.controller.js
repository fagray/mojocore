'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    nodemailer = require('nodemailer'),
    crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq',
    cipher,
	config = require('./../../config/config'),
    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    };

/**
 * Create an audit recurring_plan
 */
exports.create = function(req, res) {
    var recurringPlanAuditObject = {
        Boarding_ID: req.body.Boarding_ID,
        Processor_ID: req.body.Processor_ID,
        Recurring_Plan_ID: req.body.Recurring_Plan_ID
    };
    // save and return and instance of payment_plan on the res object.

        db.Audit_Recurring_Plan_Header.create(recurringPlanAuditObject).success(function(audit_recurring_plan,err){
            if(!audit_recurring_plan){
                return res.send('error', {errors: err});
            } else {
                return res.jsonp(audit_recurring_plan);
            }
        }).error(function(err){
            return res.send('error', {
            error: err,
            status: 500
            });
        });
};



/**
 * Show an audit_recurring_plan
 */
exports.show = function(req, res) {
    // Sending down the payment_plan that was just preloaded by the payment_plans.payment_plan function
    // and saves payment_plan on the req object.
    return res.jsonp(req.audit_recurring_plan);
};

/**
 * List recurring_plans
 */
exports.all = function(req, res) {
    db.Audit_Recurring_Plan_Header.findAll().success(function(audit_recurring_plan){
        return res.jsonp(audit_recurring_plan);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

exports.findRecords = function(req, res) {
    var boardingId = req.param('boardingId');
    var productId = req.param('productId');
    var startDate = req.param('start_date');
    var endDate = req.param('end_date');

    if(productId === 'all' && boardingId === 'all') {
        db.sequelize.query('select h.*, rp.Plan_Name, u.Display_Name, p.Description, b.Merchant_Name from Audit_Recurring_Plan_Headers h join Recurring_Plans rp on h.Recurring_Plan_ID = rp.id join Users u on h.Boarding_ID = u.Boarding_ID join Boardings b on h.Boarding_ID = b.id join Payment_Plans pp on h.Processor_ID = pp.Processor_ID join Products p on p.id = pp.Product_id where h.CreatedAt >= :startDate AND h.CreatedAt <= :endDate',null,{ raw: true },
            { startDate: startDate, endDate: endDate }
        ).success(function(audit_recurring_plan_header){
            if (!audit_recurring_plan_header) {
                return res.jsonp({NoResults:true });
            }else{
                return res.jsonp({Response: audit_recurring_plan_header});
            }
        }).error(function(err){
            return res.render('error', {
                error: err,
                status: 500
            });
        });
    } else if(productId !== 'all' && boardingId === 'all') {
        db.sequelize.query('select h.*, rp.Plan_Name, u.Display_Name, p.Description, b.Merchant_Name from Audit_Recurring_Plan_Headers h join Recurring_Plans rp on h.Recurring_Plan_ID = rp.id join Users u on h.Boarding_ID = u.Boarding_ID join Boardings b on h.Boarding_ID = b.id join Payment_Plans pp on h.Processor_ID = pp.Processor_ID join Products p on p.id = pp.Product_id where p.id = :productId AND h.CreatedAt >= :startDate AND h.CreatedAt <= :endDate',null,{ raw: true },
            { productId: productId, startDate: startDate, endDate: endDate }
        ).success(function(audit_recurring_plan_header){
            if (!audit_recurring_plan_header) {
                return res.jsonp({NoResults:true });
            }else{
                return res.jsonp({Response: audit_recurring_plan_header});
            }
        }).error(function(err){
            return res.render('error', {
                error: err,
                status: 500
            });
        });
    } else if(productId === 'all' && boardingId !== 'all') {
        db.sequelize.query('select h.*, rp.Plan_Name, u.Display_Name, p.Description, b.Merchant_Name from Audit_Recurring_Plan_Headers h join Recurring_Plans rp on h.Recurring_Plan_ID = rp.id join Users u on h.Boarding_ID = u.Boarding_ID join Boardings b on h.Boarding_ID = b.id join Payment_Plans pp on h.Processor_ID = pp.Processor_ID join Products p on p.id = pp.Product_id where h.Boarding_id = :boardingId AND h.CreatedAt >= :startDate AND h.CreatedAt <= :endDate',null,{ raw: true },
            { boardingId: boardingId, startDate: startDate, endDate: endDate }
        ).success(function(audit_recurring_plan_header){
            if (!audit_recurring_plan_header) {
                return res.jsonp({NoResults:true });
            }else{
                return res.jsonp({Response: audit_recurring_plan_header});
            }
        }).error(function(err){
            return res.render('error', {
                error: err,
                status: 500
            });
        });
    } else if(productId !== 'all' && boardingId !== 'all') {
        db.sequelize.query('select h.*, rp.Plan_Name, u.Display_Name, p.Description, b.Merchant_Name from Audit_Recurring_Plan_Headers h join Recurring_Plans rp on h.Recurring_Plan_ID = rp.id join Users u on h.Boarding_ID = u.Boarding_ID join Boardings b on h.Boarding_ID = b.id join Payment_Plans pp on h.Processor_ID = pp.Processor_ID join Products p on p.id = pp.Product_id where h.Boarding_id = :boardingId AND p.id = :productId AND h.CreatedAt >= :startDate AND h.CreatedAt <= :endDate',null,{ raw: true },
            { boardingId: boardingId, productId: productId, startDate: startDate, endDate: endDate }
        ).success(function(audit_recurring_plan_header){
            if (!audit_recurring_plan_header) {
                return res.jsonp({NoResults:true });
            }else{
                return res.jsonp({Response: audit_recurring_plan_header});
            }
        }).error(function(err){
            return res.render('error', {
                error: err,
                status: 500
            });
        });
    }
};

exports.findOne = function(req, res) {
    var Header_ID = req.params.Header_ID;
    db.sequelize.query('select h.*, rp.Plan_Name, u.Display_Name, p.Description, b.Merchant_Name from Audit_Recurring_Plan_Headers h join Recurring_Plans rp on h.Recurring_Plan_ID = rp.id join Users u on h.Boarding_ID = u.Boarding_ID join Boardings b on h.Boarding_ID = b.id join Payment_Plans pp on h.Processor_ID = pp.Processor_ID join Products p on p.id = pp.Product_id where h.id = :plan_id;',null,{ raw: true },
            { plan_id: Header_ID }
        ).success(function(recurring_plan, err) {
        if(!recurring_plan) {
            return res.jsonp({NoResults:true });
        } else {
            return res.jsonp({Response: recurring_plan});
        }
    }).error(function(err){
        return err;
    });
};

