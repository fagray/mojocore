'use strict';

/**
 * Module dependencies.
 */
var errorHandler = require('./errors'),
	http = require('http'),
	https = require('https'),
	request = require('request'),
	qs = require('querystring'),
	_ = require('lodash'),
	fs = require('fs'),
	queryString = '?',
	express = require('express'),
	app = express(),
	Q =  require('q'),
	db = require('../../config/sequelize'),
	config = require('./../../config/config'),
	log4js = require('log4js'),
	crypto = require('crypto'),
	request = require('request');

/**
 * Log4js need be configurated
 * Remember include -> config = require('./../../config/config')
 */
//log4js.configure(config.logging);
//var Log = log4js.getLogger('transactions'); // "transactions" represents category of log or type of log
var credentialsU='',
	credentialsP='',
	credentialPro='',
    boardingGlobal='',
	generatedHash ='',
	tempHash = '',
	tempHash1 = '',
	iterations = 1000, //2500;
	keylen = 12, //512;
	theObject,
	customer_vault = 0,
    send_mojopay_receipt = false,
    mojo_receipt_merchant_name = '',
    mojo_receipt_customer_name = '',
    mojo_reciept_billing_address = '',
    mojo_reciept_shipping_address = '',
    mojo_receipt_transaction_id = '',
    mojo_receipt_card_type = '',
    mojo_receipt_amount = '',
	toObject = function (arr) {
	  var rv = {};
	  for (var i = 0; i < arr.length; ++i)
	    rv[i] = arr[i];
	  return rv;
	},
	XMLMapping = require('xml-mapping'),
	queryStringToJSON = function(pairs) {		
		pairs = pairs.split('&');
		var result = {};
		pairs.forEach(function(pair) {
			pair = pair.split('=');
			result[pair[0]] = decodeURIComponent(pair[1] || '');
		});

		return JSON.parse(JSON.stringify(result));
	},
	getCredentials = function(req,res){
		var deferred = Q.defer();
        boardingGlobal = '';
			credentialPro= req.param('Processor_ID');
			if ((req.param('type') === 'create')||(req.param('customerVault') === 'add_customer')){
				var passphrase = req.param('creditCardNumber')+req.param('firstNameCard')+req.param('lastNameCard'),
					salt = new Buffer(crypto.randomBytes(keylen)).toString('hex');
					generatedHash = crypto.pbkdf2Sync(passphrase, salt, iterations, keylen).toString('hex');
			}
			db.Payment_Plan.find({where:{Processor_ID:credentialPro}}).success(function(payment_plan){
				if (payment_plan){
                    boardingGlobal = payment_plan.Boarding_ID;
					generatedHash += boardingGlobal;
					credentialsU = payment_plan.Username; 
					credentialsP = payment_plan.Password;
					deferred.resolve(true);
				}else{
					credentialsU = ''; 
					deferred.resolve(true);
					return	res.jsonp({Response:'Processor not Found'});
				}
			});
    	return deferred.promise;
	},
	doMask = function(cc){
		var first6 = cc.substring(0, 6);
		var last4 = cc.substring(cc.length - 4);
		var mask = '';
		for (var i = 0; i < cc.length-10; i++) {
		    mask+='*';
		}
		return first6+mask+last4;
	},
    getBrand = function(cc){
    var cc_number = cc;
        if ((cc_number.substr(1, 1)==='x') || (cc_number.substr(1, 1)==='*')){
            return 'Unknown';
        }else{
            // visa
            var re = new RegExp('^4');
            if (cc_number.match(re) !== null)
                return 'Visa';

            // Mastercard
            re = new RegExp('^5[1-5]');
            if (cc_number.match(re) !== null)
                return 'Mastercard';

            // AMEX
            re = new RegExp('^3[47]');
            if (cc_number.match(re) !== null)
                return 'AMEX';

            // Discover
            re = new RegExp('^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)');
            if (cc_number.match(re) !== null)
                return 'Discover';

            // Diners
            re = new RegExp('^36');
            if (cc_number.match(re) !== null)
                return 'Diners';

            // Diners - Carte Blanche
            re = new RegExp('^30[0-5]');
            if (cc_number.match(re) !== null)
                return 'Diners - Carte Blanche';

            // JCB
            re = new RegExp('^35(2[89]|[3-8][0-9])');
            if (cc_number.match(re) !== null)
                return 'JCB';

            // Visa Electron
            re = new RegExp('^(4026|417500|4508|4844|491(3|7))');
            if (cc_number.match(re) !== null)
                return 'Visa Electron';
        }
    },
	runTheAPI = function(req,res,other){
		var isAPI = true,
			deferred = Q.defer(),
			transactionType = req.param('type'),
			arg = {},
	 		url = '',
	 		xmlfile=false;
        var thisAmount = 0,
            thisTax = 0,
            thisShipping = 0;

	 	if ((other!==null)&&(other!=='')){
	 		transactionType =other;
	 	}
		switch(transactionType) {
		    case 'sale':   
		    	xmlfile=false; 
			 	url = config.api_url.root_transact; 
				arg = {
						type: req.param('type'),
						amount: req.param('amount'),
						currency: req.param('currency'),
						cvv: req.param('cardSecurityCode'),
						orderid: req.param('orderId'),
						orderdescription: req.param('orderDescription'),
						ponumber: req.param('poNumber'),
						shipping: req.param('shipping'),
						tax: req.param('tax'),
						firstname: req.param('firstNameCard'),
						lastname: req.param('lastNameCard'),
						company: req.param('companyCard'),
						country: req.param('countryCard'),
						address1: req.param('addressCard'),
						address2: req.param('addressContCard'),
						city: req.param('cityCard'),
						state: req.param('stateProvinceCard'),
						zip: req.param('zipPostalCodeCard'),
						phone: req.param('phoneNumberCard'),
						fax: req.param('faxNumberCard'),
						email: req.param('emailAddressCard'),
						shipping_firstname: req.param('firstNameShipping'),
						shipping_lastname: req.param('lastNameShipping'),
						shipping_company: req.param('companyShipping'),
						shipping_address1: req.param('addressShipping'),
						shipping_address2: req.param('addressContShipping'),
						shipping_city: req.param('cityShipping'),
						shipping_state: req.param('stateProvinceShipping'),
						shipping_zip: req.param('zipPostalCodeShipping'),
						shipping_country: req.param('countryShipping'),
						shipping_email: req.param('emailAddressShipping'),
						customer_vault:req.param('customerVault'),
						customer_vault_id: generatedHash,
						//customer_receipt:req.param('customerReceipt'),
						merchant_defined_field_1 : req.param('merchantHash'),
						merchant_defined_field_2 : req.param('referralHash'),
						merchant_defined_field_3 : req.param('referralProcessorId'),
						processor_id:credentialPro
					};
                    send_mojopay_receipt = req.param('customerReceipt');
                    mojo_receipt_customer_name = req.param('firstNameCard') + ' ' + req.param('lastNameCard');
                    mojo_reciept_billing_address = mojo_receipt_customer_name + '<br>' + 
                                                   req.param('addressCard') + '<br>' +
						                           req.param('addressContCard') + '<br>' +
                                                   req.param('cityCard') + ',' +
						                           req.param('stateProvinceCard') + ' ' +
						                           req.param('zipPostalCodeCard') + '<br>' +
                                                   req.param('countryCard') + '<br>' +
						                           req.param('phoneNumberCard');
                    mojo_reciept_shipping_address = req.param('addressShipping') + '<br>' +
						                            req.param('addressContShipping') + '<br>' +
                                                    req.param('cityShipping') + ',' +
						                            req.param('stateProvinceShipping') + ' ' +
						                            req.param('zipPostalCodeShipping') + '<br>' +
                                                    req.param('countryShipping');
                    mojo_receipt_card_type = req.param('creditCardNumber');
                    thisAmount = 0;
                    thisTax = 0;
                    thisShipping = 0;
                    if(req.param('amount') !== undefined && req.param('amount') !== null && req.param('amount') !== '') {
                        thisAmount = parseFloat(req.param('amount'));
                    }
                    if(req.param('tax') !== undefined && req.param('tax') !== null && req.param('tax') !== '') {
                        thisTax = parseFloat(req.param('tax'));
                    }
                    if(req.param('shipping') !== undefined && req.param('shipping') !== null && req.param('shipping') !== '') {
                        thisShipping = parseFloat(req.param('shipping'));
                    }
                    mojo_receipt_amount =  thisAmount + thisTax + thisShipping;
	
					break;
			case 'salevault': 
		    	xmlfile=false; 
			 	url = config.api_url.root_transact; 
				arg = {
						type: 'validate',
						amount: '0.00',
						cvv: req.param('cardSecurityCode'),
						orderid: req.param('orderId'),
						orderdescription: req.param('orderDescription'),
						ponumber: req.param('poNumber'),
						shipping: req.param('shipping'),
						tax: req.param('tax'),
						customer_vault_id:theObject.customer_vault_id,
						//customer_receipt:req.param('customerReceipt'),
						processor_id:credentialPro
					};
                    send_mojopay_receipt = req.param('customerReceipt');
                    mojo_receipt_customer_name = req.param('firstNameCard') + ' ' + req.param('lastNameCard');
                    mojo_reciept_billing_address = mojo_receipt_customer_name + '<br>' + 
                                                   req.param('addressCard') + '<br>' +
                                                   req.param('addressContCard') + '<br>' +
                                                   req.param('cityCard') + ',' +
                                                   req.param('stateProvinceCard') + ' ' +
                                                   req.param('zipPostalCodeCard') + '<br>' +
                                                   req.param('countryCard') + '<br>' +
                                                   req.param('phoneNumberCard');
                    mojo_reciept_shipping_address = req.param('addressShipping') + '<br>' +
                                                    req.param('addressContShipping') + '<br>' +
                                                    req.param('cityShipping') + ',' +
                                                    req.param('stateProvinceShipping') + ' ' +
                                                    req.param('zipPostalCodeShipping') + '<br>' +
                                                    req.param('countryShipping');
                    mojo_receipt_card_type = req.param('creditCardNumber');
                    thisAmount = 0;
                    thisTax = 0;
                    thisShipping = 0;
                    if(req.param('amount') !== undefined && req.param('amount') !== null && req.param('amount') !== '') {
                        thisAmount = parseFloat(req.param('amount'));
                    }
                    if(req.param('tax') !== undefined && req.param('tax') !== null && req.param('tax') !== '') {
                        thisTax = parseFloat(req.param('tax'));
                    }
                    if(req.param('shipping') !== undefined && req.param('shipping') !== null && req.param('shipping') !== '') {
                        thisShipping = parseFloat(req.param('shipping'));
                    }
                    mojo_receipt_amount =  thisAmount + thisTax + thisShipping;
                    
					break;
		 	case 'create':  
		 		xmlfile=false; 
		    	url = config.api_url.root_transact; 
				arg = {
					customer_vault:'add_customer',
					customer_vault_id : req.param('customerVaultId'),
					billing_id : req.param('billingId'),
					ccnumber: req.param('creditCardNumber'),
					ccexp: req.param('expirationDate'),
					currency: req.param('currency'),
					first_name  : req.param('firstNameCard'),
					last_name : req.param('lastNameCard'),
					company : req.param('companyCard'),
					country : req.param('countryCard'),
					address1 : req.param('addressCard'),
					address2 : req.param('addressContCard'),
					city : req.param('cityCard'),
					state : req.param('stateProvinceCard'),
					zip: req.param('zipPostalCodeCard'),
					phone : req.param('phoneNumberCard'),
					fax : req.param('faxNumberCard'),
					email : req.param('emailAddressCard'),
					shipping_id : req.param('shippingId'),
					shipping_firstname  : req.param('firstNameShipping'),
					shipping_lastname : req.param('lastNameShipping'),
					shipping_company : req.param('companyShipping'),
					shipping_country : req.param('countryShipping'),
					shipping_address1 : req.param('addressShipping'),
					shipping_address2 : req.param('addressContShipping'),
					shipping_city : req.param('cityShipping'),
					shipping_state : req.param('stateProvinceShipping'),
					shipping_zip: req.param('zipPostalCodeShipping'),
					shipping_phone : req.param('phoneNumberShipping'),
					shipping_fax : req.param('faxNumberShipping'),
					shipping_email : req.param('emailAddressShipping'),
					merchant_defined_field_1 : generatedHash
				};
		        break;	
		    case 'creport':
		   		xmlfile=true;
		  		url = config.api_url.root_query; 
				arg = {
					report_type: 'customer_vault',
					merchant_defined_field_1: req.param('customerHash'),
					//customer_vault_id: req.param('customerVaultId')
				};
				if(tempHash === ''){
					arg.customer_vault_id = req.param('customerVaultId');
				}else{
					arg.customer_vault_id = tempHash;
					tempHash = '';
				}
		        break;
            case 'create_customer_card_hash':
                isAPI = false;
                var customer_card_hash = {
                    transaction_id: req.param('transaction_id')
                };
                if(tempHash === ''){
                    customer_card_hash.CC_Number = doMask(req.param('cc_number'));
                    customer_card_hash.Brand = getBrand(req.param('cc_number'));
                    customer_card_hash.Hash = req.param('hash');
                    customer_card_hash.Boarding_ID = boardingGlobal;
                }else{
                    customer_card_hash.CC_Number = doMask(tempHash1);
                    customer_card_hash.Brand = getBrand(req.param('cc_number'));
                    customer_card_hash.Hash = tempHash;
                    customer_card_hash.Boarding_ID = boardingGlobal;
                    tempHash = '';
                }
                db.Customer_Card_Hash.find({where:{Hash:customer_card_hash.hash,Boarding_ID:boardingGlobal}}).success(function(cc){
                    if (!cc){
                        db.Customer_Card_Hash.create(customer_card_hash).success(function(customer_card_hash,err){
                            if(!customer_card_hash){
                                //res.send('users/signup', {errors: err});
                                if (tempHash === ''){
                                    console.log(''); // avoiding warnings
                                }else{
                                    deferred.resolve(true);
                                }
                            } else {
                                //res.jsonp({Response:'customer_card_hash done!'});
                                if (tempHash ===''){
                                    console.log(''); // avoiding warnings
                                }else{
                                    deferred.resolve(true);
                                }
                            }
                        }).error(function(err){
                            console.log(err);
                        });
                    }
                });
                break;
	        case 'void':
	    	    xmlfile=false; 
		    	url = config.api_url.root_transact;	 				        
				arg = {
					type: req.param('type'),
					merchant_defined_field_1 : req.param('merchantHash'),
					merchant_defined_field_2 : req.param('referralHash'),
					merchant_defined_field_3 : req.param('referralProcessorId'),
					transactionid: req.param('transactionId')
				};
				if (req.param('customerVaultId') !== undefined){
					arg.customer_vault_id = req.param('customerVaultId');
				}
		        break;
		    case 'refund':
		    	xmlfile=false; 
	 			url = config.api_url.root_transact;		        
				arg = {
					type: req.param('type'),
					amount: req.param('amount'),
					merchant_defined_field_1 : req.param('merchantHash'),
					merchant_defined_field_2 : req.param('referralHash'),
					merchant_defined_field_3 : req.param('referralProcessorId'),
					transactionid: req.param('transactionId')
				};
				if (req.param('customerVaultId') !== undefined){
					arg.customer_vault_id = req.param('customerVaultId');
				}
		        break;
		}
		for (var attribute in req.query){
			if (attribute.indexOf('item') !== -1){
				arg[attribute] = req.query[attribute];
			}
		}
		/**
		 * Call resources API
		 */
		if (isAPI){
			arg.username = credentialsU; 
			arg.password = credentialsP;
			var items = Object.keys(arg);
			queryString ='?';
			items.forEach(function(item) {
			  if ((arg[item]!=='')&&(arg[item]!==undefined)){
			  	 queryString+='&' + item + '=' + arg[item];
			  }
			});
			url+=queryString;
			request({url:url,async:true}, function (error, response, body) {
			    if (!error && response.statusCode === 200) {
			    		if (xmlfile){
				    		var json = XMLMapping.load(body);
				    		theObject = json;
				    		deferred.resolve(true);
				    	}else{
				    		if ( transactionType === 'create'){
			    				var database={};
			    				theObject = queryStringToJSON(body);
			    				database.hash = generatedHash;
			    				database.nmi = theObject.customer_vault_id;
		    				    db.Vault.create(database).success(function(vault,err){
									runTheAPI(req,res,'salevault').
									then(
									function(results){
										tempHash = theObject.customer_vault_id;
					    				delete theObject.customer_vault_id;
					    				theObject.customer_hash = generatedHash;
					    				database = '';
					    				deferred.resolve(true);
									});
	    				        }).error(function(err){
							    });
			    			}else if( transactionType === 'salevault'){
				    			deferred.resolve(true);
				    		}else{
                                theObject = queryStringToJSON(body);
                                if (theObject.customer_vault_id){
                                    delete theObject.customer_vault_id;
                                }
                                deferred.resolve(theObject);
                            }
				    	}
			     }else{ 
			     	console.log(error);
			     }
			});
		}
		return deferred.promise;
	};

exports.transactions = function(req, res) {
	getCredentials(req,res).then(
		function(results){
				if (credentialsU!==''){
					if (req.param('type') === 'sale'){
						var customerhashdb = req.param('customerHash');
						db.Vault.find({where:{hash:customerhashdb}}).success(function(payment_plan){
								if (payment_plan){
									generatedHash = payment_plan.nmi;							
									runTheAPI(req,res,'sale').
									then(
									function(results){
										res.jsonp({Response:results});
									});
								}
							});
					}else if (req.param('type') === 'create'){
						tempHash1 = req.param('creditCardNumber');
						runTheAPI(req,res,'').then(
							function(results){
								res.jsonp({Response:theObject});
								runTheAPI(req,res,'creport').then(
									function(results){
										tempHash = theObject.nm_response.customer_vault.customer.cc_hash.$t;
										runTheAPI(req,res,'create_customer_card_hash').then(
												function(results){
													
												});
									});

							});
					}else{
						runTheAPI(req,res,'').then(
							function(results){
								res.jsonp({Response:theObject});
							});
					}
				}else{
					res.jsonp({Response:'Api Missing Data'});
				}
		},
		function(err){
		});
};
