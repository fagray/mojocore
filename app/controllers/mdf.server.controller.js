'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    config = require('./../../config/config');
/**
 * Find mdf by id
 * Note: This is called every time that the parameter :masId is used in a URL. 
 * Its purpose is to preload the mas on the req object then call the next function. 
 */
exports.mdf = function(req, res, next, id) {
    //var transactionid = req.params.mastransactionid,
    var historyId = req.params.historyId;
    db.MDF.findAll({ where: {History_ID: historyId}}).success(function(mdf){
        if(!mdf) {
            req.mdf = {clean:'clean'};
            return next();   
        } else {
            req.mdf = {response:mdf};
            return next();            
        }
    }).error(function(err){
        return next(err);
    });
};

/**
 * Show an mas
 */
exports.show = function(req, res) {
    // Sending down the mas that was just preloaded by the mass.mas function
    // and saves mas on the req object.
    return res.jsonp(req.mdf);
};
