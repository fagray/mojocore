'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    config = require('./../../config/config'),
    log4js = require('log4js');

/**
 * Log4js need be configurated
 * Remember include -> config = require('./../../config/config')
 */
log4js.configure(config.logging);
var Log = log4js.getLogger('boardings'); 
var theboarding=[];// "boardings" represents category of log or type of log
/**
 * Find boarding by id
 * Note: This is called every time that the parameter :boardingId is used in a URL. 
 * Its purpose is to preload the boarding on the req object then call the next function. 
 */
exports.boarding = function(req, res, next, id) {
    db.Boarding.find({ where: {id: id}}).success(function(boarding){
        if(!boarding) {
            return next(new Error('Failed to load boarding ' + id));
        } else {
            req.boarding = boarding;
            return next();            
        }
    }).error(function(err){
        return next(err);
    });
};

/**
 * Create a boarding
 */
exports.create = function(req, res) {
    // augment the boarding by adding the UserId
    req.body.UserId = req.user.id;
    // save and return and instance of boarding on the res object. 
    db.Boarding.create(req.body).success(function(boarding,err){
        if(!boarding){
            return res.send('users/signup', {errors: err});
        } else {
            /**
             * Tracking
             */
            var text = '[Creating Boarding] [id = ' + boarding.dataValues.id + '] [Merchant_Name = ' + boarding.dataValues.Merchant_Name + ']';
            Log.debug(text);
            return res.jsonp(boarding);
        }
    }).error(function(err){
        return res.send('users/signup', { 
            errors: err,
            status: 500
        });
    });
};

/**
 * Update a boarding
 */
exports.update = function(req, res) {
    db.Boarding.find({ where: {id: req.body.id}}).success(function(boardingCollection){
        if(boardingCollection) {
            /**
             *Logs Boarding
             */
            var tabulator = '\t',
                newLine = '\r\n',
                txt = '' + newLine;            
    
            txt += newLine + 'Admin: ' + req.user.dataValues.First_Name + ' ' + req.user.dataValues.Last_Name + newLine;
            txt += 'Merchant: ' + req.body.Merchant_ID;
            if (req.body.Processor_ID!=='') txt += ':' + req.body.Processor_ID;
            txt += newLine;
            txt += req.body.Merchant_Name + newLine;

            if (req.body.Merchant_Name !== boardingCollection.dataValues.Merchant_Name)
                txt += tabulator + 'Merchant_Name: ' + boardingCollection.dataValues.Merchant_Name + ' => ' + req.body.Merchant_Name + newLine;

            if (req.body.Address !== boardingCollection.dataValues.Address)
                txt += tabulator + 'Address: ' + boardingCollection.dataValues.Address + ' => ' + req.body.Address + newLine;

            if (req.body.City !== boardingCollection.dataValues.City)
                txt += tabulator + 'City: ' + boardingCollection.dataValues.City + ' => ' + req.body.City + newLine;

            if (req.body.Phone !== boardingCollection.dataValues.Phone)
                txt += tabulator + 'Phone: ' + boardingCollection.dataValues.Phone + ' => ' + req.body.Phone + newLine;

            if (req.body.Merchant_ID !== boardingCollection.dataValues.Merchant_ID)
                txt += tabulator + 'Merchant_ID: ' + boardingCollection.dataValues.Merchant_ID + ' => ' + req.body.Merchant_ID + newLine;

            if (req.body.Bank_ID !== boardingCollection.dataValues.Bank_ID)
                txt += tabulator + 'Bank_ID: ' + boardingCollection.dataValues.Bank_ID + ' => ' + req.body.Bank_ID + newLine;

            if (req.body.Terminal_ID !== boardingCollection.dataValues.Terminal_ID)
                txt += tabulator + 'Terminal_ID: ' + boardingCollection.dataValues.Terminal_ID + ' => ' + req.body.Terminal_ID + newLine;

            if (req.body.MCC !== boardingCollection.dataValues.MCC)
                txt += tabulator + 'MCC: ' + boardingCollection.dataValues.MCC + ' => ' + req.body.MCC + newLine;

//            if (req.body.category !== boardingCollection.dataValues.category)
//                txt += tabulator + 'category: ' + boardingCollection.dataValues.category + ' => ' + req.body.category + newLine;

//            if (req.body.merchantCategoryCode !== boardingCollection.dataValues.merchantCategoryCode)
//                txt += tabulator + 'merchantCategoryCode: ' + boardingCollection.dataValues.merchantCategoryCode + ' => ' + req.body.merchantCategoryCode + newLine;

            if (req.body.Classification !== boardingCollection.dataValues.Classification)
                txt += tabulator + 'Classification: ' + boardingCollection.dataValues.Classification + ' => ' + req.body.Classification + newLine;

            if (req.body.Visa !== boardingCollection.dataValues.Visa)
                txt += tabulator + 'Visa: ' + boardingCollection.dataValues.Visa + ' => ' + req.body.Visa + newLine;

            if (req.body.Mastercard !== boardingCollection.dataValues.Mastercard)
                txt += tabulator + 'Mastercard: ' + boardingCollection.dataValues.Mastercard + ' => ' + req.body.Mastercard + newLine;

            if (req.body.Discover !== boardingCollection.dataValues.Discover)
                txt += tabulator + 'Discover: ' + boardingCollection.dataValues.Discover + ' => ' + req.body.Discover + newLine;

            if (req.body.American_Express !== boardingCollection.dataValues.American_Express)
                txt += tabulator + 'American_Express: ' + boardingCollection.dataValues.American_Express + ' => ' + req.body.American_Express + newLine;

            if (req.body.Diners_Club !== boardingCollection.dataValues.Diners_Club)
                txt += tabulator + 'Diners_Club: ' + boardingCollection.dataValues.Diners_Club + ' => ' + req.body.Diners_Club + newLine;

            if (req.body.JCB !== boardingCollection.dataValues.JCB)
                txt += tabulator + 'JCB: ' + boardingCollection.dataValues.JCB + ' => ' + req.body.JCB + newLine;

            if (req.body.Maestro !== boardingCollection.dataValues.Maestro)
                txt += tabulator + 'Maestro: ' + boardingCollection.dataValues.Maestro + ' => ' + req.body.Maestro + newLine;

            if (req.body.Max_Ticket_Amount !== boardingCollection.dataValues.Max_Ticket_Amount)
                txt += tabulator + 'Max_Ticket_Amount: ' + boardingCollection.dataValues.Max_Ticket_Amount + ' => ' + req.body.Max_Ticket_Amount + newLine;

            if (req.body.Max_Monthly_Amount !== boardingCollection.dataValues.Max_Monthly_Amount)
                txt += tabulator + 'Max_Monthly_Amount: ' + boardingCollection.dataValues.Max_Monthly_Amount + ' => ' + req.body.Max_Monthly_Amount + newLine;

            if (req.body.Precheck !== boardingCollection.dataValues.Precheck)
                txt += tabulator + 'Precheck: ' + boardingCollection.dataValues.Precheck + ' => ' + req.body.Precheck + newLine;

            if (req.body.Duplicate_Checking !== boardingCollection.dataValues.Duplicate_Checking)
                txt += tabulator + 'Duplicate_Checking: ' + boardingCollection.dataValues.Duplicate_Checking + ' => ' + req.body.Duplicate_Checking + newLine;

            if (req.body.Allow_Merchant !== boardingCollection.dataValues.Allow_Merchant)
                txt += tabulator + 'Allow_Merchant: ' + boardingCollection.dataValues.Allow_Merchant + ' => ' + req.body.Allow_Merchant + newLine;

            if (req.body.Duplicate_Threshold !== boardingCollection.dataValues.Duplicate_Threshold)
                txt += tabulator + 'Duplicate_Threshold: ' + boardingCollection.dataValues.Duplicate_Threshold + ' => ' + req.body.Duplicate_Threshold + newLine;

            if (req.body.Account_Description !== boardingCollection.dataValues.Account_Description)
                txt += tabulator + 'Account_Description: ' + boardingCollection.dataValues.Account_Description + ' => ' + req.body.Account_Description + newLine;

//            if (req.body.Processor_ID!== boardingCollection.dataValues.Processor_ID)
//                txt += tabulator + 'Processor_ID: ' + boardingCollection.dataValues.Processor_ID + ' => ' + req.body.Processor_ID + newLine;

            if (req.body.Location_Number !== boardingCollection.dataValues.Location_Number)
                txt += tabulator + 'Location_Number: ' + boardingCollection.dataValues.Location_Number + ' => ' + req.body.Location_Number + newLine;

            if (req.body.Aquire_Bin !== boardingCollection.dataValues.Aquire_Bin)
                txt += tabulator + 'Aquire_Bin: ' + boardingCollection.dataValues.Aquire_Bin + ' => ' + req.body.Aquire_Bin + newLine;

            if (req.body.Store_Number !== boardingCollection.dataValues.Store_Number)
                txt += tabulator + 'Store_Number: ' + boardingCollection.dataValues.Store_Number + ' => ' + req.body.Store_Number + newLine;

            if (req.body.Vital_Number !== boardingCollection.dataValues.Vital_Number)
                txt += tabulator + 'Vital_Number: ' + boardingCollection.dataValues.Vital_Number + ' => ' + req.body.Vital_Number + newLine;

            if (req.body.Agent_Chain !== boardingCollection.dataValues.Agent_Chain)
                txt += tabulator + 'Agent_Chain: ' + boardingCollection.dataValues.Agent_Chain + ' => ' + req.body.Agent_Chain + newLine;

            if (req.body.Required_Name !== boardingCollection.dataValues.Required_Name)
                txt += tabulator + 'Required_Name: ' + boardingCollection.dataValues.Required_Name + ' => ' + req.body.Required_Name + newLine;

            if (req.body.Required_Company !== boardingCollection.dataValues.Required_Company)
                txt += tabulator + 'Required_Company: ' + boardingCollection.dataValues.Required_Company + ' => ' + req.body.Required_Company + newLine;

            if (req.body.Required_Address !== boardingCollection.dataValues.Required_Address)
                txt += tabulator + 'Required_Address: ' + boardingCollection.dataValues.Required_Address + ' => ' + req.body.Required_Address + newLine;

            if (req.body.Required_City !== boardingCollection.dataValues.Required_City)
                txt += tabulator + 'Required_City: ' + boardingCollection.dataValues.Required_City + ' => ' + req.body.Required_City + newLine;

            if (req.body.Required_State !== boardingCollection.dataValues.Required_State)
                txt += tabulator + 'Required_State: ' + boardingCollection.dataValues.Required_State + ' => ' + req.body.Required_State + newLine;

            if (req.body.Required_Zip !== boardingCollection.dataValues.Required_Zip)
                txt += tabulator + 'Required_Zip: ' + boardingCollection.dataValues.Required_Zip + ' => ' + req.body.Required_Zip + newLine;

            if (req.body.Required_Country !== boardingCollection.dataValues.Required_Country)
                txt += tabulator + 'Required_Country: ' + boardingCollection.dataValues.Required_Country + ' => ' + req.body.Required_Country + newLine;

            if (req.body.Required_Country !== boardingCollection.dataValues.Required_Country)
                txt += tabulator + 'Required_Country: ' + boardingCollection.dataValues.Required_Country + ' => ' + req.body.Required_Country + newLine;

            if (req.body.Required_Email !== boardingCollection.dataValues.Required_Email)
                txt += tabulator + 'Required_Email: ' + boardingCollection.dataValues.Required_Email + ' => ' + req.body.Required_Email + newLine;

            if (req.body.Reserve_Rate !== boardingCollection.dataValues.Reserve_Rate)
                txt += tabulator + 'Reserve_Rate: ' + boardingCollection.dataValues.Reserve_Rate + ' => ' + req.body.Reserve_Rate + newLine;

            if (req.body.Reserve_Condition !== boardingCollection.dataValues.Reserve_Condition)
                txt += tabulator + 'Reserve_Condition: ' + boardingCollection.dataValues.Reserve_Condition + ' => ' + req.body.Reserve_Condition + newLine;

            if (req.body.Cap_Amount !== boardingCollection.dataValues.Cap_Amount)
                txt += tabulator + 'Cap_Amount: ' + boardingCollection.dataValues.Cap_Amount + ' => ' + req.body.Cap_Amount + newLine;

            if (req.body.Disbursement !== boardingCollection.dataValues.Disbursement)
                txt += tabulator + 'Disbursement: ' + boardingCollection.dataValues.Disbursement + ' => ' + req.body.Disbursement + newLine;

            if (req.body.Status !== boardingCollection.dataValues.Status)
                txt += tabulator + 'Status: ' + boardingCollection.dataValues.Status + ' => ' + req.body.Status + newLine;

            if (req.body.Welcome_Email !== boardingCollection.dataValues.Welcome_Email)
                txt += tabulator + 'Welcome_Email: ' + boardingCollection.dataValues.Welcome_Email + ' => ' + req.body.Welcome_Email + newLine;

            if (req.body.State !== boardingCollection.dataValues.State)
                txt += tabulator + 'State: ' + boardingCollection.dataValues.State + ' => ' + req.body.State + newLine;

            if (req.body.Zip !== boardingCollection.dataValues.Zip)
                txt += tabulator + 'Zip: ' + boardingCollection.dataValues.Zip + ' => ' + req.body.Zip + newLine;

            txt += '(Admin ID: ' + req.user.id + ', Merchant ID: ' + req.body.Merchant_ID + ', Boarding ID: ' + boardingCollection.dataValues.id + ')';

            // create a new variable to hold the boarding that was placed on the req object.

            var boarding = req.boarding;
            /**
             * Tracking
             */
            var text = '[Updating Boarding] [id = ' + req.body.id + '] [Merchant_Name = ' + req.body.Merchant_Name + ']';  
             boarding.updateAttributes({
                Date: req.body.Date,
                Merchant_Name: req.body.Merchant_Name,
                Address: req.body.Address,
                City: req.body.City,
                Phone: req.body.Phone,
                Merchant_ID: req.body.Merchant_ID,
                Bank_ID: req.body.Bank_ID,
                Terminal_ID: req.body.Terminal_ID,
                MCC: req.body.MCC,
                Classification: req.body.Classification,
                Visa: req.body.Visa,
                Mastercard: req.body.Mastercard,
                Discover: req.body.Discover,
                American_Express: req.body.American_Express,
                Diners_Club: req.body.Diners_Club,
                JCB: req.body.JCB,
                Maestro: req.body.Maestro,
                Max_Ticket_Amount: req.body.Max_Ticket_Amount,
                Max_Monthly_Amount: req.body.Max_Monthly_Amount,
                Precheck: req.body.Precheck,
                Duplicate_Checking: req.body.Duplicate_Checking,
                Allow_Merchant: req.body.Allow_Merchant,
                Duplicate_Threshold: req.body.Duplicate_Threshold,
                Account_Description: req.body.Account_Description,
                Location_Number: req.body.Location_Number,
                Aquire_Bin: req.body.Aquire_Bin,
                Store_Number: req.body.Store_Number,
                Vital_Number: req.body.Vital_Number,
                Agent_Chain: req.body.Agent_Chain,
                Required_Name: req.body.Required_Name,
                Required_Company: req.body.Required_Company,
                Required_Address: req.body.Required_Address,
                Required_City: req.body.Required_City,
                Required_State: req.body.Required_State,
                Required_Zip: req.body.Required_Zip,
                Required_Country: req.body.Required_Country,
                Required_Phone: req.body.Required_Phone,
                Required_Email: req.body.Required_Email,
                Reserve_Rate: req.body.Reserve_Rate,
                Reserve_Condition: req.body.Reserve_Condition,
                Cap_Amount: req.body.Cap_Amount,
                Disbursement: req.body.Disbursement,
                Status: req.body.Status,
                Welcome_Email: req.body.Welcome_Email,
                State: req.body.State,
                Zip: req.body.Zip,
                Last_Changed_By: req.body.Last_Changed_By
            }).success(function(a){
                Log.debug(text);
                return res.jsonp(a);
            }).error(function(err){
                return res.render('error', {
                    error: err, 
                    status: 500
                });
            });
        }
    }).error(function(err){
        console.error(err);
    });

};

/**
 * Delete an boarding
 */
exports.destroy = function(req, res) {

    // create a new variable to hold the boarding that was placed on the req object.
    var boarding = req.boarding;

    boarding.destroy().success(function(){
        return res.jsonp(boarding);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

/**
 * Show an boarding
 */
exports.show = function(req, res) {
    // Sending down the boarding that was just preloaded by the boardings.boarding function
    // and saves boarding on the req object.
    return res.jsonp(req.boarding);
};

/**
 * List of boardings
 */
exports.all = function(req, res) {
        theboarding=[];
        var countervariable=0;
    db.Boarding.findAll({ where: {Status: {ne: 'deleted'}}, order: [['Merchant_Name', 'ASC']]}).success(function(boardings){
        boardings.forEach(function(board){
            db.Payment_Plan.findAll({where:{Boarding_ID:board.id}}).success(function(payment_plan){
                if(payment_plan){
                    board.dataValues.payment_plans = payment_plan;
                    theboarding.push(board);
                }else{
                    board.dataValues.payment_plans = '';
                    theboarding.push(board);
                }
                countervariable++;
                if (Object.keys(boardings).length === countervariable){
                     return res.jsonp(theboarding);
                }
            });
        });
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};
