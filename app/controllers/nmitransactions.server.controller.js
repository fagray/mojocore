'use strict';

/**
 *
 * Express application controllers, contain the backend business logic to calls to different resources API.
 *
 * @type {exports}
 */


/**
 * Module dependencies.
 */
var errorHandler = require('./errors'),
    http = require('http'),
    https = require('https'),
    request = require('request'),
    qs = require('querystring'),
    _ = require('lodash'),
    fs = require('fs'),
    queryString = '?',
    express = require('express'),
    app = express(),
    Q =  require('q'),
    db = require('../../config/sequelize'),
    config = require('./../../config/config'),
    log4js = require('log4js'),
    request = require('request'),
    merchants = [],
    objectProductOne = [],
    objectProductTwo = [],
    objectProduct = [],
    boardingsCollection = [],
	nodemailer = require('nodemailer');

/**
 * Log4js need be configurated
 * Remember include -> config = require('./../../config/config')
 */
log4js.configure(config.logging);
var Log = log4js.getLogger('transactions'); // "transactions" represents category of log or type of log

var credentialsU='',
    credentialsP='',
    credentialPro='',
    boardingGlobal = '',
    GlobalBoardingId='',
    allCredentials='',
    
    emailParameters = {
        send_mojopay_receipt: false,
        mojo_receipt_email_address: '',
        mojo_receipt_merchant_name: '',
        mojo_receipt_customer_name: '',
        mojo_reciept_billing_address: '',
        mojo_reciept_shipping_address: '',
        mojo_receipt_transaction_id: '',
        mojo_receipt_card_type: '',
        mojo_receipt_amount: ''
    },
    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    },
    convArrToObj = function(array){ // converts an array to json object
        var thisEleObj = {};
        if(typeof array === 'object'){
            for(var i in array){
                var thisEle = convArrToObj(array[i]);
                thisEleObj[i] = thisEle;
            }
        }else {
            thisEleObj = array;
        }
        return thisEleObj;
    },
    queryStringToJSON = function(pairs) { // converts a querystring to json object
        pairs = pairs.split('&');
        var result = {};
        pairs.forEach(function(pair) {
            pair = pair.split('=');
            result[pair[0]] = decodeURIComponent(pair[1] || '');
        });

        return JSON.parse(JSON.stringify(result));
    };


    var XMLMapping = require('xml-mapping'),


    /**
     * Gets the proper credentials acording to the UNIQUE processorID
     * @param processorId
     * @param role
     * @returns {*}
     */
    getCredentials = function(processorId,boardingId,role){
        var deferred = Q.defer();
        credentialsU='';
        credentialsP='';
        credentialPro='';
        emailParameters = {
            send_mojopay_receipt: false,
            mojo_receipt_email_address: '',
            mojo_receipt_merchant_name: '',
            mojo_receipt_customer_name: '',
            mojo_reciept_billing_address: '',
            mojo_reciept_shipping_address: '',
            mojo_receipt_transaction_id: '',
            mojo_receipt_card_type: '',
            mojo_receipt_amount: ''
        };
        if (processorId!==undefined && processorId!==''){
            credentialPro = processorId;
            db.Payment_Plan.find({ where:{Boarding_ID: boardingId, Processor_ID:processorId,Is_Active:1}}).success(function(account){
                if(!account) {
                    credentialsU = ''; 
                    credentialsP = '';
                    boardingGlobal = '';
                    deferred.resolve(false);
                } else {
                    credentialsU = account.Username; 
                    credentialsP = account.Password;
                    boardingGlobal = account.Boarding_ID;
                    deferred.resolve(true);
                
                }
            });
        }else{
            credentialPro = '';
            credentialsU = ''; 
            credentialsP = '';
            boardingGlobal = '';
            deferred.resolve(false);
        }
        db.Boarding.find({ where: {id: boardingId}}).success(function(boarding){
            if (boarding) {
                emailParameters.mojo_receipt_merchant_name = boarding.Merchant_Name;   
            }
        });
        return deferred.promise;
    },


    getAllCredentials = function(boardingId,role){
        allCredentials = '';
        var deferred = Q.defer();
        //if (processorId!==undefined){
        //    credentialPro = processorId;
            db.Payment_Plan.findAll({ where:{Boarding_ID: boardingId, Product_ID: {ne: 0}, Is_Active:1}}).success(function(account){
                if(!account) {
                    allCredentials = ''; 
                    deferred.resolve(false);
                } else {
                    allCredentials = account;
                    deferred.resolve(true);
                }
            });
        //}else{
        //    credentialPro = '';
        //    deferred.resolve(false);
        //}
        return deferred.promise;
    },


    /**
     * Parse data into NMI format from DB
     * @param arr
     * @returns {*}
     */
    parseLikeNMI = function(arr,history){

        var deferred = Q.defer();
        var counter = 0;
        var newObjectTransactions = [];

        for (var i = 0; i < Object.keys(arr).length; i++) {
            newObjectTransactions.push({
                transaction_id: {$t:arr[i].transaction_id},
                partial_payment_id: {},
                partial_payment_balance: {},
                platform_id: {$t:arr[i].platform_id},
                transaction_type: {$t:arr[i].transaction_type},
                condition: {$t:arr[i].transaction_condition},
                order_id: {$t:arr[i].order_id},
                authorization_code: {$t:arr[i].authorization_code},
                ponumber: {$t:arr[i].ponumber},
                order_description: {$t:arr[i].order_description},
                first_name: {$t:arr[i].first_name},
                last_name: {$t:arr[i].last_name},
                address_1: {$t:arr[i].address_1},
                address_2: {$t:arr[i].address_2},
                company: {$t:arr[i].company},
                city: {$t:arr[i].city},
                state: {$t:arr[i].state},
                postal_code: {$t:arr[i].postal_code},
                country: {$t:arr[i].country},
                email: {$t:arr[i].email},
                phone: {$t:arr[i].phone},
                fax: {$t:arr[i].fax},
                cell_phone: {$t:arr[i].cell_phone},
                customertaxid: {$t:arr[i].customertaxid},
                website: {$t:arr[i].website},
                shipping_first_name: {$t:arr[i].shipping_first_name},
                shipping_last_name: {$t:arr[i].shipping_last_name},
                shipping_address_1: {$t:arr[i].shipping_address_1},
                shipping_address_2: {$t:arr[i].shipping_address_2},
                shipping_company: {$t:arr[i].shipping_company},
                shipping_city: {$t:arr[i].shipping_city},
                shipping_state: {$t:arr[i].shipping_state},
                shipping_postal_code: {$t:arr[i].shipping_postal_code},
                shipping_country: {$t:arr[i].shipping_country},
                shipping_email: {$t:arr[i].shipping_email},
                shipping_carrier: {$t:arr[i].shipping_carrier},
                tracking_number: {$t:arr[i].tracking_number},
                shipping_date: {$t:arr[i].shipping_date},
                shipping: {$t:arr[i].Shipping},
                shipping_phone: {$t:arr[i].shipping_phone},
                cc_number: {$t:arr[i].cc_number},
                cc_hash: {$t:arr[i].cc_hash},
                cc_exp: {$t:arr[i].cc_exp},
                cavv: {$t:arr[i].cavv},
                cavv_result: {$t:arr[i].cavv_result},
                xid: {$t:arr[i].xid},
                avs_response: {$t:arr[i].avs_response},
                csc_response: {$t:arr[i].csc_response},
                cardholder_auth: {$t:arr[i].cardholder_auth},
                cc_start_date: {$t:arr[i].cc_start_date},
                cc_issue_number: {$t:arr[i].cc_issue_number},
                check_account: {$t:arr[i].check_account},
                check_hash: {$t:arr[i].check_hash},
                check_aba: {$t:arr[i].check_aba},
                check_name: {$t:arr[i].check_name},
                account_holder_type: {$t:arr[i].account_holder_type},
                account_type: {$t:arr[i].account_type},
                sec_code: {$t:arr[i].sec_code},
                drivers_license_number: {$t:arr[i].drivers_license_number},
                drivers_license_state: {$t:arr[i].drivers_license_state},
                drivers_license_dob: {$t:arr[i].drivers_license_dob},
                social_security_number: {$t:arr[i].social_security_number},
                processor_id: {$t:arr[i].processor_id},
                tax: {$t:arr[i].tax},
                currency: {$t:arr[i].currency},
                surcharge: {$t:arr[i].surcharge},
                tip: {$t:arr[i].tip},
                card_balance: {},
                card_available_balance: {},
                entry_mode: {},
                original_transaction_id:{$t:arr[i].Original_transaction_id},
                cc_bin: {$t:arr[i].cc_bin},
				merchant_name: {$t:arr[i].Merchant_Name},
				merchant_id: {$t:arr[i].Merchant_ID},
                boarding_id: {$t:arr[i].Boarding_ID},
                action: {
                    amount: {$t:arr[i].amount},
                    action_type: {$t:arr[i].action_type},
                    date: {$t:arr[i].date},
                    success: {$t:arr[i].success},
                    ip_address: {$t:arr[i].ip_address},
                    source: {$t:arr[i].source},
                    username: {$t:arr[i].username},
                    response_text: {$t:arr[i].response_text},
                    batch_id: {$t:arr[i].batch_id},
                    processor_batch_id: {$t:arr[i].processor_batch_id},
                    response_code: {$t:arr[i].response_code},
                    processor_response_text: {},
                    processor_response_code: {},
                    device_license_number: {$t:arr[i].device_license_number},
                    device_nickname: {$t:arr[i].device_nickname}
                },
                id: arr[i].id
            });
            if (typeof history !=='undefined'){
                newObjectTransactions[counter].history=[];
                for (var j = 0; j < Object.keys(history).length; j++) {
                    newObjectTransactions[counter].history.push({
                        transaction_id: {$t:history[j].transaction_id},
                        transaction_type: {$t:history[j].transaction_type},
                        processor_id: {$t:history[j].processor_id},
                        amount: {$t:history[j].amount},
                        action_type: {$t:history[j].action_type},
                        date: {$t:history[j].date},
                        success: {$t:history[j].success},
                        response_text: {$t:history[j].response_text},
                        batch_id: {$t:history[j].batch_id},
                        processor_batch_id: {$t:history[j].processor_batch_id},
                        response_code: {$t:history[j].response_code}
                    });
                }
            }
            counter+=1;
            if (counter===Object.keys(arr).length){
                var newObjectTransactionsWrapped = {};
                newObjectTransactionsWrapped.nm_response = {};
                newObjectTransactionsWrapped.nm_response.transaction = newObjectTransactions;
                 deferred.resolve(newObjectTransactionsWrapped);
            }
        }
        return deferred.promise;
    },


    parseLikeNMIAccountBalance = function(arr){

        var deferred = Q.defer();
        var counter = 0;
        var newObjectTransactions = [];

        for (var i = 0; i < Object.keys(arr).length; i++) {
            newObjectTransactions.push({
                reserve: {$t:arr[i].reserve},
                transaction_id: {$t:arr[i].transaction_id},
                //partial_payment_id: {},
                //partial_payment_balance: {},
                //platform_id: {$t:arr[i].platform_id},
                //transaction_type: {$t:arr[i].transaction_type},
                condition: {$t:arr[i].transaction_condition},
                //order_id: {$t:arr[i].order_id},
                //authorization_code: {$t:arr[i].authorization_code},
                //ponumber: {$t:arr[i].ponumber},
                //order_description: {$t:arr[i].order_description},
                //first_name: {$t:arr[i].first_name},
                //last_name: {$t:arr[i].last_name},
                //address_1: {$t:arr[i].address_1},
                //address_2: {$t:arr[i].address_2},
                //company: {$t:arr[i].company},
                //city: {$t:arr[i].city},
                //state: {$t:arr[i].state},
                //postal_code: {$t:arr[i].postal_code},
                //country: {$t:arr[i].country},
                //email: {$t:arr[i].email},
                //phone: {$t:arr[i].phone},
                //fax: {$t:arr[i].fax},
                //cell_phone: {$t:arr[i].cell_phone},
                //customertaxid: {$t:arr[i].customertaxid},
                //website: {$t:arr[i].website},
                //shipping_first_name: {$t:arr[i].shipping_first_name},
                //shipping_last_name: {$t:arr[i].shipping_last_name},
                //shipping_address_1: {$t:arr[i].shipping_address_1},
                //shipping_address_2: {$t:arr[i].shipping_address_2},
                //shipping_company: {$t:arr[i].shipping_company},
                //shipping_city: {$t:arr[i].shipping_city},
                //shipping_state: {$t:arr[i].shipping_state},
                //shipping_postal_code: {$t:arr[i].shipping_postal_code},
                //shipping_country: {$t:arr[i].shipping_country},
                //shipping_email: {$t:arr[i].shipping_email},
                //shipping_carrier: {$t:arr[i].shipping_carrier},
                //tracking_number: {$t:arr[i].tracking_number},
                //shipping_date: {$t:arr[i].shipping_date},
                //shipping: {$t:arr[i].Shipping},
                //shipping_phone: {$t:arr[i].shipping_phone},
                //cc_number: {$t:arr[i].cc_number},
                //cc_hash: {$t:arr[i].cc_hash},
                //cc_exp: {$t:arr[i].cc_exp},
                //cavv: {$t:arr[i].cavv},
                //cavv_result: {$t:arr[i].cavv_result},
                //xid: {$t:arr[i].xid},
                //avs_response: {$t:arr[i].avs_response},
                //csc_response: {$t:arr[i].csc_response},
                //cardholder_auth: {$t:arr[i].cardholder_auth},
                //cc_start_date: {$t:arr[i].cc_start_date},
                //cc_issue_number: {$t:arr[i].cc_issue_number},
                //check_account: {$t:arr[i].check_account},
                //check_hash: {$t:arr[i].check_hash},
                //check_aba: {$t:arr[i].check_aba},
                //check_name: {$t:arr[i].check_name},
                //account_holder_type: {$t:arr[i].account_holder_type},
                //account_type: {$t:arr[i].account_type},
                //sec_code: {$t:arr[i].sec_code},
                //drivers_license_number: {$t:arr[i].drivers_license_number},
                //drivers_license_state: {$t:arr[i].drivers_license_state},
                //drivers_license_dob: {$t:arr[i].drivers_license_dob},
                //social_security_number: {$t:arr[i].social_security_number},
                processor_id: {$t:arr[i].processor_id},
                //tax: {$t:arr[i].tax},
                currency: {$t:arr[i].currency},
                //surcharge: {$t:arr[i].surcharge},
                //tip: {$t:arr[i].tip},
                //card_balance: {},
                //card_available_balance: {},
                //entry_mode: {},
                //cc_bin: {$t:arr[i].cc_bin},
                action: {
                    amount: {$t:arr[i].amount},
                    action_type: {$t:arr[i].action_type},
                    date: {$t:arr[i].date},
                    //success: {$t:arr[i].success},
                    //ip_address: {$t:arr[i].ip_address},
                    //source: {$t:arr[i].source},
                    //username: {$t:arr[i].username},
                    //response_text: {$t:arr[i].response_text},
                    batch_id: {$t:arr[i].batch_id},
                    //processor_batch_id: {$t:arr[i].processor_batch_id},
                    response_code: {$t:arr[i].response_code}
                    //processor_response_text: {},
                    //processor_response_code: {},
                    //device_license_number: {$t:arr[i].device_license_number},
                    //device_nickname: {$t:arr[i].device_nickname}
                }
            });
            counter+=1;
            if (counter===Object.keys(arr).length){
                var newObjectTransactionsWrapped = {};
                newObjectTransactionsWrapped.nm_response = {};
                newObjectTransactionsWrapped.nm_response.transaction = newObjectTransactions;
                deferred.resolve(newObjectTransactionsWrapped);
            }
        }
        return deferred.promise;
    },


    /**
     *
     * @param arg
     * @param url
     * @returns {*}
     */
    getAllMerchants = function(arg, url){
        merchants = [];
        boardingsCollection = [];
        var deferred = Q.defer();
        db.Payment_Plan.findAll({ where: {Processor_ID: {ne: null},Is_Active:1}}).success(function(accounts){
            var totalBoarding = accounts.length,
                countBoarding = 1;
            accounts.forEach(function(account) {
                  db.Boarding.find({ where: {id: account.Boarding_ID}}).success(function(boardings){
                    if(!boardings) {
                        console.log('err');
                    } else {
                        boardingsCollection.push(boardings);
                        arg.username = account.Username; 
                        arg.password = account.Password;
                        if (boardings.Processor_ID !== null){
                            arg.processor_id = account.Processor_ID;
                        }else{
                            arg.processor_id = '';
                        }
                        var items = Object.keys(arg);
                        queryString ='?';
                        items.forEach(function(item) {
                          if ((arg[item] !== '') && (arg[item] !== undefined)){
                               queryString+='&' + item + '=' + encodeURIComponent(arg[item]);
                          }
                        });
                        url+=queryString;
//console.log(url)
                        request(url, function (error, response, body) {
                            if (!error && response.statusCode === 200) {
                                var json = XMLMapping.load(body);
                                merchants.push(json);
                                if (totalBoarding===countBoarding){
                                    deferred.resolve(merchants);
                                }else{
                                    countBoarding+=1;
                                }
                            }
                        });
                    }
                }).error(function(err){
                    //return res.jsonp({Response:err});
                    console.log('err');
                });
            });
        }).error(function(err){
            console.log('err');
            /*
            return res.render('error', {
                error: err,
                status: 500
            });
            */
        });


        return deferred.promise;
    },
    /**
     *
     * @returns {*}
     */
    getBoardings = function(){
        boardingsCollection = [];
        var    deferred = Q.defer();
        db.Payment_Plan.findAll({ where: {Processor_ID: {ne: null},Is_Active:1}}).success(function(accounts){
            var totalBoarding = accounts.length,
                countBoarding = 1;
            accounts.forEach(function(account) {
                db.Boarding.find({ where: {id: account.Boarding_ID}}).success(function(boardings){
                    if(!boardings) {
                        console.log('err');
                    } else {
                        boardingsCollection.push(boardings);
                        if (totalBoarding === countBoarding){
                            deferred.resolve(boardingsCollection);
                        }else{
                            countBoarding+=1;
                        }
                    }
                }).error(function(err){
                    console.log('err');
                });

            });
        }).error(function(err){
            console.log('err');
        });
        return deferred.promise;
    },

        
    sendEmail = function (req, res, emailParameters, transaction_info){
//console.log('sending a receipt', emailParameters, transaction_info.dataValues)
        if (emailParameters.send_mojopay_receipt === 'true' && transaction_info.dataValues.response_code === '100') {
                var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
                if (pattern.test(emailParameters.mojo_receipt_email_address)) {
//console.log('sending a receipt')
                    var emailHTML = '',
                        today = new Date(),
                        billing_date = '',
                        cc_number = emailParameters.cc_number;
                    billing_date = ('0' + (today.getMonth() + 1)).slice(-2) + '/' +  ('0' + today.getDate()).slice(-2) + '/' + today.getFullYear();
                    res.render('templates/transaction_receipt_email', {
                        MOJOPAY_MERCHANT_NAME1: emailParameters.mojo_receipt_merchant_name,
                        DATE: billing_date,
                        FIRSTNAME: emailParameters.mojo_receipt_customer_name,
                        BILLING_BLOCK: emailParameters.mojo_reciept_billing_address,
                        SHIPPING_TITLE: 'Shipping Information',
                        SHIPPING_BLOCK: emailParameters.mojo_reciept_shipping_address,
                        TRANSACTIONID: transaction_info.dataValues.transaction_id,
                        CARDTYPE: transaction_info.dataValues.cc_number,
                        AMOUNT: emailParameters.mojo_receipt_amount.toFixed(2),
                        MOJOPAY_MERCHANT_NAME2: emailParameters.mojo_receipt_merchant_name
                    }, function(err, emailHTML) {
//                       if (emailHTML !== undefined && emailHTML !== null && emailHTML !== '') {
                            emailHTML = emailHTML.replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/undefined/g,' ');
                            var smtpTransport = nodemailer.createTransport(config.mailer.options);
                            // mail to a single address
                            var mailOptions = {
                                from: config.mailer.from,
                                to: emailParameters.mojo_receipt_email_address, 
                                subject: 'Receipt from ' + emailParameters.mojo_receipt_merchant_name,
                                html: emailHTML
                            };
                            smtpTransport.sendMail(mailOptions, function(err) {
    
                                if(err){
                                    console.log(err);
                                }else{
//                                    console.log("Message sent");
                                }
    
                            }); // smtpTransport.sendMail
//                        } // if (emailHTML !== undefined && emailHTML !== null && emailHTML !== '')
                    }); // res.render
                } else {
//console.log('not sending a receipt - invalid email address')
                }
        } else {
//console.log('not sending a receipt')
        }
    };


    /**
     * Mask Credit Card
     * @param cc
     * @returns {string}
     */
    function doMask (cc){
        var first6 = cc.substring(0, 6);
        var last4 = cc.substring(cc.length - 4);
        var mask = '';
        for (var i = 0; i < cc.length-10; i++) {
            mask+='*';
        }
        return first6+mask+last4;
    }


    function getBrand (cc){
        var cc_number = cc;
        if ((cc_number.substr(1, 1)==='x') || (cc_number.substr(1, 1)==='*')){
            return 'Unknown';
        }else{
            // visa
            var re = new RegExp('^4');
            if (cc_number.match(re) !== null)
                return 'Visa';

            // Mastercard
            re = new RegExp('^5[1-5]');
            if (cc_number.match(re) !== null)
                return 'Mastercard';

            // AMEX
            re = new RegExp('^3[47]');
            if (cc_number.match(re) !== null)
                return 'AMEX';

            // Discover
            re = new RegExp('^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)');
            if (cc_number.match(re) !== null)
                return 'Discover';

            // Diners
            re = new RegExp('^36');
            if (cc_number.match(re) !== null)
                return 'Diners';

            // Diners - Carte Blanche
            re = new RegExp('^30[0-5]');
            if (cc_number.match(re) !== null)
                return 'Diners - Carte Blanche';

            // JCB
            re = new RegExp('^35(2[89]|[3-8][0-9])');
            if (cc_number.match(re) !== null)
                return 'JCB';

            // Visa Electron
            re = new RegExp('^(4026|417500|4508|4844|491(3|7))');
            if (cc_number.match(re) !== null)
                return 'Visa Electron';
        } // if ((cc_number.substr(1, 1)==='x') || (cc_number.substr(1, 1)==='*'))
    }


    /**
     * Grabs Payment_plans for mojocore and twt2pay to sum them for multiple merchants
     * @param product
     * @param feesObject
     * @returns {*}
     */
    function buildBothObjects(product, feesObject){
        var deferred = Q.defer();
        objectProductOne = [];
        objectProductTwo = [];
        feesObject.forEach(function(items){
            if (parseInt(items.Product_ID)===1){
                objectProductOne.push(items);
            }else if(parseInt(items.Product_ID)===2){
                objectProductTwo.push(items);
            }
        });
        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     * Check if any process is repeated, if is repeated, then a log is recorded
     */
    function checkProcessorsRepeated (){
        var deferred = Q.defer();
        var processorsRepeated = '';
        db.sequelize.query('SELECT count(id) as count, Processor_ID FROM mean_relational.Payment_Plans WHERE Processor_ID!=\'\' GROUP BY Processor_ID',null,{ raw: true }
        ).then(function(results) {
                results.forEach(function(processor){
                    if (parseInt(processor.count)>1){
                        processorsRepeated += ' ' + processor.Processor_ID;                       
                    }                                      
                });
                deferred.resolve(processorsRepeated);
            });
        return deferred.promise;
    }

    /**
     * Sums both twt2pay and mojocore fees into one payment_plan for multiple merchants
     * @param product
     * @param feesObject
     * @param single
     * @param res
     * @returns {*}
     */
    function sumAdminFees (product, feesObject,single,processorIdSelected,res){
        // is called when product is Twt2pay and sum fees values with MojoCore fees
        var deferred = Q.defer();
        var Authorization_Fee = 0;
        var Chargeback_Fee = 0;
        var Retrieval_Fee = 0;
        var Sale_Fee = 0;
        var Capture_Fee = 0;
        var Void_Fee = 0;
        var Chargeback_Reversal_Fee = 0;
        var Refund_Fee = 0;
        var Credit_Fee = 0;
        var Declined_Fee = 0;
        var selectedboardingid = "";
        objectProduct = [];
        if (single===true){
            selectedboardingid = feesObject[0].dataValues.boarding_id;
            db.Payment_Plan.findAll({ where: {Boarding_ID: selectedboardingid,Is_Active:1}})
                .success(function(payment_plan){
                    var buildingObjects = buildBothObjects(product, payment_plan).then(
                        function(result){
                            for (var i = 0; i < Object.keys(objectProductTwo).length; i++) {
                                for(var j = 0; j < Object.keys(objectProductOne).length; j++) {
                                    if ((objectProductTwo[i].Boarding_ID ===  objectProductOne[j].Boarding_ID)&&(objectProductOne[j].Boarding_ID !== null)&&(objectProductTwo[i].Boarding_ID !== null)&&(objectProductTwo[i].Boarding_ID !== '')){
                                        var sumAuthorization_Fee = parseFloat(objectProductTwo[i].Authorization_Fee) + parseFloat(objectProductOne[j].Authorization_Fee),
                                            sumChargeback_Fee = parseFloat(objectProductTwo[i].Chargeback_Fee) + parseFloat(objectProductOne[j].Chargeback_Fee),
                                            sumRetrieval_Fee = parseFloat(objectProductTwo[i].Retrieval_Fee) + parseFloat(objectProductOne[j].Retrieval_Fee),
                                            sumSale_Fee = parseFloat(objectProductTwo[i].Sale_Fee) + parseFloat(objectProductOne[j].Sale_Fee),
                                            sumDiscount_Rate = parseFloat(objectProductTwo[i].Discount_Rate) + parseFloat(objectProductOne[j].Discount_Rate),
                                            sumCapture_Fee = parseFloat(objectProductTwo[i].Capture_Fee) + parseFloat(objectProductOne[j].Capture_Fee),
                                            sumVoid_Fee = parseFloat(objectProductTwo[i].Void_Fee) + parseFloat(objectProductOne[j].Void_Fee),
                                            sumChargeback_Reversal_Fee = parseFloat(objectProductTwo[i].Chargeback_Reversal_Fee) + parseFloat(objectProductOne[j].Chargeback_Reversal_Fee),
                                            sumRefund_Fee = parseFloat(objectProductTwo[i].Refund_Fee) + parseFloat(objectProductOne[j].Refund_Fee),
                                            sumCredit_Fee = parseFloat(objectProductTwo[i].Credit_Fee) + parseFloat(objectProductOne[j].Credit_Fee),
                                            sumDeclined_Fee = parseFloat(objectProductTwo[i].Declined_Fee) + parseFloat(objectProductOne[j].Declined_Fee),
                                            sumMonthly_Fee = parseFloat(objectProductTwo[i].Monthly_Fee) + parseFloat(objectProductOne[j].Monthly_Fee),
                                            sumSetup_Fee = parseFloat(objectProductTwo[i].Setup_Fee) + parseFloat(objectProductOne[j].Setup_Fee),
                                            sumAch_Reject_Fee = parseFloat(objectProductTwo[i].Ach_Reject_Fee) + parseFloat(objectProductOne[j].Ach_Reject_Fee);
                                        objectProduct.push({'Is_Active':1,'Setup_Fee':sumSetup_Fee,'Monthly_Fee':sumMonthly_Fee,'Discount_Rate':sumDiscount_Rate,'Boarding_ID': objectProductTwo[i].Boarding_ID, 'Username': objectProductTwo[i].Username, 'Password': objectProductTwo[i].Password, 'Product_ID': objectProductTwo[i].Product_ID, 'Processor_ID': objectProductTwo[i].Processor_ID, 'Authorization_Fee': sumAuthorization_Fee, 'Chargeback_Fee': sumChargeback_Fee, 'Retrieval_Fee': sumRetrieval_Fee,'Sale_Fee': sumSale_Fee,'Capture_Fee': sumCapture_Fee,'Void_Fee': sumVoid_Fee,'Chargeback_Reversal_Fee': sumChargeback_Reversal_Fee,'Refund_Fee': sumRefund_Fee,'Credit_Fee': sumCredit_Fee,'Declined_Fee':sumDeclined_Fee,'Ach_Reject_Fee':sumAch_Reject_Fee});
                                    } // if
                                deferred.resolve(objectProduct);
                                } // for(var j = 0; j < Object.keys(objectProductOne).length; j++)
                            } // for (var i = 0; i < Object.keys(objectProductTwo).length; i++) 
                        }, // var buildingObjects = buildBothObjects(product, payment_plan).then
                        function(err){
                            //return res.jsonp({Response:err});
                            console.log(err);
                        }); // var buildingObjects = buildBothObjects(product, payment_plan).err
                }) //db.Payment_Plan.findAll({ where: {Boarding_ID: selectedboardingid,Is_Active:1}}.success
                .error(function(err){
                    console.log('err');
                }); // db.Payment_Plan.findAll({ where: {Boarding_ID: selectedboardingid,Is_Active:1}}).error
        }else if(single==='allprocessor'){
            db.Payment_Plan.findAll({ where: {Boarding_ID:processorIdSelected,Is_Active:1}}).success(function(payment_plan){
                var buildingObjects = buildBothObjects(product, payment_plan).then(
                    function(result){
                        for(var j = 0; j < Object.keys(objectProductOne).length; j++) {
                            for (var i = 0; i < Object.keys(objectProductTwo).length; i++) {
                                if ((objectProductTwo[i].Boarding_ID ===  objectProductOne[j].Boarding_ID)&&(objectProductOne[j].Boarding_ID !== null)&&(objectProductTwo[i].Boarding_ID !== null)&&(objectProductTwo[i].Boarding_ID !== '')){
                                    var sumAuthorization_Fee = parseFloat(objectProductTwo[i].Authorization_Fee) + parseFloat(objectProductOne[j].Authorization_Fee),
                                        sumChargeback_Fee = parseFloat(objectProductTwo[i].Chargeback_Fee) + parseFloat(objectProductOne[j].Chargeback_Fee),
                                        sumRetrieval_Fee = parseFloat(objectProductTwo[i].Retrieval_Fee) + parseFloat(objectProductOne[j].Retrieval_Fee),
                                        sumSale_Fee = parseFloat(objectProductTwo[i].Sale_Fee) + parseFloat(objectProductOne[j].Sale_Fee),
                                        sumDiscount_Rate = parseFloat(objectProductTwo[i].Discount_Rate) + parseFloat(objectProductOne[j].Discount_Rate),
                                        sumCapture_Fee = parseFloat(objectProductTwo[i].Capture_Fee) + parseFloat(objectProductOne[j].Capture_Fee),
                                        sumVoid_Fee = parseFloat(objectProductTwo[i].Void_Fee) + parseFloat(objectProductOne[j].Void_Fee),
                                        sumChargeback_Reversal_Fee = parseFloat(objectProductTwo[i].Chargeback_Reversal_Fee) + parseFloat(objectProductOne[j].Chargeback_Reversal_Fee),
                                        sumRefund_Fee = parseFloat(objectProductTwo[i].Refund_Fee) + parseFloat(objectProductOne[j].Refund_Fee),
                                        sumCredit_Fee = parseFloat(objectProductTwo[i].Credit_Fee) + parseFloat(objectProductOne[j].Credit_Fee),
                                        sumDeclined_Fee = parseFloat(objectProductTwo[i].Declined_Fee) + parseFloat(objectProductOne[j].Declined_Fee),
                                        sumMonthly_Fee = parseFloat(objectProductTwo[i].Monthly_Fee) + parseFloat(objectProductOne[j].Monthly_Fee),
                                        sumSetup_Fee = parseFloat(objectProductTwo[i].Setup_Fee) + parseFloat(objectProductOne[j].Setup_Fee),
                                        sumAch_Reject_Fee = parseFloat(objectProductTwo[i].Ach_Reject_Fee) + parseFloat(objectProductOne[j].Ach_Reject_Fee);
                                    if ((objectProductOne[j].Processor_ID!== null)&&(objectProductOne[j].Processor_ID !== '')){
                                        objectProduct.push({'Is_Active':1,'Setup_Fee':objectProductOne[j].Setup_Fee,'Monthly_Fee':objectProductOne[j].Monthly_Fee,'Discount_Rate':objectProductOne[j].Discount_Rate,'Boarding_ID': objectProductOne[j].Boarding_ID, 'Username': objectProductOne[j].Username, 'Password': objectProductOne[j].Password, 'Product_ID': objectProductOne[j].Product_ID, 'Processor_ID': objectProductOne[j].Processor_ID, 'Authorization_Fee': objectProductOne[j].Authorization_Fee, 'Chargeback_Fee': objectProductOne[j].Chargeback_Fee, 'Retrieval_Fee': objectProductOne[j].Retrieval_Fee,'Sale_Fee': objectProductOne[j].Sale_Fee,'Capture_Fee': objectProductOne[j].Capture_Fee,'Void_Fee': objectProductOne[j].Void_Fee,'Chargeback_Reversal_Fee': objectProductOne[j].Chargeback_Reversal_Fee,'Refund_Fee': objectProductOne[j].Refund_Fee,'Credit_Fee': objectProductOne[j].Credit_Fee,'Declined_Fee':objectProductOne[j].Declined_Fee,'Ach_Reject_Fee':objectProductOne[j].Ach_Reject_Fee});
                                    } // if
                                    objectProduct.push({'Is_Active':1,'Setup_Fee':sumSetup_Fee,'Monthly_Fee':sumMonthly_Fee,'Discount_Rate':sumDiscount_Rate,'Boarding_ID': objectProductTwo[i].Boarding_ID, 'Username': objectProductTwo[i].Username, 'Password': objectProductTwo[i].Password, 'Product_ID': objectProductTwo[i].Product_ID, 'Processor_ID': objectProductTwo[i].Processor_ID, 'Authorization_Fee': sumAuthorization_Fee, 'Chargeback_Fee': sumChargeback_Fee, 'Retrieval_Fee': sumRetrieval_Fee,'Sale_Fee': sumSale_Fee,'Capture_Fee': sumCapture_Fee,'Void_Fee': sumVoid_Fee,'Chargeback_Reversal_Fee': sumChargeback_Reversal_Fee,'Refund_Fee': sumRefund_Fee,'Credit_Fee': sumCredit_Fee,'Declined_Fee':sumDeclined_Fee,'Ach_Reject_Fee':sumAch_Reject_Fee});
                                } // if

                                deferred.resolve(objectProduct);
                            } // for (var i = 0; i < Object.keys(objectProductTwo).length; i++)
                        } // for(var j = 0; j < Object.keys(objectProductOne).length; j++) 
                        if (Object.keys(objectProductTwo).length === 0 ){
//                            for(var j = 0; j < Object.keys(objectProductOne).length; j++) // MC-393 ISSUE duplicate variable j
                            for(j = 0; j < Object.keys(objectProductOne).length; j++) {
                                if ((objectProductOne[j].Boarding_ID !== null)&&(objectProductOne[j].Boarding_ID !== '')&&(objectProductOne[j].Processor_ID !== null)&&(objectProductOne[j].Processor_ID !== '')){
                                    objectProduct.push({'Is_Active':1,'Setup_Fee':objectProductOne[j].Setup_Fee,'Monthly_Fee':objectProductOne[j].Monthly_Fee,'Discount_Rate':objectProductOne[j].Discount_Rate,'Boarding_ID': objectProductOne[j].Boarding_ID, 'Username': objectProductOne[j].Username, 'Password': objectProductOne[j].Password, 'Product_ID': objectProductOne[j].Product_ID, 'Processor_ID': objectProductOne[j].Processor_ID, 'Authorization_Fee': objectProductOne[j].Authorization_Fee, 'Chargeback_Fee': objectProductOne[j].Chargeback_Fee, 'Retrieval_Fee': objectProductOne[j].Retrieval_Fee,'Sale_Fee': objectProductOne[j].Sale_Fee,'Capture_Fee': objectProductOne[j].Capture_Fee,'Void_Fee': objectProductOne[j].Void_Fee,'Chargeback_Reversal_Fee': objectProductOne[j].Chargeback_Reversal_Fee,'Refund_Fee': objectProductOne[j].Refund_Fee,'Credit_Fee': objectProductOne[j].Credit_Fee,'Declined_Fee':objectProductOne[j].Declined_Fee,'Ach_Reject_Fee':objectProductOne[j].Ach_Reject_Fee});
                                } // if 
                                deferred.resolve(objectProduct);
                            } // for(j = 0; j < Object.keys(objectProductOne).length; j++) 
                        } // if (Object.keys(objectProductTwo).length === 0 )
                    }, // var buildingObjects = buildBothObjects(product, payment_plan).then
                    function(err){
                        //return res.jsonp({Response:err});
						console.log(err);
                     }); // var buildingObjects = buildBothObjects(product, payment_plan).err
            }).error(function(err){
                console.log('err');
            });
        }else{
            db.Payment_Plan.findAll({ where: {Is_Active:1}}).success(function(payment_plan){
                var buildingObjects = buildBothObjects(product, payment_plan).then(
                    function(result){

                        for(var j = 0; j < Object.keys(objectProductOne).length; j++) {
                            if(single==='multiple'){
                                if ((objectProductOne[j].Boarding_ID !== null)&&(objectProductOne[j].Boarding_ID !== '')&&(objectProductOne[j].Processor_ID !== null)&&(objectProductOne[j].Processor_ID !== '')){
                                    objectProduct.push({'Is_Active':1,'Setup_Fee':objectProductOne[j].Setup_Fee,'Monthly_Fee':objectProductOne[j].Monthly_Fee,'Discount_Rate':objectProductOne[j].Discount_Rate,'Boarding_ID': objectProductOne[j].Boarding_ID, 'Username': objectProductOne[j].Username, 'Password': objectProductOne[j].Password, 'Product_ID': objectProductOne[j].Product_ID, 'Processor_ID': objectProductOne[j].Processor_ID, 'Authorization_Fee': objectProductOne[j].Authorization_Fee, 'Chargeback_Fee': objectProductOne[j].Chargeback_Fee, 'Retrieval_Fee': objectProductOne[j].Retrieval_Fee,'Sale_Fee': objectProductOne[j].Sale_Fee,'Capture_Fee': objectProductOne[j].Capture_Fee,'Void_Fee': objectProductOne[j].Void_Fee,'Chargeback_Reversal_Fee': objectProductOne[j].Chargeback_Reversal_Fee,'Refund_Fee': objectProductOne[j].Refund_Fee,'Credit_Fee': objectProductOne[j].Credit_Fee,'Declined_Fee':objectProductOne[j].Declined_Fee,'Ach_Reject_Fee':objectProductOne[j].Ach_Reject_Fee});
                                } // if
                            } // if(single==='multiple')
                            for (var i = 0; i < Object.keys(objectProductTwo).length; i++) {
                                if ((objectProductTwo[i].Boarding_ID ===  objectProductOne[j].Boarding_ID)&&(objectProductOne[j].Boarding_ID !== null)&&(objectProductTwo[i].Boarding_ID !== null)&&(objectProductTwo[i].Boarding_ID !== '')) {
                                    var sumAuthorization_Fee = parseFloat(objectProductTwo[i].Authorization_Fee) + parseFloat(objectProductOne[j].Authorization_Fee),
                                        sumChargeback_Fee = parseFloat(objectProductTwo[i].Chargeback_Fee) + parseFloat(objectProductOne[j].Chargeback_Fee),
                                        sumRetrieval_Fee = parseFloat(objectProductTwo[i].Retrieval_Fee) + parseFloat(objectProductOne[j].Retrieval_Fee),
                                        sumSale_Fee = parseFloat(objectProductTwo[i].Sale_Fee) + parseFloat(objectProductOne[j].Sale_Fee),
                                        sumDiscount_Rate = parseFloat(objectProductTwo[i].Discount_Rate) + parseFloat(objectProductOne[j].Discount_Rate),
                                        sumCapture_Fee = parseFloat(objectProductTwo[i].Capture_Fee) + parseFloat(objectProductOne[j].Capture_Fee),
                                        sumVoid_Fee = parseFloat(objectProductTwo[i].Void_Fee) + parseFloat(objectProductOne[j].Void_Fee),
                                        sumChargeback_Reversal_Fee = parseFloat(objectProductTwo[i].Chargeback_Reversal_Fee) + parseFloat(objectProductOne[j].Chargeback_Reversal_Fee),
                                        sumRefund_Fee = parseFloat(objectProductTwo[i].Refund_Fee) + parseFloat(objectProductOne[j].Refund_Fee),
                                        sumCredit_Fee = parseFloat(objectProductTwo[i].Credit_Fee) + parseFloat(objectProductOne[j].Credit_Fee),
                                        sumDeclined_Fee = parseFloat(objectProductTwo[i].Declined_Fee) + parseFloat(objectProductOne[j].Declined_Fee),
                                        sumMonthly_Fee = parseFloat(objectProductTwo[i].Monthly_Fee) + parseFloat(objectProductOne[j].Monthly_Fee),
                                        sumSetup_Fee = parseFloat(objectProductTwo[i].Setup_Fee) + parseFloat(objectProductOne[j].Setup_Fee),
                                        sumAch_Reject_Fee = parseFloat(objectProductTwo[i].Ach_Reject_Fee) + parseFloat(objectProductOne[j].Ach_Reject_Fee);
                                    objectProduct.push({'Is_Active':1,'Setup_Fee':sumSetup_Fee,'Monthly_Fee':sumMonthly_Fee,'Discount_Rate':sumDiscount_Rate,'Boarding_ID': objectProductTwo[i].Boarding_ID, 'Username': objectProductTwo[i].Username, 'Password': objectProductTwo[i].Password, 'Product_ID': objectProductTwo[i].Product_ID, 'Processor_ID': objectProductTwo[i].Processor_ID, 'Authorization_Fee': sumAuthorization_Fee, 'Chargeback_Fee': sumChargeback_Fee, 'Retrieval_Fee': sumRetrieval_Fee,'Sale_Fee': sumSale_Fee,'Capture_Fee': sumCapture_Fee,'Void_Fee': sumVoid_Fee,'Chargeback_Reversal_Fee': sumChargeback_Reversal_Fee,'Refund_Fee': sumRefund_Fee,'Credit_Fee': sumCredit_Fee,'Declined_Fee':sumDeclined_Fee,'Ach_Reject_Fee':sumAch_Reject_Fee});
                                } // if
                                deferred.resolve(objectProduct);
                            } // for (var i = 0; i < Object.keys(objectProductTwo).length; i++)
                        } // for(var j = 0; j < Object.keys(objectProductOne).length; j++)
                    }, // function(result)
                    function(err){
                        //return res.jsonp({Response:err});
                        console.log(err);
                    }); // function(err)
            }).error(function(err){
                console.log('err');
            }); // db.Payment_Plan.findAll.success
        } // if single else if single else
        return deferred.promise;
    }


    /**
     * Sums both twt2pay and mojocore fees into one payment_plan
     * @param feeObject
     * @returns {*}
     */
    function checkFees (feeObject){
        var deferred = Q.defer();

        var Discount_Rate = 0;
        var Monthly_Fee = 0;
        var Setup_Fee = 0;
        var Authorization_Fee = 0;
        var Chargeback_Fee = 0;
        var Retrieval_Fee = 0;
        var Sale_Fee = 0;
        var Capture_Fee = 0;
        var Void_Fee = 0;
        var Chargeback_Reversal_Fee = 0;
        var Refund_Fee = 0;
        var Credit_Fee = 0;
        var Declined_Fee = 0;
        var Ach_Reject_Fee = 0;

        var Discount_Rate2 = 0;
        var Monthly_Fee2 = 0;
        var Setup_Fee2 = 0;
        var Authorization_Fee2 = 0;
        var Chargeback_Fee2 = 0;
        var Retrieval_Fee2 = 0;
        var Sale_Fee2 = 0;
        var Capture_Fee2 = 0;
        var Void_Fee2 = 0;
        var Chargeback_Reversal_Fee2 = 0;
        var Refund_Fee2 = 0;
        var Credit_Fee2 = 0;
        var Declined_Fee2 = 0;
        var Ach_Reject_Fee2 = 0;

        var Discount_Rate3 = 0;
        var Monthly_Fee3 = 0;
        var Setup_Fee3 = 0;
        var Authorization_Fee3 = 0;
        var Chargeback_Fee3 = 0;
        var Retrieval_Fee3 = 0;
        var Sale_Fee3 = 0;
        var Capture_Fee3 = 0;
        var Void_Fee3 = 0;
        var Chargeback_Reversal_Fee3    = 0;
        var Refund_Fee3 = 0;
        var Credit_Fee3 = 0;
        var Declined_Fee3 = 0;
        var Ach_Reject_Fee3 = 0;

        var existMojo = false;

        var counter = 0;
        for (var i = 0; i < Object.keys(feeObject).length; i++) {
            if (feeObject[i].Product_ID === 1){
                existMojo = true;
                if ((feeObject[i].Discount_Rate!=='null') && (feeObject[i].Discount_Rate!=='')){Discount_Rate = parseFloat(feeObject[i].Discount_Rate);}
                if ((feeObject[i].Monthly_Fee!=='null') && (feeObject[i].Monthly_Fee!=='')){Monthly_Fee = feeObject[i].Monthly_Fee;}
                if ((feeObject[i].Setup_Fee!=='null') && (feeObject[i].Setup_Fee!=='')){Setup_Fee = feeObject[i].Setup_Fee;}
                if ((feeObject[i].Authorization_Fee!=='null') && (feeObject[i].Authorization_Fee!=='')){Authorization_Fee = feeObject[i].Authorization_Fee;}
                if ((feeObject[i].Chargeback_Fee!=='null') && (feeObject[i].Chargeback_Fee!=='')){Chargeback_Fee = feeObject[i].Chargeback_Fee;}
                if ((feeObject[i].Retrieval_Fee!=='null') && (feeObject[i].Retrieval_Fee!=='')){Retrieval_Fee = feeObject[i].Retrieval_Fee;}
                if ((feeObject[i].Sale_Fee!=='null') && (feeObject[i].Sale_Fee!=='')){Sale_Fee = feeObject[i].Sale_Fee;}
                if ((feeObject[i].Capture_Fee!=='null') && (feeObject[i].Capture_Fee!=='')){Capture_Fee = feeObject[i].Capture_Fee;}
                if ((feeObject[i].Void_Fee!=='null') && (feeObject[i].Void_Fee!=='')){Void_Fee = feeObject[i].Void_Fee;}
                if ((feeObject[i].Chargeback_Reversal_Fee!=='null') && (feeObject[i].Chargeback_Reversal_Fee!=='')){Chargeback_Reversal_Fee = feeObject[i].Chargeback_Reversal_Fee;}
                if ((feeObject[i].Refund_Fee!=='null') && (feeObject[i].Refund_Fee!=='')){Refund_Fee = feeObject[i].Refund_Fee;}
                if ((feeObject[i].Credit_Fee!=='null') && (feeObject[i].Credit_Fee!=='')){Credit_Fee = feeObject[i].Credit_Fee;}
                if ((feeObject[i].Declined_Fee!=='null') && (feeObject[i].Declined_Fee!=='')){Declined_Fee = feeObject[i].Declined_Fee;}
                if ((feeObject[i].Ach_Reject_Fee!=='null') && (feeObject[i].Ach_Reject_Fee!=='')){Ach_Reject_Fee = feeObject[i].Ach_Reject_Fee;}
            }
            counter+=1;
            if (parseInt(counter)===parseInt(Object.keys(feeObject).length)){
                if (existMojo){
                    var counter2 = 0;
                    for (var j = 0; j < Object.keys(feeObject).length; j++) {
                        if (feeObject[j].Product_ID===2){
                            if ((feeObject[j].Discount_Rate!=='null') && (feeObject[j].Discount_Rate!=='')){Discount_Rate2 = parseFloat(feeObject[j].Discount_Rate);}
                            if ((feeObject[j].Monthly_Fee!=='null') && (feeObject[j].Monthly_Fee!=='')){Monthly_Fee2 = feeObject[j].Monthly_Fee;}
                            if ((feeObject[j].Setup_Fee!=='null') && (feeObject[j].Setup_Fee!=='')){Setup_Fee2 = feeObject[j].Setup_Fee;}
                            if ((feeObject[j].Authorization_Fee!=='null') && (feeObject[j].Authorization_Fee!=='')){Authorization_Fee2 = feeObject[j].Authorization_Fee;}
                            if ((feeObject[j].Chargeback_Fee!=='null') && (feeObject[j].Chargeback_Fee!=='')){Chargeback_Fee2 = feeObject[j].Chargeback_Fee;}
                            if ((feeObject[j].Retrieval_Fee!=='null') && (feeObject[j].Retrieval_Fee!=='')){Retrieval_Fee2 = feeObject[j].Retrieval_Fee;}
                            if ((feeObject[j].Sale_Fee!=='null') && (feeObject[j].Sale_Fee!=='')){Sale_Fee2 = feeObject[j].Sale_Fee;}
                            if ((feeObject[j].Capture_Fee!=='null') && (feeObject[j].Capture_Fee!=='')){Capture_Fee2 = feeObject[j].Capture_Fee;}
                            if ((feeObject[j].Void_Fee!=='null') && (feeObject[j].Void_Fee!=='')){Void_Fee2 = feeObject[j].Void_Fee;}
                            if ((feeObject[j].Chargeback_Reversal_Fee!=='null') && (feeObject[j].Chargeback_Reversal_Fee!=='')){Chargeback_Reversal_Fee2 = feeObject[j].Chargeback_Reversal_Fee;}
                            if ((feeObject[j].Refund_Fee!=='null') && (feeObject[j].Refund_Fee!=='')){Refund_Fee2 = feeObject[j].Refund_Fee;}
                            if ((feeObject[j].Credit_Fee!=='null') && (feeObject[j].Credit_Fee!=='')){Credit_Fee2 = feeObject[j].Credit_Fee;}
                            if ((feeObject[j].Declined_Fee!=='null') && (feeObject[j].Declined_Fee!=='')){Declined_Fee2 = feeObject[j].Declined_Fee;}
                            if ((feeObject[j].Ach_Reject_Fee!=='null') && (feeObject[j].Ach_Reject_Fee!=='')){Ach_Reject_Fee2 = feeObject[j].Ach_Reject_Fee;}

                            Discount_Rate3 = parseFloat(Discount_Rate) + parseFloat(Discount_Rate2);
                            Monthly_Fee3 = parseFloat(Monthly_Fee) + parseFloat(Monthly_Fee2);
                            Setup_Fee3 = parseFloat(Setup_Fee) + parseFloat(Setup_Fee2);
                            Authorization_Fee3 = parseFloat(Authorization_Fee) + parseFloat(Authorization_Fee2);
                            Chargeback_Fee3 = parseFloat(Chargeback_Fee) + parseFloat(Chargeback_Fee2);
                            Retrieval_Fee3 = parseFloat(Retrieval_Fee) + parseFloat(Retrieval_Fee2);
                            Sale_Fee3 = parseFloat(Sale_Fee) + parseFloat(Sale_Fee2);
                            Capture_Fee3 = parseFloat(Capture_Fee) + parseFloat(Capture_Fee2);
                            Void_Fee3 = parseFloat(Void_Fee) + parseFloat(Void_Fee2);
                            Chargeback_Reversal_Fee3 = parseFloat(Chargeback_Reversal_Fee) + parseFloat(Chargeback_Reversal_Fee2);
                            Refund_Fee3 = parseFloat(Refund_Fee) + parseFloat(Refund_Fee2);
                            Credit_Fee3 = parseFloat(Credit_Fee) + parseFloat(Credit_Fee2);
                            Declined_Fee3 = parseFloat(Declined_Fee) + parseFloat(Declined_Fee2);
                            Ach_Reject_Fee3 = parseFloat(Ach_Reject_Fee) + parseFloat(Ach_Reject_Fee2);

                            feeObject[j].Discount_Rate = Discount_Rate3;
                            feeObject[j].Monthly_Fee = Monthly_Fee3;
                            feeObject[j].Setup_Fee = Setup_Fee3;
                            feeObject[j].Authorization_Fee = Authorization_Fee3;
                            feeObject[j].Chargeback_Fee = Chargeback_Fee3;
                            feeObject[j].Retrieval_Fee = Retrieval_Fee3;
                            feeObject[j].Sale_Fee = Sale_Fee3;
                            feeObject[j].Capture_Fee = Capture_Fee3;
                            feeObject[j].Void_Fee = Void_Fee3;
                            feeObject[j].Chargeback_Reversal_Fee = Chargeback_Reversal_Fee3;
                            feeObject[j].Refund_Fee = Refund_Fee3;
                            feeObject[j].Credit_Fee = Credit_Fee3;
                            feeObject[j].Declined_Fee = Declined_Fee3;
                            feeObject[j].Ach_Reject_Fee = Ach_Reject_Fee3;

                        }
                        counter2+=1;
                        if (parseInt(counter2)===parseInt(Object.keys(feeObject).length)){
                            deferred.resolve(feeObject);
                        }
                    }
                }else{
                    existMojo = false;
                    deferred.resolve(feeObject);
                }
            }
        }
        return deferred.promise;
    }


    /**
     * Prepares fees to be used in the front end
     * @param boarding_id
     * @param product
     * @param feesObject
     * @returns {*}
     */
    function sumFees (boarding_id, product_id, feesObject){
        var deferred = Q.defer();
        db.Payment_Plan.find({ where: {Boarding_ID: boarding_id, Product_ID:product_id,Is_Active:1}}).success(function(paymentplanMojoResults){
            if(!paymentplanMojoResults) {
                deferred.resolve({Response:'error'});
            } else {
                var paymentplanMojo = paymentplanMojoResults.dataValues;
                /**
                 * Parse Paymentplans when product = tw2pay
                 */
                if (paymentplanMojo.Authorization_Fee === null)
                    paymentplanMojo.Authorization_Fee = 0;

                if (paymentplanMojo.Chargeback_Fee === null)
                    paymentplanMojo.Chargeback_Fee = 0;

                if (paymentplanMojo.Retrieval_Fee === null)
                    paymentplanMojo.Retrieval_Fee = 0;

                if (paymentplanMojo.Sale_Fee === null)
                    paymentplanMojo.Sale_Fee = 0;

                if (paymentplanMojo.Capture_Fee === null)
                    paymentplanMojo.Capture_Fee = 0;

                if (paymentplanMojo.Void_Fee === null)
                    paymentplanMojo.Void_Fee = 0;

                if (paymentplanMojo.Chargeback_Reversal_Fee === null)
                    paymentplanMojo.Chargeback_Reversal_Fee = 0;

                if (paymentplanMojo.Refund_Fee === null)
                    paymentplanMojo.Refund_Fee = 0;

                if (paymentplanMojo.Credit_Fee === null)
                    paymentplanMojo.Credit_Fee = 0;

                if (paymentplanMojo.Declined_Fee === null)
                    paymentplanMojo.Declined_Fee = 0;

                //

                if (paymentplanMojo.Discount_Rate === null)
                    paymentplanMojo.Discount_Rate = 0;

                if (paymentplanMojo.Monthly_Fee === null)
                    paymentplanMojo.Monthly_Fee = 0;

                if (paymentplanMojo.Setup_Fee === null)
                    paymentplanMojo.Setup_Fee = 0;

                if (paymentplanMojo.Ach_Reject_Fee === null)
                    paymentplanMojo.Ach_Reject_Fee = 0;


                /**
                 * Parse Paymentplans when product = tw2pay
                 */
                if (feesObject.Authorization_Fee < 0)
                    feesObject.Authorization_Fee = 0;

                if (feesObject.Chargeback_Fee < 0)
                    feesObject.Chargeback_Fee = 0;

                if (feesObject.Retrieval_Fee < 0)
                    feesObject.Retrieval_Fee = 0;

                if (feesObject.Sale_Fee === null)
                    feesObject.Sale_Fee = 0;

                if (feesObject.Capture_Fee === null)
                    feesObject.Capture_Fee = 0;

                if (feesObject.Void_Fee === null)
                    feesObject.Void_Fee = 0;

                if (feesObject.Chargeback_Reversal_Fee === null)
                    feesObject.Chargeback_Reversal_Fee = 0;

                if (feesObject.Refund_Fee === null)
                    feesObject.Refund_Fee = 0;

                if (feesObject.Credit_Fee === null)
                    feesObject.Credit_Fee = 0;

                if (feesObject.Declined_Fee === null)
                    feesObject.Declined_Fee = 0;

                //

                if (feesObject.Discount_Rate === null)
                    feesObject.Discount_Rate = 0;

                if (feesObject.Monthly_Fee === null)
                    feesObject.Monthly_Fee = 0;

                if (feesObject.Setup_Fee === null)
                    feesObject.Setup_Fee = 0;

                if (feesObject.Ach_Reject_Fee === null)
                    feesObject.Ach_Reject_Fee = 0;



                /**
                 * SUM[mojopay+tw2pay]
                 */
                //paymentplanMojo.Discount_Rate = feesObject.Discount_Rate;
                paymentplanMojo.id = feesObject.id;
                //paymentplanMojo.Monthly_Fee = feesObject.Monthly_Fee;
                //paymentplanMojo.Setup_Fee = feesObject.Setup_Fee;
                paymentplanMojo.Processor_ID = feesObject.Processor_ID;
                paymentplanMojo.Product_ID = feesObject.Product_ID;
                paymentplanMojo.Username = feesObject.Username;
                paymentplanMojo.Password = feesObject.Password;
                paymentplanMojo.Authorization_Fee = parseFloat(feesObject.Authorization_Fee) + parseFloat(paymentplanMojo.Authorization_Fee);
                paymentplanMojo.Chargeback_Fee = parseFloat(feesObject.Chargeback_Fee) + parseFloat(paymentplanMojo.Chargeback_Fee);
                paymentplanMojo.Retrieval_Fee = parseFloat(feesObject.Retrieval_Fee) + parseFloat(paymentplanMojo.Retrieval_Fee);
                paymentplanMojo.Sale_Fee = parseFloat(feesObject.Sale_Fee) + parseFloat(paymentplanMojo.Sale_Fee);
                paymentplanMojo.Capture_Fee = parseFloat(feesObject.Capture_Fee) + parseFloat(paymentplanMojo.Capture_Fee);
                paymentplanMojo.Void_Fee = parseFloat(feesObject.Void_Fee) + parseFloat(paymentplanMojo.Void_Fee);
                paymentplanMojo.Chargeback_Reversal_Fee = parseFloat(feesObject.Chargeback_Reversal_Fee) + parseFloat(paymentplanMojo.Chargeback_Reversal_Fee);
                paymentplanMojo.Refund_Fee = parseFloat(feesObject.Refund_Fee) + parseFloat(paymentplanMojo.Refund_Fee);
                paymentplanMojo.Credit_Fee = parseFloat(feesObject.Credit_Fee) + parseFloat(paymentplanMojo.Credit_Fee);
                paymentplanMojo.Declined_Fee = parseFloat(feesObject.Declined_Fee) + parseFloat(paymentplanMojo.Declined_Fee);
                //
                paymentplanMojo.Discount_Rate = parseFloat(feesObject.Discount_Rate) + parseFloat(paymentplanMojo.Discount_Rate);
                paymentplanMojo.Monthly_Fee = parseFloat(feesObject.Monthly_Fee) + parseFloat(paymentplanMojo.Monthly_Fee);
                paymentplanMojo.Setup_Fee = parseFloat(feesObject.Setup_Fee) + parseFloat(paymentplanMojo.Setup_Fee);
                paymentplanMojo.Ach_Reject_Fee = parseFloat(feesObject.Ach_Reject_Fee) + parseFloat(paymentplanMojo.Ach_Reject_Fee);


                deferred.resolve(paymentplanMojo);
            }
        }).error(function(err){
//          return res.jsonp({Response:err});  JIRA MC-393 res not defined
            deferred.resolve({Response:'error'});
        });
        return deferred.promise;
    }

    function buildTransactcionHistory(trHist) {
        var deferred = Q.defer();
        db.Boarding.find({ where: {id: trHist.boardingId}}).success(function(boarding){
            var transactionHistory = {
                transaction_id: trHist.transaction_id.$t === undefined ? '-' : trHist.transaction_id.$t,
                partial_payment_id: trHist.partial_payment_id.$t === undefined ? '-' : trHist.partial_payment_id.$t,
                partial_payment_balance: trHist.partial_payment_balance.$t === undefined ? '-' : trHist.partial_payment_balance.$t,
                platform_id: trHist.platform_id.$t === undefined ? '-' : trHist.platform_id.$t,
                transaction_type: trHist.transaction_type.$t === undefined ? '-' : trHist.transaction_type.$t,
                transaction_condition: trHist.condition.$t === undefined ? '-' : trHist.condition.$t,
                order_id: trHist.order_id.$t === undefined ? '-' : trHist.order_id.$t,
                authorization_code: trHist.authorization_code.$t === undefined ? '-' : trHist.authorization_code.$t,
                ponumber: trHist.ponumber.$t === undefined ? '-' : trHist.ponumber.$t,
                order_description: trHist.order_description.$t === undefined ? '-' : trHist.order_description.$t,
                first_name: trHist.first_name.$t === undefined ? '-' : trHist.first_name.$t,
                last_name: trHist.last_name.$t === undefined ? '-' : trHist.last_name.$t,
                address_1: trHist.address_1.$t === undefined ? '-' : trHist.address_1.$t,
                address_2: trHist.address_2.$t === undefined ? '-' : trHist.address_2.$t,
                company: trHist.company.$t === undefined ? '-' : trHist.company.$t,
                city: trHist.city.$t === undefined ? '-' : trHist.city.$t,
                state: trHist.state.$t === undefined ? '-' : trHist.state.$t,
                postal_code: trHist.postal_code.$t === undefined ? '-' : trHist.postal_code.$t,
                country: trHist.country.$t === undefined ? '-' : trHist.country.$t,
                email: trHist.email.$t === undefined ? '-' : trHist.email.$t,
                phone: trHist.phone.$t === undefined ? '-' : trHist.phone.$t,
                fax: trHist.fax.$t === undefined ? '-' : trHist.fax.$t,
                cell_phone: trHist.cell_phone.$t === undefined ? '-' : trHist.cell_phone.$t,
                customertaxid: trHist.customertaxid.$t === undefined ? '-' : trHist.customertaxid.$t,
                customerid: trHist.customerid.$t === undefined ? '-' : trHist.customerid.$t,
                website: trHist.website.$t === undefined ? '-' : trHist.website.$t,
                shipping_first_name: trHist.shipping_first_name.$t === undefined ? '-' : trHist.shipping_first_name.$t,
                shipping_last_name: trHist.shipping_last_name.$t === undefined ? '-' : trHist.shipping_last_name.$t,
                shipping_address_1: trHist.shipping_address_1.$t === undefined ? '-' : trHist.shipping_address_1.$t,
                shipping_address_2: trHist.shipping_address_2.$t === undefined ? '-' : trHist.shipping_address_2.$t,
                shipping_company: trHist.shipping_company.$t === undefined ? '-' : trHist.shipping_company.$t,
                shipping_city: trHist.shipping_city.$t === undefined ? '-' : trHist.shipping_city.$t,
                shipping_state: trHist.shipping_state.$t === undefined ? '-' : trHist.shipping_state.$t,
                shipping_postal_code: trHist.shipping_postal_code.$t === undefined ? '-' : trHist.shipping_postal_code.$t,
                shipping_country: trHist.shipping_country.$t === undefined ? '-' : trHist.shipping_country.$t,
                shipping_email: trHist.shipping_email.$t === undefined ? '-' : trHist.shipping_email.$t,
                shipping_carrier: trHist.shipping_carrier.$t === undefined ? '-' : trHist.shipping_carrier.$t,
                tracking_number: trHist.tracking_number.$t === undefined ? '-' : trHist.tracking_number.$t,
                shipping_date: trHist.shipping_date.$t === undefined ? '-' : trHist.shipping_date.$t,
                Shipping: trHist.shipping.$t === undefined ? '-' : trHist.shipping.$t,
                shipping_phone: trHist.shipping_phone.$t === undefined ? '-' : trHist.shipping_phone.$t,
                cc_number: trHist.cc_number.$t === undefined ? '-' : trHist.cc_number.$t,
                cc_hash: trHist.cc_hash.$t === undefined ? '-' : trHist.cc_hash.$t,
                cc_exp: trHist.cc_exp.$t === undefined ? '-' : trHist.cc_exp.$t,
                cavv: trHist.cavv.$t === undefined ? '-' : trHist.cavv.$t,
                cavv_result: trHist.cavv_result.$t === undefined ? '-' : trHist.cavv_result.$t,
                xid: trHist.xid.$t === undefined ? '-' : trHist.xid.$t,
                avs_response: trHist.avs_response.$t === undefined ? '-' : trHist.avs_response.$t,
                csc_response: trHist.csc_response.$t === undefined ? '-' : trHist.csc_response.$t,
                cardholder_auth: trHist.cardholder_auth.$t === undefined ? '-' : trHist.cardholder_auth.$t,
                cc_start_date: trHist.cc_start_date.$t === undefined ? '-' : trHist.cc_start_date.$t,
                cc_issue_number: trHist.cc_issue_number.$t === undefined ? '-' : trHist.cc_issue_number.$t,
                check_account: trHist.check_account.$t === undefined ? '-' : trHist.check_account.$t,
                check_hash: trHist.check_hash.$t === undefined ? '-' : trHist.check_hash.$t,
                check_aba: trHist.check_aba.$t === undefined ? '-' : trHist.check_aba.$t,
                check_name: trHist.check_name.$t === undefined ? '-' : trHist.check_name.$t,
                account_holder_type: trHist.account_holder_type.$t === undefined ? '-' : trHist.account_holder_type.$t,
                account_type: trHist.account_type.$t === undefined ? '-' : trHist.account_type.$t,
                sec_code: trHist.sec_code.$t === undefined ? '-' : trHist.sec_code.$t,
                drivers_license_number: trHist.drivers_license_number.$t === undefined ? '-' : trHist.drivers_license_number.$t,
                drivers_license_state: trHist.drivers_license_state.$t === undefined ? '-' : trHist.drivers_license_state.$t,
                drivers_license_dob: trHist.drivers_license_dob.$t === undefined ? '-' : trHist.drivers_license_dob.$t,
                social_security_number: trHist.social_security_number.$t === undefined ? '-' : trHist.social_security_number.$t,
                processor_id: trHist.processor_id.$t === undefined ? '-' : trHist.processor_id.$t,
                tax: trHist.tax.$t === undefined ? '-' : trHist.tax.$t,
                currency: trHist.currency.$t === undefined ? '-' : trHist.currency.$t,
                surcharge: trHist.surcharge.$t === undefined ? '-' : trHist.surcharge.$t,
                tip: trHist.tip.$t === undefined ? '-' : trHist.tip.$t,
                card_balance: trHist.card_balance.$t === undefined ? '-' : trHist.card_balance.$t,
                card_available_balance: trHist.card_available_balance.$t === undefined ? '-' : trHist.card_available_balance.$t,
                entry_mode: trHist.entry_mode.$t === undefined ? '-' : trHist.entry_mode.$t,
                cc_bin: trHist.cc_bin.$t === undefined ? '-' : trHist.cc_bin.$t,
                // merchantId: trHist.boardingId
                merchantId: boarding.dataValues.Merchant_ID
            };
            
            if (trHist.action.length !== undefined && trHist.action.length >= 1 ) {
                // action is an array - a vault/capture event
                var index = trHist.action.length - 1;
                transactionHistory.amount = trHist.action[index].amount.$t === undefined ? '-' : trHist.action[index].amount.$t;
                transactionHistory.action_type = trHist.action[index].action_type.$t === undefined ? '-' : trHist.action[index].action_type.$t;
                transactionHistory.date = trHist.action[index].date.$t === undefined ? '-' : trHist.action[index].date.$t;
                transactionHistory.success = trHist.action[index].success.$t === undefined ? '-' : trHist.action[index].success.$t;
                transactionHistory.ip_address = trHist.action[index].ip_address.$t === undefined ? '-' : trHist.action[index].ip_address.$t;
                transactionHistory.source = trHist.action[index].source.$t === undefined ? '-' : trHist.action[index].source.$t;
                transactionHistory.username = trHist.action[index].username.$t === undefined ? '-' : trHist.action[index].username.$t;
                transactionHistory.response_text = trHist.action[index].response_text.$t === undefined ? '-' : trHist.action[index].response_text.$t;
                transactionHistory.batch_id = trHist.action[index].batch_id.$t === undefined ? '-' : trHist.action[index].batch_id.$t;
                transactionHistory.processor_batch_id = trHist.action[index].processor_batch_id.$t === undefined ? '-' : trHist.action[index].processor_batch_id.$t;
                transactionHistory.response_code = trHist.action[index].response_code.$t === undefined ? '-' : trHist.action[index].response_code.$t;
                transactionHistory.processor_response_text = trHist.action[index].processor_response_text.$t === undefined ? '-' : trHist.action[index].processor_response_text.$t;
                transactionHistory.processor_response_code = trHist.action[index].processor_response_code.$t === undefined ? '-' : trHist.action[index].processor_response_code.$t;
                transactionHistory.device_license_number = trHist.action[index].device_license_number.$t === undefined ? '-' : trHist.action[index].device_license_number.$t;
                transactionHistory.device_nickname = trHist.action[index].device_nickname.$t === undefined ? '-' : trHist.action[index].device_nickname.$t;
            } else {
                // non array - not a vault/capture event
                transactionHistory.amount = trHist.action.amount.$t === undefined ? '-' : trHist.action.amount.$t;
                transactionHistory.action_type = trHist.action.action_type.$t === undefined ? '-' : trHist.action.action_type.$t;
                transactionHistory.date = trHist.action.date.$t === undefined ? '-' : trHist.action.date.$t;
                transactionHistory.success = trHist.action.success.$t === undefined ? '-' : trHist.action.success.$t;
                transactionHistory.ip_address = trHist.action.ip_address.$t === undefined ? '-' : trHist.action.ip_address.$t;
                transactionHistory.source = trHist.action.source.$t === undefined ? '-' : trHist.action.source.$t;
                transactionHistory.username = trHist.action.username.$t === undefined ? '-' : trHist.action.username.$t;
                transactionHistory.response_text = trHist.action.response_text.$t === undefined ? '-' : trHist.action.response_text.$t;
                transactionHistory.batch_id = trHist.action.batch_id.$t === undefined ? '-' : trHist.action.batch_id.$t;
                transactionHistory.processor_batch_id = trHist.action.processor_batch_id.$t === undefined ? '-' : trHist.action.processor_batch_id.$t;
                transactionHistory.response_code = trHist.action.response_code.$t === undefined ? '-' : trHist.action.response_code.$t;
                transactionHistory.processor_response_text = trHist.action.processor_response_text.$t === undefined ? '-' : trHist.action.processor_response_text.$t;
                transactionHistory.processor_response_code = trHist.action.processor_response_code.$t === undefined ? '-' : trHist.action.processor_response_code.$t;
                transactionHistory.device_license_number = trHist.action.device_license_number.$t === undefined ? '-' : trHist.action.device_license_number.$t;
                transactionHistory.device_nickname = trHist.action.device_nickname.$t === undefined ? '-' : trHist.action.device_nickname.$t;
            }
            
            if (trHist.original_transaction_id !== undefined) {
                transactionHistory.Original_Transaction_ID = trHist.original_transaction_id.$t === undefined ? '-' : trHist.original_transaction_id.$t;
            } else {
                transactionHistory.Original_Transaction_ID = '-';
            }
        
             deferred.resolve(transactionHistory);
        });

     return deferred.promise;
     }


    function email_failed_insert(transaction_id) {
        var smtpTransport = nodemailer.createTransport(config.mailer.options);
        var today = new Date();
        var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        // mail to a single address
        var mailOptions = {
            from: config.mailer.from,
            to: 'techops@total-apps.com', 
            subject: 'Error downloading transaction from NMI',
            html: 'Dearest NMI,<br><br>I write to you on this ' + weekday[today.getDay()] + ', ' + month[today.getMonth()] + ' ' + today.getDate() + ', to inquire as to the status of a transaction you may be familar with. That transaction goes by the name of ' + transaction_id + ', perhaps you have witnessed them. If you could perchance provide me with details such as the exact time of said transaction, and the card by which it was conducted, it would be most kind of you.<br><br>Respectfully yours,<br><br>MojoPay.'
        };
        smtpTransport.sendMail(mailOptions, function(err) {
    
            if(err){
                console.log(err);
            }else{
//                console.log("Message sent");
            }
    
        }); // smtpTransport.sendMail
    }


    function insertIntoTransactionHistory(req, res, user, pass, transaction_id, boardingId, emailParameters) {
//console.log('=====================', user, pass, transaction_id, boardingId, emailParameters)
        if (transaction_id !== undefined && transaction_id !== null && transaction_id !== '') {
            var url = config.api_url.root_query + '?username=' + user + '&password=' + pass + '&transaction_id=' + transaction_id;
            var tr_hist_insert, tr_hist_insert1,tr_hist_insert2; 
//console.log(url)
            // try this 5 times
            request(url, function (error, response, trHist) {
//console.log('+++++0+++++',trHist)
                 if (!error && response.statusCode === 200) {
                    var json = XMLMapping.load(trHist).nm_response.transaction;
                    if (json !== undefined && json.transaction_id !== undefined && json.transaction_id !== '') {
//console.log('-----1-----',json.action,json.action.length)
                        json.boardingId = boardingId;
                        if (json.action.length !== undefined && json.action.length >= 1 ) {
                            var index = json.action.length - 1;
                            if (json.action[index].response_code.$t !== undefined && json.action[index].response_code.$t !== null && json.action[index].response_code.$t !== '') {
                                // put the transaction data into the Transaction_History table
                                tr_hist_insert1 = buildTransactcionHistory(json).then(function(tr_hist_insert) {
//console.log('=====3======',tr_hist_insert)
                                    db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                         sendEmail(req, res, emailParameters, transaction_history);
                                     }).error(function(err){
                                        tr_hist_insert = {transaction_id: transaction_id};
                                        db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                            sendEmail(req, res, emailParameters, transaction_history);
                                        }).error(function(err){
                                            console.log(err);
                                        });
                                        email_failed_insert(transaction_id);
                                        console.log(error); 
                                    });
                                }); // tr_hist_insert = buildTransactcionHistory(json).then(function()                                         email_failed_insert(transaction_id);

                            } else {
                                tr_hist_insert = {transaction_id: transaction_id};
                                db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                    sendEmail(req, res, emailParameters, transaction_history);
                                }).error(function(err){
                                    console.log(err);
                                });
                                email_failed_insert(transaction_id);
                                console.log(error);
                            } // if (json.response_code === '100')
                        } else { // action is NOT an array (capture)
                            if (json.action.response_code.$t !== undefined && json.action.response_code.$t !== null && json.action.response_code.$t !== '') {
//console.log('-----999-----',json)
                                // put the transaction data into the Transaction_History table
                                tr_hist_insert2 = buildTransactcionHistory(json).then( function(tr_hist_insert) {
//console.log('=====2======',tr_hist_insert)
                                    db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                        sendEmail(req, res, emailParameters, transaction_history);
                                    }).error(function(err){
                                        tr_hist_insert = {transaction_id: transaction_id};
                                        db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                            sendEmail(req, res, emailParameters, transaction_history);
                                        }).error(function(err){
                                            console.log(err);
                                        });
                                        email_failed_insert(transaction_id);
                                        console.log(error);
                                    });
                                }); // tr_hist_insert2 = buildTransactcionHistory(json).then( function(tr_hist_insert)
                            } else {
                                tr_hist_insert = {transaction_id: transaction_id};
                                db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                    sendEmail(req, res, emailParameters, transaction_history);
                                }).error(function(err){
                                    console.log(err);
                                });
                                email_failed_insert(transaction_id);
                                console.log(error);
                            } // if (json.response_code === '100')
                        }
                    } else {
                        tr_hist_insert = {transaction_id: transaction_id};
                        db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                            sendEmail(req, res, emailParameters, transaction_history);
                        }).error(function(err){
                            console.log(err);
                        });
                        email_failed_insert(transaction_id);
                        console.log(error);
                    } // if (json.transactionid !== undefined && json.transactionid !== '')
                } // if (!error && response.statusCode === 200) 
            }); // request(url, function (error, response, trHist)
        } // if (transaction_id !== undefined && transaction_id !== null && transaction_id !== '') 
    }


exports.getReserve = function(req,res){
    var proccesorid = req.param('processorId');
    db.sequelize.query('Select Sum(Adjusted_Amount) as amount From MAS Where Type = "RESERVE" AND Processor_ID = :processor',null,{ raw: true },
        { processor: proccesorid }
    ).then(function(results) {
            res.jsonp({Response:results});
        });

};

exports.duplicateProcessor = function (req,res){
    db.sequelize.query(' SELECT Processor_ID, COUNT(*) c FROM Payment_Plans GROUP BY Processor_ID HAVING c > 1;').spread(function(results, metadata) {
     res.jsonp({Response:results});
     });
};
/**
 * Builds the NMI Calls
 * @param req
 * @param res
 */
exports.transactions = function(req, res) {
    var id='',
    role=req.user.Roles;
    GlobalBoardingId = req.param('boardingId');
    id = req.param('processorId');
    var isAPI = true;
    getCredentials(id,GlobalBoardingId,role).then(
        function(results){
            var transactionType = req.param('type'),
                arg = {},
                url = '',
                xmlfile=false;
            var thisAmount = 0,
                thisTax = 0,
                thisShipping = 0;

            switch(transactionType) {
                case 'sale':   
                    xmlfile=false;
                    url = config.api_url.root_transact; 
                    arg = {
                            type: req.param('type'),
                            ccnumber: req.param('creditCardNumber'),
                            ccexp: req.param('expirationDate'),
                            amount: req.param('amount'),
                            currency: req.param('currency'),
                            cvv: req.param('cardSecurityCode'),
                            orderid: req.param('orderId'),
                            orderdescription: req.param('orderDescription'),
                            ponumber: req.param('poNumber'),
                            shipping: req.param('shipping'),
                            tax: req.param('tax'),
                            firstname: req.param('firstNameCard'),
                            lastname: req.param('lastNameCard'),
                            company: req.param('companyCard'),
                            country: req.param('countryCard'),
                            address1: req.param('addressCard'),
                            address2: req.param('addressContCard'),
                            city: req.param('cityCard'),
                            state: req.param('stateProvinceCard'),
                            zip: req.param('zipPostalCodeCard'),
                            phone: req.param('phoneNumberCard'),
                            fax: req.param('faxNumberCard'),
                            email: req.param('emailAddressCard'),
                            shipping_firstname: req.param('firstNameShipping'),
                            shipping_lastname: req.param('lastNameShipping'),
                            shipping_company: req.param('companyShipping'),
                            shipping_address1: req.param('addressShipping'),
                            shipping_address2: req.param('addressContShipping'),
                            shipping_city: req.param('cityShipping'),
                            shipping_state: req.param('stateProvinceShipping'),
                            shipping_zip: req.param('zipPostalCodeShipping'),
                            shipping_country: req.param('countryShipping'),
                            shipping_email: req.param('emailAddressShipping'),
                            customer_vault:req.param('customerVault'),
                            customer_vault_id: req.param('customerVaultId'),
                            merchant_defined_field_1 : req.param('merchantHash'),
                            merchant_defined_field_2 : req.param('referralHash'),
                            merchant_defined_field_3 : req.param('referralProcessor'),
                            //customer_receipt:req.param('customerReceipt')
                        };    
                        if (role==='user'){
                            //customer_receipt:req.param('customerReceipt')
                        }  
                        emailParameters.send_mojopay_receipt = req.param('customerReceipt');
                        emailParameters.mojo_receipt_email_address = req.param('emailAddressCard');
                        emailParameters.mojo_receipt_customer_name = req.param('firstNameCard') + ' ' + req.param('lastNameCard');
                        emailParameters.mojo_reciept_billing_address = emailParameters.mojo_receipt_customer_name + '<br>' + 
                                                       req.param('addressCard') + '<br>' +
                                                       req.param('addressContCard') + '<br>' +
                                                       req.param('cityCard') + ',' +
                                                       req.param('stateProvinceCard') + ' ' +
                                                       req.param('zipPostalCodeCard') + '<br>' +
                                                       req.param('countryCard') + '<br>' +
                                                       req.param('phoneNumberCard');
                        emailParameters.mojo_reciept_shipping_address = req.param('addressShipping') + '<br>' +
                                                        req.param('addressContShipping') + '<br>' +
                                                        req.param('cityShipping') + ',' +
                                                        req.param('stateProvinceShipping') + ' ' +
                                                        req.param('zipPostalCodeShipping') + '<br>' +
                                                        req.param('countryShipping');
                        emailParameters.mojo_receipt_card_type = req.param('creditCardNumber');
                        thisAmount = 0;
                        thisTax = 0;
                        thisShipping = 0;
                        if(req.param('amount') !== undefined && req.param('amount') !== null && req.param('amount') !== '') {
                            thisAmount = parseFloat(req.param('amount'));
                        }
                        if(req.param('tax') !== undefined && req.param('tax') !== null && req.param('tax') !== '') {
                            thisTax = parseFloat(req.param('tax'));
                        }
                        if(req.param('shipping') !== undefined && req.param('shipping') !== null && req.param('shipping') !== '') {
                            thisShipping = parseFloat(req.param('shipping'));
                        }
                        emailParameters.mojo_receipt_amount =  thisAmount + thisTax + thisShipping;
  
                       if (role==='user'){
                            arg.processor_id=credentialPro;
                        }        
                    break;
                case 'credit':   
                    xmlfile=false;
                     url = config.api_url.root_transact; 
                    arg = {
                            type: req.param('type'),
                            ccnumber: req.param('creditCardNumber'),
                            ccexp: req.param('expirationDate'),
                            amount: req.param('amount'),
                            currency: req.param('currency'),
                            cvv: req.param('cardSecurityCode'),
                            orderid: req.param('orderId'),
                            orderdescription: req.param('orderDescription'),
                            ponumber: req.param('poNumber'),
                            shipping: req.param('shipping'),
                            tax: req.param('tax'),
                            firstname: req.param('firstNameCard'),
                            lastname: req.param('lastNameCard'),
                            company: req.param('companyCard'),
                            country: req.param('countryCard'),
                            address1: req.param('addressCard'),
                            address2: req.param('addressContCard'),
                            city: req.param('cityCard'),
                            state: req.param('stateProvinceCard'),
                            zip: req.param('zipPostalCodeCard'),
                            phone: req.param('phoneNumberCard'),
                            fax: req.param('faxNumberCard'),
                            email: req.param('emailAddressCard'),
                            shipping_firstname: req.param('firstNameShipping'),
                            shipping_lastname: req.param('lastNameShipping'),
                            shipping_company: req.param('companyShipping'),
                            shipping_address1: req.param('addressShipping'),
                            shipping_address2: req.param('addressContShipping'),
                            shipping_city: req.param('cityShipping'),
                            shipping_state: req.param('stateProvinceShipping'),
                            shipping_zip: req.param('zipPostalCodeShipping'),
                            shipping_country: req.param('countryShipping'),
                            shipping_email: req.param('emailAddressShipping'),
                            customer_vault:req.param('customerVault'),
                            customer_vault_id: req.param('customerVaultId'),
                            merchant_defined_field_1 : req.param('merchantHash'),
                            merchant_defined_field_2 : req.param('referralHash'),
                            merchant_defined_field_3 : req.param('referralProcessor'),
                            //customer_receipt:req.param('customerReceipt')
                        };
                        emailParameters.send_mojopay_receipt = req.param('customerReceipt');
                        emailParameters.mojo_receipt_email_address = req.param('emailAddressCard');
                        emailParameters.mojo_receipt_customer_name = req.param('firstNameCard') + ' ' + req.param('lastNameCard');
                        emailParameters.mojo_reciept_billing_address = emailParameters.mojo_receipt_customer_name + '<br>' + 
                                                       req.param('addressCard') + '<br>' +
                                                       req.param('addressContCard') + '<br>' +
                                                       req.param('cityCard') + ',' +
                                                       req.param('stateProvinceCard') + ' ' +
                                                       req.param('zipPostalCodeCard') + '<br>' +
                                                       req.param('countryCard') + '<br>' +
                                                       req.param('phoneNumberCard');
                        emailParameters.mojo_reciept_shipping_address = req.param('addressShipping') + '<br>' +
                                                        req.param('addressContShipping') + '<br>' +
                                                        req.param('cityShipping') + ',' +
                                                        req.param('stateProvinceShipping') + ' ' +
                                                        req.param('zipPostalCodeShipping') + '<br>' +
                                                        req.param('countryShipping');
                        emailParameters.mojo_receipt_card_type = req.param('creditCardNumber');
                        thisAmount = 0;
                        thisTax = 0;
                        thisShipping = 0;
                        if(req.param('amount') !== undefined && req.param('amount') !== null && req.param('amount') !== '') {
                            thisAmount = parseFloat(req.param('amount'));
                        }
                        if(req.param('tax') !== undefined && req.param('tax') !== null && req.param('tax') !== '') {
                            thisTax = parseFloat(req.param('tax'));
                        }
                        if(req.param('shipping') !== undefined && req.param('shipping') !== null && req.param('shipping') !== '') {
                            thisShipping = parseFloat(req.param('shipping'));
                        }
                        emailParameters.mojo_receipt_amount =  thisAmount + thisTax + thisShipping;

                        if (role==='user'){
                            arg.processor_id=credentialPro;
                        }    
                    break;
                case 'auth':   
                    xmlfile=false;
                    url = config.api_url.root_transact; 
                    arg = {
                            type: req.param('type'),
                            ccnumber: req.param('creditCardNumber'),
                            ccexp: req.param('expirationDate'),
                            amount: req.param('amount'),
                            currency: req.param('currency'),
                            cvv: req.param('cardSecurityCode'),
                            orderid: req.param('orderId'),
                            orderdescription: req.param('orderDescription'),
                            ponumber: req.param('poNumber'),
                            shipping: req.param('shipping'),
                            tax: req.param('tax'),
                            firstname: req.param('firstNameCard'),
                            lastname: req.param('lastNameCard'),
                            company: req.param('companyCard'),
                            country: req.param('countryCard'),
                            address1: req.param('addressCard'),
                            address2: req.param('addressContCard'),
                            city: req.param('cityCard'),
                            state: req.param('stateProvinceCard'),
                            zip: req.param('zipPostalCodeCard'),
                            phone: req.param('phoneNumberCard'),
                            fax: req.param('faxNumberCard'),
                            email: req.param('emailAddressCard'),
                            shipping_firstname: req.param('firstNameShipping'),
                            shipping_lastname: req.param('lastNameShipping'),
                            shipping_company: req.param('companyShipping'),
                            shipping_address1: req.param('addressShipping'),
                            shipping_address2: req.param('addressContShipping'),
                            shipping_city: req.param('cityShipping'),
                            shipping_state: req.param('stateProvinceShipping'),
                            shipping_zip: req.param('zipPostalCodeShipping'),
                            shipping_country: req.param('countryShipping'),
                            shipping_email: req.param('emailAddressShipping'),
                            customer_vault:req.param('customerVault'),
                            customer_vault_id: req.param('customerVaultId'),
                            merchant_defined_field_1 : req.param('merchantHash'),
                            merchant_defined_field_2 : req.param('referralHash'),
                            merchant_defined_field_3 : req.param('referralProcessor'),
                            //customer_receipt:req.param('customerReceipt')
                        };

                        emailParameters.send_mojopay_receipt = req.param('customerReceipt');
                        emailParameters.mojo_receipt_email_address = req.param('emailAddressCard');
                        emailParameters.mojo_receipt_customer_name = req.param('firstNameCard') + ' ' + req.param('lastNameCard');
                        emailParameters.mojo_reciept_billing_address = emailParameters.mojo_receipt_customer_name + '<br>' + 
                                                       req.param('addressCard') + '<br>' +
                                                       req.param('addressContCard') + '<br>' +
                                                       req.param('cityCard') + ',' +
                                                       req.param('stateProvinceCard') + ' ' +
                                                       req.param('zipPostalCodeCard') + '<br>' +
                                                       req.param('countryCard') + '<br>' +
                                                       req.param('phoneNumberCard');
                        emailParameters.mojo_reciept_shipping_address = req.param('addressShipping') + '<br>' +
                                                        req.param('addressContShipping') + '<br>' +
                                                        req.param('cityShipping') + ',' +
                                                        req.param('stateProvinceShipping') + ' ' +
                                                        req.param('zipPostalCodeShipping') + '<br>' +
                                                        req.param('countryShipping');
                        emailParameters.mojo_receipt_card_type = req.param('creditCardNumber');
                        thisAmount = 0;
                        thisTax = 0;
                        thisShipping = 0;
                        if(req.param('amount') !== undefined && req.param('amount') !== null && req.param('amount') !== '') {
                            thisAmount = parseFloat(req.param('amount'));
                        }
                        if(req.param('tax') !== undefined && req.param('tax') !== null && req.param('tax') !== '') {
                            thisTax = parseFloat(req.param('tax'));
                        }
                        if(req.param('shipping') !== undefined && req.param('shipping') !== null && req.param('shipping') !== '') {
                            thisShipping = parseFloat(req.param('shipping'));
                        }
                        emailParameters.mojo_receipt_amount =  thisAmount + thisTax + thisShipping;
          
                        if (role==='user'){
                            arg.processor_id=credentialPro;
                        }    
                    break;
                case 'create': 
//console.log('create cust vault in server')  
                    xmlfile=false;
                    url = config.api_url.root_transact; 
                    arg = {
                        customer_vault:req.param('customerVault'),
                        customer_vault_id : req.param('customerVaultId'),
                        //billing_id : req.param('billingId'),
                        ccnumber: req.param('creditCardNumber'),
                        ccexp: req.param('expirationDate'),
                        currency: req.param('currency'),
                        first_name  : req.param('firstNameCard'),
                        last_name : req.param('lastNameCard'),
                        company : req.param('companyCard'),
                        country : req.param('countryCard'),
                        address1 : req.param('addressCard'),
                        address2 : req.param('addressContCard'),
                        city : req.param('cityCard'),
                        state : req.param('stateProvinceCard'),
                        zip: req.param('zipPostalCodeCard'),
                        phone : req.param('phoneNumberCard'),
                        fax : req.param('faxNumberCard'),
                        email : req.param('emailAddressCard'),
                        //shipping_id : req.param('shippingId'),
                        shipping_firstname  : req.param('firstNameShipping'),
                        shipping_lastname : req.param('lastNameShipping'),
                        shipping_company : req.param('companyShipping'),
                        shipping_country : req.param('countryShipping'),
                        shipping_address1 : req.param('addressShipping'),
                        shipping_address2 : req.param('addressContShipping'),
                        shipping_city : req.param('cityShipping'),
                        shipping_state : req.param('stateProvinceShipping'),
                        shipping_zip: req.param('zipPostalCodeShipping'),
                        shipping_phone : req.param('phoneNumberShipping'),
                        shipping_fax : req.param('faxNumberShipping'),
                        shipping_email : req.param('emailAddressShipping')
                    };
                    break;
                case 'custvaultupdate':   
//console.log('update cust vault in server')  
                    xmlfile=false;
                     url = config.api_url.root_transact; 
                    arg = {
                        customer_vault:req.param('customerVault'),
                        customer_vault_id : req.param('customerVaultId'),
                        //billing_id : req.param('billingId'),
                        //ccnumber: req.param('creditCardNumber'),
                        ccexp: req.param('expirationDate'),
                        currency: req.param('currency'),
                        first_name  : req.param('firstNameCard'),
                        last_name : req.param('lastNameCard'),
                        company : req.param('companyCard'),
                        country : req.param('countryCard'),
                        address1 : req.param('addressCard'),
                        address2 : req.param('addressContCard'),
                        city : req.param('cityCard'),
                        state : req.param('stateProvinceCard'),
                        zip: req.param('zipPostalCodeCard'),
                        phone : req.param('phoneNumberCard'),
                        fax : req.param('faxNumberCard'),
                        email : req.param('emailAddressCard'),
                        //shipping_id : req.param('shippingId'),
                        shipping_firstname  : req.param('firstNameShipping'),
                        shipping_lastname : req.param('lastNameShipping'),
                        shipping_company : req.param('companyShipping'),
                        shipping_country : req.param('countryShipping'),
                        shipping_address1 : req.param('addressShipping'),
                        shipping_address2 : req.param('addressContShipping'),
                        shipping_city : req.param('cityShipping'),
                        shipping_state : req.param('stateProvinceShipping'),
                        shipping_zip: req.param('zipPostalCodeShipping'),
                        shipping_phone : req.param('phoneNumberShipping'),
                        shipping_fax : req.param('faxNumberShipping'),
                        shipping_email : req.param('emailAddressShipping')
                    };
                    if(req.param('creditCardNumber').indexOf('x') === -1) {
                        arg.ccnumber = req.param('creditCardNumber');
                    }
                    break;
                case 'treport':
                    checkProcessorsRepeated().then(
                        function(processor){
                            if (processor!==''){
                                var text = '[ WARNING: not unique Processor Id detected, ' + processor + '] [' + req.user.Username + '] [' + req.ip + ']';
                                Log.debug(text);
                            }
                        },
                        function(err){
                            return res.jsonp({Response:err});
                        });

                   if ((config.transactionsComeFromNMI)||(req.param('query')==='nmi')){
                        xmlfile=true;
                        url = config.api_url.root_query;
                        arg = {
                            transaction_id:req.param('transactionId'),
                            action_type: req.param('actionType'),
                            order_id: req.param('orderId'),
                            condition: req.param('condition'),
                            last_name: req.param('lastName'),
                            customer_vault_id: req.param('customerVaultId'),
                            report_type: req.param('reportType'),
                            end_date: req.param('endDate'),
                            start_date: req.param('startDate')
                        };
                        if (role==='user'){
                            arg.processor_id=credentialPro;
                        }
                    } else {
						var argstring = '';
						arg = {};
						if (role !== 'admin' && role !== 'super') {
							argstring = 'SELECT TH.* FROM Transaction_History TH WHERE (TH.processor_id = :processor';
							arg.processor = credentialPro;
						} else {
							argstring = 'SELECT TH.*, B.Merchant_Name, B.Merchant_ID, PP.Boarding_ID ' +
										'FROM   Transaction_History TH ' +
										'JOIN   Payment_Plans PP ON PP.Processor_ID = TH.Processor_ID ' +
										'JOIN   Boardings B ON B.id = PP.Boarding_ID ' +
										'JOIN   Products P ON P.id = PP.Product_ID ' +
										'WHERE  (1';
							if ((typeof req.param('boardingId') !== 'undefined') && (req.param('boardingId') !== '') && (req.param('boardingId') !== 'all')) {
								argstring+=' AND PP.Boarding_ID = :boardingId';
								arg.boardingId = req.param('boardingId');
							}
							if ((typeof req.param('productId') !== 'undefined') && (req.param('productId') !== '') && (req.param('productId') !== 'all')) {
								argstring+=' AND P.id = :productId';
								arg.productId = req.param('productId');
							}
							if ((typeof req.param('processorId') !== 'undefined') && (req.param('processorId') !== '') && (req.param('productId') !== 'all')) {
								argstring+=' AND PP.Processor_ID = :processorId';
								arg.processorId = req.param('processorId');
							}
						}
                        isAPI = false; 

                        if (((typeof req.param('endDate')!=='undefined')&&(req.param('endDate')!=='')) && ((typeof req.param('startDate')!=='undefined')&&(req.param('startDate')!==''))){
                            argstring+=' AND TH.date <= :endDate AND TH.date >= :startDate';
                            arg.endDate = req.param('endDate');
							arg.startDate = req.param('startDate');
                        }
                        if ((typeof req.param('transactionId')!=='undefined') && (req.param('transactionId')!=='')){
                            argstring+=' AND TH.transaction_id = :transactionId';
                            arg.transactionId = req.param('transactionId');
                        }
                        if ((typeof req.param('merchantIdent') !== 'undefined') && (req.param('merchantIdent') !== '')) {
                            argstring += ' AND B.Merchant_ID LIKE :merchantIdent';
                            arg.merchantIdent = req.param('merchantIdent') + '%';
                        }
                        if ((typeof req.param('amountFrom') !== 'undefined') && (req.param('amountFrom') !== '')) {
                            argstring += ' AND CAST(TH.amount AS decimal(18,2)) >= :amountFrom';
                            arg.amountFrom = req.param('amountFrom');
                        }
                        if ((typeof req.param('amountTo') !== 'undefined') && (req.param('amountTo') !== '')) {
                            argstring += ' AND CAST(TH.amount AS decimal(18,2)) <= :amountTo';
                            arg.amountTo = req.param('amountTo');
                        }
                        if ((typeof req.param('last4ofCC') !== 'undefined') && (req.param('last4ofCC') !== '')) {
                            argstring += ' AND SUBSTR(cc_number,-4) = :last4ofCC';
                            arg.last4ofCC = req.param('last4ofCC');
                        }
                        if ((typeof req.param('actionType')!=='undefined') && (req.param('actionType')!=='')){
                            argstring+=' AND TH.action_type = :actionType';
                            arg.actionType = req.param('actionType');
                        }
                        if ((typeof req.param('orderId')!=='undefined') && (req.param('orderId')!=='')){
                            argstring+=' AND TH.order_id = :orderId';
                            arg.orderId = req.param('orderId');
                        }
                        if ((typeof req.param('firstName') !== 'undefined') && (req.param('firstName') !== '')){
                            argstring += ' AND TH.first_name = :firstName';
                            arg.firstName = req.param('firstName');
                        }
                        if ((typeof req.param('lastName')!=='undefined') && (req.param('lastName')!=='')){
                            argstring+=' AND TH.last_name = :lastName';
                            arg.lastName = req.param('lastName');
                        }
                        if ((typeof req.param('customerVaultId')!=='undefined') && (req.param('customerVaultId')!=='')){
                            argstring+=' AND TH.customerid = :customerVaultId';
                            arg.customerVaultId = req.param('customerVaultId');
                        }
                        if ((typeof req.param('reportType')!=='undefined') && (req.param('reportType')!=='')){
                            argstring+=' AND TH.reportType = :reportType';
                            arg.reportType = req.param('reportType');
                        }
                        if ((typeof req.param('condition')!=='undefined') && (req.param('condition')!=='')){
                            argstring+=' AND TH.transaction_condition = :condition';
                            arg.condition = req.param('condition');
                        }
						argstring+=')';
                        if ((typeof req.param('originalId')!=='undefined') && (req.param('originalId')!=='') && (req.param('transactionId')!=='') && (typeof req.param('transactionId')!=='undefined') ){
                            argstring+=' OR (TH.Original_transaction_id = :originalId)';
                            arg.originalId = req.param('transactionId');
                        }
                        argstring+=';';

                        db.sequelize.query(argstring,null,{ raw: true },
							arg
                       ).success(function(transactions) {
                               if (!transactions){
                                   return res.jsonp({});
                               }else{
                                   if (Object.keys(transactions).length===0){
                                       return res.jsonp({});
                                   }else{
                                       if ((typeof req.param('transactionId')!=='undefined') && (req.param('transactionId')!=='') && ((typeof req.param('originalId')==='undefined') || (req.param('originalId')===''))){
                                           db.Transaction_History.findAll({ where: ['Original_transaction_id=?',req.param('transactionId')]}).success(function(history) {
                                               if(!history){
                                                   parseLikeNMI(transactions).then(
                                                       function(transactionsParsed){
                                                           return res.jsonp(transactionsParsed);
                                                       },
                                                       function(err){
                                                           return res.jsonp({Response:err});
                                                       });
                                               }else{
                                                   //historicals go in here
                                                   parseLikeNMI(transactions,history).then(
                                                       function(transactionsParsed){
                                                           return res.jsonp(transactionsParsed);
                                                       },
                                                       function(err){
                                                           return res.jsonp({Response:err});
                                                       });
                                               }
                                           });
                                       }else{
                                           parseLikeNMI(transactions).then(
                                               function(transactionsParsed){
                                                   return res.jsonp(transactionsParsed);
                                               },
                                               function(err){
                                                   return res.jsonp({Response:err});
                                               });
                                       }
                                   }
                               }
                           }).error(function(err){
								console.log(err);
//                                return res.render('error', {
//                                   error: err,
//                                   status: 500
//                               });
                           });
                    }                
                    break;
                case 'creport':
                    xmlfile=true;
                    url = config.api_url.root_query; 
                    arg = {
                        report_type: 'customer_vault',
                        customer_vault_id: req.param('customerVaultId')
                    };
                    break;
                case 'capture':
                    xmlfile=false;
                    url = config.api_url.root_transact;                     
                    arg = {
                        type: req.param('type'),
                        amount: req.param('amount'),
                        merchant_defined_field_1 : req.param('merchantHash'),
                        merchant_defined_field_2 : req.param('referralHash'),
                        merchant_defined_field_3 : req.param('referralProcessor'),
                        transactionid: req.param('transactionId')
                    };
                    if (req.param('customerVaultId') !== undefined){
                        arg.customer_vault_id = req.param('customerVaultId');
                    }
                    break;
                case 'void':
//console.log("voiding...")
                    xmlfile=false;
                    url = config.api_url.root_transact;                             
                    arg = {
                        type: req.param('type'),
                        merchant_defined_field_1 : req.param('merchantHash'),
                        merchant_defined_field_2 : req.param('referralHash'),
                        merchant_defined_field_3 : req.param('referralProcessor'),
                        transactionid: req.param('transactionId')
                    };
                    if (req.param('customerVaultId') !== undefined){
                        arg.customer_vault_id = req.param('customerVaultId');
                    }
                    break;
                case 'refund':
                    xmlfile=false;
                    url = config.api_url.root_transact;                
                    arg = {
                        type: req.param('type'),
                        amount: req.param('amount'),
                        merchant_defined_field_1 : req.param('merchantHash'),
                        merchant_defined_field_2 : req.param('referralHash'),
                        merchant_defined_field_3 : req.param('referralProcessor'),
                        transactionid: req.param('transactionId')
                    };
                    if (req.param('customerVaultId') !== undefined){
                        arg.customer_vault_id = req.param('customerVaultId');
                    }
                    break;
                case 'generatebutton':
                    xmlfile=false;
                    url = config.api_url.root_cart;
                    arg = {
                        type: req.param('type'),
                        amount: req.param('amount'),
                        transactionid: req.param('transactionId')
                    };
                    break;
                case 'merchant_activity':
                    xmlfile=true;
                    url = config.api_url.root_query; 
                    arg = {
                        processor_id: credentialPro,
                        action_type:'sale,capture',
                        condition:'complete',
                        end_date: req.param('endDate'),
                        start_date: req.param('startDate')
                    };
                    if (role==='user'){
                        arg.processor_id=credentialPro;
                    }else{
                        isAPI = false;
                        getAllMerchants(arg, url).then(
                            function(results){
                                var boardings = convArrToObj(results);
                                res.jsonp({nm_response:boardings});
                            });
                    }
                    break;
                case 'settlement_details':
                    checkProcessorsRepeated().then(
                        function(processor){
                            if (processor!==''){
                                var text = '[ WARNING: not unique Processor Id detected, ' + processor + '] [' + req.user.Username + '] [' + req.ip + ']';
                                Log.debug(text);
                            }
                        },
                        function(err){
                            return res.jsonp({Response:err});
                        });


                    if (config.transactionsComeFromNMI){
                        xmlfile=true;
                         url = config.api_url.root_query; 
                        arg = {
                            processor_id: credentialPro,
                            action_type: req.param('action_type'),
                            condition: req.param('condition'),
                            end_date: req.param('endDate'),
                            start_date: req.param('startDate')
                        };
                        if (role==='user'){
                            arg.processor_id=credentialPro;
                        }                    
                    }else{
                        isAPI = false;
                        db.Transaction_History.findAll({ where: ['date >=? AND date <=? AND processor_id=? AND transaction_condition=? AND (action_type=? or action_type=?)',req.param('startDate'),req.param('endDate'),credentialPro,'complete','capture','sale']}).success(function(transactions){
                          if (!transactions){
                              return res.jsonp({});
                          }else{
                              if (Object.keys(transactions).length===0){
                                  return res.jsonp({});
                              }else{
                                  parseLikeNMI(transactions).then(
                                      function(transactionsParsed){
                                          return res.jsonp(transactionsParsed);
                                      },
                                      function(err){
                                          return res.jsonp({Response:err});
                                      });
                              }
                          }
                        }).error(function(err){
                            return res.render('error', {
                                error: err,
                                status: 500
                            });
                        });                        
                    }
                    break;

                case 'settlement_transaction_details':
                    checkProcessorsRepeated().then(
                        function(processor){
                            if (processor!==''){
                                var text = '[ WARNING: not unique Processor Id detected, ' + processor + '] [' + req.user.Username + '] [' + req.ip + ']';
                                Log.debug(text);
                            }
                        },
                        function(err){
                            return res.jsonp({Response:err});
                        });


                    if (config.transactionsComeFromNMI){
                        xmlfile=true;
                         url = config.api_url.root_query; 
                        arg = {
                            processor_id: credentialPro,
                            action_type:'sale,capture',
                            condition:'complete',
                            end_date: req.param('endDate'),
                            start_date: req.param('startDate')
                        };
                        if (role==='user'){
                            arg.processor_id=credentialPro;
                        }    
                    }else{
                        isAPI = false;
                        var action_type = req.param('action_type'),
                            processorgroup = req.param('processors'),
                            arrayprocessor = '';
                        if (processorgroup !== undefined){
                            arrayprocessor = processorgroup.split(",");
                        }
                        if (req.param('productId') === 'all'){
                            db.Transaction_History.findAll({ where: ['date >=? AND date <=?  AND transaction_condition=? AND (processor_id=? or processor_id=?)',req.param('startDate'),req.param('endDate'),'complete',arrayprocessor[0],arrayprocessor[1]]}).success(function(transactions){
                                if (!transactions){
                                    return res.jsonp({});
                                }
                                else {
                                    if (Object.keys(transactions).length===0){
                                        return res.jsonp({});
                                    }else{
                                        parseLikeNMI(transactions).then(
                                            function(transactionsParsed){
                                                return res.jsonp(transactionsParsed);
                                            },
                                            function(err){
                                                return res.jsonp({Response:err});
                                            });
                                    }
                                }
                            }).error(function(err){
                                return res.render('error', {
                                    error: err,
                                    status: 500
                                });
                            });
                        }else{
                            db.Transaction_History.findAll({ where: ['date >=? AND date <=?  AND transaction_condition=? AND processor_id=?',req.param('startDate'),req.param('endDate'),'complete',credentialPro]}).success(function(transactions) {
                                if (!transactions){
                                    return res.jsonp({});
                                }
                                else {
                                    if (Object.keys(transactions).length === 0) {
                                        return res.jsonp({});
                                    } else {
                                        parseLikeNMI(transactions).then(
                                            function (transactionsParsed) {
                                                return res.jsonp(transactionsParsed);
                                            },
                                            function (err) {
                                                return res.jsonp({Response: err});
                                            });
                                    }
                                }
                            }).error(function(err){
                                return res.render('error', {
                                    error: err,
                                    status: 500
                                });
                            });
                        }
                    }
                    break;
                case 'account_summary':
                    checkProcessorsRepeated().then(
                        function(processor){
                            if (processor!==''){
                                var text = '[ WARNING: not unique Processor Id detected, ' + processor + '] [' + req.user.Username + '] [' + req.ip + ']';
                                Log.debug(text);
                            }
                        },
                        function(err){
                            return res.jsonp({Response:err});
                        });


                    if (config.transactionsComeFromNMI){
                        xmlfile=true;
                         url = config.api_url.root_query; 
                        arg = {
                            processor_id: credentialPro,
                            action_type:'sale,capture',
                            condition:'complete',
                            end_date: req.param('endDate'),
                            start_date: req.param('startDate')
                        };
                        if (role==='user'){
                            //arg.processor_id=credentialPro;                    
                        }
                    }else{
                        // from mysql
                        isAPI = false;
                        db.Transaction_History.findAll({ where: {processor_id:credentialPro, transaction_condition:'complete', action_type:'sale', date:{lte: req.param('endDate'), gte: req.param('startDate')}}}).success(function(transactions){
                            if (!transactions){
                                return res.jsonp({});
                            }else{
                                if (Object.keys(transactions).length===0){
                                    return res.jsonp({});
                                }else{
                                    parseLikeNMI(transactions).then(
                                        function(transactionsParsed){
                                            return res.jsonp(transactionsParsed);
                                        },
                                        function(err){
                                            return res.jsonp({Response:err});
                                        });
                                }
                            }
                        }).error(function(err){
                            return res.render('error', {
                                error: err,
                                status: 500
                            });
                        });
                    }
                    break;
                case 'transactionsPerProcessorId':
                    checkProcessorsRepeated().then(
                        function(processor){
                            if (processor!==''){
                                var text = '[ WARNING: not unique Processor Id detected, ' + processor + '] [' + req.user.Username + '] [' + req.ip + ']';
                                Log.debug(text);
                            }
                        },
                        function(err){
                            return res.jsonp({Response:err});
                        });


                    if (config.transactionsComeFromNMI){
                        xmlfile=true;
                         url = config.api_url.root_query; 
                        arg = {
                            end_date: req.param('endDate'),
                            start_date: req.param('startDate'),
                            action_type:'sale,capture',
                            condition:'complete',
                            //processor_id: req.param('processor_id')
                            processor_id: credentialPro
                        };
                    }else{
                        // from mysql
                        isAPI = false;

                        var startDate = req.param('startDate');
                        var endDate = req.param('endDate');

                        /*
                        NOTE: JOIN WAS STALLING THE SERVER
                         */

                        db.sequelize.query('Select t.action_type, t.transaction_condition, t.amount, t.date, t.response_code, t.batch_id, t.currency, t.processor_id, t.transaction_id, t.id, m.Adjusted_Amount as reserve FROM mean_relational.Transaction_History as t JOIN mean_relational.MAS as m on t.transaction_id = m.Transaction_ID WHERE t.processor_id = :processor AND t.date >= :startDate AND t.date <= :endDate AND m.Type="RESERVE" AND t.transaction_condition="complete" AND t.action_type="sale"',null,{ raw: true },
                            { processor: credentialPro, startDate: startDate, endDate: endDate }
                        ).then(function(results) {
                                if (!results){
                                    return res.jsonp({});
                                }else{
                                    if (Object.keys(results).length===0){
                                        return res.jsonp({});
                                    }else{
                                        parseLikeNMIAccountBalance(results).then(
                                            function(transactionsParsed){
                                                //return res.jsonp(toObject(payment_plan));
                                                return res.jsonp(transactionsParsed);
                                            },
                                            function(err){
                                                return res.jsonp({Response:err});
                                            });
                                    }
                                }
                            });

                    }
                    break;
                case 'find_merchantId':
                    isAPI = false;
                    if (role==='user'){
                        db.Payment_Plan.find({where:{Boarding_ID: GlobalBoardingId, Processor_ID: credentialPro,Is_Active:1}}).success(function(account){
                            if (account){
                                db.Boarding.find({where: {id: account.Boarding_ID}}).success(function(boardings){
                                    var myBoarding = []; 
                                        myBoarding.push(boardings);                            
                                    return res.jsonp(toObject(myBoarding));
                                }).error(function(err){
                                    return res.render('error', {
                                        error: err,
                                        status: 500
                                    });
                                });
                            }else{
                                return res.jsonp({});
                            }
                        });
                    }else{
                        db.Boarding.findAll().success(function(boardings){
                            if (boardings) {
                                return res.jsonp(toObject(boardings));
                            }else{
                                return res.jsonp({});
                            }
                        }).error(function(err){
                            return res.render('error', {
                                error: err,
                                status: 500
                            });
                        });
                    }
                    break;
                case 'getMerchantInfo':
                    isAPI = false;
                    if (role==='user'){
                           db.Boarding.find({where: {id: GlobalBoardingId}}).success(function(boardings){
                               if (!boardings){
                                   return res.jsonp({});
                               }else{
                                   return res.jsonp(boardings);
                               }
                            }).error(function(err){
                                return res.render('error', {
                                    error: err,
                                    status: 500
                                });
                            });
                    }
                    break;
                case 'find_lastDisbursementDate':
                    isAPI = false;
                    arg = {
                        Payment_Date: {lte: req.param('startDate')},
                        Processor_ID: req.param('processorId')
                    };

                    db.MAS.findAll({ where: ['Payment_Date <=? AND Processor_ID=? AND Payment_Date!="00000000" AND Payment_Date!="null" limit 1',req.param('startDate'),req.param('processorId')]}).success(function(disbursementDates){
                        if (!disbursementDates){
                            return res.jsonp({});
                        }else{
                            if (Object.keys(disbursementDates).length > 0){
                                //return res.jsonp(disbursementDates[0].dataValues);
                                return res.jsonp(toObject(disbursementDates));
                            }else{
                                return res.jsonp({});
                            }
                        }
                    }).error(function(err){
                        return res.render('error', {
                            error: err,
                            status: 500
                        });
                    });
                    break;
                case 'find_lastBalance':
                    isAPI = false;
                    var proccesorid = req.param('processorId');
                    var lookingForLastBalance = req.param('lookingForLastBalance');
                    var date = req.param('date');
                    if ((lookingForLastBalance==='true') && (req.param('processorId')!=='')){
                        db.sequelize.query('Select date From Transaction_History Where Processor_ID = :processor Order By date asc limit 1',null,{ raw: true },
                            { processor: proccesorid }
                        ).success(function(results) {
                                if (!results){
                                    res.jsonp({Response:req.param('date')});
                                }
                                if (Object.keys(results).length===0){
                                    res.jsonp({});
                                }else{
                                    var yy = results[0].date.substr(0, 4);
                                    var mm = results[0].date.substr(4, 2);
                                    var dd = results[0].date.substr(6, 2);
                                    var dateString = yy+'-'+mm+'-'+dd;
                                    res.jsonp({Response:dateString});
                                }

                        }).error(function(err){
                            return res.jsonp({Response:err});
                        });
                    }else{
                        res.jsonp({Response:req.param('date')});
                    }
                    break;
                case 'find_dayHold':
                    isAPI = false;
                    arg = {
                        Processor_ID: req.param('processorId'),
                        Is_Active:1
                    };
                    db.Payment_Plan.find({ where: arg}).success(function(payment_plan){
                        if(!payment_plan) {
                            return res.jsonp({Response:'error'});
                        } else {
                            db.Disbursement_Schedules.find({ where: {Boarding_ID: payment_plan.Boarding_ID}}).success(function(disbursementdate){
                                if(!disbursementdate) {
                                    return res.jsonp({Response:'error'});
                                } else {
                                    return res.jsonp(disbursementdate);
                                }
                            }).error(function(err){
                                return res.jsonp({Response:err});
                            });
                        }
                    }).error(function(err){
                        return res.jsonp({Response:err});
                    });
                    break;
                case 'find_products':
                    isAPI = false; 
                    db.Product.findAll().success(function(products){
                        return res.jsonp(toObject(products));
                    }).error(function(err){
                        return res.render('error', {
                            error: err,
                            status: 500
                        });
                    });
                    break;
                case 'find_line_items':
                    isAPI = false; 
                    db.Line_Items.findAll().success(function(line_items){
                        return res.jsonp(toObject(line_items));
                    }).error(function(err){
                        return res.render('error', {
                            error: err,
                            status: 500
                        });
                    });
                    break;
                case 'find_disbursement':
                    isAPI = false; 
                    db.Disbursement_Schedules.findAll().success(function(disbursements){
                        return res.jsonp(toObject(disbursements));
                    }).error(function(err){
                        return res.render('error', {
                            error: err,
                            status: 500
                        });
                    });
                    break;
                case 'find_paymentplans':
                    isAPI = false; 
                    db.Payment_Plan.findAll({ where: {Is_Active:1}}).success(function(products){
                        return res.jsonp(toObject(products));
                    }).error(function(err){
                        return res.render('error', {
                            error: err,
                            status: 500
                        });
                    });
                    break;
                case 'create_customer_card_hash':
                    isAPI = false;
                    db.Payment_Plan.find({where:{Processor_ID:credentialPro}}).success(function(payment_plan) {
                        if (payment_plan) {
                            boardingGlobal = payment_plan.Boarding_ID;
                            var customer_card_hash = {
                                Transaction_ID: req.param('Transaction_ID'),
                                CC_Number: doMask(req.param('CC_Number')),
                                Hash: req.param('Hash'),
                                Brand: getBrand(req.param('CC_Number')),
                                Boarding_ID: boardingGlobal
                            };
                            db.Customer_Card_Hash.find({where:{Hash:customer_card_hash.Hash,Boarding_ID:boardingGlobal}}).success(function(cc){
                                if (!cc){
                                    db.Customer_Card_Hash.create(customer_card_hash).success(function(customer_card_hash,err){
                                        if(customer_card_hash){
                                            return res.jsonp({Response:'customer_card_hash done!'});
                                        }
                                    }).error(function(err){
                                        console.log(err);
                                    });
                                } else {
                                    return res.jsonp({Response:'customer_card_hash exists'});
                                }
                                
                            });
                        }
                    });
                    break;

                case 'find_customer_card_hash':
                    isAPI = false;
                    db.Customer_Card_Hash.find({ where:{Hash: req.param('cc_hash')}}).success(function(customer_card_hash){
                        if(!customer_card_hash){
                            return res.jsonp({Response:'error'});
                        } else {
                            return res.jsonp(customer_card_hash);
                        }
                    }).error(function(err){
                        return res.send('users/signup', {
                            errors: err,
                            status: 500
                        });
                    });
                    break;
                case 'find_allHash':
                    isAPI = false;
                    db.Customer_Card_Hash.findAll().success(function(customer_card_hash){
                        if(!customer_card_hash){
                            return res.jsonp({Response:'error'});
                        } else {
                            return res.jsonp(toObject(customer_card_hash));
                        }
                    }).error(function(err){
                        return res.send('users/signup', {
                            errors: err,
                            status: 500
                        });
                    });
                    break;
                case 'find_allHashByBoarding':
                    isAPI = false;
                    db.Customer_Card_Hash.findAll({ where:{Boarding_ID: req.param('boarding_id')}}).success(function(customer_card_hash){
                        if(!customer_card_hash){
                            return res.jsonp({Response:'error'});
                        } else {
                            return res.jsonp(toObject(customer_card_hash));
                        }
                    }).error(function(err){
                        return res.send('users/signup', {
                            errors: err,
                            status: 500
                        });
                    });
                    break;
                case 'find_fees':
                    isAPI = false; 
                    var product = req.param('Product_ID'),
                        processorIdSelected = req.param('processorId');
                   if (role==='user'){
                        // merchant
                        if (product==='all'){ 
                            // all products for one merchant
                            db.Payment_Plan.findAll({ where: {Boarding_ID: req.user.dataValues.Boarding_ID,Processor_ID: {ne: null},Is_Active:1}}).success(function(payment_plan){
                                if(!payment_plan) {
                                    return res.jsonp({Response:'error'});
                                } else {
                                    checkFees(payment_plan).then(
                                        function(paymentplanAdded){
                                            return res.jsonp(toObject(payment_plan));
                                        },
                                        function(err){
                                            return res.jsonp({Response:err});
                                        });
                                }
                            }).error(function(err){
                                return res.jsonp({Response:err});
                            });
                        }else{
                             // just one product
                            db.Payment_Plan.find({ where: {Boarding_ID: req.user.dataValues.Boarding_ID, Product_ID:product, Processor_ID: {ne: null},Is_Active:1}}).success(function(paymentplanOne){
                                if(!paymentplanOne) {
                                    product=1;
                                    db.Payment_Plan.find({ where: {Boarding_ID: req.user.dataValues.Boarding_ID, Product_ID:product, Processor_ID: {ne: null},Is_Active:1}}).success(function(paymentplanOne){
                                        if(!paymentplanOne) {
                                            return res.jsonp({Response:'error'});
                                        } else {
                                            return res.jsonp(paymentplanOne);
                                        }
                                    }).error(function(err){
                                        return res.jsonp({Response:err});
                                    });
                                } else {
                                    if (product==='2'){
                                        sumFees(req.user.dataValues.Boarding_ID, '1', paymentplanOne).then(
                                            function(paymentplanTwo){
                                                return res.jsonp(paymentplanTwo);
                                            },
                                            function(err){
                                                return res.jsonp({Response:err});
                                            });
                                    }else{
                                        return res.jsonp(paymentplanOne);
                                    }
                                }
                            }).error(function(err){
                                return res.jsonp({Response:err});
                            });
                        }
                    }else{
                        if ((product==='all') && (processorIdSelected===undefined)){
                             db.Payment_Plan.findAll({ where: {Processor_ID: {ne: null},Is_Active:1}}).success(function(payment_plan){
                                if(!payment_plan) {
                                    return res.jsonp({Response:'error'});
                                } else {
                                    sumAdminFees('1', payment_plan,'multiple',null,res).then(
                                        function(payment_plan){
                                            return res.jsonp(toObject(payment_plan));
                                        },
                                        function(err){
                                            return res.jsonp({Response:err});
                                        });
                                }
                            }).error(function(err){
                                return res.jsonp({Response:err});
                            });
                        }else if ((product==='all') && (processorIdSelected!=='')){
                            db.Payment_Plan.findAll({ where: {Boarding_ID: processorIdSelected,Is_Active:1}}).success(function(payment_plan){
                                if(!payment_plan) {
                                    return res.jsonp({Response:'error'});
                                } else {
                                    sumAdminFees('1', payment_plan,'allprocessor',processorIdSelected,res).then(
                                        function(payment_plan){
                                            return res.jsonp(toObject(payment_plan));
                                        },
                                        function(err){
                                            return res.jsonp({Response:err});
                                        });



                                }
                            }).error(function(err){
                                return res.jsonp({Response:err});
                            });

                        }else if (processorIdSelected!==''){
                             db.Payment_Plan.findAll({ where: {Product_ID: product, Processor_ID: processorIdSelected,Is_Active:1}}).success(function(payment_plan){
                                if(!payment_plan) {
                                    return res.jsonp({Response:'error'});
                                } else {
                                    //return res.jsonp(toObject(payment_plan));
                                    if (product==='2'){
                                        sumAdminFees('1', payment_plan,true,null,res).then(
                                            function(payment_plan){
                                                return res.jsonp(toObject(payment_plan));
                                            },
                                            function(err){
                                                return res.jsonp({Response:err});
                                            });
                                    }else{                                
                                        return res.jsonp(toObject(payment_plan));    
                                    }                            
                                }
                            }).error(function(err){
                                return res.jsonp({Response:err});
                            });
                        }else{
                            db.Payment_Plan.findAll({ where: {Product_ID: product, Processor_ID: {ne: null},Is_Active:1}}).success(function(payment_plan){
                                if(!payment_plan) {
                                    return res.jsonp({Response:'error'});
                                } else {
                                    //return res.jsonp(toObject(payment_plan));
                                    if (product==='2'){
                                        sumAdminFees('1', payment_plan,false,null,res).then(
                                            function(payment_plan){
                                                return res.jsonp(toObject(payment_plan));
                                            },
                                            function(err){
                                                return res.jsonp({Response:err});
                                            });
                                    }else{                                
                                        return res.jsonp(toObject(payment_plan));    
                                    }                            
                                }
                            }).error(function(err){
                                return res.jsonp({Response:err});
                            });
                        }
                    }
                    break;
                case 'set_itemfees':
                    isAPI = false; 

                    var boarding_id = req.param('boarding_id'); 
                    var processor = req.param('processor'); 
                    var itemType = req.param('itemType'); 
                    var feeAmount = req.param('feeAmount'); 
                    var payment = req.param('payment'); 
                    var description = req.param('description'); 
                    var transaction_id = req.param('transaction_id'); 
//                    var date = req.param('date'); // JIRA MC-393 already defined
                    date = req.param('date'); 
                    if (feeAmount === '') {
                        feeAmount = '0';
                    }
                    var item = {
                        Boarding_ID: boarding_id,
                        Processor_ID: processor,
                        Fee_Name: itemType,
                        Fee_Amount: parseFloat(feeAmount),
                        Debit_Credit: payment,
                        Transaction_ID: transaction_id,
                        Occurrence_Date: date,
                        Description: description
                    };

                    db.Line_Items.create(item).success(function(itemResponse,err){
                        if(itemResponse){
                            return res.jsonp({Response:true});
                        } else {
                            return res.jsonp({Response:false});
                        }
                    }).error(function(err){
                        return res.send('users/signup', { 
                            errors: err,
                            status: 500
                        });
                    });

                    /**
                     * Tracking
                     */
                    var text = '\r\n\r\n[disbursement - days hold was changed] [boarding_id: ' + boarding_id + '] [processor: ' + processor + '] [itemType: '+itemType+'] [payment: '+payment+'] [description: '+description+'] [transaction_id: '+transaction_id+']\r\n\r\n';  
                    Log.debug(text);

                    break;
                case 'find_reverses':
                    isAPI = false; 
                    if (role==='user'){
                        db.Boarding.find({ where: {id: req.user.dataValues.Boarding_ID}}).success(function(boardingCollection){
                            if(!boardingCollection) {
                                return res.jsonp({Response:'error'});
                            } else {
                                return res.jsonp(boardingCollection);
                            }
                        }).error(function(err){
                            return res.jsonp({Response:err});
                        });
                    }else{
                        db.Boarding.findAll().success(function(boardingCollection){
                            if(!boardingCollection) {
                                return res.jsonp({Response:'error'});
                            } else {
                                return res.jsonp(toObject(boardingCollection));
                            }
                        }).error(function(err){
                            return res.jsonp({Response:err});
                        });
                    }                    
                    break;
                case 'find_reserveByDates':
                    isAPI = false;

                    var proccesorid1 = req.param('processorId');
                    var startDate1 = req.param('startDate');
                    var endDate1 = req.param('endDate');

                    // parsing startDate
                    var startDateYear = startDate1.substr(0,4);
                    var startDateMonth = startDate1.substr(4,2);
                    var startDateDay = startDate1.substr(6,2);
                    // DB REFACTOR startDate1 = startDateYear+'-'+startDateMonth+'-'+startDateDay;

                    // parsing endDate
                    var endDateYear = endDate1.substr(0,4);
                    var endDateMonth = endDate1.substr(4,2);
                    var endDateDay = endDate1.substr(6,2);
                    // DB REFACTOR endDate1 = endDateYear+'-'+endDateMonth+'-'+endDateDay;
                    
                    //  and Payment_Date >= :startDate and Payment_Date <= :endDate
                    db.sequelize.query('Select Adjusted_Amount, Processor_ID, Transaction_ID From MAS Where Type = "RESERVE" AND Processor_ID = :processor',null,{ raw: true },
                        { processor: proccesorid1, startDate: startDate1, endDate: endDate1 }
                    ).then(function(results) {
                            res.jsonp({Response:results});
                        });
                    break;
                case 'get_all_transactions':
                    var boardingId = req.param('boardingId');
                    var startDate2 = req.param('startDate');
                    var endDate2 = req.param('endDate');
                    
                    db.sequelize.query('SELECT * FROM Transaction_History mh INNER JOIN Payment_Plans pp ON pp.Processor_ID = mh.processor_id where pp.Boarding_ID = :boardingID AND mh.date >= :startDate AND mh.date <= :endDate;',null,{ raw: true },
                        { boardingID: boardingId, startDate: startDate2, endDate: endDate2 }
                    ).then(function(results) {
                            res.jsonp({Response:results});
                    });
                    break;                 
                case 'treport_all':
                    checkProcessorsRepeated().then(
                        function(processor){
                            if (processor!==''){
                                var text = '[ WARNING: not unique Processor Id detected, ' + processor + '] [' + req.user.Username + '] [' + req.ip + ']';
                                Log.debug(text);
                            }
                        },
                        function(err){
                            return res.jsonp({Response:err});
                        });

                    getAllCredentials(GlobalBoardingId,role).then(function(allResults) {
						var argstring = '';
						var arg = {};
						if (role !== 'admin' && role !== 'super') {
							var processorList = '(';
							for (var i_proc = 0; i_proc < allCredentials.length; i_proc++) {
								if (allCredentials[i_proc].dataValues.Processor_ID !== null) {
									processorList += "'" + allCredentials[i_proc].dataValues.Processor_ID + "',";
								}
							}
							processorList = processorList.substring(0, processorList.length - 1);
							processorList += ")";

							argstring = 'SELECT TH.* FROM Transaction_History TH WHERE (TH.processor_id IN ' + processorList;
						} else {
							argstring = 'SELECT TH.*, B.Merchant_Name, B.Merchant_ID, PP.Boarding_ID ' +
										'FROM   Transaction_History TH ' +
										'JOIN   Payment_Plans PP ON PP.Processor_ID = TH.Processor_ID ' +
										'JOIN   Boardings B ON B.id = PP.Boarding_ID ' +
										'WHERE	(1';
						}
                        isAPI = false; 

                        if (((typeof req.param('endDate')!=='undefined')&&(req.param('endDate')!=='')) && ((typeof req.param('startDate')!=='undefined')&&(req.param('startDate')!==''))){
                            argstring+=' AND TH.date <= :endDate AND TH.date >= :startDate';
							arg.startDate = req.param('startDate');
                            arg.endDate = req.param('endDate');
                        }
                        if ((typeof req.param('transactionId')!=='undefined') && (req.param('transactionId')!=='')){
                            argstring+=' AND TH.transaction_id = :transactionId';
                            arg.transactionId = req.param('transactionId');
                        }
                        if ((typeof req.param('actionType')!=='undefined') && (req.param('actionType')!=='')){
                            argstring+=' AND TH.action_type = :actionType';
                            arg.actionType = req.param('actionType');
                        }
                        if ((typeof req.param('orderId')!=='undefined') && (req.param('orderId')!=='')){
                            argstring+=' AND TH.order_id = :orderId';
                            arg.orderId = req.param('orderId');
                        }
                        if ((typeof req.param('firstName') !== 'undefined') && (req.param('firstName') !== '')){
                            argstring += ' AND TH.first_name = :firstName';
                            arg.firstName = req.param('firstName');
                        }
                        if ((typeof req.param('lastName')!=='undefined') && (req.param('lastName')!=='')){
                            argstring+=' AND TH.last_name = :lastName';
                            arg.lastName = req.param('lastName');
                        }
                        if ((typeof req.param('customerVaultId')!=='undefined') && (req.param('customerVaultId')!=='')){
                            argstring+=' AND TH.customerid = :customerVaultId';
                            arg.customerVaultId = req.param('customerVaultId');
                        }
                        if ((typeof req.param('reportType')!=='undefined') && (req.param('reportType')!=='')){
                            argstring+=' AND TH.reportType = :reportType';
                            arg.reportType = req.param('reportType');
                        }
                        if ((typeof req.param('condition')!=='undefined') && (req.param('condition')!=='')){
                            argstring+=' AND TH.transaction_condition = :condition';
                            arg.condition = req.param('condition');
                        }
                        if ((typeof req.param('amountFrom') !== 'undefined') && (req.param('amountFrom') !== '')) {
                            argstring += ' AND CAST(TH.amount AS decimal(18,2)) >= :amountFrom';
                            arg.amountFrom = req.param('amountFrom');
                        }
                        if ((typeof req.param('amountTo') !== 'undefined') && (req.param('amountTo') !== '')) {
                            argstring += ' AND CAST(TH.amount AS decimal(18,2)) <= :amountTo';
                            arg.amountTo = req.param('amountTo');
                        }
                        if ((typeof req.param('last4ofCC') !== 'undefined') && (req.param('last4ofCC') !== '')) {
                            argstring += ' AND SUBSTR(cc_number,-4) = :last4ofCC';
                            arg.last4ofCC = req.param('last4ofCC');
                        }
						argstring+=')';
                        if ((typeof req.param('originalId')!=='undefined') && (req.param('originalId')!=='') && (req.param('transactionId')!=='') && (typeof req.param('transactionId')!=='undefined') ){
                            argstring+=' OR (TH.Original_transaction_id = :originalId)';
                            arg.originalId = req.param('transactionId');
                        }
                        argstring+=';';

                        db.sequelize.query(argstring,null,{ raw: true },
							arg
                       ).success(function(transactions) {
                               if (!transactions){
                                   return res.jsonp({});
                               }else{
                                   if (Object.keys(transactions).length===0){
                                       return res.jsonp({});
                                   }else{
                                       if ((typeof req.param('transactionId')!=='undefined') && (req.param('transactionId')!=='') && ((typeof req.param('originalId')==='undefined') || (req.param('originalId')===''))){
                                           db.Transaction_History.findAll({ where: ['Original_transaction_id=?',req.param('transactionId')]}).success(function(history) {
                                               if(!history){
                                                   parseLikeNMI(transactions).then(
                                                       function(transactionsParsed){
                                                           return res.jsonp(transactionsParsed);
                                                       },
                                                       function(err){
                                                           return res.jsonp({Response:err});
                                                       });
                                               }else{
                                                   //historicals go in here
                                                   parseLikeNMI(transactions,history).then(
                                                       function(transactionsParsed){
                                                           return res.jsonp(transactionsParsed);
                                                       },
                                                       function(err){
                                                           return res.jsonp({Response:err});
                                                       });
                                               }
                                           });
                                       }else{
                                           parseLikeNMI(transactions).then(
                                               function(transactionsParsed){
                                                   return res.jsonp(transactionsParsed);
                                               },
                                               function(err){
                                                   return res.jsonp({Response:err});
                                               });
                                       }
                                   }
                               }
                           }).error(function(err){
								console.log(err);
//                                return res.render('error', {
//                                   error: err,
//                                   status: 500
//                               });
                           });
                    });
                    break;                 
/*
                case 'add_recurring_plan':
                    url = config.api_url.root_transact; 
                    arg = {
                            recurring: 'add_plan',
                            plan_payments: req.param('plan_payments'),
                            plan_amount: req.param('plan_amount'),
                            plan_name: req.param('plan_name'),
                            plan_id: req.param('plan_id'),
                            day_frequency: req.param('day_frequency'),
                            month_frequency: req.param('month_frequency'),
                            day_of_month: req.param('day_of_month')
                        };                        
                    break;                 
                case 'update_recurring_plan':
                    break;                  
*/
                case 'salevault':
                    xmlfile=false;
                    url = config.api_url.root_transact; 
                    arg = {
                        type: 'validate',
                        amount: '0.00',
                        cvv: req.param('cardSecurityCode'),
                        orderid: req.param('orderId'),
                        orderdescription: req.param('orderDescription'),
                        ponumber: req.param('poNumber'),
                        shipping: req.param('shipping'),
                        tax: req.param('tax'),
                        customer_vault_id:req.param('customerHash'),
                        //customer_receipt:req.param('customerReceipt')
                    };
                    emailParameters.send_mojopay_receipt = req.param('customerReceipt');
                    emailParameters.mojo_receipt_email_address = req.param('emailAddressCard');
                    emailParameters.mojo_receipt_customer_name = req.param('firstNameCard') + ' ' + req.param('lastNameCard');
                    emailParameters.mojo_reciept_billing_address = emailParameters.mojo_receipt_customer_name + '<br>' + 
                                                   req.param('addressCard') + '<br>' +
						                           req.param('addressContCard') + '<br>' +
                                                   req.param('cityCard') + ',' +
						                           req.param('stateProvinceCard') + ' ' +
						                           req.param('zipPostalCodeCard') + '<br>' +
                                                   req.param('countryCard') + '<br>' +
						                           req.param('phoneNumberCard');
                    emailParameters.mojo_reciept_shipping_address = req.param('addressShipping') + '<br>' +
						                            req.param('addressContShipping') + '<br>' +
                                                    req.param('cityShipping') + ',' +
						                            req.param('stateProvinceShipping') + ' ' +
						                            req.param('zipPostalCodeShipping') + '<br>' +
                                                    req.param('countryShipping');
                    emailParameters.mojo_receipt_card_type = req.param('creditCardNumber');
                    thisAmount = 0;
                    thisTax = 0;
                    thisShipping = 0;
                    if(req.param('amount') !== undefined && req.param('amount') !== null && req.param('amount') !== '') {
                        thisAmount = parseFloat(req.param('amount'));
                    }
                    if(req.param('tax') !== undefined && req.param('tax') !== null && req.param('tax') !== '') {
                        thisTax = parseFloat(req.param('tax'));
                    }
                    if(req.param('shipping') !== undefined && req.param('shipping') !== null && req.param('shipping') !== '') {
                        thisShipping = parseFloat(req.param('shipping'));
                    }
                    emailParameters.mojo_receipt_amount =  thisAmount + thisTax + thisShipping;
      
                    break;
                default:
                    console.log('URL is bad...');
            }
            for (var attribute in req.query){
                if (attribute.indexOf('item') !== -1){
                    arg[attribute] = req.query[attribute];
                }
            }

            /**
             * Call resources API
             */
            if (isAPI){
                arg.username = credentialsU; 
                arg.password = credentialsP;
                var items = Object.keys(arg);
                queryString ='?';
                items.forEach(function(item) {
                  if ((arg[item]!=='')&&(arg[item]!==undefined)){
                       queryString+='&' + item + '=' + encodeURIComponent(arg[item]);
                  }
                });
                url+=queryString;
//console.log(url)
				request(url, function (error, response, body) {
                    if (!error && response.statusCode === 200) {

                        if ( parseInt(queryStringToJSON(body).response_code) >= 100 &&  parseInt(queryStringToJSON(body).response_code) < 300) {

//console.log(emailParameters)                
                            insertIntoTransactionHistory(req, res, credentialsU, credentialsP, queryStringToJSON(body).transactionid, GlobalBoardingId, emailParameters);                           
                            //sendEmail(req, res, emailParameters.send_mojopay_receipt, queryStringToJSON(body).transactionid, emailParameters);
                        }
                        if (xmlfile){
                            var json = XMLMapping.load(body);
                            res.jsonp(json);
                        }else{
                            res.jsonp({Response:queryStringToJSON(body)});
                        }
                     } else {
                        console.log(error);
                     }
                });

                /**
                 * Tracking
                 */
//                var text = '[' + transactionType + '] [' + req.user.Username + '] [' + req.ip + ']'; // JIRA MC-393 ISSUE is already defined
                text = '[' + transactionType + '] [' + req.user.Username + '] [' + req.ip + ']';  
                Log.debug(text);
            }
        },
        function(err){
            console.log(err);
        });
};

/**
 * Testapi authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    if (req.testapi.user.id !== req.user.id) {
        return res.status(403).send('User is not authorized');
    }
    next();
};
