'use strict';

/**
 * Module dependencies.
 */
var fs = require('fs');
var json = JSON.parse(fs.readFileSync('package.json', 'utf8'));
var version = json.version;

exports.getVersion = function(req, res) {
    return res.jsonp({'version': version});
};
