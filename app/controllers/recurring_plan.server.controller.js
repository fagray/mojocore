'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    nodemailer = require('nodemailer'),
    crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq',
    cipher,
	config = require('./../../config/config'),
    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    };


/**
 * retrieve all recurring_plans 
 */
exports.recurring_plan = function(req, res, next, id) {
    var product = req.params.productId;
    if (product) {
        if(product === 'all') {
            db.Recurring_Plan.findAll({ where: {Boarding_ID: id, Is_Active:1}}).success(function(recurring_plan){
                if(!recurring_plan) {
                    req.recurring_plan = {clean:'clean'};
                    return next();
                } else {
                    req.recurring_plan = {response:recurring_plan};
                    return next();
                }
            }).error(function(err){
                return next(err);
            });

        } else {
            db.Payment_Plan.find({ where: {Boarding_ID: id, Product_ID:product}}).success(function(processor){
                if (!processor){
                    return res.jsonp({});
                }else{
                    db.Recurring_Plan.findAll({ where: {Boarding_ID: id, Processor_ID: processor.Processor_ID, Is_Active:1}}).success(function(recurring_plan){
                        if(!recurring_plan) {
                            req.recurring_plan = {clean:'clean'};
                            return next();
                        } else {
                            req.recurring_plan = {response:recurring_plan};
                            return next();
                        }
                    }).error(function(err){
                        return next(err);
                    });
                }
            }).error(function(err){
                return res.render('error', {
                    error: err,
                    status: 500
                });
            });
        }
    } else {
        req.recurring_plan = {clean:'clean'};
        return next();
    }
};


/**
 * retrieve a specific recurring_plan by id with associated info
 */
exports.findOne = function(req, res) {
    var Plan_ID = req.params.Plan_ID;
    db.sequelize.query('select recurr.*,pro.Description from Recurring_Plans as recurr join Payment_Plans as pay on recurr.Processor_ID = pay.Processor_ID join Products as pro on pay.Product_ID = pro.id where recurr.id = :plan_id AND recurr.Is_Active = 1;',null,{ raw: true },
            { plan_id: Plan_ID }
        ).success(function(audit_recurring_plan, err) {
        if(!audit_recurring_plan) {
            return res.jsonp({NoResults:true });
        } else {
            return res.jsonp({Response: audit_recurring_plan});
        }
    }).error(function(err){
        return err;
    });
};


/**
 * retrieve a specific recurring_plan by id with associated info
 */
exports.checkRecurringPlan = function(req, res, next) {
    var Plan_ID = req.param('planId');
    var Product_ID = req.param('productId');
    var Boarding_ID = req.param('boardingId');
    db.Payment_Plan.find({ where: {Boarding_ID: Boarding_ID, Product_ID:Product_ID}}).success(function(processor){
        if (!processor){
            req.processor = {clean:'clean'};
            return res.jsonp(req.processor);
        } else {
            db.Recurring_Plan.find({ where: {Boarding_ID: Boarding_ID, Processor_ID: processor.Processor_ID, Plan_ID: Plan_ID}})
                .success(function(recurring_plan){
                    if(!recurring_plan) {
                        req.recurring_plan = {clean:'clean'};
                        return res.jsonp(req.recurring_plan);
                    } else {
                        req.recurring_plan = {response: recurring_plan};
                        return res.jsonp(req.recurring_plan);
                    }
                }).error(function(err){
                    return res.jsonp(err);
                });
        }
     }).error(function(err){
          return res.render('error', {
              error: err,
              status: 500
          });
      });
};

/**
 * Create a recurring_plan
 */
exports.create = function(req, res) {
    var recurringPlanObject = {
        Plan_ID: req.body.plan_id,
        Plan_Name: req.body.plan_name,
        Plan_Amount: req.body.plan_amount,
        Plan_Payments: req.body.plan_payments,
        Day_Of_Month: (req.body.day_of_month === '' ? 0 : req.body.day_of_month),
        Day_Frequency: (req.body.day_frequency === '' ? 0 : req.body.day_frequency),
        Month_Frequency: (req.body.month_frequency === '' ? 0 : req.body.month_frequency),
        Processor_ID: req.body.processorId,
        Boarding_ID: req.body.boardingId
    };
    // save and return and instance of recurring_plan on the res object.
        db.Recurring_Plan.create(recurringPlanObject)
        .success(function(recurring_plan, err){
            if(!recurring_plan){
                return res.send('error', {
                    error: err,
                    status: 500
                });
            } else {
                return res.jsonp(recurring_plan);
            }
        }).error(function(err){
            return res.send('error', {
                error: err,
                status: 500
            });
        });
};


/**
 * Update a recurring_plan
 */
exports.update = function(req, res) {
//console.log('updating a plan')    
    db.Recurring_Plan.find({ where: {id: req.body.id}}).success(function(recurring_plan_record){
        if(recurring_plan_record) {
            var recurring_plan = recurring_plan_record; 
            recurring_plan.updateAttributes({
                Plan_Name: req.body.Plan_Name,
                Plan_Amount: req.body.Plan_Amount,
                Plan_Payments: req.body.Plan_Payments,
                Day_Of_Month: (req.body.Day_Of_Month === '' ? 0 : req.body.Day_Of_Month),
                Day_Frequency: (req.body.Day_Frequency === '' ? 0 : req.body.Day_Frequency),
                Month_Frequency: (req.body.Month_Frequency === '' ? 0 : req.body.Month_Frequency),
                Is_Active: req.body.Is_Active
            }).success(function(a){
                return res.jsonp(a);
            }).error(function(err){
                return res.render('error', {
                    error: err, 
                    status: 500
                });
            });
        } // if(recurring_plan_record) 
    }).error(function(err){
        console.error(err);
    });

};


/**
 * Delete an payment_plan - not used
 */
exports.destroy = function(req, res) {
    // create a new variable to hold the recurring_plan that was placed on the req object.
    var recurring_plan = req.recurring_plan;

    recurring_plan.destroy().success(function(){
        return res.jsonp(recurring_plan);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};


/**
 * Show a recurring_plan
 */
exports.show = function(req, res) {
    // Sending down the recurring_plan that was just preloaded by the recurring_plan.recurring_plan function
    // and saves recurring_plan on the req object.
    return res.jsonp(req.recurring_plan);
};

exports.all = function(req, res) {
    var query = '';
    var arg = {};
    var boardingId = req.param('boarding'),
        productId  = req.param('product'),
        planName    = req.param('plan'),
        id         = req.param('id');
        
        query = 'SELECT rp.id as Recurr_ID, rp.*, pp.* FROM Recurring_Plans rp JOIN Payment_Plans pp ON rp.Processor_ID = pp.Processor_ID WHERE rp.Is_Active = 1';
        arg.boardingId = boardingId;
        
        if ((typeof req.param('product') !== 'undefined') && (req.param('product') !== '') && (req.param('product') !== 'all')) {
            query += ' AND Product_ID = :productId';
			arg.productId = req.param('product');
        }
        if ((typeof req.param('plan') !== 'undefined') && (req.param('plan') !== '') && (req.param('plan') !== 'all')) {
            query += ' AND Plan_Name LIKE :plan';
			arg.plan = '%' + req.param('plan') + '%';
        }
        if ((typeof req.param('id') !== 'undefined') && (req.param('id') !== '')) {
            query += ' AND rp.id = :id';
			arg.id = req.param('id');
        }
        query += ';';

        db.sequelize.query(query,null,{ raw: true },
		    arg
        ).success(function(subscription){
            if(!subscription) {
                return res.jsonp({response:null});
            } else {
                return res.jsonp({response: subscription});
            }
        }).error(function(err){
            return res.jsonp({response:err});
        });
};


