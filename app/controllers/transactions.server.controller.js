'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    Q =  require('q'),
	config = require('./../../config/config');

/**
 * Log4js need be configurated
 * Remember include -> config = require('./../../config/config')
 */
/**
 * Find boarding by id
 * Note: This is called every time that the parameter :boardingId is used in a URL. 
 * Its purpose is to preload the boarding on the req object then call the next function. 
 */
var parseLikeNMI = function(arr){

        var deferred = Q.defer();
        var counter = 0;
        var newObjectTransactions = [];

        for (var i = 0; i < Object.keys(arr).length; i++) {
            newObjectTransactions = {
                transaction_id: {$t:arr[i].transaction_id},
                partial_payment_id: {},
                partial_payment_balance: {},
                platform_id: {$t:arr[i].platform_id},
                transaction_type: {$t:arr[i].transaction_type},
                condition: {$t:arr[i].transaction_condition},
                order_id: {$t:arr[i].order_id},
                authorization_code: {$t:arr[i].authorization_code},
                ponumber: {$t:arr[i].ponumber},
                order_description: {$t:arr[i].order_description},
                first_name: {$t:arr[i].first_name},
                last_name: {$t:arr[i].last_name},
                address_1: {$t:arr[i].address_1},
                address_2: {$t:arr[i].address_2},
                company: {$t:arr[i].company},
                city: {$t:arr[i].city},
                state: {$t:arr[i].state},
                postal_code: {$t:arr[i].postal_code},
                country: {$t:arr[i].country},
                email: {$t:arr[i].email},
                phone: {$t:arr[i].phone},
                fax: {$t:arr[i].fax},
                cell_phone: {$t:arr[i].cell_phone},
                customertaxid: {$t:arr[i].customertaxid},
                website: {$t:arr[i].website},
                shipping_first_name: {$t:arr[i].shipping_first_name},
                shipping_last_name: {$t:arr[i].shipping_last_name},
                shipping_address_1: {$t:arr[i].shipping_address_1},
                shipping_address_2: {$t:arr[i].shipping_address_2},
                shipping_company: {$t:arr[i].shipping_company},
                shipping_city: {$t:arr[i].shipping_city},
                shipping_state: {$t:arr[i].shipping_state},
                shipping_postal_code: {$t:arr[i].shipping_postal_code},
                shipping_country: {$t:arr[i].shipping_country},
                shipping_email: {$t:arr[i].shipping_email},
                shipping_carrier: {$t:arr[i].shipping_carrier},
                tracking_number: {$t:arr[i].tracking_number},
                shipping_date: {$t:arr[i].shipping_date},
                shipping: {$t:arr[i].Shipping},
                shipping_phone: {$t:arr[i].shipping_phone},
                cc_number: {$t:arr[i].cc_number},
                cc_hash: {$t:arr[i].cc_hash},
                cc_exp: {$t:arr[i].cc_exp},
                cavv: {$t:arr[i].cavv},
                cavv_result: {$t:arr[i].cavv_result},
                xid: {$t:arr[i].xid},
                avs_response: {$t:arr[i].avs_response},
                csc_response: {$t:arr[i].csc_response},
                cardholder_auth: {$t:arr[i].cardholder_auth},
                cc_start_date: {$t:arr[i].cc_start_date},
                cc_issue_number: {$t:arr[i].cc_issue_number},
                check_account: {$t:arr[i].check_account},
                check_hash: {$t:arr[i].check_hash},
                check_aba: {$t:arr[i].check_aba},
                check_name: {$t:arr[i].check_name},
                account_holder_type: {$t:arr[i].account_holder_type},
                account_type: {$t:arr[i].account_type},
                sec_code: {$t:arr[i].sec_code},
                drivers_license_number: {$t:arr[i].drivers_license_number},
                drivers_license_state: {$t:arr[i].drivers_license_state},
                drivers_license_dob: {$t:arr[i].drivers_license_dob},
                social_security_number: {$t:arr[i].social_security_number},
                processor_id: {$t:arr[i].processor_id},
                tax: {$t:arr[i].tax},
                currency: {$t:arr[i].currency},
                surcharge: {$t:arr[i].surcharge},
                tip: {$t:arr[i].tip},
                card_balance: {},
                card_available_balance: {},
                entry_mode: {},
                original_transaction_id:{$t:arr[i].Original_transaction_id},
                cc_bin: {$t:arr[i].cc_bin},
                action: {
                    amount: {$t:arr[i].amount},
                    action_type: {$t:arr[i].action_type},
                    date: {$t:arr[i].date},
                    success: {$t:arr[i].success},
                    ip_address: {$t:arr[i].ip_address},
                    source: {$t:arr[i].source},
                    username: {$t:arr[i].username},
                    response_text: {$t:arr[i].response_text},
                    batch_id: {$t:arr[i].batch_id},
                    processor_batch_id: {$t:arr[i].processor_batch_id},
                    response_code: {$t:arr[i].response_code},
                    processor_response_text: {},
                    processor_response_code: {},
                    device_license_number: {$t:arr[i].device_license_number},
                    device_nickname: {$t:arr[i].device_nickname}
                }
            };
            counter+=1;
            if (counter===Object.keys(arr).length){
                var newObjectTransactionsWrapped = {};
                newObjectTransactionsWrapped.transaction = newObjectTransactions;
                deferred.resolve(newObjectTransactionsWrapped);
            }
        }
        return deferred.promise;
    };

    exports.getTransaction = function(req, res) {
    var transactionid = req.param('transactionid');
    db.Transaction_History.findAll({ where: {transaction_id:transactionid}}).success(function(transactions){
        if (!transactions){
            return res.jsonp({});
        }else{
            if (Object.keys(transactions).length===0){
                return res.jsonp({});
            }else{
                parseLikeNMI(transactions).then(
                    function(transactionsParsed){
                        return res.jsonp(transactionsParsed);
                    },
                    function(err){
                        return res.jsonp({Response:err});
                    });
            }
        }
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

exports.getProcessor = function(req, res) {

    var processorid = req.param('processorid'),
        Product_ID = req.param('Product_ID'),
        boardingid = req.param('boardingid');
     db.Payment_Plan.findAll({ where: {Processor_ID:processorid}}).success(function(processor){
        if (!processor){
            return res.jsonp({});
        }else{
            if (processor.Product_ID === Product_ID && processor.Boarding_ID === boardingid) {
                return res.jsonp({});
            } else {
                return res.jsonp({response:processor});
            }
        }
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

exports.getNMIUsername = function(req, res) {

    var username = req.param('username'),
        Product_ID = req.param('productId'),
        boardingid = req.param('boardingid');
     db.Payment_Plan.findAll({ where: {Username:username}}).success(function(username){
        if (!username){
            return res.jsonp({});
        }else{
            return res.jsonp({response:username});
        }
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};
