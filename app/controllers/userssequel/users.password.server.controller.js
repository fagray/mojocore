'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors'),
	passport = require('passport'),
	db = require('../../../config/sequelize'),
	log4js = require('log4js'),
	config = require('../../../config/config'),
	nodemailer = require('nodemailer'),
	async = require('async'),
	crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq',
    cipher;

log4js.configure(config.logging);
var Log = log4js.getLogger('user'); // "user" represents category of log or type of log

/**
 * Forgot for reset password (forgot POST)
 */
exports.forgot = function(req, res, next) {
	async.waterfall([
		// Generate random token
		function(done) {
			crypto.randomBytes(20, function(err, buffer) {
				var token = buffer.toString('hex');
				done(err, token);
			});
		},
		// Lookup user by username
		function(token, done) {
			if (req.body.Username) {
				db.User.find({ where:{Username: req.body.Username}}).success(function(user){
					if (!user) {
						return res.status(400).send({
							message: 'An email has been sent to the address associated with your account. The message includes a link to reset your password. Please contact support if you have not received the email after a few minutes.'
						});
//					} else if (user.provider !== 'local') {
//						return res.status(400).send({
//							message: 'It seems like you signed up using your ' + user.provider + ' account'
//						});
					} else {
						user.Reset_Password_Token = token;
						user.Reset_Password_Expires = Date.now() + 3600000; // 1 hour
                        user.Last_Changed_By = req.body.Last_Changed_By;
						user.save(function(err) {
							done(err, token, user);
						});

						/*
						 * Send email
						 */

						// step 1.- instance nodemailer
						var transport = nodemailer.createTransport({
						    service: config.mailer.options.service,
						    auth: {
						        user: config.mailer.options.auth.user,
						        pass: config.mailer.options.auth.pass
						    }
						});
						// step 2.- compile template
						/*
						 *  encrypting username and seed
						 */

						 // encryot seed
						var seed = String(Date.now());
						cipher = crypto.createCipher(algorithm,password);
						var cryptedSeed = cipher.update(seed,'utf8','hex');
						cryptedSeed += cipher.final('hex');

						// encrypt username						
						cipher = crypto.createCipher(algorithm,password);
						var cryptedUsername = cipher.update(user.Username,'utf8','hex');
						cryptedUsername += cipher.final('hex');
						var port = config.port;
                        var root = req.protocol + '://' + req.hostname  + ( port === 443 ? '' : ':' + port ) + '/#!/password/reset/' + cryptedUsername + '.' + cryptedSeed + '.' + token;
						res.render('templates/reset-password-email', {
							name: user.First_Name,
							appName: config.app.title,
							url: root
						}, function(err, emailHTML) {
							// step 3.- set data
							var mailOptions = {
							    from: config.mailer.from,
							    to: user.Email, 
							    subject: 'Reset password',
							    html: emailHTML
							};
							// step 4.- send mail with defined transport object
							transport.sendMail(mailOptions, function(error, info){
							    if(error){
							        console.log(error);
							    }else{
							    	res.status(200).send({message: 'An email has been sent to the address associated with your account. The message includes a link to reset your password. Please contact support if you have not received the email after a few minutes.'});
							    }
							});
						});
					}
			    }).error(function(err){
			        return next(err);
			    });
				/*User.findOne({
					username: req.body.Username
				}, '-salt -password', function(err, user) {
					if (!user) {
						return res.status(400).send({
							message: 'No account with that username has been found'
						});
//					} else if (user.provider !== 'local') {
//						return res.status(400).send({
//							message: 'It seems like you signed up using your ' + user.provider + ' account'
//						});
					} else {
						user.Reset_Password_Token = token;
						user.Reset_Password_Expires = Date.now() + 3600000; // 1 hour

						user.save(function(err) {
							done(err, token, user);
						});
					}
				});*/
			} else {
				return res.status(400).send({
					message: 'Username field must not be blank'
				});
			}
		},
		function(token, user, done) {
			res.render('templates/reset-password-email', {
				name: user.Display_Name,
				appName: config.app.title,
				url: 'http://' + req.headers.host + '/auth/reset/' + token
			}, function(err, emailHTML) {
				done(err, emailHTML, user);
			});
		},
		// If valid email, send reset email using service
		function(emailHTML, user, done) {
			var smtpTransport = nodemailer.createTransport(config.mailer.options);
			var mailOptions = {
				to: user.Email,
				from: config.mailer.from,
				subject: 'Password Reset',
				html: emailHTML
			};
			smtpTransport.sendMail(mailOptions, function(err) {
				if (!err) {
					res.send({
						message: 'An email has been sent to the address associated with your account. The message includes a link to reset your password. Please contact support if you have not received the email after a few minutes.'
					});
				}

				done(err);
			});
		}
	], function(err) {
		if (err) return next(err);
	});
};

/**
 * Verify Token bedore reset password 
 */
exports.validateResetToken = function(req, res, next) {
	var seedArray = req.params.token;
		seedArray = seedArray.split('.');
	// decrypt username
	var decipher = crypto.createDecipher(algorithm,password);
	var username = decipher.update(seedArray[0],'hex','utf8');
	username += decipher.final('utf8');
	// decrypt timestamp
	var ticipher = crypto.createDecipher(algorithm,password);
	var timestamp = ticipher.update(seedArray[1],'hex','utf8');
	timestamp += ticipher.final('utf8');
	// doble check

	db.User.find({ where:{Username:username, Reset_Password_Token:seedArray[2]}}).success(function(user){
		if(!user) {
        	return res.status(203).send({message: 'Password reset token is invalid or has expired.'});			
		}else{
			var now = parseInt(Date.now());
			var hours = parseInt(config.resetPasswordExpires);
			var plus = hours*60*60*1000;
			var expires = parseInt(timestamp) + plus;
			if (now>expires){
				return res.status(203).send({message: 'Password reset token is invalid or has expired.'});
			}else{
				return res.status(200).send();
			}
		}
    });

	/*
	db.User.find({ where:{resetPasswordToken: req.params.token,resetPasswordExpires: {$gt: Date.now()}}}).success(function(user){
		if (!user) {
			return res.redirect('/#!/password/reset/invalid');
		}
	res.redirect('/#!/password/reset/' + req.params.token);
	*/

};

/**
 * Reset password POST from email token
 */
exports.reset = function(req, res, next) {
	var params = req.body.newPassword;
	var seedArray = req.params.token;
		seedArray = seedArray.split('.');
	// decrypt username
	var decipher = crypto.createDecipher(algorithm,password);
	var username = decipher.update(seedArray[0],'hex','utf8');
	username += decipher.final('utf8');
	// decrypt timestamp
	var ticipher = crypto.createDecipher(algorithm,password);
	var timestamp = ticipher.update(seedArray[1],'hex','utf8');
	timestamp += ticipher.final('utf8');
	// double check
	if (req.body.newPassword!==req.body.verifyPassword){
		return res.status(400).send({message: 'Passwords do not match'});
	}
	db.User.find({ where:{Username:username, Reset_Password_Token:seedArray[2]}}).success(function(user){
		if(!user) {
        	return res.status(400).send({message: 'Password reset token is invalid or has expired.'});			
		}else{
			var now = parseInt(Date.now());
			var hours = parseInt(config.resetPasswordExpires);
			var plus = hours*60*60*1000;
			var expires = parseInt(timestamp) + plus;
			if (now>expires){
				return res.status(400).send({message: 'Password reset token is invalid or has expired.'});
			}else{
				/*
				 * Expiring token
				 */
				var random = String(Math.random(20));
				cipher = crypto.createCipher(algorithm,password);
				var cryptedNewToken = cipher.update(random,'utf8','hex');
				cryptedNewToken += cipher.final('hex');
				user.Password = req.body.newPassword;
				// Salt process
				user.Salt = user.generateSalt(req.body.newPassword);
				user.Password = user.generateHash(req.body.newPassword, user.Salt);
				user.Reset_Password_Token = cryptedNewToken;
				user.save(function(err) {
					if (err) {
						return res.status(400).send({message: errorHandler.getErrorMessage(err)});
					}
				});
				/*
				 * Send email
				 */
				// step 1.- instance nodemailer
				var transport = nodemailer.createTransport({
				    service: config.mailer.options.service,
				    auth: {
				        user: config.mailer.options.auth.user,
				        pass: config.mailer.options.auth.pass
				    }
				});
				// step 2.- compile template
				res.render('templates/reset-password-confirm-email', {
					name: user.First_Name,
					appName: config.app.title
				}, function(err, emailHTML) {
					// step 3.- set data
					var mailOptions = {
					    from: config.mailer.from,
					    to: user.Email,
					    subject: 'Your password has been changed',
					    html: emailHTML
					};
					// step 4.- send mail with defined transport object
					transport.sendMail(mailOptions, function(error, info){
					    if(error){
					        console.log(error);
					    }else{
					    	console.log('Your password has been changed');

							req.login(user, function(err) {
								if (err) {
									var text = 'Sign in [' + err + '] [' + req.ip + ']';  
									Log.error(text);

									res.status(400).send(err);
								} else {
									res.jsonp(user);
								}
							});
					    	//res.status(200).send(user);

					    }
					});
				});	
			}
		}
    });
};

/**
 * Change Password
 */
exports.changePassword = function(req, res) {
	// Init Variables
	var passwordDetails = req.body;

	if (req.user) {
		if (passwordDetails.newPassword) {
			db.User.find({ where:{id: req.user.id}}).success(function(user,err){
				if (!err && user) {
							if (user.authenticate(passwordDetails.currentPassword)) {
								if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
									// Salt
									user.Salt = user.generateSalt(passwordDetails.newPassword);
									user.Password = user.generateHash(passwordDetails.newPassword, user.Salt);
                                    user.Last_Changed_By = passwordDetails.Last_Changed_By;
									user.save(function(err) {
										if (err) {
											return res.status(400).send({
												message: errorHandler.getErrorMessage(err)
											});
										} else {
											req.login(user, function(err) {
												if (err) {
													res.status(400).send(err);
												} else {
													return res.status(200).send({message: 'Password changed successfully'});
												}
											});
										}
									});
									/*
									 * Send email											 
									 */
									// step 1.- instance nodemailer
									var transport = nodemailer.createTransport({
									    service: config.mailer.options.service,
									    auth: {
									        user: config.mailer.options.auth.user,
									        pass: config.mailer.options.auth.pass
									    }
									});
									// step 2.- compile template
									res.render('templates/reset-password-confirm-email', {
										name: user.First_Name,
										appName: config.app.title
									}, function(err, emailHTML) {
										// step 3.- set data
										var mailOptions = {
										    from: config.mailer.from,
										    to: user.Email,
										    subject: 'Your password has been changed',
										    html: emailHTML
										};
										// step 4.- send mail with defined transport object
										transport.sendMail(mailOptions, function(error, info){
										    if(error){
										        console.log(error);
										    }else{
										    	console.log('Your password has been changed');
										    	res.status(200).send({message: 'Your password has been changed'});
										    }
										});
									});	
								} else {
									res.status(400).send({
										message: 'Passwords do not match'
									});
								}
							} else {
								res.status(400).send({
									message: 'Current password is incorrect'
								});
							}
						} else {
							res.status(400).send({
								message: 'User is not found'
							});
						}
		    });
		} else {
			res.status(400).send({
				message: 'Please provide a new password'
			});
		}
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};
