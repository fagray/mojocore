'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors'),
	passport = require('passport'),
	db = require('../../../config/sequelize'),
	config    = require('./../../../config/config'),
	nodemailer = require('nodemailer'),	
	log4js = require('log4js'),
	Q =  require('q'),
	crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq',
    cipher;

/**
 * Log4js need be configured
 * Remember include -> config = require('./../../config/config')
 */
log4js.configure(config.logging);
var Log = log4js.getLogger('user'); // "user" represents category of log or type of log

var checkEmail = function(email){
	var deferred = Q.defer();
	db.User.find({ where:{Email:email}}).success(function(data){
		if(!data) {
			deferred.resolve(false);
        } else {
        	deferred.resolve(true); // keeping callback alive for emit a error message
        }
    });
	return deferred.promise;
};

var checkUsername = function(username){
	var deferred = Q.defer();
	db.User.find({ where:{Username:username}}).success(function(data){
		if(!data) {
			deferred.resolve(false);
        } else {
        	deferred.resolve(true); 
        	//deferred.reject(true);
        }
    });
	return deferred.promise;
};

var checkBoardingId = function(boarding_id){
	var deferred = Q.defer();
	db.User.find({ where:{Boarding_ID:boarding_id}}).success(function(data){
		if(!data) {
			deferred.resolve(false);
        } else {
        	deferred.resolve(true); 
        	//deferred.reject(true);
        }
    });
	return deferred.promise;
};

var checkUserId = function(user_id){
	var deferred = Q.defer();
	db.Account.find({ where:{user_Id:user_id}}).success(function(data){
		if(!data) {
			deferred.resolve(false);
        } else {
        	deferred.resolve(true); 
        }
    });
	return deferred.promise;
};

try {
	/**
	* Chameleon
	*/
	exports.chameleonate = function(req,res,next){
		db.User.find({ where: {Boarding_ID: req.body.bid}}).success(function(userobj){
			req.logout();
			passport.authenticate('local', function(err, user, info) {
	   			if (err || !userobj) {
					res.status(400).send(info);
				} else {
					// Remove sensitive data before login
					userobj.Password = undefined;
					userobj.Salt = undefined;

					var text = 'Chameleon Sign in [' + userobj.User_Name + '] [' + req.ip + ']';  
					req.login(userobj, function(err) {
						if (err) {

							var text = 'Chameleon Sign in [' + err + '] [' + req.ip + ']';  
							Log.error(text);

							res.status(400).send(err);
						} else {
							res.jsonp(userobj);
						}
					});
				}
			})(req, res, next);         
	    }).error(function(err){
	        return next(err);
	    });
	};
	/**
	 * Signup
	 */
	exports.signup = function(req, res) {
		// For security measurement we remove the roles from the req.body object
		delete req.body.Roles;

		// keeping a closure alive
		checkEmail(req.body.Email)
			.then(function (data) {
				if (data){
					res.status(400).send({message: 'Email is repeated'});
					return false;
				}
				return checkUsername(req.body.Username).then(function (data) {
					if (data){
						res.status(400).send({message: 'Username is repeated'});
						return false;
					}else{

						/*
						Note: this part is intact
						*/

						// Init Variables
						var user = db.User.build(req.body);
						var message = null;

						// Add missing user fields
						//user.provider = 'local';
						user.Display_Name = user.First_Name + ' ' + user.Last_Name;
						user.save().success(function(){
							user.Password = undefined;
							user.Salt = undefined;
							req.login(user, function(err) {
								if (err) {
									res.status(400).send(err);
								} else {
									res.jsonp(user);
								}
							});
						}).error(function(err){
							return res.status(400).send({
									message: errorHandler.getErrorMessage(err)
								});
						});

						/*
						/ END
						*/

					}
			});
		});
	};

	exports.boardmojoCheck = function(req, res) {
		checkEmail(req.body.email)
			.then(function (data) {
				if (data){
					res.status(200).send({exist: true, message: 'Email is repeated'});
					return false;
				}
				return checkUsername(req.body.Username).then(function (data) {
					if (data){
						res.status(200).send({exist: true, message: 'Username is repeated'});
						return false;
					}else{
						res.status(200).send({exist: false, message: ''});
						return false;
					}
				});
			});
	};

	exports.boardmojo = function(req, res) {
		// For security measurement we remove the roles from the req.body object
		delete req.body.Roles;

		// keeping a closure alive
		checkEmail(req.body.Email)
			.then(function (data) {
				if (data){
					res.status(400).send({message: 'Email is repeated'});
					return false;
				}
				return checkUsername(req.body.User_Name).then(function (data) {
					if (data){
						res.status(400).send({message: 'Username is repeated'});
						return false;
					}
					return checkBoardingId(req.body.Boarding_ID).then(function (data) {
						if (data){
							res.status(400).send({message: 'Boarding is repeated'});
							return false;
						}else{

							/*
							Note: this part is intact
							*/

							// Init Variables
							//var user = new User(req.body);
							var user = db.User.build(req.body);
							var message = null;

							// Add missing user fields
							//user.provider = 'local';
							user.Display_Name = user.First_Name + ' ' + user.Last_Name;
							var passwordRandom = Math.round(Math.floor(Math.random()*90000) + 10000);
							user.Salt = user.generateSalt(passwordRandom);
							user.Password = user.generateHash(passwordRandom, user.Salt);
							user.save().success(function(){
								user.Password = undefined;
								user.Salt = undefined;
								return res.jsonp(user);
							}).error(function(err){
								return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
							});
						}
					});
				});
			});
	};

	exports.boardmojoadmin = function(req, res) {
		// For security measurement we remove the roles from the req.body object
		//delete req.body.roles;

		// keeping a closure alive
		checkEmail(req.body.Email)
			.then(function (data) {
				if (data){
					res.status(400).send({message: 'Email is repeated'});
					return false;
				}
				return checkUsername(req.body.Username).then(function (data) {
					if (data){
						res.status(400).send({message: 'Username is repeated'});
						return false;
					}
					else{
							/*
							Note: this part is intact
							*/

							// Init Variables
							//var user = new User(req.body);
							var user = db.User.build(req.body);
							var message = null;
							// Add missing user fields
							//user.provider = 'local';
							//user.Roles = req.param;
							user.Advanced_Report = 1;
							user.Transaction_Report = 1;
							user.Display_Name = user.First_Name + ' ' + user.Last_Name;
							var passwordRandom = Math.round(Math.floor(Math.random()*90000) + 10000);
							user.Salt = user.generateSalt(passwordRandom);
							user.Password = user.generateHash(passwordRandom, user.Salt);
							user.save().success(function(){
								user.Password = undefined;
								user.Salt = undefined;
								var random = String(Math.random(20));
								cipher = crypto.createCipher(algorithm,password);
								var cryptedNewToken = cipher.update(random,'utf8','hex');
								cryptedNewToken += cipher.final('hex');
								user.Reset_Password_Token = cryptedNewToken;
								user.Reset_Password_Expires = Date.now() + 3600000; // 1 hour
								user.save();
								/*
								 * Send email
								 */

								// step 1.- instance nodemailer
								var transport = nodemailer.createTransport({
								    service: config.mailer.options.service,
								    auth: {
								        user: config.mailer.options.auth.user,
								        pass: config.mailer.options.auth.pass
								    }
								});
								// step 2.- encrypting username and seed
								var seed = String(Date.now());
								cipher = crypto.createCipher(algorithm,password);
								var cryptedSeed = cipher.update(seed,'utf8','hex');
								cryptedSeed += cipher.final('hex');

								// encrypt username compile template				
								cipher = crypto.createCipher(algorithm,password);
								var cryptedUsername = cipher.update(user.Username,'utf8','hex');
								cryptedUsername += cipher.final('hex');
								var port = config.port;
                                var root = req.protocol + '://' + req.hostname  + ( port === 443 ? '' : ':' + port ) + '/#!/password/reset/' + cryptedUsername + '.' + cryptedSeed + '.' + user.Reset_Password_Token;
								res.render('templates/boardadmin-signup-confirm-email', {
									name: user.First_Name,
									appName: config.app.title,
                                    username: user.Username,
									url: root
								}, function(err, emailHTML) {
									// step 3.- set data
									var mailOptions = {
									    from: config.mailer.from,
									    to: user.Email,
									    subject: 'Create password',
									    html: emailHTML
									};

									// step 4.- send mail with defined transport object
									transport.sendMail(mailOptions, function(error, info){
									    if(error){
									        console.log(error);
									    }else{
									    	console.log('Create password done!');
									    }
									});
								});	

								return res.jsonp(user);
							}).error(function(err){
								return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
							});
						}
					});
				});
			};
	/**
 * Update user details
 */
	 exports.getboardmerchant = function(req, res, next, bid) {
	    db.Account.find({ where: {boarding_id: bid}}).success(function(account){
	        if(!account) {
	        	req.boardmerchantdata = {clean:'clean'};
	        	return next();   
	        } else {
	           req.boardmerchantdata = account;
	            return next();            
	        }
	    }).error(function(err){
	        return next(err);
	    });
	 };

	exports.boardmerchantshow = function(req, res) {
	    return res.jsonp(req.boardmerchantdata);
	};
	 exports.updateboardmerchant = function(req, res) {
	    // create a new variable to hold the boarding that was placed on the req object.
	    var boardmerchantd = req.boardmerchantdata;
	    boardmerchantd.updateAttributes({
	        username: req.body.Username,
	        password: req.body.Password,
	        boarding_id: req.body.Boarding_ID,
	    }).success(function(a){
	        return res.jsonp(a);
	    }).error(function(err){
	        return res.render('error', {
	            error: err, 
	            status: 500
	        });
	    });
	};

	exports.boardmerchant = function(req, res) {
    // save and return and instance of boarding on the res object. 
	db.User.find({ where:{Boarding_ID:req.body.Boarding_ID}}).success(function(newUser){
		if(newUser) {
			var random = String(Math.random(20));
			cipher = crypto.createCipher(algorithm,password);
			var cryptedNewToken = cipher.update(random,'utf8','hex');
			cryptedNewToken += cipher.final('hex');
			newUser.Password = req.body.newPassword;
			newUser.Reset_Password_Token = cryptedNewToken;
			newUser.Reset_Password_Expires = Date.now() + 3600000; // 1 hour
			newUser.save();

			/*
			 * Send email
			 */

			// step 1.- instance nodemailer
			var transport = nodemailer.createTransport({
			    service: config.mailer.options.service,
			    auth: {
			        user: config.mailer.options.auth.user,
			        pass: config.mailer.options.auth.pass
			    }
			});
			// step 2.- encrypting username and seed
			var seed = String(Date.now());
			cipher = crypto.createCipher(algorithm,password);
			var cryptedSeed = cipher.update(seed,'utf8','hex');
			cryptedSeed += cipher.final('hex');

			// encrypt username compile template				
			cipher = crypto.createCipher(algorithm,password);
			var cryptedUsername = cipher.update(newUser.Username,'utf8','hex');
			cryptedUsername += cipher.final('hex');
			var port = config.port;
			var root = req.protocol + '://' + req.hostname  + ( port === 443 ? '' : ':' + port ) + '/#!/password/reset/' + cryptedUsername + '.' + cryptedSeed + '.' + newUser.Reset_Password_Token;
			res.render('templates/reset-password-email', {
				name: newUser.First_Name,
				appName: config.app.title,
				url: root
			}, function(err, emailHTML) {
				// step 3.- set data
				var mailOptions = {
				    from: config.mailer.from,
				    to: newUser.Email,
				    subject: 'Reset password',
				    html: emailHTML
				};

				// step 4.- send mail with defined transport object
				transport.sendMail(mailOptions, function(error, info){
				    if(error){
				        console.log(error);
				    }else{
				    	console.log('Reset password done!');
				    }
				});
			});	
        }
    });



    db.Account.create(req.body).success(function(account,err){
        if(!account){
        	return res.jsonp(err);
        } else {
            return res.jsonp(account);
        }
	    }).error(function(err){
	       return res.jsonp(err);
	    });
	};

	/**
	 * Signin after passport authentication
	 */
	exports.signin = function(req, res, next) {
		passport.authenticate('local', function(err, user, info) {
			if (err || !user) {
				res.status(400).send(info);
			}else {
				// Remove sensitive data before login
				user.Password = undefined;
				user.Salt = undefined;

				var text = 'Sign in [' + user.Username + '] [' + req.ip + ']';  
				Log.debug(text);

				req.login(user, function(err) {
					if (err) {
						var text = 'Sign in [' + err + '] [' + req.ip + ']';  
						Log.error(text);

						res.status(400).send(err);
					} else {
						res.jsonp(user);
					}
				});
			}
		})(req, res, next);
	};

	/**
	 * Signout
	 */
	exports.signout = function(req, res) {
		//var text = 'Sign out [' + req.user.Username + '] [' + req.ip + ']';  
		//Log.debug(text);
		req.logout();
		res.redirect('/#!');
	};

	/**
	 * OAuth callback
	 */
	exports.oauthCallback = function(strategy) {
		return function(req, res, next) {
			passport.authenticate(strategy, function(err, user, redirectURL) {
				if (err || !user) {
					return res.redirect('/#!/signin');
				}
				req.login(user, function(err) {
					if (err) {
						return res.redirect('/#!/signin');
					}

					return res.redirect(redirectURL || '/');
				});
			})(req, res, next);
		};
	};

	/**
	 * Helper function to save or update a OAuth user profile
	 */

	exports.saveOAuthUserProfile = function(req, providerUserProfile, done) {
	};

	/**
	 * Remove OAuth provider
	 */
	exports.removeOAuthProvider = function(req, res, next) {
		var user = req.user;
		var provider = req.param('provider');

		if (user && provider) {
			// Delete the additional provider
			if (user.additionalProvidersData[provider]) {
				delete user.additionalProvidersData[provider];

				// Then tell mongoose that we've updated the additionalProvidersData field
				user.markModified('additionalProvidersData');
			}
			user.save().success(function(){
				req.login(user, function(err) {
						if (err) {
							res.status(400).send(err);
						} else {
							res.jsonp(user);
						}
					});
			}).error(function(err){
				return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
			});
		}
	};
} catch (err) {
	var text = '[' + err + ']';  
	Log.fatal(text);
}
