'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../errors'),
    passport = require('passport'),
    db = require('../../../config/sequelize'),
    nodemailer = require('nodemailer'),    
    crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq',
    config = require('../../../config/config'),
    cipher;

var iterations = 1000, //2500;
    keylen = 20; //512;
    

/**
 * Update user details
 */
 exports.getUserUpdate = function(req, res, next, bid) {    
    db.User.find({ where: {Boarding_ID: bid}}).success(function(user){
        if(!user) {
            return next(new Error('Failed to load User with boarding Id ' + bid));
        } else {
            user.Password = undefined;
            user.Salt = undefined;
            req.getUser = user;
            return next();            
        }
    }).error(function(err){
        return next(err);
    });
 };
 /**
 * Update user details
 */
 exports.getAdminUpdate = function(req, res, next,bid) {
    db.User.find({ where: {id: bid}}).success(function(user){
        if(!user) {
            return next(new Error('Failed to load User with boarding Id ' + bid));
        } else {
            user.Password = undefined;
            user.Salt = undefined;
            req.getAdmin = user;
            return next();            
        }
    }).error(function(err){
        return next(err);
    });
 };
 /**
 * List of boardings
 */

    exports.all = function(req, res) {
        db.User.findAll({ where: ['Roles = ? or Roles = ?','admin','super']},{attributes:['id','First_Name','Last_Name','Username','Email','Roles','Is_Active']}).success(function(users){
            return res.jsonp(users);
        }).error(function(err){
            return res.render('error', {
                error: err,
                status: 500
            });
        });
    };

  exports.UserShow = function(req, res) {
     return res.jsonp(req.getUser);
  };

  exports.AdminShow = function(req, res) {
     return res.jsonp(req.getAdmin);
  };

 exports.adminsupdate = function(req, res) {
    // create a new variable to hold the boarding that was placed on the req object.
    var getAdmin = req.getAdmin;

    getAdmin.updateAttributes({
        Is_Active: req.body.Is_Active,
        Roles: req.body.Roles,
        Last_Changed_By: req.body.Last_Changed_By
    }).success(function(a){
        return res.jsonp(a);
    }).error(function(err){
        return res.render('error', {
            error: err, 
            status: 500
        });
    });
};

exports.usersupdate = function(req, res) {
    // create a new variable to hold the boarding that was placed on the req object.
    var getUser = req.getUser,
        Virtual_Terminal = req.body.Virtual_Terminal === false ? null : req.body.Virtual_Terminal,
        Customer_Vault = req.body.Customer_Vault === false ? null : req.body.Customer_Vault,
        Items = req.body.Items === false ? null : req.body.Items,
        Terminal_Sale = req.body.Terminal_Sale === false ? null : req.body.Terminal_Sale, 
        Terminal_Authorize = req.body.Terminal_Authorize === false ? null : req.body.Terminal_Authorize, 
        Terminal_Capture = req.body.Terminal_Capture === false ? null : req.body.Terminal_Capture, 
        Terminal_Void = req.body.Terminal_Void === false ? null : req.body.Terminal_Void, 
        Terminal_Refund = req.body.Terminal_Refund === false ? null : req.body.Terminal_Refund, 
        Terminal_Credit = req.body.Terminal_Credit === false ? null : req.body.Terminal_Credit, 
        Customer_Add = req.body.Customer_Add === false ? null : req.body.Customer_Add, 
        Customer_List = req.body.Customer_List === false ? null : req.body.Customer_List,
        Transaction_Report = req.body.Transaction_Report === false ? null : req.body.Transaction_Report, 
        Advanced_Report = req.body.Advanced_Report === false ? null : req.body.Advanced_Report,
        Integration = req.body.Integration === false ? null : req.body.Integration,
        Is_Active = req.body.Is_Active === false ? null : req.body.Is_Active,
        Enable_Nation = req.body.Enable_Nation === false ? null : req.body.Enable_Nation,
        Recurring_Payments = req.body.Recurring_Payments === false ? null : req.body.Recurring_Payments,
        Plan_Add = req.body.Plan_Add === false ? null : req.body.Plan_Add, 
        Plan_List = req.body.Plan_List === false ? null : req.body.Plan_List,
        Subscription_Add = req.body.Subscription_Add === false ? null : req.body.Subscription_Add, 
        Subscription_List = req.body.Subscription_List === false ? null : req.body.Subscription_List,
        Customer_Expiry = req.body.Customer_Expiry === false ? null : req.body.Customer_Expiry,
        Easy_Buttons = req.body.Easy_Buttons === false ? null : req.body.Easy_Buttons,
        Button_Add = req.body.Button_Add === false ? null : req.body.Button_Add, 
        Button_List = req.body.Button_List === false ? null : req.body.Button_List,
        Last_Changed_By = req.body.Last_Changed_By;
        
    if (Integration){
        var passphrase = getUser.Username+getUser.Boarding_ID,
            salt = new Buffer(crypto.randomBytes(keylen)).toString('hex'),
            generatedHash = crypto.pbkdf2Sync(passphrase, salt, iterations, keylen).toString('hex');
        db.Api_Key.find({ where: {Boarding_ID: getUser.Boarding_ID,Is_Active:0}}).success(function(apiKey){
            if(apiKey) {
                apiKey.updateAttributes({
                    Is_Active:1
                }).success(function(a){
                }).error(function(err){
                });
            }else{
                db.Api_Key.create({
                    Token:generatedHash,
                    Boarding_ID:getUser.Boarding_ID
                }).success(function(apiKey,err){ 
                }).error(function(err){
                });
            }
        }).error(function(err){
        });
    }else{
         db.Api_Key.find({ where: {Boarding_ID: getUser.Boarding_ID,Is_Active:1}}).success(function(apiKey){
            if(apiKey) {
                apiKey.updateAttributes({
                    Is_Active:0
                }).success(function(a){
                }).error(function(err){
                });
            }
        }).error(function(err){
        });
    }
    
    var emailActivation = req.body.emailActivation === false ? null : req.body.emailActivation;
    if(emailActivation){
        var newUser = getUser;
        var message = null;
    
        var random = String(Math.random(20));
        cipher = crypto.createCipher(algorithm,password);
        var cryptedNewToken = cipher.update(random,'utf8','hex');
        cryptedNewToken += cipher.final('hex');
            /*
             * Send email
             */
    
            // step 1.- instance nodemailer
            var transport = nodemailer.createTransport({
                service: config.mailer.options.service,
                auth: {
                    user: config.mailer.options.auth.user,
                    pass: config.mailer.options.auth.pass
                }
            });
            // step 2.- encrypting username and seed
            var seed = String(Date.now());
            cipher = crypto.createCipher(algorithm,password);
            var cryptedSeed = cipher.update(seed,'utf8','hex');
            cryptedSeed += cipher.final('hex');
    
            // encrypt username compile template                
            cipher = crypto.createCipher(algorithm,password);
            var cryptedUsername = cipher.update(newUser.Username,'utf8','hex');
            cryptedUsername += cipher.final('hex');
            var port = config.port;
            var root = req.protocol + '://' + req.hostname  + (port === 443 ? '' : ':' + port ) + '/#!/password/reset/' + cryptedUsername + '.' + cryptedSeed;
            res.render('templates/boardmojo-signup-confirm-email', {
                name: newUser.First_Name,
                appName: config.app.title,
                username: newUser.Username,
                url: root
            }, function(err, emailHTML) {
                // step 3.- set data
                var mailOptions = {
                    from: config.mailer.from,
                    to: newUser.Email,
                    subject: 'Set password',
                    html: emailHTML
                };
    
                // step 4.- send mail with defined transport object
                transport.sendMail(mailOptions, function(error, info){
                    if(error){
                        console.log(error);
                    }else{
                        console.log('Set password done!');
                    }
                });
            }); 
        }

    getUser.updateAttributes({
        Username: req.body.Username,
        First_Name: req.body.First_Name,
        Last_Name: req.body.Last_Name,
        Display_Name: req.body.First_Name + ' ' + req.body.Last_Name,
        Email: req.body.Email,
        Virtual_Terminal: Virtual_Terminal,
        Customer_Vault: Customer_Vault,
        Items: Items,
        Terminal_Sale: Terminal_Sale,
        Terminal_Authorize: Terminal_Authorize,
        Terminal_Capture: Terminal_Capture,
        Terminal_Void: Terminal_Void,
        Terminal_Refund: Terminal_Refund,
        Terminal_Credit: Terminal_Credit,
        Customer_Add: Customer_Add,
        Customer_List: Customer_List,
        Transaction_Report: Transaction_Report,
        Advanced_Report: Advanced_Report,
        Integration: Integration,
        Is_Active: Is_Active,
        Enable_Nation: Enable_Nation,
        Recurring_Payments: Recurring_Payments,
        Plan_Add: Plan_Add, 
        Plan_List: Plan_List,
        Subscription_Add: Subscription_Add, 
        Subscription_List: Subscription_List,
        Customer_Expiry: Customer_Expiry,
        Easy_Buttons: Easy_Buttons,
        Button_Add: Button_Add, 
        Button_List: Button_List,
        Last_Changed_By: req.body.Last_Changed_By
    }).success(function(a){
        return res.jsonp(a);        
    }).error(function(err){
        return res.render('error', {
            error: err, 
            status: 500
        });
    });
};
exports.update = function(req, res) {
    // Init Variables
    var user = req.user;
    var message = null;

    // For security measurement we remove the roles from the req.body object
    delete req.body.Roles;

    if (user) {
        // Merge existing user
        user = _.extend(user, req.body);
        //user.updated = Date.now();
        user.Display_Name = user.First_Name + ' ' + user.Last_Name;
        user.save().success(function(user) {
            return res.jsonp(user);
        }).error(function(err){
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                req.login(user, function(err) {
                    if (err) {
                        res.status(400).send(err);
                    } else {
                        res.jsonp(user);
                    }
                });
            }
        });
    } else {
        res.status(400).send({
            message: 'User is not signed in'
        });
    }
};

/**
 * Send User
 */
exports.me = function(req, res) {
    res.jsonp(req.user || null);
};
