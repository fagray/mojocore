'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	db = require('../../../config/sequelize');

/**
 * User middleware
 */
exports.userByID = function(req, res, next, id) {
	db.User.find({ where:{id: id}}).success(function(user){
		if(!user) {
            return next(new Error('Failed to load User ' + id));
        } else {
            req.profile = user;
            return next();            
        }
    }).error(function(err){
        return next(err);
    });
};

/**
 * Require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
	if (!req.isAuthenticated()) {
		return res.status(401).send({
			message: 'User is not logged in'
		});
	}

	next();
};
/**
 * User authorizations routing middleware
 */
exports.isAdmin = function(req, res, next) {
	if (!req.isAuthenticated()) {
		return res.status(401).send({
			message: 'User is not logged in'
		});
	}else{
		if ((req.user.Roles==='admin')||(req.user.Roles==='super')) {
			return next();
		} else {
			return res.status(403).send({
				message: 'User is not authorized'
			});
		}
	}
};
/**
 * User authorizations routing middleware
 */
exports.hasAuthorization = function(roles) {
	var _this = this;
	return function(req, res, next) {
		_this.requiresLogin(req, res, function() {
			if (_.intersection(req.user.Roles, roles).length) {
				return next();
			} else {
				return res.status(403).send({
					message: 'User is not authorized'
				});
			}
		});
	};
};