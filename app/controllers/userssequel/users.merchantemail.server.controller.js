'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors'),
	passport = require('passport'),
	db = require('../../../config/sequelize'),
	nodemailer = require('nodemailer'),	
	crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq',
	config = require('../../../config/config'),
 	cipher,
    request = require('request');

var iterations = 1000, //2500;
	keylen = 20; //512;



/**
 * Send email
 */
exports.emailreceipt = function(req, res) {
    var err = true;
    // added boardingID to find the Merchant Name MC-691
    var boardingID = (req.param('boardingID'));
    var merchantName = (req.param('merchantName'));
    var transactionId = (req.param('transactionId'));
    var processorId = (req.param('processorId'));
    var emailList = (req.param('emailList'));
    var mailOptions = {};
    // adding this query to find the Merchant Name
    db.Boarding.findAll({where: ['id=?', boardingID]})
    .success(function(boarding) {
        // if logged in as an admin, there is no boarding id so 
        // the name in the email will be the admins Display Name.
        if (!boarding) {
        } else {
            if (Object.keys(boarding).length===0) {
            } else {
                merchantName = boarding[0].dataValues.Merchant_Name;
            }
        }
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
    db.Transaction_History.findAll({ where: ['processor_id=? and transaction_id=?', processorId, transactionId]})
    .success(function(transactions){
        if (!transactions){
        }else{
            if (Object.keys(transactions).length===0){
            }else{
				var shipping_title = '';
				var shipping_block = '';
 				if (!(transactions[0].dataValues.shipping_first_name === "-" &&
					  transactions[0].dataValues.shipping_last_name === "-" &&
					  transactions[0].dataValues.shipping_address_1 === "-" &&
                      transactions[0].dataValues.shipping_address_2 === "-" &&
					  transactions[0].dataValues.shipping_city === "-" &&
                      transactions[0].dataValues.shipping_state === "-" &&
                      transactions[0].dataValues.shipping_postal_code === "-" &&
                      transactions[0].dataValues.shipping_country === "-" &&
                      transactions[0].dataValues.shipping_phone === "-")) {
						shipping_title = "Shipping Information";
					    shipping_block = transactions[0].dataValues.shipping_first_name + " " + transactions[0].dataValues.shipping_last_name + "\<br /\>" + 
                        transactions[0].dataValues.shipping_address_1 + "\<br /\>"; 
                	    if (transactions[0].dataValues.shipping_address_2 !== "-") {
						    shipping_block += transactions[0].dataValues.shipping_address_2 + "\<br /\>";
					    }
                	    shipping_block += transactions[0].dataValues.shipping_city + ", " + 
				                          transactions[0].dataValues.shipping_state + " " + 
                			    	      transactions[0].dataValues.shipping_postal_code + "\<br /\>" + 
				                          transactions[0].dataValues.shipping_country + "\<br /\>" + 
                				          transactions[0].dataValues.shipping_phone + "\<br /\>";

				}	
                var billing_block = transactions[0].dataValues.first_name + " " + transactions[0].dataValues.last_name + "\<br /\>" + 
                transactions[0].dataValues.address_1 + "\<br /\>"; 
                if (transactions[0].dataValues.address_2 !== "-") {
					billing_block+= transactions[0].dataValues.address_2 + "\<br /\>";
				}
                billing_block += transactions[0].dataValues.city + ", " + 
                    			 transactions[0].dataValues.state + " " + 
                    			 transactions[0].dataValues.postal_code + "\<br /\>" + 
                    			 transactions[0].dataValues.country + "\<br /\>" + 
                    			 transactions[0].dataValues.phone + "\<br /\>";
                // billing date changed for month and date MC-691
                var billing_date = transactions[0].dataValues.date.substring(4,6) + "/" + transactions[0].dataValues.date.substring(6,8) + "/" + transactions[0].dataValues.date.substring(0,4);
                var transaction_type = transactions[0].dataValues.action_type.toUpperCase();

                res.render('templates/transaction_receipt_email', {
                    MOJOPAY_MERCHANT_NAME1: merchantName,
					DATE: billing_date,
                    FIRSTNAME: transactions[0].dataValues.first_name,
                    BILLING_BLOCK: billing_block,
                    SHIPPING_TITLE: shipping_title,
                    SHIPPING_BLOCK: shipping_block,
                    TRANSACTIONID: transactions[0].dataValues.transaction_id,
                    CARDTYPE: transactions[0].dataValues.cc_number,
                    AMOUNT: transactions[0].dataValues.amount,
                    MOJOPAY_MERCHANT_NAME2: merchantName
                }, function(err, emailHTML) {
                    emailHTML = emailHTML.replace(/&gt;/g, ">").replace(/&lt;/g, "<");

                    var smtpTransport = nodemailer.createTransport(config.mailer.options);

                    if (typeof emailList === "string") {
                        // mail to a single address
                        mailOptions = {
                            from: config.mailer.from,
                            to: emailList, 
                            subject: 'Receipt from ' + merchantName,
                            html: emailHTML
                        };
//console.log(mailOptions)
                        smtpTransport.sendMail(mailOptions, function(err) {
/*
                            if(err){
                                console.log(err);
                            }else{
                                console.log("Message sent");
                            }
*/
                        }); // smtpTransport.sendMail
                    } else {
                        // mail to multiple addresses
                        for (var i = 0; i < emailList.length; i++) {
                            mailOptions = {
                                from: config.mailer.from,
                                to: emailList[i], 
                                subject: 'Receipt from ' + merchantName,
                                html: emailHTML
                            };
//console.log(mailOptions)
                            smtpTransport.sendMail(mailOptions, function(err) {
/*
                                if(err){
                                    console.log(err);
                                }else{
                                    console.log("Message sent");
                                }
*/
                            }); // smtpTransport.sendMail
                        } // for (var i = 0; i < emailList.length; i++) 
                    } // 
                }); // res.render
                res.send({
                    message: 'The receipt has been sent.'
                });
            }
        }
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

exports.printreceipt = function(req, res) {
    var err = true;
    // added boardingID to find the Merchant Name MC-691
    var boardingID = (req.param('boardingID'));
    var merchantName = (req.param('merchantName'));
    var transactionId = (req.param('transactionId'));
    var processorId = (req.param('processorId'));
    // adding this query to find the Merchant Name
    db.Boarding.findAll({where: ['id=?', boardingID]})
    .success(function(boarding) {
        // if logged in as an admin, there is no boarding id so 
        // the name in the print will be the admins Display Name.
        if (!boarding) {
        } else {
            if (Object.keys(boarding).length===0) {
            } else {
                merchantName = boarding[0].dataValues.Merchant_Name;
            }
        }
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
    db.Transaction_History.findAll({ where: ['processor_id=? and transaction_id=?', processorId, transactionId]})
    .success(function(transactions){
        if (!transactions){
            return;
        }else{
            if (Object.keys(transactions).length===0){
                return;
            }else{
               var shipping_title = '';
				var shipping_block = '';
 				if (!(transactions[0].dataValues.shipping_first_name === "-" &&
					  transactions[0].dataValues.shipping_last_name === "-" &&
					  transactions[0].dataValues.shipping_address_1 === "-" &&
                      transactions[0].dataValues.shipping_address_2 === "-" &&
					  transactions[0].dataValues.shipping_city === "-" &&
                      transactions[0].dataValues.shipping_state === "-" &&
                      transactions[0].dataValues.shipping_postal_code === "-" &&
                      transactions[0].dataValues.shipping_country === "-" &&
                      transactions[0].dataValues.shipping_phone === "-")) {
						shipping_title = "Shipping Information";
						shipping_block = transactions[0].dataValues.shipping_first_name + " " + transactions[0].dataValues.shipping_last_name + "\<br /\>" + 
				        transactions[0].dataValues.shipping_address_1 + "\<br /\>";
						if (transactions[0].dataValues.shipping_address_2 !== "-") {
							shipping_block += transactions[0].dataValues.shipping_address_2 + "\<br /\>";
						}
                		shipping_block += transactions[0].dataValues.shipping_city + ", " + 
				            	          transactions[0].dataValues.shipping_state + " " + 
                					      transactions[0].dataValues.shipping_postal_code + "\<br /\>" + 
				                    	  transactions[0].dataValues.shipping_country + "\<br /\>" + 
                				    	  transactions[0].dataValues.shipping_phone + "\<br /\>";
				}	
                var billing_block = transactions[0].dataValues.first_name + " " + transactions[0].dataValues.last_name + "\<br /\>" + 
                transactions[0].dataValues.address_1 + "\<br /\>";
                if (transactions[0].dataValues.address_2 !== "-") {
					billing_block += transactions[0].dataValues.address_2 + "\<br /\>";
				}
                billing_block += transactions[0].dataValues.city + ", " + 
                    			 transactions[0].dataValues.state + " " + 
                    			 transactions[0].dataValues.postal_code + "\<br /\>" + 
                    			 transactions[0].dataValues.country + "\<br /\>" + 
                       			 transactions[0].dataValues.phone + "\<br /\>";
                // billing date changed for month and date MC-691
                var billing_date = transactions[0].dataValues.date.substring(4,6) + "/" + transactions[0].dataValues.date.substring(6,8) + "/" + transactions[0].dataValues.date.substring(0,4);
                var transaction_type = transactions[0].dataValues.action_type.toUpperCase();

                res.render('templates/transaction_receipt_email', {
                    MOJOPAY_MERCHANT_NAME1: merchantName,
					DATE: billing_date,
                    FIRSTNAME: transactions[0].dataValues.first_name,
                    BILLING_BLOCK: billing_block,
                    SHIPPING_TITLE: shipping_title,
                    SHIPPING_BLOCK: shipping_block,
                    TRANSACTIONID: transactions[0].dataValues.transaction_id,
                    CARDTYPE: transactions[0].dataValues.cc_number,
                    AMOUNT: transactions[0].dataValues.amount,
                    MOJOPAY_MERCHANT_NAME2: merchantName
                }, function(err, printHTML) {
                    return res.jsonp(printHTML);
                });

            }
        }
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });

};