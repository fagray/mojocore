'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    nodemailer = require('nodemailer'),
    crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq',
    cipher,
	config = require('./../../config/config'),
    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    };
/**
 * Find payment_plan by id
 * Note: This is called every time that the parameter :paymentplanId is used in a URL.
 * Its purpose is to preload the payment_plan on the req object then call the next function.
 */
exports.allPaymentPlans = function(req,res){
    var boarding_id = req.param('boarding_id');
    db.sequelize.query('select pay.*,pro.Name,pro.Description,pro.Marketplace from Payment_Plans as pay join Products as pro on pay.Product_ID = pro.id where Boarding_ID = :boarding_id and pay.Product_ID != 0 and Is_Active = 1;',null,{ raw: true },
        { boarding_id: boarding_id }
    ).success(function(results) {
        if (!results){
            return res.jsonp({NoResults:true });
        }else{
            return res.jsonp({Response: results});
        }
    });
};

exports.isPayment_Plan = function(req,res,next){
    var product = req.params.product,
        boardingid = req.params.payment;
    db.Payment_Plan.find({ where: {Boarding_ID: boardingid, Product_ID:product}}).success(function(payment_plan){
        if(!payment_plan) {
            req.payment_plan = {clean:'clean'};
            return res.jsonp(req.payment_plan);
        } else {
            req.payment_plan = toObject(payment_plan);
                payment_plan.updateAttributes({
                    Is_Active: 1
                }).success(function(a){
                    return res.jsonp(a);
                }).error(function(err){
                    return res.render('error', {
                        error: err,
                        status: 500
                    });
                });
        }
    }).error(function(err){
        return res.jsonp(err);
    });
};
exports.paymentSplit = function(req,res){
    var boarding = req.param('boardingId');
    db.sequelize.query('select pay.*,split.Recipient,split.Rate,split.Type,split.Recipient_Type,pro.Name,pro.Description,pro.Marketplace from Payment_Plans as pay join Products as pro on pay.Product_ID = pro.id left join Split_Payment as split on (split.Product_ID = pay.Product_ID AND split.Boarding_ID = pay.Boarding_ID) where pay.Boarding_ID = :boarding_id and pay.Product_ID != 0 and Is_Active = 1;',null,{ raw: true },
        { boarding_id: boarding }
    ).success(function(results) {
            if (!results){
                return res.jsonp({NoResults:true });
            }else{
                return res.jsonp({Response: results});
            }
        }).error(function(err){
            return res.jsonp(err);
        });
};
exports.paymentPlanComplete = function(req,res){
    var product = req.param('productId'),
        verify = req.param('verify'),
        boarding = req.param('boardingId');
    if ((product === 'all')&&(boarding === 'all')){
        db.sequelize.query('select pay.*,pro.Name,pro.Description,pro.Marketplace from Payment_Plans as pay join Products as pro on pay.Product_ID = pro.id where pay.Product_ID != 0 and Is_Active = 1;',null,{ raw: true },
            { boarding_id: boarding }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
            }).error(function(err){
            return res.jsonp(err);
        });
    }else if ((product !== 'all')&&(boarding !== 'all')){
        db.sequelize.query('select pay.*,pro.Name,pro.Description,pro.Marketplace from Payment_Plans as pay join Products as pro on pay.Product_ID = pro.id where Boarding_ID = :boarding_id and pay.Product_ID = :product_id and Is_Active = 1;',null,{ raw: true },
            { boarding_id: boarding,product_id:product }
        ).success(function(results) {
                if (!results){
                    if (verify === 'getClean'){
                        return res.jsonp({clean:'clean'});
                    }else{
                        db.sequelize.query('select pay.*,pro.Name,pro.Description,pro.Marketplace from Payment_Plans as pay join Products as pro on pay.Product_ID = pro.id where Boarding_ID = :boarding_id and pay.Product_ID = 1 and Is_Active = 1;',null,{ raw: true },
                            { boarding_id: boarding }
                        ).success(function(results) {
                                if (!results){
                                    return res.jsonp({NoResults:true });
                                }else{
                                    return res.jsonp({Response: results});
                                }
                            }).error(function(err){
//                              return next(err); // JIRA MC-393 ISSUE
								return res.jsonp({NoResults:true });
                            });
                    }
                }else{
                    return res.jsonp({Response: results});
                }
            }).error(function(err){
                return res.jsonp(err);
            });
    }else if ((product !== 'all')&&(boarding === 'all')){
        db.sequelize.query('select pay.*,pro.Name,pro.Description,pro.Marketplace from Payment_Plans as pay join Products as pro on pay.Product_ID = pro.id where pay.Product_ID = :product_id and Is_Active = 1;',null,{ raw: true },
            { product_id: product }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
                }
        }).error(function(err){
                return res.jsonp(err);
        });
    }else if ((product === 'all')&&(boarding !== 'all')){
        db.sequelize.query('select pay.*,pro.Name,pro.Description,pro.Marketplace from Payment_Plans as pay join Products as pro on pay.Product_ID = pro.id where Boarding_ID = :boarding_id and pay.Product_ID != 0 and Is_Active = 1;',null,{ raw: true },
            { boarding_id: boarding }
        ).success(function(results) {
                if (!results){
                    return res.jsonp({NoResults:true });
                }else{
                    return res.jsonp({Response: results});
            }
        }).error(function(err){
                return res.jsonp(err);
        });
    }
};

exports.payment_plan = function(req, res, next, id) {
    var product = req.params.productId,
        verify = req.param('verify');
    if (product){
        if (product==='all'){
            db.Payment_Plan.findAll({ where: {Boarding_ID: id,Product_ID: {ne: 0},Is_Active:1}}).success(function(payment_plan){
                if(!payment_plan) {
                    req.payment_plan = {clean:'clean'};
                    return next();
                } else {
                    req.payment_plan = toObject(payment_plan);
                    return next();
                }
            }).error(function(err){
                return next(err);
            });
        }else{
            if (id!=='all'){
                db.Payment_Plan.find({ where: {Boarding_ID: id, Product_ID:product,Is_Active:1}}).success(function(payment_plan){
                    if(!payment_plan) {
                        if (verify === 'getClean'){
                            req.payment_plan = {clean:'clean'};
                            return next();
                        }else{
                            // if merchant have not mojo and return null try to search product=1
                            product = 1;
                            db.Payment_Plan.find({ where: {Boarding_ID: id, Product_ID:product,Is_Active:1}}).success(function(payment_plan){
                                if(!payment_plan) {
                                    req.payment_plan = {clean:'clean'};
                                    return next();
                                } else {
                                    req.payment_plan = payment_plan;
                                    return next();
                                }
                            }).error(function(err){
                                return next(err);
                            });
                        }
                    } else {
                        req.payment_plan = payment_plan;
                        return next();
                    }
                }).error(function(err){
                    return next(err);
                });
            }else{ // * 1 -> all merchant, one product
                db.Payment_Plan.findAll({ where: {Product_ID:product,Is_Active:1}}).success(function(payment_plan){
                    if(!payment_plan) {
                        req.payment_plan = {clean:'clean'};
                        return next();
                    } else {
                        req.payment_plan = toObject(payment_plan);
                        return next();
                    }
                }).error(function(err){
                    return next(err);
                });
            }
        }
    }else{
        db.Payment_Plan.find({ where: {Boarding_ID: id,Product_ID: {ne: 0},Is_Active:1}}).success(function(payment_plan){
            if(!payment_plan) {
                req.payment_plan = {clean:'clean'};
                return next();
            } else {
                req.payment_plan = payment_plan;
                return next();
            }
        }).error(function(err){
            return next(err);
        });
    }
};
/**
 * Create a payment_plan
 */
exports.create = function(req, res) {
    // augment the payment_plan by adding the UserId
    req.body.UserId = req.user.id;
    // save and return and instance of payment_plan on the res object.
        db.Payment_Plan.create(req.body).success(function(payment_plan,err){
            if(!payment_plan){
                return res.send('users/signup', {errors: err});
            } else {
                if (req.body.Marketplace === 1){
                    db.sequelize.query('INSERT INTO Split_Payment (Boarding_ID,Product_ID, Last_Changed_By) VALUES (:boarding_id,:Product_ID,:Last_Changed_By)',null,{ raw: true },
                        { boarding_id: req.body.Boarding_ID,Product_ID:req.body.Product_ID,Last_Changed_By:req.body.Last_Changed_By}
                    ).success(function(results) {
                                return res.jsonp(payment_plan);
                        }).error(function(err){
                            return res.jsonp(err);
                        });
                }else{
                   return res.jsonp(payment_plan);
                }
            }
        }).error(function(err){
            return res.send('users/signup', {
                errors: err,
                status: 500
            });
        });
};

/**
 * Update a payment_plan
 */
exports.update = function(req, res) {
    var boarding_id = req.body.Boarding_ID,
        product = req.body.Product_ID;
    if (req.body.Marketplace === 1){
        db.sequelize.query('SET SQL_SAFE_UPDATES = 0;UPDATE Payment_Plans as pay Join Split_Payment as split on (pay.Product_ID = split.Product_ID AND pay.Boarding_ID = split.Boarding_ID) SET pay.Password = :password, pay.Processor_ID = :processor,split.Processor_ID = :processor, pay.Username = :username, pay.Marketplace_Name = :marketplacename,split.Recipient = :Recipient, split.Rate = :Rate, split.Type = :Type, split.Recipient_Type = :Recipient_Type, pay.Last_Changed_By = :Last_Changed_By, split.Last_Changed_By = :Last_Changed_By_2 where pay.Boarding_ID = :boarding_id and pay.Product_ID = :product_id',null,{ raw: true },
            { boarding_id: boarding_id,product_id:product,marketplacename:req.body.Marketplace_Name ,Recipient:req.body.Recipient,Rate:req.body.Rate,Type:req.body.Type,Recipient_Type:req.body.Recipient_Type,password:req.body.Password, processor:req.body.Processor_ID, username:req.body.Username, Last_Changed_By:req.body.Last_Changed_By, Last_Changed_By_2:req.body.Last_Changed_By }
        ).success(function(results) {
            if (!results){
                return res.jsonp({NoResults:true });
            }else{
                return res.jsonp({Response: results});
            }
        }).error(function(err){
            return res.jsonp(err);
        });
    }else{
        db.Payment_Plan.find({
            where: {
                Boarding_ID: boarding_id,
                Product_ID: product
            }
        }).success(function (paymentplanCollection) {
            if (paymentplanCollection) {
                var payment_plan = paymentplanCollection;
                payment_plan.updateAttributes({
                    Boarding_ID: req.body.Boarding_ID,
                    Product_ID: req.body.Product_ID,
                    Capture_Fee: req.body.Capture_Fee,
                    Discount_Rate: req.body.Discount_Rate,
                    Sale_Fee: req.body.Sale_Fee,
                    Monthly_Fee: req.body.Monthly_Fee,
                    Void_Fee: req.body.Void_Fee,
                    Setup_Fee: req.body.Setup_Fee,
                    Chargeback_Reversal_Fee: req.body.Chargeback_Reversal_Fee,
                    Refund_Fee: req.body.Refund_Fee,
                    Credit_Fee: req.body.Credit_Fee,
                    Authorization_Fee: req.body.Authorization_Fee,
                    Chargeback_Fee: req.body.Chargeback_Fee,
                    Declined_Fee: req.body.Declined_Fee,
                    Retrieval_Fee: req.body.Retrieval_Fee,
                    Is_Active: req.body.Is_Active,
                    Username: req.body.Username,
                    Password: req.body.Password,
                    Processor_ID: req.body.Processor_ID,
                    Ach_Reject_Fee: req.body.Ach_Reject_Fee,
                    Last_Changed_By: req.body.Last_Changed_By 
                }).success(function (a) {
                    return res.jsonp(a);
                }).error(function (err) {
                    return res.render('error', {
                        error: err,
                        status: 500
                    });
                });
            }
        }).error(function (err) {
            console.error(err);
        });
    }
};

/**
 * Delete an payment_plan
 */
exports.destroy = function(req, res) {

    // create a new variable to hold the payment_plan that was placed on the req object.
    var payment_plan = req.payment_plan;

    payment_plan.destroy().success(function(){
        return res.jsonp(payment_plan);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

/**
 * Show an payment_plan
 */
exports.show = function(req, res) {
    // Sending down the payment_plan that was just preloaded by the payment_plans.payment_plan function
    // and saves payment_plan on the req object.
    return res.jsonp(req.payment_plan);
};

/**
 * List of payment_plans
 */
exports.all = function(req, res) {
    db.Payment_Plan.findAll().success(function(payment_plans){
        return res.jsonp(payment_plans);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};
