'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    config = require('./../../config/config');
/**
 * Find mas by id
 * Note: This is called every time that the parameter :masId is used in a URL. 
 * Its purpose is to preload the mas on the req object then call the next function. 
 */
exports.mas = function(req, res, next, id) {
    //var transactionid = req.params.mastransactionid,
    var    paymentdate = req.params.paymentdate;
    if(paymentdate){
        db.MAS.find({ where: {Processor_ID: id, Payment_Date:paymentdate}}).success(function(mas){
            if(!mas) {
                req.mas = {response:false};
                return next();   
            } else {
                req.mas = {response:true};
                return next();            
            }
        }).error(function(err){
            return next(err);
        });
    }else{
        db.MAS.findAll({ where: {Processor_ID: id}}).success(function(mas){
            if(!mas) {
                req.mas = {clean:'clean'};
                return next();   
            } else {
                req.mas = {response:mas};
                return next();            
            }
        }).error(function(err){
            return next(err);
        });
    }
};

/**
 * Show an mas
 */
exports.show = function(req, res) {
    // Sending down the mas that was just preloaded by the mass.mas function
    // and saves mas on the req object.
    return res.jsonp(req.mas);
};

/**
 * List of mass
 */
exports.all = function(req, res) {
    db.MAS.findAll().success(function(mass){
        return res.jsonp(mass);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};
