'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
	config = require('./../../config/config'),
    Q =  require('q'),

    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    };
/**
 * Find nationBuilderMojopay by id
 * Note: This is called every time that the parameter :nationBuilderMojopayId is used in a URL. 
 * Its purpose is to preload the nationBuilderMojopay on the req object then call the next function. 
 */
exports.nationBuilderMojopay = function(req, res, next, id) {
    var slug = req.params.slug;
//console.log("slug: " + slug);

        db.Nation_Builder_Mojopay.find({ where: {SLUG: slug}}).success(function(nationBuilderMojopay){
            if(!nationBuilderMojopay) {
//console.log(nationBuilderMojopay);
                req.nationBuilderMojopay = {};
                return next();
            } else {
                req.nationBuilderMojopay = {response:nationBuilderMojopay};
                return next();            
            }
         }).error(function(err){
            return res.render('error', {
            error: err,
            status: 500
        });
    });
};

exports.show = function(req, res) {
    // Sending down the nationBuilderMojopay that was just preloaded by the nationBuilderMojopay.nationBuilderMojopay function
    // and saves nationBuilderMojopay on the req object.
//console.log('show');
//console.log(req.nationBuilderMojopay);
    return res.jsonp(req.nationBuilderMojopay);
};

