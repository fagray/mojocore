'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

/**
 * Extend user's controller

module.exports = _.extend(
	require('./users/users.authentication'),
	require('./users/users.authorization'),
	require('./users/users.password'),
	require('./users/users.profile')
);
*/

module.exports = _.extend(
	require('./userssequel/users.authentication'),
	require('./userssequel/users.authorization'),
	require('./userssequel/users.password'),
	require('./userssequel/users.profile'),
	require('./userssequel/users.merchantemail')
);