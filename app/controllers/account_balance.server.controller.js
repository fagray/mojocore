'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
	config = require('./../../config/config'),
    Q =  require('q'),
    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    };
/**
 * Find Account Balance by processorId/BalanceDate
 * Note: This is called every time that the parameter :processorId is used in a URL. 
 * Its purpose is to preload the Account_Balance on the req object then call the next function. 
 */

 exports.Account_Balance = function(req, res) {

    var deferred = Q.defer();
    var processorId = req.params.processorId;
    var balanceDate = req.params.balanceDate;

    db.Account_Balance.findAll({ where: {Processor_ID: processorId, Date: {lte: balanceDate} }, order: [["Date", "DESC"]], limit: 1 }).success(function(balance){
        if (!balance){
            return res.jsonp({});
        }else{
            return res.jsonp({response:balance});
        }
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};
