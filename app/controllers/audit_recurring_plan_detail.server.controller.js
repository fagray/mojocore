'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    nodemailer = require('nodemailer'),
    crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq',
    cipher,
	config = require('./../../config/config'),
    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    };

/**
 * Create an audit recurring_plan
 */
exports.create = function(req, res) {
    var recurringPlanAuditObject = {
        Audit_Header_ID: req.body.Audit_Header_ID,
        Field_Name: req.body.Field_Name,
        Old_Value: req.body.Old_Value,
        New_Value: req.body.New_Value,
    };

    db.Audit_Recurring_Plan_Detail.create(recurringPlanAuditObject).success(function(audit_recurring_plan_detail,err){
        if(!audit_recurring_plan_detail){
            return res.send('error', {errors: err});
        } else {
            return res.jsonp(audit_recurring_plan_detail);
        }
    }).error(function(err){
        return res.send('error', {
        error: err,
        status: 500
        });
    });
};



/**
 * Show an audit_recurring_plan
 */
exports.show = function(req, res) {
    // Sending down the payment_plan that was just preloaded by the payment_plans.payment_plan function
    // and saves payment_plan on the req object.
    return res.jsonp(req.audit_recurring_plan_detail);
};

/**
 * List recurring_plans
 */
exports.all = function(req, res) {
    var auditHeaderId = req.param('Audit_Header_ID');
    db.Audit_Recurring_Plan_Detail.findAll({where: {Audit_Header_ID: auditHeaderId}})
    .success(function(audit_recurring_plan_detail){
        if (!audit_recurring_plan_detail) {
            return res.jsonp({NoResults:true });
        } else {
            return res.jsonp({Response: audit_recurring_plan_detail});
        }
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};
