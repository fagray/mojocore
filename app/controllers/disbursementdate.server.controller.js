'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
	config = require('./../../config/config'),
    log4js = require('log4js');

log4js.configure(config.logging);
var Log = log4js.getLogger('transactions');


/**
 * Find payment_plan by id
 * Note: This is called every time that the parameter :paymentplanId is used in a URL. 
 * Its purpose is to preload the payment_plan on the req object then call the next function. 
 */
exports.disbursement_schedule = function(req, res, next, id) {
        db.Disbursement_Schedule.find({ where: {Boarding_ID: id}}).success(function(disbursement_schedule){
            if(!disbursement_schedule) {
                req.disbursement_schedule = {clean:'clean'};
                return next();   
            } else {
                req.disbursement_schedule = disbursement_schedule;
                return next();            
            }
        }).error(function(err){
            return next(err);
        });
};
/**
 * Create a payment_plan
 */
exports.create = function(req, res) {   
    /**
     * Tracking
     */
    var text = '\r\n\r\n[disbursement date created] [' + req.user.Username + '] [' + JSON.stringify(req.body) + ']';  
    Log.debug(text);

    // augment the payment_plan by adding the UserId
    req.body.UserId = req.user.id;
    // save and return and instance of payment_plan on the res object. 
    db.Disbursement_Schedule.create(req.body).success(function(disbursement_schedule,err){
        if(!disbursement_schedule){
            return res.send('users/signup', {errors: err});
        } else {
		    return res.jsonp(disbursement_schedule);
        }
    }).error(function(err){
        return res.send('users/signup', { 
            errors: err,
            status: 500
        });
    });
};

/**
 * Update a payment_plan
 */
exports.update = function(req, res) {
    /**
     * Tracking
     */
    var text = '\r\n\r\n[disbursement date updated] [' + req.user.Username + '] [' + JSON.stringify(req.body) + ']';  
    Log.debug(text);


    var boarding_id = req.body.boarding_id,
        product = req.body.Product_ID;
    db.Disbursement_Schedule.find({ where: {Boarding_ID: boarding_id}}).success(function(disbursementdateCollection){
        if(disbursementdateCollection) {
	        var disbursement_schedule = disbursementdateCollection;
            /**
             * Tracking
             */
            var text = '[disbursement] [' + req.user.Username + '] [' + req.ip + '] ' + {
                            Boarding_ID: req.body.boarding_id,
                            Weekly: req.body.weekly,
                            Monday: req.body.monday,
                            Tuesday: req.body.tuesday,
                            Wednesday: req.body.wednesday,
                            Thursday: req.body.thursday,
                            Friday: req.body.friday,
                            recurweekly: req.body.recurweekly,
                            daymonth: req.body.daymonth,
                            datemonth: req.body.datemonth,
                            everydate: req.body.everydate,
                            dayorder: req.body.dayorder,
                            dayname: req.body.dayname,
                            everymonth:  req.body.everymonth,
                            Hold:  req.body.dayhold,
                            Last_Changed_By: req.body.Last_Changed_By
            };  
            Log.debug(text);

            disbursement_schedule.updateAttributes({
                            Boarding_ID: req.body.boarding_id,
                            Weekly: req.body.weekly,
                            Monday: req.body.monday,
                            Tuesday: req.body.tuesday,
                            Wednesday: req.body.wednesday,
                            Thursday: req.body.thursday,
                            Friday: req.body.friday,
                            recurweekly: req.body.recurweekly,
                            daymonth: req.body.daymonth,
                            datemonth: req.body.datemonth,
                            everydate: req.body.everydate,
                            dayorder: req.body.dayorder,
                            dayname: req.body.dayname,
                            everymonth:  req.body.everymonth,
                            Hold:  req.body.dayhold,
                            Last_Changed_By: req.body.Last_Changed_By
            }).success(function(a){
                return res.jsonp(a);
            }).error(function(err){
                return res.render('error', {
                    error: err, 
                    status: 500
                });
            });     
        }
    }).error(function(err){
        console.error(err);
    });

};

/**
 * Delete an payment_plan
 */
exports.destroy = function(req, res) {

    // create a new variable to hold the payment_plan that was placed on the req object.
    var disbursement_schedule = req.disbursement_schedule;

    disbursement_schedule.destroy().success(function(){
        return res.jsonp(disbursement_schedule);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

/**
 * Show an payment_plan
 */
exports.show = function(req, res) {
    // Sending down the payment_plan that was just preloaded by the payment_plans.payment_plan function
    // and saves payment_plan on the req object.
    return res.jsonp(req.disbursement_schedule);
};

/**
 * List of payment_plans
 */
exports.all = function(req, res) {
    db.Disbursement_Schedule.findAll().success(function(disbursement_schedule){
        return res.jsonp(disbursement_schedule);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};
