'use strict';
/**
 * Module dependencies.
 */
 
var express    = require("express");
var mysql      = require("sequelize-mysql/node_modules/mysql");
var config    = require("./../../config/config");


exports.newrelic_mysql_connect = function(req, res) {
    
    var connection = mysql.createConnection({
        host: config.db1.host,
        port: config.db1.port,
        user:config.db1.username,
        password: config.db1.password,
        database: config.db1.name
    });
    
    connection.connect(function(err) {
        if(!err) {
            return res.jsonp("Database is connected");
        } else {
            return res.jsonp("Error connecting to database (" + err + ")");
        }
    });

};
