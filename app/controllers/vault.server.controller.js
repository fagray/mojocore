'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
	config = require('./../../config/config'),
    Q =  require('q'),
    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    };
 

exports.customerVault = function(req, res, next, id) {
    db.Vault.find({ where: {id: id}}).success(function(customerVault){
        if(!customerVault) {
            return next(new Error('Failed to load customer vault ' + id));
        } else {
            req.customerVault = customerVault;
            return next();            
        }
    }).error(function(err){
        return next(err);
    });
};


exports.create = function(req, res) {
//console.log('create Vault server body', req.body)
    // save and return and instance of boarding on the res object. 
    db.Vault.create(req.body).success(function(vault,err){
        if(!vault){
            return res.send('users/signup', {errors: err});
        } else {
            return res.jsonp(vault);
        }
    }).error(function(err){
        return res.send('users/signup', { 
            errors: err,
            status: 500
        });
    });
};


exports.update = function(req, res) {
//console.log('Updating the customer vault');
    var id = req.body.customer_vault_id;
    db.Vault.find({ where: {customer_vault_id: id, Is_Active: 1}})
    .success(function(VaultCollection){
        if (VaultCollection) {
            var vault = VaultCollection;
            vault.updateAttributes({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                address_1: req.body.address_1,
                address_2: req.body.address_2,
                company: req.body.company,
                city: req.body.city,
                state: req.body.state,
                postal_code: req.body.postal_code,
                country: req.body.country,
                email: req.body.email,
                phone: req.body.phone,
                fax: req.body.fax,
                cell_phone: req.body.cell_phone,
                //customer_tax_id: req.body.customer_tax_id,
                website: req.body.website,
                shipping_first_name: req.body.shipping_first_name,
                shipping_last_name: req.body.shipping_last_name,
                shipping_address_1: req.body.shipping_address_1,
                shipping_address_2: req.body.shipping_address_2,
                shipping_company: req.body.shipping_company,
                shipping_city: req.body.shipping_city,
                shipping_state: req.body.shipping_state,
                shipping_postal_code: req.body.shipping_postal_code,
                shipping_country: req.body.country,
                shipping_email: req.body.shipping_email,
                shipping_carrier: req.body.shipping_carrier,
                tracking_number: req.body.tracking_number,
                shipping_date: req.body.shipping_date,
                shipping: req.body.shipping,
                cc_number: req.body.cc_number,
                cc_hash: req.body.cc_hash,
                cc_exp: req.body.cc_exp,
                cc_start_date: req.body.cc_start_date,
                cc_issue_number: req.body.cc_issue_number,
                check_account: req.body.check_account,
                check_hash: req.body.check_hash,
                check_aba: req.body.check_aba,
                check_name: req.body.check_name,
                //account_holder_type: vaultResults.nm_response.customer_vault.account_holder_type,
                account_type: req.body.account_type,
                sec_code: req.body.sec_code,
                processor_id: req.body.processor_id,
                cc_bin: req.body.cc_bin,
                //merchant_defined_field_1: req.body.merchant_defined_field_1,
                //merchant_defined_field_2: req.body.merchant_defined_field_2,
                //merchant_defined_field_3: req.body.merchant_defined_field_3,
                //merchant_defined_field_4: req.body.merchant_defined_field_4,
                //merchant_defined_field_5: req.body.merchant_defined_field_5,
                //merchant_defined_field_6: req.body.merchant_defined_field_6,
                //merchant_defined_field_7: req.body.merchant_defined_field_7,
                //merchant_defined_field_8: req.body.merchant_defined_field_8,
                //merchant_defined_field_9: req.body.merchant_defined_field_9,
                //merchant_defined_field_10: req.body.merchant_defined_field_10,
                //merchant_defined_field_11: req.body.merchant_defined_field_11,
                //merchant_defined_field_12: req.body.merchant_defined_field_12,
                //merchant_defined_field_13: req.body.merchant_defined_field_13,
                //merchant_defined_field_14: req.body.merchant_defined_field_14,
                //merchant_defined_field_15: req.body.merchant_defined_field_15,
                //merchant_defined_field_16: req.body.merchant_defined_field_16,
                //merchant_defined_field_17: req.body.merchant_defined_field_17,
                //merchant_defined_field_18: req.body.merchant_defined_field_18,
                //merchant_defined_field_19: req.body.merchant_defined_field_19,
                //merchant_defined_field_20: req.body.merchant_defined_field_20,
                //amount: req.body.amount,
                //currency: req.body.currency,
                //descriptor: req.body.descriptor,
                //descriptor_phone: req.body.descriptor_phone,
                //order_description: req.body.order_description,
                //order_id: req.body.order_id,
                //po_number: req.body.po_number,
                //tax: req.body.tax,
                //tax_exempt: req.body.tax_exempt,
                //Num_Emails_Sent: req.body.Num_Emails_Sent,
                //Last_Email_Sent: req.body.Last_Email_Sent,
                //Last_Call_Made: req.body.Last_Call_Made
                Is_Active: req.body.Is_Active
            }
            ).success(function(a){
                return res.jsonp(a);
            }).error(function(err){
                return res.render('error', {
                    error: err, 
                    status: 500
                });
            });
        }
    }).error(function(err){
        console.error(err);
    });
};

exports.show = function(req, res) {
    // Sending down the boarding that was just preloaded by the boardings.boarding function
    // and saves boarding on the req object.
    return res.jsonp(req.customerVault);
};


exports.customer_vault = function(req, res) {
    var query = '';
    var arg = {};
    var id = req.param('id'),
        boardingId = req.param('boarding'),
        firstName  = req.param('firstName'),
        lastName   = req.param('lastName'),
        email      = req.param('email'),
        last4cc    = req.param('last4cc');
        
        query = 'SELECT * FROM Vaults WHERE Is_Active = 1';
        
        if ((typeof req.param('boarding') !== 'undefined') && (req.param('boarding') !== '') && (req.param('boarding') !== 'all')) {
            query += ' AND Merchant_ID = :boardingId';
			arg.boardingId = req.param('boarding');
        }
        if ((typeof req.param('id') !== 'undefined') && (req.param('id') !== '') && (req.param('id') !== 'all')) {
            query += ' AND id = :id';
			arg.id = req.param('id');
        }
        if ((typeof req.param('firstName') !== 'undefined') && (req.param('firstName') !== '') && (req.param('firstName') !== 'all')) {
            query += ' AND first_name LIKE :firstName';
			arg.firstName = '%' + req.param('firstName') + '%';
        }
        if ((typeof req.param('lastName') !== 'undefined') && (req.param('lastName') !== '') && (req.param('lastName') !== 'all')) {
            query += ' AND last_name LIKE :lastName';
			arg.lastName =  '%' + req.param('lastName') + '%';
        }
        if ((typeof req.param('email') !== 'undefined') && (req.param('email') !== '') && (req.param('email') !== 'all')) {
            query += ' AND email LIKE :email';
			arg.email =  '%' + req.param('email') + '%';
        }
        if ((typeof req.param('last4cc') !== 'undefined') && (req.param('last4cc') !== '') && (req.param('last4cc') !== 'all')) {
            query += ' AND cc_number LIKE :last4cc';
			arg.last4cc =  '%' + req.param('last4cc');
        }
        
        db.sequelize.query(query,null,{ raw: true },
		    arg
        ).success(function(vault){
            if(!vault) {
                return res.jsonp({response:null});
            } else {
                return res.jsonp({response: vault});
            }
        }).error(function(err){
            return res.jsonp({response:err});
        });
};
