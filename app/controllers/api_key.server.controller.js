'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
    crypto = require('crypto'),
	config = require('./../../config/config');

var iterations = 1000, //2500;
    keylen = 20; //512;
/**
 * Find apiKey by id
 * Note: This is called every time that the parameter :apiKeyId is used in a URL. 
 * Its purpose is to preload the apiKey on the req object then call the next function. 
 */
exports.api_key = function(req, res, next, id) {
    db.Api_Key.find({ where: {Boarding_ID: id,Is_Active:1}}).success(function(apiKey){
          if(!apiKey) {
                req.apiKey = {apiKey:'false'};
                return next();   
            } else {
                req.apiKey = apiKey;
                return next();            
            }
        return next();            
    }).error(function(err){
        return next(err);
    });
};
/**
 * Create a apiKey
 */
exports.create = function(req, res) {
    // augment the apiKey by adding the UserId
    var id= req.body.Boarding_ID;
    // save and return and instance of apiKey on the res object. 
     db.Api_Key.find({ where: {Boarding_ID: id,Is_Active:1}}).success(function(apiKey){
            if(apiKey) {
                apiKey.updateAttributes({
                            Is_Active:null
                }).success(function(a){
                }).error(function(err){
                });
            }
        }).error(function(err){
            return (err);
        });

        var passphrase = req.body.Username+req.body.Boarding_ID,
            salt = new Buffer(crypto.randomBytes(keylen)).toString('hex'),
            generatedHash = crypto.pbkdf2Sync(passphrase, salt, iterations, keylen).toString('hex');
        db.Api_Key.create({
            Token:generatedHash,
            Boarding_ID:id
        }).success(function(apiKey,err){ 
            return res.jsonp(apiKey);
        }).error(function(err){
    });
};



/**
 * Delete an apiKey
 */
exports.destroy = function(req, res) {

    // create a new variable to hold the apiKey that was placed on the req object.
    var apiKey = req.apiKey;

    apiKey.destroy().success(function(){
        return res.jsonp(apiKey);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

/**
 * Show an apiKey
 */
exports.show = function(req, res) {
    // Sending down the apiKey that was just preloaded by the apiKeys.apiKey function
    // and saves apiKey on the req object.
    return res.jsonp(req.apiKey);
};

/**
 * List of apiKeys
 */
exports.all = function(req, res) {
    db.Api_Key.findAll().success(function(apiKeys){
        return res.jsonp(apiKeys);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};
