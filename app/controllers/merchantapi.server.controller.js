'use strict';

/**
 * Module dependencies.
 */
var errorHandler = require('./errors'),
    http = require('http'),
    https = require('https'),
    request = require('request'),
    qs = require('querystring'),
    _ = require('lodash'),
    fs = require('fs'),
    queryString = '?',
    express = require('express'),
    app = express(),
    Q =  require('q'),
    db = require('../../config/sequelize'),
    config = require('./../../config/config'),
    log4js = require('log4js'),
    crypto = require('crypto'),
    request = require('request'),
	nodemailer = require('nodemailer');

/**
 * Log4js need be configurated
 * Remember include -> config = require('./../../config/config')
 ***/
//log4js.configure(config.logging);
//var Log = log4js.getLogger('transactions'); // "transactions" represents category of log or type of log
var credentialsU='',
    credentialsP='',
    credentialPro='',
    boardingGlobal='',
    tokenvar = '',
    iterations = 1000, //2500;
    keylen = 12, //512;
    theObject,
    customer_vault = 0,
    
    send_mojopay_receipt = false,
    mojo_receipt_email_address = '',
    mojo_receipt_merchant_name = '',
    mojo_receipt_customer_name = '',
    mojo_reciept_billing_address = '',
    mojo_reciept_shipping_address = '',
    mojo_receipt_transaction_id = '',
    mojo_receipt_card_type = '',
    mojo_receipt_amount = '',
    
    toObject = function (arr) {
        var rv = {};
        for (var i = 0; i < arr.length; ++i)
            rv[i] = arr[i];
        return rv;
    },
    XMLMapping = require('xml-mapping'),
    queryStringToJSON = function(pairs) {
        pairs = pairs.split('&');
        var result = {};
        pairs.forEach(function(pair) {
            pair = pair.split('=');
            result[pair[0]] = decodeURIComponent(pair[1] || '');
        });

        return JSON.parse(JSON.stringify(result));
    },
    /**
     * Gets the credentials from the processorId if its a customer creation it generates a hash
     * @param req
     * @param res
     * @returns {*}
     */
    getCredentials = function(req,res){
        var deferred = Q.defer();
        boardingGlobal = '';
        credentialPro = req.param('processorId');
//console.log(credentialPro)
        send_mojopay_receipt = false;
        mojo_receipt_email_address = '';
        mojo_receipt_merchant_name = '';
        mojo_receipt_customer_name = '';
        mojo_reciept_billing_address = '';
        mojo_reciept_shipping_address = '';
        mojo_receipt_transaction_id = '';
        mojo_receipt_card_type = '';
        mojo_receipt_amount = '';

        // try to find the active payment plan for the processor passed
        db.Payment_Plan.find({where:{Processor_ID:credentialPro, Is_Active:1}}).success(function(payment_plan){
            if (credentialPro === null || credentialPro === undefined || credentialPro === '') {

                // URL is missing the processorId parameter
                credentialsU = '';
                deferred.resolve(false);
                return res.jsonp({Response:'Processor not Found'});

            } else { 

                // the URL contained a processorId parameter
                tokenvar = req.param('token');
                if (tokenvar === null || tokenvar === undefined || tokenvar === '') {

                    // URL is missing the token parameter
                    credentialsU = '';
                    deferred.resolve(false);
                    return res.jsonp({Response:'Wrong or Inactive Api Token'});

                } else {

                    // the URL contained a token parameter
                    if (payment_plan) { // there's a record in the database 
//console.log(payment_plan)

                        // find the passed token for this processorId (merchant) in the database
                        db.Api_Key.find({ where: {Boarding_ID: payment_plan.Boarding_ID,Token:tokenvar,Is_Active:1}}).success(function(apiKey){
                            if (!apiKey) {

                                // did not find this token for this merchant
                                /*
                                 var text = '\r\n\r\n[Wrong or Inactive Api Token] \r\n\r\n';
                                 Log.debug(text);
                                 */
                                credentialsU = '';
                                deferred.resolve(false);
                                return res.jsonp({Response:'Wrong or Inactive Api Token'});

                            } else {
                                
                                // valid token/processorId combination found for this merchant.  Set variables necessary for NMI call
                                boardingGlobal = payment_plan.Boarding_ID;
                                credentialsU = payment_plan.Username;
                                credentialsP = payment_plan.Password;
                                deferred.resolve(true);
                            
                            } // if (!apiKey)
                        }); // db.Api_Key.find 
                        
                        db.Boarding.find({ where: {id: payment_plan.Boarding_ID,status:'active'}}).success(function(boarding){
//console.log(boarding)
                            if (boarding) {

                                // get the Merchant Name for the receipt 
                                mojo_receipt_merchant_name = boarding.Merchant_Name; 
                            } else {
                                credentialsU = '';
                                deferred.resolve(false);
                                return res.jsonp({Response:'Merchant Inactive or Not Found'});                           
                            }//if (boarding) 
                            
                        }); // db.Boarding.find

                    } else {
                        
                        // not a valid token/processorId combination
                        credentialsU = '';
                        deferred.resolve(true);
                        return res.jsonp({Response:'Processor not Found'});
                    } // if (payment_plan)

                } // if (tokenvar === null || tokenvar === undefined || tokenvar === '')
            } // if (credentialPro === null || credentialPro === undefined || credentialPro === '')  

        }); // db.Payment_Plan.find
        return deferred.promise;
    },
    /**
     * Masks the Credit Card
     * @param cc
     * @returns {string}
     */
    doMask = function(cc){
        var first6 = cc.substring(0, 6);
        var last4 = cc.substring(cc.length - 4);
        var mask = '';
        for (var i = 0; i < cc.length-10; i++) {
            mask+='*';
        }
        return first6+mask+last4;
    },
    getBrand = function(cc){
        var cc_number = cc;    
        if ((cc_number.substr(1, 1)==='x') || (cc_number.substr(1, 1)==='*')){
            return 'Unknown';
        }else{
            // visa
            var re = new RegExp('^4');
            if (cc_number.match(re) !== null)
                return 'Visa';

            // Mastercard
            re = new RegExp('^5[1-5]');
            if (cc_number.match(re) !== null)
                return 'Mastercard';

            // AMEX
            re = new RegExp('^3[47]');
            if (cc_number.match(re) !== null)
                return 'AMEX';

            // Discover
            re = new RegExp('^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)');
            if (cc_number.match(re) !== null)
                return 'Discover';

            // Diners
            re = new RegExp('^36');
            if (cc_number.match(re) !== null)
                return 'Diners';

            // Diners - Carte Blanche
            re = new RegExp('^30[0-5]');
            if (cc_number.match(re) !== null)
                return 'Diners - Carte Blanche';

            // JCB
            re = new RegExp('^35(2[89]|[3-8][0-9])');
            if (cc_number.match(re) !== null)
                return 'JCB';

            // Visa Electron
            re = new RegExp('^(4026|417500|4508|4844|491(3|7))');
            if (cc_number.match(re) !== null)
                return 'Visa Electron';
        }
    },
    vaultMaintenance = function(req, operation, id, username, password) {
        var deferred = Q.defer();
        var url = config.api_url.root_query + '?username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password) + '&report_type=customer_vault&customer_vault_id=' + encodeURIComponent(id);
        request({url:url,async:true}, function (error, response, body) {
            var json = XMLMapping.load(body);
            var theObject = json.nm_response.customer_vault.customer;
            if (!error && response.statusCode === 200) {
                var vaultObject = {
                    nmi: theObject.id,
                    Merchant_ID: boardingGlobal,
                    customer_vault_id: theObject.customer_vault_id.$t,
                    first_name: theObject.first_name.$t,
                    last_name: theObject.last_name.$t,
                    address_1: theObject.address_1.$t,
                    address_2: theObject.address_2.$t,
                    company: theObject.company.$t,
                    city: theObject.city.$t,
                    state: theObject.state.$t,
                    postal_code: theObject.postal_code.$t,
                    country: theObject.country.$t,
                    email: theObject.email.$t,
                    phone: theObject.phone.$t,
                    fax: theObject.fax.$t,
                    cell_phone: theObject.cell_phone.$t,
                    customer_tax_id: theObject.customertaxid.$t,
                    website: theObject.website.$t,
                    shipping_first_name : theObject.shipping_first_name.$t,
                    shipping_last_name: theObject.shipping_last_name.$t,
                    shipping_address_1: theObject.shipping_address_1.$t,
                    shipping_address_2: theObject.shipping_address_2.$t,
                    shipping_company: theObject.shipping_company.$t,
                    shipping_city: theObject.shipping_city.$t,
                    shipping_state: theObject.shipping_state.$t,
                    shipping_postal_code: theObject.shipping_postal_code.$t,
                    shipping_country: theObject.shipping_country.$t,
                    shipping_email: theObject.shipping_email.$t,
                    shipping_carrier: theObject.shipping_carrier.$t,
                    tracking_number: theObject.tracking_number.$t,
                    shipping_date: theObject.shipping_date.$t,
                    shipping: theObject.shipping.$t,
                    cc_number: theObject.cc_number.$t,
                    cc_hash: theObject.cc_hash.$t,
                    cc_exp: theObject.cc_exp.$t,
                    cc_start_date: theObject.cc_start_date.$t,
                    cc_issue_number: theObject.cc_issue_number.$t,
                    check_account: theObject.check_account.$t,
                    check_hash: theObject.check_hash.$t,
                    check_aba: theObject.check_aba.$t,
                    check_name: theObject.check_name.$t,
                    account_holder_type: theObject.account_holder_type.$t,
                    account_type: theObject.account_type.$t,
                    sec_code: theObject.sec_code.$t,
                    //processor_id: theObject.processor_id,
                    cc_bin: theObject.cc_bin.$t,
                    merchant_defined_field_1: req.param('productMDF1'),
                    merchant_defined_field_2: req.param('productMDF2'),
                    merchant_defined_field_3: req.param('productMDF3'),
                    merchant_defined_field_4: req.param('productMDF4'),
                    merchant_defined_field_5: req.param('productMDF5'),
                    merchant_defined_field_6: req.param('productMDF6'),
                    merchant_defined_field_7: req.param('productMDF7'),
                    merchant_defined_field_8: req.param('productMDF8'),
                    merchant_defined_field_9: req.param('productMDF9'),
                    merchant_defined_field_10: req.param('productMDF10'),
                    merchant_defined_field_11: req.param('adminMDF1'),
                    merchant_defined_field_12: req.param('adminMDF2'),
                    merchant_defined_field_13: req.param('adminMDF3'),
                    merchant_defined_field_14: req.param('adminMDF4'),
                    merchant_defined_field_15: req.param('adminMDF5'),
                    merchant_defined_field_16: req.param('merchantMDF1'),
                    merchant_defined_field_17: req.param('merchantMDF2'),
                    merchant_defined_field_18: req.param('merchantMDF3'),
                    merchant_defined_field_19: req.param('merchantMDF4'),
                    merchant_defined_field_20: req.param('merchantMDF5'),
                    //amount: theObject.amount.$t,
                    //currency: theObject.currency.$t,
                    //descriptor: theObject.descriptor.$t,
                    //descriptor_phone: theObject.descriptor_phone.$t,
                    //order_description: theObject.order_description.$t,
                    //order_id: theObject.order_id.$t,
                    //po_number: theObject.po_number.$t,
                    //tax: theObject.tax.$t,
                    //tax_exempt: theObject.tax_exempt.$t,
                    Is_Active: 1
                };
                
                if (operation === 'create') {
                    db.Vault.create(vaultObject).success(function(vault,err){
                        if(!vault){
                            vault.customer_hash = vault.id;
            
                            //res.send('users/signup', {errors: err});
                            //deferred.resolve(true);
                        } else {
                            //res.jsonp({Response:'customer_card_hash done!'});
                            //deferred.resolve(true);
                        }
                    }).error(function(err){
                        console.log(err);
                    });
                } else if (operation === 'update') {
                    db.Vault.find({ where: {Merchant_ID: boardingGlobal, customer_vault_id: id, Is_Active:1}}).success(function(vault){
                        if(vault) {
                            vault.updateAttributes({
                                nmi: theObject.id,
                                Merchant_ID: boardingGlobal,
                                customer_vault_id: theObject.customer_vault_id.$t,
                                first_name: theObject.first_name.$t,
                                last_name: theObject.last_name.$t,
                                address_1: theObject.address_1.$t,
                                address_2: theObject.address_2.$t,
                                company: theObject.company.$t,
                                city: theObject.city.$t,
                                state: theObject.state.$t,
                                postal_code: theObject.postal_code.$t,
                                country: theObject.country.$t,
                                email: theObject.email.$t,
                                phone: theObject.phone.$t,
                                fax: theObject.fax.$t,
                                cell_phone: theObject.cell_phone.$t,
                                ustomer_tax_id: theObject.customertaxid.$t,
                                website: theObject.website.$t,
                                shipping_first_name : theObject.shipping_first_name.$t,
                                shipping_last_name: theObject.shipping_last_name.$t,
                                shipping_address_1: theObject.shipping_address_1.$t,
                                shipping_address_2: theObject.shipping_address_2.$t,
                                shipping_company: theObject.shipping_company.$t,
                                shipping_city: theObject.shipping_city.$t,
                                shipping_state: theObject.shipping_state.$t,
                                shipping_postal_code: theObject.shipping_postal_code.$t,
                                shipping_country: theObject.shipping_country.$t,
                                shipping_email: theObject.shipping_email.$t,
                                shipping_carrier: theObject.shipping_carrier.$t,
                                tracking_number: theObject.tracking_number.$t,
                                shipping_date: theObject.shipping_date.$t,
                                shipping: theObject.shipping.$t,
                                cc_number: theObject.cc_number.$t,
                                cc_hash: theObject.cc_hash.$t,
                                cc_exp: theObject.cc_exp.$t,
                                cc_start_date: theObject.cc_start_date.$t,
                                cc_issue_number: theObject.cc_issue_number.$t,
                                check_account: theObject.check_account.$t,
                                check_hash: theObject.check_hash.$t,
                                check_aba: theObject.check_aba.$t,
                                check_name: theObject.check_name.$t,
                                account_holder_type: theObject.account_holder_type.$t,
                                account_type: theObject.account_type.$t,
                                sec_code: theObject.sec_code.$t,
                                //processor_id: theObject.processor_id,
                                cc_bin: theObject.cc_bin.$t
                            }).success(function(a){
                            }).error(function(err){
                            });
                        }
                    }).error(function(err){
                        return (err);
                    });
                } else if (operation === 'delete') {  
                    db.Vault.find({ where: {Merchant_ID: boardingGlobal, customer_vault_id: id, Is_Active:1}}).success(function(vault){
                        if(vault) {
                            vault.updateAttributes({
                                Is_Active: 0
                            }).success(function(a){
                            }).error(function(err){
                            });
                        }
                    }).error(function(err){
                        return (err);
                    });
                }

            } else {
                console.log(error);
            }

        });
        return deferred.promise;
    },
    sendEmail = function (req, res, send_receipt, transaction_id){
//console.log('sending a receipt', send_receipt)
        if (send_receipt === 'true' ) {
            var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
            if (pattern.test(mojo_receipt_email_address)) {
//console.log('sending a receipt')
                var emailHTML = '',
                    today = new Date(),
                    billing_date = '',
                    cc_number = mojo_receipt_card_type.substr(0,1) + 'xxxxxxxxxxx' + mojo_receipt_card_type.slice(-4);
                billing_date = ('0' + (today.getMonth() + 1)).slice(-2) + '/' +  ('0' + today.getDate()).slice(-2) + '/' + today.getFullYear();
                res.render('templates/transaction_receipt_email', {
                    MOJOPAY_MERCHANT_NAME1: mojo_receipt_merchant_name,
					DATE: billing_date,
                    FIRSTNAME: mojo_receipt_customer_name,
                    BILLING_BLOCK: mojo_reciept_billing_address,
                    SHIPPING_TITLE: 'Shipping Information',
                    SHIPPING_BLOCK: mojo_reciept_shipping_address,
                    TRANSACTIONID: transaction_id,
                    CARDTYPE: cc_number,
                    AMOUNT: mojo_receipt_amount.toFixed(2),
                    MOJOPAY_MERCHANT_NAME2: mojo_receipt_merchant_name
                }, function(err, emailHTML) {
//                   if (emailHTML !== undefined && emailHTML !== null && emailHTML !== '') {
                        emailHTML = emailHTML.replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/undefined/g,' ');
                        var smtpTransport = nodemailer.createTransport(config.mailer.options);
                        // mail to a single address
                        var mailOptions = {
                            from: config.mailer.from,
                            to: mojo_receipt_email_address, 
                            subject: 'Receipt from ' + mojo_receipt_merchant_name,
                            html: emailHTML
                        };
                        smtpTransport.sendMail(mailOptions, function(err) {

                            if(err){
                                console.log(err);
                            }else{
                                console.log("Message sent");
                            }

                        }); // smtpTransport.sendMail
//                    } // if (emailHTML !== undefined && emailHTML !== null && emailHTML !== '')
                }); // res.render
            } else {
//console.log('not sending a receipt - invalid email address')
            }
        } else {
//console.log('not sending a receipt')
        }
    },
    billing_desc = function(duration, day_freq, month_freq, day_of_month) {
        var ordinal = ["th","st","nd","rd"];
        var description = '';
        
        //how often
        if (day_freq !== 0) {
            description += 'Every ' + day_freq + ' days,';
        } else {
            description += 'On the ' + day_of_month + (ordinal[((day_of_month%100)-20)%10]||ordinal[(day_of_month%100)]||ordinal[0]) + ' day of every ' + month_freq + ' months,';
        }
        
        // duration
        if (duration === 0) {
            description += ' until cancelled';
        } else {
            description += ' a total of ' + duration + ' times';
        }
        
        return description;
    },
    

    buildTransactcionHistory = function(trHist) {
       var deferred = Q.defer();
        db.Boarding.find({ where: {id: trHist.boardingId}}).success(function(boarding){
            var transactionHistory = {
                transaction_id: trHist.transaction_id.$t === undefined ? '-' : trHist.transaction_id.$t,
                partial_payment_id: trHist.partial_payment_id.$t === undefined ? '-' : trHist.partial_payment_id.$t,
                partial_payment_balance: trHist.partial_payment_balance.$t === undefined ? '-' : trHist.partial_payment_balance.$t,
                platform_id: trHist.platform_id.$t === undefined ? '-' : trHist.platform_id.$t,
                transaction_type: trHist.transaction_type.$t === undefined ? '-' : trHist.transaction_type.$t,
                transaction_condition: trHist.condition.$t === undefined ? '-' : trHist.condition.$t,
                order_id: trHist.order_id.$t === undefined ? '-' : trHist.order_id.$t,
                authorization_code: trHist.authorization_code.$t === undefined ? '-' : trHist.authorization_code.$t,
                ponumber: trHist.ponumber.$t === undefined ? '-' : trHist.ponumber.$t,
                order_description: trHist.order_description.$t === undefined ? '-' : trHist.order_description.$t,
                first_name: trHist.first_name.$t === undefined ? '-' : trHist.first_name.$t,
                last_name: trHist.last_name.$t === undefined ? '-' : trHist.last_name.$t,
                address_1: trHist.address_1.$t === undefined ? '-' : trHist.address_1.$t,
                address_2: trHist.address_2.$t === undefined ? '-' : trHist.address_2.$t,
                company: trHist.company.$t === undefined ? '-' : trHist.company.$t,
                city: trHist.city.$t === undefined ? '-' : trHist.city.$t,
                state: trHist.state.$t === undefined ? '-' : trHist.state.$t,
                postal_code: trHist.postal_code.$t === undefined ? '-' : trHist.postal_code.$t,
                country: trHist.country.$t === undefined ? '-' : trHist.country.$t,
                email: trHist.email.$t === undefined ? '-' : trHist.email.$t,
                phone: trHist.phone.$t === undefined ? '-' : trHist.phone.$t,
                fax: trHist.fax.$t === undefined ? '-' : trHist.fax.$t,
                cell_phone: trHist.cell_phone.$t === undefined ? '-' : trHist.cell_phone.$t,
                customertaxid: trHist.customertaxid.$t === undefined ? '-' : trHist.customertaxid.$t,
                customerid: trHist.customerid.$t === undefined ? '-' : trHist.customerid.$t,
                website: trHist.website.$t === undefined ? '-' : trHist.website.$t,
                shipping_first_name: trHist.shipping_first_name.$t === undefined ? '-' : trHist.shipping_first_name.$t,
                shipping_last_name: trHist.shipping_last_name.$t === undefined ? '-' : trHist.shipping_last_name.$t,
                shipping_address_1: trHist.shipping_address_1.$t === undefined ? '-' : trHist.shipping_address_1.$t,
                shipping_address_2: trHist.shipping_address_2.$t === undefined ? '-' : trHist.shipping_address_2.$t,
                shipping_company: trHist.shipping_company.$t === undefined ? '-' : trHist.shipping_company.$t,
                shipping_city: trHist.shipping_city.$t === undefined ? '-' : trHist.shipping_city.$t,
                shipping_state: trHist.shipping_state.$t === undefined ? '-' : trHist.shipping_state.$t,
                shipping_postal_code: trHist.shipping_postal_code.$t === undefined ? '-' : trHist.shipping_postal_code.$t,
                shipping_country: trHist.shipping_country.$t === undefined ? '-' : trHist.shipping_country.$t,
                shipping_email: trHist.shipping_email.$t === undefined ? '-' : trHist.shipping_email.$t,
                shipping_carrier: trHist.shipping_carrier.$t === undefined ? '-' : trHist.shipping_carrier.$t,
                tracking_number: trHist.tracking_number.$t === undefined ? '-' : trHist.tracking_number.$t,
                shipping_date: trHist.shipping_date.$t === undefined ? '-' : trHist.shipping_date.$t,
                Shipping: trHist.shipping.$t === undefined ? '-' : trHist.shipping.$t,
                shipping_phone: trHist.shipping_phone.$t === undefined ? '-' : trHist.shipping_phone.$t,
                cc_number: trHist.cc_number.$t === undefined ? '-' : trHist.cc_number.$t,
                cc_hash: trHist.cc_hash.$t === undefined ? '-' : trHist.cc_hash.$t,
                cc_exp: trHist.cc_exp.$t === undefined ? '-' : trHist.cc_exp.$t,
                cavv: trHist.cavv.$t === undefined ? '-' : trHist.cavv.$t,
                cavv_result: trHist.cavv_result.$t === undefined ? '-' : trHist.cavv_result.$t,
                xid: trHist.xid.$t === undefined ? '-' : trHist.xid.$t,
                avs_response: trHist.avs_response.$t === undefined ? '-' : trHist.avs_response.$t,
                csc_response: trHist.csc_response.$t === undefined ? '-' : trHist.csc_response.$t,
                cardholder_auth: trHist.cardholder_auth.$t === undefined ? '-' : trHist.cardholder_auth.$t,
                cc_start_date: trHist.cc_start_date.$t === undefined ? '-' : trHist.cc_start_date.$t,
                cc_issue_number: trHist.cc_issue_number.$t === undefined ? '-' : trHist.cc_issue_number.$t,
                check_account: trHist.check_account.$t === undefined ? '-' : trHist.check_account.$t,
                check_hash: trHist.check_hash.$t === undefined ? '-' : trHist.check_hash.$t,
                check_aba: trHist.check_aba.$t === undefined ? '-' : trHist.check_aba.$t,
                check_name: trHist.check_name.$t === undefined ? '-' : trHist.check_name.$t,
                account_holder_type: trHist.account_holder_type.$t === undefined ? '-' : trHist.account_holder_type.$t,
                account_type: trHist.account_type.$t === undefined ? '-' : trHist.account_type.$t,
                sec_code: trHist.sec_code.$t === undefined ? '-' : trHist.sec_code.$t,
                drivers_license_number: trHist.drivers_license_number.$t === undefined ? '-' : trHist.drivers_license_number.$t,
                drivers_license_state: trHist.drivers_license_state.$t === undefined ? '-' : trHist.drivers_license_state.$t,
                drivers_license_dob: trHist.drivers_license_dob.$t === undefined ? '-' : trHist.drivers_license_dob.$t,
                social_security_number: trHist.social_security_number.$t === undefined ? '-' : trHist.social_security_number.$t,
                processor_id: trHist.processor_id.$t === undefined ? '-' : trHist.processor_id.$t,
                tax: trHist.tax.$t === undefined ? '-' : trHist.tax.$t,
                currency: trHist.currency.$t === undefined ? '-' : trHist.currency.$t,
                surcharge: trHist.surcharge.$t === undefined ? '-' : trHist.surcharge.$t,
                tip: trHist.tip.$t === undefined ? '-' : trHist.tip.$t,
                card_balance: trHist.card_balance.$t === undefined ? '-' : trHist.card_balance.$t,
                card_available_balance: trHist.card_available_balance.$t === undefined ? '-' : trHist.card_available_balance.$t,
                entry_mode: trHist.entry_mode.$t === undefined ? '-' : trHist.entry_mode.$t,
                cc_bin: trHist.cc_bin.$t === undefined ? '-' : trHist.cc_bin.$t,
                // merchantId: trHist.boardingId
                merchantId: boarding.dataValues.Merchant_ID
            };
            if (trHist.action.length !== undefined && trHist.action.length >= 1 ) {
                // action is an array - a vault/capture event
                var index = trHist.action.length - 1;
                transactionHistory.amount = trHist.action[index].amount.$t === undefined ? '-' : trHist.action[index].amount.$t;
                transactionHistory.action_type = trHist.action[index].action_type.$t === undefined ? '-' : trHist.action[index].action_type.$t;
                transactionHistory.date = trHist.action[index].date.$t === undefined ? '-' : trHist.action[index].date.$t;
                transactionHistory.success = trHist.action[index].success.$t === undefined ? '-' : trHist.action[index].success.$t;
                transactionHistory.ip_address = trHist.action[index].ip_address.$t === undefined ? '-' : trHist.action[index].ip_address.$t;
                transactionHistory.source = trHist.action[index].source.$t === undefined ? '-' : trHist.action[index].source.$t;
                transactionHistory.username = trHist.action[index].username.$t === undefined ? '-' : trHist.action[index].username.$t;
                transactionHistory.response_text = trHist.action[index].response_text.$t === undefined ? '-' : trHist.action[index].response_text.$t;
                transactionHistory.batch_id = trHist.action[index].batch_id.$t === undefined ? '-' : trHist.action[index].batch_id.$t;
                transactionHistory.processor_batch_id = trHist.action[index].processor_batch_id.$t === undefined ? '-' : trHist.action[index].processor_batch_id.$t;
                transactionHistory.response_code = trHist.action[index].response_code.$t === undefined ? '-' : trHist.action[index].response_code.$t;
                transactionHistory.processor_response_text = trHist.action[index].processor_response_text.$t === undefined ? '-' : trHist.action[index].processor_response_text.$t;
                transactionHistory.processor_response_code = trHist.action[index].processor_response_code.$t === undefined ? '-' : trHist.action[index].processor_response_code.$t;
                transactionHistory.device_license_number = trHist.action[index].device_license_number.$t === undefined ? '-' : trHist.action[index].device_license_number.$t;
                transactionHistory.device_nickname = trHist.action[index].device_nickname.$t === undefined ? '-' : trHist.action[index].device_nickname.$t;
            } else {
                // non array - not a vault/capture event
                transactionHistory.amount = trHist.action.amount.$t === undefined ? '-' : trHist.action.amount.$t;
                transactionHistory.action_type = trHist.action.action_type.$t === undefined ? '-' : trHist.action.action_type.$t;
                transactionHistory.date = trHist.action.date.$t === undefined ? '-' : trHist.action.date.$t;
                transactionHistory.success = trHist.action.success.$t === undefined ? '-' : trHist.action.success.$t;
                transactionHistory.ip_address = trHist.action.ip_address.$t === undefined ? '-' : trHist.action.ip_address.$t;
                transactionHistory.source = trHist.action.source.$t === undefined ? '-' : trHist.action.source.$t;
                transactionHistory.username = trHist.action.username.$t === undefined ? '-' : trHist.action.username.$t;
                transactionHistory.response_text = trHist.action.response_text.$t === undefined ? '-' : trHist.action.response_text.$t;
                transactionHistory.batch_id = trHist.action.batch_id.$t === undefined ? '-' : trHist.action.batch_id.$t;
                transactionHistory.processor_batch_id = trHist.action.processor_batch_id.$t === undefined ? '-' : trHist.action.processor_batch_id.$t;
                transactionHistory.response_code = trHist.action.response_code.$t === undefined ? '-' : trHist.action.response_code.$t;
                transactionHistory.processor_response_text = trHist.action.processor_response_text.$t === undefined ? '-' : trHist.action.processor_response_text.$t;
                transactionHistory.processor_response_code = trHist.action.processor_response_code.$t === undefined ? '-' : trHist.action.processor_response_code.$t;
                transactionHistory.device_license_number = trHist.action.device_license_number.$t === undefined ? '-' : trHist.action.device_license_number.$t;
                transactionHistory.device_nickname = trHist.action.device_nickname.$t === undefined ? '-' : trHist.action.device_nickname.$t;
            }
            
            if (trHist.original_transaction_id !== undefined) {
                transactionHistory.Original_Transaction_ID = trHist.original_transaction_id.$t === undefined ? '-' : trHist.original_transaction_id.$t;
            } else {
                transactionHistory.Original_Transaction_ID = '-';
            }
            deferred.resolve(transactionHistory);
        });

    return deferred.promise;
    },


    email_failed_insert = function(transaction_id) {
        var smtpTransport = nodemailer.createTransport(config.mailer.options);
        var today = new Date();
        var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        // mail to a single address
        var mailOptions = {
            from: config.mailer.from,
            to: 'techops@total-apps.com', 
            subject: 'Error downloading transaction from NMI',
            html: 'Dearest NMI,<br><br>I write to you on this ' + weekday[today.getDay()] + ', ' + month[today.getMonth()] + ' ' + today.getDate() + ', to inquire as to the status of a transaction you may be familar with. That transaction goes by the name of ' + transaction_id + ', perhaps you have witnessed them. If you could perchance provide me with details such as the exact time of said transaction, and the card by which it was conducted, it would be most kind of you.<br><br>Respectfully yours,<br><br>MojoPay.'
        };
        smtpTransport.sendMail(mailOptions, function(err) {
    
            if(err){
                console.log(err);
            }else{
                console.log("Message sent");
            }
    
        }); // smtpTransport.sendMail
    },


    insertIntoTransactionHistory = function(user, pass, transaction_id, boardingId) {
        if (transaction_id !== undefined && transaction_id !== null && transaction_id !== '') {
            var url = config.api_url.root_query + '?username=' + encodeURIComponent(user) + '&password=' + encodeURIComponent(pass) + '&transaction_id=' + encodeURIComponent(transaction_id);
            var tr_hist_insert, tr_hist_insert1, tr_hist_insert2; 
//console.log(url)
            // try this 5 times
            request(url, function (error, response, trHist) {
//console.log('+++++0+++++',trHist);
                 if (!error && response.statusCode === 200) {
                    var json = XMLMapping.load(trHist).nm_response.transaction;
                    if (json !== undefined && json.transaction_id !== undefined && json.transaction_id !== '') {
//console.log('-----1-----',json.action.length);
                        json.boardingId = boardingId;
                        if (json.action.length !== undefined && json.action.length >= 1 ) {
                            var index = json.action.length - 1;
                            if (json.action[index].response_code.$t !== undefined && json.action[index].response_code.$t !== null && json.action[index].response_code.$t !== '') {
                                // put the transaction data into the Transaction_History table
                                tr_hist_insert1 = buildTransactcionHistory(json).then(function(tr_hist_insert) {
//console.log('=====3======',tr_hist_insert);
                                    db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                    
                                    }).error(function(err){
                                        tr_hist_insert = {transaction_id: transaction_id};
                                        db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                                
                                        }).error(function(err){
                                            console.log(err);
                                        });
                                        email_failed_insert(transaction_id);
                                        console.log(error);
                                    });
                                }); // tr_hist_insert1 = buildTransactcionHistory(json).then(function(tr_hist_insert)
                            } else {
                                tr_hist_insert = {transaction_id: transaction_id};
                                db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                        
                                }).error(function(err){
                                    console.log(err);
                                });
                                email_failed_insert(transaction_id);
                                console.log(error);
                            } // if (json.response_code === '100')
                        } else { // action is not an array (capture)
                            if (json.action.response_code.$t !== undefined && json.action.response_code.$t !== null && json.action.response_code.$t !== '') {
                                // put the transaction data into the Transaction_History table
                                tr_hist_insert2 = buildTransactcionHistory(json).then(function(tr_hist_insert) {
//console.log('=====2======',tr_hist_insert);
                                    db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                            
                                    }).error(function(err){
                                         tr_hist_insert = {transaction_id: transaction_id};
                                        db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                                
                                        }).error(function(err){
                                            console.log(err);
                                        });
                                        email_failed_insert(transaction_id);
                                        console.log(error);
                                    });
                                }); // tr_hist_insert2 = buildTransactcionHistory(json).then(function(tr_hist_insert)
                            } else {
                                tr_hist_insert = {transaction_id: transaction_id};
                                db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                        
                                }).error(function(err){
                                    console.log(err);
                                });
                                email_failed_insert(transaction_id);
                                console.log(error);
                            } // if (json.response_code === '100')
                        }
                    } else {
                        tr_hist_insert = {transaction_id: transaction_id};
                        db.Transaction_History.create(tr_hist_insert).success(function(transaction_history,err){
                                
                        }).error(function(err){
                            console.log(err);
                        });
                        email_failed_insert(transaction_id);
                        console.log(error);
                    } // if (json.transactionid !== undefined && json.transactionid !== '')
                } // if (!error && response.statusCode === 200) 
            }); // request(url, function (error, response, trHist)
        } // if (transaction_id !== undefined && transaction_id !== null && transaction_id !== '') 
    },


    /**
     * Builds the NMI calls
     * @param req
     * @param res
     * @param other is used to change the transaction type from the call
     * @returns {*}
     */
    runTheAPI = function(req,res,other){
//console.log('runTheAPI')
        var isAPI = true,
            deferred = Q.defer(),
            transactionType = req.param('type'),
            arg = {},
            url = '',
            xmlfile=false;
        var thisAmount = 0,
            thisTax = 0,
            thisShipping = 0;
        if ((transactionType === undefined)&&(req.param('customerVault') === 'add_customer')){
            transactionType = 'create';
        }
        if ((transactionType === undefined)&&(req.param('customerVault') === 'update_customer')){
            transactionType = 'update';
        }
        if ((other!==null)&&(other!=='')){
            transactionType = other;
        }
        switch(transactionType) {
            case 'sale':
                xmlfile=false;
                url = config.api_url.root_transact;
                arg = {
                    type: req.param('type'),
                    ccnumber: req.param('creditCardNumber'),
                    ccexp: req.param('expirationDate'),
                    cvv: req.param('cardSecurityCode'),
                    amount: req.param('amount'),
                    currency: req.param('currency'),
                    processor_id:req.param('processorId'),
                    orderdescription: req.param('orderDescription'),
                    orderid: req.param('orderId'),
                    tax: req.param('tax'),
                    shipping: req.param('shipping'),
                    ponumber: req.param('poNumber'),
                    firstname: req.param('firstNameCard'),
                    lastname: req.param('lastNameCard'),
                    company: req.param('companyCard'),
                    address1: req.param('addressCard'),
                    address2: req.param('addressContCard'),
                    city: req.param('cityCard'),
                    state: req.param('stateProvinceCard'),
                    zip: req.param('zipPostalCodeCard'),
                    country: req.param('countryCard'),
                    phone: req.param('phoneNumberCard'),
                    fax: req.param('faxNumberCard'),
                    email: req.param('emailAddressCard'),
                    shipping_firstname: req.param('firstNameShipping'),
                    shipping_lastname: req.param('lastNameShipping'),
                    shipping_company: req.param('companyShipping'),
                    shipping_address1: req.param('addressShipping'),
                    shipping_address2: req.param('addressContShipping'),
                    shipping_city: req.param('cityShipping'),
                    shipping_state: req.param('stateProvinceShipping'),
                    shipping_zip: req.param('zipPostalCodeShipping'),
                    shipping_country: req.param('countryShipping'),
                    shipping_email: req.param('emailAddressShipping'),
                    customer_vault:req.param('customerVault'),
                    //customer_receipt:req.param('customerReceipt'),
                    merchant_defined_field_1 : req.param('productMDF1'),
                    merchant_defined_field_2 : req.param('productMDF2'),
                    merchant_defined_field_3 : req.param('productMDF3'),
                    merchant_defined_field_4 : req.param('productMDF4'),
                    merchant_defined_field_5 : req.param('productMDF5'),
                    merchant_defined_field_6 : req.param('productMDF6'),
                    merchant_defined_field_7 : req.param('productMDF7'),
                    merchant_defined_field_8 : req.param('productMDF8'),
                    merchant_defined_field_9 : req.param('productMDF9'),
                    merchant_defined_field_10 : req.param('productMDF10'),
                    merchant_defined_field_11 : req.param('adminMDF1'),
                    merchant_defined_field_12 : req.param('adminMDF2'),
                    merchant_defined_field_13 : req.param('adminMDF3'),
                    merchant_defined_field_14 : req.param('adminMDF4'),
                    merchant_defined_field_15 : req.param('adminMDF5'),
                    merchant_defined_field_16 : req.param('merchantMDF1'),
                    merchant_defined_field_17 : req.param('merchantMDF2'),
                    merchant_defined_field_18 : req.param('merchantMDF3'),
                    merchant_defined_field_19 : req.param('merchantMDF4'),
                    merchant_defined_field_20 : req.param('merchantMDF5')
                };
                send_mojopay_receipt = req.param('customerReceipt');
                mojo_receipt_email_address = req.param('emailAddressCard');
                mojo_receipt_customer_name = req.param('firstNameCard') + ' ' + req.param('lastNameCard');
                mojo_reciept_billing_address = mojo_receipt_customer_name + '<br>' + 
                                               req.param('addressCard') + '<br>' +
                                               req.param('addressContCard') + '<br>' +
                                               req.param('cityCard') + ',' +
                                               req.param('stateProvinceCard') + ' ' +
                                               req.param('zipPostalCodeCard') + '<br>' +
                                               req.param('countryCard') + '<br>' +
                                               req.param('phoneNumberCard');
                mojo_reciept_shipping_address = req.param('addressShipping') + '<br>' +
                                                req.param('addressContShipping') + '<br>' +
                                                req.param('cityShipping') + ',' +
                                                req.param('stateProvinceShipping') + ' ' +
                                                req.param('zipPostalCodeShipping') + '<br>' +
                                                req.param('countryShipping');
                mojo_receipt_card_type = req.param('creditCardNumber');
                thisAmount = 0;
                thisTax = 0;
                thisShipping = 0;
                if(req.param('amount') !== undefined && req.param('amount') !== null && req.param('amount') !== '') {
                    thisAmount = parseFloat(req.param('amount'));
                }
                if(req.param('tax') !== undefined && req.param('tax') !== null && req.param('tax') !== '') {
                    thisTax = parseFloat(req.param('tax'));
                }
                if(req.param('shipping') !== undefined && req.param('shipping') !== null && req.param('shipping') !== '') {
                    thisShipping = parseFloat(req.param('shipping'));
                }
                mojo_receipt_amount =  thisAmount + thisTax + thisShipping;
	
                if (req.param('customerHash') !== undefined){
                    arg.customer_vault_id = req.param('customerHash');
                }
                break;
            case 'auth':
                xmlfile=false;
                url = config.api_url.root_transact;
                arg = {
                    type: req.param('type'),
                    ccnumber: req.param('creditCardNumber'),
                    ccexp: req.param('expirationDate'),
                    cvv: req.param('cardSecurityCode'),
                    amount: req.param('amount'),
                    currency: req.param('currency'),
                    processor_id:req.param('processorId'),
                    orderdescription: req.param('orderDescription'),
                    orderid: req.param('orderId'),
                    tax: req.param('tax'),
                    shipping: req.param('shipping'),
                    ponumber: req.param('poNumber'),
                    firstname: req.param('firstNameCard'),
                    lastname: req.param('lastNameCard'),
                    company: req.param('companyCard'),
                    address1: req.param('addressCard'),
                    address2: req.param('addressContCard'),
                    city: req.param('cityCard'),
                    state: req.param('stateProvinceCard'),
                    zip: req.param('zipPostalCodeCard'),
                    country: req.param('countryCard'),
                    phone: req.param('phoneNumberCard'),
                    fax: req.param('faxNumberCard'),
                    email: req.param('emailAddressCard'),
                    shipping_firstname: req.param('firstNameShipping'),
                    shipping_lastname: req.param('lastNameShipping'),
                    shipping_company: req.param('companyShipping'),
                    shipping_address1: req.param('addressShipping'),
                    shipping_address2: req.param('addressContShipping'),
                    shipping_city: req.param('cityShipping'),
                    shipping_state: req.param('stateProvinceShipping'),
                    shipping_zip: req.param('zipPostalCodeShipping'),
                    shipping_country: req.param('countryShipping'),
                    shipping_email: req.param('emailAddressShipping'),
                    customer_vault:req.param('customerVault'),
                    //customer_receipt:req.param('customerReceipt'),
                    merchant_defined_field_1 : req.param('productMDF1'),
                    merchant_defined_field_2 : req.param('productMDF2'),
                    merchant_defined_field_3 : req.param('productMDF3'),
                    merchant_defined_field_4 : req.param('productMDF4'),
                    merchant_defined_field_5 : req.param('productMDF5'),
                    merchant_defined_field_6 : req.param('productMDF6'),
                    merchant_defined_field_7 : req.param('productMDF7'),
                    merchant_defined_field_8 : req.param('productMDF8'),
                    merchant_defined_field_9 : req.param('productMDF9'),
                    merchant_defined_field_10 : req.param('productMDF10'),
                    merchant_defined_field_11 : req.param('adminMDF1'),
                    merchant_defined_field_12 : req.param('adminMDF2'),
                    merchant_defined_field_13 : req.param('adminMDF3'),
                    merchant_defined_field_14 : req.param('adminMDF4'),
                    merchant_defined_field_15 : req.param('adminMDF5'),
                    merchant_defined_field_16 : req.param('merchantMDF1'),
                    merchant_defined_field_17 : req.param('merchantMDF2'),
                    merchant_defined_field_18 : req.param('merchantMDF3'),
                    merchant_defined_field_19 : req.param('merchantMDF4'),
                    merchant_defined_field_20 : req.param('merchantMDF5')
                };
                send_mojopay_receipt = req.param('customerReceipt');
                mojo_receipt_email_address = req.param('emailAddressCard');
                mojo_receipt_customer_name = req.param('firstNameCard') + ' ' + req.param('lastNameCard');
                mojo_reciept_billing_address = mojo_receipt_customer_name + '<br>' + 
                                               req.param('addressCard') + '<br>' +
                                               req.param('addressContCard') + '<br>' +
                                               req.param('cityCard') + ',' +
                                               req.param('stateProvinceCard') + ' ' +
                                               req.param('zipPostalCodeCard') + '<br>' +
                                               req.param('countryCard') + '<br>' +
                                               req.param('phoneNumberCard');
                mojo_reciept_shipping_address = req.param('addressShipping') + '<br>' +
                                                req.param('addressContShipping') + '<br>' +
                                                req.param('cityShipping') + ',' +
                                                req.param('stateProvinceShipping') + ' ' +
                                                req.param('zipPostalCodeShipping') + '<br>' +
                                                req.param('countryShipping');
                mojo_receipt_card_type = req.param('creditCardNumber');
                thisAmount = 0;
                thisTax = 0;
                thisShipping = 0;
                if(req.param('amount') !== undefined && req.param('amount') !== null && req.param('amount') !== '') {
                    thisAmount = parseFloat(req.param('amount'));
                }
                if(req.param('tax') !== undefined && req.param('tax') !== null && req.param('tax') !== '') {
                    thisTax = parseFloat(req.param('tax'));
                }
                if(req.param('shipping') !== undefined && req.param('shipping') !== null && req.param('shipping') !== '') {
                    thisShipping = parseFloat(req.param('shipping'));
                }
                mojo_receipt_amount =  thisAmount + thisTax + thisShipping;
                
                if (req.param('customerHash') !== undefined){
                    arg.customer_vault_id = req.param('customerHash');
                }
                break;
            case 'credit':
                xmlfile=false;
                url = config.api_url.root_transact;
                arg = {
                    type: req.param('type'),
                    ccnumber: req.param('creditCardNumber'),
                    ccexp: req.param('expirationDate'),
                    cvv: req.param('cardSecurityCode'),
                    amount: req.param('amount'),
                    currency: req.param('currency'),
                    processor_id:req.param('processorId'),
                    orderdescription: req.param('orderDescription'),
                    orderid: req.param('orderId'),
                    tax: req.param('tax'),
                    shipping: req.param('shipping'),
                    ponumber: req.param('poNumber'),
                    firstname: req.param('firstNameCard'),
                    lastname: req.param('lastNameCard'),
                    company: req.param('companyCard'),
                    address1: req.param('addressCard'),
                    address2: req.param('addressContCard'),
                    city: req.param('cityCard'),
                    state: req.param('stateProvinceCard'),
                    zip: req.param('zipPostalCodeCard'),
                    country: req.param('countryCard'),
                    phone: req.param('phoneNumberCard'),
                    fax: req.param('faxNumberCard'),
                    email: req.param('emailAddressCard'),
                    shipping_firstname: req.param('firstNameShipping'),
                    shipping_lastname: req.param('lastNameShipping'),
                    shipping_company: req.param('companyShipping'),
                    shipping_address1: req.param('addressShipping'),
                    shipping_address2: req.param('addressContShipping'),
                    shipping_city: req.param('cityShipping'),
                    shipping_state: req.param('stateProvinceShipping'),
                    shipping_zip: req.param('zipPostalCodeShipping'),
                    shipping_country: req.param('countryShipping'),
                    shipping_email: req.param('emailAddressShipping'),
                    customer_vault:req.param('customerVault'),
                    //customer_receipt:req.param('customerReceipt'),
                    merchant_defined_field_1 : req.param('productMDF1'),
                    merchant_defined_field_2 : req.param('productMDF2'),
                    merchant_defined_field_3 : req.param('productMDF3'),
                    merchant_defined_field_4 : req.param('productMDF4'),
                    merchant_defined_field_5 : req.param('productMDF5'),
                    merchant_defined_field_6 : req.param('productMDF6'),
                    merchant_defined_field_7 : req.param('productMDF7'),
                    merchant_defined_field_8 : req.param('productMDF8'),
                    merchant_defined_field_9 : req.param('productMDF9'),
                    merchant_defined_field_10 : req.param('productMDF10'),
                    merchant_defined_field_11 : req.param('adminMDF1'),
                    merchant_defined_field_12 : req.param('adminMDF2'),
                    merchant_defined_field_13 : req.param('adminMDF3'),
                    merchant_defined_field_14 : req.param('adminMDF4'),
                    merchant_defined_field_15 : req.param('adminMDF5'),
                    merchant_defined_field_16 : req.param('merchantMDF1'),
                    merchant_defined_field_17 : req.param('merchantMDF2'),
                    merchant_defined_field_18 : req.param('merchantMDF3'),
                    merchant_defined_field_19 : req.param('merchantMDF4'),
                    merchant_defined_field_20 : req.param('merchantMDF5')
                };
                send_mojopay_receipt = req.param('customerReceipt');
                mojo_receipt_email_address = req.param('emailAddressCard');
                mojo_receipt_customer_name = req.param('firstNameCard') + ' ' + req.param('lastNameCard');
                mojo_reciept_billing_address = mojo_receipt_customer_name + '<br>' + 
                                               req.param('addressCard') + '<br>' +
                                               req.param('addressContCard') + '<br>' +
                                               req.param('cityCard') + ',' +
                                               req.param('stateProvinceCard') + ' ' +
                                               req.param('zipPostalCodeCard') + '<br>' +
                                               req.param('countryCard') + '<br>' +
                                               req.param('phoneNumberCard');
                mojo_reciept_shipping_address = req.param('addressShipping') + '<br>' +
                                                req.param('addressContShipping') + '<br>' +
                                                req.param('cityShipping') + ',' +
                                                req.param('stateProvinceShipping') + ' ' +
                                                req.param('zipPostalCodeShipping') + '<br>' +
                                                req.param('countryShipping');
                mojo_receipt_card_type = req.param('creditCardNumber');
                thisAmount = 0;
                thisTax = 0;
                thisShipping = 0;
                if(req.param('amount') !== undefined && req.param('amount') !== null && req.param('amount') !== '') {
                    thisAmount = parseFloat(req.param('amount'));
                }
                if(req.param('tax') !== undefined && req.param('tax') !== null && req.param('tax') !== '') {
                    thisTax = parseFloat(req.param('tax'));
                }
                if(req.param('shipping') !== undefined && req.param('shipping') !== null && req.param('shipping') !== '') {
                    thisShipping = parseFloat(req.param('shipping'));
                }
                mojo_receipt_amount =  thisAmount + thisTax + thisShipping;
 
                if (req.param('customerHash') !== undefined){
                    arg.customer_vault_id = req.param('customerHash');
                }
                break;
            case 'validate':
                xmlfile=false;
                url = config.api_url.root_transact;
                arg = {
                    type: req.param('type'),
                    ccnumber: req.param('creditCardNumber'),
                    ccexp: req.param('expirationDate'),
                    cvv: req.param('cardSecurityCode'),
                    amount: req.param('amount'),
                    currency: req.param('currency'),
                    processor_id:req.param('processorId'),
                    orderdescription: req.param('orderDescription'),
                    orderid: req.param('orderId'),
                    tax: req.param('tax'),
                    shipping: req.param('shipping'),
                    ponumber: req.param('poNumber'),
                    firstname: req.param('firstNameCard'),
                    lastname: req.param('lastNameCard'),
                    company: req.param('companyCard'),
                    address1: req.param('addressCard'),
                    address2: req.param('addressContCard'),
                    city: req.param('cityCard'),
                    state: req.param('stateProvinceCard'),
                    zip: req.param('zipPostalCodeCard'),
                    country: req.param('countryCard'),
                    phone: req.param('phoneNumberCard'),
                    fax: req.param('faxNumberCard'),
                    email: req.param('emailAddressCard'),
                    shipping_firstname: req.param('firstNameShipping'),
                    shipping_lastname: req.param('lastNameShipping'),
                    shipping_company: req.param('companyShipping'),
                    shipping_address1: req.param('addressShipping'),
                    shipping_address2: req.param('addressContShipping'),
                    shipping_city: req.param('cityShipping'),
                    shipping_state: req.param('stateProvinceShipping'),
                    shipping_zip: req.param('zipPostalCodeShipping'),
                    shipping_country: req.param('countryShipping'),
                    shipping_email: req.param('emailAddressShipping'),
                    customer_vault:req.param('customerVault'),
                    //customer_receipt:req.param('customerReceipt'),
                    merchant_defined_field_1 : req.param('productMDF1'),
                    merchant_defined_field_2 : req.param('productMDF2'),
                    merchant_defined_field_3 : req.param('productMDF3'),
                    merchant_defined_field_4 : req.param('productMDF4'),
                    merchant_defined_field_5 : req.param('productMDF5'),
                    merchant_defined_field_6 : req.param('productMDF6'),
                    merchant_defined_field_7 : req.param('productMDF7'),
                    merchant_defined_field_8 : req.param('productMDF8'),
                    merchant_defined_field_9 : req.param('productMDF9'),
                    merchant_defined_field_10 : req.param('productMDF10'),
                    merchant_defined_field_11 : req.param('adminMDF1'),
                    merchant_defined_field_12 : req.param('adminMDF2'),
                    merchant_defined_field_13 : req.param('adminMDF3'),
                    merchant_defined_field_14 : req.param('adminMDF4'),
                    merchant_defined_field_15 : req.param('adminMDF5'),
                    merchant_defined_field_16 : req.param('merchantMDF1'),
                    merchant_defined_field_17 : req.param('merchantMDF2'),
                    merchant_defined_field_18 : req.param('merchantMDF3'),
                    merchant_defined_field_19 : req.param('merchantMDF4'),
                    merchant_defined_field_20 : req.param('merchantMDF5')
                };
                send_mojopay_receipt = req.param('customerReceipt');
                mojo_receipt_email_address = req.param('emailAddressCard');
                mojo_receipt_customer_name = req.param('firstNameCard') + ' ' + req.param('lastNameCard');
                mojo_reciept_billing_address = mojo_receipt_customer_name + '<br>' + 
                                               req.param('addressCard') + '<br>' +
                                               req.param('addressContCard') + '<br>' +
                                               req.param('cityCard') + ',' +
                                               req.param('stateProvinceCard') + ' ' +
                                               req.param('zipPostalCodeCard') + '<br>' +
                                               req.param('countryCard') + '<br>' +
                                               req.param('phoneNumberCard');
                mojo_reciept_shipping_address = req.param('addressShipping') + '<br>' +
                                                req.param('addressContShipping') + '<br>' +
                                                req.param('cityShipping') + ',' +
                                                req.param('stateProvinceShipping') + ' ' +
                                                req.param('zipPostalCodeShipping') + '<br>' +
                                                req.param('countryShipping');
                mojo_receipt_amount = parseFloat(req.param('amount')) + parseFloat(req.param('tax')) + parseFloat(req.param('shipping'));
                mojo_receipt_card_type = req.param('creditCardNumber');
                thisAmount = 0;
                thisTax = 0;
                thisShipping = 0;
                if(req.param('amount') !== undefined && req.param('amount') !== null && req.param('amount') !== '') {
                    thisAmount = parseFloat(req.param('amount'));
                }
                if(req.param('tax') !== undefined && req.param('tax') !== null && req.param('tax') !== '') {
                    thisTax = parseFloat(req.param('tax'));
                }
                if(req.param('shipping') !== undefined && req.param('shipping') !== null && req.param('shipping') !== '') {
                    thisShipping = parseFloat(req.param('shipping'));
                }
                mojo_receipt_amount =  thisAmount + thisTax + thisShipping;
  
                if (req.param('customerHash') !== undefined){
                    arg.customer_vault_id = req.param('customerHash');
                }
                break;
            case 'salevault':
                xmlfile=false;
                url = config.api_url.root_transact;
                arg = {
                    type: 'validate',
                    amount: '0.00',
                    cvv: req.param('cardSecurityCode'),
                    orderid: req.param('orderId'),
                    orderdescription: req.param('orderDescription'),
                    ponumber: req.param('poNumber'),
                    shipping: req.param('shipping'),
                    tax: req.param('tax'),
                    customer_vault_id:req.param('customerHash'),
                    //customer_receipt:req.param('customerReceipt'),
                    processor_id:req.param('processorId')
                };
                send_mojopay_receipt = req.param('customerReceipt');
                mojo_receipt_email_address = req.param('emailAddressCard');
                mojo_receipt_customer_name = req.param('firstNameCard') + ' ' + req.param('lastNameCard');
                mojo_reciept_billing_address = mojo_receipt_customer_name + '<br>' + 
                                               req.param('addressCard') + '<br>' +
                                               req.param('addressContCard') + '<br>' +
                                               req.param('cityCard') + ',' +
                                               req.param('stateProvinceCard') + ' ' +
                                               req.param('zipPostalCodeCard') + '<br>' +
                                               req.param('countryCard') + '<br>' +
                                               req.param('phoneNumberCard');
                mojo_reciept_shipping_address = req.param('addressShipping') + '<br>' +
                                                req.param('addressContShipping') + '<br>' +
                                                req.param('cityShipping') + ',' +
                                                req.param('stateProvinceShipping') + ' ' +
                                                req.param('zipPostalCodeShipping') + '<br>' +
                                                req.param('countryShipping');
                mojo_receipt_card_type = req.param('creditCardNumber');
                thisAmount = 0;
                thisTax = 0;
                thisShipping = 0;
                if(req.param('amount') !== undefined && req.param('amount') !== null && req.param('amount') !== '') {
                    thisAmount = parseFloat(req.param('amount'));
                }
                if(req.param('tax') !== undefined && req.param('tax') !== null && req.param('tax') !== '') {
                    thisTax = parseFloat(req.param('tax'));
                }
                if(req.param('shipping') !== undefined && req.param('shipping') !== null && req.param('shipping') !== '') {
                    thisShipping = parseFloat(req.param('shipping'));
                }
                mojo_receipt_amount =  thisAmount + thisTax + thisShipping;
  
                break;
            case 'create':
                xmlfile=false;
                url = config.api_url.root_transact;
                arg = {
                    type: 'validate', // Try this out.
                    amount: '0.00',
                    customer_vault:'add_customer',
                    billing_id : req.param('billingId'),
                    ccnumber: req.param('creditCardNumber'),
                    cvv: req.param('cardSecurityCode'),
                    ccexp: req.param('expirationDate'),
                    currency: req.param('currency'),
                    orderdescription: req.param('orderDescription'),
                    orderid: req.param('orderId'),
                    first_name  : req.param('firstNameCard'),
                    last_name : req.param('lastNameCard'),
                    address1 : req.param('addressCard'),
                    city : req.param('cityCard'),
                    state : req.param('stateProvinceCard'),
                    zip: req.param('zipPostalCodeCard'),
                    country : req.param('countryCard'),
                    phone : req.param('phoneNumberCard'),
                    email : req.param('emailAddressCard'),
                    company : req.param('companyCard'),
                    address2 : req.param('addressContCard'),
                    fax : req.param('faxNumberCard'),
                    shipping_id : req.param('shippingId'),
                    shipping_firstname  : req.param('firstNameShipping'),
                    shipping_lastname : req.param('lastNameShipping'),
                    shipping_company : req.param('companyShipping'),
                    shipping_address1 : req.param('addressShipping'),
                    shipping_address2 : req.param('addressContShipping'),
                    shipping_city : req.param('cityShipping'),
                    shipping_state : req.param('stateProvinceShipping'),
                    shipping_zip: req.param('zipPostalCodeShipping'),
                    shipping_country : req.param('countryShipping'),
                    shipping_phone : req.param('phoneNumberShipping'),
                    shipping_fax : req.param('faxNumberShipping'),
                    shipping_email : req.param('emailAddressShipping')
                };
                if (req.param('customerHash') !== undefined){
                    arg.customer_vault_id = req.param('customerHash');
                }
                break;
            case 'delete':
                isAPI = false;
                xmlfile=false;
                url = config.api_url.root_transact;
                arg = {
                    customer_vault:'delete_customer',
                    customer_vault_id : req.param('customerHash')
                };                           
                vaultMaintenance(req, 'delete', req.param('customerHash'), credentialsU, credentialsP);
                deferred.resolve(true);
                break;
            case 'update':
                xmlfile=false;
                url = config.api_url.root_transact;
                arg = {
                    customer_vault:'update_customer',
                    customer_vault_id : req.param('customerHash'),
                    billing_id : req.param('billingId'),
                    ccnumber: req.param('creditCardNumber'),
                    cvv: req.param('cardSecurityCode'),
                    ccexp: req.param('expirationDate'),
                    currency: req.param('currency'),
                    orderdescription: req.param('orderDescription'),
                    orderid: req.param('orderId'),
                    first_name  : req.param('firstNameCard'),
                    last_name : req.param('lastNameCard'),
                    address1 : req.param('addressCard'),
                    city : req.param('cityCard'),
                    state : req.param('stateProvinceCard'),
                    zip: req.param('zipPostalCodeCard'),
                    country : req.param('countryCard'),
                    phone : req.param('phoneNumberCard'),
                    email : req.param('emailAddressCard'),
                    company : req.param('companyCard'),
                    address2 : req.param('addressContCard'),
                    fax : req.param('faxNumberCard'),
                    shipping_id : req.param('shippingId'),
                    shipping_firstname  : req.param('firstNameShipping'),
                    shipping_lastname : req.param('lastNameShipping'),
                    shipping_company : req.param('companyShipping'),
                    shipping_address1 : req.param('addressShipping'),
                    shipping_address2 : req.param('addressContShipping'),
                    shipping_city : req.param('cityShipping'),
                    shipping_state : req.param('stateProvinceShipping'),
                    shipping_zip: req.param('zipPostalCodeShipping'),
                    shipping_country : req.param('countryShipping'),
                    shipping_phone : req.param('phoneNumberShipping'),
                    shipping_fax : req.param('faxNumberShipping'),
                    shipping_email : req.param('emailAddressShipping')
                };
                break;
            case 'creport':
                xmlfile=true;
                url = config.api_url.root_query;
                arg = {
                    report_type: 'customer_vault',
                    customer_vault_id: req.param('customerHash')
                     //customer_vault_id: req.param('customerVaultId')
                };
                break;
            case 'create_customer_card_hash':
                isAPI = false;
                var customer_card_hash = {
                    transaction_id: req.param('transaction_id')
                };
                credentialPro = req.param('processorId');
                db.Payment_Plan.find({where:{Processor_ID:credentialPro}}).success(function(payment_plan){
                    if (payment_plan) {
                        boardingGlobal = payment_plan.Boarding_ID;
                        if (theObject.customer_hash !== undefined) {
                            customer_card_hash.CC_Number = doMask(req.param('creditCardNumber'));
                            customer_card_hash.Brand = getBrand(req.param('creditCardNumber'));
                            customer_card_hash.Hash = theObject.customer_hash;
                            customer_card_hash.Boarding_ID = boardingGlobal;
                        }else {
                            customer_card_hash.CC_Number = doMask(req.param('creditCardNumber'));
                            customer_card_hash.Brand = getBrand(req.param('creditCardNumber'));
                            customer_card_hash.Hash = req.param('customerHash');
                            customer_card_hash.Boarding_ID = boardingGlobal;
                        }
                        db.Customer_Card_Hash.find({where:{Hash:customer_card_hash.Hash,Boarding_ID:boardingGlobal}}).success(function(cc){
                            if (!cc){
                                db.Customer_Card_Hash.create(customer_card_hash).success(function(customer_card_hash,err){
                                    if(!customer_card_hash){
                                        //res.send('users/signup', {errors: err});
                                        deferred.resolve(true);
                                    } else {
                                        //res.jsonp({Response:'customer_card_hash done!'});
                                        deferred.resolve(true);
                                    }
                                }).error(function(err){
                                    console.log(err);
                                });
                            } else {
                                deferred.resolve(true);
                            }
                        });
                    }
                });
                break;
            case 'capture':
                xmlfile=false;
                url = config.api_url.root_transact;
                arg = {
                    type: req.param('type'),
                    amount: req.param('amount'),
                    transactionid: req.param('transactionId'),
                    merchant_defined_field_1 : req.param('productMDF1'),
                    merchant_defined_field_2 : req.param('productMDF2'),
                    merchant_defined_field_3 : req.param('productMDF3'),
                    merchant_defined_field_4 : req.param('productMDF4'),
                    merchant_defined_field_5 : req.param('productMDF5'),
                    merchant_defined_field_6 : req.param('productMDF6'),
                    merchant_defined_field_7 : req.param('productMDF7'),
                    merchant_defined_field_8 : req.param('productMDF8'),
                    merchant_defined_field_9 : req.param('productMDF9'),
                    merchant_defined_field_10 : req.param('productMDF10'),
                    merchant_defined_field_11 : req.param('adminMDF1'),
                    merchant_defined_field_12 : req.param('adminMDF2'),
                    merchant_defined_field_13 : req.param('adminMDF3'),
                    merchant_defined_field_14 : req.param('adminMDF4'),
                    merchant_defined_field_15 : req.param('adminMDF5'),
                    merchant_defined_field_16 : req.param('merchantMDF1'),
                    merchant_defined_field_17 : req.param('merchantMDF2'),
                    merchant_defined_field_18 : req.param('merchantMDF3'),
                    merchant_defined_field_19 : req.param('merchantMDF4'),
                    merchant_defined_field_20 : req.param('merchantMDF5')
                };
                if (req.param('customerVaultId') !== undefined){
                    arg.customer_vault_id = req.param('customerVaultId');
                }
                break;
            case 'void':
                xmlfile=false;
                url = config.api_url.root_transact;
                arg = {
                    type: req.param('type'),
                    transactionid: req.param('transactionId'),
                    merchant_defined_field_1 : req.param('productMDF1'),
                    merchant_defined_field_2 : req.param('productMDF2'),
                    merchant_defined_field_3 : req.param('productMDF3'),
                    merchant_defined_field_4 : req.param('productMDF4'),
                    merchant_defined_field_5 : req.param('productMDF5'),
                    merchant_defined_field_6 : req.param('productMDF6'),
                    merchant_defined_field_7 : req.param('productMDF7'),
                    merchant_defined_field_8 : req.param('productMDF8'),
                    merchant_defined_field_9 : req.param('productMDF9'),
                    merchant_defined_field_10 : req.param('productMDF10'),
                    merchant_defined_field_11 : req.param('adminMDF1'),
                    merchant_defined_field_12 : req.param('adminMDF2'),
                    merchant_defined_field_13 : req.param('adminMDF3'),
                    merchant_defined_field_14 : req.param('adminMDF4'),
                    merchant_defined_field_15 : req.param('adminMDF5'),
                    merchant_defined_field_16 : req.param('merchantMDF1'),
                    merchant_defined_field_17 : req.param('merchantMDF2'),
                    merchant_defined_field_18 : req.param('merchantMDF3'),
                    merchant_defined_field_19 : req.param('merchantMDF4'),
                    merchant_defined_field_20 : req.param('merchantMDF5')
                };
                if (req.param('customerVaultId') !== undefined){
                    arg.customer_vault_id = req.param('customerVaultId');
                }
                break;
            case 'refund':
                xmlfile=false;
                url = config.api_url.root_transact;
                arg = {
                    type: req.param('type'),
                    amount: req.param('amount'),
                    transactionid: req.param('transactionId'),
                    merchant_defined_field_1 : req.param('productMDF1'),
                    merchant_defined_field_2 : req.param('productMDF2'),
                    merchant_defined_field_3 : req.param('productMDF3'),
                    merchant_defined_field_4 : req.param('productMDF4'),
                    merchant_defined_field_5 : req.param('productMDF5'),
                    merchant_defined_field_6 : req.param('productMDF6'),
                    merchant_defined_field_7 : req.param('productMDF7'),
                    merchant_defined_field_8 : req.param('productMDF8'),
                    merchant_defined_field_9 : req.param('productMDF9'),
                    merchant_defined_field_10 : req.param('productMDF10'),
                    merchant_defined_field_11 : req.param('adminMDF1'),
                    merchant_defined_field_12 : req.param('adminMDF2'),
                    merchant_defined_field_13 : req.param('adminMDF3'),
                    merchant_defined_field_14 : req.param('adminMDF4'),
                    merchant_defined_field_15 : req.param('adminMDF5'),
                    merchant_defined_field_16 : req.param('merchantMDF1'),
                    merchant_defined_field_17 : req.param('merchantMDF2'),
                    merchant_defined_field_18 : req.param('merchantMDF3'),
                    merchant_defined_field_19 : req.param('merchantMDF4'),
                    merchant_defined_field_20 : req.param('merchantMDF5')
                };
                if (req.param('customerVaultId') !== undefined){
                    arg.customer_vault_id = req.param('customerVaultId');
                }
                break;
            case 'add_plan':
                isAPI = false;
                db.Payment_Plan.find({where:{Processor_ID:credentialPro}}).success(function(payment_plan){
                    if (payment_plan) {
                        var day_frequency = req.param('dayFrequency'),
                            month_frequency = req.param('monthFrequency'),
                            day_of_month = req.param('dayOfMonth');

                        if (day_frequency === undefined || day_frequency === null || day_frequency === '') {
                            day_frequency = 0;
                        }
                        if (month_frequency === undefined || month_frequency === null || month_frequency === '') {
                            month_frequency = 0;
                        }
                        if (day_of_month === undefined || day_of_month === null || day_of_month === '') {
                            day_of_month = 0;
                        }
                        
                        if (isNaN(req.param('amount')) || req.param('amount') <= 0) {
                            deferred.resolve('amount must be greater than zero');
                        } else if ((+req.param('planId') !== parseInt(req.param('planId'))) || isNaN(req.param('planId')) || req.param('planId') < 0) {
                            deferred.resolve('planId must be a number greater than zero');
                        } else if ((+req.param('planPayments') !== parseInt(req.param('planPayments'))) || isNaN(req.param('planPayments')) || req.param('planPayments') < 0) {
                            deferred.resolve('planPayments must be greater than or equal to zero');
                        } else if (!(req.param('dayFrequency') === undefined || req.param('dayFrequency') === null || req.param('dayFrequency') === '') &&  (+req.param('dayFrequency') !== parseInt(req.param('dayFrequency'))) || isNaN(day_frequency) || ((req.param('monthFrequency') === undefined || req.param('monthFrequency') === null || req.param('monthFrequency') === '') && day_frequency < 1)) {
                            deferred.resolve('dayFrequency must be greater than zero');
                        } else if (!(req.param('monthFrequency') === undefined || req.param('monthFrequency') === null || req.param('monthFrequency') === '') && (+req.param('monthFrequency') !== parseInt(req.param('monthFrequency'))) || (isNaN(month_frequency)) || ((req.param('dayFrequency') === undefined || req.param('dayFrequency') === null || req.param('dayFrequency') === '') && (month_frequency < 1 || month_frequency > 24))) {
                            deferred.resolve('monthFrequency must be between 1 and 24');
                        } else if (!(req.param('monthFrequency') === undefined || req.param('monthFrequency') === null || req.param('monthFrequency') === '') && (+req.param('dayOfMonth') !== parseInt(req.param('dayOfMonth'))) || (isNaN(day_of_month)) || ((req.param('dayFrequency') === undefined || req.param('dayFrequency') === null || req.param('dayFrequency') === '')) && (day_of_month < 1 || day_of_month > 31)) {
                            deferred.resolve('dayOfMonth must be between 1 and 31');
                        } else {
                            var recurringPlanObject = {
                                Boarding_ID: payment_plan.Boarding_ID,
                                Plan_ID: req.param('planId'),
                                Plan_Name: req.param('planName'),
                                Plan_Amount: req.param('amount'),
                                Plan_Payments: req.param('planPayments'),
                                Day_Of_Month: day_of_month,
                                Day_Frequency: day_frequency,
                                Month_Frequency: month_frequency,
                                Processor_ID: credentialPro
                            };
                            // save and return and instance of recurring_plan on the res object.
                            db.Recurring_Plan.create(recurringPlanObject).success(function(plan,err){
                                if(!plan){
                                    //res.send('users/signup', {errors: err});
                                    deferred.resolve('There was an error adding the plan - ' + err);
                                } else {
                                    //res.jsonp({Response:'customer_card_hash done!'});
                                    deferred.resolve('Plan ' + req.param('planId') + ' added successfully');
                                }
                            }).error(function(err){
                                console.log(err);
                                if (err.toString().indexOf('ER_DUP_ENTRY: Duplicate entry') >= 0 ) {
                                    deferred.resolve('This planId has already been used in the past, on an active or previously deleted plan');
                                } else {
                                    deferred.resolve('There was an error adding the plan - ' + err);
                                }
                            });
                        } // if () else if () else if () ... else
                    } // if (payment_plan)
                }).error(function(err){
                    console.log(err);
                    deferred.resolve('There was an error adding the plan - ' + err);
                }); // db.Payment_Plan.find()
                break;
            case 'delete_plan':
                isAPI = false;
                db.Subscription.find({where: {Plan_ID: req.param('planId')}}).success(function(subscription,err) {
                    if (subscription) {
                        deferred.resolve('A customer is associated with this plan. You can not delete the plan.');
                    } else {
                        db.Recurring_Plan.find({where: {Plan_ID: req.param('planId'), Is_Active: 1}}).success(function(plan,err){
                            if(!plan){
                                //res.send('users/signup', {errors: err});
                                deferred.resolve('This plan does not exist.');
                            } else {
                                plan.updateAttributes({
                                    Is_Active: 0
                                }).success(function(a){
                                    deferred.resolve('Plan ' + req.param('planId') + ' deleted successfully');
                                }).error(function(err){
                                    deferred.resolve('There was an error deleting the plan - ' + err);
                                });
                            }
                        }).error(function(err){
                            console.log(err);
                            deferred.resolve('There was an error deleting the plan - ' + err);
                        });
                    } // if (subscription)
                }).error(function(err){
                    console.log(err);
                    deferred.resolve('There was an error deleting the plan - ' + err);
                });
                break;
            case 'update_plan':
                isAPI = false;
                db.Subscription.find({where: {Plan_ID: req.param('planId')}}).success(function(subscription,err) {
                    if (subscription) {
                        deferred.resolve('A customer is associated with this plan. You can not change the plan.');
                    } else {
                        db.Payment_Plan.find({where:{Processor_ID:credentialPro}}).success(function(payment_plan){
                            if (payment_plan) {
                                var day_frequency = req.param('dayFrequency'),
                                    month_frequency = req.param('monthFrequency'),
                                    day_of_month = req.param('dayOfMonth');
        
                                if (day_frequency === undefined || day_frequency === null || day_frequency === '') {
                                    day_frequency = 0;
                                }
                                if (month_frequency === undefined || month_frequency === null || month_frequency === '') {
                                    month_frequency = 0;
                                }
                                if (day_of_month === undefined || day_of_month === null || day_of_month === '') {
                                    day_of_month = 0;
                                }
                                
                        if (isNaN(req.param('amount')) || req.param('amount') <= 0) {
                            deferred.resolve('amount must be greater than zero');
                        } else if ((+req.param('planId') !== parseInt(req.param('planId'))) || isNaN(req.param('planId')) || req.param('planId') < 0) {
                            deferred.resolve('planId must be a number greater than zero');
                        } else if ((+req.param('planPayments') !== parseInt(req.param('planPayments'))) || isNaN(req.param('planPayments')) || req.param('planPayments') < 0) {
                            deferred.resolve('planPayments must be greater than or equal to zero');
                        } else if (!(req.param('dayFrequency') === undefined || req.param('dayFrequency') === null || req.param('dayFrequency') === '') &&  (+req.param('dayFrequency') !== parseInt(req.param('dayFrequency'))) || isNaN(day_frequency) || ((req.param('monthFrequency') === undefined || req.param('monthFrequency') === null || req.param('monthFrequency') === '') && day_frequency < 1)) {
                            deferred.resolve('dayFrequency must be greater than zero');
                        } else if (!(req.param('monthFrequency') === undefined || req.param('monthFrequency') === null || req.param('monthFrequency') === '') && (+req.param('monthFrequency') !== parseInt(req.param('monthFrequency'))) || (isNaN(month_frequency)) || ((req.param('dayFrequency') === undefined || req.param('dayFrequency') === null || req.param('dayFrequency') === '') && (month_frequency < 1 || month_frequency > 24))) {
                            deferred.resolve('monthFrequency must be between 1 and 24');
                        } else if (!(req.param('monthFrequency') === undefined || req.param('monthFrequency') === null || req.param('monthFrequency') === '') && (+req.param('dayOfMonth') !== parseInt(req.param('dayOfMonth'))) || (isNaN(day_of_month)) || ((req.param('dayFrequency') === undefined || req.param('dayFrequency') === null || req.param('dayFrequency') === '')) && (day_of_month < 1 || day_of_month > 31)) {
                            deferred.resolve('dayOfMonth must be between 1 and 31');
                        } else {
                                    var recurringPlanObject = {
                                        Boarding_ID: payment_plan.Boarding_ID,
                                        Plan_ID: req.param('planId'),
                                        Plan_Name: req.param('planName'),
                                        Plan_Amount: req.param('amount'),
                                        Plan_Payments: req.param('planPayments'),
                                        Day_Of_Month: day_of_month,
                                        Day_Frequency: day_frequency,
                                        Month_Frequency: month_frequency,
                                        Processor_ID: credentialPro
                                    };
                                    // save and return and instance of recurring_plan on the res object.
                            
                                    db.Recurring_Plan.find({where: {Plan_ID: req.param('planId'), Is_Active: 1}}).success(function(plan,err){
                                        if(!plan){
                                            //res.send('users/signup', {errors: err});
                                            deferred.resolve('This plan does not exist.');
                                        } else {
                                            plan.updateAttributes({
                                                Plan_Amount: req.param('amount'),
                                                Plan_Payments: req.param('planPayments'),
                                                Day_Of_Month: day_of_month,
                                                Day_Frequency: day_frequency,
                                                Month_Frequency: month_frequency
                                            }).success(function(a){
                                                deferred.resolve('Plan ' + req.param('planId') + ' updated successfully');
                                            }).error(function(err){
                                                deferred.resolve('There was an error updating the plan - ' + err);
                                            });
                                        }
                                    }).error(function(err){
                                        console.log(err);
                                        deferred.resolve('There was an error updating the plan - ' + err);
                                    });
                                } // if () else if () else if () ... else
                           } // if (payment_plan) 
                        }).error(function(err){
                            console.log(err);
                        }); // db.Payment_Plan.find
                            
                    } // if (subscription)
                }).error(function(err){
                    console.log(err);
                    deferred.resolve('There was an error updating the plan - ' + err);
                });
                break;
            case 'delete_sub':
                isAPI = false;
                if (req.param('subscriptionId') !== undefined && req.param('subscriptionId') !== null && req.param('subscriptionId') !== '') {
                    db.Subscription.find({where: {id: req.param('subscriptionId'), Is_Active: 1}}).success(function(subscription,err) {
                        if (subscription) {
                            var customer_id = req.param('customerHash'),
                                plan_id = req.param('planId');
                                
                            if (plan_id === undefined || plan_id === null || plan_id === '') {
                                if (customer_id === undefined || customer_id === null || customer_id === '') {
                                    // planID and CustomerHash are not defined
                                    subscription.updateAttributes({
                                        Is_Active: 0
                                    }).success(function(a){
                                        deferred.resolve('Subscription successfully deleted');
                                    }).error(function(err){
                                        deferred.resolve('There was an error deleting the subscription - ' + err);
                                    });
                                } else if (customer_id === subscription.Customer_Token) {
                                    // planID is undefined, parameter CustomerHash = subscription Customer_Token
                                    subscription.updateAttributes({
                                        Is_Active: 0
                                    }).success(function(a){
                                        deferred.resolve('Subscription successfully deleted');
                                    }).error(function(err){
                                        deferred.resolve('There was an error deleting the subscription - ' + err);
                                    });
                                } else {
                                    deferred.resolve('This customer is not associated with the subscription.');
                                } // if (customer_id !== undefined && customer_id !== null && customer_id !== '') 
                            } else if (customer_id === undefined || customer_id === null || customer_id === '') {
                                if (plan_id.toString() === subscription.Plan_ID.toString()) {
                                    // planID = subscription Plan_ID, customer_id is undefined
                                    subscription.updateAttributes({
                                        Is_Active: 0
                                    }).success(function(a){
                                        deferred.resolve('Subscription successfully deleted');
                                    }).error(function(err){
                                        deferred.resolve('There was an error deleting the subscription - ' + err);
                                    });
                                } else {
                                    deferred.resolve('This plan is not associated with the subscription.');
                                } // if (plan_id === subscription.Plan_ID) 
                            } else {
                                if (plan_id.toString() === subscription.Plan_ID.toString() && customer_id.toString() === subscription.Customer_Token.toString()) {
                                    // planID = subscription Plan_ID, customer_id = subscription Customer Token
                                    subscription.updateAttributes({
                                        Is_Active: 0
                                    }).success(function(a){
                                        deferred.resolve('Subscription successfully deleted');
                                    }).error(function(err){
                                        deferred.resolve('There was an error deleting the subscription - ' + err);
                                    });
                                } else if (plan_id.toString() !== subscription.Plan_ID.toString() && customer_id.toString() !== subscription.Customer_Token.toString()) {
                                    deferred.resolve('This plan and Customer are not associated with the subscription.');
                                } else if (plan_id.toString() !== subscription.Plan_ID.toString()) {
                                    deferred.resolve('This plan is not associated with the subscription.');
                                } else if (customer_id.toString() !== subscription.Customer_Token.toString()) {
                                    deferred.resolve('This customer is not associated with the subscription.');
                                } // if (plan_id === subscription.Plan_ID && (customer_id === subscription.Customer_Token) 
                            } // if else if else
                            
                        } else {
                            deferred.resolve('Could not find a subscription with those parameters.');
                        } // if (subscription)
                    }).error(function(err){
                        console.log(err);
                    });
                } else if (req.param('planId') !== undefined && req.param('planId') !== null && req.param('planId') !== '' &&
                           req.param('customerHash') !== undefined && req.param('customerHash') !== null && req.param('customerHash') !== '') {
                   db.Subscription.find({where: {Plan_ID: req.param('planId'), Customer_Token: req.param('customerHash'), Is_Active: 1}}).success(function(subscription,err) {
                        if (subscription) {
                            subscription.updateAttributes({
                                Is_Active: 0
                            }).success(function(a){
                                deferred.resolve('Subscription successfully deleted');
                            }).error(function(err){
                                deferred.resolve('There was an error deleting the subscription');
                            });
                        } else {
                            deferred.resolve('Could not find a subscription with those parameters.');
                        } // if (subscription)
                    }).error(function(err){
                        console.log(err);
                        deferred.resolve('There was an error deleting the subscription');
                    });
                }
                break;
            case 'add_sub':
                isAPI = false;
                var customer_token = req.param('customerHash'),
                    plan_id = req.param('planId');
                    
                db.Vault.find({where: {customer_vault_id: customer_token, Is_Active: 1}}).success(function(vault,err) {
                    if (!vault) {
                        deferred.resolve('Customer Token does not exist');
                    } else {
                        db.Recurring_Plan.find({where: {Plan_ID: plan_id, Is_Active: 1}}).success(function(plan,err) {
                            if (!plan) {
                                deferred.resolve('Recurring Plan does not exist');
                            } else {
                                // Add the Subscription
                                var subscriptionObject = {
                                    Customer_Token: customer_token,
                                    Plan_ID: plan_id,
                                    Start_Date: req.param('startDate'),
                                    Order_ID: req.param('orderId'),
                                    Order_Description: req.param('orderDescription'),
                                    PO_Number: req.param('poNumber'),
                                    Shipping: req.param('shipping'),
                                    Tax: req.param('tax'),
                                    Tax_Exempt: req.param('taxExempt'),
                                    Next_Charge_Date: req.param('startDate'),
                                    Is_Active: 1
                                };
                                // save and return and instance of recurring_plan on the res object.
                                db.Subscription.create(subscriptionObject).success(function(subscription,err){
                                    if(!subscription){
                                        //res.send('users/signup', {errors: err});
                                        deferred.resolve('There was an error adding the subscription');
                                    } else {
                                        //res.jsonp({Response:'customer_card_hash done!'});
                                        deferred.resolve('Subscription created');
                                    }
                                }).error(function(err){
                                    console.log(err);
                                    deferred.resolve('There was an error adding the subscription');
                                });
                            }
                        }).error(function(err){
                            console.log(err);
                        });

                    }
                }).error(function(err){
                    console.log(err);
                });
                break;
            case 'list_sub':
                isAPI = false;
                var query = '',
                    qarg = {};
                query = 'SELECT sub.id AS subscriptionId, rp.Plan_Payments as billingCycle, rp.Day_Frequency, rp.Month_Frequency, rp.Day_Of_Month, sub.Start_Date as startDate, sub.Order_ID as orderID, sub.Order_Description as orderDescription, sub.PO_Number as poNumber, rp.Plan_Amount as amount, sub.Shipping as shipping, sub.Tax as tax, sub.Tax_Exempt as taxExempt, sub.Completed_Payments as completedPayments, sub.Attempted_Payments as attemptedPayments, sub.Count_Failed as countFailed, sub.Next_Charge_Date as nextChargeDate, rp.Plan_Name as planName, rp.Plan_ID as planId , v.customer_vault_id as customerHash, v.cc_number, v.cc_exp, v.cc_bin, v.check_account, v.check_aba, v.check_name, v.account_holder_type, v.account_type, v.currency, v.first_name, v.last_name, v.address_1, v.address_2, v.company, v.city, v.state, v.postal_code, v.country, v.email, v.phone, v.fax, v.website, v.shipping_first_name, v.shipping_last_name, v.shipping_address_1, v.shipping_address_2, v.shipping_company, v.shipping_city, v.shipping_state, v.shipping_postal_code, v.shipping_country, v.shipping_email, v.merchant_defined_field_1, v.merchant_defined_field_2, v.merchant_defined_field_3, v.merchant_defined_field_4, v.merchant_defined_field_5, v.merchant_defined_field_6, v.merchant_defined_field_7, v.merchant_defined_field_8, v.merchant_defined_field_9, v.merchant_defined_field_10, v.merchant_defined_field_11, v.merchant_defined_field_12, v.merchant_defined_field_13, v.merchant_defined_field_14, v.merchant_defined_field_15, v.merchant_defined_field_16, v.merchant_defined_field_17, v.merchant_defined_field_18, v.merchant_defined_field_19, v.merchant_defined_field_20 FROM Payment_Plans pp JOIN Vaults v ON v.Merchant_ID = pp.Boarding_ID JOIN Subscriptions sub on sub.Customer_Token = v.customer_vault_id JOIN Recurring_Plans rp on rp.Plan_ID = sub.Plan_ID WHERE sub.Is_Active = 1 AND pp.Processor_ID = :processorId';
                qarg.processorId = credentialPro;
                
                if (req.param('customerHash') !== undefined && req.param('customerHash') !== null && req.param('customerHash') !== ''){
                    query += ' AND v.customer_vault_id = :customerHash';
                    qarg.customerHash = req.param('customerHash');
                }
                if (req.param('planId') !== undefined && req.param('planId') !== null && req.param('planId') !== ''){
                    query += ' AND rp.Plan_ID = :planId';
                    qarg.planId = req.param('planId');
                }
                if (req.param('planName') !== undefined && req.param('planName') !== null && req.param('planName') !== ''){
                    query += ' AND rp.Plan_Name LIKE :planName';
                    qarg.planName = '%' + req.param('planName') + '%';
                }
                if (req.param('lastName') !== undefined && req.param('lastName') !== null && req.param('lastName') !== ''){
                    query += ' AND v.last_name LIKE :lastName';
                    arg.lastName = '%' + req.param('lastName') + '%';
                }
                if (req.param('firstName') !== undefined && req.param('firstName') !== null && req.param('firstName') !== ''){
                    query += ' AND v.first_name LIKE :firstName';
                    qarg.firstName = '%' + req.param('firstName') + '%';
                }
                if (req.param('email') !== undefined && req.param('email') !== null && req.param('email') !== ''){
                    query += ' AND v.email LIKE :email';
                    qarg.email = '%' + req.param('email') + '%';
                }
                if (req.param('last4cc') !== undefined && req.param('last4cc') !== null && req.param('last4cc') !== ''){
                    query += ' AND v.cc_number LIKE :last4cc';
                    qarg.last4cc = '%' + req.param('last4cc');
                }
                query += ';';
                db.sequelize.query(query,null,{ raw: true },
                    qarg
                ).success(function(subs) {
                    // pretty up the billingCycle response
                    for (var sub_item = 0; sub_item < subs.length; sub_item++) {
                        var billing_cycle = billing_desc(subs[sub_item].billingCycle, subs[sub_item].Day_Frequency, subs[sub_item].Month_Frequency, subs[sub_item].Day_Of_Month);
                        subs[sub_item].billingCycle = billing_cycle;
                        delete subs[sub_item].Day_Frequency;
                        delete subs[sub_item].Month_Frequency;
                        delete subs[sub_item].Day_Of_Month;
                    } // for (var sub_item = 0; sub_item < subs.length; sub_item++) 

                    deferred.resolve(subs);
                }).error(function(err){
                    console.log(err);
                });                
                break;
            case 'list_plan':
                isAPI = false;
                var pquery = '',
                    parg={};
                pquery = 'SELECT rp.Plan_ID as planId, rp.Plan_Name as planName, rp.Plan_Amount as amount, rp.Plan_Payments as billingCycle, rp.Day_Frequency, rp.Month_Frequency, rp.Day_Of_Month FROM Payment_Plans pp JOIN Recurring_Plans rp on rp.Processor_ID = pp.Processor_ID WHERE rp.Is_Active = 1 AND pp.Processor_ID = :processorId';
                parg.processorId = credentialPro;
                
                if (req.param('planId') !== undefined && req.param('planId') !== null && req.param('planId') !== ''){
                    pquery += ' AND rp.Plan_ID = :planId';
                    parg.planId = req.param('planId');
                }
                if (req.param('planName') !== undefined && req.param('planName') !== null && req.param('planName') !== ''){
                    pquery += ' AND rp.Plan_Name LIKE :planName';
                    parg.planName = '%' + req.param('planName') + '%';
                }

                pquery += ';';
//console.log(pquery);
                db.sequelize.query(pquery,null,{ raw: true },
                    parg
                ).success(function(plans) {
                    // pretty up the billingCycle response
                    for (var plan_item = 0; plan_item < plans.length; plan_item++) {
                        var billing_cycle = billing_desc(plans[plan_item].billingCycle, plans[plan_item].Day_Frequency, plans[plan_item].Month_Frequency, plans[plan_item].Day_Of_Month);
                        plans[plan_item].billingCycle = billing_cycle;
                        delete plans[plan_item].Day_Frequency;
                        delete plans[plan_item].Month_Frequency;
                        delete plans[plan_item].Day_Of_Month;
                    } // for (var plan_item = 0; plan_item < plan.length; plan_item++) 

                    deferred.resolve(plans);
                }).error(function(err){
                    console.log(err);
                });                
                break;
            case 'list_customer':
                isAPI = false;
                var cquery = '',
                carg={};
                cquery = 'SELECT v.customer_vault_id as customerHash, v.first_name, v.last_name, v.address_1, v.address_2, v.company, v.city, v.state, v.postal_code, v.country, v.email, v.phone, v.fax, v.website, v.shipping_first_name, v.shipping_last_name, v.shipping_address_1, v.shipping_address_2, v.shipping_company, v.shipping_city, v.shipping_state, v.shipping_postal_code, v.shipping_country, v.shipping_email, v.cc_number, v.cc_exp, v.cc_bin, v.check_account, v.check_aba, v.check_name, v.account_holder_type, v.account_type, v.currency, v.merchant_defined_field_1, v.merchant_defined_field_2, v.merchant_defined_field_3, v.merchant_defined_field_4, v.merchant_defined_field_5, v.merchant_defined_field_6, v.merchant_defined_field_7, v.merchant_defined_field_8, v.merchant_defined_field_9, v.merchant_defined_field_10, v.merchant_defined_field_11, v.merchant_defined_field_12, v.merchant_defined_field_13, v.merchant_defined_field_14, v.merchant_defined_field_15, v.merchant_defined_field_16, v.merchant_defined_field_17, v.merchant_defined_field_18, v.merchant_defined_field_19, v.merchant_defined_field_20, v.currency, v.amount, v.shipping, v.tax, v.tax_exempt, v.order_description, v.order_id, v.po_number FROM Payment_Plans pp JOIN Vaults v ON v.Merchant_ID = pp.Boarding_ID  WHERE v.Is_Active = 1 AND pp.Processor_ID = :processorId';
                carg.processorId = credentialPro;
                
                if (req.param('customerHash') !== undefined && req.param('customerHash') !== null && req.param('customerHash') !== ''){
                    cquery += ' AND v.customer_vault_id = :customerHash';
                    carg.customerHash = req.param('customerHash');
                }
                if (req.param('lastName') !== undefined && req.param('lastName') !== null && req.param('lastName') !== ''){
                    cquery += ' AND v.last_name LIKE :lastName';
                    carg.lastName = '%' + req.param('lastName') + '%';
                }
                if (req.param('firstName') !== undefined && req.param('firstName') !== null && req.param('firstName') !== ''){
                    cquery += ' AND v.first_name LIKE :firstName';
                    carg.firstName = '%' + req.param('firstName') + '%';
                }
                if (req.param('email') !== undefined && req.param('email') !== null && req.param('email') !== ''){
                    cquery += ' AND v.email LIKE :email';
                    carg.email = '%' + req.param('email') + '%';
                }
                if (req.param('last4cc') !== undefined && req.param('last4cc') !== null && req.param('last4cc') !== ''){
                    cquery += ' AND v.cc_number LIKE :last4cc';
                    carg.last4cc = '%' + req.param('last4cc');
                }
                
                cquery += ';';
                db.sequelize.query(cquery,null,{ raw: true },
                    carg
                ).success(function(cust) {
                    deferred.resolve(cust);
                }).error(function(err){
                    console.log(err);
                });                
                break;
            default:
                isAPI = false;
                deferred.resolve('Invalid API Data');
                break;
        }
        for (var attribute in req.query){
            if (attribute.indexOf('item') !== -1){
                arg[attribute] = req.query[attribute];
            }
        }
        /**
         * Call resources API
         */
        if (isAPI){
            arg.username = credentialsU;
            arg.password = credentialsP;
            var items = Object.keys(arg);
            queryString ='?';
            items.forEach(function(item) {
                if ((arg[item]!=='')&&(arg[item]!==undefined)){
console.log(arg[item])
                    queryString+='&' + item + '=' + encodeURIComponent(arg[item]);
                }
            });
            url+=queryString;
console.log('url', url)
            request({url:url,async:true}, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    if ( parseInt(queryStringToJSON(body).response_code) >= 100 &&  parseInt(queryStringToJSON(body).response_code) < 300) {

                       insertIntoTransactionHistory(credentialsU, credentialsP, queryStringToJSON(body).transactionid, boardingGlobal);
                       if (queryStringToJSON(body).response_code === '100') {
                           sendEmail(req, res, send_mojopay_receipt, queryStringToJSON(body).transactionid);
    
                            if (xmlfile){
                                var json = XMLMapping.load(body);
                                theObject = json;
                                deferred.resolve(true);
                                //res.jsonp(json);
                            }else{
                                if ( transactionType === 'create'){
                                    theObject = queryStringToJSON(body);
                                    theObject.customer_hash = theObject.customer_vault_id;
                                    delete theObject.customer_vault_id;
                                    vaultMaintenance(req, 'create', theObject.customer_hash, credentialsU, credentialsP);
                                    deferred.resolve(theObject);
                                }else if ( transactionType === 'update'){
                                    theObject = queryStringToJSON(body);
                                    theObject.customer_hash = theObject.customer_vault_id;
                                    delete theObject.customer_vault_id;
                                    vaultMaintenance(req, 'update', theObject.customer_hash, credentialsU, credentialsP);
                                    deferred.resolve(theObject);
                                }else{
                                    theObject = queryStringToJSON(body);
                                    if (req.param('customerVault') === 'add_customer'){
                                        theObject.customer_hash = theObject.customer_vault_id;
                                        delete theObject.customer_vault_id;
                                        vaultMaintenance(req, 'create', theObject.customer_hash, credentialsU, credentialsP);
                                        deferred.resolve(theObject);
                                    }else{
                                        if (theObject.customer_vault_id){
                                            delete theObject.customer_vault_id;
                                            theObject.customer_hash = req.param('customerHash');
                                            vaultMaintenance(req, 'update', theObject.customer_hash, credentialsU, credentialsP);
                                        }
                                        deferred.resolve(theObject);
                                    }
                                }
                            }
                        } else {
                            deferred.resolve(queryStringToJSON(body));
                        }
                    } else {
                        deferred.resolve(queryStringToJSON(body));
                    }
                }else{
                    console.log(error);
                }
            });
        }
        return deferred.promise;
    };



/**
 * Decides if customer exist or not and runs transaction acordingly
 * @param req
 * @param res
 */
exports.transactions = function(req, res) {
    getCredentials(req,res).then(
        function(results){
//console.log('results',results)
            var transactionType;
            transactionType = req.param('type');
            if ((transactionType === undefined)&&(req.param('customerVault') === 'add_customer')){
                transactionType = 'create';
            }
            if ((transactionType === undefined)&&(req.param('customerVault') === 'update_customer')){
                transactionType = 'update';
            }
//console.log(credentialsU)
//console.log('transactionType',transactionType)
            if (credentialsU!=='' && transactionType !== undefined){
//console.log('credentialsU',credentialsU)
                var customerhashdb = req.param('customerHash'),
                    type = req.param('type');
                if ((transactionType === 'sale')||(transactionType === 'validate')||(transactionType === 'auth')||(transactionType === 'credit')){
                    if (customerhashdb!==undefined){
                        runTheAPI(req,res,type).
                            then(
                            function(results){
                                res.jsonp({Response:results});
                            });
                    }else{
                        runTheAPI(req,res,type).
                            then(
                            function(results){
                                res.jsonp({Response:results});
                                if (req.param('customerVault') === 'add_customer'){
                                    runTheAPI(req,res,'create_customer_card_hash').then(
                                        function(results){
                                        });
                                }
                            });
                    }
                }else if (transactionType === 'update'){
                    if (customerhashdb!==undefined){
                        runTheAPI(req,res,'update').
                            then(
                            function(results){
                                res.jsonp({Response:results});
                            });
                    }else{
                        res.jsonp({Response:'Customer Hash missing'});
                    }
                }else if (transactionType === 'delete'){
                    if (customerhashdb!==undefined){
                        runTheAPI(req,res,'delete').
                            then(
                            function(results){
                                res.jsonp({Response:results});
                            });
                    }else{
                        res.jsonp({Response:'Customer Hash missing'});
                    }
                }else if (transactionType === 'create'){
                    runTheAPI(req,res,'').then(
                        function(results){
                            if (results.response_code === '100') {
                                res.jsonp({Response:results});
                                runTheAPI(req,res,'creport').then(
                                    function(results){
                                        runTheAPI(req,res,'create_customer_card_hash').then(
                                            function(results){
                                            });
                                    });
                            } else {
                                res.jsonp({Response:results});
                            }
                        });
                } else if (transactionType === 'customsubscribe') {
//console.log('transactionType', transactionType)
                    // Custom Subscription
                    if ( 
                        req.param('firstNameCard') !== undefined &&
                        req.param('lastNameCard') !== undefined &&
                        req.param('zipPostalCodeCard') !== undefined &&
                        req.param('creditCardNumber') !== undefined &&
                        req.param('expirationDate') !== undefined &&
                        req.param('cardSecurityCode') !== undefined &&
                        req.param('currency') !== undefined &&
                        req.param('planPayments') !== undefined &&
                        ( req.param('dayFrequency') !== undefined && (req.param('monthFrequency') === undefined && req.param('dayOfMonth') === undefined) ||
                          req.param('dayFrequency') === undefined && (req.param('monthFrequency') !== undefined && req.param('dayOfMonth') !== undefined) ) &&
                          req.param('amount') !== undefined
                    ) {
                        runTheAPI(req,res,'create').then(
                            function(createResults) {
//console.log('createResults', createResults)
//console.log('req.body.customerHash',req.body.customerHash)
//console.log('theObject.customer_hash',theObject.customer_hash)
                                if (createResults.response_code === '100') {
                                    req.body.customerHash = createResults.customer_hash;
                                    runTheAPI(req,res,'create_customer_card_hash').then(
                                        function(hashResults) {
                                            if (hashResults) {
                                                var plan_name=req.param('planName');
                                                if (plan_name === undefined || plan_name === null || plan_name === '') {
                                                    req.body.planName = 'api' + req.param('lastNameCard') + new Date().getFullYear() + ('0' + (new Date().getMonth() + 1)).slice(-2) + ('0' + new Date().getDate()).slice(-2) + ('0' + new Date().getHours()).slice(-2) + ('0' + new Date().getMinutes()).slice(-2) + ('0' + new Date().getSeconds()).slice(-2) + Math.random().toString(36).substr(2);
                                                }
                                                req.body.planId = theObject.customer_hash;
                                                runTheAPI(req,res,'add_plan').then(
                                                    function(planResults) {
                                                        if (planResults) {
//console.log('----------------------planResults',planResults,planResults.indexOf(' added successfully'))
                                                            if (planResults.indexOf(' added successfully') >= 0) {
                                                                // add subscription
                                                                var start_date = req.param('startDate'),
                                                                    next_charge_date = req.param('startDate');
                                                                    
                                                                var today = new Date().getFullYear() + ('0' + (new Date().getMonth() + 1)).slice(-2) + ('0' + new Date().getDate()).slice(-2);
                                                                if ((start_date === '' || start_date === null || start_date === undefined)) {
                                                                    start_date = today;
                                                                    next_charge_date =  new Date().getFullYear() + ('0' + (new Date().getMonth() + 2) % 12).slice(-2) + ('0' + new Date().getDate()).slice(-2);
                                                                } else {
                                                                    start_date = req.param('startDate');
                                                                    next_charge_date = req.param('startDate');
                                                                } // if ((start_date === '' || start_date === null || start_date === undefined)) 
                                                                var subscriptionObject = {
                                                                    Customer_Token: theObject.customer_hash,
                                                                    Plan_ID: theObject.customer_hash,
                                                                    Start_Date: start_date,
                                                                    Next_Charge_Date: next_charge_date
                                                                };
                                                                db.Subscription.create(subscriptionObject).success(function(subscription,err){
                                                                    if (!subscription) {
                                                                        res.jsonp('error', {errors: err});
                                                                    } else {
                                                                        if  (start_date <= today) {
                                                                            req.body.type = 'sale';
                                                                            runTheAPI(req,res,'sale').then (
                                                                                function(saleResults) {
                                                                                    if (saleResults.response_code === '100') {
                                                                                        subscription.updateAttributes({
                                                                                            Attempted_Payments: 1,
                                                                                            Completed_Payments: 1
                                                                                        }).success(function(a){
                                                                                        }).error(function(err){
                                                                                        });
                                                                                    } else {
                                                                                        subscription.updateAttributes({
                                                                                            Attempted_Payments: 1,
                                                                                            Count_Failed: 1,
                                                                                            Next_Charge_Date: start_date
                                                                                        }).success(function(a){
                                                                                        }).error(function(err){
                                                                                        });
                                                                                    }
                                                                                    res.jsonp({Response:saleResults});
                                                                                } // function(saleResults)
                                                                            ); // runTheApi(req,res,'sale').then
                                                                        } else {
                                                                            res.jsonp({Response:createResults});
                                                                        } // if  (start_date <= today)
                                                                    } // if (!subscription) 
                                                                }).error(function(err){
                                                                    res.jsonp('error', {
                                                                        error: err,
                                                                        status: 500
                                                                    });
                                                                });
                                                            } else {
                                                                res.jsonp({Response:planResults});
                                                            }
                                                        } // if(planResults)
                                                    } // function(planResults) 
                                                ); // runTheAPI(req,res,'add_plan').then
                                            } // if (hashResults)
                                        } // function(hashResults) 
                                    ); // runTheAPI(req,res,'create_customer_card_hash').then
                                } else {
//console.log('not 100 createResults', createResults)
                                    res.jsonp({Response:createResults});
                                } // if (createResults.response_code === '100') 
                            } // function(createResults)
                        ); // runTheAPI(req,res,'create').then

                    } else {
                        res.jsonp({Response:'Api Missing Data'});
                    } // if (req.param('firstNameCard') !== undefined && . . .
                } else if (transactionType === 'add_plan') {
                    if (
                        req.param('planId') !== undefined &&
                        req.param('planPayments') !== undefined &&
                        ( req.param('dayFrequency') !== undefined && (req.param('monthFrequency') === undefined && req.param('dayOfMonth') === undefined) ||
                          req.param('dayFrequency') === undefined && (req.param('monthFrequency') !== undefined && req.param('dayOfMonth') !== undefined) ) &&
                        req.param('amount') !== undefined
                    ) {
                        var plan_name = req.param('planName');
                        if (plan_name === undefined || plan_name === null || plan_name === '') {
                            req.body.planName = 'api' + req.param('lastNameCard') + new Date().getFullYear() + ('0' + (new Date().getMonth() + 1)).slice(-2) + ('0' + new Date().getDate()).slice(-2) + ('0' + new Date().getHours()).slice(-2) + ('0' + new Date().getMinutes()).slice(-2) + ('0' + new Date().getSeconds()).slice(-2) + Math.random().toString(36).substr(2);
                        } // if (plan_name === undefined || plan_name === null || plan_name === '')
                        runTheAPI(req,res,'add_plan').then(
                            function(planResults) {
                                res.jsonp({Response:planResults});
                            } // function(planResults)
                        ); // runTheAPI(req,res,'add_plan').then
                    } else {
                            res.jsonp({Response:'Api Missing Data'});
                    } // if ( ... )
                } else if (transactionType === 'delete_plan') {
                    if (req.param('planId') !== undefined) {
                        runTheAPI(req,res,'delete_plan').then(
                            function(planResults) {
                                res.jsonp({Response:planResults});
                            } // function(planResults)
                        ); // runTheAPI(req,res,'delete_plan').then
                    } else {
                            res.jsonp({Response:'Api Missing Data'});
                    } // if ( ... )
                } else if (transactionType === 'update_plan') {
                    if (
                        req.param('planId') !== undefined &&
                        req.param('planPayments') !== undefined &&
                        ( req.param('dayFrequency') !== undefined && (req.param('monthFrequency') === undefined && req.param('dayOfMonth') === undefined) ||
                          req.param('dayFrequency') === undefined && (req.param('monthFrequency') !== undefined && req.param('dayOfMonth') !== undefined) ) &&
                        req.param('amount') !== undefined
                    ) {
                         runTheAPI(req,res,'update_plan').then(
                            function(planResults) {
                                res.jsonp({Response:planResults});
                            } // function(planResults)
                        ); // runTheAPI(req,res,'update_plan').then
                    } else {
                        res.jsonp({Response:'Api Missing Data'});
                    } // if ( ... )
                } else if (transactionType === 'delete_sub') {
                    if (
                         (req.param('subscriptionId') !== undefined) ||
                         (req.param('planId') !== undefined && req.param('customerHash') !== undefined)
                    ) {
                        // RUN THE API
                        runTheAPI(req,res,'delete_sub').then(
                            function(subResults) {
                                res.jsonp({Response:subResults});
                            } // function(subResults)
                        ); // runTheAPI(req,res,'delete_sub').then
                    } else {
                        res.jsonp({Response:'Api Missing Data'});
                    } // if ( ... )
                } else if (transactionType === 'add_sub') {
                    if (req.param('planId') !== undefined && req.param('customerHash') !== undefined && req.param('startDate')) {
                        var start_date = req.param('startDate');
                        var yy = start_date.substr(0,4),
                            mm = start_date.substr(4,2),
                            dd = start_date.substr(6,2);
                        var testDate = new Date(parseInt(yy), parseInt(mm) - 1, parseInt(dd));
                        var test_yy = testDate.getFullYear().toString(),
                            test_mm = ('0' + (testDate.getMonth() + 1).toString()).slice(-2),
                            test_dd = ('0' + testDate.getDate().toString()).slice(-2);
                        if (test_yy === yy && test_mm === mm && test_dd === dd) {
                            // RUN THE API
                            runTheAPI(req,res,'add_sub').then(
                                function(subResults) {
                                    res.jsonp({Response:subResults});
                                } // function(subResults)
                            ); // runTheAPI(req,res,'delete_sub').then
                        } else {
                            res.jsonp({Response:'Invalid Start Date'});
                        } // if ( ... )
                    } else {
                        res.jsonp({Response:'API Missing Data'});
                    } // if ( ... )



                } else if (transactionType === 'add_cust_sub') {
                    // Custom Subscription
                    if ( 
                        req.param('firstNameCard') !== undefined &&
                        req.param('lastNameCard') !== undefined &&
                        req.param('zipPostalCodeCard') !== undefined &&
                        req.param('creditCardNumber') !== undefined &&
                        req.param('expirationDate') !== undefined &&
                        req.param('cardSecurityCode') !== undefined &&
                        req.param('currency') !== undefined &&
                        req.param('planId') !== undefined
                    ) {
                        db.Recurring_Plan.find({where: {Plan_ID: req.param('planId'), Is_Active: 1}}).success(function(plan,err) {
                            if (!plan) {
                                res.jsonp({Response: 'Recurring Plan does not exist'});
                            } else {
                                runTheAPI(req,res,'create').then(
                                    function(createResults) {
                                        if (createResults.response_code === '100') {
                                            theObject = createResults;
                                            //theObject.customer_hash = createResults.customer_vault_id;
                                            req.body.customerHash = theObject.customer_hash;
                                            if (theObject.customer_vault_id) {
                                                delete theObject.customer_vault_id;
                                            }
                                            runTheAPI(req,res,'create_customer_card_hash').then(
                                                function(hashResults) {
                                                    if (hashResults) {
                                                        // add subscription
                                                        var start_date = req.param('startDate'),
                                                            next_charge_date = req.param('startDate');

                                                        var today = new Date().getFullYear() + ('0' + (new Date().getMonth() + 1)).slice(-2) + ('0' + new Date().getDate()).slice(-2);
                                                        if ((start_date === '' || start_date === null || start_date === undefined)) {
                                                            start_date = today;
                                                            next_charge_date =  new Date().getFullYear() + ('0' + (new Date().getMonth() + 2) % 12).slice(-2) + ('0' + new Date().getDate()).slice(-2);
                                                        } else {
                                                            start_date = req.param('startDate');
                                                            next_charge_date = req.param('startDate');
                                                        }
                                                        var subscriptionObject = {
                                                            Customer_Token: theObject.customer_hash,
                                                            Plan_ID: req.param('planId'),
                                                            Start_Date: start_date,
                                                            Next_Charge_Date: next_charge_date
                                                        };
                                                        db.Subscription.create(subscriptionObject).success(function(subscription,err){
                                                            if(!subscription){
                                                                res.jsonp('error', {errors: err});
                                                            } else {
                                                                if  (start_date <= today) {
                                                                    // set the amount from the plan4
                                                                    req.body.amount = plan.Plan_Amount;                                                                    
                                                                    req.body.type = 'sale';
                                                                    runTheAPI(req,res,'sale').then (
                                                                        function(saleResults) {
                                                                            if (saleResults.response_code === '100') {
                                                                                subscription.updateAttributes({
                                                                                    Attempted_Payments: 1,
                                                                                    Completed_Payments: 1
                                                                                }).success(function(a){
                                                                                }).error(function(err){
                                                                                });
                                                                            } else {
                                                                                subscription.updateAttributes({
                                                                                    Attempted_Payments: 1,
                                                                                    Count_Failed: 1,
                                                                                    Next_Charge_Date: start_date
                                                                                }).success(function(a){
                                                                                }).error(function(err){
                                                                                });
                                                                            }
                                                                            res.jsonp({Response:saleResults});
                                                                        } // function(saleResults)
                                                                    ); // runTheApi(req,res,'sale').then
                                                                }else {
                                                                    res.jsonp({Response:createResults});
                                                                } // if ((start_date === '' || start_date === null || start_date === undefined) || (start_date === today))
                                                            }
                                                        }).error(function(err){
                                                            res.jsonp('error', {
                                                                error: err,
                                                                status: 500
                                                            });
                                                        });
                                                    } // if (hashResults)
                                                } // function(hashResults) 
                                            ); // runTheAPI(req,res,'create_customer_card_hash').then
                                        } else {
                                            res.jsonp({Response: createResults});
                                        } // if (createResults.response_code === '100') 
                                    } // function(createResults)
                                ); // runTheAPI(req,res,'create').then
                            } //if (!plan)
                        }).error(function(err){
                            console.log(err);
                            res.jsonp({Response: 'Recurring Plan does not exist'});
                        });
                    } else {
                        res.jsonp({Response:'Api Missing Data'});
                    }




                } else {
                    runTheAPI(req,res,type).then(
                        function(results){
                            res.jsonp({Response:results});
                        }
                    );
                }
            } else {
                res.jsonp({Response:'Api Missing Data'});
            }
            
        },
        function(err){
        });
};

