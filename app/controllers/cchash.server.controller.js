'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize');

/**
 * Find boarding by id
 * Note: This is called every time that the parameter :boardingId is used in a URL. 
 * Its purpose is to preload the boarding on the req object then call the next function. 
 */
exports.cchash = function(req, res, next, id) {
    db.Cchash.find({ where: {id: id}}).success(function(cchash){
        if(!cchash) {
            return next(new Error('Failed to load cchash ' + id));
        } else {
            req.cchash = cchash;
            return next();            
        }
    }).error(function(err){
        return next(err);
    });
};

/**
 * Create a boarding
 */
exports.create = function(req, res) {
    // augment the boarding by adding the UserId
    req.body.UserId = req.user.id;
    // save and return and instance of boarding on the res object. 
    db.Cchash.create(req.body).success(function(cchash,err){
        if(!cchash){
            return res.send('users/signup', {errors: err});
        } else {
            return res.jsonp(cchash);
        }
    }).error(function(err){
        return res.send('users/signup', { 
            errors: err,
            status: 500
        });
    });
};

/**
 * Update a boarding
 */
exports.update = function(req, res) {

    // create a new variable to hold the boarding that was placed on the req object.

    var cchash = req.cchash;
    cchash.updateAttributes({
        Date: req.body.Date,
        Merchant_Name: req.body.Merchant_Name,
        Address: req.body.Address,
        City: req.body.City,
        Phone: req.body.Phone,
        Merchant_ID: req.body.Merchant_ID,
        Bank_ID: req.body.Bank_ID,
        Terminal_ID: req.body.Terminal_ID,
        MCC: req.body.MCC,
        Classification: req.body.Classification,
        Visa: req.body.Visa,
        Mastercard: req.body.Mastercard,
        Discover: req.body.Discover,
        American_Express: req.body.American_Express,
        Diners_Club: req.body.Diners_Club,
        JCB: req.body.JCB,
        Maestro: req.body.Maestro,
        Max_Ticket_Amount: req.body.Max_Ticket_Amount,
        Max_Monthly_Amount: req.body.Max_Monthly_Amount,
        Precheck: req.body.Precheck,
        Duplicate_Checking: req.body.Duplicate_Checking,
        Allow_Merchant: req.body.Allow_Merchant,
        Duplicate_Threshold: req.body.Duplicate_Threshold,
        Account_Description: req.body.Account_Description,
        processor: req.body.Processor_ID,
        Location_Number: req.body.Location_Number,
        Aquire_Bin: req.body.Aquire_Bin,
        Store_Number: req.body.Store_Number,
        Vital_Number: req.body.Vital_Number,
        Agent_Chain: req.body.Agent_Chain,
        Required_Name: req.body.Required_Name,
        Required_Company: req.body.Required_Company,
        Required_Address: req.body.Required_Address,
        Required_City: req.body.Required_City,
        Required_State: req.body.Required_State,
        Required_Zip: req.body.Required_Zip,
        Required_Country: req.body.Required_Country,
        Required_Phone: req.body.Required_Phone,
        Required_Email: req.body.Required_Email,
        Reserve_Rate: req.body.Reserve_Rate,
        Reserve_Condition: req.body.Reserve_Condition,
        Cap_Amount: req.body.Cap_Amount,
        Disbursement: req.body.Disbursement,
        Status: req.body.Status,
        Welcome_Email: req.body.Welcome_Email,
        State: req.body.State,
        Zip: req.body.Zip
    }).success(function(a){
        return res.jsonp(a);
    }).error(function(err){
        return res.render('error', {
            error: err, 
            status: 500
        });
    });
};

/**
 * Delete an boarding
 */
exports.destroy = function(req, res) {

    // create a new variable to hold the boarding that was placed on the req object.
    var cchash = req.cchash;

    cchash.destroy().success(function(){
        return res.jsonp(cchash);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

/**
 * Show an boarding
 */
exports.show = function(req, res) {
    // Sending down the boarding that was just preloaded by the boardings.boarding function
    // and saves boarding on the req object.
    return res.jsonp(req.cchash);
};

/**
 * List of boardings
 */
exports.all = function(req, res) {
    db.Cchash.findAll().success(function(cchashs){
        return res.jsonp(cchashs);
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};
