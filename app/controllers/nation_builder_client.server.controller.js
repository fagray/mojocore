'use strict';
/**
 * Module dependencies.
 */
var db = require('../../config/sequelize'),
	config = require('./../../config/config'),
    Q =  require('q'),
    request = require('request'),
    http = require('http'),
    https = require('https'),
    
    toObject = function (arr) {
      var rv = {};
      for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
      return rv;
    };
/**
 * Find nationBuilderClient by id
 * Note: This is called every time that the parameter :nationBuilderClientId is used in a URL. 
 * Its purpose is to preload the nationBuilderClient on the req object then call the next function. 
 */
exports.nationBuilderClient = function(req, res, next, id) {
    var processorId = req.params.ProcessorID;
//console.log("processorId: " + processorId);

        db.Nation_Builder_Client.find({ where: {Processor_ID: processorId}}).success(function(nationBuilderClient){
            if(!nationBuilderClient) {
//console.log(nationBuilderClient);
                req.nationBuilderClient = {};
                return next();
            } else {
                req.nationBuilderClient = {response:nationBuilderClient};
                return next();            
            }
         }).error(function(err){
            return res.render('error', {
            error: err,
            status: 500
        });
    });
};

exports.create = function(req, res) {
    var arg = {
        Processor_ID: req.param('processorId'),
        URL: req.param('nationBuilderURL')
    };
};

exports.show = function(req, res) {
    // Sending down the nationBuilderClient that was just preloaded by the nationBuilderClient.nationBuilderClient function
    // and saves nationBuilderClient on the req object.
//console.log(req.nationbnationBuilderClientuilderkey);
//console.log('show');
    return res.jsonp(req.nationBuilderClient);
};

exports.update = function(req, res) {
    var arg = {
        Processor_ID: req.param('processorId'),
        Nation_Builder_Key: req.param('nationBuilderClient'),
        URL: req.param('nationBuilderURL')
    };

};

exports.activation = function(req, res) {
    var Nation_ID = req.param('nationBuilderKeyId');
    var Processor_ID = req.param('ProcessorId');
    var Nation_Builder_Key = req.param('Nation_Builder_Key');
    var Type = req.param('Type');
    var URL = req.param('URL');
//console.log(Nation_ID, Processor_ID,Nation_Builder_Key,Type,URL);
    db.Nation_Builder_Client.find({ where: {Processor_ID: Processor_ID, id: Nation_ID}}).success(function(nationBuilderClient){
        if(nationBuilderClient) {
            if(Type==="activate") {
//console.log('activating', Nation_ID, Processor_ID, URL, Type);
                nationBuilderClient.updateAttributes({
                    URL: URL,
                    Active: 1
                }).success(function(a){
                    return res.jsonp(a);
                }).error(function(err){
                    return res.render('error', {
                        error: err,
                        status: 500
                    });
                });
            }else if (Type==="inactivate") {
//console.log('inactivating', Nation_ID, Processor_ID, URL, Type);
                nationBuilderClient.updateAttributes({
                    Active: 0
                }).success(function(a){
                    return res.jsonp(a);
                }).error(function(err){
                    return res.render('error', {
                        error: err,
                        status: 500
                    });
                });
            }else{
                return res.jsonp('false');
            }
        }else{
//console.log('creating', Nation_ID, Processor_ID, URL, Type);
             if(Type==="create") {
                
                db.Nation_Builder_Client.create({
                    Processor_ID: Processor_ID,
                    Nation_Builder_Key: Nation_Builder_Key,
                    URL: URL,
                    Active: 1
                }).success(function(a){
                    return res.jsonp(a);
                }).error(function(err){
                    return res.render('error', {
                        error: err,
                        status: 500
                    });
                });
            }
        }
    }).error(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};


exports.callback = function(req, res) {
//    console.log(req.param('code'));
//    console.log('OAuth Callback');
};

exports.gotcode = function(req, res) {
//console.log(req.param('code'));
//console.log(req.params);
//console.log('Code Callback');
    var url='/#!/settings/nation_builder_oauth_callback?code=' + req.param('code');
//    console.log(url);
    return res.redirect(url);
};

exports.getoauth = function(req, res) {     
//    console.log( req.body );
    var postURL = req.body.url;
    var path = req.body.path;
//console.log( postURL );
//console.log( path );
    delete req.body.url;
    delete req.body.path;
    var post_data = JSON.stringify(req.body);
//console.log( post_data);
//console.log('Get Oauth');
    var options = {
        hostname: postURL,
        path: path,
        port: 443,
        method:'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Content-Length': post_data.length
        },

    };
//console.log(options);

    var ResultObject= '';
    var request = https.request(options, function(response){
        var responseString = '';
        response.on('data', function(data1){
            responseString += data1;
        });

        response.on('end', function(data2){
            ResultObject=JSON.parse(responseString);
//console.log(ResultObject);
            res.send(ResultObject);
        });

        response.on('error', function(data2){
        });

    });
request.write(post_data);
request.end();

};