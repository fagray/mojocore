'use strict';

/**
 * Module dependencies.
 */
var 
	http 			= require('http'),
	https 			= require('https'),
	request 		= require('request'),
	qs 				= require('querystring'),
	_ 				= require('lodash'),
	fs 				= require('fs'),
	should 			= require('should'),
	assert 			= require('assert'),
	supertest 		= require('supertest'),
	config			= require('./../../config/env/development.js'),
	queryString 	= '?a=a';



/*
 * Globals var
 */ 
var transactionId = '',
	url1 = config.api_url.root_transact,
	url = config.api_url.root_query;

var queryStringToJSON = function(pairs) {		
		pairs = pairs.split('&');
		var result = {};
		pairs.forEach(function(pair) {
			pair = pair.split('=');
			result[pair[0]] = decodeURIComponent(pair[1] || '');
		});

		return JSON.parse(JSON.stringify(result));
	};


/**
 * Unit tests
 */
describe('Testing Api with Unit Test Mocha', function() {
	describe('API Calls Virtual Machine', function() {
		
		this.timeout(500000); // API response is very slow
		
		it('should be able to do a Sale', function(done) { 
			var called = false;
			var arg = {
				username: 'mojotest',
				password: 'testing123',
				type: 'sale',
				ccnumber: '6011601160116611',
				ccexp: '1025',
				cvv: '999',
				amount: '10.00',
				zipcode: '77777',
				customer_vault_id: '',
			};
			var items = Object.keys(arg);
			items.forEach(function(item) {
			  queryString+='&' + item + '=' + arg[item];
			});
			url+=queryString;
			return request(url, function (error, response, body) {
				console.log(error);
			    if (!error && response.statusCode === 200) {
			    	var data = queryStringToJSON(body);
			        transactionId = data.transactionid;
			        called = true;
			    }else{
			    	console.log(error);
			    }
				if (called.should.be.true) done();
			});
		});


		it('should be able to do a Capture with transaction Id generated before', function(done) {
			var called = false;
			var arg = {
				username: 'Demo321',
				password: 'demo1234',
				type: 'capture',
				customer_vault_id: '',
				transactionid: transactionId
			};
			var items = Object.keys(arg);
			items.forEach(function(item) {
			  queryString+='&' + item + '=' + arg[item];
			});
			url+=queryString;
			var request = require('request');
			return request(url, function (error, response, body) {
			    if (!error && response.statusCode === 200) {
			        called = true;
			    }
				if (called.should.be.true) done();
			});			 
		});


		it('should be able to do a Refund with transaction Id generated before', function(done) {
			var called = false;
			var arg = {
				username: 'mojotest',
				password: 'testing123',
				type: 'refund',
				customer_vault_id: '',
				transactionid: transactionId
			};
			var items = Object.keys(arg);
			items.forEach(function(item) {
			  queryString+='&' + item + '=' + arg[item];
			});
			url+=queryString;
			var request = require('request');
			return request(url, function (error, response, body) {
			    if (!error && response.statusCode === 200) {
			        called = true;
			    }
				if (called.should.be.true) done();
			});
		});

		it('should be able to do Generate a Report', function(done) {
			var called = false;
			var arg = {
				username: 'mojotest',
				password: 'testing123',
				type: 'treport'
			};
			var items = Object.keys(arg);
			items.forEach(function(item) {
			  queryString+='&' + item + '=' + arg[item];
			});
			url1+=queryString;
			var request = require('request');
			return request(url1, function (error, response, body) {
			    if (!error && response.statusCode === 200) {
			        called = true;
			    }
			    /*
			    reference:
			    https://jslinterrors.com/expected-an-assignment-or-function-call
			    
				called.should.be.true;
				done();
				*/
				if (called.should.be.true) done();				
			});			 
		});

	});
});




/**
* REVERVE TEST

describe('Testing Reserve Fees with Unit Test Mocha', function() {
	describe('', function() {
		
		function calcReserve(lifetimeNetSales, trasactionAmount, rate, lifetimeCap, isLifetimeCap){


			// step 1 = Calc lifetime from netSales

			if (isLifetimeCap){
				var reserveNetSales = (lifetimeNetSales * (rate/100));
				// step 2 = Compare step1 (reserve) with Cap
				if (lifetimeCap > reserveNetSales) { // still have room before hitting the cap
					// step 3 = calc full reserve from amount
					var transactionReserve = (trasactionAmount * (rate/100));
					if (lifetimeCap - reserveNetSales > transactionReserve) { // there is enough room under the cap for the full transactionReserve
						return transactionReserve;
					} else { // only enough room under the cap for part of the transactionReserve
						return parseFloat(lifetimeCap - reserveNetSales);
					}
				}else{
					return 0;
				}
			}else{
				var reserveNetSales = (lastPeriodsNetSales * (rate/100));
				// step 2 = Compare step1 (reserve) with Cap
				if (lastPeriodsCap > reserveNetSales) { // still have room before hitting the cap
					// step 3 = calc full reserve from amount
					var transactionReserve = (trasactionAmount * (rate/100));
					if (lastPeriodsCap - reserveNetSales > transactionReserve) { // there is enough room under the cap for the full transactionReserve
						return transactionReserve;
					} else { // only enough room under the cap for part of the transactionReserve
						return parseFloat(lastPeriodsCap - reserveNetSales);
					}
				}else{
					return 0;
				}
				// Rolling 6
				// return (trasactionAmount * (rate/100));
			}
		}

		
		it('should be able to calculate reserve fees', function(done) { 
			var called = false;

			var netSales = 10000,
				reserveRate = 5,
				capAmount = 100,
				iscap = true;

			console.log('netSales: ' + netSales);
			console.log('reserveRate: ' + reserveRate);
			console.log('capAmount: ' + capAmount);
			console.log('');

			var amountCollection = [];
			for (var i = 0; i < 10; i++) {
				var amount = (Math.random()*1000).toFixed(2);
				amountCollection.push(amount);
			};

			console.log('random amounts:');
			console.log(amountCollection);

			console.log('');
			amountCollection.forEach(function (amount) {
				var reserveColumn = calcReserve(netSales, amount, reserveRate, capAmount, iscap);
				console.log('reserve fee: ' + reserveColumn);
			});



		    called = true;
			if (called.should.be.true) done();
		});




	});
});
*/


/*
describe('Testing reserve formula', function() {

  var scope;
  var ctrl;

  beforeEach(module('creditcard'));

  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    ctrl = $controller('SaleController', {$scope: scope});
  }));

  describe('reserve calc property', function() {
    describe('when calling the calcReserve function', function() {
    });
  });
});
*/