/*jslint node: true */
'use strict';

/**
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Account = sequelize.define('Account', {
			user_Id: DataTypes.INTEGER,
			boarding_Id: DataTypes.INTEGER,
			username: DataTypes.STRING,
			password: DataTypes.STRING
		},
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Account;
};
