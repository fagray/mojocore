/*jslint node: true */
'use strict';

/**
 *
 * Contain time fees, used by Admin Account in Merchant Activity Report.
 * These fess will be processed in all Reports
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Timefee = sequelize.define('Timefee', {
			boarding_id: DataTypes.INTEGER,
			processor: DataTypes.STRING,
			feeName: DataTypes.STRING,
			feeAmount: DataTypes.STRING,
			payment: DataTypes.STRING,
			transaction_id: DataTypes.STRING,
			date: DataTypes.STRING,
			description: DataTypes.STRING
		},
		{
			associate: function(models){
				//Timefee.belongsTo(models.User);
			}
		}
	);

	return Timefee;
};
