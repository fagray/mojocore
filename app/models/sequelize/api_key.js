/*jslint node: true */
'use strict';

/**
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Api_Key = sequelize.define('Api_Key', {
			Token: DataTypes.STRING,
			Is_Active:{type:DataTypes.BOOLEAN,defaultValue: 1},
			Boarding_ID: DataTypes.INTEGER
		},
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Api_Key;
};
