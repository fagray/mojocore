/*jslint node: true */
'use strict';

/**
 *
 * Contains Account Balances
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Nation_Builder_Client = sequelize.define('Nation_Builder_Client', {
			Processor_ID: DataTypes.STRING,
			Nation_Builder_Key: DataTypes.STRING,
			URL: DataTypes.STRING,
			Active: {type:DataTypes.BOOLEAN,defaultValue: 1}
		},
		{
		    freezeTableName: true,
		    tableName: 'Nation_Builder_Client',
		    associate: function(models){
		    
			}
	   }
	);

	return Nation_Builder_Client;
};
