/*jslint node: true */
'use strict';

/**
 *
 * Contain subscription information.
 * Used for Revurring billing
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Subscription = sequelize.define('Subscription', {
			Customer_Token: DataTypes.STRING,
			Plan_ID: DataTypes.INTEGER,
			Start_Date: DataTypes.STRING,
            Order_ID: DataTypes.STRING,
            Order_Description: DataTypes.STRING,
            PO_Number: DataTypes.STRING,
			Shipping: {type:DataTypes.DECIMAL(10,2),defaultValue: 0},
			Tax: {type:DataTypes.DECIMAL(10,2),defaultValue: 0},
			Tax_Exempt: {type:DataTypes.BOOLEAN,defaultValue: 0},
            Completed_Payments: {type:DataTypes.INTEGER,defaultValue: 0},
            Attempted_Payments: {type:DataTypes.INTEGER,defaultValue: 0},
            Count_Failed: {type:DataTypes.INTEGER,defaultValue: 0},
            Next_Charge_Date: DataTypes.STRING,
            Is_Active: {type:DataTypes.BOOLEAN,defaultValue: 1}
		},
		{
			associate: function(models){
				//Subscription.belongsTo(models.User);
			}
		}
	);

	return Subscription;
};
