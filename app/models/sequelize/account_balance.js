/*jslint node: true */
'use strict';

/**
 *
 * Contains Account Balances
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Account_Balance = sequelize.define('Account_Balance', {
			Processor_ID: DataTypes.STRING,
			Date: DataTypes.STRING,
			Gross_Amount: DataTypes.DECIMAL(10,2),
			Net_Amount: DataTypes.DECIMAL(10,2),
			Split_Amount: DataTypes.DECIMAL(10,2),
			Reserve_Amount: DataTypes.DECIMAL(10,2),
			Misc_Fee_Amount: DataTypes.DECIMAL(10,2),
			Disbursement: {type:DataTypes.INTEGER,defaultValue: 0},
			Disbursed_Fees_Amount: DataTypes.DECIMAL(10,2),
			Disbursed_Transactions_Amount: DataTypes.DECIMAL(10,2),
			Current_Reserve_Amount: DataTypes.DECIMAL(10,2),
			Current_Split_Amount: DataTypes.DECIMAL(10,2),
			Previous_Net_Plus_Gross: DataTypes.DECIMAL(10,2),
			Disbursed_Amount: DataTypes.DECIMAL(10,2)
		},
		{
		    freezeTableName: true,
		    tableName: 'Account_Balance',
		    associate: function(models){
		    //Boarding.belongsTo(models.User);
			}
	   }
	);

	return Account_Balance;
};
