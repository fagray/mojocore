/*jslint node: true */
'use strict';

/**
 * Contain hash related to credit card number.
 * Is used to retrieve the first six and last four credit card numbers.
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Customer_Card_Hash = sequelize.define('Customer_Card_Hash', {
			Boarding_ID: DataTypes.STRING,
			CC_Number: DataTypes.STRING,
            Brand: DataTypes.STRING,
			Hash: DataTypes.STRING
		},
		{
			associate: function(models){
				//Customer_Card_Hash.belongsTo(models.User);
			}
		}
	);

	return Customer_Card_Hash;
};
