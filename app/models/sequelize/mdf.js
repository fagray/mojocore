/*jslint node: true */
'use strict';

/**
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var MDF = sequelize.define('MDF', {
			History_ID: DataTypes.BIGINT(15),
			Value: DataTypes.STRING(255),
			Number: DataTypes.STRING(2),
			Product_Mapping_ID: DataTypes.BIGINT(20),
			Processor_Mapping_ID: DataTypes.BIGINT(20)
		},
		{
			freezeTableName: true,
			tableName: 'MDF',
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
		);

	return MDF;
};
