/*jslint node: true */
'use strict';

/**
 *
 * @type {exports}
 *
 * The crypto module offers a way of encapsulating secure credentials.
 * Salt, is a hight level cryptography to defend against dictionary attacks, reference: http://en.wikipedia.org/wiki/Dictionary_attack
 *
 * More about security: https://www.owasp.org
 *
 */

var crypto = require('crypto');
var iterations = 1000; //2500;
var keylen = 64; //512;


module.exports = function(sequelize, DataTypes) {
	/**
	 * A Validation function for local strategy properties
	 */
	var validateLocalStrategyProperty = function(property) {
		return ((this.Provider !== 'local' && !this.updated) || property.length);
	};

	/**
	 * A Validation function for local strategy password
	 */
	var validateLocalStrategyPassword = function(password) {
		return (this.Provider !== 'local' || (password && password.length > 6));
	};

	var User = sequelize.define('User', 
		{	
			Boarding_ID: DataTypes.INTEGER,
			First_Name: DataTypes.STRING,
			Last_Name: DataTypes.STRING,
			Display_Name: DataTypes.STRING,
			Email: DataTypes.STRING,
			Username: DataTypes.STRING,
			Password: DataTypes.STRING,
			Salt: DataTypes.STRING, 
			Roles:{type:DataTypes.ENUM('user','admin','super','marketplace','agent','affiliate'),defaultValue: 'user'},
			Virtual_Terminal:{type:DataTypes.BOOLEAN,defaultValue: 0},
			Customer_Vault:{type:DataTypes.BOOLEAN,defaultValue: 0},
			Items:{type:DataTypes.BOOLEAN,defaultValue: 0},
			Reset_Password_Token: DataTypes.STRING,
			reset_Password_Expires: DataTypes.DATE,
			Terminal_Sale: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Terminal_Authorize: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Terminal_Capture: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Terminal_Void: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Terminal_Refund: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Terminal_Credit: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Customer_Add: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Customer_List: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Transaction_Report: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Advanced_Report: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Is_Active:{type:DataTypes.BOOLEAN,defaultValue: 1},
			Integration: {type:DataTypes.BOOLEAN,defaultValue: 0},
            Enable_Nation: {type:DataTypes.BOOLEAN,defaultValue: 1},
            Recurring_Payments:{type:DataTypes.BOOLEAN,defaultValue: 0},
			Plan_Add: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Plan_List: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Subscription_Add: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Subscription_List: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Customer_Expiry: {type:DataTypes.BOOLEAN,defaultValue: 0},
            Easy_Buttons:{type:DataTypes.BOOLEAN,defaultValue: 0},
			Button_Add: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Button_List: {type:DataTypes.BOOLEAN,defaultValue: 0},
            Last_Changed_By: DataTypes.STRING
		},
		{
			instanceMethods: {
				generateSalt: function(password) {
					return new Buffer(crypto.randomBytes(keylen)).toString('base64');
				},
				generateHash: function(password, salt) {
					var hash = function(passphrase) {
						return crypto.pbkdf2Sync(passphrase, salt, iterations, keylen).toString('base64');
					};
					var hashPassword = hash(String(password));
					return hashPassword;
				},
				hashPassword: function(password) {
					if (this.Salt && password) {
						return crypto.pbkdf2Sync(password, this.Salt, iterations, keylen).toString('base64');
					} else {
						return password;
					}
				},
				authenticate: function(password){
					return this.Password === this.hashPassword(password);
				},
				findUniqueUsername: function(username, suffix, callback) {
					/*if (!password || !salt) return '';
					salt = new Buffer(salt, 'base64');
					return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');*/
					var _this = this;
					var possibleUsername = username + (suffix || '');

					_this.findOne({
						username: possibleUsername
					}, function(err, user) {
						if (!err) {
							if (!user) {
								callback(possibleUsername);
							} else {
								return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
							}
						} else {
							callback(null);
						}
					});
				}
			},
			associate: function(models) {
				//User.hasMany(models.Boarding);
			}
		}
	);

	return User;
};
