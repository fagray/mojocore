/*jslint node: true */
'use strict';

/**
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var MAS = sequelize.define('MAS', {
			Type: DataTypes.STRING(20),
			Ajusted_Amount: DataTypes.DECIMAL(10,2),
			Original_Amount: DataTypes.DECIMAL(10,2),
			Transaction_ID: DataTypes.STRING(20),
			Payment_Date: DataTypes.STRING(15),
			Merchant_ID: DataTypes.BIGINT(15),
			Payment_ID: DataTypes.BIGINT(20),
			Currency: DataTypes.INTEGER(4),
			Payment_Type: DataTypes.STRING(20),
			Destination: DataTypes.BIGINT(20),
			Disbursement_Type: DataTypes.STRING(30),
			Payment_Plan_ID: DataTypes.BIGINT(20),
			Processor_ID: DataTypes.STRING(16),
			Original_date: DataTypes.STRING(15),
			Line_Item_ID: DataTypes.INTEGER(20),
		},
		{
			freezeTableName: true,
			tableName: 'MAS',
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
		);

	return MAS;
};
