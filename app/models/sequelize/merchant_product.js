/*jslint node: true */
'use strict';

/**
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

var crypto = require('crypto');
var iterations = 1000; //2500;
var keylen = 64; //512;


module.exports = function(sequelize, DataTypes) {

	var Merchant_Product = sequelize.define('Merchant_Product', {
			Boarding_ID: DataTypes.INTEGER,
			Is_Active: {type:DataTypes.BOOLEAN,defaultValue: 1},
			Product_Name: DataTypes.STRING,
			Product_Description: DataTypes.STRING,
            Product_SKU: DataTypes.STRING,
            Product_Price: {type:DataTypes.DECIMAL(20,2),defaultValue: 0}, 
            Tax: {type:DataTypes.DECIMAL(20,2),defaultValue: 0}, 
            Shipping: {type:DataTypes.DECIMAL(20,2),defaultValue: 0},
            Hash: DataTypes.STRING,
            Button_Size_Display: DataTypes.STRING,
            Product_Currency: DataTypes.STRING
		},
		{
			instanceMethods: {
				generateHash: function() {
					return new Buffer(crypto.randomBytes(keylen)).toString('hex');
				},
			},
        },
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Merchant_Product;
};
