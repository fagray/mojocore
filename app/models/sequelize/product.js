/*jslint node: true */
'use strict';

/**
 *
 * Contain product names, e.g. Mojocore, Twt2pay, VideoCheckout
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Product = sequelize.define('Product', {
			name: DataTypes.STRING,
			description: DataTypes.STRING
		},
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Product;
};
