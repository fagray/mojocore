/*jslint node: true */
'use strict';

/**
 *
 * Contain time fees, used by Admin Account in Merchant Activity Report.
 * These fess will be processed in all Reports
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Line_Items = sequelize.define('Line_Items', {
			Boarding_ID: DataTypes.INTEGER,
			Processor_ID: DataTypes.STRING,
			Fee_Name: DataTypes.STRING,
			Fee_Amount: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
			Debit_Credit: DataTypes.STRING,
			Transaction_ID: DataTypes.STRING,
			Occurrence_Date: DataTypes.STRING,
			Description: DataTypes.STRING
		},
		{
			associate: function(models){
				//Line_Items.belongsTo(models.User);
			}
		}
	);

	return Line_Items;
};
