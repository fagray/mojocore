/*jslint node: true */
'use strict';

/**
 *
 * Contain discounts, fees, and credentials for each Merchant.
 * Is filled by Step 3 (pricing plan) in Boarding View.
 * This model is filled only for Admin and Super Admin Accounts.
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Payment_Plan = sequelize.define('Payment_Plan', {
            Boarding_ID: DataTypes.INTEGER,
            Product_ID: DataTypes.INTEGER,
            Discount_Rate: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Monthly_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Setup_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Authorization_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Chargeback_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Retrieval_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Is_Active: {type:DataTypes.BOOLEAN,defaultValue: 1},
            Sale_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Capture_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Void_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Chargeback_Reversal_Fee: {type:DataTypes.DECIMAL(10,3) ,defaultValue: 0},
            Refund_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Credit_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Declined_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Username: DataTypes.STRING,
            Processor_ID: DataTypes.STRING,
            Password: DataTypes.STRING,
            Ach_Reject_Fee: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
            Marketplace_Name: DataTypes.STRING,
            Last_Changed_By: DataTypes.STRING
		},
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Payment_Plan;
};
