/*jslint node: true */
'use strict';

/**
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

var crypto = require('crypto');
var iterations = 1000; //2500;
var keylen = 64; //512;


module.exports = function(sequelize, DataTypes) {

	var Merchant_Donation = sequelize.define('Merchant_Donation', {
			Boarding_ID: DataTypes.INTEGER,
			Is_Active: {type:DataTypes.BOOLEAN,defaultValue: 1},
			Donation_Name: DataTypes.STRING,
			Donation_Description: DataTypes.STRING,
            Donation_Amount_1: {type:DataTypes.DECIMAL(20,2),defaultValue: 5.00}, 
            Donation_Amount_2: {type:DataTypes.DECIMAL(20,2),defaultValue: 10.00}, 
            Donation_Amount_3: {type:DataTypes.DECIMAL(20,2),defaultValue: 25.00}, 
            Donation_Amount_4: {type:DataTypes.DECIMAL(20,2),defaultValue: 50.00}, 
            Allow_Custom_Donations: {type:DataTypes.BOOLEAN,defaultValue: 1},
            Hash: DataTypes.STRING,
            Button_Size_Display: DataTypes.STRING,
            Donation_Currency: DataTypes.STRING,
            Allow_Recurring_Donation: {type:DataTypes.BOOLEAN,defaultValue: 0} 
		},
		{
			instanceMethods: {
				generateHash: function() {
					return new Buffer(crypto.randomBytes(keylen)).toString('hex');
				},
			},
        },
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Merchant_Donation;
};
