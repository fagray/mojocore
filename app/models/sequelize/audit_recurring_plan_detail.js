/*jslint node: true */
'use strict';

/**
 *
 * Contains Recurring_Plan_Detail
 *
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Audit_Recurring_Plan_Detail = sequelize.define('Audit_Recurring_Plan_Detail', {
			Audit_Header_ID: DataTypes.BIGINT(20),
			Field_Name: DataTypes.STRING,
			Old_Value: DataTypes.STRING,
			New_Value: DataTypes.STRING
		},
		{
			associate: function(models){
				//Line_Items.belongsTo(models.User);
			}
		}
	);

	return Audit_Recurring_Plan_Detail;
};
