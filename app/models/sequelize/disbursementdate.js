/*jslint node: true */
'use strict';

/**
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Disbursementdate = sequelize.define('Disbursementdate', {
			boarding_id: DataTypes.INTEGER,
			weekly: {type:DataTypes.BOOLEAN,defaultValue: 0},
			monday: {type:DataTypes.BOOLEAN,defaultValue: 0},
			tuesday: {type:DataTypes.BOOLEAN,defaultValue: 0},
			wednesday: {type:DataTypes.BOOLEAN,defaultValue: 0},
			thursday: {type:DataTypes.BOOLEAN,defaultValue: 0},
			friday: {type:DataTypes.BOOLEAN,defaultValue: 0},
			recurweekly:DataTypes.INTEGER,
			daymonth: {type:DataTypes.BOOLEAN,defaultValue: 0},
			datemonth: DataTypes.INTEGER,
			everydate: DataTypes.INTEGER,
			dayorder: DataTypes.STRING,
			dayname: DataTypes.STRING,
			everymonth: DataTypes.STRING,
            dayhold: {type:DataTypes.INTEGER,defaultValue: 0}
		},
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Disbursementdate;
};
