/*jslint node: true */
'use strict';

/**
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Disbursement_Schedules = sequelize.define('Disbursement_Schedules', {
			Boarding_ID: DataTypes.INTEGER,
			Weekly: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Monday: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Tuesday: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Wednesday: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Thursday: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Friday: {type:DataTypes.BOOLEAN,defaultValue: 0},
			recurweekly:DataTypes.INTEGER,
			daymonth: {type:DataTypes.BOOLEAN,defaultValue: 0},
			datemonth: DataTypes.INTEGER,
			everydate: DataTypes.INTEGER,
			dayorder: DataTypes.STRING,
			dayname: DataTypes.STRING,
			everymonth: DataTypes.STRING,
            Hold: {type:DataTypes.INTEGER,defaultValue: 0},
            Last_Changed_By: DataTypes.STRING
		},
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Disbursement_Schedules;
};
