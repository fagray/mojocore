/*jslint node: true */
'use strict';

module.exports = function(sequelize, DataTypes) {

	var Vault = sequelize.define('Vault', {
			nmi: DataTypes.STRING,
            Merchant_ID: DataTypes.STRING,
            customer_vault_id: DataTypes.STRING,
            first_name: DataTypes.STRING,
            last_name: DataTypes.STRING,
            address_1: DataTypes.STRING,
            address_2: DataTypes.STRING,
            company: DataTypes.STRING,
            city: DataTypes.STRING,
            state: DataTypes.STRING,
            postal_code: DataTypes.STRING,
            country: DataTypes.STRING,
            email: DataTypes.STRING,
            phone: DataTypes.STRING,
            fax: DataTypes.STRING,
            cell_phone: DataTypes.STRING,
            customer_tax_id: DataTypes.STRING,
            website: DataTypes.STRING,
            shipping_first_name: DataTypes.STRING,
            shipping_last_name: DataTypes.STRING,
            shipping_address_1: DataTypes.STRING,
            shipping_address_2: DataTypes.STRING,
            shipping_company: DataTypes.STRING,
            shipping_city: DataTypes.STRING,
            shipping_state: DataTypes.STRING,
            shipping_postal_code: DataTypes.STRING,
            shipping_country: DataTypes.STRING,
            shipping_email: DataTypes.STRING,
            shipping_carrier: DataTypes.STRING,
            tracking_number: DataTypes.STRING,
            shipping_date: DataTypes.STRING,
            shipping: DataTypes.STRING,
            cc_number: DataTypes.STRING,
            cc_hash: DataTypes.STRING,
			cc_exp: DataTypes.STRING,
            cc_start_date: DataTypes.STRING,
            cc_issue_number: DataTypes.STRING,
            check_account: DataTypes.STRING,
            check_hash: DataTypes.STRING,
            check_aba: DataTypes.STRING,
            check_name: DataTypes.STRING,
            account_holder_type: DataTypes.STRING,
            account_type: DataTypes.STRING,
            sec_code: DataTypes.STRING,
            processor_id: DataTypes.STRING,
            cc_bin: DataTypes.STRING,
            merchant_defined_field_1: DataTypes.STRING,
            merchant_defined_field_2: DataTypes.STRING,
            merchant_defined_field_3: DataTypes.STRING,
            merchant_defined_field_4: DataTypes.STRING,
            merchant_defined_field_5: DataTypes.STRING,
            merchant_defined_field_6: DataTypes.STRING,
            merchant_defined_field_7: DataTypes.STRING,
            merchant_defined_field_8: DataTypes.STRING,
            merchant_defined_field_9: DataTypes.STRING,
            merchant_defined_field_10: DataTypes.STRING,
            merchant_defined_field_11: DataTypes.STRING,
            merchant_defined_field_12: DataTypes.STRING,
            merchant_defined_field_13: DataTypes.STRING,
            merchant_defined_field_14: DataTypes.STRING,
            merchant_defined_field_15: DataTypes.STRING,
            merchant_defined_field_16: DataTypes.STRING,
            merchant_defined_field_17: DataTypes.STRING,
            merchant_defined_field_18: DataTypes.STRING,
            merchant_defined_field_19: DataTypes.STRING,
            merchant_defined_field_20: DataTypes.STRING,
            amount: DataTypes.STRING,
            currency: DataTypes.STRING,
            descriptor: DataTypes.STRING,
            descriptor_phone: DataTypes.STRING,
            order_description: DataTypes.STRING,
            order_id: DataTypes.STRING,
            po_number: DataTypes.STRING,
            tax: DataTypes.STRING,
            tax_exempt: DataTypes.STRING,
            Is_Active: {type:DataTypes.BOOLEAN,defaultValue: 1},
            Num_Emails_Sent: {type:DataTypes.INTEGER,defaultValue: 0},
            Last_Email_Sent: DataTypes.DATE,
            Last_Call_Made: DataTypes.DATE
		},
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Vault;
};
