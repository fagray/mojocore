/*jslint node: true */
'use strict';

/**
 *
 * Contains Account Balances
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Nation_Builder_Mojopay = sequelize.define('Nation_Builder_Mojopay', {
			Redirect_URL: DataTypes.STRING,
			SLUG: DataTypes.STRING,
			Client_ID: DataTypes.STRING,
			Client_Secret: DataTypes.STRING
		},
		{
		    freezeTableName: true,
		    tableName: 'Nation_Builder_Mojopay',
		    associate: function(models){
		    
			}
	   }
	);

	return Nation_Builder_Mojopay;
};
