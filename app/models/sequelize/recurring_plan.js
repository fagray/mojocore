/*jslint node: true */
'use strict';

/**
 *
 * Contains Payment Plans.
 * These fess will be processed in all Reports
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Recurring_Plan = sequelize.define('Recurring_Plan', {
			Boarding_ID: DataTypes.INTEGER,
			Processor_ID: DataTypes.STRING,
			Plan_Name: DataTypes.STRING,
			Plan_ID: DataTypes.INTEGER,
			Plan_Amount: {type:DataTypes.DECIMAL(10,2) ,defaultValue: 0},
			Plan_Payments: DataTypes.INTEGER,
			Day_Frequency: DataTypes.INTEGER,
			Month_Frequency: DataTypes.INTEGER,
            Day_Of_Month: DataTypes.INTEGER,
            Is_Active:{type:DataTypes.BOOLEAN,defaultValue: 1} 
		},
		{
			associate: function(models){
				//Line_Items.belongsTo(models.User);
			}
		}
	);

	return Recurring_Plan;
};
