/*jslint node: true */
'use strict';

/**
 *
 * Contains Recurring_Plan_Header
 *
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Audit_Recurring_Plan_Header = sequelize.define('Audit_Recurring_Plan_Header', {
			Boarding_ID: DataTypes.BIGINT(20),
			Processor_ID: DataTypes.STRING,
			Recurring_Plan_ID: DataTypes.BIGINT(20),
		},
		{
			associate: function(models){
				//Line_Items.belongsTo(models.User);
			}
		}
	);

	return Audit_Recurring_Plan_Header;
};
