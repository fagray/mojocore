/*jslint node: true */
'use strict';

/**
 *
 * An alternative model to retrieve transactions instead to call NMI API.
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Transaction_History = sequelize.define('Transaction_History', {
			transaction_id: DataTypes.STRING(50),
			platform_id: DataTypes.STRING(50),
			transaction_type: DataTypes.STRING(50),
			transaction_condition: DataTypes.STRING(50),
			order_id: DataTypes.STRING(50),
			authorization_code: DataTypes.STRING(50),
			ponumber: DataTypes.STRING(50),
			order_description: DataTypes.STRING(50),
			first_name: DataTypes.STRING(50),
			last_name: DataTypes.STRING(50),
			address_1: DataTypes.STRING(50),
			address_2: DataTypes.STRING(50),
			company: DataTypes.STRING(50),
			city: DataTypes.STRING(50),
			state: DataTypes.STRING(50),
			postal_code: DataTypes.STRING(50),
			country: DataTypes.STRING(50),
			email: DataTypes.STRING(50),
			phone: DataTypes.STRING(50),
			fax: DataTypes.STRING(50),
			cell_phone: DataTypes.STRING(50),
			customertaxid: DataTypes.STRING(50),
			customerid: DataTypes.STRING(50),
			website: DataTypes.STRING(50),
			shipping_first_name: DataTypes.STRING(50),
			shipping_last_name: DataTypes.STRING(50),
			shipping_address_1: DataTypes.STRING(50),
			shipping_address_2: DataTypes.STRING(50),
			shipping_company: DataTypes.STRING(50),
			shipping_city: DataTypes.STRING(50),
			shipping_state: DataTypes.STRING(50),
			shipping_postal_code: DataTypes.STRING(50),
			shipping_country: DataTypes.STRING(50),
			shipping_email: DataTypes.STRING(50),
			shipping_carrier: DataTypes.STRING(50),
			tracking_number: DataTypes.STRING(50),
			shipping_date: DataTypes.STRING(50),
			Shipping: DataTypes.STRING(50),
			shipping_phone: DataTypes.STRING(50),
			cc_number: DataTypes.STRING(50),
			cc_hash: DataTypes.STRING(50),
			cc_exp: DataTypes.STRING(50),
			cavv: DataTypes.STRING(50),
			cavv_result: DataTypes.STRING(50),
			xid: DataTypes.STRING(50),
			avs_response: DataTypes.STRING(50),
			csc_response: DataTypes.STRING(50),
			cardholder_auth: DataTypes.STRING(50),
			cc_start_date: DataTypes.STRING(50),
			cc_issue_number: DataTypes.STRING(50),
			check_account: DataTypes.STRING(50),
			check_hash: DataTypes.STRING(50),
			check_aba: DataTypes.STRING(50),
			check_name: DataTypes.STRING(50),
			account_holder_type: DataTypes.STRING(50),
			account_type: DataTypes.STRING(50),
			sec_code: DataTypes.STRING(50),
			drivers_license_number: DataTypes.STRING(50),
			drivers_license_state: DataTypes.STRING(50),
			drivers_license_dob: DataTypes.STRING(50),
			social_security_number: DataTypes.STRING(50),
			processor_id: DataTypes.STRING(50),
			tax: DataTypes.STRING(50),
			currency: DataTypes.STRING(50),
			surcharge: DataTypes.STRING(50),
			tip: DataTypes.STRING(50),
			cc_bin: DataTypes.STRING(50),
			amount: DataTypes.STRING(50),
			action_type: DataTypes.STRING(50),
			date: DataTypes.STRING(50),
			success: DataTypes.STRING(50),
			ip_address: DataTypes.STRING(50),
			source: DataTypes.STRING(50),
			username: DataTypes.STRING(50),
			response_text: DataTypes.STRING(50),
			batch_id: DataTypes.STRING(50),
			processor_batch_id: DataTypes.STRING(50),
			response_code: DataTypes.STRING(50),
			device_license_number: DataTypes.STRING(50),
			device_nickname: DataTypes.STRING(50),
			merchantId: DataTypes.STRING(25),
			id: DataTypes.BIGINT(20),
            Original_Transaction_ID: DataTypes.STRING(50)
		},
		{
			freezeTableName: true,
			tableName: 'Transaction_History',
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
		);

	return Transaction_History;
};
