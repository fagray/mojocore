/*jslint node: true */
'use strict';

/**
 * Contain hash related to credit card number.
 * Is used to retrieve the first six and last four credit card numbers.
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Cchash_boarding = sequelize.define('Cchash_boarding', {
			boarding_id: DataTypes.STRING,
			cc_number: DataTypes.STRING,
            brand: DataTypes.STRING,
			hash: DataTypes.STRING
		},
		{
			associate: function(models){
				//Cchash.belongsTo(models.User);
			}
		}
	);

	return Cchash_boarding;
};
