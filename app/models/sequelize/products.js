'use strict';

/**
 *
 * Contain product names, e.g. Mojocore, Twt2pay, VideoCheckout
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Product = sequelize.define('Product', {
			Name: DataTypes.STRING,
			Description: DataTypes.STRING,
            Active: {type:DataTypes.INTEGER,defaultValue: 1},
            Marketplace: {type:DataTypes.INTEGER,defaultValue: 0}
		},
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Product;
};
