/*jslint node: true */
'use strict';

/**
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Disbursements = sequelize.define('Disbursements', {
			name: DataTypes.STRING,
			description: DataTypes.STRING
		},
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Disbursements;
};
