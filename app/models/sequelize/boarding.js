/*jslint node: true */
'use strict';

/**
 * Contain information about the Merchant, is filled in Boarding View.
 *
 * @param sequelize         A promise-based ORM for Node.js, reference: sequelizejs.com
 * @param DataTypes         Used to define data type, reference: http://docs.sequelizejs.com/en/latest/api/datatypes/
 * @returns {*}
 *
 */

module.exports = function(sequelize, DataTypes) {

	var Boarding = sequelize.define('Boarding', {
			Date: DataTypes.STRING,
			Merchant_Name: DataTypes.STRING,
			Address: DataTypes.STRING,
			City: DataTypes.STRING,
			Phone: DataTypes.STRING,
			Merchant_ID: DataTypes.STRING,
			Bank_ID: DataTypes.STRING,
			Terminal_ID: DataTypes.STRING,
			MCC: DataTypes.STRING,
			Classification: DataTypes.STRING,
			Visa: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Mastercard: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Discover: {type:DataTypes.BOOLEAN,defaultValue: 0},
			American_Express: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Diners_Club: {type:DataTypes.BOOLEAN,defaultValue: 0},
			JCB: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Maestro: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Max_Ticket_Amount: DataTypes.DECIMAL(10,2),
			Max_Monthly_Amount: DataTypes.DECIMAL(10,2),
			Precheck: DataTypes.STRING,
			Duplicate_Checking: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Allow_Merchant: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Duplicate_Threshold: DataTypes.INTEGER,
			Account_Description: DataTypes.STRING,
			Location_Number: DataTypes.STRING,
			Aquire_Bin: DataTypes.STRING,
			Store_Number: DataTypes.STRING,
			Vital_Number: DataTypes.STRING,
			Agent_Chain: DataTypes.STRING,
			Required_Name: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Required_Company: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Required_Address: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Required_City: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Required_State: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Required_Zip:{type:DataTypes.BOOLEAN,defaultValue: 0},
			Required_Country: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Required_Phone: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Required_Email: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Reserve_Rate: {type:DataTypes.INTEGER,defaultValue: 0},
			Reserve_Condition: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Cap_Amount: DataTypes.DECIMAL(10,2),
			Disbursement: {type:DataTypes.BOOLEAN,defaultValue: 0},
			Status:{type:DataTypes.ENUM('pending','active','achreject','restricted','suspended','closed','deleted'),defaultValue: 'pending'},
            Welcome_Email: {type:DataTypes.BOOLEAN,defaultValue: 0},
            State: DataTypes.STRING,
            Zip: DataTypes.STRING,
            Last_Changed_By: DataTypes.STRING
		},
		{
			associate: function(models){
				//Boarding.belongsTo(models.User);
			}
		}
	);

	return Boarding;
};
