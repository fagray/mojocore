'use strict';

/**
 *
 * Express routes
 * MDF Routes
 * To make HTTP requests to controller methods for MDF Controller
 * @param app
 */

module.exports = function(app) {
	var mdf = require('../../app/controllers/mdf');

	app.route('/mdf/:historyId')
		.get(mdf.show);

	app.param('historyId', mdf.mdf);

};
