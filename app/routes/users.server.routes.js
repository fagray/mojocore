'use strict';


/**
 *
 * Express routes
 * Users Date Routes
 * To make HTTP requests to controller methods for Users Controller
 * @param app
 */

var passport = require('passport');

module.exports = function(app) {
	// User Routes
	var users = require('../../app/controllers/users');

	// Setting up the users profile api
	app.route('/users/me').get(users.me);
	app.route('/users').put(users.update);
	app.route('/users/accounts').delete(users.removeOAuthProvider);
	app.route('/usersupdate')
		.get(users.isAdmin,users.all);
	app.route('/usersupdate/:Boarding_ID')
		.get(users.UserShow)
		.put(users.usersupdate);
	app.route('/adminsupdate/:id')
		.get(users.AdminShow)
		.put(users.adminsupdate);
	app.route('/boardmerchant/:nmiboardingId')
		.get(users.boardmerchantshow)
		.put(users.updateboardmerchant);
	// Setting up the users password api
	app.route('/users/password').post(users.changePassword);
	app.route('/auth/forgot').post(users.forgot);
	//app.route('/auth/reset/:token').get(users.validateResetToken);
	app.route('/auth/reset/:token').post(users.reset);
	app.route('/auth/validateResetToken/:token').post(users.validateResetToken);
	app.route('/auth/chameleonate').post(users.chameleonate);
	// Setting up the users authentication api
	//app.route('/auth/signup').post(users.signup);
	app.route('/auth/boardmojo').post(users.boardmojo);
	app.route('/auth/boardmojoadmin').post(users.boardmojoadmin);
	app.route('/auth/boardmojoCheck').post(users.boardmojoCheck);
	app.route('/auth/boardmerchant').post(users.boardmerchant);
	app.route('/auth/signin').post(users.signin);
	app.route('/auth/signout').get(users.signout);

	// Setting the facebook oauth routes
	app.route('/auth/facebook').get(passport.authenticate('facebook', {
		scope: ['email']
	}));
	app.route('/auth/facebook/callback').get(users.oauthCallback('facebook'));

	// Setting the twitter oauth routes
	app.route('/auth/twitter').get(passport.authenticate('twitter'));
	app.route('/auth/twitter/callback').get(users.oauthCallback('twitter'));

	// Setting the google oauth routes
	app.route('/auth/google').get(passport.authenticate('google', {
		scope: [
			'https://www.googleapis.com/auth/userinfo.profile',
			'https://www.googleapis.com/auth/userinfo.email'
		]
	}));
	app.route('/auth/google/callback').get(users.oauthCallback('google'));

	// Setting the linkedin oauth routes
	app.route('/auth/linkedin').get(passport.authenticate('linkedin'));
	app.route('/auth/linkedin/callback').get(users.oauthCallback('linkedin'));
	
	// Setting the github oauth routes
	app.route('/auth/github').get(passport.authenticate('github'));
	app.route('/auth/github/callback').get(users.oauthCallback('github'));

	// merchant email routine
	app.route('/users/emailreceipt').get(users.emailreceipt);
	// merchant print routine
	app.route('/users/printreceipt').get(users.printreceipt);
	
	// Finish by binding the user middleware
	app.param('nmiboardingId', users.getboardmerchant);
	app.param('Boarding_ID', users.getUserUpdate);
	app.param('id', users.getAdminUpdate);
	app.param('userId', users.userByID);
};
	
