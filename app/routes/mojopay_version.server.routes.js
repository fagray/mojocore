'use strict';

/**
 *
 * Express routes
 * Vault Routes
 * To make HTTP requests to controller methods for Vault Controller
 * @param app
 */

module.exports = function(app) {
	var mojopay_version = require('../../app/controllers/mojopay_version');

	// vault Routes
	app.route('/getversion')
		.get(mojopay_version.getVersion);
};
