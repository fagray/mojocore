'use strict';

/**
 *
 * Express routes
 * Product Routes
 * To make HTTP requests to controller methods for Product Controller
 * @param app
 */

module.exports = function(app) {
	var product = require('../../app/controllers/product');

	app.route('/product/:productId')
		.get(product.show);

	app.param('productId', product.product);

};
