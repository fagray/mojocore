'use strict';

/**
 *
 * Express routes
 * Product Routes
 * To make HTTP requests to controller methods for Product Controller
 * @param app
 */

module.exports = function(app) {
    var nation_builder_client = require('../../app/controllers/nation_builder_client');

    app.route('/nationbuilderintegration/:ProcessorID?')
    .get(nation_builder_client.show);
    app.route('/nationbuilderactivation')
    .put(nation_builder_client.activation);
    app.route('/nationbuilderoauthcallback/:code?')
    .get(nation_builder_client.callback);
    app.route('/nation_builder_code_callback')
    .get(nation_builder_client.gotcode);
    app.route('/nation_builder_get_oauth')
    .post(nation_builder_client.getoauth);

    app.param('ProcessorID', nation_builder_client.nationBuilderClient);

};
