'use strict';

/**
 *
 * Express routes
 * MAS Routes
 * To make HTTP requests to controller methods for MAS Controller
 * @param app
 */

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var mas = require('../../app/controllers/mas');

	// Nmitransactions Routes
	app.route('/mas')
		.get(mas.all);

	app.route('/mas/:masprocessorid/:paymentdate?')
		.get(mas.show);

	app.param('masprocessorid', mas.mas);

};
