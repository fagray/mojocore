'use strict';

/**
 *
 * Express routes
 * Product Routes
 * To make HTTP requests to controller methods for Product Controller
 * @param app
 */

module.exports = function(app) {
	var nation_builder_mojopay = require('../../app/controllers/nation_builder_mojopay');

	app.route('/nationbuilderinfo/:slug')
	.get(nation_builder_mojopay.show);

	app.param('slug', nation_builder_mojopay.nationBuilderMojopay);
};
