'use strict';

/**
 *
 * Express routes
 * Subscription Routes
 * To make HTTP requests to controller methods for Merchant Product Controller
 * @param app
 */

module.exports = function(app) {
	var merchant_product = require('../../app/controllers/merchant_product');

	// Nmitransactions Routes
	app.route('/merchant_product')
		.post(merchant_product.create)
        .put(merchant_product.update);

	app.route('/merchant_product/:boardingId')
		.get(merchant_product.show);

    app.param('boardingId', merchant_product.merchant_product);    
};
