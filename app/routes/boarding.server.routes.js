'use strict';

/**
 *
 * Express routes
 * Boarding Routes
 * To make HTTP requests to controller methods for Boarding Controller
 * @param app
 */

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var boarding = require('../../app/controllers/boarding');

	// Nmitransactions Routes
	app.route('/boardings')
		.get(users.isAdmin,boarding.all)
		.post(users.isAdmin,boarding.create);

	app.route('/boardings/:boardingId')
		.get(users.isAdmin,boarding.show)
		.put(users.isAdmin, boarding.update)
		.delete(users.isAdmin, boarding.destroy);

	app.param('boardingId', boarding.boarding);
};
