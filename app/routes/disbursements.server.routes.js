'use strict';

/**
 *
 * Express routes
 * Disbursement Routes
 * To make HTTP requests to controller methods for Disbursement Controller
 * @param app
 */

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var disbursements = require('../../app/controllers/disbursements');

	// Nmitransactions Routes
	app.route('/disbursements')
		.get(disbursements.all)
		.post(disbursements.create);
};
