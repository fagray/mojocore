'use strict';

/**
 *
 * Express routes
 * NMI Transactions Routes
 * To make HTTP requests to controller methods for NMI Transactions Controller
 * @param app
 */

module.exports = function(app) {
    var users = require('../../app/controllers/users');
    var transactions = require('../../app/controllers/transactions');

    // Nmitransactions Routes
    app.route('/transactions')
        .get(users.requiresLogin,transactions.getTransaction);

    app.route('/processor')
        .get(users.requiresLogin,transactions.getProcessor);
        
    app.route('/nmiusername')
        .get(users.requiresLogin,transactions.getNMIUsername);
};


