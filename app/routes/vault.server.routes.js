'use strict';

/**
 *
 * Express routes
 * Vault Routes
 * To make HTTP requests to controller methods for Vault Controller
 * @param app
 */

module.exports = function(app) {
	var vault = require('../../app/controllers/vault');

	// vault Routes
	app.route('/customervault')
		.get(vault.customer_vault)
		.post(vault.create)
        .put(vault.update);
};
