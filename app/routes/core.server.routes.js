'use strict';

/**
 *
 * Express routes
 * Core
 * @param app
 */

module.exports = function(app) {
	// Root routing
	var core = require('../../app/controllers/core');
	app.route('/').get(core.index);
};
