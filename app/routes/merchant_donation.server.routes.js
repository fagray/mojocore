'use strict';

/**
 *
 * Express routes
 * Subscription Routes
 * To make HTTP requests to controller methods for Merchant Donation Controller
 * @param app
 */

module.exports = function(app) {
	var merchant_donation = require('../../app/controllers/merchant_donation');

	// Nmitransactions Routes
	app.route('/merchant_donation')
		.post(merchant_donation.create)
        .put(merchant_donation.update);

	app.route('/merchant_donation/:boardingId')
		.get(merchant_donation.show);

    app.param('boardingId', merchant_donation.merchant_donation);    
};
