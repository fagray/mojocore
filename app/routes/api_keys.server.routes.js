'use strict';

/**
 *
 * Express routes
 * ApiKey Routes
 * To make HTTP requests to controller methods for ApiKey Controller
 * @param app
 */

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var api_key = require('../../app/controllers/api_key');

	app.route('/apikey')
		.get(api_key.all)
		.post(api_key.create);

	app.route('/apikey/:boardingapi')
		.get(api_key.show)
		.delete(api_key.destroy);

	app.param('boardingapi', api_key.api_key);

};
