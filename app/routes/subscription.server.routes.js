'use strict';

/**
 *
 * Express routes
 * Subscription Routes
 * To make HTTP requests to controller methods for Subscription Controller
 * @param app
 */

module.exports = function(app) {
	var subscription = require('../../app/controllers/subscription');

	// Nmitransactions Routes
	app.route('/subscription')
		.get(subscription.all)
		.post(subscription.create)
        .put(subscription.update);

	app.route('/subscription/:subId')
        .get(subscription.findOne);

	app.route('/subscription/:boardingId/:productId')
		.get(subscription.show);

    app.param('boardingId', subscription.subscription);    
};
