'use strict';

/**
 *
 * Express routes
 * Disbursement Date Routes
 * To make HTTP requests to controller methods for Disbursement Date Controller
 * @param app
 */

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var disbursement_schedule = require('../../app/controllers/disbursement_schedule');

	// Nmitransactions Routes
	app.route('/disbursement_schedule')
		.get(users.isAdmin,disbursement_schedule.all)
		.post(users.isAdmin,disbursement_schedule.create);

	app.route('/disbursement_schedule/:userboardingId')
		.get(disbursement_schedule.show)
		.put(disbursement_schedule.update)
		.delete(users.isAdmin, disbursement_schedule.destroy);

	app.param('userboardingId', disbursement_schedule.disbursement_schedule);

};
