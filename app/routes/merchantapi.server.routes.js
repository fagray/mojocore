'use strict';

/**
 *
 * Express routes
 * Merchant API Routes
 * To make HTTP requests to controller methods for Merchant API Controller
 * @param app
 */

module.exports = function(app) {
	var merchantapi = require('../../app/controllers/merchantapi');

	// Nmitransactions Routes
	app.route('/merchantapi')
		.get(merchantapi.transactions);

};
