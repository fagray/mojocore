'use strict';

/**
 *
 * Express routes
 * Customer_Card_Hash Routes
 * To make HTTP requests to controller methods for Customer_Card_Hash Controller
 * @param app
 */

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var customer_card_hash = require('../../app/controllers/customer_card_hash');

	// Nmitransactions Routes
	app.route('/customer_card_hash')
		.get(customer_card_hash.all)
		.post(customer_card_hash.create);
};
