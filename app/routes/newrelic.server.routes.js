'use strict';

module.exports = function(app) {
	// Root routing
	var newrelic_mysql_connect = require('../../app/controllers/newrelic_mysql_connect');
	var newrelic_mysql_query = require('../../app/controllers/newrelic_mysql_query');
	app.route('/newrelic_mysql_connect').get(newrelic_mysql_connect.newrelic_mysql_connect);
	app.route('/newrelic_mysql_query').get(newrelic_mysql_query.newrelic_mysql_query);
};