'use strict';

/**
 *
 * Express routes
 * Product Routes
 * To make HTTP requests to controller methods for Product Controller
 * @param app
 */

module.exports = function(app) {
	var Account_Balance = require('../../app/controllers/account_balance');

	app.route('/account_balance/:processorId/:balanceDate')
		.get(Account_Balance.Account_Balance);

	//app.param('processorId', Account_Balance.Account_Balance);
	//app.param('balanceDate', Account_Balance.Account_Balance);

};
