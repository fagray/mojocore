'use strict';

/**
 *
 * Express routes
 * Recurring Plan Routes
 * To make HTTP requests to controller methods for Recurring Plan Controller
 * @param app
 */

module.exports = function(app) {
	var recurring_plan = require('../../app/controllers/recurring_plan');
	var audit_recurring_plan_header = require('../../app/controllers/audit_recurring_plan_header');
    var audit_recurring_plan_detail = require('../../app/controllers/audit_recurring_plan_detail');

    // Recurring_Plan Routes
    app.route('/recurringplan/:boardingId/:productId')
        .get(recurring_plan.show);
    app.route('/recurringplan')
        .get(recurring_plan.all)
        .post(recurring_plan.create)
        .put(recurring_plan.update);
	app.route('/recurringplan/:Plan_ID')
		.get(recurring_plan.findOne);
    app.route('/checkrecurringplan')
        .get(recurring_plan.checkRecurringPlan);
	
    app.route('/auditrecurringplanheader/:planID')
        .get(audit_recurring_plan_header.show);
    app.route('/auditrecurringplanheader')
        .post(audit_recurring_plan_header.create);
    app.route('/auditrecurringplanrecord')
        .get(audit_recurring_plan_header.findRecords);
    app.route('/auditrecurringplanheaderrecord/:Header_ID')
        .get(audit_recurring_plan_header.findOne);

    app.route('/auditrecurringplandetailrecords/:Audit_Header_ID')
        .get(audit_recurring_plan_detail.all);
    app.route('/auditrecurringplandetail')
        .post(audit_recurring_plan_detail.create);

    app.param('boardingId', recurring_plan.recurring_plan);
    app.param('Plan_ID', recurring_plan.findOne);
};
