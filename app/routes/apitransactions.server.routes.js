'use strict';

/**
 *
 * Express routes
 * Api Transactions Routes
 * To make HTTP requests to controller methods for Api Transactions Controller
 * @param app
 */

module.exports = function(app) {
	var apitransactions = require('../../app/controllers/apitransactions');

	// Nmitransactions Routes
	app.route('/apitransactions')
		.get(apitransactions.transactions);

};
