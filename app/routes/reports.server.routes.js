'use strict';

/**
 *
 * Express routes
 * NMI Transactions Routes
 * To make HTTP requests to controller methods for NMI Transactions Controller
 * @param app
 */

module.exports = function(app) {
    var users = require('../../app/controllers/users');
    var reports = require('../../app/controllers/reports');

    // Nmitransactions Routes
    app.route('/merchantActivity')
        .get(users.requiresLogin,reports.getMerchantActivity);
    app.route('/accountBalance')
        .get(users.requiresLogin,reports.getAccountBalance);
    app.route('/paymentDates')
        .get(users.requiresLogin,reports.getPaymentDates);
    app.route('/settleTransactions')
        .get(users.requiresLogin,reports.getSettleTransactions);
    app.route('/LineItemsTransactions')
        .get(users.requiresLogin,reports.getLineItemsTransactions);
    app.route('/discountTransactions')
        .get(users.requiresLogin,reports.getDiscounts);
    app.route('/feeTransactions')
        .get(users.requiresLogin,reports.getTransactionFee);
    app.route('/doBrand')
        .get(reports.createBrand);
    app.route('/ACHActivity')
        .get(users.requiresLogin,reports.getACHActivity);
    app.route('/refundTransactions')
        .get(users.requiresLogin,reports.getRefundActivity);    
};
