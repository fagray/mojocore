'use strict';

/**
 *
 * Express routes
 * NMI Transactions Routes
 * To make HTTP requests to controller methods for NMI Transactions Controller
 * @param app
 */

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var nmitransactions = require('../../app/controllers/nmitransactions');

	// Nmitransactions Routes
	app.route('/reserve')
		.get(users.requiresLogin,nmitransactions.getReserve);
};


