'use strict';

/**
 *
 * Express routes
 * Product Routes
 * To make HTTP requests to controller methods for Product Controller
 * @param app
 */

module.exports = function(app) {
	var Line_Items = require('../../app/controllers/line_items');

	app.route('/line_items/:processorId')
		.get(Line_Items.Line_Items);

	//app.param('processorId', Account_Balance.Account_Balance);

};
