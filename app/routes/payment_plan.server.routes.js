'use strict';

/**
 *
 * Express routes
 * Payment Plan Routes
 * To make HTTP requests to controller methods for Payment Plan Controller
 * @param app
 */

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var payment_plan = require('../../app/controllers/payment_plan');

	// Nmitransactions Routes
	app.route('/paymentplan')
		.get(users.isAdmin,payment_plan.all)
		.post(users.isAdmin,payment_plan.create);

	app.route('/paymentplan/:paymentId/:productId?')
		.get(payment_plan.show)
		.put(payment_plan.update)
		.delete(users.isAdmin, payment_plan.destroy);

    app.route('/updatepaymentplan')
        .put(payment_plan.update);

    app.route('/allPaymentPlans')
        .get(users.isAdmin,payment_plan.allPaymentPlans);

    app.route('/isPayment_Plan/:payment?/:product?')
        .get(payment_plan.isPayment_Plan);

    app.route('/paymentPlanComplete')
        .get(payment_plan.paymentPlanComplete);

    app.route('/paymentSplit')
        .get(payment_plan.paymentSplit);

	app.param('paymentId', payment_plan.payment_plan);
};
