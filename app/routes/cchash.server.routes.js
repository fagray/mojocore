'use strict';

/**
 *
 * Express routes
 * CChash Routes
 * To make HTTP requests to controller methods for CChash Controller
 * @param app
 */

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var cchash = require('../../app/controllers/cchash');

	// Nmitransactions Routes
	app.route('/cchashs')
		.get(cchash.all)
		.post(cchash.create);
};
